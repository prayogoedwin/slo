<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
      {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> --}}
    <title>Cetak Detail Verifikasi</title>

    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .gray {
            background-color: lightgray
        }
    </style>

</head>

<body>

    {{-- <table style="width: 786px; margin-left: auto; margin-right: auto;">
  <tbody>
  <tr style="height: 68px;">
  <td style="width: 708px; height: 121px; text-align: center;"><img style="float: left; margin-top: -30px;" src="http://slo.sertifikasiinstalasiprima.co.id/images/kopp.png" alt="" width="708" height="121" /></td>
  </tr>
  </tbody>
  </table>
  <br> --}}
    <div class="container">
        <div class="col-lg-12 text-center">
            <img src="http://slo.sertifikasiinstalasiprima.co.id/images/kopp.png" alt="" class="img-responsive" width="708" height="121">
        </div>
        <div class="col-lg-12 text-center" style="margin-top: 10px;">
            <h6><b>DATA HASIL VERIFIKASI</b></h6>
        </div>
    </div>
    <div class="col-lg-12" style="margin-top: 10px;">
        <div class="row">
            <table width="100%" style="font-size: 14px">
                @foreach($pelanggan as $p)
                <tr>
                    <th>No Pendaftaran</th>
                    <td>:</td>
                    <td>{{$p->no_pendaftaran}}</td>
                </tr>
                <tr>
                    <th>Atas Nama Pemilik / Konsumen</th>
                    <td>:</td>
                    <td>{{$p->nama}}</td>
                </tr>
                <tr>
                    <th>Nama BTL</th>
                    <td>:</td>
                    <td>{{$p->btl->nama_btl}}</td>
                </tr>
                <tr>
                    <th>Alamat</th>
                    <td>:</td>
                    <td>{{$p->alamat}}</td>
                </tr>
                <tr>
                    <th>Tarif/ Daya</th>
                    <td>:</td>
                    <td>{{$p->tarif->jenis_tarif}}/ {{$p->daya->daya}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <hr>
    <div class="col-lg-12" style="margin-top: 10px;">
            <p>Hasil Pemeriksaan</p>
            <table class="table-bordered" width="100%" style="font-size: 14px">
                @foreach($pemeriksaan as $pm)
                <tr>
                    <td>Jumlah PHB</td>
                    <td class="text-center">{{$pm->phbk_utama}}</td>
                </tr>
                <tr>
                    <td>Jumlah Saluran Cabang</td>
                    <td class="text-center">{{$pm->jml_saluran_cabang}}</td>
                </tr>
                <tr>
                    <td>Jumlah Saluran Akhir</td>
                    <td class="text-center">{{$pm->jml_saluran_akhir}}</td>
                </tr>
                <tr>
                    <td>Jumlah Titik Lampu</td>
                    <td class="text-center">{{$pm->jml_titik_lampu}}</td>
                </tr>
                <tr>
                    <td>Jumlah KKB</td>
                    <td class="text-center">{{$pm->kkb}}</td>
                </tr>
                <tr>
                    <td>Jumlah KKK</td>
                    <td class="text-center">{{$pm->kkk}}</td>
                </tr>
                <tr>
                    <td>Jumlah KKK</td>
                    <td class="text-center">{{$pm->kkk}}</td>
                </tr>
                <tr>
                    <td>Jumlah Tahanan Isolasi Penghantar</td>
                    <td class="text-center">{{$pm->tahanan_isolasi_penghantar}} (Ω)</td>
                </tr>
                <tr>
                    <td>Sistem Pembumian</td>
                    <td class="text-center">{{$pm->penghantar_bumi_sistem}}</td>
                </tr>
                <tr>
                    <td>Jumlah Sistem Pembumian</td>
                    <td class="text-center">{{$pm->resisten_pembumian}} (MΩ)</td>
                </tr>
                @endforeach
            </table>
    </div>
    <div class="row">
      <div class="col-lg-12" style="margin-top: 10px;">
        <table width="100%" style="font-size: 14px; margin: 5px;">
          @foreach($verifikasi as $ver)
          <tr>
            <td>Hasil Verifikasi</td>
            <td>{{$ver->hasil_pemeriksaan}}</td>
          </tr>
          @if ($ver->hasil_pemeriksaan = "Perlu perbaikan ulang (PPU)")
              <tr>
                <td>Daftar Perbaikan</td>
                <td>{{$ver->ceklist_ppu}}</td>
              </tr>
          @endif
          <tr>
            <td>Catatan</td>
            <td>{{$ver->catatan}}</td>
          </tr>
          @endforeach
          </table>

      </div>
    </div>

    <div class="row">
      <div class="col-lg-12 text-right" style="margin-top: 30px;">
        <p>{{ date('d F Y')}}</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 text-right" style="margin-top: 60px;">
        @foreach ($verifikasi as $verifikator)
          <p>{{$verifikator->verifikator->nama}}</p>
        @endforeach
      </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>


</body>

</html>
