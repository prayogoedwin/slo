<!doctype html>
<html lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Cetak Sertifikat</title>
		<style>
			
		</style>
	</head>
	<body  style="background-image: url(https://slo.sertifikasiinstalasiprima.co.id/images/sertifikat_blank_a4.png); width:100%; padding:80px; background-position: center; background-repeat: no-repeat; background-size: cover;">
		 @foreach($sertifikat as $sertif)
    <table style="width: 100%; margin-right: auto; margin-left; auto; font-size: 15px; margin-top: -20px">
        <tbody>
            <tr>
                <td style="width: 100%; text-align: center;"><strong>PT. SERTIFIKASI INSTALASI PRIMA</strong></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table style="width: 100%; margin-right: auto; margin-left: auto; font-size: 12px;">
        <tbody>
            <tr>
                <td style="width: 561.667px; text-align: center;"><strong>Wilayah {{$sertif->nama_wilayah}}, Area Pelayanan {{$sertif->nama_area}}</strong></td>
            </tr>
            <tr>
                <td style="width: 561.667px; text-align: center;">Jl. Jatiluhur No. 71 Telp : 024 76407217 </strong></td>
            </tr>-1-1
            <tr>
                <td style="width: 561.667px; text-align: center;">Penetapan Menteri Energi dan Sumber Daya Mineral Nomor 182 K/24/DJL.4/2017 Tahun 2017</strong></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table style="width: 100%; margin-right: auto; margin-left: auto; font-size: 15px; margin-top:-10px">
        <tbody>
            <tr>
                <td style="width: 561.667px; text-align: center;"><strong>SERTIFIKAT LAIK OPERASI</strong></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table style="width: 50%; margin-left: 230px;font-size: 12px; margin-top: -8px">
        <tbody>
            <tr>
                <td style="width: 148.667px;">Nomor Sertifikat</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;"><strong>{{$sertif->no_sertifikat}}</strong></td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Nomor Registrasi</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;"><strong>{{$sertif->no_registrasi}}</strong></td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%; margin-left: 50px; font-size: 12px;">
        <tbody>
            <tr>
                <td style="width: 561.667px; text-align: left;">Dengan ini menyatakan bahwa instalasi pemanfaatan tenaga listrik tegangan rendah:</td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%;margin-left: 50px;font-size: 10px">
        <tbody>
            <tr>
                <td style="width: 148.667px;">Nama Pemilik</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->nama_pelanggan}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Alamat Pemilik</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->alamat}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Titik Koordinat</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->lat}},{{$sertif->lng}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Daya Tersambung</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->daya}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Panel Hubung Bagi Utama</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->jml_phb_utama}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Panel Hubung Bagi 3 Fasa</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->jml_phb_3fasa}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Jumlah Titik Kotak Kontak</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->kkb}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Jumlah Titik Lampu</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->jml_titik_lampu}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Jumlah Sakelar</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->jml_sakelar}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Penyedia Tenaga Listrik</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->nama_penyedia}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Penanggung Jawab Teknik</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->nama_pjt}}</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Nomor LHPP</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{$sertif->no_lhpp}}</td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%; margin-left: 50px; font-size: 10px;">
        <tbody>
            <tr>
                <td style="width: 561.667px; text-align: left;">Telah sesuai dengan ketentuan keselamatan ketenagalistrikan sehingga dinyatakan:</td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%; margin-right: auto; margin-left; auto; font-size: 11px;">
        <tbody>
            <tr>
                <td style="width: 561.667px; text-align: center;"><strong>LAIK OPERASI</strong></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table style="width: 100%; margin-left: 50px; font-size: 10px; ">
        <tbody>
            <tr>
                <td style="width: 561.667px; text-align: left;">Sertifikat ini berlaku sampai dengan tanggal <strong style="font-size: 11px">{{ Date::parse($sertif->masa_berlaku)->format('j F Y') }}</strong> sepanjang tidak ada perubahan kapasitas, perubahan instalasi, direkondisi atau direlokasi</td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%; margin-left: 100px; font-size: 10px; margin-top: 0px">
        <tbody>
            <tr>
                <td style="width: 561.667px; text-align: left;">
{!! DNS2D::getBarcodeHTML("No Sertifikat : $sertif->no_sertifikat
Nama Pelanggan : $sertif->nama_pelanggan
Nomor Seri     : $sertif->no_seri" , "QRCODE",3,3); !!}</td>
            </tr>
        </tbody>
    </table>

    <table style="width: 50%; margin-left: 380px;font-size: 10px; margin-top: -170px;">
        <tbody>
            <tr>
                <td style="width: 148.667px;">Ditetapkan di</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">Semarang</td>
            </tr>
            <tr>
                <td style="width: 148.667px;">Pada Tanggal</td>
                <td style="width: 10px;">:</td>
                <td style="width: 411px;">{{ Date::parse($sertif->tgl_cetak)->format('j F Y') }}</td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%; margin-left: 380px; font-size: 10px; ">
        <tbody>
            <tr>
                <td style="width: 561.667px; text-align: left;"><strong>General Manager/ Manager</strong> </td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%; margin-left: 380px; font-size: 10px; margin-top: 45px">
        <tbody>
            <tr>
                <td style="width: 561.667px; text-align: left;"><strong>{{$sertif->penanda_tangan}}</strong></td>
            </tr>
        </tbody>
    </table>

    @endforeach
	</body>
</html>
