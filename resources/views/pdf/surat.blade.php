
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">

<title>Surat Tugas {{$surat->no_surat}}</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

  <table style="width: 786px; margin-left: auto; margin-right: auto;">
  <tbody>
  <tr style="height: 68px;">
  <td style="width: 708px; height: 121px; text-align: center;"><img style="float: left; margin-top: -30px;" src="http://slo.sertifikasiinstalasiprima.co.id/images/kopp.png" alt="" width="708" height="121" /></td>
  </tr>
  </tbody>
  </table>
  <br>
  <table style="width: 786px; margin-left: auto; margin-right: auto; margin-top: -50px;">
  <tbody>
  <tr style="height: 55px;">
  <td style="width: auto; text-align: center;">
  <p><span style="text-decoration: underline; font-size: 14px;"><strong>SURAT TUGAS</strong></span></p>
  <p>No . {{$surat->no_surat}}</p>
  </td>
  </tr>
  <tr style="height: 55px;">
  <td style="width: 660.5px; height: 75px; text-align: center;">
  <p style="text-align: left;">Surat tugas diberikan kepada :</p>

  <ol>
  @if($surat->pemeriksa1->nama == $surat->pemeriksa2->nama)
  <li style="text-align: left;">{{$surat->pemeriksa1->nama}}</li>
  @else()
      <li style="text-align: left;">{{$surat->pemeriksa1->nama}}</li>
      <li style="text-align: left;">{{$surat->pemeriksa2->nama}}</li>
  @endif
  </ol>
  </td>
  </tr>
  <tr style="height: auto;">
  <td style="width: 660.5px; height: 75px; text-align: center;">
  <p style="text-align: left;">Sebagai petugas pemeriksa PT. Sertifikasi Instalasi Prima untuk melakukan instalasi listrik pada bangunan :</p>
  </td>
  </tr>
  </tbody>
  </table>

  <table class="table table-bordered" style="margin-top:-20px">
    <thead>
      <tr>
        <th>No Pendaftaran</th>
        <th>No Agenda</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>Tarif/Daya</th>
        <th>Instalatir</th>
        <th>Telepon</th>
      </tr>
    </thead>
    <tbody>
      @foreach($pelanggan as $temp)
        <tr>
          <td>{{$temp->no_surat}}</td>
          <td>{{$temp->pelanggan->no_agenda}}</td>
          <td>{{$temp->pelanggan->nama}}</td>
          <td>{{$temp->pelanggan->alamat}}</td>
          <td>{{$temp->tarif->jenis_tarif}}/{{$temp->daya->daya}}</td>
          <td>{{$temp->pelanggan->alamat}}</td>
          <td>{{$temp->pelanggan->no_telepon}}</td>
        </tr>

      @endforeach
    </tbody>
  </table>


  <table style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 50px">
    <tbody>
      <tr>
        <td style="width: 249px;">&nbsp;</td>
        <td style="width: 21px;">&nbsp;</td>
        <td style="width: 494px; text-align: right;">
          <p style="text-align: center;">{{ date('d F Y')}}</p>
          <p style="text-align: center;">&nbsp;</p>
          <p style="text-align: center;">{{$surat->penerbit->nama}}</p>
          <p style="text-align: center;">(Koordinator Pemeriksa)</p>

        </td>
      </tr>
    </tbody>
  </table>


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>


</body>
</html>
