
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Kwitansi</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>



  <table style="width: 786px; margin-left: auto; margin-right: auto;">
    <tbody>
      <tr style="height: 68px;">
        <td style="width: 708px; height: 121px; text-align: center;"><img style="float: left; margin-top: -30px" src="https://slo.sertifikasiinstalasiprima.co.id/images/kopp.png" alt="" width="708" height="121" /></td>
      </tr>
    </tbody>
  </table>
  <table style="width: 786px; margin-left: auto; margin-right: auto; margin-top: -50px;">
    <tbody>
      <tr style="height: 55px;">
        <td style="width: 660.5px; text-align: center;">
          <p>&nbsp;<span style="text-decoration: underline; font-size: 14px"><strong>BUKTI PEMBAYARAN</strong></span></p>
          <p style="margin-bottom: -50px">{{ $kwitansi->pembayaran->no_kwitansi }}</p>
        </td>
      </tr>
      <tr style="height: 55px;">
        <td style="width: 660.5px; height: 75px; text-align: center;">
          <p style="text-align: left;">Telah diterima pembayaran permintaan pemeriksaan instalasi dari :</p>
        </td>
      </tr>
    </tbody>
  </table>
  <table style="width: 780px; margin-left: auto; margin-right: auto; margin-top: -20px">
    <tbody>
      <tr>
        <td style="width: 249px;">No. Pendaftaran</td>
        <td style="width: 21px;">&nbsp;:</td>
        <td style="width: 494px;">{{$kwitansi->no_pendaftaran}}</td>
      </tr>
      <tr>
        <td style="width: 249px;">Atas Nama Pemilik / Konsumen</td>
        <td style="width: 21px;">&nbsp;:</td>
        <td style="width: 494px;">{{$kwitansi->nama}}</td>
      </tr>
      <tr>
        <td style="width: 249px;">Nama BTL</td>
        <td style="width: 21px;">&nbsp;:</td>
        <td style="width: 494px;">{{$kwitansi->btl->nama_btl}}</td>
      </tr>
      <tr>
        <td style="width: 249px;">Penyedia Listrik</td>
        <td style="width: 21px;">&nbsp;:</td>
        <td style="width: 494px;">{{$kwitansi->penyedia->nama_penyedia}}</td>
      </tr>
      <tr>
        <td style="width: 249px;">Alamat</td>
        <td style="width: 21px;">&nbsp;:</td>
        <td style="width: 494px;">{{$kwitansi->alamat}}</td>
      </tr>
      <tr>
        <td style="width: 249px;">Tarif/ Daya</td>
        <td style="width: 21px;">&nbsp;:</td>
        <td style="width: 494px;">{{$kwitansi->tarif->jenis_tarif}}/{{$kwitansi->daya->daya}}</td>
      </tr>
      <tr>
        <td style="width: 249px;">Biaya Pemeriksaan</td>
        <td style="width: 21px;">&nbsp;:</td>
        <td style="width: 494px;">Rp. {{$kwitansi->daya->biaya}}</td>
      </tr>
      <tr>
        <td style="width: 249px;">Terbilang</td>
        <td style="width: 21px;">&nbsp;:</td>
        <td style="width: 494px;">{{$kwitansi->daya->terbilang}}</td>
      </tr>
      <tr>
        <td style="width: 249px;">&nbsp;</td>
        <td style="width: 21px;">&nbsp;</td>
        <td style="width: 494px; text-align: right;">
          <p style="text-align: center;">{{ Date::now()->format('j F Y')}}</p>
          <p style="text-align: center;">Admin</p>
          <p style="text-align: center;">&nbsp;</p>
          <p style="text-align: center;">{{Auth::user()->nama}}</p>
        </td>
      </tr>
    </tbody>
  </table>



</body>
</html>
