<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PT. Sertifikasi Instalasi Prima | awew</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/css/app.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style media="screen">
    .body{
      background-image: url();

    }
   
  </style>
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page" style="background: #04224C !important;width:100vw;height:100vh;overflow:hidden;">
    {{-- <div class="bg">
     
    <img src="{{asset('images/bg/bg-tl.png')}}" alt="" srcset="" width="150" height="150" style="position:fixed;top:0;left:0;z-index:-1;opacity:.4" >
    <img src="{{asset('images/bg/bg-tr.png')}}" alt="" srcset="" width="150" height="150" style="position:fixed;top:0;right:0;z-index:-1;opacity:.4" >
    <img src="{{asset('images/bg/bg-bl.png')}}" alt="" srcset="" width="150" height="150" style="position:fixed;bottom:0;left:0;z-index:-1;opacity:.4" >
    <img src="{{asset('images/bg/bg-br.png')}}" alt="" srcset="" width="150" height="150" style="position:fixed;bottom:0;right:0;z-index:-1;opacity:.4" >
    
    </div> --}}
    <img src="{{asset('images/bg/bg-center.jpg')}}" alt="" srcset="" style="width:100vw;height:100vh;object-fit:cover;object-position:0;position:fixed;z-index:-1;opacity:.7" >
    <div class="login-box" style="margin-top:20px;">
        <div class="login-logo">
            <img src="/images/logoku.png" width="200px" height="200px" alt="">
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body" style="border-radius:10px;padding:50px 30px 30px 30px !important;">
            <h3 class="login-box-msg"><b>Login SIP</b></h3>

            <form action="{{ route('login') }}" method="post">
                @csrf
                <div class="form-group has-feedback">
                    <input name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Email atau Username" required autofocus>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong class="red">{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" required id="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong class="red">{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="row">
                  <div class="col-xs-12" style="margin-bottom:30px;">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" style="border-radius:50px;width:150px;">Login</button>
                  </div>
                    <div class="col-xs-12">
                        <input type="checkbox" id="checkbox"> Show password
                        <br>
                        <a href="{{route('password.request')}}" style="margin-top: 30px">Lupa Password?</a>
                    </div>
                    <!-- /.col -->
                    
                    <!-- /.col -->
                </div>
            </form>

            <!-- /.social-auth-links -->

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
<div id="app"></div>
    <!-- jQuery 3 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="/js/app.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded',()=>{
          $(document).ready(function() {
              $('#checkbox').on('change', function() {
                  $('#password').attr('type', $('#checkbox').prop('checked') == true ? "text" : "password");
              });
          });
        });
    </script>
</body>

</html>
