
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue'
import money from 'v-money'
Vue.use(money, {precision:4})

import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use( CKEditor );

import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);

import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
  load:{
    key: "AIzaSyAGrWmGKCkNuPU17QKvC5m9YCkhAv4CJpg",
    libraries: "places"
  }
});

import DateRangePicker from "@gravitano/vue-date-range-picker";
Vue.use(DateRangePicker);

window.Vue = require('vue');
import { Form, HasError, AlertError} from 'vform';
import {ServerTable, ClientTable, Event} from 'vue-tables-2';
Vue.use(ServerTable, {}, false, 'bootstrap3');
Vue.use(ClientTable, {}, false, 'bootstrap3');
Vue.use(Event);

import daterangepicker from 'daterangepicker';
//This is very important to used with the window object
const moment = require('moment')
require('moment/locale/id')

Vue.use(require('vue-moment'), {
    moment
})

Vue.filter('myDate', function(created){
	return moment(created).format('MMMM Do YYYY');
  // return moment().to(data);
});

import VueCurrencyFilter from 'vue-currency-filter'
Vue.use(VueCurrencyFilter)

Vue.use(VueCurrencyFilter,
{
  symbol : 'Rp',
  thousandsSeparator: '.',
  fractionCount: 2,
  fractionSeparator: ',',
  symbolPosition: 'front',
  symbolSpacing: true
})

// console.log(Vue.moment().locale()) //es

Vue.component('pagination', require('laravel-vue-pagination'));

import VueProgressBar from 'vue-progressbar';
Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '5px'
});

window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import swal from 'sweetalert2';
window.swal = swal;
const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
window.toast = toast;


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
import VueRouter from 'vue-router'
window.Fire = new Vue();
Vue.use(VueRouter)
let routes = [
	{ path: '/home', component: require('./components/Dashboard.vue').default},
  { path: '/admin-pusat', component: require('./components/AdminPusat.vue').default},
  { path: '/isiberita', component: require('./components/IsiBerita.vue').default},
	{ path: '/kantor-wilayah', component: require('./components/Master/KantorWilayah.vue').default},
	{ path: '/kantor-area', component: require('./components/Master/KantorArea.vue').default},

  { path: '/sub-area', component: require('./components/Master/SubArea.vue').default},
	{ path: '/jenis-bangunan', component: require('./components/Master/JenisBangunan.vue').default},

	// { path: '/jenis-bangunan', component: require('./components/Master/JenisBangunan.vue').default},

  { path: '/tarif', component: require('./components/Master/Tarif.vue').default},
  { path: '/daya', component: require('./components/Master/Daya.vue').default},
  { path: '/btl', component: require('./components/Master/Btl.vue').default},
  { path: '/asosiasi', component: require('./components/Master/Asosiasi.vue').default},
  { path: '/penyedia-listrik', component: require('./components/Master/PenyediaListrik.vue').default},
  { path: '/staff', component: require('./components/Staff.vue').default},
  { path: '/staff/btl', component: require('./components/StaffBtl.vue').default, name: 'staff-btl'},
  { path: '/pelanggan', component: require('./components/Pelanggan.vue').default, name: 'pelanggan'},
  { path: '/pelanggan/:id', component: require('./components/EditPelanggan.vue').default},
  { path: '/pelanggan-btl', component: require('./components/PelangganBtl.vue').default, name: 'pelanggan-btl'},
  { path: '/pelanggan-web/accept/:id', component: require('./components/AcceptPelanggan.vue').default, name: 'accept-pelanggan'},
  { path: '/pelanggan/no-agenda/:no_pendaftaran', component: require('./components/FormAgendaDjk.vue').default, name:'rekues-agenda'},
  { path: '/pelanggan/ubah-daya/:no_pendaftaran', component: require('./components/FormUbahDaya.vue').default, name:'ubah-daya'},
  { path: '/pelanggan/edit/:no_pendaftaran', component: require('./components/FormEditPelanggan.vue').default, name: 'edit-pelanggan'},
  { path: '/verifikasi/:no_pendaftaran', component: require('./components/FormVerifikasi.vue').default, name: 'proses-verifikasi'},
  { path: '/verifikasi/edit/:no_pendaftaran', component: require('./components/EditVerifikasi.vue').default, name: 'edit-verifikasi'},
  { path: '/pelanggan-baru', component: require('./components/PelangganBaru.vue').default},
  { path: '/pelanggan-lama', component: require('./components/PelangganLama.vue').default},
  { path: '/belum-bayar', component: require('./components/BelumBayar.vue').default},
  { path: '/sudah-bayar', component: require('./components/SudahBayar.vue').default},
  { path: '/pemeriksaan', component: require('./components/Pemeriksaan.vue').default},

  { path: '/pemeriksaan/ubah-data/:no_pendaftaran', component: require('./components/PemeriksaEdit.vue').default, name: 'edit-pemeriksaan'},
  { path: '/pemeriksaan/sudah-diperiksa', component: require('./components/SudahDiperiksa.vue').default, name: 'sudah-diperiksa'},
  { path: '/pemeriksaan/add/:no_pendaftaran', component: require('./components/FormPeriksa.vue').default, name: 'proses-pemeriksaan'},
  { path: '/form-periksa', component: require('./components/FormPeriksa.vue').default},
  { path: '/form-verifikasi', component: require('./components/FormVerifikasi.vue').default},
  { path: '/verifikasi', component: require('./components/Verifikasi.vue').default},
  { path: '/sudah-verifikasi', component: require('./components/SudahVerifikasi.vue').default, name: 'sudah-verifikasi'},
  { path: '/sertifikasi', component: require('./components/Sertifikasi.vue').default},
  { path: '/sertifikasi/add/:no_pendaftaran', component: require('./components/FormSertifikasi.vue').default, name: 'registrasi-sertif'},
  { path: '/sertifikasi/sudah-terbit', component: require('./components/SertifikasiTerbit.vue').default, name: 'sertifikasi-terbit'},
  { path: '/form-sertifikasi', component: require('./components/FormSertifikasi.vue').default},
  { path: '/management-user', component: require('./components/ManagementUser.vue').default},
  { path: '/profile', component: require('./components/Profile.vue').default},
  { path: '/lap-pelanggan', component: require('./components/Laporan/lapPelanggan.vue').default},
  { path: '/lap-pembayaran', component: require('./components/Laporan/lapPembayaran.vue').default},
  { path: '/lap-pemeriksa', component: require('./components/Laporan/lapPemeriksa.vue').default},
  { path: '/lap-verifikasi', component: require('./components/Laporan/lapVerifikasi.vue').default},
  { path: '/lap-sertifikasi', component: require('./components/Laporan/lapSertifikasi.vue').default},

  { path: '/rekap-penerimaan', component: require('./components/Rekap/RekapPenerimaan.vue').default},
  { path: '/pembayaran-tagihan', component: require('./components/Rekap/Tagihan.vue').default},
  { path: '/setoran-sub-area', component: require('./components/Rekap/SetorSubArea.vue').default},

  { path: '/pembayaran-tagihan/area', component: require('./components/Rekap/TagihanArea.vue').default},
  { path: '/setoran-area', component: require('./components/Rekap/SetorArea.vue').default},

  { path: '/share-wilayah', component: require('./components/Rekap/ShareWilayah.vue').default},
  { path: '/rekap-penerimaan/wilayah', component: require('./components/Rekap/RekapWilayah.vue').default},
  { path: '/pembayaran-tagihan/wilayah', component: require('./components/Rekap/TagihanWilayah.vue').default},

  { path: '/setoran-wilayah', component: require('./components/Rekap/SetorWilayah.vue').default},
  { path: '/rekap-penerimaan/pusat', component: require('./components/Rekap/RekapPusat.vue').default},

  { path: '/tagihan-dan-piutang/wilayah', component: require('./components/Rekap/TagihanPusat.vue').default},
  { path: '/share-internal', component: require('./components/Rekap/ShareInternal.vue').default},
  { path: '/coba', component: require('./components/coba.vue').default}

]


const router = new VueRouter({
	mode: 'history',
	routes
})
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
