export default class Gate{

	constructor(user){
		this.user = user;
	}

	isAdminPusat(){
		return this.user.jabatan == "Admin Pusat";
	}

	isTenagaTeknik(){
		return this.user.jabatan == "TT";
	}

	isPenanggungJawabTeknik(){
		return this.user.jabatan == "PJT";
	}

	isAdmin(){
		return this.user.jabatan == 'Admin';
	}

	isAdWilayah(){
		return this.user.departemen == 'Admin Wilayah';
	}

	isAdArea(){
		return this.user.departemen == 'Admin Area';
	}

	isAdSubArea(){
		return this.user.departemen == 'Admin Sub Area';
	}

	isManagerArea() {
		return this.user.departemen == 'Manajer Area'
	}

	isVerifikator(){
		return this.user.departemen == 'Verifikator';
	}

	isPemeriksa(){
		return this.user.departemen == 'Pemeriksa';
	}

	isBtl(){
		return this.user.departemen == 'BTL';
	}

	isInternal(){
		return this.user.departemen == 'Internal';
	}


}
