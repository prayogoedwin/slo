<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Password telah direset',
    'sent' => 'Terima kasih, silakan periksa Email anda!',
    'token' => 'This password reset token is invalid.',
    'user' => "Email tidak ditemukan, coba lagi.",

];
