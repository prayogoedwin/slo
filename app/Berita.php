<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Berita;

class Berita extends Model
{
	protected $table = "informasi";

	protected $fillable = ['id','berita'];

}
