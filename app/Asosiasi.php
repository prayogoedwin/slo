<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Btl;

class Asosiasi extends Model
{
    protected $table = "asosiasi";

    protected $fillable = ['id','nama_asosiasi'];

    public function btl(){
    	return $this->hasMany('App\Btl');
    }

    public function pelanggan(){
        return $this->hasMany('App\Pelanggan');
    }
}
