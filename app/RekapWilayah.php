<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekapWilayah extends Model
{
    protected $table = 'rekap_wilayah';

    protected $fillable = ['id_kwilayah', 'periode', 'total_penerimaan'];
}
