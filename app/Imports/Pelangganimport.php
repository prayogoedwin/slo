<?php

namespace App\Imports;

use App\Pelanggan;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class Pelangganimport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Pelanggan([
           'id' => $row[0],
           'no_pendaftaran' => $row[1],
           'no_agenda' => $row[2],
           'nama' => $row[3],
           'no_identitas' => $row[4],
           'no_telepon' => $row[5],
           'alamat' => => $row[6],
           'id_kwilayah' => => $row[7],
           'kode_kota'=> => $row[8],
           'id_karea'=> => $row[9],
           'id_subarea'=> => $row[10],
           'id_tarif'=> => $row[11],
           'id_daya'=> => $row[12],
           'id_btl'=> => $row[13],
           'id_bangunan'=> => $row[14],
           'tipe'=> => $row[15],
           'status'=> => $row[16],
           'no_gambar' => => $row[17],
           'tgl_gambar' => => $row[18],
           'id_asosiasi'=> => $row[19],
           'no_agendapln' => => $row[20],
           'tgl_regpln' => => $row[21],
           
        ]);
    }
}
