<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisBangunan extends Model
{
    protected $table = "jenis_bangunan";

    protected $fillable = ['id', 'jenis_bangunan'];

    public function pelanggan(){
        return $this->hasMany('App\Pelanggan');
    }
}
