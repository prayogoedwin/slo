<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Provinsi;
use App\Kota;
class Kota extends Model
{
    protected $table = "kota";

    public function provinsi(){
    	return $this->belongsTo('App\Provinsi','kode_prov','kode_prov');
    }

    public function area(){
    	return $this->hasMany('App\KantorArea','kode_kota','kode_kota');
    }

    public function pelanggan(){
        return $this->hasMany('App\Pelanggan');
    }
}
