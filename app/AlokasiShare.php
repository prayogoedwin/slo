<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlokasiShare extends Model
{
    protected $table = 'alokasi_share';

    protected $fillable = ['periode', 'pendapatan_kotor', 'alokasi_pengembangan', 'jumlah_share'];
}
