<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nip','nama', 'email', 'no_telp', 'username', 'jabatan', 'departemen', 'wewenang', 'id_kwilayah', 'id_karea', 'id_subarea', 'status', 'password', 'masa_berlaku','id_btl'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function area(){
        return $this->belongsTo('App\KantorArea','id_karea');
    }

    public function wilayah(){
        return $this->belongsTo('App\KantorWilayah','id_kwilayah');
    }

    public function subarea(){
        return $this->belongsTo('App\SubArea','id_subarea');
    }

    public function surat(){
      return $this->hasMany('App\SuratTugas', 'id_pemeriksa1');
    }

    public function verifikasi(){
      return $this->hasMany('App\Verifikasi', 'id_staff');
    }


}
