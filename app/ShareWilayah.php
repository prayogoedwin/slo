<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareWilayah extends Model
{
    protected $table = 'share_wilayah';

    protected $fillable = ['id_kwilayah', 'share'];
    
}
