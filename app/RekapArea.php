<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekapArea extends Model
{
  protected $table = 'rekap_area';

  protected $fillable = ['id_kwilayah', 'id_area', 'bulan', 'pendapatan_pelanggan', 'pendapatan_sub_area', 'total'];
}
