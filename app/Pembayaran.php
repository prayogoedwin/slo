<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = "pembayaran";

    protected $fillable = ['no_kwitansi', 'no_pendaftaran', 'diterima', 'cetak_kwitansi', 'cetak', 'dicetak_oleh', 'jumlah', 'tanggal', 'tipe'];

    public function pelanggan(){
    	return $this->belongsTo('App\Pelanggan','no_pendaftaran','no_pendaftaran');
    }
}
