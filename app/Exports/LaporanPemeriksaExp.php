<?php

namespace App\Exports;

use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LaporanPemeriksaExp implements FromCollection, withHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         return DB::table('pelanggan')
      ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
      ->join('daya','daya.id','=','pelanggan.id_daya')
      ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
      ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
      ->join('sub_area','sub_area.id','=','id_subarea')
      ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
      ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
      ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
      ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
      ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
      ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
      ->select('pembayaran.no_kwitansi','pelanggan.no_pendaftaran','pelanggan.nama','pelanggan.alamat','daya.daya',
        'kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea',
        'pemeriksa.no_lhpp','pemeriksa.tgl_lhpp','pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2',
        'pembayaran.created_at as tanggal_pembayaran','pemeriksa.created_at as tgl_pemeriksaan')
      ->where('pelanggan_temp.status_periksa', '=', '1')
      ->get();
    }
    public function headings(): array
    {
        return [
   
            'No Kwitansi',    
            'No Pendaftaran',
            'Nama Pelanggan',
            'Alamat',
            'Daya',
            'Kota',
            'Wilayah',
            'Area',
            'Sub Area',
            'Nomor LHPP',
            'Tgl LHPP',
            'Nama Pemeriksa 1',
            'Nama Pemeriksa 2',
            'Tgl Pembayaran',
            'Tgl Pemeriksaan',
        ];
    }
}
