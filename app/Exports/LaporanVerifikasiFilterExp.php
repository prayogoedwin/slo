<?php

namespace App\Exports;

use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LaporanVerifikasiFilterExp implements FromCollection, withHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         return DB::table('pelanggan')
        ->join('kota', 'kota.kode_kota', '=', 'pelanggan.kode_kota')
        ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'pelanggan.id_kwilayah')
        ->join('kantor_area', 'kantor_area.id', '=', 'pelanggan.id_karea')
        ->join('sub_area', 'sub_area.id', '=', 'pelanggan.id_subarea')
        ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
        ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
        ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
        ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->join('btl', 'btl.id', '=', 'pelanggan.id_btl')
        ->select('pelanggan.no_pendaftaran', 'pelanggan.nama', 'kota.kota', 'kantor_wilayah.nama_wilayah',
            'kantor_area.nama as nama_area', 'sub_area.nama_subarea', 'pemeriksa.no_lhpp','verifikasi.created_at','verifikasi.hasil_pemeriksaan')
        ->where([['pelanggan_temp.status_periksa', '=', '1'],
            ['pemeriksa.status_verifikasi', '=', '1']])
        ->get();
    }
    public function headings(): array
    {
        return [
   
            'No Pendaftaran',
            'Nama Pelanggan',
            'Kota',
            'Wilayah',
            'Area',
            'Sub Area',
            'Nomor LHPP',
            'Tanggal Pemeriksaan',
            'Hasil Verifikasi',
        ];
    }
}
