<?php

namespace App\Exports;

use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class LaporanPelangganFilter implements withHeadings, FromQuery
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

     public function __construct(string $tgl_awal, string $tgl_akhir)
     {
         $this->tgl_awal = $tgl_awal;
         $this->tgl_akhir = $tgl_akhir;
     }

    public function query()
    {
      return DB::table('pelanggan')
      ->join('tarif','tarif.id','=','pelanggan.id_tarif')
      ->join('daya','daya.id','=','pelanggan.id_daya')
      ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
      ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
      ->join('sub_area','sub_area.id','=','id_subarea')
      ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
      ->join('btl','btl.id','=','pelanggan.id_btl')
      ->join('penyedia_listrik','penyedia_listrik.id','=','pelanggan.id_penyedia')
      ->select('pelanggan.no_pendaftaran','pelanggan.created_at','pelanggan.tipe','tarif.jenis_tarif','daya.daya','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pelanggan.nama','pelanggan.alamat','kota.kota','btl.nama_btl','penyedia_listrik.nama_penyedia','pelanggan.status as status_pembayaran','pelanggan.diinput_oleh')
      // ->whereBetween('pelanggan.created_at',[$this->tgl_awal, $this->tgl_akhir])
      ->get();
    }

    public function headings(): array
    {
        return [

            'No Pendaftaran',
            'Tgl Pendaftaran',
            'Tipe',
            'Jenis Tarif',
            'Daya',
            'Wilayah',
            'Area',
            'Sub Area',
            'Nama Pelanggan',
            'Alamat',
            'Kota',
            'BTL',
            'Penyedia Listrik',
            'Status Pembayaran',
            'Diinput Oleh',
        ];
    }
}
