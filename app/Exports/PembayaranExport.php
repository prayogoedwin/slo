<?php

namespace App\Exports;

use App\Pelanggan;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PembayaranExport implements FromCollection, withHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $staff = Auth::user();
        if ($staff->departemen == 'Admin Wilayah') {
            
            return Pelanggan::with('wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->select('id','no_pendaftaran','no_agenda','nama','alamat','id_kwilayah','kode_kota','id_karea','id_subarea','id_btl','id_bangunan','id_tarif','id_daya','id_asosiasi','created_at','tipe')->where([
                ['id_kwilayah', '=', $staff->id_kwilayah],
                ['status', '=', '0']])                
                ->get();
        }
        if ($staff->departemen == 'Admin Area') {
            
            return Pelanggan::with('wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->select('id','no_pendaftaran','no_agenda','nama','alamat','id_kwilayah','kode_kota','id_karea','id_subarea','id_btl','id_bangunan','id_tarif','id_daya','id_asosiasi','created_at','tipe')->where([
                ['id_karea', '=', $staff->id_karea],
                ['status', '=', '0']])
                ->get();
            }
        if ($staff->departemen == 'Admin Sub Area') {
            return Pelanggan::with('wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->select('id','no_pendaftaran','no_agenda','nama','alamat','id_kwilayah','kode_kota','id_karea','id_subarea','id_btl','id_bangunan','id_tarif','id_daya','id_asosiasi','created_at','tipe')->where([
                ['id_subarea', '=', $staff->id_subarea],
                ['status', '=', '0']])
                ->get();
        }
    }
    public function headings(): array
    {
        return [
            'id',
            'no_pendaftaran',  
            'no_agendapln',
            'nama',
            'alamat',
            'kota',
            'nama_wilayah',
            'area',
            'subarea',
            'nama_btl',
            'jenis_bangunan',
            'jenis_tarif',
            'daya',
            'nama_asosiasi',
            'tgldaftar',
            'tipe',
        ];
    }
}
