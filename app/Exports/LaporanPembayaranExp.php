<?php

namespace App\Exports;

use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LaporanPembayaranExp implements FromCollection, withHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         return DB::table('pelanggan')
         ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
         ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
         ->join('sub_area','sub_area.id','=','id_subarea')
         ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
         ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
         ->select('pembayaran.no_kwitansi','pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at as tgl_pembayaran','pembayaran.diterima as diterima_oleh','pembayaran.cetak','pembayaran.dicetak_oleh')
         ->orderBy('pembayaran.id','desc')
         ->where('pelanggan.status','=','Sudah Bayar')
         ->get();
    }
    public function headings(): array
    {
        return [
            'Nomor Kwitansi',  
            'Nomor Pendaftaran',
            'nama',
            'kota',
            'Nama Wilayah',
            'nama Area',
            'nama Subarea',
            'Tanggal Pembayaran',
            'Diterima Oleh',
            'Jumlah Cetak',
            'Dicetak Oleh',
        ];
    }
}
