<?php

namespace App\Exports;

use App\Pelanggan;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;

class SudahbayarExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $staff = Auth::user();
        if ($staff->departemen == 'Admin Wilayah') {
            
            return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['id_kwilayah', '=', $staff->id_kwilayah],
                ['status', '=', '1']])                
                ->get();
        }
        if ($staff->departemen == 'Admin Area') {
            
            return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['id_karea', '=', $staff->id_karea],
                ['status', '=', '1']])
                ->get();
            }
        if ($staff->departemen == 'Admin Sub Area') {
            return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['id_subarea', '=', $staff->id_subarea],
                ['status', '=', '1']])
                ->get();
        }
    }
    
}
