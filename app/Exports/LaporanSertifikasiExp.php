<?php

namespace App\Exports;

use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LaporanSertifikasiExp implements FromCollection, withHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         return DB::table('sertifikasi')
                 ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'sertifikasi.no_pendaftaran')
                 ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
                 ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
                 ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
                 ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
                 ->join('btl','btl.id','=','pelanggan.id_btl')
                 ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
                 ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
                 ->select('sertifikasi.no_pendaftaran', 'pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl',
                 'sertifikasi.no_seri','sertifikasi.created_at as tgl_cetak','sertifikasi.dicetak_oleh')
                 ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
                 ['verifikasi.status_sertifikasi', '=', '1']])
                  ->get();
       
    }
    public function headings(): array
    {
        return [
   
            'No Pendaftaran',
            'Nama Pelanggan',
            'Kota',
            'Wilayah',
            'Area',
            'Sub Area',
            'Nomor LHPP',
            'BTL',
            'No Seri',
            'Tgl Cetak',
            'Dicetak Oleh',
        ];
    }
}
