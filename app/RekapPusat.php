<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekapPusat extends Model
{
    protected $table = 'rekap_pusat';

    protected $fillable = ['periode', 'total_penerimaan'];
}
