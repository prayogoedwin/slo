<?php

namespace App\Http\Controllers\EXL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\PembayaranExport;
use App\Exports\SudahBayarExport;
use App\Exports\LaporanVerifikasiExp;
use App\Exports\LaporanVerifikasiFilterExp;
use App\Exports\LaporanPembayaranExp;
use App\Exports\LaporanSertifikasiExp;
use App\Exports\LaporanPemeriksaExp;
use App\Exports\LaporanPelangganExp;
use App\Exports\LaporanPelangganFilter;
use Excel;
use Auth;

class ExcelController extends Controller
{
    public function exportBlmByr(){
    	return Excel::download(new PembayaranExport, 'belum_bayar.xlsx');
    }
    public function exportSdhByr(){
    	return Excel::download(new SudahBayarExport, 'sudah_bayar.xlsx');
    }
    public function import()
    {
        Excel::import(new Pelangganimport, 'pelanggan.xlsx');

        return redirect('/pelanggan');
    }

    public function lapPembayaran()
    {
        return Excel::download(new LaporanPembayaranExp, 'laporan_pembayaran.xlsx');
    }
    public function lapPembayaranCSV()
    {
        return Excel::download(new LaporanPembayaranExp, 'laporan_pembayaran.csv');
    }

    public function lapVerifikasi()
    {
        return Excel::download(new LaporanVerifikasiExp, 'laporan_verifikasi.xlsx');
    }
    public function lapVerifikasiCSV()
    {
        return Excel::download(new LaporanVerifikasiExp, 'laporan_verifikasi.csv');
    }

    public function lapVerifikasiFilter()
    {
        return Excel::download(new LaporanVerifikasiExp, 'filter_laporan_verifikasi.xlsx');
    }
    public function lapVerifikasiFilterCSV()
    {
        return Excel::download(new LaporanVerifikasiExp, 'filter_laporan_verifikasi.csv');
    }

    public function lapSertifikasi()
    {
        return Excel::download(new LaporanSertifikasiExp, 'laporan_sertifikasi.xlsx');
    }
    public function lapSertifikasiCSV()
    {
        return Excel::download(new LaporanSertifikasiExp, 'laporan_sertifikasi.csv');
    }

    public function lapPemeriksa()
    {
        return Excel::download(new LaporanPemeriksaExp, 'laporan_pemeriksa.xlsx');
    }
    public function lapPemeriksaCSV()
    {
        return Excel::download(new LaporanPemeriksaExp, 'laporan_pemeriksa.csv');
    }

    public function lapPelanggan()
    {
        return Excel::download(new LaporanPelangganExp, 'laporan_pelanggan.xlsx');
    }

    public function lapPelangganCSV()
    {
        return Excel::download(new LaporanPelangganExp, 'laporan_pelanggan.csv');
    }

    public function lapPelangganFilter($tgl1,$tgl2)
    {
        $tgl_awal = $tgl1;
        $tgl_akhir = $tgl2;

        return (new LaporanPelangganFilter($tgl_awal,$tgl_akhir))->download('invoices.xlsx');
    }


}
