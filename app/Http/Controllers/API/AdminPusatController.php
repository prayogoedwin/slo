<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class AdminPusatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::where('jabatan', '=', 'Admin Pusat')->orderBy('id','asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate( $request,[
            'nip' => 'sometimes',
            'nama' => 'required|min:2',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|min:4|unique:users',
            'password' => 'required|string|min:6|confirmed',
      ]);

      return User::create([
          'nip' => $request['nip'],
          'nama' => $request['nama'],
          'email' => $request['email'],
          'username' => $request['username'],
          'jabatan' => 'Admin Pusat',
          'departemen' => 'Admin Pusat',
          'wewenang' => 'Admin Pusat',
          'id_kwilayah' => '10',
          'id_karea' => '59',
          'id_subarea' => '0',
          'status' => 'Aktif',
          'password' => bcrypt($request['password']),
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = User::findOrFail($id);

        $this->validate( $request,[
              'nip' => 'sometimes',
              'nama' => 'required|min:2',
              'email' => 'required|string|email|max:255|unique:users,email,'.$admin->id,
              'username' => 'required|string|min:4|unique:users,username,'.$admin->id,
        ]);

        $admin->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $admin = User::findOrFail($id);

      $admin->delete();
    }
}
