<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use App\StaffBtl;

class StaffBtlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return User::with('wilayah','area','subarea')->orderBy('id','asc')->where('jabatan', '=', 'BTL')->get();
        return DB::table('users')
                   ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'users.id_kwilayah')
                   ->join('kantor_area', 'kantor_area.id', '=', 'users.id_karea')
                   ->join('sub_area', 'sub_area.id', '=', 'users.id_subarea')
                   ->join('staff_btl', 'staff_btl.id_users', '=', 'users.id')
                   ->join('btl', 'btl.id', '=', 'staff_btl.id_btl')
                   ->select('users.id','users.nip', 'users.id_kwilayah', 'users.id_karea', 'users.id_subarea', 'users.email', 'users.no_telp', 'users.nama', 'users.username', 'kantor_wilayah.nama_wilayah',
                   'kantor_area.nama as nama_area', 'sub_area.nama_subarea', 'btl.nama_btl', 'users.status')
                   ->where('users.jabatan', '=', 'BTL')
                   ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate( $request,[
            'nip' => 'sometimes',
            'nama' => 'required|min:2',
            'email' => 'required|string|email|max:255|unique:users',
            'no_telp' => 'required|string|max:14|unique:users',
            'username' => 'required|string|min:4|unique:users',
            'id_kwilayah' => 'required',
            'id_karea' => 'sometimes',
            'id_subarea' => 'sometimes',
            'status' => 'required',
            'id_btl' => 'required',
            'password' => 'required|string|min:6|confirmed',
      ]);

      $user = new User();
      $user->nip = $request['nip'];
      $user->nama = $request['nama'];
      $user->email = $request['email'];
      $user->no_telp = $request['no_telp'];
      $user->username = $request['username'];
      $user->jabatan = 'BTL';
      $user->departemen = 'BTL';
      $user->wewenang = 'BTL';
      $user->id_kwilayah = $request['id_kwilayah'];
      $user->id_karea = $request['id_karea'];
      $user->id_subarea = $request['id_subarea'];
      $user->status = $request['status'];
      $user->password = bcrypt($request['password']);
      $user->masa_berlaku = $request['masa_berlaku'];

      $user->save();

      if ($user->save()) {
        StaffBtl::create([
          'id_users' => $user->id,
          'id_btl' => $request['id_btl'],
        ]);
      }

      // $user = User::create([
      //     'nip' => $request['nip'],
      //     'nama' => $request['nama'],
      //     'email' => $request['email'],
      //     'no_telp' => $request['no_telp'],
      //     'username' => $request['username'],
      //     'jabatan' => 'BTL',
      //     'departemen' => 'BTL',
      //     'wewenang' => 'BTL',
      //     'id_kwilayah' => $request['id_kwilayah'],
      //     'id_karea' => $request['id_karea'],
      //     'id_subarea' => $request['id_subarea'],
      //     'status' => $request['status'],
      //     'password' => bcrypt($request['password']),
      //     'masa_berlaku' => $request['masa_berlaku'],
      // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', '=', $id)->first();

        $this->validate( $request,[
              'nip' => 'sometimes',
              'nama' => 'required|min:2',
              'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
              'no_telp' => 'required|string|max:14|unique:users,email,'.$user->id,
              'username' => 'required|string|min:4|unique:users,username,'.$user->id,
              'id_kwilayah' => 'required',
              'id_karea' => 'sometimes',
              'id_subarea' => 'sometimes',
              'status' => 'required',
              'id_btl' => 'required',
        ]);

        $user->nip = $request['nip'];
        $user->nama = $request['nama'];
        $user->email = $request['email'];
        $user->no_telp = $request['no_telp'];
        $user->username = $request['username'];
        $user->jabatan = 'BTL';
        $user->departemen = 'BTL';
        $user->wewenang = 'BTL';
        $user->id_kwilayah = $request['id_kwilayah'];
        $user->id_karea = $request['id_karea'];
        $user->id_subarea = $request['id_subarea'];
        $user->status = $request['status'];

        $user->save();

        if ($user->save()) {
          // $staff = StaffBtl::where('id_users', '=', $user->id)->first();
          StaffBtl::where('id_users', '=', $user->id)->update([
            'id_users' => $user->id,
            'id_btl' => $request['id_btl'],
          ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
