<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pelanggan;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\CalonWeb;
use App\LogStatus;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = auth('api')->user();
        if (auth('api')->user()->jabatan == 'Admin Pusat' || auth('api')->user()->departemen == 'Manajer Area') {
            return Pelanggan::with('wilayah','penyedia', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->whereNotIn('tipe', ['BTL'])->orderBy('id','asc')->get();

        }
        if (auth('api')->user()->departemen == 'Admin Wilayah') {
            return Pelanggan::with('wilayah','penyedia', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where('id_kwilayah', '=', $staff->id_kwilayah)
            ->whereNotIn('tipe', ['BTL'])->orderBy('id','asc')->get();

        }
        if (auth('api')->user()->departemen == 'Admin Area') {
            return Pelanggan::with('wilayah','penyedia', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where('id_karea', '=', $staff->id_karea )
            ->whereNotIn('tipe', ['BTL'])->orderBy('id','asc')->get();

        }
        if (auth('api')->user()->departemen == 'Admin Sub Area') {
            return Pelanggan::with('wilayah','penyedia', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where('id_subarea', '=', $staff->id_subarea )
            ->whereNotIn('tipe', ['BTL'])->orderBy('id','asc')->get();
        }
    }

    public function pelangganBtl(){
      $staff = auth('api')->user();
      if (auth('api')->user()->jabatan == 'Admin Pusat') {
          return Pelanggan::with('wilayah','penyedia', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where('tipe', 'BTL')->orderBy('id','asc')->get();

      }
      if (auth('api')->user()->departemen == 'Admin Wilayah') {
          return Pelanggan::with('wilayah','penyedia', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([['id_kwilayah', '=', $staff->id_kwilayah],['tipe', '=', 'BTL']])->orderBy('id','asc')->get();

      }
      if (auth('api')->user()->departemen == 'Admin Area') {
          return Pelanggan::with('wilayah','penyedia', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([['id_karea', '=', $staff->id_karea],['tipe', '=', 'BTL']])->orderBy('id','asc')->get();

      }
      if (auth('api')->user()->departemen == 'Admin Sub Area') {
          return Pelanggan::with('wilayah','penyedia', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([['id_subarea', '=', $staff->id_subarea],['tipe', '=', 'BTL']])->orderBy('id','asc')->get();
      }
      if ($staff->jabatan == 'BTL') {
        $btl = DB::table('users')
                     ->join('staff_btl', 'staff_btl.id_users', '=', 'users.id')
                     ->join('btl', 'btl.id', '=', 'staff_btl.id')
                     ->select('staff_btl.id_btl')
                     ->where('staff_btl.id_users', '=', $staff->id)
                     ->first();
        // return response()->json($btl);
        return Pelanggan::with('wilayah','penyedia', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([['id_btl', '=', $btl->id_btl],['tipe', '=', 'BTL']])->orderBy('id','asc')->get();

      }
    }

    public function lapPelanggan(){
      $staff = auth('api')->user();
      if (auth('api')->user()->jabatan == 'Admin Pusat') {
        return DB::table('pelanggan')
        ->join('tarif','tarif.id','=','pelanggan.id_tarif')
        ->join('daya','daya.id','=','pelanggan.id_daya')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('btl','btl.id','=','pelanggan.id_btl')
        ->join('penyedia_listrik','penyedia_listrik.id','=','pelanggan.id_penyedia')
        ->select('pelanggan.no_pendaftaran','pelanggan.created_at','pelanggan.tipe','tarif.jenis_tarif','daya.daya','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pelanggan.nama','pelanggan.alamat','kota.kota','btl.nama_btl','penyedia_listrik.nama_penyedia','pelanggan.status as status_pembayaran','pelanggan.diinput_oleh')
        ->get();
      }
      if (auth('api')->user()->departemen == 'Admin Wilayah') {
        return DB::table('pelanggan')
        ->join('tarif','tarif.id','=','pelanggan.id_tarif')
        ->join('daya','daya.id','=','pelanggan.id_daya')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('btl','btl.id','=','pelanggan.id_btl')
        ->join('penyedia_listrik','penyedia_listrik.id','=','pelanggan.id_penyedia')
        ->select('pelanggan.no_pendaftaran','pelanggan.created_at','pelanggan.tipe','tarif.jenis_tarif','daya.daya','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pelanggan.nama','pelanggan.alamat','kota.kota','btl.nama_btl','penyedia_listrik.nama_penyedia','pelanggan.status as status_pembayaran','pelanggan.diinput_oleh')
        ->where('pelanggan.id_kwilayah', '=', $staff->id_kwilayah)
        ->get();
      }
      if (auth('api')->user()->departemen == 'Admin Area') {
        return DB::table('pelanggan')
        ->join('tarif','tarif.id','=','pelanggan.id_tarif')
        ->join('daya','daya.id','=','pelanggan.id_daya')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('btl','btl.id','=','pelanggan.id_btl')
        ->join('penyedia_listrik','penyedia_listrik.id','=','pelanggan.id_penyedia')
        ->select('pelanggan.no_pendaftaran','pelanggan.created_at','pelanggan.tipe','tarif.jenis_tarif','daya.daya','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pelanggan.nama','pelanggan.alamat','kota.kota','btl.nama_btl','penyedia_listrik.nama_penyedia','pelanggan.status as status_pembayaran','pelanggan.diinput_oleh')
        ->where('pelanggan.id_karea', '=', $staff->id_karea)
        ->get();
      }
      if (auth('api')->user()->departemen == 'Admin Sub Area') {
        return DB::table('pelanggan')
        ->join('tarif','tarif.id','=','pelanggan.id_tarif')
        ->join('daya','daya.id','=','pelanggan.id_daya')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('btl','btl.id','=','pelanggan.id_btl')
        ->join('penyedia_listrik','penyedia_listrik.id','=','pelanggan.id_penyedia')
        ->select('pelanggan.no_pendaftaran','pelanggan.created_at','pelanggan.tipe','tarif.jenis_tarif','daya.daya','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pelanggan.nama','pelanggan.alamat','kota.kota','btl.nama_btl','penyedia_listrik.nama_penyedia','pelanggan.status as status_pembayaran','pelanggan.diinput_oleh')
        ->where('pelanggan.id_subarea', '=', $staff->id_subarea)
        ->get();
      }


    }

    public function filterPelanggan($tgl1,$tgl2){
       return DB::table('pelanggan')
      ->join('tarif','tarif.id','=','pelanggan.id_tarif')
      ->join('daya','daya.id','=','pelanggan.id_daya')
      ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
      ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
      ->join('sub_area','sub_area.id','=','id_subarea')
      ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
      ->join('btl','btl.id','=','pelanggan.id_btl')
      ->join('penyedia_listrik','penyedia_listrik.id','=','pelanggan.id_penyedia')
      ->select('pelanggan.no_pendaftaran','pelanggan.created_at','pelanggan.tipe','tarif.jenis_tarif','daya.daya','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pelanggan.nama','pelanggan.alamat','kota.kota','btl.nama_btl','penyedia_listrik.nama_penyedia','pelanggan.status as status_pembayaran','pelanggan.diinput_oleh')
      ->whereBetween('pelanggan.created_at',[$tgl1, $tgl2])
      ->paginate(10);

    }

    public function calonWeb(){
      return DB::table('calon_pelanggan_web')->orderBy('id','asc')->get();
    }

    public function reqNoAgenda($no_pendaftaran){
      return DB::table('pelanggan')
                  ->join('penyedia_listrik', 'penyedia_listrik.id', '=', 'pelanggan.id_penyedia')
                  ->join('tarif', 'tarif.id', '=', 'pelanggan.id_tarif')
                  ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
                  ->select('pelanggan.no_pendaftaran','pelanggan.no_agenda','pelanggan.nama','pelanggan.alamat','penyedia_listrik.nama_penyedia',
                  'pelanggan.no_telepon','tarif.jenis_tarif','daya.daya')
                  ->where([['pelanggan.no_pendaftaran', '=', $no_pendaftaran],['pelanggan.no_agenda', '=', null]])
                  ->get();
    }

    public function ubahDaya($no_pendaftaran){
      // dd($no_pendaftaran);
      return Pelanggan::where([['no_pendaftaran', '=', $no_pendaftaran]])->get();
      return Pelanggan::where([['no_pendaftaran', '=', $no_pendaftaran],['status', '=', 'Belum Bayar']])->get();
    }

    public function updateDaya(Request $request, $no_pendaftaran){
      $this->validate($request,[
            'id_tarif' => 'required',
            'id_daya' => 'required',
      ]);

      $pelanggan = Pelanggan::where('no_pendaftaran', '=', $no_pendaftaran)->update([
        'id_daya' => $request->input('id_daya'),
        'id_tarif' => $request->input('id_tarif'),
      ]);

      return response()->json([
        'msg' => 'Daya berhasil diubah'
      ]);
    }

    public function detailWeb($id){
      return CalonWeb::find($id);

    }

    public function deleteWeb($id){
      $pelanggan = CalonWeb::findOrFail($id);

      $pelanggan->delete();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $staff = auth('api')->user();

        if ($staff->jabatan == 'BTL') {
          $tipe = 'BTL';
        } else {
          $tipe = $request['tipe'];
        }


        $this->validate($request, [
              'nama' => 'required|min:2|max:191|string',
              'alamat' => 'required|min:2|max:191|string',
              'no_telepon' => 'required|numeric',
              'no_identitas' => 'required|min:16|numeric|unique:pelanggan',
              'id_kwilayah' => 'required',
              'id_karea'=> 'required',
              'kode_kota'=> 'required',
              'id_subarea'=> 'required',
              'id_tarif'=> 'required',
              'id_daya'=> 'required',
              'id_bangunan'=> 'required',
              'id_penyedia'=> 'required',
              'id_asosiasi'=> 'required',
              'id_btl'=> 'required',
              'tgl_gambar' => 'sometimes',
              'tgl_regpln' => 'sometimes',
              'no_agendapln' => 'sometimes',
              'no_gambar' => 'sometimes',
        ]);

        $huruf = "SIP";
        $xkode =  'SIP-'.date('my').'-'.sprintf("%03s", $request['id_kwilayah']) . '-' . sprintf("%03s", $request['id_karea']) . '-';
        $noTerakhir =Pelanggan::where('no_pendaftaran' , 'like', $xkode.'%')->max('no_pendaftaran');
        
        $nomor = (int) substr($noTerakhir, 17, 21);
        $nomor++;
        $no = 1;

        $hrf = "BR";
        $noTer = Pelanggan::max('no_agenda');
        $no_agenda = (int) substr($noTer, 2, 6);
        $no_agenda++;

       

        if($noTerakhir){
            $noPendaftaran = $huruf . '-' . date('my') . '-' . sprintf("%03s", $request['id_kwilayah']) . '-' . sprintf("%03s", $request['id_karea']) . '-' . sprintf("%04s", abs($nomor));
        }
        else{
            $noPendaftaran = $huruf . '-' . date('my') . '-' . sprintf("%03s", $request['id_kwilayah']) . '-' . sprintf("%03s", $request['id_karea']) . '-' . sprintf("%04s", abs($no));
        }

        if ($noTer) {
          $noAgenda = $hrf . sprintf("%03s", abs($no_agenda));
        } else {
          $noAgenda = $hrf . sprintf("%03s", abs($no));
        }
        
        Pelanggan::create([
            'no_pendaftaran' => $noPendaftaran,
            'no_agenda' => $noAgenda,
            'nama' => $request['nama'],
            'alamat' => $request['alamat'],
            'no_telepon' => $request['no_telepon'],
            'no_identitas' => $request['no_identitas'],
            'id_kwilayah' => $request['id_kwilayah'],
            'id_karea'=> $request['id_karea'],
            'kode_kota'=> $request['kode_kota'],
            'id_subarea'=> $request['id_subarea'],
            'id_tarif'=> $request['id_tarif'],
            'id_daya'=> $request['id_daya'],
            'id_bangunan'=> $request['id_bangunan'],
            'id_penyedia'=> $request['id_penyedia'],
            'id_asosiasi'=> $request['id_asosiasi'],
            'id_btl'=> $request['id_btl'],
            'tipe' => $tipe,
            'status' => 'Belum Bayar',
            'tgl_gambar' => Carbon::parse($request['tgl_gambar']),
            'tgl_regpln' => Carbon::parse($request['tgl_regpln']),
            'no_agendapln' => $request['no_agendapln'],
            'no_gambar' => $request['no_gambar'],
            'diinput_oleh' => $staff->nama,
        ]);

        LogStatus::create([
          'no_pendaftaran' => $noPendaftaran,
          'status' => 'Menunggu Pembayaran',
          'kode_status' => '1',
        ]);

        return response()->json([
          'msg' => 'Data berhasil disimpan'
        ]);


    }


    public function detailPelanggan($no_pendaftaran){
       // return Pelanggan::with('bangunan','asosiasi')->where('no_pendaftaran', '=', $no_pendaftaran)->get();
       $staff = auth('api')->user();
       if ($staff->departemen == 'Admin Pusat') {
         $pelanggan = DB::table('pelanggan')
                          ->join('jenis_bangunan', 'jenis_bangunan.id', '=', 'pelanggan.id_bangunan')
                          ->join('asosiasi', 'asosiasi.id', '=', 'pelanggan.id_asosiasi')
                          ->join('log_status', 'log_status.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                          ->select('pelanggan.nama', 'pelanggan.alamat', 'pelanggan.no_identitas', 'pelanggan.no_telepon', 'jenis_bangunan.id as id_bangunan', 'jenis_bangunan.jenis_bangunan',
                          'pelanggan.no_gambar', 'pelanggan.tgl_gambar', 'asosiasi.id as id_asosiasi', 'asosiasi.nama_asosiasi', 'pelanggan.no_agendapln', 'pelanggan.tgl_regpln',
                          'log_status.kode_status')
                          ->where([['pelanggan.no_pendaftaran', '=', $no_pendaftaran]])
                          ->whereNotIn('log_status.kode_status', [4])
                          ->get();

         return response()->json($pelanggan);
       }
       if ($staff->departemen == 'Admin Wilayah') {
         $pelanggan = DB::table('pelanggan')
                          ->join('jenis_bangunan', 'jenis_bangunan.id', '=', 'pelanggan.id_bangunan')
                          ->join('asosiasi', 'asosiasi.id', '=', 'pelanggan.id_asosiasi')
                          ->join('log_status', 'log_status.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                          ->select('pelanggan.nama', 'pelanggan.alamat', 'pelanggan.no_identitas', 'pelanggan.no_telepon', 'jenis_bangunan.id as id_bangunan', 'jenis_bangunan.jenis_bangunan',
                          'pelanggan.no_gambar', 'pelanggan.tgl_gambar', 'asosiasi.id as id_asosiasi', 'asosiasi.nama_asosiasi', 'pelanggan.no_agendapln', 'pelanggan.tgl_regpln',
                          'log_status.kode_status')
                          ->where([['pelanggan.no_pendaftaran', '=', $no_pendaftaran]])
                          ->whereNotIn('log_status.kode_status', [2,3,4])
                          ->get();

         return response()->json($pelanggan);
       }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Pelanggan::findOrFail($id);
    }


    public function updatePelanggan(Request $request, $no_pendaftaran){
      $pelanggan = Pelanggan::where('no_pendaftaran', '=', $no_pendaftaran)->first();

      $this->validate($request,[
            'nama' => 'required|min:2',
            // 'email' => 'required|string|email|max:255|unique:users,email,'.$pelanggan->id,
            'alamat' => 'required',
            'no_telepon' => 'required|unique:pelanggan,no_telepon,'.$pelanggan->id,
            'no_identitas' => 'required',
            'id_bangunan' => 'required',
            'id_asosiasi' => 'required',
            'tgl_gambar' => 'required',
            'tgl_regpln' => 'required',
            'no_agendapln' => 'sometimes',
            'no_gambar' => 'sometimes',
      ]);

      $pelanggan->update([
        'nama' => $request['nama'],
        'alamat' => $request['alamat'],
        'no_telepon' => $request['no_telepon'],
        'no_identitas' => $request['no_identitas'],
        'id_bangunan' => $request['id_bangunan'],
        'id_asosiasi' => $request['id_asosiasi'],
        'tgl_gambar' => $request['tgl_gambar'],
        'tgl_regpln' => $request['tgl_regpln'],
        'no_agendapln' => $request['no_agendapln'],
        'no_gambar' => $request['no_gambar'],
      ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pelanggan = Pelanggan::findOrFail($id);

        $this->validate( $request,[
              'nama' => 'required|min:2',
              // 'email' => 'required|string|email|max:255|unique:users,email,'.$pelanggan->id,
              'no_telepon' => 'required',
              'no_identitas' => 'required',
              'id_kwilayah' => 'required',
              'id_karea' => 'required',
              'kode_kota' => 'required',
              'id_subarea' => 'required',
              'no_telepon' => 'required',
              'id_tarif' => 'required',
              'id_daya' => 'required',
              'id_bangunan' => 'required',
              'id_penyedia' => 'required',
              'id_asosiasi' => 'required',
              'id_btl' => 'required',
              'tgl_gambar' => 'required',
              'tgl_regpln' => 'required',
              'no_agendapln' => 'required',
              'no_gambar' => 'required',
        ]);

        $pelanggan->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($no_pendaftaran)
    {

        $staff = auth('api')->user();
      if ($staff->departemen == 'Admin Pusat') {
        $pelanggan = DB::table('pelanggan')
                         ->join('jenis_bangunan', 'jenis_bangunan.id', '=', 'pelanggan.id_bangunan')
                         ->join('asosiasi', 'asosiasi.id', '=', 'pelanggan.id_asosiasi')
                         ->join('log_status', 'log_status.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                         ->select('pelanggan.nama', 'pelanggan.alamat', 'pelanggan.no_identitas', 'pelanggan.no_telepon', 'jenis_bangunan.id as id_bangunan', 'jenis_bangunan.jenis_bangunan',
                         'pelanggan.no_gambar', 'pelanggan.tgl_gambar', 'asosiasi.id as id_asosiasi', 'asosiasi.nama_asosiasi', 'pelanggan.no_agendapln', 'pelanggan.tgl_regpln',
                         'log_status.kode_status')
                         ->where([['pelanggan.no_pendaftaran', '=', $no_pendaftaran]])
                         ->whereNotIn('log_status.kode_status', [4])
                         ->delete();

        // $pelanggan->delete();
        if (!$pelanggan) {
          return response()->json([
            'msg' => 'SLO sudah terbit, data tidak dapat dihapus'
          ],402);
        } else {
          return response()->json([
            'msg' => 'Data berhasil dihapus'
          ]);
        }
      }

      if ($staff->departemen == 'Admin Wilayah') {
        $pelanggan = DB::table('pelanggan')
                         ->join('jenis_bangunan', 'jenis_bangunan.id', '=', 'pelanggan.id_bangunan')
                         ->join('asosiasi', 'asosiasi.id', '=', 'pelanggan.id_asosiasi')
                         ->join('log_status', 'log_status.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                         ->select('pelanggan.nama', 'pelanggan.alamat', 'pelanggan.no_identitas', 'pelanggan.no_telepon', 'jenis_bangunan.id as id_bangunan', 'jenis_bangunan.jenis_bangunan',
                         'pelanggan.no_gambar', 'pelanggan.tgl_gambar', 'asosiasi.id as id_asosiasi', 'asosiasi.nama_asosiasi', 'pelanggan.no_agendapln', 'pelanggan.tgl_regpln',
                         'log_status.kode_status')
                         ->where([['pelanggan.no_pendaftaran', '=', $no_pendaftaran]])
                         ->whereNotIn('log_status.kode_status', [2,3,4])
                         ->delete();

                         if (!$pelanggan) {
                           return response()->json([
                             'msg' => 'Data telah diperiksa, data tidak dapat dihapus'
                           ],402);
                         } else {
                           return response()->json([
                             'msg' => 'Data berhasil dihapus'
                           ]);
                         }
      }
    }
}
