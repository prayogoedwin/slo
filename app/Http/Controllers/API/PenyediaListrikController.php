<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PenyediaListrik;

class PenyediaListrikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PenyediaListrik::with('wilayah','area')->orderBy('id','asc')->get();
    }
    public function loadPenyedia($id){
        $peny = PenyediaListrik::where('id_karea', '=', $id)->orderBy('nama_penyedia')->get();

        return response()->json($peny);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_penyedia' => 'required',
            'kode_pln' => 'required',
            'id_kwilayah' => 'required',
            'id_karea' => 'required',
            'alamat' => 'required',
            'status' => 'required',
        ]);
          $id = intval(PenyediaListrik::max('id'));
          $id++;
         
        return PenyediaListrik::create([
            'id'=>$id ,
            'nama_penyedia' => $request['nama_penyedia'],
            'kode_pln' => $request['kode_pln'],
            'id_kwilayah' => $request['id_kwilayah'],
            'id_karea' => $request['id_karea'],
            'alamat' => $request['alamat'],
            'status' => $request['status'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $plistrik = PenyediaListrik::findOrFail($id);

        $this->validate($request,[
            'nama_penyedia' => 'required',
            'kode_pln' => 'required',
            'id_kwilayah' => 'required',
            'id_karea' => 'required',
            'alamat' => 'required',
            'status' => 'required',
        ]);

        $plistrik->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plistrik = PenyediaListrik::findOrFail($id);

        $plistrik->delete();
    }
}
