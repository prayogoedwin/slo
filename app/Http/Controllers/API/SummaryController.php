<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;

use App\Pelanggan;
use App\Pembayaran;
use App\Verifikasi;
use App\Sertifikasi;
use App\SuratTugas;
use App\User;
use App\KantorWilayah;
use Carbon\Carbon;
use App\Berita;
class SummaryController extends Controller
{

    public function berita(){
      return Berita::first();
    }

    public function cekKantor(){
      $staff = auth('api')->user();
      if ($staff->departemen == "Admin Wilayah") {
        return DB::table('kantor_wilayah')
                   ->join('users', 'users.id_kwilayah', '=', 'kantor_wilayah.id')
                   ->select('kantor_wilayah.nama_wilayah as nama')
                   ->where('kantor_wilayah.id', '=', $staff->id_kwilayah)
                   ->groupBy('kantor_wilayah.id')
                   ->get();
      }

      if ($staff->departemen == "Admin Area") {
        return DB::table('kantor_area')
                   ->join('users', 'users.id_karea', '=', 'kantor_area.id')
                   ->select('kantor_area.nama')
                   ->where('kantor_area.id', '=', $staff->id_karea)
                   ->groupBy('kantor_area.id')
                   ->get();
      }
      if ($staff->departemen == "Admin Sub Area") {
        return DB::table('sub_area')
                   ->join('users', 'users.id_subarea', '=', 'sub_area.id')
                   ->select('sub_area.nama_subarea as nama')
                   ->where('sub_area.id', '=', $staff->id_subarea)
                   ->groupBy('sub_area.id')
                   ->get();
      }
    }

    public function nomor(){
        return KantorWilayah::with('provinsi')->paginate();
    }

    public function masaBerlaku(){
    //   $staff = auth('api')->user();

    //   $tgl = DB::table('users')
    //               ->select('users.masa_berlaku')
    //               ->where([['users.id', '=', $staff->id],['users.jabatan', '=', 'TT']])
    //               ->orWhere([['users.id', '=', $staff->id],['users.jabatan', '=', 'PJT']])
    //               ->first();
    // $tgl_berlaku = is_null($tgl->masa_berlaku) ? datte : Date::parse($tgl->masa_berlaku)->format('j F Y');

    //   $dt = Carbon::now();

    //   $hitung = $dt->diffForHumans($tgl->masa_berlaku);

    //   if(Carbon::parse($tgl->masa_berlaku)->isPast()){
    //     $staff = User::where([['users.id', '=', $staff->id],['users.jabatan', '=', 'TT']])
    //                   ->orWhere([['users.id', '=', $staff->id],['users.jabatan', '=', 'PJT']])
    //                   ->first();
    //     $staff->update([
    //       'status' => 'Tidak Aktif',
    //     ]);
    //   }

    //   $status = $staff->status;

    //   return response()->json([
    //     'masa_berlaku' => $tgl_berlaku,
    //     'hitung' => $hitung,
    //     'status' => $status,
    //   ]);
    }

    public function pelanggan(){
      $staff = auth('api')->user();
      if ($staff->jabatan == "Admin Pusat") {
        return Pelanggan::count();
      }
      if ($staff->departemen == "Admin Wilayah") {
        return Pelanggan::where('id_kwilayah', '=', $staff->id_kwilayah)->count();
      }
      if ($staff->departemen == "Admin Area") {
        return Pelanggan::where('id_karea', '=', $staff->id_karea)->count();
      }
      if ($staff->departemen == "Admin Sub Area") {
        return Pelanggan::where('id_subarea', '=', $staff->id_subarea)->count();
      }
    }

    public function pembayaran(){
      $staff = auth('api')->user();
      if ($staff->jabatan == "Admin Pusat") {
        return Pembayaran::count();
      }
      if ($staff->departemen == "Admin Wilayah") {
        // return Pembayaran::where('id_kwilayah', '=', $staff->id_kwilayah)->count();
        return DB::table('pembayaran')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                   ->where('pelanggan.id_kwilayah', '=', $staff->id_kwilayah)
                   ->count();
      }
      if ($staff->departemen == "Admin Area") {
        return DB::table('pembayaran')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                   ->where([['pelanggan.id_karea', '=', $staff->id_karea],['pembayaran.tipe', '=', 'Area']])
                   ->count();
      }
      if ($staff->departemen == "Admin Sub Area") {
        return DB::table('pembayaran')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                   ->where([['pelanggan.id_subarea', '=', $staff->id_subarea],['pembayaran.tipe', '=', 'Sub Area']])
                   ->count();
      }
    }

    public function sertifikasi(){
      $staff = auth('api')->user();
      if ($staff->jabatan == "Admin Pusat") {
        return Sertifikasi::count();
      }
      if ($staff->departemen == "Admin Wilayah") {
        // return Sertifikasi::where('id_kwilayah', '=', $staff->id_kwilayah)->count();
        return DB::table('sertifikasi')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', 'sertifikasi.no_pendaftaran')
                   ->where('pelanggan.id_kwilayah', '=', $staff->id_kwilayah)
                   ->count();
      }
      if ($staff->departemen == "Admin Area") {
        return DB::table('sertifikasi')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', 'sertifikasi.no_pendaftaran')
                   ->where('pelanggan.id_karea', '=', $staff->id_karea)
                   ->count();
      }
      if ($staff->departemen == "Admin Sub Area") {
        return DB::table('sertifikasi')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', 'sertifikasi.no_pendaftaran')
                   ->where('pelanggan.id_subarea', '=', $staff->id_subarea)
                   ->count();
      }
    }

    public function verifikasi(){
      $staff = auth('api')->user();
      if($staff->jabatan == 'PJT'){
        return DB::table('verifikasi')
            ->where('id_staff', '=', $staff->id )
            ->count();
      }else{
        return Verifikasi::count();
      }

    }

    // public function pemeriksaan(){

    //  return SuratTugas::count();

    // }

    public function pemeriksaan(){

      // return SuratTugas::count();
      $staff = auth('api')->user();
      //return $staff->id;
      if($staff->jabatan == 'TT'){
      return DB::table('surat_tugas')
            ->where('id_pemeriksa1', '=', $staff->id )
            ->where('id_pemeriksa2', '=', $staff->id )
            ->count();
      }else{
        return SuratTugas::count();
      }
    }
  

}
