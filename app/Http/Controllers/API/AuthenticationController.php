<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Validator;

use App\Perangkat;

class AuthenticationController extends Controller
{

    public function login(Request $request){
    	$request->validate([
    		'username' => 'required|string',
    		'password' => 'required|string',
        'token' => 'required|string',
        'remember_me' => 'boolean'
    	]);

        $credentials = request(['username', 'password']);

        $perangkat = Perangkat::where('username', '=', $request['username'])->first();

        if ($perangkat) {
          //return response()->json([
          //  'message' => 'Akun anda sedang aktif diperangkat lain. Silakan coba lagi',
          //  'status' => 901
          //],401);
          return json_encode(
            array(
              'message' => 'Akun anda sedang aktif diperangkat lain. Silakan coba lagi',
              'status' => 901
            )
          );
        }


        if (!Auth::attempt($credentials)){
        	//return response()->json([
        	//	'message' => 'Email atau Password Salah',
          //  'status' => 901
          //], 401);
          return json_encode(
            array(
              'message' => 'Email atau Password Salah',
              'status' => 902
            )
          );
        }

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if($request->remember_me)
        	$token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        $token = $tokenResult->accessToken;
        $token_type = 'Bearer';
        $accessToken = $token_type . ' ' . $token;

        $device = new Perangkat();

        $device->username = $request['username'];
        $device->id_users = $request->user()->id;
        $device->token = $request['token'];

        $device->save();
        
        //         $xusername =  $device->username ;
        //         $user_detail = DB::select("SELECT users.id_kwilayah, kantor_wilayah.nama_wilayah, users.id_karea , kantor_area.nama , users.id_subarea , sub_area.nama_subarea
        // FROM users LEFT JOIN kantor_wilayah ON users.id_kwilayah = kantor_wilayah.id
        // LEFT JOIN kantor_area ON users.id_karea = kantor_area.id
        // LEFT JOIN sub_area ON users.id_subarea = sub_area.id
        // WHERE username ='$xusername'");

        //$request->session()->put('user_detail', $user_detail);
        
        // return response()->json([
        //     'user' => $request->user(),
        //   	'acces_token' => $accessToken,
        //     'expires_at' => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString(),
        //     'message' => 'Perangkat didaftarkan',
        //     'detail_data'=>$user,
        //     'status' => 200
        // ]);
        return json_encode(
          array(
            'user' => $request->user(),
          	'acces_token' => $accessToken,
            'expires_at' => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString(),
            'message' => 'Perangkat didaftarkan',
            'detail_data'=>$user,
            'status' => 200
          )
        );
    }

    public function logout(Request $request){
    	$request->user()->token()->revoke();

      $id_users = $request->user()->id;

      $perangkat = Perangkat::where('id_users', '=', $id_users)->first();

      $perangkat->delete();

    	return response()->json([
    		'message' => 'Berhasil Logout',
    	]);
    }
}
