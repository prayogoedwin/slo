<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Asosiasi;

class AsosiasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Asosiasi::orderBy('id','asc')->get();
    }

    public function loadAsosiasi(){
        $asosiasi = Asosiasi::orderBy('nama_asosiasi')->get();
        return response()->json($asosiasi);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_asosiasi'=>'required'
        ]);

        return Asosiasi::create([
            'nama_asosiasi' => $request['nama_asosiasi']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asosiasi = Asosiasi::findOrFail($id);
        $this->validate($request,[
            'nama_asosiasi'=>'required'
        ]);

        $asosiasi->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asosiasi = Asosiasi::findOrFail($id);

        $asosiasi->delete();
    }
}
