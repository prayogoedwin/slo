<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Provinsi;
use App\Kota;

class IndonesiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function loadProvinsi(){
        return Provinsi::latest()->sortBy('provinsi')->paginate(34);
    }

    public function loadKotaProv($id){
     
        //return Kota::with('provinsi')->where('kode_prov', '=', request('kode_prov'))->orderBy('kota')->get();
	return Kota::with('provinsi')->where('kode_prov', '=', $id)->orderBy('kota')->get();
    }

    public function loadKota(){
        return Kota::where('id', '=', request('id_kota'))->get();
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
