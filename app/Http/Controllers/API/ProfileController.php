<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

use App\Pelanggan;
use App\Pembayaran;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Pelanggan::orderBy('id','asc')->paginate(1);
        //
    }

    public function profile()

    {
        return auth('api')->user();
    }

    public function profileApps(){

       // $user = User::findOrFail($id);
      $user = auth('api')->user();


      return response()->json($user, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {

          return response()->json([
                'msg' => 'Password lama yang Anda masukan salah! Silahkan coba lagi',
          ],401);
      }
      if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {

          return response()->json([
                'msg' => 'Password baru tidak boleh sama dengan password lama. Silakan coba lagi',
          ]);

      }
      $validatedData = $request->validate([
          'current_password' => 'required',
          'new_password' => 'required|string|min:6|confirmed',
      ]);
      $admin = auth('api')->user();
      $admin->password = bcrypt($request->get('new_password'));
      $admin->save();

      return response()->json([
                'msg' => 'Password berhasil diubah. Silakan login ulang',
          ]);

    }

    public function store(Request $request)
    {



    }

    public function nopendaftaran(){

        $huruf = "BP-SIP";
        $noTerakhir = Pembayaran::max('no_kwitansi');
        $nomor = (int) substr($noTerakhir, 0, 14);
        $nomor++;


        $bulanRomawi = array("", "I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $no = 1;

        if($noTerakhir){
            $noPendaftaran = $huruf . '-' . sprintf("%05s", abs($nomor)) . '-' . $bulanRomawi[date('n')] . '-' . date('Y');
        }
        else{
            $noPendaftaran = $huruf . '-' . sprintf("%05s", abs($no)) . '-' . $bulanRomawi[date('n')] . '-' . date('Y');
        }

        if ($noTerakhir) {
            $noPendaftaran = sprintf("%05s", abs($nomor)) . '/' . $huruf . '/' . $bulanRomawi[date('n')] . '-' . date('Y');
        }
        else{
            $noPendaftaran = sprintf("%05s", abs($no)) . '/' . $huruf . '/' . $bulanRomawi[date('n')] . '-' . date('Y');
        }

        dd($noPendaftaran);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = User::findOrFail($id);

        $this->validate( $request,[
              'nip' => 'required',
              'nama' => 'required|min:2',
              // 'email' => 'required|string|email|max:255|unique:users,email,'.$profile->id,
              'username' => 'required',
              'jabatan' => 'required',
              'departemen' => 'required',
              'wewenang' => 'required',
        ]);

        $profile->update($request->all());
    }

    public function updateProfile(Request $request){
        $user = auth('api')->user();

        $this->validate( $request,[
              'nip' => 'required|unique:users,nip,'.$user->id,
              'nama' => 'required|min:2',
              // 'email' => 'required|string|email|max:255|unique:users,email,'.$profile->id,
              'username' => 'required|unique:users,email,'.$user->id,
        ]);

        $user->update($request->all());

        return response([
          'user' => $user,
          'msg' => 'Profil berhasil diubah',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
