<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Pembayaran;
use App\Pelanggan;
use App\LogStatus;

use Carbon\Carbon;

class PembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         // return Pembayaran::orderBy('id','asc')->paginate(10);
        $staff = auth('api')->user();
        if (auth('user')->jabatan == 'Admin Pusat') {
          return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota')
                  ->orderBy('id','asc')
                  ->where('status', '=', 'Sudah Bayar')
                  ->get();
        }
        if (auth('user')->jabatan == 'Admin Wilayah') {
          return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota')
                  ->orderBy('id','asc')
                  ->where([['status', '=', 'Sudah Bayar'],['id_kwilayah', '=', $staff->id_kwilayah]])
                  ->get();
        }
        if (auth('user')->jabatan == 'Admin Area'  || $staff->departemen == "Manajer Area") {
          return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota')
                  ->orderBy('id','asc')
                  ->where([['status', '=', 'Sudah Bayar'],['id_karea', '=', $staff->id_karea]])
                  ->get();
        }
        if (auth('user')->jabatan == 'Admin Sub Area') {
          return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota')
                  ->orderBy('id','asc')
                  ->where([['status', '=', 'Sudah Bayar'],['id_karea', '=', $staff->id_subarea]])
                  ->get();
        }

    }

    public function LapPembayaran(){
      $staff = auth('api')->user();
      if ($staff->jabatan == "Admin Pusat") {
        $laporan = DB::table('pelanggan')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->select('pembayaran.id','pembayaran.no_kwitansi', 'pembayaran.jumlah', 'pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
        ->orderBy('id','desc')
        ->where('pelanggan.status','=','Sudah Bayar')
        ->get();
        return response()->json($laporan);
      }
      if ($staff->departemen == "Admin Wilayah") {
        $laporan = DB::table('pelanggan')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->select('pembayaran.id','pembayaran.no_kwitansi', 'pembayaran.jumlah', 'pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
        ->orderBy('id','desc')
        ->where([['pelanggan.status','=','Sudah Bayar'],['pelanggan.id_kwilayah', '=', $staff->id_kwilayah]])
        ->get();
        return response()->json($laporan);
      }
      if ($staff->departemen == "Admin Area" || $staff->departemen == "Manajer Area") {
        $laporan = DB::table('pelanggan')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->select('pembayaran.id','pembayaran.no_kwitansi', 'pembayaran.jumlah','pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
        ->orderBy('id','desc')
        ->where([['pelanggan.status','=','Sudah Bayar'],['pelanggan.id_karea', '=', $staff->id_karea]])
        ->get();
        return response()->json($laporan);
      }
      if ($staff->departemen == "Admin Sub Area") {
        $laporan = DB::table('pelanggan')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->select('pembayaran.id','pembayaran.no_kwitansi', 'pembayaran.jumlah', 'pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
        ->orderBy('id','desc')
        ->where([['pelanggan.status','=','Sudah Bayar'],['pelanggan.id_subarea', '=', $staff->id_subarea]])
        ->get();
        return response()->json($laporan);
      }
	
    }

    public function filterPembayaran($tgl1,$tgl2){
      $laporan = DB::table('pelanggan')
      ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
      ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
      ->join('sub_area','sub_area.id','=','id_subarea')
      ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
      ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
      ->select('pembayaran.id','pembayaran.no_kwitansi','pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
      ->orderBy('id','desc')
      ->where('pelanggan.status','=','Sudah Bayar')
      ->whereBetween('pembayaran.created_at',[$tgl1, $tgl2])
      ->paginate(10);

      return response()->json($laporan);
    }

    public function belumBayar(){
        $staff = auth('api')->user();
        if (auth('api')->user()->jabatan == 'Admin Pusat' || auth('api')->user()->departemen == 'Manajer Area' || auth('api')->user()->departemen == 'MG') {

            return Pelanggan::with('wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['status', '=', 'Belum Bayar']])
                ->orderBy('id','asc')->get();

            // return response()->json([
            //   'msg' => 'Tidak memiliki hak akses'
            // ],401);
        }
        if (auth('api')->user()->departemen == 'Admin Wilayah') {

            return Pelanggan::with('wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['id_kwilayah', '=', $staff->id_kwilayah],
                ['status', '=', 'Belum Bayar']])
                ->orderBy('id','asc')->get();
            // return response()->json([
            //   'msg' => 'Tidak memiliki hak akses'
            // ],401);
        }
        if (auth('api')->user()->departemen == 'Admin Area') {

            return Pelanggan::with('wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['id_karea', '=', $staff->id_karea],
                ['status', '=', 'Belum Bayar']])
                ->orderBy('id','asc')->get();
            }
        if (auth('api')->user()->departemen == 'Admin Sub Area') {
            return Pelanggan::with('wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['id_subarea', '=', $staff->id_subarea],
                ['status', '=', 'Belum Bayar']])
                ->orderBy('id','asc')->get();
        }
    }

    public function loadPelanggan(){
        $pelanggan = Pelanggan::get();
      return response()->json($pelanggan);
    }

    public function sudahBayar(){
        $staff = auth('api')->user();
	
	//var_dump(auth('api')->user()->jabatan);die();
        if (auth('api')->user()->jabatan == 'Admin Pusat' || auth('api')->user()->departemen == 'Manajer Area' || auth('api')->user()->departemen == 'MG') {

          return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
              ['status', '=', 'Sudah Bayar']])
              ->orderBy('id','asc')->get();

          // return response()->json([
          //   'msg' => 'Tidak memiliki hak akses'
          // ],401);
        }
        if (auth('api')->user()->departemen == 'Admin Wilayah') {
            return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['id_kwilayah', '=', $staff->id_kwilayah],
                ['status', '=', 'Sudah Bayar']])
                ->orderBy('id','asc')->get();
        }
        if (auth('api')->user()->departemen == 'Admin Area') {

            return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['id_karea', '=', $staff->id_karea],
                ['status', '=', 'Sudah Bayar']])
                ->orderBy('id','asc')->get();
            }
        if (auth('api')->user()->departemen == 'Admin Sub Area') {
            return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
                ['id_subarea', '=', $staff->id_subarea],
                ['status', '=', 'Sudah Bayar']])
                ->orderBy('id','asc')->get();
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $huruf = "BP-SIP";
        $noTerakhir = Pembayaran::max('no_kwitansi');
        $nomor = (int) substr($noTerakhir, 0, 14);
        $nomor++;


        $bulanRomawi = array("", "I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $no = 1;

        if ($noTerakhir) {
            $noKwitansi = sprintf("%05s", abs($nomor)) . '/' . $huruf . '/' . $bulanRomawi[date('n')] . '/' . date('Y');
        }
        else{
            $noKwitansi = sprintf("%05s", abs($no)) . '/' . $huruf . '/' . $bulanRomawi[date('n')] . '/' . date('Y');
        }

        $staff = auth('api')->user();
	$tipe = 'Area';
        if ($staff->departemen == "Admin Area") {
          $tipe = "Area";
        }
        if ($staff->departemen == "Admin Sub Area") {
          $tipe = "Sub Area";
        }
        Pembayaran::create([
            'no_kwitansi' => $noKwitansi,
            'no_pendaftaran' => $request['no_pendaftaran'],
            'diterima' => $staff->nama,
            'cetak' => "Belum Dicetak",
            'jumlah' => $request['jumlah'],
            'tanggal' => Carbon::now(),
            'tipe' => $tipe
        ]);


        $pelanggan = Pelanggan::where('no_pendaftaran', '=', $request['no_pendaftaran']);

        $pelanggan->update([
            'status' => 'Sudah Bayar',
            'status_temp' => '0',
        ]);

        $log = LogStatus::where('no_pendaftaran', '=', $request['no_pendaftaran']);

        $log->update([
          'status' => 'Pembayaran dikonfirmasi. Proses pemeriksaan'
        ]);

        // 000001/BP-SIP/I/2019
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    public function dtBelumBayar($id){
      $pelanggan = Pelanggan::with('daya', 'tarif', 'btl')->where('id', '=', $id)->get();

      return response()->json($pelanggan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateCetak(Request $request, $id){
      $staff = auth('api')->user()->nama;
      $pembayaran = Pembayaran::findOrFail($id);

      $noTerakhir = Pembayaran::where('id', '=', $id)->max('cetak_kwitansi');
      $noTerakhir++;
      $pembayaran->cetak_kwitansi = $noTerakhir;
      $pembayaran->cetak = "Sudah Dicetak " . $noTerakhir . "X";
      $pembayaran->dicetak_oleh = $staff;

      $pembayaran->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
