<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\KantorArea;
use App\KantorWilayah;
//use Request;
class KAreaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return KantorArea::with('kwilayah','kota')->orderBy('id', 'ASC')->get();
    }

    public function loadArea($id){
        $area = KantorArea::where('id_kwilayah', '=', $id)->get();
        return response()->json($area);
    }

    public function getArea($id){
        $staff = auth('api')->user();
        if ($staff->jabatan == 'Admin Pusat' || $staff->departemen == 'Admin Wilayah') {
          $area = KantorArea::where('id_kwilayah', '=', $id)->orderBy('nama')->get();
          return response()->json($area);
        }
        if ($staff->departemen == 'Admin Area' || $staff->departemen == 'Admin Sub Area' || $staff->departemen == 'BTL') {
          $area = KantorArea::where([['id_kwilayah', '=', $id],['id', '=', $staff->id_karea]])->orderBy('nama')->get();
          return response()->json($area);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate( $request,[
            'id_kwilayah'=>'required|',
            'kode_kota'=>'required',
            'nama'=>'required|min:5',
            'manager' => 'required|min:5',
            'pjt' => 'required|min:5',
            'status' => 'required',
        ]);

        return KantorArea::create([
            'id_kwilayah'=> $request['id_kwilayah'],
            'kode_kota'=> $request['kode_kota'],
            'nama'=> $request['nama'],
            'manager' => $request['manager'],
            'pjt' => $request['pjt'],
            'status' => $request['status']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $area = KantorArea::findOrFail($id);
        $this->validate( $request,[
            'id_kwilayah'=>'required|',
            'kode_kota'=>'required',
            'nama'=>'required|min:5',
            'manager' => 'required|min:5',
            'pjt' => 'required|min:5',
            'status' => 'required',
        ]);

        $area->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area = KantorArea::findOrFail($id);

        $area->delete();
    }
}
