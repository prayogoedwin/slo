<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\SubArea;

class SubAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SubArea::with('wilayah','area')->orderBy('id','asc')->get();
    }

    public function loadSubArea($id){
        $staff = auth('api')->user();
        if ($staff->departemen == 'Admin Sub Area') {
          $subarea = SubArea::where([['id_karea', '=',$id],['id', '=', $staff->id_subarea]])->orderBy('nama_subarea')->get();

          return response()->json($subarea);
        } else {
          $subarea = SubArea::where('id_karea', '=', $id)->orderBy('nama_subarea')->get();

          return response()->json($subarea);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate( $request,[
            'id_kwilayah' => 'required',
            'id_karea' => 'required',
            'nama_subarea' => 'required|min:3|unique:sub_area',
            'kode' => 'required|min:3|unique:sub_area',
            'alamat' => 'sometimes',
            'nama_manager' => 'sometimes',
            'status' => 'required',
        ]);

        return SubArea::create([
            'id_kwilayah' => $request['id_kwilayah'],
            'id_karea' => $request['id_karea'],
            'nama_subarea' => $request['nama_subarea'],
            'kode' => $request['kode'],
            'alamat' => $request['alamat'],
            'nama_manager' => $request['nama_manager'],
            'status' => $request['status'],
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subarea = SubArea::findOrFail($id);

        $this->validate( $request,[
            'id_kwilayah' => 'required',
            'id_karea' => 'required',
            'nama_subarea' => 'required|min:3|unique:sub_area,nama_subarea,'.$subarea->id,
            'kode' => 'required|min:3|unique:sub_area,kode,'.$subarea->id,
            'alamat' => 'sometimes',
            'nama_manager' => 'sometimes',
            'status' => 'required',
        ]);

        $subarea->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subarea = SubArea::findOrFail($id);

        $subarea->delete();
    }
}
