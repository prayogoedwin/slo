<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

use App\User;
use App\PelangganTemp;
use App\KantorWilayah;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    // public function index()
    // {
    //     return User::with('wilayah','area','subarea')->orderBy('id','asc')->whereNotIn('jabatan', ['Admin Pusat', 'BTL', 'Internal'])->get();
    // }

    public function index()
    {
      $staff = auth('api')->user();

      if($staff->jabatan == 'Admin Pusat'){
        return User::with('wilayah','area','subarea')->orderBy('id','asc')->whereNotIn('jabatan', ['Admin Pusat', 'BTL', 'Internal'])->get();
      }elseif($staff->jabatan == 'Admin'){
        return User::with('wilayah','area','subarea')->orderBy('id','asc')
        ->where('id_kwilayah','=',$staff->id_kwilayah)
        ->whereNotIn('jabatan', ['Admin Pusat', 'BTL', 'Internal'])->get();
      }else{
        return User::with('wilayah','area','subarea')->orderBy('id','asc')
        ->where('id_kwilayah','=',$staff->id_kwilayah)
        ->where('id_karea','=',$staff->id_karea)
        ->whereNotIn('jabatan', ['Admin Pusat', 'BTL', 'Internal'])->get();
      }
    }

    public function loadWilayah(Request $request){
      $userLogin = auth('api')->user();
      // $user = User::with('wilayah')->where('id', '=', $userLogin)->get();
      // return response()->json($user);
      if ($userLogin->jabatan == 'Admin Pusat') {
        $wilayah = KantorWilayah::all();
        return response()->json($wilayah);
      }
      if ($userLogin->jabatan == 'Admin') {
        $wilayah = KantorWilayah::where('id', '=', $userLogin->id_kwilayah)->get();
        return response()->json($wilayah);
      }
      if ($userLogin->jabatan == 'BTL') {
        $wilayah = KantorWilayah::where('id', '=', $userLogin->id_kwilayah)->get();
        return response()->json($wilayah);
      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate( $request,[
              'nip' => 'sometimes',
              'nama' => 'required|min:2',
              'email' => 'required|string|email|max:255|unique:users',
              'no_telp' => 'required|string|max:14|unique:users',
              'username' => 'required|string|min:4|unique:users',
              'jabatan' => 'required',
              'departemen' => 'required',
              'wewenang' => 'required',
              'id_kwilayah' => 'required',
              'id_karea' => 'sometimes',
              'id_subarea' => 'sometimes',
              'status' => 'required',
              'password' => 'required|string|min:6|confirmed',
        ]);

        return User::create([
            'nip' => $request['nip'],
            'nama' => $request['nama'],
            'email' => $request['email'],
            'no_telp' => $request['no_telp'],
            'username' => $request['username'],
            'jabatan' => $request['jabatan'],
            'departemen' => $request['departemen'],
            'wewenang' => $request['wewenang'],
            'id_kwilayah' => $request['id_kwilayah'],
            'id_karea' => $request['id_karea'],
            'id_subarea' => $request['id_subarea'],
            'status' => $request['status'],
            'password' => bcrypt($request['password']),
            'masa_berlaku' => $request['masa_berlaku'],
        ]);
    }

    public function loadPemeriksa(){
      	$staff = auth('api')->user();
	    $pelanggan = PelangganTemp::where([['id_staff', '=', $staff->id], ['status_terbit', '=', '0']])->first();
	    $res = User::where([['departemen', '=', 'Pemeriksa'], ['id_karea', '=', $staff->id_karea]])->get();	    
      	return $res;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $staff = User::findOrFail($id);

        $this->validate( $request,[
              'nip' => 'sometimes',
              'nama' => 'required|min:2',
              'email' => 'required|string|email|max:255|unique:users,email,'.$staff->id,
              'no_telp' => 'required|string|max:14|unique:users,no_telp,'.$staff->id,
              'username' => 'required|string|min:4|unique:users,username,'.$staff->id,
              'jabatan' => 'required',
              'departemen' => 'required',
              'wewenang' => 'required',
              'id_kwilayah' => 'required',
              'id_karea' => 'sometimes',
              'id_subarea' => 'sometimes',
              'status' => 'required',
        ]);

        $staff->update([
          'nip' => $request['nip'],
          'nama' => $request['nama'],
          'email' => $request['email'],
          'no_telp' => $request['no_telp'],
          'username' => $request['username'],
          'jabatan' => $request['jabatan'],
          'departemen' => $request['departemen'],
          'wewenang' => $request['wewenang'],
          'id_kwilayah' => $request['id_kwilayah'],
          'id_karea' => $request['id_karea'],
          'id_subarea' => $request['id_subarea'],
          'status' => $request['status'],
        ]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = User::findOrFail($id);

        $staff->delete();
    }
}
