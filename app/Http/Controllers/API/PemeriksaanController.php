<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pemeriksa;
use App\Pelanggan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\PelangganTemp;
use App\LogStatus;

// use Image;
use Intervention\Image\Facades\Image;
class PemeriksaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $staff = auth('api')->user();
      if ($staff->status == 'Aktif') {

        $surat = DB::table('pelanggan')->select('pelanggan.id as id','pelanggan.no_pendaftaran','pelanggan.nama','pelanggan.alamat','pembayaran.no_kwitansi','daya.daya','kota.kota',
                      'kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea', 'penyedia_listrik.nama_penyedia','surat_tugas.no_surat','pembayaran.created_at as tgl_bayar','surat_tugas.jangka_waktu')
                      ->join('pembayaran', 'pembayaran.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                      ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
                      ->join('kota', 'kota.kode_kota', '=', 'pelanggan.kode_kota')
                      ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'pelanggan.id_kwilayah')
                      ->join('kantor_area', 'kantor_area.id', '=', 'pelanggan.id_karea')
                      ->join('sub_area', 'sub_area.id', '=', 'pelanggan.id_subarea')
                      ->join('penyedia_listrik', 'penyedia_listrik.id', '=', 'pelanggan.id_penyedia')
                      ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
                      ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
                      ->where([['surat_tugas.id_pemeriksa1', '=', $staff->id],['pelanggan_temp.status_periksa', '=', '0']])
                      ->orWhere([['surat_tugas.id_pemeriksa2', '=', $staff->id],['pelanggan_temp.status_periksa', '=', '0']])
                      ->get();

        return response()->json($surat);
      } else {
        return response()->json([
          'msg' => 'Akun anda tidak aktif',
        ], 401);
      }


      // return response()->json($pelanggan);
    }

    public function dahDiperiksa(){
      $staff = auth('api')->user();
      if ($staff->status == 'Aktif') {
        return DB::table('pelanggan')->select('pelanggan.id as id','pelanggan.no_pendaftaran','pelanggan.nama','pelanggan.alamat','pembayaran.no_kwitansi','daya.daya','kota.kota',
                      'kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea', 'penyedia_listrik.nama_penyedia','surat_tugas.no_surat','pembayaran.created_at as tgl_bayar',
                      'pemeriksa.status_verifikasi')
                      ->join('pembayaran', 'pembayaran.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                      ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
                      ->join('kota', 'kota.kode_kota', '=', 'pelanggan.kode_kota')
                      ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'pelanggan.id_kwilayah')
                      ->join('kantor_area', 'kantor_area.id', '=', 'pelanggan.id_karea')
                      ->join('sub_area', 'sub_area.id', '=', 'pelanggan.id_subarea')
                      ->join('penyedia_listrik', 'penyedia_listrik.id', '=', 'pelanggan.id_penyedia')
                      ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
                      ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
                      ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                      ->where([['surat_tugas.id_pemeriksa1', '=', $staff->id],['pelanggan_temp.status_periksa', '=', '1']])
                      ->orWhere([['surat_tugas.id_pemeriksa2', '=', $staff->id],['pelanggan_temp.status_periksa', '=', '1']])
                      // ->orWhere([['surat_tugas.id_pemeriksa2', '=', $staff],['pelanggan_temp.status_periksa', '=', '1']])
                      ->get();
      } else {
        return response()->json([
          'msg' => 'Akun anda tidak aktif',
        ], 401);
      }
    }

    public function lapPemeriksa(){
      $staff = auth('api')->user();
      if ($staff->jabatan == "Admin Pusat") {
        return DB::table('pelanggan')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->join('daya','daya.id','=','pelanggan.id_daya')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
        ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
        ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
        ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
        ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
        ->select('pembayaran.no_kwitansi','pelanggan.no_pendaftaran','pelanggan.nama','pelanggan.alamat','daya.daya',
          'kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea',
          'pemeriksa.no_lhpp','pemeriksa.tgl_lhpp','pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2',
          'pembayaran.created_at as tanggal_pembayaran','pemeriksa.created_at as tgl_pemeriksaan','pemeriksa.foto1','pemeriksa.foto2','pemeriksa.foto3','pemeriksa.foto4')
        ->where('pelanggan_temp.status_periksa', '=', '1')
        ->get();
      }
      if ($staff->departemen == "Admin Wilayah") {
        return DB::table('pelanggan')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->join('daya','daya.id','=','pelanggan.id_daya')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
        ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
        ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
        ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
        ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
        ->select('pembayaran.no_kwitansi','pelanggan.no_pendaftaran','pelanggan.nama','pelanggan.alamat','daya.daya',
          'kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea',
          'pemeriksa.no_lhpp','pemeriksa.tgl_lhpp','pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2',
          'pembayaran.created_at as tanggal_pembayaran','pemeriksa.created_at as tgl_pemeriksaan','pemeriksa.foto1','pemeriksa.foto2','pemeriksa.foto3','pemeriksa.foto4')
        ->where([['pelanggan_temp.status_periksa', '=', '1'],['pelanggan.id_kwilayah', '=', $staff->id_kwilayah]])
        ->get();
      }
      if ($staff->departemen == "Admin Area") {
        return DB::table('pelanggan')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->join('daya','daya.id','=','pelanggan.id_daya')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
        ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
        ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
        ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
        ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
        ->select('pembayaran.no_kwitansi','pelanggan.no_pendaftaran','pelanggan.nama','pelanggan.alamat','daya.daya',
          'kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea',
          'pemeriksa.no_lhpp','pemeriksa.tgl_lhpp','pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2',
          'pembayaran.created_at as tanggal_pembayaran','pemeriksa.created_at as tgl_pemeriksaan','pemeriksa.foto1','pemeriksa.foto2','pemeriksa.foto3','pemeriksa.foto4')
        ->where([['pelanggan_temp.status_periksa', '=', '1'],['pelanggan.id_karea', '=', $staff->id_karea]])
        ->get();
      }
      if ($staff->departemen == "Admin Sub Area") {
        return DB::table('pelanggan')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->join('daya','daya.id','=','pelanggan.id_daya')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
        ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
        ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
        ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
        ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
        ->select('pembayaran.no_kwitansi','pelanggan.no_pendaftaran','pelanggan.nama','pelanggan.alamat','daya.daya',
          'kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea',
          'pemeriksa.no_lhpp','pemeriksa.tgl_lhpp','pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2',
          'pembayaran.created_at as tanggal_pembayaran','pemeriksa.created_at as tgl_pemeriksaan','pemeriksa.foto1','pemeriksa.foto2','pemeriksa.foto3','pemeriksa.foto4')
        ->where([['pelanggan_temp.status_periksa', '=', '1'],['pelanggan.id_subarea', '=', $staff->id_subarea]])
        ->get();
      }

    }
    public function filterPemeriksa($tgl1,$tgl2){
      return DB::table('pelanggan')
      ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
      ->join('daya','daya.id','=','pelanggan.id_daya')
      ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
      ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
      ->join('sub_area','sub_area.id','=','id_subarea')
      ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
      ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
      ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
      ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
      ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
      ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
      ->select('pembayaran.no_kwitansi','pelanggan.no_pendaftaran','pelanggan.nama','pelanggan.alamat','daya.daya',
        'kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea',
        'pemeriksa.no_lhpp','pemeriksa.tgl_lhpp','pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2',
        'pembayaran.created_at as tanggal_pembayaran','pemeriksa.created_at as tgl_pemeriksaan','pemeriksa.foto1','pemeriksa.foto2','pemeriksa.foto3','pemeriksa.foto4')
      ->where('pelanggan_temp.status_periksa', '=', '1')
      ->whereBetween('pemeriksa.created_at',[$tgl1, $tgl2])
      ->paginate(10);
    }

    public function generateNoLHPP(){
      $huruf = "LHI";
      // $pelanggan = Pelanggan::where('no_pendaftaran', '=', $no_pendaftaran)->get();
      $noTerakhir = Pemeriksa::max('no_lhpp');
      $nomor = (int) substr($noTerakhir, 17, 21);
      $nomor++;

      $staff = auth('api')->user();
      $no = 1;

      if ($noTerakhir) {
        $noLhpp = $huruf . '-' . date('my') . '-' . sprintf("%03s", $staff->id_kwilayah) . '-' . sprintf("%03s", $staff->id) . '-' . sprintf("%04s", abs($nomor));
        return response()->json([
          'noLhpp' => $noLhpp
        ]);
      } else {
        $noLhpp = $huruf . '-' . date('my') . '-' . sprintf("%03s", $staff->id_kwilayah) . '-' . sprintf("%03s", $staff->id) . '-' . sprintf("%04s", abs($no));
        return response()->json([
          'noLhpp' => $noLhpp
        ]);
      }
    }

    public function prosesPemeriksaan($no_pendaftaran){
      // $pemeriksa  = User::where('departemen', '=', 'Pemeriksa')->all();

      return DB::table('pelanggan')->select('pelanggan.id as id','pelanggan.no_pendaftaran','pelanggan.nama','pelanggan.alamat','tarif.jenis_tarif','daya.daya',
                    'btl.nama_btl','surat_tugas.no_surat','pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2')
                    ->join('tarif', 'tarif.id', '=', 'pelanggan.id_tarif')
                    ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
                    ->join('btl', 'btl.id', '=', 'pelanggan.id_btl')
                    ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
                    ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
                    ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
                    ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
                    ->where('pelanggan.no_pendaftaran', '=', $no_pendaftaran)
                    ->get();

      // $huruf = "LHI";
      // $noTerakhir = Pemeriksa::max('no_lhpp');
      // $nomor = (int) substr($noTerakhir, 17, 21);
      // $nomor++;
      //
      // $no = 1;
      //
      // if($noTerakhir){
      //     $noLhpp = $huruf . '-' . date('my') . '-' . sprintf("%03s", $request['id_kwilayah']) . '-' . sprintf("%03s", $request['id_karea']) . '-' . sprintf("%04s", abs($nomor));
      // }
      // else{
      //     $noLhpp = $huruf . '-' . date('my') . '-' . sprintf("%03s", $request['id_kwilayah']) . '-' . sprintf("%03s", $request['id_karea']) . '-' . sprintf("%04s", abs($no));
      // }


     // return response()->json($pelanggan);

    }

    public function query(){
      $huruf = "SIP";
      $noTerakhir = Pelanggan::max('no_pendaftaran');
      $nomor = (int) substr($noTerakhir, 17, 21);
      $nomor++;
      $no = 1;

      if($noTerakhir){
          return $noPendaftaran = $huruf . '-' . date('my') . '-' . sprintf("%03s", $request['id_kwilayah']) . '-' . sprintf("%03s", $request['id_karea']) . '-' . sprintf("%04s", abs($nomor));
      }
      else{
          return $noPendaftaran = $huruf . '-' . date('my') . '-' . sprintf("%03s", $request['id_kwilayah']) . '-' . sprintf("%03s", $request['id_karea']) . '-' . sprintf("%04s", abs($no));
      }
    }

    public function konfirmPeriksa($id){
      $pelanggan = Pelanggan::where('id', '=', $id)->get();

      return response()->json($pelanggan);
    }

    public function getForEdit($no_pendaftaran){

    }

    public function editPeriksa(Request $request, $no_pendaftaran){
      $this->validate($request,[
          'no_pendaftaran' => 'required',
          'no_surat_tugas' => 'required',
          // 'no_pemeriksaan' => 'required',
          'no_lhpp' => 'required',
          'tgl_lhpp' => 'required',
          'gambar_instalasi' => 'required',
          'diagram_garis_tunggal' => 'required',
          'pe_utama' => 'required',
          'pe_cabang' => 'required',
          'pe_akhir' => 'required',
          'pe_kotak_kontak' => 'required',
          'jenis_peng_utama' => 'required',
          'jenis_peng_cabang' => 'required',
          'jenis_peng_akhir' => 'required',
          'penghantar_bumi_jenis' => 'required',
          'penghantar_bumi_penampang' => 'required',
          'penghantar_bumi_sistem' => 'required',
          'saklar_utama' => 'required',
          'saklar_cabang1' => 'required',
          'saklar_cabang2' => 'required',
          'phbk_utama' => 'required',
          'phbk_cabang1' => 'required',
          'phbk_cabang2' => 'required',
          'penghantar_utama' => 'required',
          'penghantar_cabang' => 'required',
          'penghantar_akhir' => 'required',
          'penghantar_3fasa' => 'required',
          'fitting_lampu' => 'required',
          'kotak_kontak' => 'required',
          'sakelar' => 'required',
          'tinggi_kotak_kontak' => 'required',
          'tinggi_phbk' => 'required',
          'jenis_kotak_kontak' => 'required',
          'tanda_komponen' => 'required',
          'pengujian_pembebanan' => 'required',
          'jml_phb_utama' => 'required',
          'jml_phb_1fasa' => 'required',
          'jml_phb_3fasa' => 'required',
          'jml_phb_cabang' => 'required',
          'jml_saluran_cabang' => 'required',
          'jml_saluran_akhir' => 'required',
          'jml_titik_lampu' => 'required',
          'jml_sakelar' => 'required',
          'kkb' => 'required',
          'kkk' => 'required',
          'tahanan_isolasi_penghantar' => 'required',
          'resisten_pembumian' => 'required',
          'jml_motor_listrik_unit' => 'required',
          'jml_motor_listrik_kwh' => 'required',
          'catatan' => 'required',
      ]);

      ini_set('gd.jpeg_ignore_warning', 1);

      $pemeriksa = Pemeriksa::where('no_pendaftaran', '=', $no_pendaftaran)->first();

      $pemeriksa->gambar_instalasi = $request['gambar_instalasi'];
      $pemeriksa->diagram_garis_tunggal = $request['diagram_garis_tunggal'];
      $pemeriksa->pe_utama = $request['pe_utama'];
      $pemeriksa->pe_cabang = $request['pe_cabang'];
      $pemeriksa->pe_akhir = $request['pe_akhir'];
      $pemeriksa->pe_kotak_kontak = $request['pe_kotak_kontak'];
      $pemeriksa->jenis_peng_utama = $request['jenis_peng_utama'];
      $pemeriksa->jenis_peng_cabang = $request['jenis_peng_utama'];
      $pemeriksa->jenis_peng_akhir = $request['jenis_peng_akhir'];
      $pemeriksa->penghantar_bumi_jenis = $request['penghantar_bumi_jenis'];
      $pemeriksa->penghantar_bumi_penampang = $request['penghantar_bumi_penampang'];
      $pemeriksa->penghantar_bumi_sistem = $request['penghantar_bumi_sistem'];
      $pemeriksa->saklar_utama = $request['saklar_utama'];
      $pemeriksa->saklar_cabang1 = $request['saklar_cabang1'];
      $pemeriksa->saklar_cabang2 = $request['saklar_cabang2'];
      $pemeriksa->phbk_utama = $request['phbk_utama'];
      $pemeriksa->phbk_cabang1 = $request['phbk_cabang1'];
      $pemeriksa->phbk_cabang2 = $request['phbk_cabang2'];
      $pemeriksa->penghantar_utama = $request['penghantar_utama'];
      $pemeriksa->penghantar_cabang = $request['penghantar_cabang'];
      $pemeriksa->penghantar_akhir = $request['penghantar_akhir'];
      $pemeriksa->penghantar_3fasa = $request['penghantar_3fasa'];
      $pemeriksa->fitting_lampu = $request['fitting_lampu'];
      $pemeriksa->kotak_kontak = $request['kotak_kontak'];
      $pemeriksa->sakelar = $request['sakelar'];
      $pemeriksa->tinggi_kotak_kontak = $request['tinggi_kotak_kontak'];
      $pemeriksa->tinggi_phbk = $request['tinggi_phbk'];
      $pemeriksa->jenis_kotak_kontak = $request['jenis_kotak_kontak'];
      $pemeriksa->tanda_komponen = $request['tanda_komponen'];
      $pemeriksa->pengujian_pembebanan = $request['pengujian_pembebanan'];
      $pemeriksa->jml_phb_utama = $request['jml_phb_utama'];
      $pemeriksa->jml_phb_1fasa = $request['jml_phb_1fasa'];
      $pemeriksa->jml_phb_3fasa = $request['jml_phb_3fasa'];
      $pemeriksa->jml_phb_cabang = $request['jml_phb_cabang'];
      $pemeriksa->jml_saluran_cabang = $request['jml_saluran_cabang'];
      $pemeriksa->jml_saluran_akhir = $request['jml_saluran_akhir'];
      $pemeriksa->jml_titik_lampu = $request['jml_titik_lampu'];
      $pemeriksa->jml_sakelar = $request['jml_sakelar'];
      $pemeriksa->kkb = $request['kkb'];
      $pemeriksa->kkk = $request['kkk'];
      $pemeriksa->tahanan_isolasi_penghantar = $request['tahanan_isolasi_penghantar'];
      $pemeriksa->resisten_pembumian = $request['resisten_pembumian'];
      $pemeriksa->jml_motor_listrik_unit = $request['jml_motor_listrik_unit'];
      $pemeriksa->jml_motor_listrik_kwh = $request['jml_motor_listrik_kwh'];
      $pemeriksa->catatan = $request['catatan'];
      $pemeriksa->location = $request['location'];
      $pemeriksa->lat = $request['lat'];
      $pemeriksa->lng = $request['lng'];

      if ($request->hasFile('foto1')) {
         $foto1 = $request->file('foto1');
         $nomor1 = Rand(1000, 9999);
         $filename = time() . '.' . $nomor1 . '.' . $foto1->getClientOriginalExtension();
         $location = public_path('images/pemeriksa/' . $filename);
        //  Image::make($foto1)->save($location);


         $fotolama1 = $pemeriksa->foto1;
         $lokasiLama = public_path($fotolama1);
         $pemeriksa->foto1 = '/images/pemeriksa/'.$filename;

         if (file_exists($lokasiLama)) {
          unlink($lokasiLama); 
         }
       }

       if ($request->hasFile('foto2')) {
          $foto2 = $request->file('foto2');
          $nomor2 = Rand(1000, 9999);
          $filename = time() . '.' . $nomor2 . '.' . $foto2->getClientOriginalExtension();
          $location = public_path('images/pemeriksa/' . $filename);
          // Image::make($foto2)->save($location);


          $fotolama2 = $pemeriksa->foto2;
          $lokasiLama = public_path($fotolama2);
          $pemeriksa->foto2 = '/images/pemeriksa/'.$filename;

          if (file_exists($lokasiLama)) {
            unlink($lokasiLama); 
          }

        }

        if ($request->hasFile('foto3')) {
           $foto3 = $request->file('foto3');
           $nomor3 = Rand(1000, 9999);
           $filename = time() . '.' . $nomor3 . '.' . $foto3->getClientOriginalExtension();
           $location = public_path('images/pemeriksa/' . $filename);
          //  Image::make($foto3)->save($location);


           $fotolama3 = $pemeriksa->foto3;
           $lokasiLama = public_path($fotolama3);
           $pemeriksa->foto3 = '/images/pemeriksa/'.$filename;

           if (file_exists($lokasiLama)) {
            unlink($lokasiLama); 
           }
         }

         if ($request->hasFile('foto4')) {
            $foto4 = $request->file('foto4');
            $nomor4 = Rand(1000, 9999);
            $filename = time() . '.' . $nomor4 . '.' . $foto4->getClientOriginalExtension();
            $location = public_path('images/pemeriksa/' . $filename);
            // Image::make($foto4)->save($location);


            $fotolama4 = $pemeriksa->foto4;
            $lokasiLama = public_path($fotolama4);
            $pemeriksa->foto4 = '/images/pemeriksa/'.$filename;

            if (file_exists($lokasiLama)) {
              unlink($lokasiLama); 
            }

          }

      $pemeriksa->save();

      return response()->json([
        'msg' => 'Data berhasil diubah'
      ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'no_pendaftaran' => 'required',
            'no_surat_tugas' => 'required',
            // 'no_pemeriksaan' => 'required',
            'no_lhpp' => 'required',
            'tgl_lhpp' => 'required',
            'gambar_instalasi' => 'required',
            'diagram_garis_tunggal' => 'required',
            'pe_utama' => 'required',
            'pe_cabang' => 'required',
            'pe_akhir' => 'required',
            'pe_kotak_kontak' => 'required',
            'jenis_peng_utama' => 'required',
            'jenis_peng_cabang' => 'required',
            'jenis_peng_akhir' => 'required',
            'penghantar_bumi_jenis' => 'required',
            'penghantar_bumi_penampang' => 'required',
            'penghantar_bumi_sistem' => 'required',
            'saklar_utama' => 'required',
            'saklar_cabang1' => 'required',
            'saklar_cabang2' => 'required',
            'phbk_utama' => 'required',
            'phbk_cabang1' => 'required',
            'phbk_cabang2' => 'required',
            'penghantar_utama' => 'required',
            'penghantar_cabang' => 'required',
            'penghantar_akhir' => 'required',
            'penghantar_3fasa' => 'required',
            'fitting_lampu' => 'required',
            'kotak_kontak' => 'required',
            'sakelar' => 'required',
            'tinggi_kotak_kontak' => 'required',
            'tinggi_phbk' => 'required',
            'jenis_kotak_kontak' => 'required',
            'tanda_komponen' => 'required',
            'pengujian_pembebanan' => 'required',
            'jml_phb_utama' => 'required',
            'jml_phb_1fasa' => 'required',
            'jml_phb_3fasa' => 'required',
            'jml_phb_cabang' => 'required',
            'jml_saluran_cabang' => 'required',
            'jml_saluran_akhir' => 'required',
            'jml_titik_lampu' => 'required',
            'jml_sakelar' => 'required',
            'kkb' => 'required',
            'kkk' => 'required',
            'tahanan_isolasi_penghantar' => 'required',
            'resisten_pembumian' => 'required',
            'jml_motor_listrik_unit' => 'required',
            'jml_motor_listrik_kwh' => 'required',
            'catatan' => 'required',
            'foto1' => 'required',
            'foto2' => 'required',
            'foto3' => 'required',
            'foto4' => 'required',
            'location' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);

        $no_pemeriksaan = Rand(1000,9999);
        ini_set('gd.jpeg_ignore_warning', 1);
        $pemeriksa = new Pemeriksa;
            $pemeriksa->no_pendaftaran = $request->input('no_pendaftaran');
            $pemeriksa->no_surat_tugas = $request->input('no_surat_tugas');
            $pemeriksa->no_pemeriksaan = $no_pemeriksaan;
            $pemeriksa->no_lhpp = $request->input('no_lhpp');
            $pemeriksa->tgl_lhpp = $request->input('tgl_lhpp');
            $pemeriksa->gambar_instalasi = $request->input('gambar_instalasi');
            $pemeriksa->diagram_garis_tunggal = $request->input('diagram_garis_tunggal');
            $pemeriksa->pe_utama = $request->input('pe_utama');
            $pemeriksa->pe_cabang = $request->input('pe_cabang');
            $pemeriksa->pe_akhir = $request->input('pe_akhir');
            $pemeriksa->pe_kotak_kontak = $request->input('pe_kotak_kontak');
            $pemeriksa->jenis_peng_utama = $request->input('jenis_peng_utama');
            $pemeriksa->jenis_peng_cabang = $request->input('jenis_peng_cabang');
            $pemeriksa->jenis_peng_akhir = $request->input('jenis_peng_akhir');
            $pemeriksa->penghantar_bumi_jenis = $request->input('penghantar_bumi_jenis');
            $pemeriksa->penghantar_bumi_penampang = $request->input('penghantar_bumi_penampang');
            $pemeriksa->penghantar_bumi_sistem = $request->input('penghantar_bumi_sistem');
            $pemeriksa->saklar_utama = $request->input('saklar_utama');
            $pemeriksa->saklar_cabang1 = $request->input('saklar_cabang1');
            $pemeriksa->saklar_cabang2 = $request->input('saklar_cabang2');
            $pemeriksa->phbk_utama = $request->input('phbk_utama');
            $pemeriksa->phbk_cabang1 = $request->input('phbk_cabang1');
            $pemeriksa->phbk_cabang2 = $request->input('phbk_cabang2');
            $pemeriksa->penghantar_utama = $request->input('penghantar_utama');
            $pemeriksa->penghantar_cabang = $request->input('penghantar_cabang');
            $pemeriksa->penghantar_akhir = $request->input('penghantar_akhir');
            $pemeriksa->penghantar_3fasa = $request->input('penghantar_3fasa');
            $pemeriksa->fitting_lampu = $request->input('fitting_lampu');
            $pemeriksa->kotak_kontak = $request->input('kotak_kontak');
            $pemeriksa->sakelar = $request->input('sakelar');
            $pemeriksa->tinggi_kotak_kontak = $request->input('tinggi_kotak_kontak');
            $pemeriksa->tinggi_phbk = $request->input('tinggi_phbk');
            $pemeriksa->jenis_kotak_kontak = $request->input('jenis_kotak_kontak');
            $pemeriksa->tanda_komponen = $request->input('tanda_komponen');
            $pemeriksa->pengujian_pembebanan = $request->input('pengujian_pembebanan');
            $pemeriksa->jml_phb_utama = $request->input('jml_phb_utama');
            $pemeriksa->jml_phb_1fasa = $request->input('jml_phb_1fasa');
            $pemeriksa->jml_phb_3fasa = $request->input('jml_phb_3fasa');
            $pemeriksa->jml_phb_cabang = $request->input('jml_phb_cabang');
            $pemeriksa->jml_saluran_cabang = $request->input('jml_saluran_cabang');
            $pemeriksa->jml_saluran_akhir = $request->input('jml_saluran_akhir');
            $pemeriksa->jml_titik_lampu = $request->input('jml_titik_lampu');
            $pemeriksa->jml_sakelar = $request->input('jml_sakelar');
            $pemeriksa->kkk = $request->input('kkk');
            $pemeriksa->kkb = $request->input('kkb');
            $pemeriksa->tahanan_isolasi_penghantar = $request->input('tahanan_isolasi_penghantar');
            $pemeriksa->resisten_pembumian = $request->input('resisten_pembumian');
            $pemeriksa->jml_motor_listrik_unit = $request->input('jml_motor_listrik_unit');
            $pemeriksa->jml_motor_listrik_kwh = $request->input('jml_motor_listrik_kwh');
            $pemeriksa->catatan = $request->input('catatan');
            $pemeriksa->foto1 = $request->input('foto1');
            $pemeriksa->foto2 = $request->input('foto2');
            $pemeriksa->foto3 = $request->input('foto3');
            $pemeriksa->foto4 = $request->input('foto4');

             if ($request->hasFile('foto1')) {
                $foto1 = $request->file('foto1');
                $nomor1 = Rand(1000, 9999);
                $filename = time() . '.' . $nomor1 . '.' . $foto1->getClientOriginalExtension();
                $location = public_path('/images/pemeriksa/' . $filename);
                // Image::make($foto1)->save($location);
                $pemeriksa->foto1 = '/images/pemeriksa/'.$filename;
              }

              if ($request->hasFile('foto2')) {
                $foto2 = $request->file('foto2');
                $nomor2 = Rand(1000, 9999);
                $filename = time() . '.' . $nomor2 . '.' . $foto2->getClientOriginalExtension();
                $location = public_path('/images/pemeriksa/' . $filename);
                // Image::make($foto2)->save($location);
                $pemeriksa->foto2 = '/images/pemeriksa/'.$filename;
              }

              if ($request->hasFile('foto3')) {
                $foto3 = $request->file('foto3');
                $nomor3 = Rand(1000, 9999);
                $filename = time() . '.' . $nomor3 . '.' . $foto3->getClientOriginalExtension();
                $location = public_path('/images/pemeriksa/' . $filename);
                // Image::make($foto3)->save($location);
                $pemeriksa->foto3 = '/images/pemeriksa/'.$filename;
              }


              if ($request->hasFile('foto4')) {
                $foto4 = $request->file('foto4');
                $nomor4 = Rand(1000, 9999);
                $filename = time() . '.' . $nomor4 . '.' . $foto4->getClientOriginalExtension();
                $location = public_path('/images/pemeriksa/' . $filename);
                // Image::make($foto4)->save($location);
                $pemeriksa->foto4 = '/images/pemeriksa/'.$filename;
              }

            $pemeriksa->location = $request->input('location');
            $pemeriksa->lat = $request->input('lat');
            $pemeriksa->lng = $request->input('lng');
            $pemeriksa->status_verifikasi = '0';

        $pemeriksa->save();

        if ($pemeriksa->save()) {
          $id_pelanggan = $request['id_pelanggan'];
          $temp = PelangganTemp::where('id_pelanggan', '=', $id_pelanggan)->update([
            'status_periksa' => '1'
          ]);

          $log = LogStatus::where('no_pendaftaran', '=', $request['no_pendaftaran']);

          $log->update([
            'status' => 'Telah diperiksa. Proses verifikasi',
            'kode_status' => '2',
          ]);

          return response()->json([
            'msg' => 'Data berhasil Ditambah',
          ]);
        } else {
          return response()->json([
            'msg' => 'Gagal',
          ],402);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($no_pendaftaran)
    {
      return DB::table('pelanggan')
        ->join('tarif', 'tarif.id', '=', 'pelanggan.id_tarif')
        ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
        ->join('btl', 'btl.id', '=', 'pelanggan.id_btl')
        ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
        ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
        ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
        ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
        ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
        ->select('pelanggan.no_pendaftaran','pelanggan.nama','pemeriksa.no_lhpp',
            'pelanggan.alamat','tarif.jenis_tarif','daya.daya','btl.nama_btl','surat_tugas.no_surat',
            'pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2', 'pemeriksa.tgl_lhpp',
            'pemeriksa.gambar_instalasi','pemeriksa.diagram_garis_tunggal','pemeriksa.pe_utama',
            'pemeriksa.pe_cabang','pemeriksa.pe_akhir','pemeriksa.pe_kotak_kontak','pemeriksa.jenis_peng_utama',
            'pemeriksa.jenis_peng_cabang','pemeriksa.jenis_peng_akhir','pemeriksa.penghantar_utama',
            'pemeriksa.penghantar_cabang','pemeriksa.penghantar_bumi_jenis','pemeriksa.saklar_cabang1',
            'pemeriksa.saklar_cabang2','pemeriksa.phbk_utama','pemeriksa.phbk_cabang1','pemeriksa.phbk_cabang2',
            'pemeriksa.penghantar_akhir','pemeriksa.saklar_utama','pemeriksa.tinggi_kotak_kontak',
            'pemeriksa.tinggi_phbk','pemeriksa.jenis_kotak_kontak','pemeriksa.jml_phb_cabang',
            'pemeriksa.penghantar_3fasa','pemeriksa.fitting_lampu','pemeriksa.kotak_kontak','pemeriksa.sakelar',
            'pemeriksa.jml_saluran_cabang','pemeriksa.jml_saluran_akhir','pemeriksa.jml_titik_lampu',
            'pemeriksa.jml_sakelar','pemeriksa.kkb','pemeriksa.kkk','pemeriksa.tahanan_isolasi_penghantar',
            'pemeriksa.penghantar_bumi_sistem','pemeriksa.resisten_pembumian','pemeriksa.catatan',
            'pemeriksa.foto1','pemeriksa.foto2','pemeriksa.foto3','pemeriksa.foto4',
            'pemeriksa.location','pemeriksa.lat','pemeriksa.lng','pemeriksa.penghantar_bumi_penampang',
            'pemeriksa.tanda_komponen','pemeriksa.pengujian_pembebanan','pemeriksa.jml_phb_utama',
            'pemeriksa.jml_phb_cabang','pemeriksa.jml_phb_1fasa','pemeriksa.jml_phb_3fasa',
            'pemeriksa.jml_motor_listrik_unit','pemeriksa.jml_motor_listrik_kwh')
        ->where([['pelanggan_temp.status_periksa', '=', '1'],
            ['pemeriksa.status_verifikasi', '=', '0'],
            ['pelanggan.no_pendaftaran', '=', $no_pendaftaran]])
        ->get();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $no_pendaftaran)
    {
      $this->validate($request,[
          'no_pendaftaran' => 'required',
          'no_surat_tugas' => 'required',
          // 'no_pemeriksaan' => 'required',
          'no_lhpp' => 'required',
          'tgl_lhpp' => 'required',
          'gambar_instalasi' => 'required',
          'diagram_garis_tunggal' => 'required',
          'pe_utama' => 'required',
          'pe_cabang' => 'required',
          'pe_akhir' => 'required',
          'pe_kotak_kontak' => 'required',
          'jenis_peng_utama' => 'required',
          'jenis_peng_cabang' => 'required',
          'jenis_peng_akhir' => 'required',
          'penghantar_bumi_jenis' => 'required',
          'penghantar_bumi_penampang' => 'required',
          'penghantar_bumi_sistem' => 'required',
          'saklar_utama' => 'required',
          'saklar_cabang1' => 'required',
          'saklar_cabang2' => 'required',
          'phbk_utama' => 'required',
          'phbk_cabang1' => 'required',
          'phbk_cabang2' => 'required',
          'penghantar_utama' => 'required',
          'penghantar_cabang' => 'required',
          'penghantar_akhir' => 'required',
          'penghantar_3fasa' => 'required',
          'fitting_lampu' => 'required',
          'kotak_kontak' => 'required',
          'sakelar' => 'required',
          'tinggi_kotak_kontak' => 'required',
          'tinggi_phbk' => 'required',
          'jenis_kotak_kontak' => 'required',
          'tanda_komponen' => 'required',
          'pengujian_pembebanan' => 'required',
          'jml_phb_utama' => 'required',
          'jml_phb_1fasa' => 'required',
          'jml_phb_3fasa' => 'required',
          'jml_phb_cabang' => 'required',
          'jml_saluran_cabang' => 'required',
          'jml_saluran_akhir' => 'required',
          'jml_titik_lampu' => 'required',
          'jml_sakelar' => 'required',
          'kkb' => 'required',
          'kkk' => 'required',
          'tahanan_isolasi_penghantar' => 'required',
          'resisten_pembumian' => 'required',
          'jml_motor_listrik_unit' => 'required',
          'jml_motor_listrik_kwh' => 'required',
          'catatan' => 'required',
      ]);

      $periksa = Pemeriksa::where('no_pendaftaran', '=', $no_pendaftaran)->update([
        'gambar_instalasi' => $request->input('gambar_instalasi'),
        'diagram_garis_tunggal' => $request->input('diagram_garis_tunggal'),
        'pe_utama' => $request->input('pe_utama'),
        'pe_cabang' => $request->input('pe_cabang'),
        'pe_akhir' => $request->input('pe_akhir'),
        'pe_kotak_kontak' => $request->input('pe_kotak_kontak'),
        'jenis_peng_utama' => $request->input('jenis_peng_utama'),
        'jenis_peng_cabang' => $request->input('jenis_peng_cabang'),
        'jenis_peng_akhir' => $request->input('jenis_peng_akhir'),
        'penghantar_bumi_jenis' => $request->input('penghantar_bumi_jenis'),
        'penghantar_bumi_penampang' => $request->input('penghantar_bumi_penampang'),
        'penghantar_bumi_sistem' => $request->input('penghantar_bumi_sistem'),
        'saklar_utama' => $request->input('saklar_utama'),
        'saklar_cabang1' => $request->input('saklar_cabang1'),
        'saklar_cabang2' => $request->input('saklar_cabang2'),
        'phbk_utama' => $request->input('phbk_utama'),
        'phbk_cabang1' => $request->input('phbk_cabang1'),
        'phbk_cabang2' => $request->input('phbk_cabang2'),
        'penghantar_utama' => $request->input('penghantar_utama'),
        'penghantar_cabang' => $request->input('penghantar_cabang'),
        'penghantar_akhir' => $request->input('penghantar_akhir'),
        'penghantar_3fasa' => $request->input('penghantar_3fasa'),
        'fitting_lampu' => $request->input('fitting_lampu'),
        'kotak_kontak' => $request->input('kotak_kontak'),
        'sakelar' => $request->input('sakelar'),
        'tinggi_kotak_kontak' => $request->input('tinggi_kotak_kontak'),
        'tinggi_phbk' => $request->input('tinggi_phbk'),
        'jenis_kotak_kontak' => $request->input('jenis_kotak_kontak'),
        'tanda_komponen' => $request->input('tanda_komponen'),
        'pengujian_pembebanan' => $request->input('pengujian_pembebanan'),
        'jml_phb_utama' => $request->input('jml_phb_utama'),
        'jml_phb_1fasa' => $request->input('jml_phb_1fasa'),
        'jml_phb_3fasa' => $request->input('jml_phb_3fasa'),
        'jml_phb_cabang' => $request->input('jml_phb_cabang'),
        'jml_saluran_cabang' => $request->input('jml_saluran_cabang'),
        'jml_saluran_akhir' => $request->input('jml_saluran_akhir'),
        'jml_titik_lampu' => $request->input('jml_titik_lampu'),
        'jml_sakelar' => $request->input('jml_sakelar'),
        'kkb' => $request->input('kkb'),
        'kkk' => $request->input('kkk'),
        'tahanan_isolasi_penghantar' => $request->input('tahanan_isolasi_penghantar'),
        'resisten_pembumian' => $request->input('resisten_pembumian'),
        'jml_motor_listrik_unit' => $request->input('jml_motor_listrik_unit'),
        'jml_motor_listrik_kwh' => $request->input('jml_motor_listrik_kwh'),
        'catatan' => $request->input('catatan'),
        'location' => $request->input('location'),
        'lat' => $request->input('lat'),
        'lng' => $request->input('lng'),
      ]);



      return response()->json([
        'msg' => 'Data berhasil diubah'
      ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
