<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\KantorWilayah;
use App\Provinsi;

use Auth;

class KWilayahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return KantorWilayah::with('provinsi')->orderBy('nomor','asc')->get();
    }

    public function loadWilayah(){
        return KantorWilayah::all();
    }

    public function loadProvinsi(){
        return Provinsi::orderBy('provinsi')->get();
    }

    public function getKanWil(){
      // $area = KantorArea::where('id_kwilayah', '=', request('id_kwilayah'))->get();
      //
      // return response()->json($area);

      $wilayah = KantorWilayah::where('kode_prov', '=', request('kode_prov'))->get();

      return response()->json($wilayah);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate( $request,[
            'nama_wilayah'=>'required|min:3',
            'general_manager'=>'required|min:5',
            'kode_prov'=>'required',
            'alamat'=>'required',
            'telp'=>'required',
            'email'=>'required'
        ]);

        return KantorWilayah::create([
            'nama_wilayah' => $request['nama_wilayah'],
            'general_manager' => $request['general_manager'],
            'kode_prov' => $request['kode_prov'],
            'alamat' => $request['alamat'],
            'telp' => $request['telp'],
            'email' => $request['email'],
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kantor = KantorWilayah::findOrFail($id);
        $this->validate( $request,[
            'nama_wilayah'=>'required|min:5',
            'general_manager'=>'required|min:5',
            'kode_prov'=>'required',
            'alamat'=>'required',
            'telp'=>'required',
            'email'=>'required'
        ]);

        $kantor->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kantor = KantorWilayah::findOrFail($id);

        $kantor->delete();
    }
}
