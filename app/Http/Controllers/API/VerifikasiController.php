<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Verifikasi;
use App\Pemeriksa;
use App\LogStatus;

class VerifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return "test";
        $staff = auth('api')->user();
        if ($staff->status == 'Aktif') {
          return DB::table('pelanggan')
          ->join('kota', 'kota.kode_kota', '=', 'pelanggan.kode_kota')
          ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'pelanggan.id_kwilayah')
          ->join('kantor_area', 'kantor_area.id', '=', 'pelanggan.id_karea')
          ->join('sub_area', 'sub_area.id', '=', 'pelanggan.id_subarea')
          ->join('btl', 'btl.id', '=', 'pelanggan.id_btl')
          ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
          ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
          ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
          ->select('pelanggan.no_pendaftaran', 'pelanggan.nama', 'kota.kota', 'kantor_wilayah.nama_wilayah',
              'kantor_area.nama as nama_area', 'sub_area.nama_subarea', 'pemeriksa.no_lhpp', 'btl.nama_btl')
          ->where([['pelanggan_temp.status_periksa', '=', '1'],
              ['pemeriksa.status_verifikasi', '=', '0'],['pelanggan.id_karea', '=', $staff->id_karea]])
          ->get();
        } else {
          return response()->json([
            'msg' => 'Akun anda tidak aktif',
          ], 401);
        }


    }

    public function sudahVerif(){
        $staff = auth('api')->user();
        //return $staff->status;
        if ($staff->status == 'Aktif') {
          if($staff->id_karea == 0){
            return DB::table('pelanggan')
          ->join('kota', 'kota.kode_kota', '=', 'pelanggan.kode_kota')
          ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'pelanggan.id_kwilayah')
          ->join('kantor_area', 'kantor_area.id', '=', 'pelanggan.id_karea')
          ->join('sub_area', 'sub_area.id', '=', 'pelanggan.id_subarea')
          ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
          ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
          ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
          ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
          ->join('btl', 'btl.id', '=', 'pelanggan.id_btl')
          ->select('verifikasi.id','pelanggan.no_pendaftaran', 'pelanggan.nama', 'kota.kota', 'kantor_wilayah.nama_wilayah',
              'kantor_area.nama as nama_area', 'sub_area.nama_subarea', 'pemeriksa.no_lhpp','verifikasi.created_at','verifikasi.hasil_pemeriksaan','btl.nama_btl')
          ->where([['pelanggan_temp.status_periksa', '=', '1'],
              ['pemeriksa.status_verifikasi', '=', '1']
              ])
          ->get();

          }else{
          return DB::table('pelanggan')
          ->join('kota', 'kota.kode_kota', '=', 'pelanggan.kode_kota')
          ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'pelanggan.id_kwilayah')
          ->join('kantor_area', 'kantor_area.id', '=', 'pelanggan.id_karea')
          ->join('sub_area', 'sub_area.id', '=', 'pelanggan.id_subarea')
          ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
          ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
          ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
          ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
          ->join('btl', 'btl.id', '=', 'pelanggan.id_btl')
          ->select('verifikasi.id','pelanggan.no_pendaftaran', 'pelanggan.nama', 'kota.kota', 'kantor_wilayah.nama_wilayah',
              'kantor_area.nama as nama_area', 'sub_area.nama_subarea', 'pemeriksa.no_lhpp','verifikasi.created_at','verifikasi.hasil_pemeriksaan','btl.nama_btl')
          ->where([['pelanggan_temp.status_periksa', '=', '1'],
              ['pemeriksa.status_verifikasi', '=', '1'],
              ['pelanggan.id_karea', '=', $staff->id_karea]
              ])
          ->get();
          //->toSql();
          }
        } else {
          return response()->json([
            'msg' => 'Akun anda tidak aktif',
          ], 401);
        }



    }

      public function filterVerif($tgl1,$tgl2){
        // return $tgl1.'-'.$tgl2;
        // $tgl1 = $request->input('tgl1');
        // $tgl2 = $request->input('tgl2');
        $laporan = DB::table('pelanggan')
        ->join('kota', 'kota.kode_kota', '=', 'pelanggan.kode_kota')
        ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'pelanggan.id_kwilayah')
        ->join('kantor_area', 'kantor_area.id', '=', 'pelanggan.id_karea')
        ->join('sub_area', 'sub_area.id', '=', 'pelanggan.id_subarea')
        ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
        ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
        ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
        ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->join('btl', 'btl.id', '=', 'pelanggan.id_btl')
        ->select('verifikasi.id','pelanggan.no_pendaftaran', 'pelanggan.nama', 'kota.kota', 'kantor_wilayah.nama_wilayah',
            'kantor_area.nama as nama_area', 'sub_area.nama_subarea', 'pemeriksa.no_lhpp','verifikasi.created_at','verifikasi.hasil_pemeriksaan','btl.nama_btl')
        ->where([['pelanggan_temp.status_periksa', '=', '1'],
            ['pemeriksa.status_verifikasi', '=', '1']])
        ->whereBetween('verifikasi.created_at',[$tgl1, $tgl2])
        ->paginate(10);

        return response()->json($laporan);
        //->toSql();

    }

    // public function filter($tgl1,$tgl2){
    //   $laporan = DB::table('pelanggan')
    //   ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
    //   ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
    //   ->join('sub_area','sub_area.id','=','id_subarea')
    //   ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
    //   ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
    //   ->select('pembayaran.id','pembayaran.no_kwitansi','pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
    //   ->orderBy('id','desc')
    //   ->where('pelanggan.status','=','Sudah Bayar')
    //   ->whereBetween('pembayaran.created_at',[$tgl1, $tgl2])
    //   ->paginate(10);

    //   return response()->json($laporan);
    // }

    public function prosesVerif($no_pendaftaran){
        return DB::table('pelanggan')
        ->join('tarif', 'tarif.id', '=', 'pelanggan.id_tarif')
        ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
        ->join('btl', 'btl.id', '=', 'pelanggan.id_btl')
        ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
        ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
        ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
        ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
        ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
        ->select('pelanggan.no_pendaftaran','pelanggan.nama','pemeriksa.no_lhpp',
            'pelanggan.alamat','tarif.jenis_tarif','daya.daya','btl.nama_btl','surat_tugas.no_surat',
            'pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2', 'pemeriksa.tgl_lhpp',
            'pemeriksa.gambar_instalasi','pemeriksa.diagram_garis_tunggal','pemeriksa.pe_utama',
            'pemeriksa.pe_cabang','pemeriksa.pe_akhir','pemeriksa.jenis_peng_utama','pemeriksa.jenis_peng_cabang',
            'pemeriksa.jenis_peng_akhir','pemeriksa.penghantar_utama','pemeriksa.penghantar_cabang',
            'pemeriksa.penghantar_akhir','pemeriksa.saklar_utama','pemeriksa.tinggi_kotak_kontak',
            'pemeriksa.tinggi_phbk','pemeriksa.jenis_kotak_kontak','pemeriksa.jml_phb_cabang',
            'pemeriksa.jml_saluran_cabang','pemeriksa.jml_saluran_akhir','pemeriksa.jml_titik_lampu',
            'pemeriksa.jml_sakelar','pemeriksa.kkb','pemeriksa.kkk','pemeriksa.tahanan_isolasi_penghantar',
            'pemeriksa.penghantar_bumi_sistem','pemeriksa.resisten_pembumian','pemeriksa.catatan',
            'pemeriksa.foto1','pemeriksa.foto2','pemeriksa.foto3','pemeriksa.foto4',
            'pemeriksa.location','pemeriksa.lat','pemeriksa.lng','pemeriksa.penghantar_bumi_penampang')
        ->where([['pelanggan_temp.status_periksa', '=', '1'],
            ['pemeriksa.status_verifikasi', '=', '0'],
            ['pelanggan.no_pendaftaran', '=', $no_pendaftaran]])
        ->get();
    }

    public function getEditVerif($no_pendaftaran){
        return DB::table('pelanggan')
        ->join('tarif', 'tarif.id', '=', 'pelanggan.id_tarif')
        ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
        ->join('btl', 'btl.id', '=', 'pelanggan.id_btl')
        ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
        ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
        ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
        ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
        ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
        ->select('pelanggan.no_pendaftaran','pelanggan.nama','pemeriksa.no_lhpp',
            'pelanggan.alamat','tarif.jenis_tarif','daya.daya','btl.nama_btl','surat_tugas.no_surat',
            'pemeriksa1.nama as pemeriksa1','pemeriksa2.nama as pemeriksa2', 'pemeriksa.tgl_lhpp',
            'pemeriksa.gambar_instalasi','pemeriksa.diagram_garis_tunggal','pemeriksa.pe_utama',
            'pemeriksa.pe_cabang','pemeriksa.pe_akhir','pemeriksa.jenis_peng_utama','pemeriksa.jenis_peng_cabang',
            'pemeriksa.jenis_peng_akhir','pemeriksa.penghantar_utama','pemeriksa.penghantar_cabang',
            'pemeriksa.penghantar_akhir','pemeriksa.saklar_utama','pemeriksa.tinggi_kotak_kontak',
            'pemeriksa.tinggi_phbk','pemeriksa.jenis_kotak_kontak','pemeriksa.jml_phb_cabang',
            'pemeriksa.jml_saluran_cabang','pemeriksa.jml_saluran_akhir','pemeriksa.jml_titik_lampu',
            'pemeriksa.jml_sakelar','pemeriksa.kkb','pemeriksa.kkk','pemeriksa.tahanan_isolasi_penghantar',
            'pemeriksa.penghantar_bumi_sistem','pemeriksa.resisten_pembumian','pemeriksa.catatan',
            'pemeriksa.foto1','pemeriksa.foto2','pemeriksa.foto3','pemeriksa.foto4',
            'pemeriksa.location','pemeriksa.lat','pemeriksa.lng','pemeriksa.penghantar_bumi_penampang')
        ->where([['pelanggan_temp.status_periksa', '=', '1'],
            ['pemeriksa.status_verifikasi', '=', '1'],
            ['pelanggan.no_pendaftaran', '=', $no_pendaftaran]])
        ->get();
    }

    public function dataEditVerif($no_pendaftaran){
        return Verifikasi::
        where('no_pendaftaran', '=', $no_pendaftaran)
        ->get();
    }

    public function updateVerifikasi(Request $request, $no_pendaftaran){
        $this->validate($request,[
            'no_pendaftaran' => 'required',
            'no_lhpp' => 'required',
            'pemeriksaan_dokumen' => 'required',
            'proteksi_sentuh_langung' => 'required',
            'pemeriksaan_penghantar' => 'required',
            'pemeriksaan_phb' => 'required',
            'pemeriksaan_elektroda' => 'required',
            'pemeriksaan_polaritas' => 'required',
            'pemeriksaan_pemasangan' => 'required',
            'pemeriksaan_sni' => 'required',
            'pemeriksaan_kamar_mandi' => 'required',
            'catatan' => 'required',
            'hasil_pemeriksaan' => 'required',
            'ceklist_ppu' => 'sometimes',
            'hasil_verifikasi' => 'required',
            'alasan_reject' => 'sometimes',
        ]);

        $verifikasi = Verifikasi::where('no_pendaftaran', '=', $no_pendaftaran)->update([
          'no_pendaftaran' => $request->input('no_pendaftaran'),
          'no_lhpp' => $request->input('no_lhpp'),
          'pemeriksaan_dokumen' => $request->input('pemeriksaan_dokumen'),
          'proteksi_sentuh_langung' => $request->input('proteksi_sentuh_langung'),
          'pemeriksaan_penghantar' =>  $request->input('pemeriksaan_penghantar'),
          'pemeriksaan_phb' => $request->input('pemeriksaan_phb'),
          'pemeriksaan_elektroda' => $request->input('pemeriksaan_elektroda'),
          'pemeriksaan_polaritas' => $request->input('pemeriksaan_polaritas'),
          'pemeriksaan_pemasangan' => $request->input('pemeriksaan_pemasangan'),
          'pemeriksaan_sni' => $request->input('pemeriksaan_sni'),
          'pemeriksaan_kamar_mandi' => $request->input('pemeriksaan_kamar_mandi'),
          'catatan' => $request->input('catatan'),
          'hasil_pemeriksaan' => $request->input('hasil_pemeriksaan'),
          'hasil_verifikasi' => $request->input('hasil_verifikasi'),
          'alasan_reject' => $request->input('alasan_reject'),

        ]);


      return response()->json([
        'msg' => 'Data berhasil diubah'
      ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $staff = auth('api')->user();
     
        $this->validate($request,[
            'no_pendaftaran' => 'required',
            'no_lhpp' => 'required',
            'pemeriksaan_dokumen' => 'required',
            'proteksi_sentuh_langung' => 'required',
            'pemeriksaan_penghantar' => 'required',
            'pemeriksaan_phb' => 'required',
            'pemeriksaan_elektroda' => 'required',
            'pemeriksaan_polaritas' => 'required',
            'pemeriksaan_pemasangan' => 'required',
            'pemeriksaan_sni' => 'required',
            'pemeriksaan_kamar_mandi' => 'required',
            'catatan' => 'required',
            'hasil_pemeriksaan' => 'required',
            'ceklist_ppu1' => 'sometimes',
            'ceklist_ppu2' => 'sometimes',
            'hasil_verifikasi' => 'required',
            'alasan_reject' => 'sometimes',

        ]);

        $verifikasi = new Verifikasi;
        $verifikasi->no_pendaftaran = $request->input('no_pendaftaran');
        $verifikasi->no_lhpp = $request->input('no_lhpp');
        $verifikasi->pemeriksaan_dokumen = $request->input('pemeriksaan_dokumen');
        $verifikasi->proteksi_sentuh_langung = $request->input('proteksi_sentuh_langung');
        $verifikasi->pemeriksaan_penghantar =  $request->input('pemeriksaan_penghantar');
        $verifikasi->pemeriksaan_phb = $request->input('pemeriksaan_phb');
        $verifikasi->pemeriksaan_elektroda = $request->input('pemeriksaan_elektroda');
        $verifikasi->pemeriksaan_polaritas = $request->input('pemeriksaan_polaritas');
        $verifikasi->pemeriksaan_pemasangan = $request->input('pemeriksaan_pemasangan');
        $verifikasi->pemeriksaan_sni = $request->input('pemeriksaan_sni');
        $verifikasi->pemeriksaan_kamar_mandi = $request->input('pemeriksaan_kamar_mandi');
        $verifikasi->catatan = $request->input('catatan');
        $verifikasi->hasil_pemeriksaan = $request->input('hasil_pemeriksaan');

        $Iya = $request->input('Iya');
        $Tidak = $request->input('Tidak');
        if($Iya == 1 && $Tidak == 2){
              $verifikasi->ceklist_ppu1 = ' Penghantar Proteksi PE tidak ada pada sirkit cabang';
              $verifikasi->ceklist_ppu2 = ' Penghantar proteksi PE tidak dihubungkan pada kotak kontak';
        }else if($Iya == 1){
             $verifikasi->ceklist_ppu1 = ' Penghantar Proteksi PE tidak ada pada sirkit cabang';
             $verifikasi->ceklist_ppu2 = '';


        }else if($Tidak == 2){
             $verifikasi->ceklist_ppu1 = '';
             $verifikasi->ceklist_ppu2 = 'Penghantar proteksi PE tidak dihubungkan pada kotak kontak';

        }else{
             $verifikasi->ceklist_ppu1 = '';
             $verifikasi->ceklist_ppu2 = '';
        };


        $hasil_verif = $request->input('hasil_verifikasi');
        if($hasil_verif == 'approve'){
           $verifikasi->hasil_verifikasi = $hasil_verif;
           $verifikasi->alasan_reject = '';
       }else{
           $verifikasi->hasil_verifikasi = $hasil_verif;
           $verifikasi->alasan_reject = $request->input('alasan_reject');
       }


        $verifikasi->id_staff = $staff->id;
        $verifikasi->status_sertifikasi = '0';

        $verifikasi->save();

        if($verifikasi->save()){
          $statusVerif = $request['no_pendaftaran'];
          $periksa = Pemeriksa::where('no_pendaftaran', '=', $statusVerif)->update([
            'status_verifikasi' => '1'
          ]);

          $log = LogStatus::where('no_pendaftaran', '=', $request['no_pendaftaran']);

          $log->update([
            'status' => 'Telah diverifikasi. Proses penerbitan SLO',
            'kode_status' => '3',
          ]);

          return response()->json([
            'msg' => 'Data berhasil Ubah',
          ]);
        } else {
          return response()->json([
            'msg' => 'Gagal',
          ],402);
        }


        // return response()->json([
        //   'msg' => 'Data berhasil Ditambah',
        // ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Verifikasi::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
