<?php

namespace App\Http\Controllers\API;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Pelanggan;
use App\PelangganTemp;
use App\SuratTugas;
use App\Perangkat;

use App\User;

class SuratTugasController extends Controller
{

    public function suratKu(){
      $staff = auth('api')->user()->id;
      if (auth('api')->user()->departemen == 'Pemeriksa') {
        $surat = SuratTugas::with('pemeriksa1', 'pemeriksa2')->where(
          [['id_pemeriksa1', '=', $staff],['status', '=', 'Belum']]
        )->orWhere([['id_pemeriksa2', '=', $staff],['status', '=', 'Belum']])->latest()->get();

        $arraySurat =  SuratTugas::with('pemeriksa1', 'pemeriksa2')->where(
          [['id_pemeriksa1', '=', $staff],['status', '=', 'Belum']]
        )->orWhere([['id_pemeriksa2', '=', $staff],['status', '=', 'Belum']])->latest()->get();

        foreach ($arraySurat as $value) {
          if (Carbon::parse($value->updated_at)->addMinutes(1)->isPast()) {
            $jml = $value->no_surat;

            $notification = Perangkat::where('id_users', '=', $value->id_pemeriksa1)->orWhere('id_users', '=', $value->id_pemeriksa2)->pluck('token')->toArray();

            foreach ($notification as $key => $values) {
              $this->sendWarning($values, $jml);
            }
          }
        }



        return response()->json($surat);
      }
    }

    public function sendWarning($token, $body){
      $optionBuilder = new OptionsBuilder();
      $optionBuilder->setTimeToLive(60*20);

      $notificationBuilder = new PayloadNotificationBuilder('SLO SIP');
      $notificationBuilder->setTitle('SLO SIP')
                          ->setBody('Surat tugas dengan nomor ' . $body . ' belum dikerjakan. Harap segera dikerjakan')
                          ->setSound('default');

      $dataBuilder = new PayloadDataBuilder();
      $dataBuilder->addData(['a_data' => 'my_data']);

      $option = $optionBuilder->build();
      $notification = $notificationBuilder->build();
      $data = $dataBuilder->build();

      $tokens = $token;

      $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

      $downstreamResponse->numberSuccess();
      $downstreamResponse->numberFailure();
      $downstreamResponse->numberModification();
    }

    public function kirimNotif($id){
      $surat = SuratTugas::with('pemeriksa1', 'pemeriksa2')->findOrFail($id);

      $token = Perangkat::where('id_users', '=', $surat->id_pemeriksa1)->orWhere('id_users', '=', $surat->id_pemeriksa2)->pluck('token')->toArray();

      $optionBuilder = new OptionsBuilder();
      $optionBuilder->setTimeToLive(60*20);

      $notificationBuilder = new PayloadNotificationBuilder('SLO SIP');
      $notificationBuilder->setTitle('SLO SIP')
                          ->setBody('Surat tugas dengan nomor ' . $surat->no_surat . ' belum dikerjakan. Harap segera dikerjakan sebelum tanggal ' . $surat->jangka_waktu)
                          ->setSound('default');

      $dataBuilder = new PayloadDataBuilder();
      $dataBuilder->addData(['a_data' => 'my_data']);

      $option = $optionBuilder->build();
      $notification = $notificationBuilder->build();
      $data = $dataBuilder->build();

      $tokens = $token;

      $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

      $downstreamResponse->numberSuccess();
      $downstreamResponse->numberFailure();
      $downstreamResponse->numberModification();
    }

    public function detailSurat($id){

      $pelanggan = DB::table('surat_tugas')
                       ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
                       ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
                       ->join('pelanggan_temp', 'pelanggan_temp.no_surat', '=', 'surat_tugas.no_surat')
                       ->join('pelanggan', 'pelanggan.id', '=', 'pelanggan_temp.id_pelanggan')
                       ->join('tarif', 'tarif.id', '=', 'pelanggan.id_tarif')
                       ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
                       ->join('penyedia_listrik', 'penyedia_listrik.id', 'pelanggan.id_penyedia')
                       ->join('btl', 'btl.id', 'pelanggan.id_btl')
                       ->select('surat_tugas.id', 'surat_tugas.no_surat', 'pemeriksa1.nama as pemeriksa1', 'pemeriksa2.nama as pemeriksa2', 'pelanggan.no_pendaftaran', 'pelanggan.nama', 'pelanggan.alamat',
                       'tarif.jenis_tarif','daya.daya', 'penyedia_listrik.nama_penyedia', 'btl.nama_btl')
                       ->where([['surat_tugas.id', '=', $id],['pelanggan_temp.status_periksa', '=', '0']])
                       ->get();

      $jml = $pelanggan->count();

        //return response()->json([
        //    'pelanggan' => $pelanggan,
        //    'jml' => $jml,
        //]);
        return json_encode(
            array(
                'pelanggan' => $pelanggan,
                'jml' => $jml,
            )
        );
    }



    public function selesaikan($id){
      $surat = SuratTugas::find($id);

      $surat->update([
        'status' => 'Sudah'
      ]);

      return response()->json([
        'message' => 'Tugas diselesaikan'
      ]);
    }

    public function sudahSelesai(){
      $staff = auth('api')->user()->id;
      if (auth('api')->user()->departemen == 'Pemeriksa') {
        $surat = SuratTugas::with('pemeriksa1', 'pemeriksa2')->where(
          [['id_pemeriksa1', '=', $staff],['status', '=', 'Sudah']]
        )->orWhere([['id_pemeriksa2', '=', $staff],['status', '=', 'Sudah']])->latest()->get();

        //return response()->json($surat);
        return json_encode(array($surat));
      }
    }

    public function sudahDetail($id){
      $pelanggan = DB::table('surat_tugas')
                       ->join('users as pemeriksa1', 'pemeriksa1.id', '=', 'surat_tugas.id_pemeriksa1')
                       ->join('users as pemeriksa2', 'pemeriksa2.id', '=', 'surat_tugas.id_pemeriksa2')
                       ->join('pelanggan_temp', 'pelanggan_temp.no_surat', '=', 'surat_tugas.no_surat')
                       ->join('pelanggan', 'pelanggan.id', '=', 'pelanggan_temp.id_pelanggan')
                       ->join('tarif', 'tarif.id', '=', 'pelanggan.id_tarif')
                       ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
                       ->join('penyedia_listrik', 'penyedia_listrik.id', 'pelanggan.id_penyedia')
                       ->join('btl', 'btl.id', 'pelanggan.id_btl')
                       ->select('surat_tugas.id', 'surat_tugas.no_surat', 'pemeriksa1.nama as pemeriksa1', 'pemeriksa2.nama as pemeriksa2', 'pelanggan.no_pendaftaran', 'pelanggan.nama', 'pelanggan.alamat',
                       'tarif.jenis_tarif','daya.daya', 'penyedia_listrik.nama_penyedia', 'btl.nama_btl')
                       ->where([['surat_tugas.id', '=', $id],['pelanggan_temp.status_periksa', '=', '1']])
                       ->get();

        //return response()->json([
        //    'pelanggan' => $pelanggan
        //]);
        return json_encode(
            array(
                'pelanggan' => $pelanggan
            )
        );
    }



    public function belumTerbit(){
      $staff = auth('api')->user();
      if (auth('api')->user()->jabatan == 'Admin Pusat' || auth('api')->user()->departemen == 'Manajer Area') {
          return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl', 'penyedia')->where([
              ['status', '=', 'Sudah Bayar'],
              ['status_temp', '=', '0']])
              ->orderBy('id','asc')->get();
      }
      if (auth('api')->user()->departemen == 'Admin Wilayah') {
          return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl', 'penyedia')->where([
              ['id_kwilayah', '=', $staff->id_kwilayah],
              ['status', '=', 'Sudah Bayar'],
              ['status_temp', '=', '0']])
              ->orderBy('id','asc')->get();
      }
      if (auth('api')->user()->departemen == 'Admin Area') {

          return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl', 'penyedia')->where([
              ['id_karea', '=', $staff->id_karea],
              ['status', '=', 'Sudah Bayar'],
              ['status_temp', '=', '0']])
              ->orderBy('id','asc')->get();
          }
      if (auth('api')->user()->departemen == 'Admin Sub Area') {
          return Pelanggan::with('pembayaran','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl')->where([
              ['id_subarea', '=', $staff->id_subarea],
              ['status', '=', 'Sudah Bayar'],
              ['status_temp', '=', '0']])
              ->orderBy('id','asc')->get();
      }
    }



    public function loadSurat(){
      $staff = auth('api')->user();
      // return SuratTugas::with('pemeriksa1', 'pemeriksa2')->where([
      //   ['id_staff', '=', $staff->id]
      // ])->paginate(10);
      if ($staff->jabatan == 'Admin Pusat' || $staff->departemen == 'Manajer Area') {
        return DB::table('surat_tugas')
                   ->join('users as pemeriksa1', 'pemeriksa1.id', 'surat_tugas.id_pemeriksa1')
                   ->join('users as pemeriksa2', 'pemeriksa2.id', 'surat_tugas.id_pemeriksa2')
                   ->join('pelanggan_temp', 'pelanggan_temp.no_surat', 'surat_tugas.no_surat')
                   ->join('pelanggan', 'pelanggan.id', 'pelanggan_temp.id_pelanggan')
                   ->select('surat_tugas.id','surat_tugas.no_surat', 'pemeriksa1.nama as nama_pemeriksa1', 'pemeriksa2.nama as nama_pemeriksa2','surat_tugas.updated_at','surat_tugas.jangka_waktu')
                   ->where('surat_tugas.status', '=', 'Belum')
                   ->get();
      }
      if ($staff->departemen == 'Admin Wilayah') {
        return DB::table('surat_tugas')
                   ->join('users as pemeriksa1', 'pemeriksa1.id', 'surat_tugas.id_pemeriksa1')
                   ->join('users as pemeriksa2', 'pemeriksa2.id', 'surat_tugas.id_pemeriksa2')
                   ->join('pelanggan_temp', 'pelanggan_temp.no_surat', 'surat_tugas.no_surat')
                   ->join('pelanggan', 'pelanggan.id', 'pelanggan_temp.id_pelanggan')
                   ->select('surat_tugas.id','surat_tugas.no_surat', 'pemeriksa1.nama as nama_pemeriksa1', 'pemeriksa2.nama as nama_pemeriksa2','surat_tugas.updated_at','surat_tugas.jangka_waktu')
                   ->where([['surat_tugas.status', '=', 'Belum'],['pemeriksa1.id_kwilayah', '=', $staff->id_kwilayah]])
                   ->orWhere([['surat_tugas.status', '=', 'Belum'],['pemeriksa2.id_kwilayah', '=', $staff->id_kwilayah]])
                   ->get();
      }
      if ($staff->departemen == 'Admin Area') {
        return DB::table('surat_tugas')
                   ->join('users as pemeriksa1', 'pemeriksa1.id', 'surat_tugas.id_pemeriksa1')
                   ->join('users as pemeriksa2', 'pemeriksa2.id', 'surat_tugas.id_pemeriksa2')
                   ->join('pelanggan_temp', 'pelanggan_temp.no_surat', 'surat_tugas.no_surat')
                   ->join('pelanggan', 'pelanggan.id', 'pelanggan_temp.id_pelanggan')
                   ->select('surat_tugas.id','surat_tugas.no_surat', 'pemeriksa1.nama as nama_pemeriksa1', 'pemeriksa2.nama as nama_pemeriksa2','surat_tugas.updated_at','surat_tugas.jangka_waktu')
                   ->where([['surat_tugas.status', '=', 'Belum'],['pemeriksa1.id_karea', '=', $staff->id_karea]])
                   ->orWhere([['surat_tugas.status', '=', 'Belum'],['pemeriksa2.id_karea', '=', $staff->id_karea]])
                   ->get();
      }
      if ($staff->departemen == 'Admin Sub Area') {
        return DB::table('surat_tugas')
                   ->join('users as pemeriksa1', 'pemeriksa1.id', 'surat_tugas.id_pemeriksa1')
                   ->join('users as pemeriksa2', 'pemeriksa2.id', 'surat_tugas.id_pemeriksa2')
                   ->join('pelanggan_temp', 'pelanggan_temp.no_surat', 'surat_tugas.no_surat')
                   ->join('pelanggan', 'pelanggan.id', 'pelanggan_temp.id_pelanggan')
                   ->select('surat_tugas.id','surat_tugas.no_surat', 'pemeriksa1.nama as nama_pemeriksa1', 'pemeriksa2.nama as nama_pemeriksa2','surat_tugas.updated_at','surat_tugas.jangka_waktu')
                   ->where([['surat_tugas.status', '=', 'Belum'],['pemeriksa1.id_subarea', '=', $staff->id_subarea]])
                   ->orWhere([['surat_tugas.status', '=', 'Belum'],['pemeriksa2.id_subarea', '=', $staff->id_subarea]])
                   ->get();
      }
    }

    public function tambahSurat(Request $request, $id){
      $staff = auth('api')->user()->id;
      $pelanggan = Pelanggan::findOrFail($id);
      //
      if (!$pelanggan) {
        return response()->json([
          'msg' => 'Data tidak ditemukan',
        ],404);
      }

      // $temp = DB::table('pelanggan_temp')->where('id_pelanggan', '=', $id)->get();

      $temp = PelangganTemp::where('id_staff', '=', $staff)->get();

      if (count($temp) > 20) {

        //return response()->json([
        //  'msg' => 'Data melebihi batas',
        //]);
        return json_encode(
            array(
                'msg' => 'Data melebihi batas'        
            )
        );


      } else {
        $newTemp = new PelangganTemp;

        $newTemp->id_pelanggan = $id;
        $newTemp->id_tarif = $pelanggan->id_tarif;
        $newTemp->id_daya = $pelanggan->id_daya;
        $newTemp->id_penyedia = $pelanggan->id_penyedia;
        $newTemp->id_subarea = $pelanggan->id_subarea;
        $newTemp->id_staff = $staff;
        $newTemp->status_terbit = '0';
        $newTemp->save();

        $pelanggan->status_temp = '1';
        $pelanggan->save();

        return response()->json([
          'msg' => 'Berhasil'
        ]);
      }
    }

    public function countTemp(){
      $staff = auth('api')->user()->id;
      return PelangganTemp::where([['id_staff', '=', $staff],
      ['status_terbit', '=', '0']])->count();
    }

    public function temp(){
      $staff = auth('api')->user()->id;
      return PelangganTemp::with('pelanggan', 'tarif', 'daya', 'penyedia')->where([['id_staff', '=', $staff],
      ['status_terbit', '=', '0']])->get();
    }

    public function deleteTemp($id){
      $staff = auth('api')->user()->id;

      $temp = PelangganTemp::where('id_pelanggan', '=', $id);

      $pelanggan = Pelanggan::findOrFail($id);
      $pelanggan->status_temp = '0';

      $pelanggan->update();

      $temp->delete();
    }

    public function terbitkanSurat(Request $request){
      $noTerakhir = SuratTugas::max('no_surat');
      $nomor = (int) substr($noTerakhir, 0, 14);
      $nomor++;

      $bulanRomawi = array("", "I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $no = 1;

      if ($noTerakhir) {
        $noSurat = sprintf("%05s", abs($nomor)) . '/' . $bulanRomawi[date('n')] . '/' .date('Y');
      } else {
        $noSurat = sprintf("%05s", abs($no)) . '/' . $bulanRomawi[date('n')] . '/' .date('Y');
      }


      $staff = auth('api')->user()->id;
      $departemen = auth('api')->user()->departemen;
      $temp = PelangganTemp::with('pelanggan', 'tarif', 'daya', 'penyedia')->where([['id_staff', '=', $staff],
      ['status_terbit', '=', '0']])->get();

      $id_pemeriksa2 = $request['id_pemeriksa2'];
      if($departemen == "Manajer Area" && $request['id_pemeriksa2'] == null){
        $id_pemeriksa2 = $request['id_pemeriksa1'];
      }
      if (count($temp) == 0) {
        return response()->json([
          'msg' => 'Tambahkan pelanggan terlebih dahulu'
        ], 404);
     } else {
       SuratTugas::create([
         'no_surat' => $noSurat,
         'id_pemeriksa1' => $request['id_pemeriksa1'],
         'id_pemeriksa2' => $id_pemeriksa2,
         'id_staff' => $staff,
         'status' => 'Belum',
         'jangka_waktu' => Carbon::now()->addDays(2),
       ]);

       PelangganTemp::where([
         ['id_staff', '=', $staff],
         ['status_terbit', '=', '0']
       ])->update([
         'no_surat' => $noSurat,
         'status_terbit' => '1',
         'status_periksa' => '0'
       ]);


       $notification = Perangkat::where('id_users', '=', $request['id_pemeriksa1'])->orWhere('id_users', '=', $request['id_pemeriksa2'])->pluck('token')->toArray();

       foreach ($notification as $key => $value) {
          $this->notification($value, $noSurat);
       }


      return response()->json([
        'msg' => 'Data berhasil disimpan',
      ]);
     }
    }

    public function notification($token, $body){

      $optionBuilder = new OptionsBuilder();
      $optionBuilder->setTimeToLive(60*20);

      $notificationBuilder = new PayloadNotificationBuilder('SLO SIP');
      $notificationBuilder->setTitle('SLO SIP')
                          ->setBody('Surat tugas dengan nomor ' . $body . ' telah diterbitkan untuk Anda')
                          ->setSound('default');

      $dataBuilder = new PayloadDataBuilder();
      $dataBuilder->addData(['a_data' => 'my_data']);

      $option = $optionBuilder->build();
      $notification = $notificationBuilder->build();
      $data = $dataBuilder->build();

      $tokens = $token;

      $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

      $downstreamResponse->numberSuccess();
      $downstreamResponse->numberFailure();
      $downstreamResponse->numberModification();
    }

    public function test(){
      // $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
      // $token='daW4LfbQvgg:APA91bFeT3oOvTWpK9ZcEF-JnVzhZl85EMyHfmborZHPJNi68GjySo3LgG4stPFwZDSLr75gFpjoMuneUPM2BLZiu1cQi__fO7XgPB2PJB4RvAMmze6qwcJ6F_d2n4Dnzxvcx7CiMCUu';
      //
      //
      // $notification = [
      //     'body' => 'this is test',
      //     'sound' => true,
      // ];
      //
      // $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];
      //
      // $fcmNotification = [
      //     //'registration_ids' => $tokenList, //multple token array
      //     'to'        => $token, //single token
      //     'notification' => $notification,
      //     'data' => $extraNotificationData
      // ];
      //
      // $headers = [
      //     'Authorization: key=AIzaSyAyrVJJQbBq9pZecBJtZKGTHKMqataCCh8',
      //     'Content-Type: application/json'
      // ];
      //
      //
      // $ch = curl_init();
      // curl_setopt($ch, CURLOPT_URL,$fcmUrl);
      // curl_setopt($ch, CURLOPT_POST, true);
      // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
      // $result = curl_exec($ch);
      // curl_close($ch);
      //
      //
      // return response()->json($notification);

       return Perangkat::where('id_users', '=', '4')->orWhere('id_users', '=', '6')->pluck('token')->toArray();

    }


}
