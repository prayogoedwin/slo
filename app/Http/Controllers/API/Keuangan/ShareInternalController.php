<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\ShareInternal;

class ShareInternalController extends Controller
{
    public function index(){
      $dt = Carbon::now();
      // $internal = DB::table('share_internal')
      //                 ->join('alokasi_share', 'alokasi_share.kode_alokasi', '=', 'share_internal.kode_alokasi')
      //                 ->select('share_internal.periode','alokasi_share.pendapatan_kotor', 'alokasi_share.alokasi_pengembangan', 'alokasi_share.jumlah_share',
      //                 'share_internal.nama', 'share_internal.persen', 'share_internal.jumlah')
      //                 ->orderBy('share_internal.periode', 'asc')
      //                 ->get();

      $internal = DB::table('alokasi_share')
                      ->select(DB::raw("kode_alokasi, periode, pendapatan_kotor, alokasi_pengembangan, jumlah_share"))
                      ->get();
      return response()->json($internal);
    }

    public function detail($kode_alokasi){
      $internal = DB::table('share_internal')
                      ->select(DB::raw("share_internal.nama, share_internal.persen, share_internal.jumlah"))
                      ->where('kode_alokasi', '=', $kode_alokasi)
                      ->get();

      return response()->json($internal);
    }
}
