<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\RekapArea;
use App\TagihanArea;
class RekapAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $staff = auth('api')->user();
      return RekapArea::where('id_karea', '=', $staff->id_karea)->orderBy('bulan', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $dt = Carbon::now();
      $staff = auth('api')->user();
      $rekap = DB::table('rekap_area')
                   ->select('bulan', 'total')
                   ->where('id_karea', '=', $staff->id_karea)
                   ->whereMonth('bulan', '=', $dt->month)
                   ->whereYear('bulan', '=', $dt->year)
                   ->count();

      if (!$rekap) {

        $rekapbaru = new RekapArea();
        $rekapbaru->id_kwilayah = $staff->id_kwilayah;
        $rekapbaru->id_karea = $staff->id_karea;
        $rekapbaru->bulan = $dt;
        $rekapbaru->pendapatan_pelanggan = $request->total;
        $rekapbaru->pendapatan_sub_area = $request->pendapatan_sub_area;
        $rekapbaru->total = $rekapbaru->pendapatan_sub_area + $rekapbaru->pendapatan_pelanggan;

        $rekapbaru->save();

        if ($rekapbaru->save()) {
          $huruf = "KA";
          $nomor = rand(1000, 9999);

          $no_ref = $huruf . $nomor;

          $tagihanbaru = new TagihanArea();
          $tagihanbaru->no_ref = $no_ref;
          $tagihanbaru->id_kwilayah = $staff->id_kwilayah;
          $tagihanbaru->id_karea = $staff->id_karea;
          $tagihanbaru->periode = $dt;
          $tagihanbaru->jml_tagihan = $rekapbaru->total;
          $tagihanbaru->jml_terbayar = 0;
          $tagihanbaru->jml_hutang = $rekapbaru->total;

          $tagihanbaru->save();

          return response()->json([
            'msg' => 'Berhasil',
          ],200);
        } else {
          return response()->json([
            'msg' => 'Gagal',
          ],401);
        }
      } else {
        $rekap1 = DB::table('rekap_area')
                     ->select('id','bulan', 'total')
                     ->where('id_karea', '=', $staff->id_karea)
                     ->whereMonth('bulan', '=', $dt->month)
                     ->whereYear('bulan', '=', $dt->year)
                     ->first();

        $rekapupdate = RekapArea::findOrFail($rekap1->id);
        $rekapupdate->pendapatan_pelanggan = $request->total;
        $rekapupdate->total = $rekapupdate->pendapatan_pelanggan + $rekapupdate->pendapatan_sub_area;

        $rekapupdate->save();

        if ($rekapupdate->save()) {
          $tagihan = TagihanArea::where('id_karea', '=', $staff->id_karea)
                                  ->whereMonth('periode', '=', $dt->month)
                                  ->whereYear('periode', '=', $dt->year)
                                  ->first();

          $tagihanupdate = TagihanArea::findOrFail($tagihan->id);
          $tagihanupdate->jml_tagihan = $rekapupdate->total;
          $tagihanupdate->jml_hutang = $rekapupdate->total - $tagihanupdate->jml_terbayar;
          $tagihanupdate->save();

          return response()->json([
            'msg' => 'Berhasil',
          ],200);
        } else {
          return response()->json([
            'msg' => 'Gagal',
          ],401);
        }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
