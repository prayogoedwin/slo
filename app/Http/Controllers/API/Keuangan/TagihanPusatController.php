<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\TagihanWilayah;
use Illuminate\Support\Facades\DB;
use App\SetorWilayah;

class TagihanPusatController extends Controller
{
    public function index(){
      $dt = Carbon::now();
      $tagihan = DB::table('tagihan_wilayah')
                    ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'tagihan_wilayah.id_kwilayah')
                    ->join('share_wilayah', 'share_wilayah.id', '=', 'tagihan_wilayah.id_share')
                    ->select(DB::raw("kantor_wilayah.nama_wilayah, tagihan_wilayah.periode, share_wilayah.share,
                    tagihan_wilayah.rp_share, tagihan_wilayah.jml_tagihan, tagihan_wilayah.jml_terbayar, tagihan_wilayah.jml_hutang"))
                    ->whereMonth('tagihan_wilayah.periode', '=', $dt->month)
                    ->get();

      return response()->json($tagihan);
    }

    public function totalTagihan(){
      $dt = Carbon::now();
      $totaltagihan = DB::table('tagihan_wilayah')
                          ->select(DB::raw("SUM(jml_hutang) as sisa_tagihan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"))
                          ->whereMonth('tagihan_wilayah.periode', '=', $dt->month)
                          ->get();

      return response()->json($totaltagihan);
    }

    public function filterTagihan($tgl1, $tgl2){
      $tagihan = DB::table('tagihan_wilayah')
                    ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'tagihan_wilayah.id_kwilayah')
                    ->join('share_wilayah', 'share_wilayah.id', '=', 'tagihan_wilayah.id_share')
                    ->select(DB::raw("kantor_wilayah.nama_wilayah, tagihan_wilayah.periode, share_wilayah.share,
                    tagihan_wilayah.rp_share, tagihan_wilayah.jml_tagihan, tagihan_wilayah.jml_terbayar, tagihan_wilayah.jml_hutang"))
                    ->whereBetween('tagihan_wilayah.periode', [$tgl1, $tgl2])
                    ->get();

        return response()->json($tagihan);
    }

    public function filterTotalTagihan($tgl1, $tgl2){
      $staff = auth('api')->user();
      $totaltagihan = DB::table('tagihan_wilayah')
                          ->select(DB::raw("SUM(jml_hutang) as sisa_tagihan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"))
                          ->whereBetween('periode', [$tgl1, $tgl2])
                          ->get();

      return response()->json($totaltagihan);
    }

}
