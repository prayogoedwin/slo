<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\TagihanSubArea;
use App\SetorSubArea;
use App\TagihanArea;
use App\RekapArea;

class TagihanSubAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function tagihanQ(){
      $dt = Carbon::now();
      $staff = auth('api')->user();
      $tagihan = DB::table('tagihan_sub_area')
                     ->select('jml_tagihan', 'jml_terbayar', 'jml_hutang')
                     ->whereMonth('periode', '=', $dt->month)
                     ->whereYear('periode', '=', $dt->year)
                     ->where('id_subarea', '=', $staff->id_subarea)
                     ->get();

      return response()->json($tagihan);
    }

    public function index()
    {
        $staff = auth('api')->user();
        $tagihan = DB::table('tagihan_sub_area')
                       ->join('rekap_sub_area', 'rekap_sub_area.id_subarea', '=', 'tagihan_sub_area.id_subarea')
                       ->select('tagihan_sub_area.id','tagihan_sub_area.no_ref','tagihan_sub_area.periode', 'rekap_sub_area.total as jml_penerimaan', 'tagihan_sub_area.jml_tagihan', 'tagihan_sub_area.jml_terbayar', 'tagihan_sub_area.jml_hutang', 'tagihan_sub_area.updated_at')
                       ->where('tagihan_sub_area.id_subarea', '=', $staff->id_subarea)
                       ->get();

        return response()->json($tagihan);
    }

    public function totalTagihan(){
      $staff = auth('api')->user();
      $totaltagihan = DB::table('tagihan_sub_area')
                          ->select(DB::raw("SUM(jml_hutang) as sisa_tagihan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"))
                          ->where('id_subarea', '=', $staff->id_subarea)
                          ->get();

      return response()->json($totaltagihan);
    }

    public function filterTagihan($tgl1, $tgl2){
      $staff = auth('api')->user();
      $tagihan = DB::table('tagihan_sub_area')
                     ->join('rekap_sub_area', 'rekap_sub_area.id_subarea', '=', 'tagihan_sub_area.id_subarea')
                     ->select('tagihan_sub_area.periode', 'rekap_sub_area.total as jml_penerimaan', 'tagihan_sub_area.jml_tagihan', 'tagihan_sub_area.jml_terbayar', 'tagihan_sub_area.jml_hutang', 'tagihan_sub_area.updated_at')
                     ->where('tagihan_sub_area.id_subarea', '=', $staff->id_subarea)
                     ->whereBetween('tagihan_sub_area.periode', [$tgl1, $tgl2])
                     ->get();

      return response()->json($tagihan);
    }

    public function filterTotalTagihan($tgl1, $tgl2){
      $staff = auth('api')->user();
      $totaltagihan = DB::table('tagihan_sub_area')
                          ->select(DB::raw("SUM(jml_hutang) as sisa_tagihan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"))
                          ->where('id_subarea', '=', $staff->id_subarea)
                          ->whereBetween('periode', [$tgl1, $tgl2])
                          ->get();

      return response()->json($totaltagihan);
    }

    public function cekPembayaran($id){
      $setoran = DB::table('setor_sub_area')
                     ->join('users', 'users.id', '=', 'setor_sub_area.id_users')
                     // ->select('setor_sub_area.jml_setoran', 'users.nama', 'setor_sub_area.created_at', DB::raw("CASE WHEN (setor_sub_area.status = 0) THEN 'Belum Konfirmasi' ELSE 'Terkonfirmasi' END"))
                     ->select(DB::raw("setor_sub_area.no_ref, setor_sub_area.jml_setoran, users.nama, setor_sub_area.created_at, (CASE WHEN (setor_sub_area.status = 0) THEN 'Belum Konfirmasi' WHEN (setor_sub_area.status = 1) THEN 'Terkonfirmasi' ELSE 'Ditolak' END) as keterangan"))
                     ->where('setor_sub_area.id_tagihan', '=', $id)
                     ->get();

      return response()->json($setoran);
    }

    public function cekTagihan($id){
      // $staff = auth('api')->user();
      $tagihan = DB::table('tagihan_sub_area')
                     ->join('sub_area', 'sub_area.id', '=', 'tagihan_sub_area.id_subarea')
                     ->select(DB::raw("tagihan_sub_area.no_ref, sub_area.nama_subarea, tagihan_sub_area.id, tagihan_sub_area.id_subarea, tagihan_sub_area.periode, tagihan_sub_area.jml_tagihan, tagihan_sub_area.jml_terbayar, tagihan_sub_area.jml_hutang, tagihan_sub_area.updated_at"))
                     ->where('tagihan_sub_area.id_subarea', '=', $id)
                     ->get();

      return response()->json($tagihan);
    }

    public function cekTotalTagihan($id){
      $tagihan = DB::table('tagihan_sub_area')
                     ->select(DB::raw("SUM(jml_terbayar) as jml_terbayarkan, SUM(jml_hutang) as jml_hutang"))
                     ->where('tagihan_sub_area.id_subarea', '=', $id)
                     ->get();

      return response()->json($tagihan);
    }

    public function totalSeluruhTagihan(){
      $staff = auth('api')->user();
      $totaltagihan = DB::table('tagihan_sub_area')
                          ->select(DB::raw("SUM(jml_tagihan) as total_pendapatan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"), DB::raw("sum(jml_hutang) as total_hutang"))
                          ->where('id_karea', '=', $staff->id_karea)
                          ->get();

      return response()->json($totaltagihan);
    }

    public function getSetoran(){
      $staff = auth('api')->user();
      $tagihan = DB::table('setor_sub_area')
                      ->join('tagihan_sub_area', 'tagihan_sub_area.id', '=', 'setor_sub_area.id_tagihan')
                      ->join('sub_area', 'sub_area.id', '=', 'tagihan_sub_area.id_subarea')
                      ->join('users', 'users.id', '=', 'setor_sub_area.id_users')
                      ->select('setor_sub_area.id','sub_area.nama_subarea', 'setor_sub_area.no_ref', 'setor_sub_area.jml_setoran', 'users.nama')
                      // ->where([['tagihan_sub_area.id_subarea', '=', $staff->id_subarea], ['setor_sub_area.status', '=', 0]])
                      ->where([['tagihan_sub_area.id_karea', '=', $staff->id_karea], ['setor_sub_area.status', '=', '0']])
                      ->get();

      return response()->json($tagihan);
    }

    public function konfirmSetoran($id){
      $setoran = SetorSubArea::findOrFail($id);

      $setoran->status = 1;
      $setoran->save();

      $staff = auth('api')->user();
      $dt = Carbon::now();

      if ($setoran->save()) {
        $tagihan = TagihanSubArea::where('id', '=', $setoran->id_tagihan)->first();

        $tagihan->jml_terbayar = $setoran->jml_setoran + $tagihan->jml_terbayar;
        $tagihan->jml_hutang = $tagihan->jml_hutang - $setoran->jml_setoran;
        $tagihan->save();

        if ($tagihan->save()) {
          $rekapArea = RekapArea::where('id_karea', '=', $staff->id_karea)
                                  ->whereMonth('bulan', '=', $dt->month)
                                  ->whereYear('bulan', '=', $dt->year)
                                  ->first();

          $rekapArea->total = $rekapArea->pendapatan_pelanggan + $rekapArea->pendapatan_sub_area;

          $rekapArea->save();
          if ($rekapArea->save()) {
            $tagihanArea = TagihanArea::where('id_karea', '=', $staff->id_karea)
                                        ->whereMonth('periode', '=', $dt->month)
                                        ->whereYear('periode', '=', $dt->year)
                                        ->first();

            $tagihanArea->jml_tagihan = $rekapArea->total;
            $tagihanArea->jml_hutang = $rekapArea->total;

            $tagihanArea->save();
            return response()->json([
              'msg' => 'Berhasil'
            ], 200);
          } else {
            return response()->json([
              'msg' => 'Gagal'
            ], 401);
          }


        } else {
          return response()->json([
            'msg' => 'Gagal'
          ], 401);
        }

      } else {
        return response()->json([
          'msg' => 'Gagal'
        ], 401);
      }

    }

    public function tolakSetoran($id){
      $setoran = SetorSubArea::findOrFail($id);

      $setoran->status = 2;
      $setoran->save();


        return response()->json([
          'msg' => 'Berhasil'
        ], 200);
    }

    public function loadSubArea(){
      $staff = auth('api')->user();
      $subarea = DB::table('sub_area')
                     ->select(DB::raw("id, nama_subarea, alamat, nama_manager, status"))
                     ->where('id_karea', '=', $staff->id_karea)
                     ->get();

      return response()->json($subarea);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $staff = auth('api')->user();

        $this->validate($request, [
          'jml_setoran' => 'required',
        ]);

        $tagihan = TagihanSubArea::findOrFail($id);

        $setoran = new SetorSubArea();
        $setoran->id_tagihan = $tagihan->id;
        $setoran->no_ref = $tagihan->no_ref;
        $setoran->jml_setoran = $request['jml_setoran'];
        $setoran->id_users = $staff->id;
        $setoran->status = 0;

        $setoran->save();

        return response()->json([
          'msg' => 'Berhasil'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'jml_setoran' => 'required',
        ]);

        $tagihan = TagihanSubArea::findOrFail($id);
        $hutang = $tagihan->jml_hutang - $request['jml_setoran'];
        $terbayar = $tagihan->jml_terbayar + $request['jml_setoran'];
        $tagihan->jml_terbayar = $terbayar;
        $tagihan->jml_hutang = $hutang;

        $tagihan->save();



        return response()->json([
          'msg' => 'Berhasil'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
