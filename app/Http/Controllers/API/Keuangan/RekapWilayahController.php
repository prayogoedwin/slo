<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\RekapWilayah;
use App\TagihanWilayah;
class RekapWilayahController extends Controller
{

    public function index(){
      $staff = auth('api')->user();

      return RekapWilayah::where('id_kwilayah', '=', $staff->id_kwilayah)->get();
    }
    public function penerimaan(){
      $staff = auth('api')->user();

      $dt = Carbon::now();

      return DB::table('setor_area')
             ->join('tagihan_area', 'tagihan_area.id', '=', 'setor_area.id_tagihan')
             ->where([['tagihan_area.id_kwilayah', '=', $staff->id_kwilayah],['setor_area.status', '=', '1']])
             ->whereMonth('setor_area.updated_at', '=', $dt->month)
             ->whereYear('setor_area.updated_at', '=', $dt->year)
             ->count();
    }

    public function totalPendapatan(){
      $staff = auth('api')->user();
      $dt = Carbon::now();

      $pemasukan = DB::table('setor_area')
                      ->join('tagihan_area', 'tagihan_area.id', '=', 'setor_area.id_tagihan')
                      ->select(DB::raw("SUM(setor_area.jml_setoran) as pendapatan"))
                      ->where([['tagihan_area.id_kwilayah', '=', $staff->id_kwilayah],['setor_area.status', '=', '1']])
                      ->whereMonth('setor_area.updated_at', '=', $dt->month)
                      ->whereYear('setor_area.updated_at', '=', $dt->year)
                      ->get();

      return response()->json($pemasukan);
    }

    public function filter($tgl1, $tgl2){
      $staff = auth('api')->user();

      $pemasukan = DB::table('setor_area')
                      ->join('tagihan_area', 'tagihan_area.id', '=', 'setor_area.id_tagihan')
                      ->select(DB::raw("SUM(setor_area.jml_setoran) as pendapatan"))
                      ->where([['tagihan_area.id_kwilayah', '=', $staff->id_kwilayah],['setor_area.status', '=', '1']])
                      ->whereBetween('setor_area.updated_at', [$tgl1,$tgl2])
                      ->get();

      return response()->json($pemasukan);
    }

    public function hitungPenerimaan($tgl1,$tgl2){
        $staff = auth('api')->user();
        return DB::table('setor_area')
               ->join('tagihan_area', 'tagihan_area.id', '=', 'setor_area.id_tagihan')
               ->where([['tagihan_area.id_kwilayah', '=', $staff->id_kwilayah],['setor_area.status', '=', '1']])
               ->whereBetween('setor_area.updated_at', [$tgl1,$tgl2])
               ->count();
    }

    public function dataPenerimaan(){
      $staff = auth('api')->user();
      $dt = Carbon::now();

      $pemasukan = DB::table('setor_area')
                      ->join('tagihan_area', 'tagihan_area.id', '=', 'setor_area.id_tagihan')
                      ->join('kantor_area', 'kantor_area.id', '=', 'tagihan_area.id_karea')
                      ->join('users', 'users.id', '=', 'setor_area.id_users')
                      ->select('setor_area.id','kantor_area.nama as nama_area', 'setor_area.no_ref', 'setor_area.jml_setoran',
                      'users.nama', 'setor_area.updated_at')
                      ->where([['tagihan_area.id_kwilayah', '=', $staff->id_kwilayah],['setor_area.status', '=', '1']])
                      ->whereMonth('setor_area.updated_at', '=', $dt->month)
                      ->whereYear('setor_area.updated_at', '=', $dt->year)
                      ->get();

      return response()->json($pemasukan);
    }

    public function FilterDataPenerimaan($tgl1,$tgl2){
      $staff = auth('api')->user();

      $pemasukan = DB::table('setor_area')
                      ->join('tagihan_area', 'tagihan_area.id', '=', 'setor_area.id_tagihan')
                      ->join('kantor_area', 'kantor_area.id', '=', 'tagihan_area.id_karea')
                      ->join('users', 'users.id', '=', 'setor_area.id_users')
                      ->select('setor_area.id','kantor_area.nama as nama_area', 'setor_area.no_ref', 'setor_area.jml_setoran',
                      'users.nama', 'setor_area.updated_at')
                      ->where([['tagihan_area.id_kwilayah', '=', $staff->id_kwilayah],['setor_area.status', '=', '1']])
                      ->whereBetween('setor_area.updated_at', [$tgl1,$tgl2])
                      ->get();

      return response()->json($pemasukan);
    }

    public function tagihanArea(){
      $staff = auth('api')->user();
      $dt = Carbon::now();

      $tagihan = DB::table('tagihan_area')
                    ->select(DB::raw("SUM(jml_tagihan) as total_tagihan"))
                    ->where('id_kwilayah', '=', $staff->id_kwilayah)
                    ->whereMonth('periode', '=', $dt->month)
                    ->whereYear('periode', '=', $dt->year)
                    ->get();

      return response()->json($tagihan);
    }

    public function store(Request $request){
      $dt = Carbon::now();
      $staff = auth('api')->user();

      $rekap = DB::table('rekap_wilayah')
                    ->select('id', 'periode', 'total_penerimaan')
                    ->where('id_kwilayah', '=', $staff->id_kwilayah)
                    ->whereMonth('periode', '=', $dt->month)
                    ->whereYear('periode', '=', $dt->year)
                    ->first();


      if (!$rekap) {

        $rekapbaru = new RekapWilayah();

        $rekapbaru->id_kwilayah = $staff->id_kwilayah;
        $rekapbaru->periode = $dt;
        $rekapbaru->total_penerimaan = $request->total;

        $rekapbaru->save();

        if ($rekapbaru->save()) {
          $huruf = "WL";
          $nomor = rand(1000, 9999);

          $no_ref = $huruf . $nomor;
          $persen = $request->share / 100;

          $tagihanbaru = new TagihanWilayah ();
          $tagihanbaru->no_ref = $no_ref;
          $tagihanbaru->id_kwilayah = $staff->id_kwilayah;
          $tagihanbaru->periode = $dt;
          $tagihanbaru->id_share = $request->id_share;
          $tagihanbaru->rp_share = $request->tagihan_area * $persen;
          $tagihanbaru->jml_tagihan = $request->tagihan_area;
          $tagihanbaru->jml_terbayar = 0;
          $tagihanbaru->jml_hutang = $request->tagihan_area;

          $tagihanbaru->save();
        }

        return response()->json([
          'msg' => 'Berhasil',
        ],200);

      } else {
        $rekapupdate = RekapWilayah::findOrFail($rekap->id);

        $rekapupdate->total_penerimaan = $request->total;
        $rekapupdate->save();
        if ($rekapupdate->save()) {
          $persen = $request->share / 100;
          $tagihan = TagihanWilayah::where('id_kwilayah', '=', $staff->id_kwilayah)
                                  ->whereMonth('periode', '=', $dt->month)
                                  ->whereYear('periode', '=', $dt->year)
                                  ->first();

          $tagihanupdate = TagihanWilayah::findOrFail($tagihan->id);
          $tagihanupdate->jml_tagihan = $request->tagihan_area;
          $tagihanupdate->rp_share = $request->tagihan_area * $persen;
          $tagihanupdate->jml_hutang = $request->tagihan_area - $tagihanupdate->jml_terbayar;

          $tagihanupdate->save();
        }
        return response()->json([
          'msg' => 'Berhasil Update',
        ],200);
      }


    }

    public function getShareWilayah(){
        $staff = auth('api')->user();

        $sharewilayah = DB::table('share_wilayah')
                    ->select(DB::raw("id, share"))
                    ->where('id_kwilayah', '=', $staff->id_kwilayah)
                    ->get();

        return response()->json($sharewilayah);
    }
}
