<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\RekapSubArea;
use App\TagihanSubArea;

class RekapSubAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = auth('api')->user();
        return RekapSubArea::where('id_subarea', '=', $staff->id_subarea)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $dt = Carbon::now();
        $staff = auth('api')->user();
        $rekap = DB::table('rekap_sub_area')
                     ->select('bulan', 'total')
                     ->where('id_subarea', '=', $staff->id_subarea)
                     ->whereMonth('bulan', '=', $dt->month)
                     ->whereYear('bulan', '=', $dt->year)
                     ->count();

        if (!$rekap) {
          $rekapbaru = new RekapSubArea();
          $rekapbaru->id_karea = $staff->id_karea;
          $rekapbaru->id_subarea = $staff->id_subarea;
          $rekapbaru->bulan = $dt;
          $rekapbaru->total = $request->total;

          $rekapbaru->save();

          if ($rekapbaru->save()) {
            $huruf = "SA";
            $nomor = rand(1000, 9999);

            $no_ref = $huruf . $nomor;

            $tagihanbaru = new TagihanSubArea();
            $tagihanbaru->id_karea = $staff->id_karea;
            $tagihanbaru->id_subarea = $staff->id_subarea;
            $tagihanbaru->periode = $dt;
            $tagihanbaru->jml_tagihan = $request->total;
            $tagihanbaru->jml_terbayar = 0;
            $tagihanbaru->jml_hutang = $request->total;
            $tagihanbaru->no_ref = $no_ref;

            $tagihanbaru->save();

            return response()->json([
              'msg' => 'Berhasil',
            ],200);
          } else {
            return response()->json([
              'msg' => 'Gagal',
            ],401);
          }

        } else {
          $rekap1 = DB::table('rekap_sub_area')
                       ->select('id','bulan', 'total')
                       ->where('id_subarea', '=', $staff->id_subarea)
                       ->whereMonth('bulan', '=', $dt->month)
                       ->whereYear('bulan', '=', $dt->year)
                       ->first();

          $rekapupdate = RekapSubArea::findOrFail($rekap1->id);
          $rekapupdate->bulan = $dt;
          $rekapupdate->total = $request->total;

          $rekapupdate->save();

          if ($rekapupdate->save()) {
            $tagihan = DB::table('tagihan_sub_area')
                         ->select('id','periode', 'jml_tagihan', 'jml_hutang', 'jml_terbayar')
                         ->where('id_subarea', '=', $staff->id_subarea)
                         ->whereMonth('periode', '=', $dt->month)
                         ->whereYear('periode', '=', $dt->year)
                         ->first();

            // $jml_hutang = $tagihan->jml_tagihan - $tagihan->jml_terbayar;

            $tagihanupdate = TagihanSubArea::findOrFail($tagihan->id);
            $tagihanupdate->jml_tagihan = $request->total;
            $jml_hutang = $request->total - $tagihanupdate->jml_terbayar;
            $tagihanupdate->jml_hutang = $jml_hutang;

            $tagihanupdate->save();
            return response()->json([
              'msg' => 'Berhasil',
            ],200);
          } else {
            return response()->json([
              'msg' => 'Gagal',
            ],401);
          }

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
