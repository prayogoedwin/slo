<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\ShareWilayah;

class ShareWilayahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $share = DB::table('share_wilayah')
                      ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'share_wilayah.id_kwilayah')
                      ->select(DB::raw("share_wilayah.id, share_wilayah.id_kwilayah, kantor_wilayah.nama_wilayah, share_wilayah.share"))
                      ->orderBy('share_wilayah.id', 'ASC')
                      ->get();

        return response()->json($share);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
         'id_kwilayah' => 'required|unique:share_wilayah',
         'share' => 'required|numeric',
       ]);

       $share = new ShareWilayah();

       $share->id_kwilayah = $request['id_kwilayah'];
       $share->share = $request['share'];

       $share->save();

       return response()->json([
         'msg' => 'Berhasil',
       ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $share = ShareWilayah::findOrFail($id);

        $this->validate($request, [
          'id_kwilayah' => 'required|unique:share_wilayah,id_kwilayah,'.$share->id,
          'share' => 'required|numeric',
        ]);

        $share->share = $request['share'];

        $share->save();

        return response()->json([
          'msg' => 'Berhasil',
        ],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
