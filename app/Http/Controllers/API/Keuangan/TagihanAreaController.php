<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\TagihanArea;
use Illuminate\Support\Facades\DB;
use App\SetorArea;

class TagihanAreaController extends Controller
{
    public function index(){
      $staff = auth('api')->user();
      $tagihan = DB::table('tagihan_area')
                     ->join('rekap_area', 'rekap_area.id_karea', '=', 'tagihan_area.id_karea')
                     ->select('tagihan_area.id','tagihan_area.no_ref','tagihan_area.periode', 'rekap_area.pendapatan_pelanggan as jml_penerimaan',
                     'rekap_area.pendapatan_sub_area','tagihan_area.jml_tagihan', 'tagihan_area.jml_terbayar', 'tagihan_area.jml_hutang', 'tagihan_area.updated_at')
                     ->where('tagihan_area.id_karea', '=', $staff->id_karea)
                     ->get();

      return response()->json($tagihan);
    }

    public function cekTagihan($id){
      // $staff = auth('api')->user();
      $tagihan = DB::table('tagihan_area')
                     ->join('kantor_area', 'kantor_area.id', '=', 'tagihan_area.id_karea')
                     ->join('rekap_area', 'rekap_area.id_karea', '=', 'tagihan_area.id_karea')
                     ->select('kantor_area.nama','tagihan_area.id','tagihan_area.no_ref','tagihan_area.periode', 'rekap_area.pendapatan_pelanggan as jml_penerimaan',
                     'rekap_area.pendapatan_sub_area','tagihan_area.jml_tagihan', 'tagihan_area.jml_terbayar', 'tagihan_area.jml_hutang', 'tagihan_area.updated_at')
                     ->where('tagihan_area.id_karea', '=', $id)
                     ->get();

      return response()->json($tagihan);
    }

    public function cekTotalTagihan($id){
      $tagihan = DB::table('tagihan_area')
                     ->select(DB::raw("SUM(jml_terbayar) as jml_terbayarkan, SUM(jml_hutang) as jml_hutang"))
                     ->where('tagihan_area.id_karea', '=', $id)
                     ->get();

      return response()->json($tagihan);
    }

    public function getSetoran(){
      $staff = auth('api')->user();
      $tagihan = DB::table('setor_area')
                      ->join('tagihan_area', 'tagihan_area.id', '=', 'setor_area.id_tagihan')
                      ->join('kantor_area', 'kantor_area.id', '=', 'tagihan_area.id_karea')
                      ->join('users', 'users.id', '=', 'setor_area.id_users')
                      ->select('setor_area.id','kantor_area.nama as nama_area', 'setor_area.no_ref', 'setor_area.jml_setoran', 'users.nama')
                      // ->where([['tagihan_area.id_karea', '=', $staff->id_karea], ['setor_area.status', '=', 0]])
                      ->where([['tagihan_area.id_karea', '=', $staff->id_karea], ['setor_area.status', '=', '0']])
                      ->get();

      return response()->json($tagihan);
    }

    public function totalTagihan(){
      $staff = auth('api')->user();
      $totaltagihan = DB::table('tagihan_area')
                          ->select(DB::raw("SUM(jml_hutang) as sisa_tagihan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"))
                          ->where('id_karea', '=', $staff->id_karea)
                          ->get();

      return response()->json($totaltagihan);
    }

    public function konfirmSetoran($id){
      $setoran = SetorArea::findOrFail($id);
      $setoran->status = 1;
      $setoran->save();

      $staff = auth('api')->user();
      $dt = Carbon::now();

      if ($setoran->save()) {
        $tagihan = TagihanArea::where('id', '=', $setoran->id_tagihan)->first();

        $tagihan->jml_terbayar = $setoran->jml_setoran + $tagihan->jml_terbayar;
        $tagihan->jml_hutang = $tagihan->jml_hutang - $setoran->jml_setoran;
        $tagihan->save();

        return response()->json([
          'msg' => 'Berhasil'
        ], 200);

      } else {
        return response()->json([
          'msg' => 'Gagal'
        ], 401);
      }
    }

    public function totalSeluruhTagihan(){
      $staff = auth('api')->user();
      $totaltagihan = DB::table('tagihan_area')
                          ->select(DB::raw("SUM(jml_tagihan) as total_pendapatan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"), DB::raw("sum(jml_hutang) as total_hutang"))
                          ->where('id_kwilayah', '=', $staff->id_kwilayah)
                          ->get();

      return response()->json($totaltagihan);
    }

    public function filterTagihan($tgl1, $tgl2){
      $staff = auth('api')->user();
      $tagihan = DB::table('tagihan_area')
                    ->join('rekap_area', 'rekap_area.id_karea', '=', 'tagihan_area.id_karea')
                    ->select('tagihan_area.id','tagihan_area.no_ref','tagihan_area.periode', 'rekap_area.pendapatan_pelanggan as jml_penerimaan',
                    'rekap_area.pendapatan_sub_area','tagihan_area.jml_tagihan', 'tagihan_area.jml_terbayar', 'tagihan_area.jml_hutang', 'tagihan_area.updated_at')
                    ->where('tagihan_area.id_karea', '=', $staff->id_karea)
                     ->whereBetween('tagihan_area.periode', [$tgl1, $tgl2])
                     ->get();

      return response()->json($tagihan);
    }

    public function filterTotalTagihan($tgl1, $tgl2){
      $staff = auth('api')->user();
      $totaltagihan = DB::table('tagihan_area')
                          ->select(DB::raw("SUM(jml_hutang) as sisa_tagihan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"))
                          ->where('id_karea', '=', $staff->id_karea)
                          ->whereBetween('periode', [$tgl1, $tgl2])
                          ->get();

      return response()->json($totaltagihan);
    }

    public function loadArea(){
      $staff = auth('api')->user();

      $area = DB::table('kantor_area')
                  ->select(DB::raw("id, nama"))
                  ->where('id_kwilayah', '=', $staff->id_kwilayah)
                  ->get();

      return response()->json($area);
    }


    public function store(Request $request, $id){
      $staff = auth('api')->user();

      $this->validate($request, [
        'jml_setoran' => 'required',
      ]);

      $tagihan = TagihanArea::findOrFail($id);

      $setoran = new SetorArea();
      $setoran->id_tagihan = $tagihan->id;
      $setoran->no_ref = $tagihan->no_ref;
      $setoran->jml_setoran = $request['jml_setoran'];
      $setoran->id_users = $staff->id;
      $setoran->status = 0;

      $setoran->save();

      return response()->json([
        'msg' => 'Berhasil'
      ],200);
    }

    public function cekPembayaran($id){
      $setoran = DB::table('setor_area')
                     ->join('users', 'users.id', '=', 'setor_area.id_users')
                     // ->select('setor_area.jml_setoran', 'users.nama', 'setor_area.created_at', DB::raw("CASE WHEN (setor_area.status = 0) THEN 'Belum Konfirmasi' ELSE 'Terkonfirmasi' END"))
                     ->select(DB::raw("setor_area.no_ref, setor_area.jml_setoran, users.nama, setor_area.created_at, (CASE WHEN (setor_area.status = 0) THEN 'Belum Konfirmasi' WHEN (setor_area.status = 1) THEN 'Terkonfirmasi' ELSE 'Ditolak' END) as keterangan"))
                     ->where('setor_area.id_tagihan', '=', $id)
                     ->get();

      return response()->json($setoran);
    }

}
