<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pembayaran;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PemasukanController extends Controller
{
    public function totalPendapatan(){
      $staff = auth('api')->user();
      $dt = Carbon::now();
      if ($staff->departemen == "Admin Area") {
        $pemasukan = DB::table('pembayaran')
                         ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                         ->select(DB::raw("SUM(pembayaran.jumlah) as pendapatan"))
                         ->where([['pelanggan.id_karea', '=', $staff->id_karea],['pembayaran.tipe', '=', 'Area']])
                         ->whereMonth('pembayaran.tanggal', '=', $dt->month)
                         ->get();

        return response()->json($pemasukan);
      }
      if ($staff->departemen == "Admin Sub Area") {
        $pemasukan = DB::table('pembayaran')
                         ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                         ->select(DB::raw("SUM(pembayaran.jumlah) as pendapatan"))
                         ->where([['pelanggan.id_subarea', '=', $staff->id_subarea],['pembayaran.tipe', '=', 'Sub Area']])
                         ->whereMonth('pembayaran.tanggal', '=', $dt->month)
                         ->get();


        return response()->json($pemasukan);
      }

    }

    public function pendapatanSubArea(){
        $staff = auth('api')->user();
        $dt = Carbon::now();

        $pendapatan = DB::table('tagihan_sub_area')
                          ->select(DB::raw("SUM(jml_tagihan) as pendapatan_sub_area"))
                          ->where('id_karea', '=', $staff->id_karea)
                          ->whereMonth('periode', '=', $dt->month)
                          ->whereYear('periode', '=', $dt->year)
                          ->get();

        return response()->json($pendapatan);
    }

    public function filter($tgl1, $tgl2){
      $staff = auth('api')->user();
      if ($staff->departemen == "Admin Area") {
        $pemasukan = DB::table('pembayaran')
                      ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                      ->select(DB::raw("SUM(pembayaran.jumlah) as pendapatan"))
                      ->where([['pelanggan.id_karea', '=', $staff->id_karea],['pembayaran.tipe', '=', 'Area']])
                      ->whereBetween('pembayaran.tanggal', [$tgl1,$tgl2])
                      ->get();
        // dd($query);
        return response()->json($pemasukan);
      }
      if ($staff->departemen == "Admin Sub Area") {
        $pemasukan = DB::table('pembayaran')
                      ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                      ->select(DB::raw("SUM(pembayaran.jumlah) as pendapatan"))
                      ->where([['pelanggan.id_subarea', '=', $staff->id_subarea],['pembayaran.tipe', '=', 'Sub Area']])
                      // ->whereMonth('pembayaran.tanggal', '=', $tgl1)
                      ->whereBetween('pembayaran.tanggal', [$tgl1,$tgl2])
                      ->get();

        return response()->json($pemasukan);
      }

    }

    public function hitungPenerimaan($tgl1, $tgl2){
      $staff = auth('api')->user();
      if ($staff->departemen == "Admin Area") {
        return DB::table('pembayaran')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                   ->where([['pelanggan.id_karea', '=', $staff->id_karea],['pembayaran.tipe', '=', 'Area']])
                   // ->whereMonth('pembayaran.tanggal', '=', $tgl1)
                   ->whereBetween('pembayaran.tanggal', [$tgl1,$tgl2])
                   ->count();
      }
      if ($staff->departemen == "Admin Sub Area") {
        return DB::table('pembayaran')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                   ->where([['pelanggan.id_subarea', '=', $staff->id_subarea],['pembayaran.tipe', '=', 'Sub Area']])
                   // ->whereMonth('pembayaran.tanggal', '=', $tgl1)
                   ->whereBetween('pembayaran.tanggal', [$tgl1,$tgl2])
                   ->count();
      }
    }

    public function penerimaan(){
      $staff = auth('api')->user();
      $dt = Carbon::now();
      if ($staff->departemen == "Admin Area") {
        return DB::table('pembayaran')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                   ->where([['pelanggan.id_karea', '=', $staff->id_karea],['pembayaran.tipe', '=', 'Area']])
                   ->whereMonth('pembayaran.tanggal', '=', $dt->month)
                   // ->whereBetween('pembayaran.tanggal', [$tgl1,$tgl2])
                   ->count();
      }
      if ($staff->departemen == "Admin Sub Area") {
        return DB::table('pembayaran')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'pembayaran.no_pendaftaran')
                   ->where([['pelanggan.id_subarea', '=', $staff->id_subarea],['pembayaran.tipe', '=', 'Sub Area']])
                   // ->whereMonth('pembayaran.tanggal', '=', $tgl1)
                   ->whereMonth('pembayaran.tanggal', '=', $dt->month)
                   ->count();
      }
    }

    public function dataPenerimaan(){
      $staff = auth('api')->user();
      $dt = Carbon::now();
      if ($staff->departemen == "Admin Area") {
        $laporan = DB::table('pelanggan')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->select('pembayaran.id','pembayaran.no_kwitansi', 'pembayaran.jumlah','pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
        ->orderBy('id','desc')
        ->where([['pelanggan.status','=','Sudah Bayar'],['pelanggan.id_karea', '=', $staff->id_karea],['pembayaran.tipe', '=', 'Area']])
        ->whereMonth('pembayaran.tanggal', '=', $dt->month)
        ->get();
        return response()->json($laporan);
      }
      if ($staff->departemen == "Admin Sub Area") {
        $laporan = DB::table('pelanggan')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->select('pembayaran.id','pembayaran.no_kwitansi', 'pembayaran.jumlah', 'pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
        ->orderBy('id','desc')
        ->where([['pelanggan.status','=','Sudah Bayar'],['pelanggan.id_subarea', '=', $staff->id_subarea],['pembayaran.tipe', '=', 'Sub Area']])
        ->whereMonth('pembayaran.tanggal', '=', $dt->month)
        ->get();
        return response()->json($laporan);
      }
    }

    public function FilterDataPenerimaan($tgl1, $tgl2){
      $staff = auth('api')->user();
      if ($staff->departemen == "Admin Area") {
        $laporan = DB::table('pelanggan')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->select('pembayaran.id','pembayaran.no_kwitansi', 'pembayaran.jumlah','pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
        ->orderBy('id','desc')
        ->where([['pelanggan.status','=','Sudah Bayar'],['pelanggan.id_karea', '=', $staff->id_karea],['pembayaran.tipe', '=', 'Area']])
        // ->whereMonth('pembayaran.tanggal', '=', $tgl1)
        ->whereBetween('pembayaran.tanggal', [$tgl1,$tgl2])
        ->get();
        return response()->json($laporan);
      }
      if ($staff->departemen == "Admin Sub Area") {
        $laporan = DB::table('pelanggan')
        ->join('kantor_wilayah','kantor_wilayah.id','=','id_kwilayah')
        ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
        ->join('sub_area','sub_area.id','=','id_subarea')
        ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
        ->join('pembayaran','pembayaran.no_pendaftaran','=','pelanggan.no_pendaftaran')
        ->select('pembayaran.id','pembayaran.no_kwitansi', 'pembayaran.jumlah', 'pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pembayaran.created_at','pembayaran.diterima','pembayaran.cetak','pembayaran.dicetak_oleh')
        ->orderBy('id','desc')
        ->where([['pelanggan.status','=','Sudah Bayar'],['pelanggan.id_subarea', '=', $staff->id_subarea],['pembayaran.tipe', '=', 'Sub Area']])
        // ->whereMonth('pembayaran.tanggal', '=', $tgl1)
        ->whereBetween('pembayaran.tanggal', [$tgl1,$tgl2])
        ->get();
        return response()->json($laporan);
      }
    }

    public function bulan(){
      $dt = Carbon::now();
      $staff = auth('api')->user();
      $rekap = DB::table('rekap_sub_area')
                   ->select('bulan', 'total')
                   // ->where('id_subarea', '=', $staff->id_subarea)
                   ->whereMonth('bulan', '=', $dt->month)
                   ->whereYear('bulan', '=', $dt->year)
                   ->count();

      return response()->json($rekap);
    }
}
