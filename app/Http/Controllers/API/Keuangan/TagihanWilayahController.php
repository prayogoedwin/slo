<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\TagihanWilayah;
use Illuminate\Support\Facades\DB;
use App\SetorWilayah;

class TagihanWilayahController extends Controller
{
    public function index(){
      $staff = auth('api')->user();

      $tagihan = DB::table('tagihan_wilayah')
                    ->join('share_wilayah', 'share_wilayah.id', '=', 'tagihan_wilayah.id_share')
                    ->select(DB::raw("tagihan_wilayah.id, tagihan_wilayah.no_ref, tagihan_wilayah.id_kwilayah, tagihan_wilayah.periode,
                     share_wilayah.share, tagihan_wilayah.rp_share, tagihan_wilayah.jml_tagihan, tagihan_wilayah.jml_terbayar,
                     tagihan_wilayah.jml_hutang, tagihan_wilayah.updated_at"))
                    ->where('tagihan_wilayah.id_kwilayah', '=', $staff->id_kwilayah)
                    ->get();

      return response()->json($tagihan);
    }

    public function totalTagihan(){
      $staff = auth('api')->user();
      $totaltagihan = DB::table('tagihan_wilayah')
                          ->select(DB::raw("SUM(jml_hutang) as sisa_tagihan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"))
                          ->where('id_kwilayah', '=', $staff->id_kwilayah)
                          ->get();

      return response()->json($totaltagihan);
    }

    public function filterTagihan($tgl1, $tgl2){
        $staff = auth('api')->user();

        $tagihan = DB::table('tagihan_wilayah')
                      ->join('share_wilayah', 'share_wilayah.id', '=', 'tagihan_wilayah.id_share')
                      ->select(DB::raw("tagihan_wilayah.id, tagihan_wilayah.no_ref, tagihan_wilayah.id_kwilayah, tagihan_wilayah.periode,
                       share_wilayah.share, tagihan_wilayah.rp_share, tagihan_wilayah.jml_tagihan, tagihan_wilayah.jml_terbayar,
                       tagihan_wilayah.jml_hutang, tagihan_wilayah.updated_at"))
                      ->where('tagihan_wilayah.id_kwilayah', '=', $staff->id_kwilayah)
                      ->whereBetween('tagihan_wilayah.periode', [$tgl1, $tgl2])
                      ->get();

        return response()->json($tagihan);
    }

    public function filterTotalTagihan($tgl1, $tgl2){
      $staff = auth('api')->user();
      $totaltagihan = DB::table('tagihan_wilayah')
                          ->select(DB::raw("SUM(jml_hutang) as sisa_tagihan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"))
                          ->where('id_kwilayah', '=', $staff->id_kwilayah)
                          ->whereBetween('periode', [$tgl1, $tgl2])
                          ->get();

      return response()->json($totaltagihan);
    }

    public function store(Request $request, $id){
      $staff = auth('api')->user();

      $this->validate($request, [
        'jml_setoran' => 'required',
      ]);

      $tagihan = TagihanWilayah::findOrFail($id);

      $setoran = new SetorWilayah();
      $setoran->id_tagihan = $tagihan->id;
      $setoran->no_ref = $tagihan->no_ref;
      $setoran->jml_setoran = $request['jml_setoran'];
      $setoran->id_users = $staff->id;
      $setoran->status = 0;

      $setoran->save();

      return response()->json([
        'msg' => 'Berhasil'
      ],200);
    }

    public function cekPembayaran($id){
      $setoran = DB::table('setor_wilayah')
                     ->join('users', 'users.id', '=', 'setor_wilayah.id_users')
                     // ->select('setor_wilayah.jml_setoran', 'users.nama', 'setor_wilayah.created_at', DB::raw("CASE WHEN (setor_wilayah.status = 0) THEN 'Belum Konfirmasi' ELSE 'Terkonfirmasi' END"))
                     ->select(DB::raw("setor_wilayah.no_ref, setor_wilayah.jml_setoran, users.nama, setor_wilayah.created_at, (CASE WHEN (setor_wilayah.status = 0) THEN 'Belum Konfirmasi' WHEN (setor_wilayah.status = 1) THEN 'Terkonfirmasi' ELSE 'Ditolak' END) as keterangan"))
                     ->where('setor_wilayah.id_tagihan', '=', $id)
                     ->get();

      return response()->json($setoran);
    }

    public function totalSeluruhTagihan(){
      // $staff = auth('api')->user();
      $dt = Carbon::now();
      $totaltagihan = DB::table('tagihan_wilayah')
                          ->select(DB::raw("SUM(jml_tagihan) as total_pendapatan"),
                          DB::raw("sum(jml_terbayar) as total_terbayarkan"), DB::raw("sum(jml_hutang) as total_hutang"))
                          ->whereMonth('periode', '=', $dt->month)
                          ->get();

      return response()->json($totaltagihan);
    }

    public function cekTotalTagihan($id){
      $tagihan = DB::table('tagihan_wilayah')
                     ->select(DB::raw("SUM(jml_terbayar) as jml_terbayarkan, SUM(jml_hutang) as jml_hutang"))
                     ->where('tagihan_wilayah.id_kwilayah', '=', $id)
                     ->get();

      return response()->json($tagihan);
    }

    public function loadWilayah(){
      // $staff = auth('api')->user();

      $wilayah = DB::table('kantor_wilayah')
                  ->select(DB::raw("id, nama_wilayah as nama"))
                  ->get();

      return response()->json($wilayah);
    }

    public function getSetoran(){
      $staff = auth('api')->user();

      $tagihan = DB::table('setor_wilayah')
                    ->join('tagihan_wilayah', 'tagihan_wilayah.id', '=', 'setor_wilayah.id_tagihan')
                    ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'tagihan_wilayah.id_kwilayah')
                    ->join('users', 'users.id', '=', 'setor_wilayah.id_users')
                    ->select(DB::raw("setor_wilayah.id, kantor_wilayah.nama_wilayah, setor_wilayah.no_ref,
                    setor_wilayah.jml_setoran, users.nama"))
                    ->where([['setor_wilayah.status', '=', '0']])
                    ->get();

       return response()->json($tagihan);
    }

    public function cekTagihan($id){
      $tagihan = DB::table('tagihan_wilayah')
                    ->join('share_wilayah', 'share_wilayah.id', 'tagihan_wilayah.id_share')
                    ->select(DB::raw("tagihan_wilayah.id, tagihan_wilayah.no_ref,
                      tagihan_wilayah.periode, share_wilayah.share, tagihan_wilayah.rp_share,
                      tagihan_wilayah.jml_tagihan, tagihan_wilayah.jml_terbayar, tagihan_wilayah.jml_hutang,
                      tagihan_wilayah.updated_at"))
                    ->where('tagihan_wilayah.id_kwilayah', '=', $id)
                    ->get();

      return response()->json($tagihan);
    }

    public function konfirmSetoran(Request $request, $id){
      $setoran = SetorWilayah::findOrFail($id);
      $setoran->status = 1;
      $setoran->save();

      $staff = auth('api')->user();
      $dt = Carbon::now();

      if ($setoran->save()) {
        $tagihan = TagihanWilayah::where('id', '=', $setoran->id_tagihan)->first();

        $tagihan->jml_terbayar = $setoran->jml_setoran + $tagihan->jml_terbayar;
        $tagihan->jml_hutang = $tagihan->jml_hutang - $setoran->jml_setoran;
        $tagihan->save();

        return response()->json([
          'msg' => 'Berhasil'
        ], 200);
      } else {
        return response()->json([
          'msg' => 'Gagal'
        ], 401);
      }

    }

}
