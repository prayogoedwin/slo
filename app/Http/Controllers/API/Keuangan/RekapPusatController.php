<?php

namespace App\Http\Controllers\API\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\RekapPusat;
use App\TagihanWilayah;

class RekapPusatController extends Controller
{
    public function index(){
      return RekapPusat::get();
    }

    public function penerimaan(){
      $staff = auth('api')->user();

      $dt = Carbon::now();

      return DB::table('setor_wilayah')
             ->join('tagihan_wilayah', 'tagihan_wilayah.id', '=', 'setor_wilayah.id_tagihan')
             ->where([['setor_wilayah.status', '=', '1']])
             ->whereMonth('setor_wilayah.updated_at', '=', $dt->month)
             ->whereYear('setor_wilayah.updated_at', '=', $dt->year)
             ->count();
    }

    public function tagihanWilayah(){
      $staff = auth('api')->user();
      $dt = Carbon::now();

      $tagihan = DB::table('tagihan_wilayah')
                    ->select(DB::raw("SUM(jml_tagihan) as total_tagihan"))
                    ->whereMonth('periode', '=', $dt->month)
                    ->whereYear('periode', '=', $dt->year)
                    ->get();

      return response()->json($tagihan);
    }

    public function totalPendapatan(){
      $staff = auth('api')->user();
      $dt = Carbon::now();

      $pemasukan = DB::table('setor_wilayah')
                      ->join('tagihan_wilayah', 'tagihan_wilayah.id', '=', 'setor_wilayah.id_tagihan')
                      ->select(DB::raw("SUM(setor_wilayah.jml_setoran) as pendapatan"))
                      ->where([['setor_wilayah.status', '=', '1']])
                      ->whereMonth('setor_wilayah.updated_at', '=', $dt->month)
                      ->whereYear('setor_wilayah.updated_at', '=', $dt->year)
                      ->get();

      return response()->json($pemasukan);
    }

    public function dataPenerimaan(){
      $staff = auth('api')->user();
      $dt = Carbon::now();

      $pemasukan = DB::table('setor_wilayah')
                      ->join('tagihan_wilayah', 'tagihan_wilayah.id', '=', 'setor_wilayah.id_tagihan')
                      ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'tagihan_wilayah.id_kwilayah')
                      ->join('users', 'users.id', '=', 'setor_wilayah.id_users')
                      ->select(DB::raw("setor_wilayah.id, kantor_wilayah.nama_wilayah, setor_wilayah.no_ref,
                      setor_wilayah.jml_setoran, users.nama, setor_wilayah.updated_at"))
                      ->where([['setor_wilayah.status', '=', '1']])
                      ->whereMonth('setor_wilayah.updated_at', '=', $dt->month)
                      ->whereYear('setor_wilayah.updated_at', '=', $dt->year)
                      ->get();

      return response()->json($pemasukan);
    }

    public function store(Request $request){
      $dt = Carbon::now();
      $staff = auth('api')->user();

      $rekap = DB::table('rekap_pusat')
                    ->select('id', 'periode', 'total_penerimaan')
                    ->whereMonth('periode', '=', $dt->month)
                    ->whereYear('periode', '=', $dt->year)
                    ->first();

      if (!$rekap) {
        $rekapbaru = new RekapPusat();

        $rekapbaru->periode = $dt;
        $rekapbaru->total_penerimaan = $request->total;

        $rekapbaru->save();

        return response()->json([
          'msg' => 'Berhasil',
        ],200);
      } else {
        $rekapupdate = RekapPusat::findOrFail($rekap->id);

        $rekapupdate->total_penerimaan = $request->total;
        $rekapupdate->save();

        return response()->json([
          'msg' => 'Berhasil Update',
        ],200);
      }
    }

    public function filter($tgl1, $tgl2){
      $pemasukan = DB::table('setor_wilayah')
                      ->join('tagihan_wilayah', 'tagihan_wilayah.id', '=', 'setor_wilayah.id_tagihan')
                      ->select(DB::raw("SUM(setor_wilayah.jml_setoran) as pendapatan"))
                      ->where([['setor_wilayah.status', '=', '1']])
                      ->whereBetween('setor_wilayah.updated_at', [$tgl1,$tgl2])
                      ->get();

      return response()->json($pemasukan);
    }

    public function hitungPenerimaan($tgl1, $tgl2){
      return DB::table('setor_wilayah')
             ->join('tagihan_wilayah', 'tagihan_wilayah.id', '=', 'setor_wilayah.id_tagihan')
             ->where([['setor_wilayah.status', '=', '1']])
             ->whereBetween('setor_wilayah.updated_at', [$tgl1,$tgl2])
             ->count();
    }

    public function FilterDataPenerimaan($tgl1, $tgl2){
      $pemasukan = DB::table('setor_wilayah')
                      ->join('tagihan_wilayah', 'tagihan_wilayah.id', '=', 'setor_wilayah.id_tagihan')
                      ->join('kantor_wilayah', 'kantor_wilayah.id', '=', 'tagihan_wilayah.id_kwilayah')
                      ->join('users', 'users.id', '=', 'setor_wilayah.id_users')
                      ->select(DB::raw("setor_wilayah.id, kantor_wilayah.nama_wilayah, setor_wilayah.no_ref,
                      setor_wilayah.jml_setoran, users.nama, setor_wilayah.updated_at"))
                      ->where([['setor_wilayah.status', '=', '1']])
                      ->whereBetween('setor_wilayah.updated_at', [$tgl1,$tgl2])
                      ->get();

      return response()->json($pemasukan);
    }
}
