<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Btl;

class BtlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Btl::with('wilayah','kota','asosiasi')->orderBy('nama_btl','asc')->get();
    }

    public function generateKode(){
        $awal = "JX";

        $kodeAkhir = Btl::max('kodefikasi');

        $kode = (int) substr($kodeAkhir, 2,2);
        $kode++;

        $kodefikasi = $awal . sprintf("%02s", $kode);

        return response()->json($kodefikasi);
    }

    public function loadBiro(){
        $staff = auth('api')->user();
        if ($staff->jabatan == 'BTL') {
          // $btl = Btl::where([['id_kwilayah', '=', request('id_kwilayah')],['']])->get();
          $btl = DB::table('btl')
              ->join('staff_btl', 'staff_btl.id_btl', '=', 'btl.id')
              ->join('users', 'users.id', '=', 'staff_btl.id_users')
              ->select('btl.id', 'btl.nama_btl')
              ->where('staff_btl.id_users', '=', $staff->id)
              ->get();

         return response()->json($btl);
       } else {
        //  $btl = Btl::where('id_kwilayah', '=', request('id_kwilayah'))->orderBy('nama_btl')->get();
        $btl = Btl::orderBy('nama_btl')->get();
         return response()->json($btl);
       }

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id= intval(Btl::max('id')) + 1;
        $data = $this->validate( $request,[
            'nama_btl' => 'required',
            'id_kwilayah' => 'required',
            'kode_kota' => 'required',
            'alamat' => 'required',
            'email' => 'required|email',
            'telp' => 'required',
        ]);
          // dd();
        return Btl::create([
            'id' => $id,
            'nama_btl' => $request['nama_btl'],
            'id_kwilayah' => $request['id_kwilayah'],
            'kode_kota' => $request['kode_kota'],
            'alamat' => $request['alamat'],
            'email' => $request['email'],
            'nama_ahli_teknik' => $request['nama_ahli_teknik'],
            'telp' => $request['telp'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $btl = Btl::findOrFail($id);

        $this->validate( $request,[
            'nama_btl' => 'required',
            'id_kwilayah' => 'required',
            'kode_kota' => 'required',
            'alamat' => 'required',
            'email' => 'required|email',
            'telp' => 'required',
        ]);

        $btl->update($request->all());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $btl = Btl::findOrFail($id);

        $btl->delete();
    }
}
