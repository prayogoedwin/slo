<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\Pelanggan;
use App\Verifikasi;
use App\User;
use App\Sertifikasi;
use App\KantorWilayah;
use App\LogStatus;
class SertifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = auth('api')->user();
        if (auth('api')->user()->departemen == 'Admin Pusat' || auth('api')->user()->departemen == 'Manajer Area') {
          return DB::table('pelanggan')
         ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
         ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
         ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
         ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
         ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
         ->join('btl','btl.id','=','pelanggan.id_btl')
         ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
         ->select('pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl')
         ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
         ['verifikasi.status_sertifikasi', '=', '0']])
         ->get();
        }
        if (auth('api')->user()->departemen == 'Admin Wilayah') {
          return DB::table('pelanggan')
         ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
         ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
         ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
         ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
         ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
         ->join('btl','btl.id','=','pelanggan.id_btl')
         ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
         ->select('pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl')
         ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
         ['verifikasi.status_sertifikasi', '=', '0'],['pelanggan.id_kwilayah', '=', $staff->id_kwilayah]])
         ->get();
        }
        if (auth('api')->user()->departemen == 'Admin Area') {
          return DB::table('pelanggan')
         ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
         ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
         ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
         ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
         ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
         ->join('btl','btl.id','=','pelanggan.id_btl')
         ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
         ->select('pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl')
         ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
         ['verifikasi.status_sertifikasi', '=', '0'],['pelanggan.id_karea', '=', $staff->id_karea]])
         ->get();
        }
        if (auth('api')->user()->departemen == 'Admin Sub Area') {
          return DB::table('pelanggan')
         ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
         ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
         ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
         ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
         ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
         ->join('btl','btl.id','=','pelanggan.id_btl')
         ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
         ->select('pelanggan.no_pendaftaran','pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl')
         ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
         ['verifikasi.status_sertifikasi', '=', '0'],['pelanggan.id_subarea', '=', $staff->id_subarea]])
         ->get();
        }
    }

    public function sudahTerbit(){
      $staff = auth('api')->user();
      if (auth('api')->user()->departemen == 'Admin Pusat' || auth('api')->user()->departemen == 'Manajer Area') {
         return DB::table('sertifikasi')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'sertifikasi.no_pendaftaran')
                   ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
                   ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
                   ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
                   ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
                   ->join('btl','btl.id','=','pelanggan.id_btl')
                   ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('users as dicetak_oleh', 'dicetak_oleh.id', '=', 'sertifikasi.dicetak_oleh')
                   ->select('sertifikasi.no_pendaftaran', 'pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl',
                   'sertifikasi.no_seri','sertifikasi.created_at as tgl_cetak','sertifikasi.dicetak_oleh','dicetak_oleh.nama as nama_pencetak')
                   ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
                   ['verifikasi.status_sertifikasi', '=', '1']])
                   ->get();
      }
       if (auth('api')->user()->departemen == 'Admin Wilayah') {
        return DB::table('sertifikasi')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'sertifikasi.no_pendaftaran')
                   ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
                   ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
                   ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
                   ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
                   ->join('btl','btl.id','=','pelanggan.id_btl')
                   ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('users as dicetak_oleh', 'dicetak_oleh.id', '=', 'sertifikasi.dicetak_oleh')
                   ->select('sertifikasi.no_pendaftaran', 'pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl',
                   'sertifikasi.no_seri','sertifikasi.created_at as tgl_cetak','sertifikasi.dicetak_oleh','dicetak_oleh.nama as nama_pencetak')
                   ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
                   ['verifikasi.status_sertifikasi', '=', '1'],['pelanggan.id_kwilayah', '=', $staff->id_kwilayah]])
                   ->get();
      }
      if (auth('api')->user()->departemen == 'Admin Area') {
        return DB::table('sertifikasi')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'sertifikasi.no_pendaftaran')
                   ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
                   ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
                   ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
                   ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
                   ->join('btl','btl.id','=','pelanggan.id_btl')
                   ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('users as dicetak_oleh', 'dicetak_oleh.id', '=', 'sertifikasi.dicetak_oleh')
                   ->select('sertifikasi.no_pendaftaran', 'pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl',
                   'sertifikasi.no_seri','sertifikasi.created_at as tgl_cetak','sertifikasi.dicetak_oleh','dicetak_oleh.nama as nama_pencetak')
                   ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
                   ['verifikasi.status_sertifikasi', '=', '1'],['pelanggan.id_karea', '=', $staff->id_karea]])
                   ->get();
      }
      if (auth('api')->user()->departemen == 'Admin Sub Area') {
        return DB::table('sertifikasi')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'sertifikasi.no_pendaftaran')
                   ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
                   ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
                   ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
                   ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
                   ->join('btl','btl.id','=','pelanggan.id_btl')
                   ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('users as dicetak_oleh', 'dicetak_oleh.id', '=', 'sertifikasi.dicetak_oleh')
                   ->select('sertifikasi.no_pendaftaran', 'pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl',
                   'sertifikasi.no_seri','sertifikasi.created_at as tgl_cetak','sertifikasi.dicetak_oleh','dicetak_oleh.nama as nama_pencetak')
                   ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
                   ['verifikasi.status_sertifikasi', '=', '1'],['pelanggan.id_subarea', '=', $staff->id_subarea]])
                   ->get();
      }

    }

    public function filterSertifikasi($tgl1, $tgl2){
       return DB::table('sertifikasi')
                   ->join('pelanggan', 'pelanggan.no_pendaftaran', '=', 'sertifikasi.no_pendaftaran')
                   ->join('kota','kota.kode_kota','=','pelanggan.kode_kota')
                   ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
                   ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
                   ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
                   ->join('btl','btl.id','=','pelanggan.id_btl')
                   ->join('pemeriksa','pemeriksa.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('verifikasi','verifikasi.no_pendaftaran','=','pelanggan.no_pendaftaran')
                   ->join('users as dicetak_oleh', 'dicetak_oleh.id', '=', 'sertifikasi.dicetak_oleh')
                   ->select('sertifikasi.no_pendaftaran', 'pelanggan.nama','kota.kota','kantor_wilayah.nama_wilayah','kantor_area.nama as nama_area','sub_area.nama_subarea','pemeriksa.no_lhpp','btl.nama_btl',
                   'sertifikasi.no_seri','sertifikasi.created_at as tgl_cetak','sertifikasi.dicetak_oleh','dicetak_oleh.nama as nama_pencetak')
                   ->where([['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
                   ['verifikasi.status_sertifikasi', '=', '1']])
                   ->whereBetween('verifikasi.created_at',[$tgl1, $tgl2])
                   ->paginate(10);
    }

    public function registrasiSertif($no_pendaftaran){
      return DB::table('pelanggan')
                  ->join('kantor_wilayah','kantor_wilayah.id','=','pelanggan.id_kwilayah')
                  ->join('kantor_area','kantor_area.id','=','pelanggan.id_karea')
                  ->join('sub_area','sub_area.id','=','pelanggan.id_subarea')
                  ->join('jenis_bangunan', 'jenis_bangunan.id', '=', 'pelanggan.id_bangunan')
                  ->join('daya', 'daya.id', '=', 'pelanggan.id_daya')
                  ->join('penyedia_listrik', 'penyedia_listrik.id', '=', 'pelanggan.id_penyedia')
                  ->join('btl','btl.id','=','pelanggan.id_btl')
                  ->join('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                  ->join('verifikasi', 'verifikasi.no_lhpp', '=', 'pemeriksa.no_lhpp')
                  ->join('users', 'users.id', '=', 'verifikasi.id_staff')
                  ->join('pelanggan_temp', 'pelanggan_temp.id_pelanggan', '=', 'pelanggan.id')
                  ->join('surat_tugas', 'surat_tugas.no_surat', '=', 'pelanggan_temp.no_surat')
                  ->join('users as penanda_tangan', 'penanda_tangan.id', '=', 'surat_tugas.id_staff')
                  ->select('pelanggan.no_pendaftaran', 'pelanggan.nama', 'pelanggan.alamat','pelanggan.no_agenda','pelanggan.no_agendapln','pelanggan.id_karea','kantor_wilayah.nama_wilayah',
                  'kantor_area.nama as nama_area','sub_area.nama_subarea','jenis_bangunan.jenis_bangunan','daya.daya','penyedia_listrik.nama_penyedia','btl.nama_btl','pemeriksa.no_lhpp',
                  'pemeriksa.jml_phb_utama','pemeriksa.jml_saluran_cabang','pemeriksa.jml_titik_lampu','pemeriksa.jml_sakelar','pemeriksa.kkb','pemeriksa.kkk',
                  'penanda_tangan.nama as pttd','users.nama as pjt')
                  ->where([['pelanggan.no_pendaftaran', '=', $no_pendaftaran],['pemeriksa.status_verifikasi','=','1'],['verifikasi.hasil_verifikasi','=','approve'],['verifikasi.hasil_pemeriksaan','=','Laik Operasi (LO)'],
                  ['verifikasi.status_sertifikasi', '=', '0']])
                  ->get();
    }

    public function loadPjt($no_pendaftaran){
      $pelanggan = Pelanggan::where('no_pendaftaran', '=', $no_pendaftaran)->get();

      foreach ($pelanggan as $p) {
        return User::where([['id_karea', '=', $p->id_karea],['jabatan', '=', 'PJT']])->get();
      }
    }

    public function loadTT($no_pendaftaran){
      $pelanggan = Pelanggan::where('no_pendaftaran', '=', $no_pendaftaran)->get();

      foreach ($pelanggan as $p) {
        return User::where([['id_karea', '=', $p->id_karea],['jabatan', '=', 'TT']])->get();
      }
    }

    public function penandaTangan(){
      $staff = auth('api')->user();
      // if ($staff->departemen == 'Admin Wilayah') {
         return DB::table('kantor_wilayah')->select('kantor_wilayah.general_manager as penanda_tangan')->where('kantor_wilayah.id', '=', $staff->id_kwilayah)->get();
      // }
      // if ($staff->departemen == 'Admin Area') {
      //    return DB::table('kantor_area')->select('kantor_area.manager as penanda_tangan')->where('kantor_area.id', '=', $staff->id_karea)->get();
      // }
      // if ($staff->departemen == 'Admin Sub Area') {
      //    return DB::table('kantor_area')->select('kantor_area.manager as penanda_tangan')->where('kantor_area.id', '=', $staff->id_karea)->get();
      // }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $staff = auth('api')->user();

      $this->validate($request,[
          'no_pendaftaran' => 'required',
          'no_agenda' => 'required',
          'no_lhpp' => 'required',
          'pjt_cetak' => 'required',
          'tt_cetak' => 'required',
          'no_seri' => 'required'
      ]);

      $date = Carbon::now();


      $sertifikat = new Sertifikasi;
      $sertifikat->no_pendaftaran = $request->input('no_pendaftaran');
      $sertifikat->no_agenda = $request->input('no_agenda');
      $sertifikat->no_registrasi = 'B191589618975';
      $sertifikat->no_sertifikat = '8UA8.628.7.3322.JM19.19';
      $sertifikat->no_lhpp = $request->input('no_lhpp');
      $sertifikat->no_seri = $request->input('no_seri');
      $sertifikat->pjt_cetak = $request->input('pjt_cetak');
      $sertifikat->tt_cetak = $request->input('tt_cetak');
      $sertifikat->tgl_cetak = date('Y-m-d');
      $sertifikat->masa_berlaku = $date->addYear(15);
      $sertifikat->dicetak_oleh = $staff->id;
      $sertifikat->penanda_tangan = $request->input('penanda_tangan');

      $sertifikat->save();

      if ($sertifikat->save()) {
        $verifikasi = Verifikasi::where('no_pendaftaran', '=', $request->input('no_pendaftaran'))->update([
          'status_sertifikasi' => '1',
        ]);

        $log = LogStatus::where('no_pendaftaran', '=', $request['no_pendaftaran']);

        $log->update([
          'status' => 'SLO telah terbit. Proses verifikasi PLN',
          'kode_status' => '4',
        ]);
      }
      // if ($sertifikat->save()) {
      //   return redirect()->url('/pdf/sertifikat/cetak/')
      // }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
