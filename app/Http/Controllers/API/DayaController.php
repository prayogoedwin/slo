<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Daya;

class DayaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Daya::orderBy('id','asc')->get();
    }

    public function loadDaya(){
        $daya = Daya::get();
        return response()->json($daya);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'daya' => 'required',
            'jenis_fasa' => 'required',
            'harga' => 'required',
            'biaya' => 'required',
            'ppn' => 'required',
            'total' => 'required',
            'terbilang' => 'required'

        ]);

        return Daya::create([
            'daya' => $request['daya'],
            'jenis_fasa' => $request['jenis_fasa'],
            'harga' => $request['harga'],
            'biaya' => $request['biaya'],
            'ppn' => $request['ppn'],
            'total' => $request['total'],
            'terbilang' => $request['terbilang']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $daya = Daya::findOrFail($id);
         $this->validate($request,[
            'daya' => 'required',
            'jenis_fasa' => 'required',
            'harga' => 'required',
            'biaya' => 'required',
            'ppn' => 'required',
            'total' => 'required',
            'terbilang' => 'required'

        ]);

         $daya->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $daya = Daya::findOrFail($id);

        $daya->delete();
    }
}
