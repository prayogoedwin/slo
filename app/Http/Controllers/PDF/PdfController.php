<?php

namespace App\Http\Controllers\PDF;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use PDF;
use App\Pelanggan;
use App\User;
use App\PelangganTemp;
use App\SuratTugas;
use App\Pemeriksa;
use App\Verifikasi;
class PdfController extends Controller
{
    public function generateKwitansi($id){
		$kwitansi =  Pelanggan::with(['pembayaran','penyedia','wilayah', 'area', 'subarea', 'daya', 'tarif', 'kota', 'bangunan', 'asosiasi', 'btl'])->find($id);

		$pdf = PDF::loadView('pdf.kwitansi', compact('kwitansi'))->setPaper('a4', 'potrait');

		return $pdf->stream('kwitansi.pdf');
    }

    public function generateSuratTugas($id){
      $surat = SuratTugas::with('pemeriksa1', 'pemeriksa2', 'penerbit')->find($id);

      $pelanggan = PelangganTemp::with('pelanggan', 'tarif', 'daya', 'penyedia')->where('no_surat', '=', $surat->no_surat)->get();

      $pdf = PDF::loadView('pdf.surat', compact('surat', 'pelanggan'))->setPaper('a4', 'potrait');

      return $pdf->stream();
    }

    public function generateVerifikasi($no_pendaftaran){
      $pelanggan = Pelanggan::with('tarif', 'daya', 'btl')->where('no_pendaftaran', '=', $no_pendaftaran)->get();
      $pemeriksaan = Pemeriksa::where('no_pendaftaran', '=', $no_pendaftaran)->get();
      $verifikasi = Verifikasi::with('verifikator')->where('no_pendaftaran', '=', $no_pendaftaran)->get();
      $pdf = PDF::loadView('pdf.verifikasi', compact('pelanggan', 'pemeriksaan', 'verifikasi'))->setPaper('a4', 'potrait');

      return $pdf->stream();
    }

    public function generateSertifikasi($no_pendaftaran){
      Date::setLocale('id');
      $sertifikat = DB::table('sertifikasi')
                        ->leftJoin('pelanggan', 'pelanggan.no_pendaftaran', '=', 'sertifikasi.no_pendaftaran')
                        ->leftJoin('daya', 'daya.id', '=', 'pelanggan.id_daya')
                        ->leftJoin('pemeriksa', 'pemeriksa.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                        ->leftJoin('penyedia_listrik', 'penyedia_listrik.id', '=', 'pelanggan.id_penyedia')
                        ->leftJoin('users as pjt', 'pjt.id', '=', 'sertifikasi.pjt_cetak')
                        ->leftJoin('kantor_area', 'kantor_area.id', '=', 'pelanggan.id_karea')
                        ->leftJoin('verifikasi', 'verifikasi.no_pendaftaran', '=', 'pelanggan.no_pendaftaran')
                        ->leftJoin('kantor_wilayah', 'kantor_wilayah.id', '=', 'pelanggan.id_kwilayah')
                        ->select('sertifikasi.no_pendaftaran','sertifikasi.no_sertifikat', 'sertifikasi.no_registrasi','sertifikasi.no_seri','sertifikasi.no_lhpp','sertifikasi.tgl_cetak','pelanggan.nama as nama_pelanggan','pelanggan.alamat','daya.daya',
                        'pemeriksa.jml_phb_utama','pemeriksa.jml_phb_3fasa','pemeriksa.kkb','pemeriksa.jml_titik_lampu','pemeriksa.jml_sakelar',
                        'penyedia_listrik.nama_penyedia','pjt.nama as nama_pjt','kantor_area.nama as nama_area','verifikasi.hasil_pemeriksaan','pemeriksa.lat','pemeriksa.lng','kantor_wilayah.nama_wilayah',
                        'sertifikasi.masa_berlaku','sertifikasi.penanda_tangan')
                        ->where('sertifikasi.no_pendaftaran', '=', $no_pendaftaran)
                        ->get();
      $pdf = PDF::loadView('pdf.sertifikat', compact('sertifikat'))->setPaper('a4','landscape')->setOptions([ 'defaultFont' => 'sans-serif', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);

      return $pdf->stream('sertifikat-slo.pdf');
    }
}
