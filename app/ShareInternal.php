<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareInternal extends Model
{
    protected $table = ['share_internal'];

    protected $fillable = ['kode_alokasi', 'periode', 'nama', 'persen', 'jumlah'];
}
