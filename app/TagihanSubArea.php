<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagihanSubArea extends Model
{
    protected $table = 'tagihan_sub_area';

    protected $fillable = ['id_karea', 'id_subarea', 'periode', 'jml_tagihan', 'jml_terbayar', 'jml_piutang',
                          'no_ref'];
}
