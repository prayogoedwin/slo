<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Provinsi;
use App\KantorArea;

class KantorWilayah extends Model
{
    protected $table = "kantor_wilayah";

    protected $fillable = ['id_wilayah','nama_wilayah','general_manager','kode_prov','alamat','telp','email'];
    
    public function get($model, array $fields)
   {
       extract(request()->only(['query', 'limit', 'page', 'orderBy', 'ascending', 'byColumn']));

       $data = $model->select($fields);

       if (isset($query) && $query) {
           $data = $byColumn == 1 ?
               $this->filterByColumn($data, $query) :
               $this->filter($data, $query, $fields);
       }

       $count = $data->count();

       $data->limit($limit)
           ->skip($limit * ($page - 1));

       if (isset($orderBy)) {
           $direction = $ascending == 1 ? 'ASC' : 'DESC';
           $data->orderBy($orderBy, $direction);
       }

       $results = $data->get()->toArray();

       return [
           'data' => $results,
           'count' => $count,
       ];
   }

   protected function filterByColumn($data, $queries)
   {
       return $data->where(function ($q) use ($queries) {
           foreach ($queries as $field => $query) {
               if (is_string($query)) {
                   $q->where($field, 'LIKE', "%{$query}%");
               } else {
                   $start = Carbon::createFromFormat('Y-m-d', $query['start'])->startOfDay();
                   $end = Carbon::createFromFormat('Y-m-d', $query['end'])->endOfDay();

                   $q->whereBetween($field, [$start, $end]);
               }
           }
       });
   }

   protected function filter($data, $query, $fields)
   {
       return $data->where(function ($q) use ($query, $fields) {
           foreach ($fields as $index => $field) {
               $method = $index ? 'orWhere' : 'where';
               $q->{$method}($field, 'LIKE', "%{$query}%");
           }
       });
   }

    public function provinsi(){
    	return $this->belongsTo('App\Provinsi','kode_prov','kode_prov');
    }

    public function kantorarea(){
    	return $this->hasMany('App\KantorArea');
    }

    public function btl(){
    	return $this->hasMany('App\Btl');
    }

    public function penyedialistrik(){
        return $this->hasMany('App\PenyediaListrik');
    }

    public function user(){
        return $this->hasMany('App\User');
    }

    public function pelanggan(){
        return $this->hasMany('App\Pelanggan');
    }
}
