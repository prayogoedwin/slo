<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenyediaListrik extends Model
{
    protected $table = "penyedia_listrik";

    protected $fillable = ['id','nama_penyedia','kode_pln','id_kwilayah','id_karea','alamat','status'];

    public function area(){
    	return $this->belongsTo('App\KantorArea','id_karea');
    }

    public function wilayah(){
    	return $this->belongsTo('App\KantorWilayah','id_kwilayah');
    }
    public function pelanggan(){
    	return $this->hasMany('App\Pelanggan');
    }
}
