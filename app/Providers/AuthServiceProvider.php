<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isBtl', function($user){
          return $user->departemen == "BTL";
        });

        Gate::define('isAdminPusat', function($user){
          return $user->jabatan == "Admin Pusat";
        });

        Gate::define('isAdmin', function ($user) {
            return $user->jabatan == "Admin";
        });

        Gate::define('isAdWilayah', function($user){
           return $user->departemen === 'Admin Wilayah';
        });

        Gate::define('isAdArea', function($user){
           return $user->departemen === 'Admin Area';
        });

        Gate::define('isAdSubArea', function($user){
           return $user->departemen === 'Admin Sub Area';
        });

        Gate::define('isManagerArea', function($user){
           return $user->departemen === 'Manajer Area';
        });

        Gate::define('isVerifikator', function($user){
           return $user->departemen === 'Verifikator';
        });

        Gate::define('isPemeriksa', function($user){
           return $user->departemen === 'Pemeriksa';
        });

        Gate::define('isInternal', function($user){
          return $user->departemen === "Internal";
        });

        Passport::routes();

        //
    }
}
