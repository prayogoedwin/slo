<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Kota;
use App\KantorWilayah;

class Provinsi extends Model
{
    protected $table = "provinsi";

    public function kota(){
    	return $this->hasMany('App\Kota','kode_prov');
    }

    public function kantor_wilayah(){
    	return $this->hasMany('App\KantorWilayah','kode_prov','kode_prov');
    }
}
