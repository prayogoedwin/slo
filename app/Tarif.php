<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarif extends Model
{
    protected $table = "tarif";

    protected $fillable = ['id', 'jenis_tarif'];

    public function pelanggan(){
        return $this->hasMany('App\Pelanggan');
    }
}
