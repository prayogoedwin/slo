<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KantorArea extends Model
{
    protected $table = "kantor_area";

    protected $fillable = ['id_kwilayah','kode_kota','nama','manager','pjt','status'];

    public function kwilayah(){
    	return $this->belongsTo('App\KantorWilayah','id_kwilayah');
    }

    public function kota(){
    	return $this->belongsTo('App\Kota','kode_kota','kode_kota');
    }

    public function btl(){
    	return $this->hasMany('App\Btl');
    }

    public function penyedialistrik(){
        return $this->hasMany('App\PenyediaListrik');
    }

    public function user(){
        return $this->hasMany('App\User');
    }
    
    public function pelanggan(){
        return $this->hasMany('App\Pelanggan');
    }
}
