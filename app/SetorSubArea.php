<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\SetorSubArea;

class SetorSubArea extends Model
{
    protected $table = 'setor_sub_area';

    protected $fillable = ['id_tagihan', 'no_ref', 'jml_setoran', 'id_users'];
}
