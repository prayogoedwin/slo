<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PelangganTemp extends Model
{
    protected $table = "pelanggan_temp";

    protected $fillable = ['id_pelanggan', 'id_tarif', 'id_daya', 'id_penyedia', 'id_staff', 'no_surat'];

    public function pelanggan(){
      return $this->hasOne('App\Pelanggan', 'id', 'id_pelanggan');
    }


    public function daya(){
    	return $this->belongsTo('App\Daya','id_daya');
    }

    public function tarif(){
    	return $this->belongsTo('App\Tarif','id_tarif');
    }


    public function penyedia(){
        return $this->belongsTo('App\PenyediaListrik','id_penyedia');
    }

    public function surat(){
      return $this->hasOne('App\SuratTugas', 'no_surat', 'no_surat');
    }

}
