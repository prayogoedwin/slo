<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Daya extends Model
{
    protected $table = "daya";

    protected $fillable = ['id', 'daya', 'jenis_fasa', 'harga', 'biaya', 'ppn', 'total', 'terbilang'];

    public function pelanggan(){
        return $this->hasMany('App\Pelanggan');
    }
}
