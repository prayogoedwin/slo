<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekapSubArea extends Model
{
    protected $table = 'rekap_sub_area';

    protected $fillable = ['id_karea', 'id_subarea', 'bulan', 'total'];
}
