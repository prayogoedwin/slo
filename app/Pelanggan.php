<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Pelanggan extends Model
{
    protected $table = "pelanggan";

    protected $fillable = ['no_pendaftaran', 'no_agenda', 'nama', 'alamat', 'no_telepon', 'no_identitas', 'id_kwilayah', 'id_karea', 'kode_kota', 'id_subarea', 'id_tarif', 'id_daya','id_penyedia',
        'id_bangunan', 'id_asosiasi', 'id_btl', 'tipe', 'status', 'tgl_gambar', 'tgl_regpln',
        'no_agendapln', 'no_gambar','diinput_oleh'];

        public function get($model, array $fields)
        {
            extract(request()->only(['query', 'limit', 'page', 'orderBy', 'ascending', 'byColumn']));

            $data = $model->select($fields);

            if (isset($query) && $query) {
                $data = $byColumn == 1 ?
                    $this->filterByColumn($data, $query) :
                    $this->filter($data, $query, $fields);
            }

            $count = $data->count();

            $data->limit($limit)
                ->skip($limit * ($page - 1));

            if (isset($orderBy)) {
                $direction = $ascending == 1 ? 'ASC' : 'DESC';
                $data->orderBy($orderBy, $direction);
            }

            $results = $data->get()->toArray();

            return [
                'data' => $results,
                'count' => $count,
            ];
        }

        protected function filterByColumn($data, $queries)
        {
            return $data->where(function ($q) use ($queries) {
                foreach ($queries as $field => $query) {
                    if (is_string($query)) {
                        $q->where($field, 'LIKE', "%{$query}%");
                    } else {
                        $start = Carbon::createFromFormat('Y-m-d', $query['start'])->startOfDay();
                        $end = Carbon::createFromFormat('Y-m-d', $query['end'])->endOfDay();

                        $q->whereBetween($field, [$start, $end]);
                    }
                }
            });
        }

        protected function filter($data, $query, $fields)
        {
            return $data->where(function ($q) use ($query, $fields) {
                foreach ($fields as $index => $field) {
                    $method = $index ? 'orWhere' : 'where';
                    $q->{$method}($field, 'LIKE', "%{$query}%");
                }
            });
        }

    public function area(){
        return $this->belongsTo('App\KantorArea','id_karea');
    }

    public function wilayah(){
        return $this->belongsTo('App\KantorWilayah','id_kwilayah');
    }

    public function subarea(){
        return $this->belongsTo('App\SubArea','id_subarea');
    }

    public function asosiasi(){
    	return $this->belongsTo('App\Asosiasi','id_asosiasi');
    }

    public function kota(){
    	return $this->belongsTo('App\Kota','kode_kota','kode_kota');
    }

    public function daya(){
    	return $this->belongsTo('App\Daya','id_daya');
    }

    public function tarif(){
    	return $this->belongsTo('App\Tarif','id_tarif');
    }

    public function bangunan(){
        return $this->belongsTo('App\JenisBangunan','id_bangunan');
    }

    public function penyedia(){
        return $this->belongsTo('App\PenyediaListrik','id_penyedia');
    }

     public function btl(){
        return $this->belongsTo('App\Btl','id_btl');
    }

    public function pembayaran(){
        return $this->hasOne('App\Pembayaran','no_pendaftaran','no_pendaftaran');
    }

    public function pelangganTemp(){
      return $this->belongsTo('App\PelangganTemp', 'id_pelanggan');
    }


}
