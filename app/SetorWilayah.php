<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetorWilayah extends Model
{
    protected $table = 'setor_wilayah';

    protected $fillable = ['id_tagihan', 'id_kwilayah', 'jml_setoran', 'id_users', 'status'];
}
