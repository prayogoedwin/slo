<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Btl extends Model
{
    protected $table = "btl";

    protected $fillable = ['id','nama_btl','id_kwilayah','kode_kota','alamat','email','telp'];

    public function kota(){
    	return $this->belongsTo('App\Kota','kode_kota','kode_kota');
    }

    public function wilayah(){
    	return $this->belongsTo('App\KantorWilayah','id_kwilayah');
    }

    public function asosiasi(){
    	return $this->belongsTo('App\Asosiasi','id_asosiasi');
    }

    public function pelanggan(){
        return $this->hasMany('App\Pelanggan');
    }

}
