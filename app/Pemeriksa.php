<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemeriksa extends Model
{
    protected $table = "pemeriksa";

    protected $fillable = ['no_pendaftaran','no_surat_tugas','no_pemeriksaan','no_lhpp','tgl_lhpp','gambar_instalasi', 'diagram_garis_tunggal','pe_utama','pe_cabang','pe_akhir','pe_kotak_kontak','jenis_peng_utama','jenis_peng_cabang','jenis_peng_akhir','penghantar_bumi_jenis','penghantar_bumi_penampang','penghantar_bumi_sistem','saklar_utama','saklar_cabang1','saklar_cabang2','phbk_utama','phbk_cabang1','phbk_cabang2','penghantar_utama','penghantar_cabang','penghantar_akhir','penghantar_3fasa','fitting_lampu','kotak_kontak','sakelar','tinggi_kotak_kontak','tinggi_phbk','jenis_kotak_kontak','tanda_komponen','pengujian_pembebanan','jml_phb_utama','jml_phb_1fasa','jml_phb_3fasa','jml_phb_cabang','jml_saluran_cabang','jml_saluran_akhir','jml_titik_lampu','jml_sakelar','kkk','kkb','tahanan_isolasi_penghantar','resisten_pembumian','jml_motor_listrik_unit','jml_motor_listrik_kwh','catatan','foto1','foto2','foto3','foto4','foto5','location','lat','lng','status_verifikasi'];
}
