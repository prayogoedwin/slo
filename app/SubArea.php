<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubArea extends Model
{
    protected $table = "sub_area";

    protected $fillable = ['id_kwilayah', 'id_karea', 'nama_subarea', 'kode', 'alamat', 'nama_manager', 'status'];

    public function area(){
    	return $this->belongsTo('App\KantorArea','id_karea');
    }

    public function wilayah(){
    	return $this->belongsTo('App\KantorWilayah','id_kwilayah');
    }

    public function user(){
        return $this->hasMany('App\User');
    }

    public function pelanggan(){
        return $this->hasMany('App\Pelanggan');
    }
}
