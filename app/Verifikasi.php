<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verifikasi extends Model
{
    protected $table = "verifikasi";

    protected $fillable = ['id', 'no_pendaftaran','no_lhpp','pemeriksaan_dokumen','proteksi_sentuh_langung','pemeriksaan_penghantar','pemeriksaan_phb','pemeriksaan_elektroda','pemeriksaan_polaritas','pemeriksaan_pemasangan','pemeriksaan_sni','pemeriksaan_kamar_mandi','catatan','hasil_pemeriksaan','ceklist_ppu1','ceklist_ppu2','hasil_verifikasi','alasan_reject','id_staff'];

    public function verifikator(){
      return $this->belongsTo('App\User', 'id_staff');
    }
}
