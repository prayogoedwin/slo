<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagihanWilayah extends Model
{
    protected $table = 'tagihan_wilayah';

    protected $fillable = ['no_ref', 'id_kwilayah', 'periode', 'id_share', 'rp_share', 'jml_tagihan', 'jml_terbayar', 'jml_hutang'];

}
