<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffBtl extends Model
{
    protected $table = "staff_btl";

    protected $fillable = ['id_users', 'id_btl'];
}
