<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuratTugas extends Model
{
    protected $table = "surat_tugas";

    protected $fillable = ['no_surat', 'id_pemeriksa1', 'id_pemeriksa2', 'id_staff','status','jangka_waktu'];

    public function pelanggan(){
      return $this->hasMany('App\Pelanggan', 'id_pelanggan');
    }

    public function pemeriksa1(){
      return $this->belongsTo('App\User','id_pemeriksa1');
    }

    public function pemeriksa2(){
      return $this->belongsTo('App\User','id_pemeriksa2');
    }

    public function penerbit(){
      return $this->belongsTo('App\User', 'id_staff');
    }
}
