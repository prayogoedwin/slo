<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetorArea extends Model
{
  protected $table = 'setor_area';

  protected $fillable = ['id_tagihan', 'no_ref', 'jml_setoran', 'id_users'];

}
