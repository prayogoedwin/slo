<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagihanArea extends Model
{
    protected $table = 'tagihan_area';

    protected $fillable = ['no_ref', 'id_kwilayah', 'id_karea', 'periode', 'jml_tagihan', 'jml_terbayar', 'jml_hutang'];
}
