<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogStatus extends Model
{
    protected $table = "log_status";

    protected $fillable = ['no_pendaftaran','status','kode_status'];
}
