
<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PT. Sertifikasi Instalasi Prima</title>

  
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/css/app.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style media="screen">
  body {
  background-image: url("paper.gif");
  background-color: #cccccc;
  }
  .main-header .sidebar-toggle:before {
    content: "" !important;
}
</style>

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
</head>
<body class="hold-transition skin-blue fixed sidebar-mini" background="{{asset('/images/sip.jpg')}}">
<!-- Site wrapper -->
<div class="wrapper" id="app">

  <!-- header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo" style="background-color: #222d32">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      {{-- <span class="logo-mini"><b>SIP</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SIP</span> --}}
        <img src=" {{asset('images/logo-bar.png')}} " alt="" srcset="" style="height:32px;object-fit:contain;translate:transformX(-30px);">
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu">
        {{-- <span class="sr-only">Toggle navigation</span> --}}
        <i class="fa fa-bars"></i>
        <span>
          <?php
            // $wilayah =  DB::table('kantor_wilayah')->where('id', Auth::user()->id_kwilayah)->value('nama_wilayah');
            // $area =  DB::table('kantor_area')->where('id', Auth::user()->id_karea)->value('nama');
            // $subarea =  DB::table('sub_area')->where('id', Auth::user()->id_subarea)->value('nama_subarea');
            // echo "$wilayah / $area / $subarea";
        ?> 
        
        </span>
      </a>
      

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <i data-count="0" class="fa fa-bell dropdown-notifications dropdown-toolbar-title"><span class="notif-count">0</span></i>

            </a>
            <ul class="dropdown-menu">
              <li class="header">Pemberitahuan</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#" class="dropdown-toolbar-title">
                      <i class="fa fa-users text-aqua"><font color="black"> Pendaftar Web Baru  (<span class="notif-count">0</span>)</font>
                    </a></i>
                  </li>

                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Messages: style can be found in dropdown.less-->

          <!-- Notifications: style can be found in dropdown.less -->

          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="/images/Default.png" class="user-image" alt="User Image">
              <span class="hidden-xs">{{Auth::user()->nama}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="/images/Default.png" class="img-circle" alt="User Image">

                <p>
                  {{Auth::user()->nama}}
                  <small>{{Auth::user()->departemen}}</small>
                </p>
              </li>
              <!-- Menu Body -->

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <router-link to="/profile" class="btn btn-default btn-flat">Profile</router-link>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">Sign out</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- tutup -->

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!--<div class="user-panel">
        <div class="text-center image">
          <img src="/images/logoku.png" class="img-circle" alt="User Image">
        </div>
      </div>-->
      <!-- search form -->

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU UTAMA</li>
        <li><router-link to="/home"><i class="fa fa-home"></i> <span>Home</span></router-link></li>
        @if(Gate::check('isInternal'))
        <li><router-link to="/share-internal"><i class="fa fa-file-invoice-dollar"></i> <span>Internal</span></router-link></li>
        @endif
        @if(Gate::check('isAdminPusat'))
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><router-link to="/admin-pusat"><i class="fa fa-circle-o"></i> Admin Pusat</router-link></li>
            <li><router-link to="/isiberita"><i class="fa fa-circle-o"></i> Update Berita</router-link></li>

          </ul>
        </li>
      @endif

        @if(Gate::check('isAdmin') || Gate::check('isAdminPusat') || Gate::check('isBtl'))
        <li class="treeview">
          <a href="#">
            <i class="fa fa-briefcase"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><router-link to="/kantor-wilayah"><i class="fa fa-circle-o"></i> Kantor Wilayah</router-link></li>
            <li><router-link to="/kantor-area"><i class="fa fa-circle-o"></i> Kantor Area</router-link></li>
            <li><router-link to="/sub-area"><i class="fa fa-circle-o"></i> Sub Area</router-link></li>
            {{-- <li><router-link to="/jenis-bangunan"><i class="fa fa-circle-o"></i> Jenis Bangunan</router-link></li> --}}
            <li><router-link to="/tarif"><i class="fa fa-circle-o"></i> Tarif</router-link></li>
            <li><router-link to="/daya"><i class="fa fa-circle-o"></i> Daya</router-link></li>
            <li><router-link to="/btl"><i class="fa fa-circle-o"></i> BTL</router-link></li>
            <li><router-link to="/asosiasi"><i class="fa fa-circle-o"></i> Asosiasi</router-link></li>
            <li><router-link to="/penyedia-listrik"><i class="fa fa-circle-o"></i> Penyedia Listrik</router-link></li>
          </ul>
        </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa fa-user-friends"></i>
              <span>Staff</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @if(Gate::check('isAdmin') || Gate::check('isAdminPusat'))
                <li><router-link to="/staff"><i class="fa fa-circle-o"></i> Staff Internal</router-link></li>
              @endif

              <li><router-link to="/staff/btl"><i class="fa fa-circle-o"></i> Staff BTL</router-link></li>
            </ul>
          </li>
        {{-- <li><router-link to="/pelanggan"><i class="fa fa-users"></i> <span>Daftar Pelanggan</router-link></li> --}}
          <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i>
              <span>Pelanggan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @if(Gate::check('isAdmin') || Gate::check('isAdminPusat'))
              <li><router-link to="/pelanggan"><i class="fa fa-circle-o"></i> Daftar Pelanggan</router-link></li>
              @endif
              @if (Gate::check('isBtl'))
                <li><router-link to="/pelanggan-btl"><i class="fa fa-circle-o"></i> Pelanggan BTL</router-link></li>
              @endif
            </ul>
          </li>
         @if(Gate::check('isAdmin') || Gate::check('isAdminPusat'))
        <li class="treeview">
          <a href="#">
            <i class="fa fa fa-money-bill-alt"></i>
            <span>Pembayaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><router-link to="/belum-bayar"><i class="fa fa-circle-o"></i> Belum Bayar</router-link></li>
            <li><router-link to="/sudah-bayar"><i class="fa fa-circle-o"></i> Sudah Bayar</router-link></li>
          </ul>
        </li>
      @endif
       @endif
        @if(Gate::check('isPemeriksa') && Auth::user()->status == 'Aktif')
          <li class="treeview">
            <a href="#">
              <i class="fa fa fa-database"></i>
              <span>Pemeriksaan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><router-link to="/pemeriksaan"><i class="fa fa-circle-o"></i> Belum Diperiksa</router-link></li>
              <li><router-link to="/pemeriksaan/sudah-diperiksa"><i class="fa fa-circle-o"></i> Sudah Diperiksa</router-link></li>
            </ul>
          </li>
        @endif
        @if(Gate::check('isVerifikator') || && Auth::user()->status == 'Aktif')
        <li class="treeview">
          <a href="#">
            <i class="fa fa fa-money-bill-alt"></i>
            <span>VerifikasiTESTT</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><router-link to="/verifikasi"><i class="fa fa-circle-o"></i> Belum Verifikasi</router-link></li>
            <li><router-link to="/sudah-verifikasi"><i class="fa fa-circle-o"></i> Sudah Verifikasi</router-link></li>
          </ul>
        </li>
        @endif
        @if(Gate::check('isAdmin') || Gate::check('isAdminPusat'))
        <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i>
            <span>Sertifikasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><router-link to="/sertifikasi"><i class="fa fa-circle-o"></i> Belum Sertifikasi</router-link></li>
            <li><router-link to="/sertifikasi/sudah-terbit"><i class="fa fa-circle-o"></i> Sudah Sertifikasi</router-link></li>
          </ul>
        </li>
      @endif
      @if(Gate::check('isAdmin') || Gate::check('isAdminPusat'))
      {{-- <li><router-link to="/rekap-keuangan"><i class="fa fa-file-invoice-dollar"></i> <span>Rekap Keuangan</span></router-link></li> --}}
      <li class="treeview">
        <a href="#">
          <i class="fa fa-file-invoice-dollar"></i>
          <span>Menu Keuangan</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @if(Gate::check('isAdminPusat'))
          <li><router-link to="/share-wilayah"><i class="fa fa-circle-o"></i>Share Wilayah</router-link></li>
          <li><router-link to="/setoran-wilayah"><i class="fa fa-circle-o"></i>Setoran Wilayah</router-link></li>
          <li><router-link to="/rekap-penerimaan/pusat"><i class="fa fa-circle-o"></i>Rekap Penerimaan</router-link></li>
          <li><router-link to="/tagihan-dan-piutang/wilayah"><i class="fa fa-circle-o"></i>Tagihan dan Piutang Wilayah</router-link></li>
          @endif
          @if(Gate::check('isAdArea') || Gate::check('isAdSubArea'))
          <li><router-link to="/rekap-penerimaan"><i class="fa fa-circle-o"></i>Rekap Pendapatan</router-link></li>
          @endif
          @if(Gate::check('isAdArea'))
          <li><router-link to="/setoran-sub-area"><i class="fa fa-circle-o"></i>Setoran Sub Area</router-link></li>
          @endif
          @if(Gate::check('isAdWilayah'))
          <li><router-link to="/setoran-area"><i class="fa fa-circle-o"></i>Setoran Area</router-link></li>
          <li><router-link to="/rekap-penerimaan/wilayah"><i class="fa fa-circle-o"></i>Rekap Penerimaan</router-link></li>
          <li><router-link to="/pembayaran-tagihan/wilayah"><i class="fa fa-circle-o"></i>Tagihan dan Hutang</router-link></li>
          @endif
          @if(Gate::check('isAdSubArea'))
          <li><router-link to="/pembayaran-tagihan"><i class="fa fa-circle-o"></i>Tagihan dan Hutang</router-link></li>
          @endif
          @if(Gate::check('isAdArea'))
          <li><router-link to="/pembayaran-tagihan/area"><i class="fa fa-circle-o"></i>Tagihan dan Hutang</router-link></li>
          @endif
        </ul>
      </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-chart-pie"></i>
          <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><router-link to="/lap-pelanggan"><i class="fa fa-circle-o"></i>Laporan Pelanggan</router-link></li>
            <li><router-link to="/lap-pembayaran"><i class="fa fa-circle-o"></i>Laporan Pembayaran</router-link></li>
            <li><router-link to="/lap-pemeriksa"><i class="fa fa-circle-o"></i>Laporan Pemeriksa</router-link></li>
            <li><router-link to="/lap-verifikasi"><i class="fa fa-circle-o"></i>Laporan Verifikasi</router-link></li>
            <li><router-link to="/lap-sertifikasi"><i class="fa fa-circle-o"></i>Laporan Sertifikasi</router-link></li>
          </ul>
        </li>
      @endif
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <router-view></router-view>

    <vue-progress-bar></vue-progress-bar>



  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versi</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2019 <a href="#">PT. Sertifikasi Prima Indonesia</a>.</strong> Hak dilindungi UUD.
  </footer>


  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
@auth
<script>
    window.user = @json(auth()->user())
</script>
@endauth
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script type="text/javascript" src="/js/app.js"></script>
<script src="https://js.pusher.com/4.2/pusher.min.js"></script>
<script type="text/javascript">
      var notificationsWrapper   = $('.notifications-menu');
      var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
      var notificationsCountElem = notificationsToggle.find('i[data-count]');
      var notificationsCount     = parseInt(notificationsCountElem.data('count'));
      var notifications          = notificationsWrapper.find('ul.dropdown-menu');

      if (notificationsCount <= 0) {

      }

      // Enable pusher logging - don't include this in production
      // Pusher.logToConsole = true;

      var pusher = new Pusher('e6ad5cca252093b98277', {
       cluster: 'ap1',
        encrypted: true
      });

      // Subscribe to the channel we specified in our Laravel Event
       var channel = pusher.subscribe('notify');

      // Bind a function to a Event (the full Laravel class)
      channel.bind('notify-event', function(message) {
        var existingNotifications = notifications.html();
        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
        var newNotificationHtml = `

                  <strong class="notification-title"></strong>


        `;
        notifications.html(newNotificationHtml + existingNotifications);

        notificationsCount += 1;
        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsWrapper.find('.notif-count').text(notificationsCount);
        notificationsWrapper.show();
      });
    </script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

</body>
</html>
