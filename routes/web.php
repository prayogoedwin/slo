<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@redirect');

Auth::routes();

// Route::get('password/reset/{token?}','Auth\ResetPasswordController@showResetForm');
// Route::post('password/email', 'Auth\ResetPasswordController@sendResetLinkEmail');
// Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout')->name('logout');
Route::group(['prefix' => 'pdf'], function(){

	Route::get('/kwitansi/print/{id}', 'PDF\PdfController@generateKwitansi')->name('print_kwitansi');
	Route::get('/surat-tugas/print/{id}', 'PDF\PdfController@generateSuratTugas')->name('print_surat');
	Route::get('/verifikasi/print/{no_pendaftaran}', 'PDF\PdfController@generateVerifikasi')->name('print_verifikasi');
	Route::get('/sertifikat/cetak/{no_pendaftaran}', 'PDF\PdfController@generateSertifikasi');

});

Route::group(['prefix' => 'excel'], function(){

	Route::get('/blmbayar/export/', 'EXL\ExcelController@exportBlmByr')->name('export_blmbayar');
	Route::get('/sdhbayar/export/', 'EXL\ExcelController@exportSdhByr')->name('export_sdhbayar');
	Route::get('/pelanggan/import/', 'EXL\ExcelController@import')->name('import_pelanggan');

	Route::get('/lap-pembayaran/export/', 'EXL\ExcelController@lapPembayaran')->name('laporan-pembayaran-export');
	Route::get('/lap-pembayaran/export/csv/', 'EXL\ExcelController@lapPembayaranCSV')->name('laporan-pembayaran-export-csv');

	Route::get('/lap-verifikasi/export/', 'EXL\ExcelController@lapVerifikasi')->name('laporan-verifikasi-export');
	Route::get('/lap-verifikasi/export/csv/', 'EXL\ExcelController@lapVerifikasiCSV')->name('laporan-verifikasi-export-csv');

	Route::get('/lap-verifikasi-filter/export/', 'EXL\ExcelController@lapVerifikasiFilter')->name('laporan-verifikasi-export');
	Route::get('/lap-verifikasi-filter/export/csv/', 'EXL\ExcelController@lapVerifikasiFilterCSV')->name('laporan-verifikasi-export-csv');

	Route::get('/lap-sertifikasi/export/', 'EXL\ExcelController@lapSertifikasi')->name('laporan-sertifikasi-export');
	Route::get('/lap-sertifikasi/export/csv/', 'EXL\ExcelController@lapSertifikasiCSV')->name('laporan-sertifikasi-export-csv');

	Route::get('/lap-pemeriksa/export/', 'EXL\ExcelController@lapPemeriksa')->name('laporan-pemeriksa-export');
	Route::get('/lap-pemeriksa/export/csv/', 'EXL\ExcelController@lapPemeriksaCSV')->name('laporan-pemeriksa-export-csv');

	Route::get('/lap-pelanggan/export/', 'EXL\ExcelController@lapPelanggan')->name('laporan-pelanggan-export');
	Route::get('/lap-pelanggan/export/csv/', 'EXL\ExcelController@lapPelangganCSV')->name('laporan-pelanggan-export-csv');
	Route::get('/lap-pelanggan-filter/export/{tgl_awal}/{tgl_akhir}', 'EXL\ExcelController@lapPelangganFilter');



});
//pregmatch lama ([A-z\d-\/_.]+)?
//Route::get('/{any}', 'HomeController@index')->where('any', '.*');
Route::get('{path}', 'HomeController@index')->where('path', '([a-zA-Z0-9\.\,\-_%=+]+)?');
