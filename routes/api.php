<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\AuthenticationController@login');

Route::group(['namespace' => 'API', 'middleware' => 'api', 'prefix' => 'password'], function() {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});

Route::group(['middleware' => 'auth:api'], function() {

    Route::apiResources([
        'kantor-wilayah' => 'API\KWilayahController',
        'kantor-area' => 'API\KAreaController',
        'asosiasi' => 'API\AsosiasiController',
        // 'jenis-bangunan' => 'API\JenisBangunanController',
        'tarif' => 'API\TarifController',
        'daya' => 'API\DayaController',
        'btl' => 'API\BtlController',
        'sub-area' => 'API\SubAreaController',
        'penyedia-listrik' => 'API\PenyediaListrikController',
        'staff' => 'API\StaffController',
        'pelanggan' => 'API\PelangganController',
        'user' => 'API\ProfileController',
        'pembayaran' => 'API\PembayaranController',
        'pemeriksa' => 'API\PemeriksaanController',
        'sertifikasi' => 'API\SertifikasiController',
        'verifikasi' => 'API\VerifikasiController',
        'berita' => 'API\BeritaController',
        'admin-pusat' => 'API\AdminPusatController',
        'biro-staff' => 'API\StaffBtlController',
        'keuangan/pusat/share-wilayah' => 'API\Keuangan\ShareWilayahController',
    ]);

    Route::get('/khusus/share-internal', 'API\Keuangan\ShareInternalController@index');
    Route::get('/khusus/share-internal/{kode_alokasi}', 'API\Keuangan\ShareInternalController@detail');

    Route::get('/keuangan/tagihan/pusat', 'API\Keuangan\TagihanPusatController@index');
    Route::get('/keuangan/tagihan/pusat/total', 'API\Keuangan\TagihanPusatController@totalTagihan');
    Route::get('/keuangan/tagihan/pusat/filter/{tanggal_x}/{tanggal_y}', 'API\Keuangan\TagihanPusatController@filterTagihan');
    Route::get('/keuangan/tagihan/pusat/filter/total/{tanggal_x}/{tanggal_y}', 'API\Keuangan\TagihanPusatController@filterTotalTagihan');


    Route::get('/keuangan/pusat/tagihan/wilayah', 'API\Keuangan\RekapPusatController@tagihanWilayah');
    Route::get('/keuangan/pusat/pemasukan', 'API\Keuangan\RekapPusatController@totalPendapatan');
    Route::get('/keuangan/pusat/data-penerimaan', 'API\Keuangan\RekapPusatController@dataPenerimaan');
    Route::get('/rekap/pusat', 'API\Keuangan\RekapPusatController@index');
    Route::post('/rekap/pusat', 'API\Keuangan\RekapPusatController@store');
    Route::get('/keuangan/pusat/filter/{tanggal_x}/{tanggal_y}', 'API\Keuangan\RekapPusatController@filter');
    Route::get('/keuangan/pusat/filter/hitung/{tanggal_x}/{tanggal_y}', 'API\Keuangan\RekapPusatController@hitungPenerimaan');
    Route::get('/keuangan/pusat/filter/data-penerima/{tanggal_x}/{tanggal_y}', 'API\Keuangan\RekapPusatController@FilterDataPenerimaan');


    Route::get('/keuangan/total-tagihan/wilayah', 'API\Keuangan\TagihanWilayahController@totalSeluruhTagihan');
    Route::get('/keuangan/pusat/daftar-wilayah', 'API\Keuangan\TagihanWilayahController@loadWilayah');
    Route::get('/keuangan/setoran/wilayah', 'API\Keuangan\TagihanWilayahController@getSetoran');
    Route::get('/keuangan/cek-tagihan/wilayah/{id}', 'API\Keuangan\TagihanWilayahController@cekTagihan');
    Route::get('/keuangan/cek-tagihan/total/wilayah/{id}', 'API\Keuangan\TagihanWilayahController@cekTotalTagihan');
    Route::post('/keuangan/setoran/wilayah/konfirm/{id}', 'API\Keuangan\TagihanWilayahController@konfirmSetoran');
    Route::get('/keuangan/pusat/filter/hitung', 'API\Keuangan\RekapPusatController@penerimaan');

    Route::get('/keuangan/tagihan/kantor-wilayah', 'API\Keuangan\TagihanWilayahController@index');
    Route::get('/keuangan/tagihan/kantor-wilayah/total', 'API\Keuangan\TagihanWilayahController@totalTagihan');
    Route::get('/keuangan/tagihan/kantor-wilayah/filter/{tanggal_x}/{tanggal_y}', 'API\Keuangan\TagihanWilayahController@filterTagihan');
    Route::get('/keuangan/tagihan/kantor-wilayah/filter/total/{tanggal_x}/{tanggal_y}', 'API\Keuangan\TagihanWilayahController@filterTotalTagihan');
    Route::post('/keuangan/tagihan/kantor-wilayah/setor/{id}', 'API\Keuangan\TagihanWilayahController@store');
    Route::get('/keuangan/tagihan/kantor-wilayah/pembayaran/{id}', 'API\Keuangan\TagihanWilayahController@cekPembayaran');

    Route::get('/rekap/kantor-wilayah', 'API\Keuangan\RekapWilayahController@index');
    Route::post('/rekap/kantor-wilayah', 'API\Keuangan\RekapWilayahController@store');
    Route::get('/keuangan/kantor-wilayah/tagihan/area', 'API\Keuangan\RekapWilayahController@tagihanArea');
    Route::get('/keuangan/share_wilayah/', 'API\Keuangan\RekapWilayahController@getShareWilayah');


    Route::get('/keuangan/kantor-wilayah/daftar-area', 'API\Keuangan\TagihanAreaController@loadArea');
    Route::get('/keuangan/kantor-wilayah/filter/hitung', 'API\Keuangan\RekapWilayahController@penerimaan');
    Route::get('/keuangan/kantor-wilayah/pemasukan', 'API\Keuangan\RekapWilayahController@totalPendapatan');
    Route::get('/keuangan/kantor-wilayah/data-penerimaan', 'API\Keuangan\RekapWilayahController@dataPenerimaan');
    Route::get('/keuangan/kantor-wilayah/filter/{tanggal_x}/{tanggal_y}', 'API\Keuangan\RekapWilayahController@filter');
    Route::get('/keuangan/kantor-wilayah/filter/hitung/{tanggal_x}/{tanggal_y}', 'API\Keuangan\RekapWilayahController@hitungPenerimaan');
    Route::get('/keuangan/kantor-wilayah/filter/data-penerima/{tanggal_x}/{tanggal_y}', 'API\Keuangan\RekapWilayahController@FilterDataPenerimaan');


    Route::get('/keuangan/total-tagihan/area', 'API\Keuangan\TagihanAreaController@totalSeluruhTagihan');
    Route::get('/keuangan/cek-tagihan/area/{id}', 'API\Keuangan\TagihanAreaController@cekTagihan');
    Route::get('/keuangan/cek-tagihan/total/area/{id}', 'API\Keuangan\TagihanAreaController@cekTotalTagihan');
    Route::get('/keuangan/setoran/area', 'API\Keuangan\TagihanAreaController@getSetoran');
    Route::post('/keuangan/setoran/area/konfirm/{id}', 'API\Keuangan\TagihanAreaController@konfirmSetoran');

    Route::get('/keuangan/tagihan/kantor-area', 'API\Keuangan\TagihanAreaController@index');
    Route::get('/keuangan/tagihan/kantor-area/total', 'API\Keuangan\TagihanAreaController@totalTagihan');
    Route::get('/keuangan/tagihan/kantor-area/filter/{tanggal_x}/{tanggal_y}', 'API\Keuangan\TagihanAreaController@filterTagihan');
    Route::get('/keuangan/tagihan/kantor-area/filter/total/{tanggal_x}/{tanggal_y}', 'API\Keuangan\TagihanAreaController@filterTotalTagihan');
    Route::post('/keuangan/tagihan/kantor-area/setor/{id}', 'API\Keuangan\TagihanAreaController@store');
    Route::get('/keuangan/tagihan/kantor-area/pembayaran/{id}', 'API\Keuangan\TagihanAreaController@cekPembayaran');
    Route::get('/keuangan/pemasukan/sub-area', 'API\Keuangan\PemasukanController@pendapatanSubArea');

    Route::get('/keuangan/kantor-area/daftar-sub-area', 'API\Keuangan\TagihanSubAreaController@loadSubArea');
    Route::get('/keuangan/setoran/sub-area', 'API\Keuangan\TagihanSubAreaController@getSetoran');
    Route::post('/keuangan/setoran/sub-area/konfirm/{id}', 'API\Keuangan\TagihanSubAreaController@konfirmSetoran');
    Route::post('/keuangan/setoran/sub-area/tolak/{id}', 'API\Keuangan\TagihanSubAreaController@tolakSetoran');
    Route::get('/keuangan/cek-tagihan/sub-area/{id}', 'API\Keuangan\TagihanSubAreaController@cekTagihan');
    Route::get('/keuangan/cek-tagihan/total/sub-area/{id}', 'API\Keuangan\TagihanSubAreaController@cekTotalTagihan');
    Route::get('/keuangan/total-tagihan/sub-area', 'API\Keuangan\TagihanSubAreaController@totalSeluruhTagihan');

    Route::get('/keuangan/tagihan/pembayaran/{id}', 'API\Keuangan\TagihanSubAreaController@cekPembayaran');
    Route::post('/keuangan/tagihan/setor/{id}', 'API\Keuangan\TagihanSubAreaController@store');
    Route::get('/keuangan/tagihan/detail', 'API\Keuangan\TagihanSubAreaController@tagihanQ');
    Route::get('/keuangan/tagihan', 'API\Keuangan\TagihanSubAreaController@index');
    Route::get('/keuangan/tagihan/total', 'API\Keuangan\TagihanSubAreaController@totalTagihan');
    Route::get('/keuangan/tagihan/filter/{tanggal_x}/{tanggal_y}', 'API\Keuangan\TagihanSubAreaController@filterTagihan');
    Route::get('/keuangan/tagihan/filter/total/{tanggal_x}/{tanggal_y}', 'API\Keuangan\TagihanSubAreaController@filterTotalTagihan');

    Route::get('/rekap/kantor-area', 'API\Keuangan\RekapAreaController@index');
    Route::post('/rekap/kantor-area', 'API\Keuangan\RekapAreaController@store');

    Route::get('/rekap/sub-area', 'API\Keuangan\RekapSubAreaController@index');
    Route::post('/rekap/sub-area', 'API\Keuangan\RekapSubAreaController@store');

    Route::get('/cek-kantor', 'API\SummaryController@cekKantor');
    Route::get('/keuangan/pemasukan', 'API\Keuangan\PemasukanController@totalPendapatan');
    Route::get('/keuangan/filter/{tanggal_x}/{tanggal_y}', 'API\Keuangan\PemasukanController@filter');
    Route::get('/keuangan/filter/hitung/{tanggal_x}/{tanggal_y}', 'API\Keuangan\PemasukanController@hitungPenerimaan');
    Route::get('/keuangan/filter/hitung', 'API\Keuangan\PemasukanController@penerimaan');
    Route::get('/keuangan/data-penerimaan', 'API\Keuangan\PemasukanController@dataPenerimaan');
    Route::get('/keuangan/filter/data-penerima/{tanggal_x}/{tanggal_y}', 'API\Keuangan\PemasukanController@FilterDataPenerimaan');

    Route::get('biro-pelanggan', 'API\PelangganController@pelangganBtl');
    Route::get('/pelanggan/edit/{no_pendaftaran}', 'API\PelangganController@detailPelanggan');
    Route::post('/pelanggan/edit/{no_pendaftaran}', 'API\PelangganController@updatePelanggan');

    Route::get('/filter-tgl-verif/{tgl_awal}/{tgl_akhir}', 'API\VerifikasiController@filterVerif');
    Route::get('/filter-tgl-pelanggan/{tgl_awal}/{tgl_akhir}', 'API\PelangganController@filterPelanggan');
    Route::get('/filter-tgl-pembayaran/{tgl_awal}/{tgl_akhir}', 'API\PembayaranController@filterPembayaran');
    Route::get('/filter-tgl-pemeriksa/{tgl_awal}/{tgl_akhir}', 'API\PemeriksaanController@filterPemeriksa');
    Route::get('/filter-tgl-sertif/{tgl_awal}/{tgl_akhir}', 'API\SertifikasiController@filterSertifikasi');


    Route::get('/masa-berlaku', 'API\SummaryController@masaBerlaku');
    Route::get('/penanda-tangan', 'API\SertifikasiController@penandaTangan');

    Route::get('sertif/sudah-terbit', 'API\SertifikasiController@sudahTerbit');
    Route::get('pelanggan/request-agenda/{no_pendaftaran}', 'API\PelangganController@reqNoAgenda');
    Route::get('pelanggan/ubah-daya/{no_pendaftaran}', 'API\PelangganController@ubahDaya');
    Route::match(['put', 'patch'], 'pelanggan/ubah-daya/{no_pendaftaran}', 'API\PelangganController@updateDaya');


    Route::get('sertifikasi/add/{no_pendaftaran}', 'API\SertifikasiController@registrasiSertif');
    Route::get('sertifikasi/load-pjt/{no_pendaftaran}', 'API\SertifikasiController@loadPjt');
    Route::get('sertifikasi/load-tt/{no_pendaftaran}', 'API\SertifikasiController@loadTT');
    Route::get('sudah-verifikasi', 'API\VerifikasiController@sudahVerif');
    Route::get('proses-verif/add/{no_pendaftaran}', 'API\VerifikasiController@prosesVerif');
    Route::get('edit-verif/edit/{no_pendaftaran}', 'API\VerifikasiController@getEditVerif');
    Route::get('data-verif/{no_pendaftaran}', 'API\VerifikasiController@dataEditVerif');

    Route::post('verifikasi/edit/{no_pendaftaran}', 'API\VerifikasiController@updateVerifikasi');

    Route::get('sudah-diperiksa', 'API\PemeriksaanController@dahDiperiksa');
    Route::get('pelanggan-web', 'API\PelangganController@calonWeb');
    Route::get('/pelanggan-web/detail/{id}', 'API\PelangganController@detailWeb');
    Route::delete('/pelanggan-web/delete/{id}', 'API\PelangganController@deleteWeb');
    Route::get('/konfirmasi-periksa/{id}', 'API\PemeriksaanController@konfirmPeriksa');
    Route::get('/pemeriksaan/add/{no_pendaftaran}', 'API\PemeriksaanController@prosesPemeriksaan');
    Route::post('edit-data/pemeriksaan/{no_pendaftaran}', 'API\PemeriksaanController@editPeriksa');
    Route::get('pemeriksaan/edit/{no_pendaftaran}', 'API\PemeriksaanController@getForEdit');

    Route::get('/generate-lhpp', 'API\PemeriksaanController@generateNoLHPP');
    Route::get('/suratku', 'API\SuratTugasController@suratKu');
    Route::get('/suratku/detail/{id}', 'API\SuratTugasController@detailSurat');
    Route::post('/suratku/selesaikan/{id}', 'API\SuratTugasController@selesaikan');
    Route::get('/suratku/sudah-selesai/', 'API\SuratTugasController@sudahSelesai');
    Route::get('/suratku/sudah-selesai/detail/{id}', 'API\SuratTugasController@sudahDetail');
    Route::get('/suratku/kirim-notif/{id}', 'API\SuratTugasController@kirimNotif');

    Route::get('/hitung-surat', 'API\SuratTugasController@countTemp');
    Route::get('profile/app', 'API\ProfileController@profileApps');
    Route::get('/belum-terbit', 'API\SuratTugasController@belumTerbit');
    Route::get('/get-surat', 'API\SuratTugasController@loadSurat');
    Route::post('/terbitkan-surat', 'API\SuratTugasController@terbitkanSurat');
    Route::get('/load-pemeriksa', 'API\StaffController@loadPemeriksa');
    Route::post('/hapus-temp/{id}', 'API\SuratTugasController@deleteTemp');
    Route::get('/load-surat', 'API\SuratTugasController@temp');
    Route::post('/tambah-surat/{id}', 'API\SuratTugasController@tambahSurat');
    Route::get('/sudah-bayar', 'API\PembayaranController@sudahBayar');
    Route::get('/belum-bayar', 'API\PembayaranController@belumBayar');

    Route::post('/update-cetak/{id}', 'API\PembayaranController@updateCetak');
    Route::get('/pembayaran/detail/{id}', 'API\PembayaranController@dtBelumBayar');

    Route::get('logout', 'API\AuthenticationController@logout');

    Route::get('load-wilayah', 'API\StaffController@loadWilayah');

    Route::post('profile/edit', 'API\ProfileController@updateProfile');

    Route::get('profile', 'API\ProfileController@profile');
    Route::post('edit-password', 'API\ProfileController@changePassword');

    Route::post('edit-password', 'API\ProfileController@changePassword');
    Route::get('load-daya', 'API\DayaController@loadDaya');
    Route::get('load-kota', 'API\IndonesiaController@loadKota');
    Route::get('data-provinsi', 'API\IndonesiaController@loadProvinsi');
    Route::get('load-tarif', 'API\TarifController@loadTarif');

    Route::get('load-bangunan', 'API\JenisBangunanController@loadBangunan');
    Route::get('load-asosiasi', 'API\AsosiasiController@loadAsosiasi');
    Route::get('data-wilayah', 'API\KWilayahController@loadWilayah');

    Route::get('data-kotaprov/{kode_prov}', 'API\IndonesiaController@loadKotaProv');
    Route::get('data-provwilayah', 'API\KWilayahController@loadProvinsi');

    Route::get('data-asosiasi', 'API\AsosiasiController@loadAsosiasi');

    Route::get('get-area/{id_kwilayah}', 'API\KAreaController@getArea');

    Route::get('load-area/{id_kwilayah}', 'API\KAreaController@loadArea');
    Route::get('get-kanwil', 'API\KWilayahController@getKanWil');

    Route::get('nokodefikasi', 'API\BtlController@generateKode');
    Route::get('load-subarea/{id_karea}', 'API\SubAreaController@loadSubArea');

    Route::get('load-biro/{id_kwilayah}', 'API\BtlController@loadBiro');
    Route::get('load-penyedia/{id_karea}', 'API\PenyediaListrikController@loadPenyedia');

    Route::get('berita-terkini', 'API\SummaryController@berita');
    Route::get('summary-pelanggan', 'API\SummaryController@pelanggan');
    Route::get('summary-pembayaran', 'API\SummaryController@pembayaran');
    Route::get('summary-pemeriksaan', 'API\SummaryController@pemeriksaan');
    Route::get('summary-verifikasi', 'API\SummaryController@verifikasi');
    Route::get('summary-sertifikasi', 'API\SummaryController@sertifikasi');

    Route::get('lap-pembayaran', 'API\PembayaranController@lapPembayaran');
    Route::get('lap-pelanggan', 'API\PelangganController@lapPelanggan');
});

Route::get('load-query', 'API\Keuangan\PemasukanController@bulan');
Route::get('nomor', 'API\SummaryController@nomor');

// Route::get('sertifikasi','API\SertifikasiController@index');

Route::get('lap-pemeriksa', 'API\PemeriksaanController@lapPemeriksa');
Route::get('notifikasi', 'API\SuratTugasController@test');

