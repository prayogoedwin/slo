--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Berita; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE "Berita" (
    id uuid NOT NULL,
    read_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    gambar character varying(100),
    jenis character varying(50),
    judul character varying(100),
    isi text
);


ALTER TABLE public."Berita" OWNER TO sertifik;

--
-- Name: adminfrontend; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE adminfrontend (
    email character varying,
    password text
);


ALTER TABLE public.adminfrontend OWNER TO sertifik;

--
-- Name: alokasi_share; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE alokasi_share (
    id integer NOT NULL,
    kode_alokasi character varying(255) NOT NULL,
    periode date NOT NULL,
    pendapatan_kotor numeric(8,2) NOT NULL,
    alokasi_pengembangan numeric(8,2) NOT NULL,
    jumlah_share numeric(8,2) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.alokasi_share OWNER TO sertifik;

--
-- Name: alokasi_share_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE alokasi_share_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alokasi_share_id_seq OWNER TO sertifik;

--
-- Name: alokasi_share_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE alokasi_share_id_seq OWNED BY alokasi_share.id;


--
-- Name: asosiasi; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE asosiasi (
    id integer NOT NULL,
    nama_asosiasi character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.asosiasi OWNER TO sertifik;

--
-- Name: asosiasi_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE asosiasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.asosiasi_id_seq OWNER TO sertifik;

--
-- Name: asosiasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE asosiasi_id_seq OWNED BY asosiasi.id;


--
-- Name: informasi; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE informasi (
    id integer NOT NULL,
    berita text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.informasi OWNER TO sertifik;

--
-- Name: berita_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE berita_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.berita_id_seq OWNER TO sertifik;

--
-- Name: berita_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE berita_id_seq OWNED BY informasi.id;


--
-- Name: berita; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE berita (
    id integer DEFAULT nextval('berita_id_seq'::regclass) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    judul character varying,
    isi text,
    jenis character varying,
    gambar character varying
);


ALTER TABLE public.berita OWNER TO sertifik;

--
-- Name: btl; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE btl (
    id integer NOT NULL,
    nama_btl character varying(255) NOT NULL,
    id_kwilayah integer NOT NULL,
    kode_kota integer NOT NULL,
    alamat character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    telp character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.btl OWNER TO sertifik;

--
-- Name: btl_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE btl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.btl_id_seq OWNER TO sertifik;

--
-- Name: btl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE btl_id_seq OWNED BY btl.id;


--
-- Name: calon_pelanggan_web; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE calon_pelanggan_web (
    id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    nama character varying(100),
    email character varying(100),
    no_telepon character varying(20),
    alamat character varying(255),
    tipe character varying,
    status character varying,
    kode_prov integer,
    kode_kota integer,
    id_kwilayah integer,
    status_daya character varying(50)
);


ALTER TABLE public.calon_pelanggan_web OWNER TO sertifik;

--
-- Name: daya; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE daya (
    id integer NOT NULL,
    daya character varying(255) NOT NULL,
    jenis_fasa character varying(255) NOT NULL,
    harga numeric(8,0) DEFAULT 0::numeric,
    biaya numeric(8,0) DEFAULT 0::numeric,
    ppn numeric(8,0) DEFAULT 0::numeric,
    total numeric(8,0) DEFAULT 0::numeric NOT NULL,
    terbilang character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.daya OWNER TO sertifik;

--
-- Name: daya_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE daya_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.daya_id_seq OWNER TO sertifik;

--
-- Name: daya_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE daya_id_seq OWNED BY daya.id;


--
-- Name: galeri; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE galeri (
    id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    judul character varying,
    gambar character varying
);


ALTER TABLE public.galeri OWNER TO sertifik;

--
-- Name: galeri_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE galeri_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.galeri_id_seq OWNER TO sertifik;

--
-- Name: galeri_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE galeri_id_seq OWNED BY galeri.id;


--
-- Name: jenis_bangunan; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE jenis_bangunan (
    id integer NOT NULL,
    jenis_bangunan character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.jenis_bangunan OWNER TO sertifik;

--
-- Name: jenis_bangunan_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE jenis_bangunan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jenis_bangunan_id_seq OWNER TO sertifik;

--
-- Name: jenis_bangunan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE jenis_bangunan_id_seq OWNED BY jenis_bangunan.id;


--
-- Name: kantor_area; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE kantor_area (
    id integer NOT NULL,
    id_kwilayah integer NOT NULL,
    kode_kota integer NOT NULL,
    nama character varying(255) NOT NULL,
    manager character varying(255) NOT NULL,
    pjt character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.kantor_area OWNER TO sertifik;

--
-- Name: kantor_area_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE kantor_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kantor_area_id_seq OWNER TO sertifik;

--
-- Name: kantor_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE kantor_area_id_seq OWNED BY kantor_area.id;


--
-- Name: kantor_wilayah; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE kantor_wilayah (
    id integer NOT NULL,
    nama_wilayah character varying(255) NOT NULL,
    general_manager character varying(255) NOT NULL,
    kode_prov integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    alamat character varying(255),
    telp character varying,
    email character varying,
    nomor integer
);


ALTER TABLE public.kantor_wilayah OWNER TO sertifik;

--
-- Name: kantor_wilayah_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE kantor_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kantor_wilayah_id_seq OWNER TO sertifik;

--
-- Name: kantor_wilayah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE kantor_wilayah_id_seq OWNED BY kantor_wilayah.id;


--
-- Name: kanwil; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE kanwil (
    kantor_wilayah character varying(100),
    general_manager character varying(50),
    alamat character varying(255),
    id integer
);


ALTER TABLE public.kanwil OWNER TO sertifik;

--
-- Name: kota; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE kota (
    id integer NOT NULL,
    kode_kota integer NOT NULL,
    kode_prov integer NOT NULL,
    kota character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.kota OWNER TO sertifik;

--
-- Name: kota_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE kota_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kota_id_seq OWNER TO sertifik;

--
-- Name: kota_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE kota_id_seq OWNED BY kota.id;


--
-- Name: log_status; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE log_status (
    id integer NOT NULL,
    no_pendaftaran character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    kode_status integer
);


ALTER TABLE public.log_status OWNER TO sertifik;

--
-- Name: log_status_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE log_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_status_id_seq OWNER TO sertifik;

--
-- Name: log_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE log_status_id_seq OWNED BY log_status.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO sertifik;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO sertifik;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE notifications (
    id uuid NOT NULL,
    type character varying(255) NOT NULL,
    notifiable_id integer NOT NULL,
    notifiable_type character varying(255) NOT NULL,
    data text NOT NULL,
    read_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.notifications OWNER TO sertifik;

--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE oauth_access_tokens (
    id character varying(100) NOT NULL,
    user_id integer,
    client_id integer NOT NULL,
    name character varying(255),
    scopes text,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_access_tokens OWNER TO sertifik;

--
-- Name: oauth_auth_codes; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE oauth_auth_codes (
    id character varying(100) NOT NULL,
    user_id integer NOT NULL,
    client_id integer NOT NULL,
    scopes text,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_auth_codes OWNER TO sertifik;

--
-- Name: oauth_clients; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE oauth_clients (
    id integer NOT NULL,
    user_id integer,
    name character varying(255) NOT NULL,
    secret character varying(100) NOT NULL,
    redirect text NOT NULL,
    personal_access_client boolean NOT NULL,
    password_client boolean NOT NULL,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_clients OWNER TO sertifik;

--
-- Name: oauth_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE oauth_clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth_clients_id_seq OWNER TO sertifik;

--
-- Name: oauth_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE oauth_clients_id_seq OWNED BY oauth_clients.id;


--
-- Name: oauth_personal_access_clients; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE oauth_personal_access_clients (
    id integer NOT NULL,
    client_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_personal_access_clients OWNER TO sertifik;

--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE oauth_personal_access_clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth_personal_access_clients_id_seq OWNER TO sertifik;

--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE oauth_personal_access_clients_id_seq OWNED BY oauth_personal_access_clients.id;


--
-- Name: oauth_refresh_tokens; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE oauth_refresh_tokens (
    id character varying(100) NOT NULL,
    access_token_id character varying(100) NOT NULL,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_refresh_tokens OWNER TO sertifik;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE password_resets (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO sertifik;

--
-- Name: password_resets_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE password_resets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.password_resets_id_seq OWNER TO sertifik;

--
-- Name: password_resets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE password_resets_id_seq OWNED BY password_resets.id;


--
-- Name: pelanggan; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE pelanggan (
    id integer NOT NULL,
    no_pendaftaran character varying(255) NOT NULL,
    no_agenda character varying(255),
    nama character varying(255) NOT NULL,
    no_identitas character varying(255) NOT NULL,
    no_telepon character varying(255) NOT NULL,
    alamat character varying(255) NOT NULL,
    id_kwilayah integer,
    kode_kota integer,
    id_karea integer,
    id_subarea integer,
    telp integer,
    id_tarif integer,
    id_daya integer,
    id_btl integer,
    id_bangunan integer,
    id_penyedia integer,
    tipe character varying(255),
    status character varying(255),
    no_gambar integer,
    tgl_gambar date,
    id_asosiasi integer,
    no_agendapln character varying(255),
    tgl_regpln date,
    diinput_oleh character varying(255),
    status_temp character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.pelanggan OWNER TO sertifik;

--
-- Name: pelanggan_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE pelanggan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pelanggan_id_seq OWNER TO sertifik;

--
-- Name: pelanggan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE pelanggan_id_seq OWNED BY pelanggan.id;


--
-- Name: pelanggan_temp; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE pelanggan_temp (
    id integer NOT NULL,
    id_pelanggan integer NOT NULL,
    id_tarif integer NOT NULL,
    id_daya integer NOT NULL,
    id_penyedia integer NOT NULL,
    id_staff integer NOT NULL,
    no_surat character varying(255),
    status_terbit integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id_subarea integer,
    status_periksa integer
);


ALTER TABLE public.pelanggan_temp OWNER TO sertifik;

--
-- Name: pelanggan_temp_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE pelanggan_temp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pelanggan_temp_id_seq OWNER TO sertifik;

--
-- Name: pelanggan_temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE pelanggan_temp_id_seq OWNED BY pelanggan_temp.id;


--
-- Name: pembayaran; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE pembayaran (
    id integer NOT NULL,
    no_pendaftaran character varying(255) NOT NULL,
    no_kwitansi character varying(255) NOT NULL,
    diterima character varying(255) NOT NULL,
    cetak_kwitansi integer,
    cetak character varying(255),
    dicetak_oleh character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    jumlah numeric,
    tipe character varying,
    tanggal date
);


ALTER TABLE public.pembayaran OWNER TO sertifik;

--
-- Name: pembayaran_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE pembayaran_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pembayaran_id_seq OWNER TO sertifik;

--
-- Name: pembayaran_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE pembayaran_id_seq OWNED BY pembayaran.id;


--
-- Name: pemeriksa; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE pemeriksa (
    id integer NOT NULL,
    no_pendaftaran character varying(255) NOT NULL,
    no_surat_tugas character varying(255) NOT NULL,
    no_pemeriksaan character varying(255) NOT NULL,
    no_lhpp character varying(255),
    tgl_lhpp character varying,
    gambar_instalasi character varying(255),
    diagram_garis_tunggal character varying(255),
    pe_utama character varying(255),
    pe_cabang character varying(255),
    pe_akhir character varying(255),
    pe_kotak_kontak character varying(255),
    jenis_peng_utama character varying(255),
    jenis_peng_cabang character varying(255),
    jenis_peng_akhir character varying(255),
    penghantar_bumi_jenis character varying(255),
    penghantar_bumi_penampang character varying(255),
    penghantar_bumi_sistem character varying(255),
    saklar_utama character varying(255),
    saklar_cabang1 character varying(255),
    saklar_cabang2 character varying(255),
    phbk_utama character varying(255),
    phbk_cabang1 character varying(255),
    phbk_cabang2 character varying(255),
    penghantar_utama character varying(255),
    penghantar_cabang character varying(255),
    penghantar_akhir character varying(255),
    penghantar_3fasa character varying(255),
    fitting_lampu character varying(255),
    kotak_kontak character varying(255),
    sakelar character varying(255),
    tinggi_kotak_kontak character varying(255),
    tinggi_phbk character varying(255),
    jenis_kotak_kontak character varying(255),
    tanda_komponen character varying(255),
    pengujian_pembebanan character varying(255),
    jml_phb_utama character varying(255),
    jml_phb_1fasa character varying(255),
    jml_phb_3fasa character varying(255),
    jml_phb_cabang character varying(255),
    jml_saluran_cabang character varying(255),
    jml_saluran_akhir character varying(255),
    jml_titik_lampu character varying(255),
    jml_sakelar character varying(255),
    kkb character varying(255),
    kkk character varying(255),
    tahanan_isolasi_penghantar character varying(255),
    resisten_pembumian character varying(255),
    jml_motor_listrik_unit character varying(255),
    jml_motor_listrik_kwh character varying(255),
    catatan character varying(255),
    foto1 character varying(255),
    foto2 character varying(255),
    foto3 character varying(255),
    foto4 character varying(255),
    location character varying(255),
    lat double precision,
    lng double precision,
    status_verifikasi integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.pemeriksa OWNER TO sertifik;

--
-- Name: pemeriksa_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE pemeriksa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pemeriksa_id_seq OWNER TO sertifik;

--
-- Name: pemeriksa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE pemeriksa_id_seq OWNED BY pemeriksa.id;


--
-- Name: penyedia_listrik; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE penyedia_listrik (
    id integer NOT NULL,
    nama_penyedia character varying(255) NOT NULL,
    kode_pln character varying(255) NOT NULL,
    id_kwilayah integer NOT NULL,
    id_karea integer NOT NULL,
    alamat character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.penyedia_listrik OWNER TO sertifik;

--
-- Name: penyedia_listrik_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE penyedia_listrik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.penyedia_listrik_id_seq OWNER TO sertifik;

--
-- Name: penyedia_listrik_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE penyedia_listrik_id_seq OWNED BY penyedia_listrik.id;


--
-- Name: perangkat; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE perangkat (
    id integer NOT NULL,
    id_users integer NOT NULL,
    username character varying(255) NOT NULL,
    token text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.perangkat OWNER TO sertifik;

--
-- Name: perangkat_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE perangkat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.perangkat_id_seq OWNER TO sertifik;

--
-- Name: perangkat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE perangkat_id_seq OWNED BY perangkat.id;


--
-- Name: provinsi; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE provinsi (
    id integer NOT NULL,
    kode_prov integer NOT NULL,
    provinsi character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.provinsi OWNER TO sertifik;

--
-- Name: provinsi_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE provinsi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.provinsi_id_seq OWNER TO sertifik;

--
-- Name: provinsi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE provinsi_id_seq OWNED BY provinsi.id;


--
-- Name: rekap_area; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE rekap_area (
    id integer NOT NULL,
    id_kwilayah integer NOT NULL,
    id_karea integer NOT NULL,
    bulan date NOT NULL,
    pendapatan_pelanggan numeric(10,0) NOT NULL,
    pendapatan_sub_area numeric(10,0) DEFAULT 0,
    total numeric(10,0) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rekap_area OWNER TO sertifik;

--
-- Name: rekap_area_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE rekap_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rekap_area_id_seq OWNER TO sertifik;

--
-- Name: rekap_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE rekap_area_id_seq OWNED BY rekap_area.id;


--
-- Name: rekap_pusat; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE rekap_pusat (
    id integer NOT NULL,
    periode date NOT NULL,
    total_penerimaan numeric(10,0) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rekap_pusat OWNER TO sertifik;

--
-- Name: rekap_pusat_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE rekap_pusat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rekap_pusat_id_seq OWNER TO sertifik;

--
-- Name: rekap_pusat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE rekap_pusat_id_seq OWNED BY rekap_pusat.id;


--
-- Name: rekap_sub_area; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE rekap_sub_area (
    id integer NOT NULL,
    id_karea integer NOT NULL,
    id_subarea integer NOT NULL,
    bulan date NOT NULL,
    total numeric(10,0) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rekap_sub_area OWNER TO sertifik;

--
-- Name: rekap_sub_area_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE rekap_sub_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rekap_sub_area_id_seq OWNER TO sertifik;

--
-- Name: rekap_sub_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE rekap_sub_area_id_seq OWNED BY rekap_sub_area.id;


--
-- Name: rekap_wilayah; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE rekap_wilayah (
    id integer NOT NULL,
    id_kwilayah integer NOT NULL,
    periode date NOT NULL,
    total_penerimaan numeric(10,0) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rekap_wilayah OWNER TO sertifik;

--
-- Name: rekap_wilayah_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE rekap_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rekap_wilayah_id_seq OWNER TO sertifik;

--
-- Name: rekap_wilayah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE rekap_wilayah_id_seq OWNED BY rekap_wilayah.id;


--
-- Name: runingtext_frontend; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE runingtext_frontend (
    id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    judul character varying,
    isi character varying
);


ALTER TABLE public.runingtext_frontend OWNER TO sertifik;

--
-- Name: runingtext_frontend_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE runingtext_frontend_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.runingtext_frontend_id_seq OWNER TO sertifik;

--
-- Name: runingtext_frontend_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE runingtext_frontend_id_seq OWNED BY runingtext_frontend.id;


--
-- Name: sertifikasi; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE sertifikasi (
    id integer NOT NULL,
    no_pendaftaran character varying(255) NOT NULL,
    no_agenda character varying(255) NOT NULL,
    no_lhpp character varying(255) NOT NULL,
    no_registrasi character varying(255) NOT NULL,
    no_sertifikat character varying(255) NOT NULL,
    no_seri integer NOT NULL,
    pjt_cetak integer NOT NULL,
    tt_cetak character varying(255) NOT NULL,
    tgl_cetak date NOT NULL,
    masa_berlaku date NOT NULL,
    penanda_tangan character varying(255) NOT NULL,
    dicetak_oleh integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.sertifikasi OWNER TO sertifik;

--
-- Name: sertifikasi_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE sertifikasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sertifikasi_id_seq OWNER TO sertifik;

--
-- Name: sertifikasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE sertifikasi_id_seq OWNED BY sertifikasi.id;


--
-- Name: setor_area; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE setor_area (
    id integer NOT NULL,
    id_tagihan integer NOT NULL,
    no_ref character varying(255) NOT NULL,
    jml_setoran numeric(10,0) NOT NULL,
    id_users integer NOT NULL,
    status integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.setor_area OWNER TO sertifik;

--
-- Name: setor_area_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE setor_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.setor_area_id_seq OWNER TO sertifik;

--
-- Name: setor_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE setor_area_id_seq OWNED BY setor_area.id;


--
-- Name: setor_sub_area; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE setor_sub_area (
    id integer NOT NULL,
    id_tagihan integer NOT NULL,
    no_ref character varying(255) NOT NULL,
    jml_setoran numeric(10,0) NOT NULL,
    id_users integer NOT NULL,
    status integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.setor_sub_area OWNER TO sertifik;

--
-- Name: setor_sub_area_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE setor_sub_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.setor_sub_area_id_seq OWNER TO sertifik;

--
-- Name: setor_sub_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE setor_sub_area_id_seq OWNED BY setor_sub_area.id;


--
-- Name: setor_wilayah; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE setor_wilayah (
    id integer NOT NULL,
    no_ref character varying(255) NOT NULL,
    id_tagihan integer NOT NULL,
    jml_setoran numeric(10,0) NOT NULL,
    id_users integer NOT NULL,
    status integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.setor_wilayah OWNER TO sertifik;

--
-- Name: setor_wilayah_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE setor_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.setor_wilayah_id_seq OWNER TO sertifik;

--
-- Name: setor_wilayah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE setor_wilayah_id_seq OWNED BY setor_wilayah.id;


--
-- Name: share_internal; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE share_internal (
    id integer NOT NULL,
    kode_alokasi character varying(255) NOT NULL,
    periode date NOT NULL,
    nama character varying(255) NOT NULL,
    persen numeric(8,2) NOT NULL,
    jumlah numeric(8,2) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.share_internal OWNER TO sertifik;

--
-- Name: share_internal_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE share_internal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.share_internal_id_seq OWNER TO sertifik;

--
-- Name: share_internal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE share_internal_id_seq OWNED BY share_internal.id;


--
-- Name: share_wilayah; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE share_wilayah (
    id integer NOT NULL,
    id_kwilayah integer NOT NULL,
    share numeric(10,0) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.share_wilayah OWNER TO sertifik;

--
-- Name: share_wilayah_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE share_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.share_wilayah_id_seq OWNER TO sertifik;

--
-- Name: share_wilayah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE share_wilayah_id_seq OWNED BY share_wilayah.id;


--
-- Name: slideshow; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE slideshow (
    id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    judul character varying,
    gambar character varying
);


ALTER TABLE public.slideshow OWNER TO sertifik;

--
-- Name: slideshow_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE slideshow_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.slideshow_id_seq OWNER TO sertifik;

--
-- Name: slideshow_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE slideshow_id_seq OWNED BY slideshow.id;


--
-- Name: staff_btl; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE staff_btl (
    id integer NOT NULL,
    id_users integer NOT NULL,
    id_btl integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.staff_btl OWNER TO sertifik;

--
-- Name: staff_btl_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE staff_btl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.staff_btl_id_seq OWNER TO sertifik;

--
-- Name: staff_btl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE staff_btl_id_seq OWNED BY staff_btl.id;


--
-- Name: sub_area; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE sub_area (
    id integer NOT NULL,
    id_kwilayah integer NOT NULL,
    id_karea integer NOT NULL,
    nama_subarea character varying(255) NOT NULL,
    kode integer NOT NULL,
    alamat character varying(255),
    nama_manager character varying(255),
    status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.sub_area OWNER TO sertifik;

--
-- Name: sub_area_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE sub_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sub_area_id_seq OWNER TO sertifik;

--
-- Name: sub_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE sub_area_id_seq OWNED BY sub_area.id;


--
-- Name: surat_tugas; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE surat_tugas (
    id integer NOT NULL,
    no_surat character varying(255) NOT NULL,
    id_pemeriksa1 integer NOT NULL,
    id_pemeriksa2 integer NOT NULL,
    id_staff integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    status character varying,
    jangka_waktu timestamp without time zone
);


ALTER TABLE public.surat_tugas OWNER TO sertifik;

--
-- Name: surat_tugas_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE surat_tugas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.surat_tugas_id_seq OWNER TO sertifik;

--
-- Name: surat_tugas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE surat_tugas_id_seq OWNED BY surat_tugas.id;


--
-- Name: table_calon_pelanggan_web_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE table_calon_pelanggan_web_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_calon_pelanggan_web_id_seq OWNER TO sertifik;

--
-- Name: table_calon_pelanggan_web_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE table_calon_pelanggan_web_id_seq OWNED BY calon_pelanggan_web.id;


--
-- Name: tagihan_area; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE tagihan_area (
    id integer NOT NULL,
    no_ref character varying(255) NOT NULL,
    id_kwilayah integer NOT NULL,
    id_karea integer NOT NULL,
    periode date NOT NULL,
    jml_tagihan numeric(10,0) NOT NULL,
    jml_terbayar numeric(10,0) NOT NULL,
    jml_hutang numeric(10,0) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.tagihan_area OWNER TO sertifik;

--
-- Name: tagihan_area_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE tagihan_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tagihan_area_id_seq OWNER TO sertifik;

--
-- Name: tagihan_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE tagihan_area_id_seq OWNED BY tagihan_area.id;


--
-- Name: tagihan_pusat; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE tagihan_pusat (
    id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.tagihan_pusat OWNER TO sertifik;

--
-- Name: tagihan_pusats_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE tagihan_pusats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tagihan_pusats_id_seq OWNER TO sertifik;

--
-- Name: tagihan_pusats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE tagihan_pusats_id_seq OWNED BY tagihan_pusat.id;


--
-- Name: tagihan_sub_area; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE tagihan_sub_area (
    id integer NOT NULL,
    no_ref character varying NOT NULL,
    id_karea integer NOT NULL,
    id_subarea integer NOT NULL,
    periode date NOT NULL,
    jml_tagihan numeric(10,0) NOT NULL,
    jml_terbayar numeric(10,0) NOT NULL,
    jml_hutang numeric(10,0) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.tagihan_sub_area OWNER TO sertifik;

--
-- Name: tagihan_sub_area_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE tagihan_sub_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tagihan_sub_area_id_seq OWNER TO sertifik;

--
-- Name: tagihan_sub_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE tagihan_sub_area_id_seq OWNED BY tagihan_sub_area.id;


--
-- Name: tagihan_wilayah; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE tagihan_wilayah (
    id integer NOT NULL,
    no_ref character varying(255) NOT NULL,
    id_kwilayah integer NOT NULL,
    periode date NOT NULL,
    id_share integer NOT NULL,
    rp_share numeric(10,0) NOT NULL,
    jml_tagihan numeric(10,0) NOT NULL,
    jml_terbayar numeric(10,0) NOT NULL,
    jml_hutang numeric(10,0) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.tagihan_wilayah OWNER TO sertifik;

--
-- Name: tagihan_wilayah_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE tagihan_wilayah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tagihan_wilayah_id_seq OWNER TO sertifik;

--
-- Name: tagihan_wilayah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE tagihan_wilayah_id_seq OWNED BY tagihan_wilayah.id;


--
-- Name: tarif; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE tarif (
    id integer NOT NULL,
    jenis_tarif character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.tarif OWNER TO sertifik;

--
-- Name: tarif_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE tarif_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tarif_id_seq OWNER TO sertifik;

--
-- Name: tarif_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE tarif_id_seq OWNED BY tarif.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    nip character varying DEFAULT 0,
    nama character varying(255) NOT NULL,
    email character varying(255) DEFAULT '0'::character varying NOT NULL,
    jabatan character varying(255) DEFAULT '0'::character varying,
    departemen character varying(255) DEFAULT '0'::character varying,
    wewenang character varying(255) DEFAULT '0'::character varying,
    id_kwilayah integer DEFAULT 0,
    id_karea integer DEFAULT 0,
    id_subarea integer DEFAULT 0,
    status character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    username character varying NOT NULL,
    masa_berlaku date,
    no_telp character varying
);


ALTER TABLE public.users OWNER TO sertifik;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO sertifik;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: verifikasi; Type: TABLE; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE TABLE verifikasi (
    id integer NOT NULL,
    no_pendaftaran character varying(255) NOT NULL,
    no_lhpp character varying(255) NOT NULL,
    pemeriksaan_dokumen character varying(255) NOT NULL,
    proteksi_sentuh_langung character varying(255) NOT NULL,
    pemeriksaan_penghantar character varying(255) NOT NULL,
    pemeriksaan_phb character varying(255) NOT NULL,
    pemeriksaan_elektroda character varying(255) NOT NULL,
    pemeriksaan_polaritas character varying(255) NOT NULL,
    pemeriksaan_pemasangan character varying(255) NOT NULL,
    pemeriksaan_sni character varying(255) NOT NULL,
    pemeriksaan_kamar_mandi character varying(255) NOT NULL,
    catatan character varying(255) NOT NULL,
    hasil_pemeriksaan character varying(255) NOT NULL,
    ceklist_ppu1 character varying(255) NOT NULL,
    ceklist_ppu2 character varying(255) NOT NULL,
    hasil_verifikasi character varying(255) NOT NULL,
    alasan_reject character varying(255) NOT NULL,
    id_staff integer NOT NULL,
    status_sertifikasi integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.verifikasi OWNER TO sertifik;

--
-- Name: verifikasi_id_seq; Type: SEQUENCE; Schema: public; Owner: sertifik
--

CREATE SEQUENCE verifikasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.verifikasi_id_seq OWNER TO sertifik;

--
-- Name: verifikasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sertifik
--

ALTER SEQUENCE verifikasi_id_seq OWNED BY verifikasi.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY alokasi_share ALTER COLUMN id SET DEFAULT nextval('alokasi_share_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY asosiasi ALTER COLUMN id SET DEFAULT nextval('asosiasi_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY btl ALTER COLUMN id SET DEFAULT nextval('btl_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY calon_pelanggan_web ALTER COLUMN id SET DEFAULT nextval('table_calon_pelanggan_web_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY daya ALTER COLUMN id SET DEFAULT nextval('daya_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY galeri ALTER COLUMN id SET DEFAULT nextval('galeri_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY informasi ALTER COLUMN id SET DEFAULT nextval('berita_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY jenis_bangunan ALTER COLUMN id SET DEFAULT nextval('jenis_bangunan_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY kantor_area ALTER COLUMN id SET DEFAULT nextval('kantor_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY kantor_wilayah ALTER COLUMN id SET DEFAULT nextval('kantor_wilayah_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY kota ALTER COLUMN id SET DEFAULT nextval('kota_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY log_status ALTER COLUMN id SET DEFAULT nextval('log_status_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY oauth_clients ALTER COLUMN id SET DEFAULT nextval('oauth_clients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY oauth_personal_access_clients ALTER COLUMN id SET DEFAULT nextval('oauth_personal_access_clients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY password_resets ALTER COLUMN id SET DEFAULT nextval('password_resets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY pelanggan ALTER COLUMN id SET DEFAULT nextval('pelanggan_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY pelanggan_temp ALTER COLUMN id SET DEFAULT nextval('pelanggan_temp_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY pembayaran ALTER COLUMN id SET DEFAULT nextval('pembayaran_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY pemeriksa ALTER COLUMN id SET DEFAULT nextval('pemeriksa_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY penyedia_listrik ALTER COLUMN id SET DEFAULT nextval('penyedia_listrik_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY perangkat ALTER COLUMN id SET DEFAULT nextval('perangkat_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY provinsi ALTER COLUMN id SET DEFAULT nextval('provinsi_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY rekap_area ALTER COLUMN id SET DEFAULT nextval('rekap_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY rekap_pusat ALTER COLUMN id SET DEFAULT nextval('rekap_pusat_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY rekap_sub_area ALTER COLUMN id SET DEFAULT nextval('rekap_sub_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY rekap_wilayah ALTER COLUMN id SET DEFAULT nextval('rekap_wilayah_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY runingtext_frontend ALTER COLUMN id SET DEFAULT nextval('runingtext_frontend_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY sertifikasi ALTER COLUMN id SET DEFAULT nextval('sertifikasi_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY setor_area ALTER COLUMN id SET DEFAULT nextval('setor_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY setor_sub_area ALTER COLUMN id SET DEFAULT nextval('setor_sub_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY setor_wilayah ALTER COLUMN id SET DEFAULT nextval('setor_wilayah_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY share_internal ALTER COLUMN id SET DEFAULT nextval('share_internal_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY share_wilayah ALTER COLUMN id SET DEFAULT nextval('share_wilayah_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY slideshow ALTER COLUMN id SET DEFAULT nextval('slideshow_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY staff_btl ALTER COLUMN id SET DEFAULT nextval('staff_btl_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY sub_area ALTER COLUMN id SET DEFAULT nextval('sub_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY surat_tugas ALTER COLUMN id SET DEFAULT nextval('surat_tugas_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY tagihan_area ALTER COLUMN id SET DEFAULT nextval('tagihan_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY tagihan_pusat ALTER COLUMN id SET DEFAULT nextval('tagihan_pusats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY tagihan_sub_area ALTER COLUMN id SET DEFAULT nextval('tagihan_sub_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY tagihan_wilayah ALTER COLUMN id SET DEFAULT nextval('tagihan_wilayah_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY tarif ALTER COLUMN id SET DEFAULT nextval('tarif_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sertifik
--

ALTER TABLE ONLY verifikasi ALTER COLUMN id SET DEFAULT nextval('verifikasi_id_seq'::regclass);


--
-- Data for Name: Berita; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY "Berita" (id, read_at, created_at, updated_at, gambar, jenis, judul, isi) FROM stdin;
\.


--
-- Data for Name: adminfrontend; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY adminfrontend (email, password) FROM stdin;
admin@gmail.com	$2y$10$5TMFW2QHaz1PQ6q2D6UgTO6i3gRqlyYul9bLH8S/qb2XDroSatVS6
\.


--
-- Data for Name: alokasi_share; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY alokasi_share (id, kode_alokasi, periode, pendapatan_kotor, alokasi_pengembangan, jumlah_share, created_at, updated_at) FROM stdin;
1	S001	2019-04-23	3800.00	1900.00	1900.00	\N	\N
\.


--
-- Name: alokasi_share_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('alokasi_share_id_seq', 1, false);


--
-- Data for Name: asosiasi; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY asosiasi (id, nama_asosiasi, created_at, updated_at) FROM stdin;
1	AKLINAS	2019-01-24 09:35:31	2019-01-24 09:35:31
2	INDEPENDENT	2019-01-24 09:35:48	2019-01-24 09:35:48
3	AKLI	2019-03-20 14:20:50	2019-03-20 14:20:50
4	AKLINDO	2019-03-20 14:21:12	2019-03-20 14:21:12
5	AKKLINDO	2019-03-20 14:21:23	2019-03-20 14:21:23
6	APKENAS	2019-03-20 14:21:36	2019-03-20 14:21:36
7	PAKLINA	2019-03-20 14:21:48	2019-03-20 14:21:48
8	AKEI	2019-03-20 14:22:05	2019-03-20 14:22:05
9	GAKLIMDO	2019-03-20 14:22:16	2019-03-20 14:22:16
10	ASKOMELIN	2019-03-20 14:22:31	2019-03-20 14:22:31
11	AKLI2	2019-03-20 14:22:42	2019-03-20 14:22:42
12	DESINDO	2019-03-20 14:22:55	2019-03-20 14:22:55
13	AKMI	2019-03-20 14:23:04	2019-03-20 14:23:04
14	K3EN	2019-03-20 14:23:41	2019-03-20 14:23:41
15	AKLI 1	2019-03-20 14:23:52	2019-03-20 14:23:52
16	ASLI	2019-03-20 14:24:01	2019-03-20 14:24:01
17	AKMELINDO	2019-03-20 14:24:14	2019-03-20 14:24:14
\.


--
-- Name: asosiasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('asosiasi_id_seq', 18, true);


--
-- Data for Name: berita; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY berita (id, created_at, updated_at, judul, isi, jenis, gambar) FROM stdin;
11	2019-03-10 20:38:44	2019-03-10 20:38:44	PLN Yogyakarta Upayakan Perbaikan Jaringan Secepatnya	<p>YOGYAKARTA,KOMPAS.com- Akibat angin kencang di wilayah Yogyakarta, sejumlah instalasi listrik mengalami kendala, seperti hampir ambruknya tiang listrik di jalan Majapahit, tepatnya di perempatan Blok O. PLN Area Yogyakarta berupaya pelayanan listrik pulih pada Jumat (1/3/2019) malam. &quot;Kami upayakan listrik normal dinyalakan dulu, meski dari konstruksi belum sepenuhnya pulih,&quot; kata Manajer PLN Area Yogyakarta, Eric Rossi saat dihubungi wartawan, Jumat petang. Dia mengatakan, pihaknya masih mendata dampak pemadaman. Untuk tiang listrik yang miring di sekitar blok O, arus induknya dari daerah Gejayan, Sleman, menuju wilayah Banguntapan, Bantul. Diperkirakan, perbaikan akan selesai 3 jam setelah petang tadi. &quot;Kondisinya masih basah, kan juga harus mengutamakan keselamatan dari rekan-rekan. Kemungkinan 3 jam baru pulih,&quot;ucapnya. Baca juga: Hujan dan Angin Kencang di Yogyakarta, Pohon Tumbang Timpa Mobil, Tiang Listrik Miring Bagus, salah satu warga di Desa Banguntapan mengaku sejak angin kencang sore tadi listrik di kosnya mati. &quot;Karena pekerjaan membutuhkan aliran listrik, sementara saya pergi ke warnet di kota Yogyakarta. Tadi 20 menit lalu masih mati listrik,&quot;ucapnya. Hujan dan angin kencang menyebabkan banyak terjadi pohon tumbang di sejumlah wilayah di Yogyakarta, terutama wilayah Bantul. Dari data Pusdalop BPBD DIY pukul 19.00 WIB, sejumlah pohon tumbang di Bantul, Kota Yogyakarta, dan Sleman. Di Kecamatan Kotagede, Kota Yogyakarta, ada dua kejadian, yang pertama di halaman parkir Kebun Binatang Gembira Loka, Rejowinangun, dimana pohon tumbang menimpa bus pariwisata. Kedua, pohon mangga menimpa tembok pembatas rumah. Di Kecamatan Wirobrajan, Kota Yogyakarta, ada 4 kejadian, diantaranya pohon tumbang. Di Kecamatan Ngampilan, atap rumah seorang warga bertebaran. Untuk Kabupaten Bantul, di Kecamatan Banguntapan ada 11 kejadian, pohon tumbang hingga tiang listrik miring. Sementara, untuk Kecamatan Kasihan, dua lokasi pohon tumbang menimpa jaringan listrik serta rumah.&nbsp; Masih di Bantul, di Kecamatan Piyungan dan Kecamatan Sedayu pohon tumbang menimpa jaringan listrik. Untuk Kabupaten Sleman, di Kecamatan Pakem ada dua kejadian yakni pohon tumbang dan atap rumah milik warga rusak terkena angin. Di Kecamatan Gamping, sejumlah pohon tumbang menimpa jaringan listrik dan jalan. &quot;Proses penanganan masih berlangsung,&quot; kata Manager Pusdalop BPBD DIY Danang Syamsurizal.&nbsp;<br />\r\n<br />\r\nArtikel ini telah tayang di&nbsp;<a href="http://kompas.com/">Kompas.com</a>&nbsp;dengan judul &quot;PLN Yogyakarta Upayakan Perbaikan Jaringan Secepatnya&quot;,&nbsp;<a href="https://regional.kompas.com/read/2019/03/01/19565791/pln-yogyakarta-upayakan-perbaikan-jaringan-secepatnya">https://regional.kompas.com/read/2019/03/01/19565791/pln-yogyakarta-upayakan-perbaikan-jaringan-secepatnya</a>.&nbsp;<br />\r\nPenulis : Kontributor Yogyakarta, Markus Yuwono<br />\r\nEditor : Khairina</p>	Headline	440058907.jpg
12	2019-03-10 20:39:52	2019-03-10 20:39:52	PLTU Cilacap Ekspansi Beroperasi, 1,4 Juta Rumah Tangga Bisa Teraliri Listrik	<p>JAKARTA, KOMPAS.com - Pembangkit Listrik Tenaga Uap (PLTU) Cilacap Ekspansi 1x660 MW telah resmi beroperasi setelah diresmikan oleh Presiden Joko Widodo pada Senin (25/2/2019). Menteri Energi dan Sumbardaya Mineral (ESDM) Ignasius Jonan mengatakan, kehadiran PLTU tersebut bisa mendukung penambahan sambungan listrik baru di Jawa dan Bali. &quot;Terdapat peluang penambahan sambungan listrik baru di masyarakat untuk rumah tangga yang 900 watt ampere hampir sebesar 700.000 rumah tangga,&quot; ujar Jonan. &quot;Kalau yang 450 watt ampere tinggal dikalikan dua yakni hampir 1,4 juta rumah tangga,&quot; sambungnya. Mantan Direktur Utama PT KAI itu mengatakan, pembangkit listrik ini dikembangkan oleh anak perusahaan PLN yakni PT Pembangkit Jawa Bali (PJB) dan PT Sumber Segara Primadaya (S2P). PLTU Cilacap Ekspansi 1x660 MW sudah menggunakan Super-Critical Boiler batubara Low Range sebagai bahan bakarnya. Selain itu, PLTU ini dilengkapi dengan Electristastic Precipitat or dan Flue Gas Desulpurization sehingga beroperasi secara efisien dan ramah lingkungan. PLTU Cilacap Ekspansi 1x660 MW menelan investasi sebesar 899 juta dollar AS. Saat beroperasi, PLTU tersebut menyerap 800 tenaga kerja.<br />\r\n<br />\r\nArtikel ini telah tayang di&nbsp;<a href="http://kompas.com/">Kompas.com</a>&nbsp;dengan judul &quot;PLTU Cilacap Ekspansi Beroperasi, 1,4 Juta Rumah Tangga Bisa Teraliri Listrik&quot;,&nbsp;<a href="https://ekonomi.kompas.com/read/2019/02/25/152602426/pltu-cilacap-ekspansi-beroperasi-14-juta-rumah-tangga-bisa-teraliri-listrik">https://ekonomi.kompas.com/read/2019/02/25/152602426/pltu-cilacap-ekspansi-beroperasi-14-juta-rumah-tangga-bisa-teraliri-listrik</a>.&nbsp;<br />\r\nPenulis : Yoga Sukmana<br />\r\nEditor : Bambang Priyo Jatmiko</p>	News	650656110.jpg
13	2019-03-10 20:40:53	2019-03-10 20:40:53	Telat Bayar Listrik, Warga di Bandung Merasa Diintimidasi Petugas PLN	<p>BANDUNG, KOMPAS.com - Warga RT 05, RW 02, Kelurahan Karangpamulang, Kecamatan Mandalajati, Kota Bandung, merasa diintimidasi oleh petugas PLN yang datang untuk memeriksa dan memberikan surat tagihan listrik bulanan. Teti Rohaeti (48), salah seorang pelanggan PLN, mengatakan, belum lama ini petugas PLN datang ke rumahnya dengan nada mengancam. Ibu satu anak tersebut mengatakan, petugas PLN tersebut mengancam akan mencabut dan memutuskan sambungan listrik di rumah Teti apabila ia tidak membayar tagihan listrik sebelum tanggal 21 Februari 2019. &ldquo;Ngancamnya kalau enggak bayar sampai tanggal 21 Februari mau diputus dan dicabut listriknya,&rdquo; kata Teti saat ditemui di rumahnya, Kamis (21/2/2019). Teti mengku heran dengan ancaman tersebut. Sebab, selama ini dia tidak pernah menunggak tagihan listrik hingga berbulan-bulan. Baca juga: Nenek Ini 10 Tahun Hidup Tanpa Listrik, Polisi Pasangkan Panel Surya 500 Watt Gratis Meski demikian, dia mengakui memang tidak pernah membayar tagihan listrik tepat waktu lantaran suaminya biasa menerima gaji di akhir bulan. &ldquo;Biasanya bayar akhir bulan enggak ada masalah. Kenapa ini tiba-tiba diancam. Biasanya kan kalau nunggak tiga bulan baru diputus,&rdquo; ucapnya. Selain itu, Teti mengatakan, petugas PLN tersebut juga memaksa akan mengganti meteran listrik pascabayar ke meteran prabayar atau yang lebih dikenal dengan token apabila tidak membayar tagihan listrik sebelum tanggal 21 Februari. &ldquo;Saya juga dipaksa tanda tangan (ganti meteran prabayar). Saya iya iya saja karena takut,&rdquo; katanya. Warga lainnya, Priwadi (57) mengutarakan hal yang sama. Dia menilai, apa yang dilakukan petugas PLN merupakan kesewenang-wenangan. Dia tidak bisa menuruti ancaman petugas PLN lantaran rumah yang ditempatinya adalah rumah sewa. &ldquo;Bagaimana kalau orang punya uangnya di akhir bulan. Masa main putus listrik begitu saja. Kenapa kami orang kecil jadi yang digencet,&rdquo; ucapnya. Baca juga: Polisi Tangkap Dua Petani Curi Kabel Listrik PLN di Riau Selain itu, Priwadi menilai, meteran pascabayar yang digunakan saat ini di rumah kontrakannya jauh lebih murah ketimbang menggunakan meteran prabayar meski dengan KWh yang sama. &ldquo;Barang elektronik saya enggak banyak. Kalau pakai token sebulan bisa habis Rp 100.000. Kalau pakai ini (meteran pascabayar) paling besar cuma Rp 70.000 sebulan,&rdquo; katanya. Respons PLN Dihubungi melalui ponselnya, Deputy Manajer Komunikasi &amp; Bina Lingkungan PT PLN (Persero) Distribusi Jawa Barat Iwan Ridwan membenarkan pihaknya bakal melakukan pemutusan apabila tagihan listrik tidak dibayar sebelum tanggal 20. &ldquo;Kalau mengikuti aturan akan diputus sementara, bukan dicabut. Kalau tiga bulan tidak dibayar baru akan dicabut,&rdquo; jelas Iwan. Meski demikian, Iwan menyayangkan tindakan petugas PLN yang dinilai mengintimidasi. &ldquo;Mungkin hanya kesalahan dalam penyampaian kepada pelanggan saja,&rdquo; ucapnya. Selama ini, lanjut Iwan, toleransi yang diberikan kepada pelanggan yang kerap telat membayar tagihan listrik lantaran kurangnya petugas pemutus aliran listrik di lapangan. &ldquo;Kita akui, kita memang kekurangan tenaga di lapangan. Sejak Desember 2018 lalu target kita memang zero tunggakan,&rdquo; katanya. Selain itu, Iwan memastikan tidak ada paksaan untuk mengganti meteran listrik dari prabayar ke pascabayar. &ldquo;Kalau sudah terlalu sering nunggak sampai tiga bulan kami sarankan untuk mengganti dari pascabayar ke prabayar,&rdquo; katanya.<br />\r\n<br />\r\nArtikel ini telah tayang di&nbsp;<a href="http://kompas.com/">Kompas.com</a>&nbsp;dengan judul &quot;Telat Bayar Listrik, Warga di Bandung Merasa Diintimidasi Petugas PLN&quot;,&nbsp;<a href="https://regional.kompas.com/read/2019/02/21/16471001/telat-bayar-listrik-warga-di-bandung-merasa-diintimidasi-petugas-pln">https://regional.kompas.com/read/2019/02/21/16471001/telat-bayar-listrik-warga-di-bandung-merasa-diintimidasi-petugas-pln</a>.&nbsp;<br />\r\nPenulis : Kontributor Bandung, Putra Prima Perdana<br />\r\nEditor : Farid Assifa</p>	News	29253161.JPG
14	2019-03-20 09:46:51	2019-03-20 09:46:51	Syarat Agar PLN Bisa Turunkan Tarif Listrik	<p>PT PLN (Persero) masih menunggu beberapa hal untuk menurunkan tarif&nbsp;<a href="https://www.liputan6.com/bisnis/read/3917939/pemerintah-dan-pln-terus-sambungkan-listrik-gratis-buat-warga-miskin">listrik</a>. PLN memastikan bahwa perusahaan tidak akan menaikan tarif listrik.</p>\r\n\r\n<p>Direktur Utama PLN Sofyan Basir mengatakan, PLN akan menurunkan tarif listrik jika harga bahan bakar untuk pembangkit listrik mengalami penurunan.&nbsp;Selain itu, penurunan tarif listrik juga bisa terjadi apabila&nbsp;nilai tukar&nbsp;rupiah terhadap dolar Amerika Serikat (AS) mengalami penguatan.</p>\r\n\r\n<p>&quot;Kalau bahan bakar diturunkan, kurs turun nanti saya juga turunin tarif listrik,&quot; kata Sofyan, di Jakarta, Sabtu (16/3/2019).</p>\r\n\r\n<p>&quot;Enggak ada kenaikan sampai 2019. kenaikan tarif itu enggak ada,&quot; tuturnya.</p>\r\n\r\n<p>Sebelumnya PLN telah memberikan diskon tarif listrik kepada pelanggan R-I 900 Volt Amper (VA) Rumah Tangga Mampu(RTM) mulai 1 Maret 2019, &lrm;insentif ini diberikan karena adanya efisiensi digolongan ini, serta terjadinya penurunan harga minyak dan kurs dolar AS.</p>\r\n\r\n<p>Executive Vice President Corporate Communication &amp; CSR PLN I Made Suprateka mengatakan,&lrm; dengan pemberlakuan insentif ini maka pelanggan golongan R-1 900 VA RTM hanya membayar tarif listrik sebesar Rp 1.300 per kilowatt hour (kWh) dari tarif normal sebesar Rp 1.352 per kWh.</p>\r\n\r\n<p>&quot;Penurunan tarif ini berlaku bagi 21 juta pelanggan&nbsp;<a href="https://www.liputan6.com/bisnis/read/3917939/pemerintah-dan-pln-terus-sambungkan-listrik-gratis-buat-warga-miskin">listrik</a>&nbsp;R-1 900 VA RTM,&quot; kata Made.</p>\r\n\r\n<p>Menurut Made, penurunan atau pemberian insentif tarif ini dilakukan karena PLN berhasil melakukan efisiensi diantaranya penurunan susut jaringan, perbaikan Specified Fuel Consumption (SFC) dan peningkatan Capacity Factor (CF) pembangkit. Selain itu insentif diberikan juga mengingat kondisi harga ICP selama 3 bulan terakhir mengalami penurunan dari USD 62,98 per barel memjadi USD 56,55 per barel.</p>\r\n\r\n<p>Made mengungkapkan, listrik sudah menjadi kebutuhan dasar masyarakat saat ini. Seluruh aktivitas masyarakat ditopang oleh pasokan listrik.Dengan adanya insentif ini, PLN ingin memberikan ruang untuk pelanggan R-1 900 VA RTM agar dapat lebih banyak memanfaatkan listrik untuk menunjang kegiatan ekonominya dan dalam kegiatan kesehariannya.</p>\r\n\r\n<p>Made menambahkan bahwa insentif penurunan tarif bagi RTM 900 VA ini tidak menyertakan syarat apapun. &ldquo;Silahkan nikmati penurunan tarif ini. Dan gunakan listrik PLN dengan nyaman dan tentu saja aman,&rdquo; pungkasnya.</p>	Headline	174640154.jpg
\.


--
-- Name: berita_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('berita_id_seq', 7, true);


--
-- Data for Name: btl; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY btl (id, nama_btl, id_kwilayah, kode_kota, alamat, email, telp, created_at, updated_at) FROM stdin;
1	PT. RAJA JULOK AGUNG	2	1105	JL. ALUE IE MIRAH NO. 138 GP. BLANG PAUH SA KEC. JULOK Kab. Aceh Timur Aceh	pt.rajajulokagung@yahoo.com	085207377575	\N	\N
2	PT. RIZKI LINGE LESTARI	2	1106	JL. YOS SUDARSO LR. LESTARI KP. BLANG KOLAK II KEC. BEBESEN Kab. Aceh Tengah Aceh	ptrizkilingelestari@gmail.com		\N	\N
3	PT. BINTANG PERMATA TAKENGON	2	1106	JL. LEBE KADER KP. REJE BUKIT KEC. BEBESEN Kab. Aceh Tengah Aceh	ptbintangpermata.tkn@gmail.com		\N	\N
4	PT. TAMITA NANDA MANDIRI	2	1109	JALAN RAJA KUALA NO.1 LINGKUNGAN TANJONG HARAPAN KRAMAT LUAR SIGLI Kab. Pidie Aceh	tamitanandamandiri@gmail.com	085260947630	\N	\N
5	CV. SINAR KHAIFIRA	2	1109	Gampong Raya Sanggeue Kab. Pidie Aceh	sinarkhaifira@gmail.com	081362577679	\N	\N
6	PT. MITA JAYA RAKAN KONTRUKSI	2	1109	Jl. Lingkar Blang Paseh Gp. Blang Paseh Kab. Pidie Aceh	mitajayalpse2@gmail.com	085210115349	\N	\N
7	PT. MURTILA PERKASA	2	1109	Jl. Iskandar Muda No.19 Sigli Kramat Luar Kab. Pidie Aceh	murtila_diana@yahoo.co.id	0653-21317	\N	\N
8	PT. CAHAYA MUDA MURTILA	2	1109	JLN.LAWENG-KRUENG RAYA,GP.IE MASEN KEC.MUARA TIGA.KAB.PIDIE ACEH Kab. Pidie Aceh	ptcahayamuda.murtila@yahoo.com		\N	\N
9	PT. ABI ZAFRAN ACEH	2	1109	Jl. Perdagangan No. 1, Beureunuen, Gp. Masjid Yaman Kec. Mutiara Kab. Pidie Aceh	aklidpp@gmail.com		\N	\N
10	PT. NATIGA DARA ELEKTRIKAL	2	1110	JL. BAKTI NO. 44 GAMPONG BANDAR BIREUEN KEC. KUTA JUANG Kab. Bireuen Aceh	na.tigadara@gmail.com	0644-324165	\N	\N
11	PT. ASHA CAKRA HARMONI	2	1111	JL. NASIONAL NO.127A Gp. DRIEN RAMPAK Kec. JOHAN PAHLAWAN Kab. Aceh Barat Aceh	pt.ashacakraharmoni@gmail.com	082274603090	\N	\N
12	PT. SINYAL LENTERA ANTARIKSA	2	1111	Jl. Meriam Dusun Cot Nibong Desa Lapang Meulaboh Kab. Aceh Barat Aceh	pt.sla2017@gmail.com		\N	\N
13	CV. TRI KARYA	2	1111	DUSUN RUKUN KAMPUNG DALAM KECAMATAN KARANG BARU Kab. Aceh Barat Aceh	cv.tri_k@yahoo.co.id		\N	\N
14	PT. JASA DAFI USIMARINDO	2	1111	Jl. Medan - B.Aceh Ulee Madon, Muara Batu, Aceh Utara, Aceh Kab. Aceh Utara Aceh	dafi.usimarindo@yahoo.co.id	08126957434	\N	\N
15	PT. JASA KARINSA PERDANA	2	1111	Jl. Medan - Banda Aceh No. 8 Keutapang Syamtalira Aron Kab. Aceh Utara Kab. Aceh Utara Aceh	pt.jkp2328@gmail.com	081360200597	\N	\N
16	PT. ANUGRAH RINANG PEPARA	2	1117	JL. TAKENGON - BIREUEN, KP. PANTE RAYA BARAT, KEC. WIH PESAM Kab. Bener Meriah Aceh	pt.anugrahrinangpepara@gmail.com	085277340000	\N	\N
17	PT. BINTANG MAHA SURYA	2	1117	KP. KUTE TANYUNG, KEC. BUKIT KABUPATEN BENER MERIAH Kab. Bener Meriah Aceh	bintangmahasurya@yahoo.com	08129587 2901	\N	\N
18	PT. Horas Bangun Persada	2	1171	Jl. Singgahmata No. 65, GP Sukaramai Kec. Baiturrahman Kota Banda Aceh Aceh	horasbgnpersada@gmail.com	08126050659	\N	\N
19	CV. Garuda Engineering	2	1171	Jl. T. Iskandar Desa Lambhuk Kota Banda Aceh Aceh	garuda_engineering@yahoo.co.id	08126920759	\N	\N
20	PT. ANDESMONT SAKTI	2	1171	JLN. T. UMAR NO. 143 GP. SETUI � BANDA ACEH Kota Banda Aceh Aceh	sakti_nad@yahoo.com	0651-49347	\N	\N
21	PT. ZEIN PUTRA MALAKA	2	1171	JL. TENGKU ABU BAKAR NO. 17 LAMTEUMEN TIMUR KEC. JAYA BARU KOTA BANDA ACEH Kota Banda Aceh Aceh	zeinputra.malaka@yahoo.co.id	08126920759	\N	\N
22	PT. SULTAN MALAKA ELEKTRIK	2	1171	JL. TGK. H. ABUBAKAR NO. 17 GP. LAMTEUMEN TIMUR KEC. JAYA BARU KOTA BANDA ACEH Kota Banda Aceh Aceh	malaka_electrik@yahoo.co.id	08126920759	\N	\N
23	PT. CITRA MUARA JAYA	2	1171	JL. MUJUR NO. 57 A LAMLAGANG - BANDA ACEH Kota Banda Aceh Aceh	citramuarajaya@yahoo.com	08126905457	\N	\N
24	CV. RAJA REUBAH	2	1171	JL. RESIDEN DANU BROTO NO.19 GP. LAM LAGANG KEC. BANDA RAYA Kota Banda Aceh Aceh	raja.reubah@yahoo.co.id	08116823737	\N	\N
25	PT. DARMA KARYA GROUP	2	1171	Jl. SA. JOHANSYAH NO. 49 E GAMPONG NEUSU Kota Banda Aceh Aceh	dkgroup.pt@gmail.com	08126920759	\N	\N
26	PT. DARCO UTAMA INDONESIA	2	1171	JL. MUJUR NO. 57 LAMLAGANG BANDA ACEH 23239 Kota Banda Aceh Aceh	darcoutama@yahoo.co.id	+62 651 46632 / +62 812 690 5457	\N	\N
27	PT. AWAYNA OPANINDO UTAMA	2	1171	JL.PROF ALI HASYIMI GP.ILIE, KEC.ULEE KARENG KOTA BANDA ACEH Kota Banda Aceh Aceh	opanindo.utama@yahoo.co.id	6518010430	\N	\N
28	PT. LENTERA DJAYA OETAMA	2	1171	JL. TGK. CHIK DIPINEUNG VII NO.2 GP. PINEUNG Kota Banda Aceh Aceh	pt.lentera.djaya.oetama@gmail.com	06516303656 / 081360078495	\N	\N
29	PT. NAJASA PRIBUMI ATJEH	2	1171	Jl. T. Nyak Arief No.256 Gp. Jeulingke Kec. Baiturrahman Kota Banda Aceh Aceh	najasapribumiatjeh@yahoo.co.id		\N	\N
30	PT. BANGUN JAYA ATJEH	2	1171	Jl. Rawa Sakti Barat Gp. Jeulingke Jeulingke Kec. Ulee Kareng Kota Banda Aceh Aceh	bangunjayaaceh@yahoo.co.id		\N	\N
31	PT. KURNIA ASYIFA SEJATI	2	1171	JL. PERKASA NO. 12 GP KUTA ALAM KEC KUTA ALAM KOTA BANDA ACEH Kota Banda Aceh Aceh	ptkurniaasyifa@gmail.com		\N	\N
32	PT. TANJUNG BLANG PASE	2	1171	JL.T.RADEN NO.8 ULEE KAREENG - BANDA ACEH Kota Banda Aceh Aceh	pt.tanjungblangpase@yahoo.com		\N	\N
33	PT. ALAM HIJAU ENERGI	2	1171	JL. IR. HM. THAHER NO. 36 GP. LUENG BATA KECAMATAN LUENG BATA BANDA ACEH Kota Banda Aceh Aceh	alam.hijau.energi@gmail.com		\N	\N
34	PT. PALMA BANNA MANDIRI	2	1173	JL. SUDIRMAN GP. MTG. SEULIMENG KEC. LANGSA BARAT Kota Langsa Aceh	hendri.palma@yahoo.co.id	081281621350	\N	\N
35	CV. DESTIA	2	1173	Jl. Prof. A. Majid Ibrahim Lr. Peutua Bayan Lk. III. Gp Mtg. Seulimeng Kec. Langsa Barat Kota Langsa Aceh	lskn_pusat@yahoo.co.id		\N	\N
36	CV. BEC	2	1173	Jl. Sudirman Gg. Sederhana No. 49 Matang Seulimeng Kec. Langsa Barat Kota Langsa Aceh	lskn_pusat@yahoo.co.id		\N	\N
37	CV. ARTA MANDIRI GROUP	2	1173	Jl. Sidomulyo Dsn. Rahmat GAmpong Lengkong Kel. Lengkong Kec. Langsa Baro Kota Langsa Aceh	lskn_pusat@yahoo.co.id		\N	\N
38	CV. BANNA ELECTRIC	2	1173	Dsn. Pendidikan Gp. Sungai Lueng Kota Langsa Aceh	swandilangsa@gmail.com		\N	\N
39	CV. KEUMALA INDAH	2	1173	JL.SIMPANG KRAMAT ALUE LIM KECAMATAN BLANG MANGAT KOTA LHOKSEUMAWE Kota Lhokseumawe Aceh	keumalaindah_cv@yahoo.co.id	081362695822	\N	\N
40	PT. BIMA SATRIA MANDIRI	2	1173	Jl. Medan Banda Aceh No. 2 Sp. IV Paloh, Meuria Paloh Gampong Meuria Paloh Kec. Muara Satu Kota Lhokseumawe, Aceh Kota Lhokseumawe Aceh	bimasatriamandiri@gmail.com	08116117985	\N	\N
41	PT. ANANDA HUMAIRAH	2	1173	Jl. Buloh Blang Ara Gampong Paya Punteuet Kec. Muara Dua Kota Lhokseumawe, Aceh Kota Lhokseumawe Aceh	anandahumairah00@gmail.com	0811686949	\N	\N
42	PT. HEMAT JAYA MAKMU ENERGY	2	1173	Jl. Bangdes Komp. PLN No. 22 Dusun I Gampong Kel. Tumpok Teungoh Kec. Banda Sakti Kota Lhokseumawe, ACeh Kota Lhokseumawe Aceh	hemat.jaya_pt@yahoo.com	085260044788	\N	\N
43	PT. BUGAK BRAWANG CEMERLANG	2	1173	Jl. Pase No.6 Lancang Garam Kec. Banda Sakti Kota Lhokseumawe Aceh	pusat@ptaskomelin.co.id		\N	\N
44	PT. WEELFA TEHNIKA MANDIRI	5	1501	Desa Pancuran Tiga, Kec Keliling Danau, Kab Kerinci Jambi Kab. Kerinci Jambi	wellfatehnika@gmail.com	(0748) 365229	\N	\N
45	PT. TREE MITRA ENGINEERING	5	1502	JL.PEMUDA RT.21 RW.03 KEL.PEMATANG KANDIS BANGKO Kab. Merangin Jambi	treemitra@yahoo.co.id		\N	\N
46	PT. MITRA SINAR MANDIR	5	1503	Jl. Abu Tohid No. 08 RT.06 RW.02 Kel. Sukasari Sarolangun, Jambi Kab. Sarolangun Jambi	msm.mandiri@gmail.com	081366785220	\N	\N
47	PT. ALMEVIA TEKNISINDO ELEKTRA	5	1504	Villa Kenali Permai Blok P. 12 No. 05 Jambi Kab. Batanghari Jambi	ptaltera@yahoo.co.id		\N	\N
48	PT. GEMILANG REZKY UTAMA	5	1505	Kebun Kolim RT.001/001 Desa Tangkit Kel. Sungai Gelam Kec. Muaro Jambi Kab. Muaro Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
49	PT. SALSA TEHNIK PRIMA	5	1508	JL. AMARTA, KEL. PURWOHARJO, KEC. RIMBO BUJANG, KAB TEBO, JAMBI Kab. Tebo Jambi	salsatehnikprima@gmail.com	082376462626	\N	\N
50	PT. Lafan Tri Perdana	5	1508	Jl. 25 Poros RT.04/07, Desa Perintis, Kec. Rimbo Bujang Kab. Tebo Jambi	lafan.tri.perdana@gmail.com		\N	\N
51	PT. DAMAI SUKSES SEJAHTERA	5	1508	Komplek Pasar Kembang Rt 07 Rw 04 Desa Suka Damai Kecamatan Rimbo Ulu Kab. Tebo Jambi	pt.damaisuksessejahtera@yahoo.co.id		\N	\N
52	PT. Energi Bungo Listrindo	5	1509	JL. Sultan Thaha RT.007 RW. 003 Bungo Timur, Kec. Pasar Muaro bungo, Kab Bungo Jambi Kab. Bungo Jambi	pt.energibungolistrindo@yahoo.co.id	085266104900	\N	\N
53	PT. APARINDO JAYA UTAMA	5	1509	JL. TEUKU UMAR BTN BUNGO MAKMUR BLOK E NO.16 RT.031 RW.009 KEL. PASIR PUTIH, KEC. RIMBO TENGAH KAB. BUNGO - JAMBI Kab. Bungo Jambi	pt.aparindojayautama94@gmail.com		\N	\N
54	PT. GEMURUH ELEKTRIK PERKASA	5	1509	Perumahan Lintas Asri Blok B No.28 RT.16/25 Kel. Sungai Kerjan Kec. Bungo Dani Kab. Bungo Jambi Kab. Bungo Jambi	pusat@ptaskomelin.co.id		\N	\N
55	PT. BERDIKARI UTAMA JAYA	5	1571	Jl. Letnan Muda Sani Bandung No. 8/65 T Sungai Putri, Kec. Telanaipura Kota Jambi Jambi	berdikari.utamajaya@yahoo.com	0741-63499	\N	\N
56	PT. SINAR AGUS TEHNIK	5	1571	Jl. Sersan Anwar Bay Lrg. Pasundan No.57 Kel Bagan Pete. Kec.Kota Baru, Kota Jambi Kota Jambi Jambi	pt.sinaragustehnik@YAHOO.CO.ID	08127875987	\N	\N
57	PT. MITRA GLOBAL ELEKTRINDO	5	1571	Jl. Raden Wijaya No. 250 RT. 025 Kel. tehok Kec. jambi selatan Kota Jambi Kota Jambi Jambi	mitra_ge@yahoo.co.id	08127400305	\N	\N
58	PT. Skorpion Permata Mandiri	5	1571	Jl. Darma Karya RT. 32 Kel. Kenali Asam Bawah Kec. Jelutung Pos 36128 Jambi Kota Jambi Jambi	pt.spm.jambi@gmail.com	+62 852 6611 1768	\N	\N
59	PT. MANDARSI TIGA SEKAWAN	5	1571	Perum. Kembar Lestari Blok A.10 Rt.45 Kel. Kenali Besar Kec. Alam Barajo Kota Jambi Kota Jambi Jamb	pt.mandarsi@gmail.com	085266521376	\N	\N
60	PT. REZA TEKNIK MANDIRI	5	1571	Jl. Letkol Hasan Effendi No. 52 RT. 025 Kel. Sungai Putri Kec. Telanai Pura Kota Jambi Jambi	rahviqtungkal@gmail.com	081274972958	\N	\N
61	PT. AN ALMURTADHA	5	1571	JL.LETKOL.HASAN EFENDI RT.17/06 KELURAHAN SUNGAI PUTRI KECAMATAN TELANAIPURA KOTA JAMBI Kota Jambi Jambi	pt.analmurtadha@gmail.com	081366033158	\N	\N
62	PT. RESTU SINAR KARYA	5	1571	Jl. Guru Muchtar, No. 45 RT. 13, Kelurahan Jelutung, Kecamatan Jelutung Kota Jambi Jambi	restusinarkarya@gmail.com	085266221433	\N	\N
63	PT. HACA TRIO BERKAH	5	1571	Jl. TP. Sriwijaya No.30 RT.01 Kel. Rawasari Kec. Kota Baru Kota Jambi Jambi	hacatehnik2013@yahoo.com		\N	\N
64	PT. RASATI SAKLAR TRIPEL	5	1571	Jl. Pangeran Antasari RT.32 No.74 Kel.Talang Banjar Kec. Jambi Timur Kota Jambi JAMBI Kota Jambi Jambi	rasati.saklar@gmail.com		\N	\N
65	PT. DUNIA JAYA ELEKTRO	5	1571	JLN. SISINGAMANGARAJA N0.05 JAMBI Kota Jambi Jambi	dunia.surya@gmail.com		\N	\N
66	PT. Riski Tekhnik Konstruksi	5	1571	Jl. Kebun Daging No. 01 RT. 33 Kel Mayang Mangurai Kota Jambi Jambi	konstruksiriski@gmail.com		\N	\N
67	PT. Mega Energi Elektrik	5	1571	Jl. Dharmawangsa Rt.31 No.13, Kel. Paal Merah, Kec. Jambi Selatan, Kota Jambi, Provinsi Jambi Kota Jambi Jambi	ptmegaenergielektrik@gmail.com		\N	\N
68	PT. Allabib Sukses Bersama	5	1571	Komplek Puri Mayang / Anggrek Blok A 45, Kel. Mayang Mengurai, Kec. Kota Baru Kota Jambi Jambi	pt.allabibsb@gmail.com		\N	\N
69	PT. Ganting Putra Mandiri	5	1571	Jl. Sunan Giri No.70, RT.004, Kel. Suka Karya Kota Jambi Jambi	gantingpt@yahoo.com		\N	\N
70	PT. VYSMA KARYA PERSADA	5	1571	Jl. Parluhutan Lubis No. 07 B RT. 001 Kel. Telanaipura Kec. Telanaipura Kota Jambi- Prov. Jambi Kota Jambi Jambi	lskn_pusat@Yahoo.co.id		\N	\N
71	PT. HASINDA UTAMA MANDIRI	5	1571	Jl.Dr.Tazar Komplek Permata Buluran Permai Blok-G/06 RT.14 Kelurahan Buluran Kenali Kecamatan Telanaipura Kota Jambi 36123 Kota Jambi Jambi	hasinda_utama21@gmail.com		\N	\N
72	PT. PIJAR BUMANTARA	5	1571	JL.TEUKU SUALAIMAN NO 36 RT 15 KELURAHAN PAKUANBARU KECAMATAN JAMBI SELATAN Kota Jambi Jambi	pijarbumantara@gmail.com		\N	\N
73	PT. PURNAMA ABADI SEJAHTERA	5	1571	Jl. Sunan Drajat Rt.35 Kenali asam Bawah Kec. Kota Baru, Kota Jambi Kota Jambi Jambi	pas.jambi@yahoo.co.id		\N	\N
74	PT. WISNU SAPUTRA UTAMA	5	1571	Jl. A Hasyim Rt. 003 Kel. Eka Jaya Kec. Paal Merah Kota Jambi Jambi	pusat.askomelin@gmail.com		\N	\N
75	PT. BIR'ALI TERANG HIDAYAH	5	1571	Jl. Mayjend Sutoyo No. 33 Rt. 08 Kel. pamatang sulur Jambi Kec. Telanaipura Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
76	PT. BELAPAN KUMBANG JANTAN	5	1571	Jl. Irian No. 75 Rt. 28 Kel. Lebak Bandung Kec. Jelutung Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
77	PT. YUESBE JAYA SENDIRAL	5	1571	Jl. Pratu Satir Komplek Prama Residence Blok A No.06 Kel. Thehok Jambi Thehok Kec. Jambi Selatan Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
78	PT. SINAR KUMBANG SAKTI	5	1571	Jl. Pratu Satir Komplek Parma Residence Blok A/7 No. 6 Kel. Thehok Kec. Jambi Selatan Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
79	PT. SEGARIS KARYA UTAMA	5	1571	Jl. DR. Tazar Perum Permata Citra 4 Blok H 3 Rt. 18 Kel. Buluran Kenali Kec.Telanaipura Kota Jambi Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
80	PT. RASATI SAKLAR TRIVEL	5	1571	Jl. Pangeran Antasari Rt. 32 No. 74 Kel. Talang Banjar Kec. Jambi Timur Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
81	PT. RALKYKO PRIMA TEKINDO	5	1571	Jl. Syailendra No. 86 Rt.15 Kel. Rawasari Kec. Kotabaru Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
82	PT. NILAM SARI CIPTA AMRIZAL	5	1571	Jl. Yunus Sanis Lr. Andalas No. 38 Rt. 02/02 Kel. Kebun Handil Kec. Jelutung Kota Jambi Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
83	PT. BERSAMA MENERANGI NUSANTARA	5	1571	Jl. Dharma Karya II NO.69 RT.32 Kel. Kenali Asam Bawah Kec. Kotabaru Kota Jambi Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
84	PT. RAFI JAYA TIARA	5	1571	Jl. Batam No. 05 Rt. 24 Kel. Lebak Bandung Kec. Jelutung Jambi Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
85	PT. KARYA PUTRA PERKASA	5	1571	Jl. Sei Lilin No. 10 Rt. 13 Kel. Kenali Asam Atas Kec. Kota Baru Jambi Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
86	PT. EHSAN TERANG PERKASA	5	1571	Jl.Mayjend Sutoyo No. 33 Rt. 08 Kel. Pematang Sulur Kec. Telanaipura Kota Jambi Pematang Sulur Kec. Telanaipura Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
87	PT. JABAL RAHMAH ABADI JAYA	5	1571	Jl. Perumnas Aurduri Blok C RT.16 NO.181 Penyengat Rendah Kec. Telanaipura Kota Jambi Kota Jambi Jambi	pusat@ptaskomelin.co.id		\N	\N
88	PT. PUTRA PARIS JAMBI MANDIRI	5	1571	Jl. T.P Sriwijaya No. RT/RW.07 Kelurahan Beliung Kecamatan Kota Baru Kota Jambi Jambi	ppjmandiri@gmail.com		\N	\N
89	PT. SINAR DUNIA ABADI	5	1571	JL.PRABU SILIWANGI NO. 41 RT. 21 KEL. TANJUNG SARI - KEC. JAMBI TIMUR Kota Jambi Jambi	sinarduniaabadi.sda@gmail.com		\N	\N
90	PT. Mulia Citra Utama	5	1571	Jl. Kopral Kahar Komplek Puri Beringin Blok E No. 08 Kel. The Hok Kec. Jambi Selatan Kota Jambi Kota Jambi Jambi	muliacitrautama@yahoo.co.id		\N	\N
91	PT. DEROZ UTAMA KARYA	5	1571	JL.RAJA YAMIN NO.22 RT.O2 KELURAHAN TELANAIPURA KECAMATAN TELANAIPURA KOTA JAMBI Kota Jambi Jambi	ptderozutamakarya@gmail.com		\N	\N
92	PT. PATHUR TEKNIK MANDIRI	5	1571	Jl. Slamet Riadi Lrg. Remaja RT. 25 Kel. Solok Sipin Kec. Telanaipura Kota Jambi Jambi	ceve.jtm@gmail.com		\N	\N
93	PT. CITRA DUTA BUANA	5	1571	JL. SUNAN BONANG NO.180 RT.18 KELURAHAN SIMPANG III SIPIN KECAMATAN KOTA BARU Kota Jambi Jambi	citradutabuana@yahoo.com		\N	\N
94	PT. MEGA FAMILY SUKSES	5	1571	Jl. Sunan Giri No. 70Rt. 04 Sukakarya, Kotabaru Kota Jambi Jambi	mfahroni@yahoo.com		\N	\N
95	PT. MITRA MANDIRI ELEKTRIKAL	5	1571	Jl. Sriwijaya No. 50 Rt. 007 Beliung Kota Baru Kota Jambi Jambi	mitramandirielektrikal@yahoo.co.id		\N	\N
96	PT. MITRA UTAMA CONSERSIUM	5	1572	Jl. Yos Sudarso No. 92 LK. Renah Surian, Kecamatan Pondok Tinggi Kota Sungai Penuh Jambi	ptmitrautamaconsesium@gmail.com	08127470696	\N	\N
97	PT. BERKAH DIKARI ELECTRICAL	7	3601	Jl. Raya Serang Kp. Curugsawer Rt.01/09 Pandeglang Kec. Pandeglang Kab. Pandeglang Banten	berkahdikari@yahoo.com	0253-5550830	\N	\N
98	PT. MUTIARA KARANG SABILA	7	3601	Jl. Raya Serang - Pandeglang KM.12 Pandeglang, Banten Kab. Pandeglang Banten	mutiarakarangsabila@yahoo.com		\N	\N
99	PT. NUSANTARA UTAMA RAYA	7	3604	Jl. Raya Petir Serang, Kp. Sabrang Rt.014/04 Desa Petir Kec. Petir Kab. Serang Banten	pt.nur14@gmail.com		\N	\N
100	PT. CITRA TEKNIK ANDALAS	7	3604	JL. USMAN EFFENDI NO.40 SEPANGTANJAK RT.05/RW.06 KEL. SERANG BANTEN Kab. Serang Banten	citrateknik.andalas@yahoo.com		\N	\N
101	PT. KARYA HEKSA SAUDARA	7	3604	KOMP. PCI BLOK C68 NO. 03 RT 01/06 DS. HARJATANI KEC. KRAMATWATU Kab. Serang Banten	heksatehnik@yahoo.com		\N	\N
102	PT. NAYLA ZAHWA MANDIRI	7	3603	Kp. Pasirnangka RT. 002/001 Desa Pasirnangka Kec. Tigaraksa Kab. Tangerang Banten	nayla_zahwa11@yahoo.com	021-59401921/0859-3954-9119	\N	\N
103	PT. RIYAN JAYA MAKMUR	7	3603	Jl. Raya Mauk KM. 8 Sepatan Rt. 002 Rw. 002 Desa Pondok Jaya Kecamatan Sepatan Kab. Tangerang Banten	pt.riyanjayamakmur@yahoo.co.id	59371774	\N	\N
104	PT. DIKCY JAYA UTAMA	7	3603	Jl. Raya Mauk KM. 8 RT. 002 RW. 002 Desa pondok Jaya Kecamatan Sepatan Kab. Tangerang Banten	pt.dickyjayautama@yahoo.co.id	59371774	\N	\N
105	PT. WASIS UNGGUL WISESA	7	3603	JL. RAYA BALARAJA - KRONJO KP. TONJONG RT.013/004, KEL. RANCAILAT KEC. KRESEK KAB. TANGERANG BANTEN Kab. Tangerang Banten	wasisunggulwisesa@gmail.com	08176878222	\N	\N
106	PT. Gridtech Indonesia	7	3603	Jl. Millenium 4-A.24/5 Peusar, Panongan, Tangerang, Banten 15710 Kab. Tangerang Banten	info@gridtech.co.id	021-54350157	\N	\N
107	PT. Nilam Cipta Anugrah	7	3603	Villa Taman Bandara Blok O 5 No. 21 Rt. 009 Rw. 09 Kel. Dadap Kec. Kosambi, Kab. Tangerang - Banten Kab. Tangerang Banten	pt.nilamcipta_anugrah@yahoo.co.id	021 - 5561653 - 081287906036	\N	\N
108	PT. Shima Citra Selaras	7	3603	Mardi Grass Blok KF.03 No,03 Citra Raya, kelurahan Mekar Bakti, kecamatan Panongan Kabupaten Tangerang, Banten Kab. Tangerang Banten	shimacitraselaras_pt4@yahoo.com	021 - 59404580	\N	\N
109	PT. PRIMA AGUNG PERKASA	7	3603	JL KALI PASIR NO 11 RT 04 RW 04 KEL SUKA SARI KEC TANGERANG KAB BANTEN Kab. Tangerang Banten	primaagungperkasa@yahoo.com	081219580058	\N	\N
110	PT. Panca Jaya Firdaus	7	3603	Villa Taman Bandara Blok O-5 Rt.009/010 Kel. Dadap, Kec. Kosambi, Tangerang - Banten Kab. Tangerang Banten	willy.dadap@gmail.com	021 - 5561653 - 085211657560	\N	\N
111	PT. Andriv Jaya Bersama	7	3603	Villa Taman Bandara Blok O 5 No.21 Rt. 009 Rw. 010 Kel. Dadap, Kec. Kosambi, Tangerang - Banten 15211 Kab. Tangerang Banten	willy.dadap@gmail.com	021 - 5561653 - 081210410903	\N	\N
112	PT. RAMEYZA ELYA LESTARI	7	3603	VILLA TAMAN BANDARA BLOK C12 NO.19\\\\r\\\\nRT005/RW010 DADAP - KOSAMBI TANGERANG 15211 Kab. Tangerang Banten	RAMEYZAELYALESTARI@YAHOO.COM	021 5558460/ 081389595049	\N	\N
113	PT. TRI AGUNG	7	3603	Jl. Kelapa Puan XXII AK.2/2 Rt.001/010 Kab. Tangerang Banten	ririlsuhartinah@yahoo.com	021-54204826	\N	\N
114	PT. MULYA SINAR PERKASA	7	3603	KP. SUKAMULYA RT.032 RW. 006 KELURAHAN SUKAMULYA, KECAMATAN CIKUPA KABUPATEN TANGERANG, BANTEN Kab. Tangerang Banten	muslimsulaeman43@gmail.com		\N	\N
115	PT. MAULANA JAYA MANDIRI	7	3603	Jl. Kota Batara - Galih Rt.03/02 Kel. Pesanggrahan Kec. Solear Kab. Tangerang Banten	maujadipt@gmail.com		\N	\N
116	PT. BANYU EMAS BERSAMA	7	3603	Jl. Patuha Raya No.55 Rt.010/004 Desa Suradita Kec. Cisauk Kab. Tangerang Banten	banyuemasbersama@gmail.com		\N	\N
117	PT. FEBRIYAN CIPTA ANUGRAH	7	3603	Jl. Raya Ranca Dulang Rt.007/002 Desa Rancaiyuh Kec. Panongan Kab. Tangerang Banten	febri.yansyah.fy45@gmail.com		\N	\N
118	PT. ERA JAYA SAKTI BERSAMA	7	3603	Jl. Raya Pagedangan Kp. Cicayur I Rt.01/02 No.2 Desa Pagedangan Kec. Pagedangan Kab. Tangerang Banten	erajaya.saktibersama@gmail.com		\N	\N
119	PT. DWI RAHMA GEMILANG	7	3603	JL. BIDAR IV A NO.10. RT.004/008, KEL.KELAPA DUA. KEC. KELAPA DUA Kab. Tangerang Banten	dwirahmagemilang@yahoo.com		\N	\N
120	PT. PANGLIMA PARA SATRIA	7	3603	Taman Telaga Mediterania Blok O. 01/15 RT.44/03, Kel. Ciakan, Kec. Panongan Kab. Tangerang Banten	asiregar1455@yahoo.com		\N	\N
121	PT. BUHIT PERSADA	7	3672	JL. KEMUNING VI K-6 NO 03 RT 27 RW 05, CIWEDUS CILEGON, BANTEN Kota Cilegon Banten	buhit.persada@gmail.com	0254-383305 / 0811127141	\N	\N
122	PT. LENTERA LESTARI	7	3672	Jl. Ir. H. Sutami Link. Delingseng No.06 Kel. Kebonsari Kec. Citangkil Kota Cilegon Banten	lentera_lestari_pt@yahoo.com		\N	\N
123	PT. FADAFUS INSPIRINDO	7	3673	Lingkungan Pabuaran RT. 004 RW. 004 Kelurahan Unyur, Kecamatan Serang, Kota Serang, Banten Kota Serang Banten	fadafus_inspirindo@ymail.com	0254228365	\N	\N
124	PT. AGUSTINA SEJAHTERA ABADI	7	3673	PERUM BUKIT PERMAI BLOK N NO 24 RT 04/RW 15 KEL. SERANG, KEC. SERANG Kota Serang Banten	yudadha@gmail.com	0254 219427 / 081389595049	\N	\N
125	PT. JAHERMOSA	7	3673	Jl. Ruko Sukses 2 No. R 26 RT 002/001 Kel. Sumur Pecung Kec. Serang Kota Serang Banten	marketing@jahermosa.com	0254-8482854	\N	\N
126	PT. AMANAH MULTI KREASI	7	3673	CIJAWA GEDE NO. 46 RT. 002 RW. 002 KEL, CIPARE KEC, SERANG KOTA SERANG BANTEN Kota Serang Banten	office@amkreasi.com		\N	\N
127	PT. LINGGAR BHAKTI TEKNIKA	7	3673	Komp. Ciputat Indah Blok H No.4 Rt.001/010 Kaligandu Kec. Serang Kota Serang Banten	elbete@rocketmail.com		\N	\N
128	PT. ENERGITAMA PANEN SEJAHTERA	7	3673	Perumahan Taman Mutiara Indah Blok I.8 No.19 Kel. Kaligandu Kec. Serang Kota Serang Banten	energitama111@gmail.com		\N	\N
129	PT. DINO FORTUN INVESTAMA	7	3673	TAMAN BANTEN LESTARI BLOK 3C NO. 42 KEL. UNYUR KEC. SERANG BANTEN Kota Serang Banten	dinofortun_investama@yahoo.co.id		\N	\N
130	PT. ANTA JAYA LAKSANA	7	3673	Taman Mutiara Indah Rt. 006/018 Kaligandu Kec. Serang Kota Serang Banten	pusat@ptaskomelin.co.id		\N	\N
131	PT. OVITE TEKNIK CONTRACTOR	7	3673	JL. KH. Abdul Fatah Hasan No.81 Kel. Cipare Kec.Serang Kota Serang Banten	cv_ovite@yahoo.com		\N	\N
132	PT. KEMAL UNGGUL SEJAHTERA	7	3673	PDK PENANCANGAN BARU NO.171 RT.003 RW.004 Kel. Kaligandu kec. Serang Kota Serang Banten	pusat@ptaskomelin.co.id		\N	\N
133	PT. TAHTA NABILA JAYA	7	3673	JL. RAYA PETIR KP. TIMBANG RT 001/001 KEL. CILAKU, KEC. CURUG Kota Serang Banten	tahtanabilajaya@gmail.com		\N	\N
134	PT. ASTILA PURNAMA	7	3673	Jl. TB. SUWANDI RUKO PURI MUTIARA NO. 3 SERANG 42116 Kota Serang Banten	cv_astila@yahoo.com		\N	\N
135	PT. PUTRA BANTEN MANDIRI ELEKTRIKAL	7	3673	KOMP. BUKIT CIRACAS PERMAI BLOK C 5 NO. 3 Kota Serang Banten	cv_pbm@yahoo.com		\N	\N
136	PT. WAHANA AGUNG MANDIRI	7	3673	JL. KH. ABDUL LATIF No. 76 SERANG Kota Serang Banten	pt.wahanaagungmandiri@yahoo.com		\N	\N
137	PT. PUNCAK TIGA PILAR	7	3673	Taman Puri Indah A.1 No. 3 Jl. Tb. Suwandi Ciracas Kel. Serang Kec. Serang Kota Serang Banten	puncak3pilar@yahoo.co.id		\N	\N
138	PT. BUKIT TEKNIK	7	3673	JL. KH. A FATAH HASAN NO. 52 RT. 03 RW. 01 KEL. CIPARE KEC. SERANG - BANTEN Kota Serang Banten	bukit_teknik@yahoo.com		\N	\N
139	PT. AMARTA MEGANANTA PERSADA	7	3673	JL. BENGGALA RSU RT.004/010 KEL. CIPARE, SERANG Kota Serang Banten	amartamegananta@yahoo.co.id		\N	\N
140	PT. HIRAYASA UTAMA ELEKTRIKA	7	3673	JL. KOL YUSUF MARTADILAGA NO.20 RT.004/010 KEL CIPARE, SERANG Kota Serang Banten	hirayasautamaelektrika@yahoo.co.id		\N	\N
141	PT. MUTIARA ENERGI TEKNIK	7	3673	JALAN K.H. ABDUL FATAH HASAN NO. 11 RT 001 RW 006 KEL. SUMUR PECUNG, KEC. SERANG BANTEN Kota Serang Banten	mutiara_et3@yahoo.com		\N	\N
142	PT. ROYAL CITRA ABADI	7	3671	JL. SADAR RT. 001 RW. 001 KELURAHAN PANINGGILAN KEC. CILEDUG Kota Tangerang Banten	royalcitraabadi@yahoo.co.id	02173455773	\N	\N
143	PT. TRANS BATU LICIN	7	3671	JL. KASASI VII BLOK C8 NO.10, KELURAHAN SUKASARI, KECAMATAN TANGERANG Kota Tangerang Banten	ridoadityamaulana@yahoo.com	08126000257	\N	\N
144	PT. PANCAPUTRI BINABAWANA	7	3671	Ruko Cendana No.63 jl. benteng betawi rt 04 rw 15 kel. tanah tinggi kec.Tangerang kota Tangerang Kota Tangerang Banten	binabawana_pt@yahoo.com	021-29009295	\N	\N
145	PT. RIFAMAS ALDERIA	7	3671	Jl. Jati 1 No. 23 Rt. 10/15 Cluster Jati Taman Royal 3 Kel. Tanah Tinggi Kec. Tangerang Kota Tangerang Kota Tangerang Banten	rifamas.alderia@gmail.com	085214886664	\N	\N
146	PT. DIPO MANUNGGAL JAYA	7	3671	Jl. Eksekusi 4 Blok E 4 No. 6 Rt.004 RW. 013 Kel. Sukasari Kec. Tangerang Kota Tangerang Provinsi Banten Kota Tangerang Banten	dipo_pt@yahoo.com	021 - 2900 9295	\N	\N
147	PT. CITRAMAS PUTRA MANDIRI	7	3671	Jl. Kemuning I Blok A-12 No. 17 RT. 08/08 Griya Sangiang Mas Kel. Gebang Raya, Kec. Periuk Kota Tangerang Banten	ptcitramasputramandiri@yahoo.co.id	021-5512914, 08159913347	\N	\N
148	PT. LUMINTU INSAN MANDIRI	7	3671	JL. TROCADERO NO. 15 RUKO PALAIS DE EUROPE, KARAWACI-TANGERANG Kota Tangerang Banten	surat@pt-lim.com	021-55767333	\N	\N
149	PT. RAKSHA AIDAN NALADHIPA	7	3671	JL. GENO UTOMO NO 4 RT 03/02 KEL. SUMUR PACING KECAMATAN KARAWACI KOTA TANGERANG 15114 Kota Tangerang Banten	aidannaladhipa@gmail.com	021.55731843/0816927608	\N	\N
150	PT. JAYA TERANG SAMUDRA	7	3671	JALAN BANDING II BLOK D5 NO. 15 KOMPLEK KEHAKIMAN KOTA TANGERANG Kota Tangerang Banten	jayaterangsamudra111213@gmail.com	081310950877 / 085691776969	\N	\N
151	PT. MERAIH KEJAYAAN BARU	7	3671	Puri Kartika Blok D1 No. 3 RT.001 RW.008, Kelurahan Tajur, Kecamatan Ciledug Kota Tangerang, Banten Kota Tangerang Banten	meraihkejayaanbaru@gmail.com	0812 - 9767 - 0235	\N	\N
152	PT. Citra Mikar Perkasa	7	3671	Jl. Pondok Surya Blok E/14 RT.008/11. Kelurahan Karang Tengah, Kecamatan Karang Tengah Kota Tangerang, Banten Kota Tangerang Banten	pt.citramikarperkasa@gmail.com	0813 - 1498 - 3167	\N	\N
153	PT. MULTI KREASINDO MEGATAMA	7	3671	Ciledug Indah II Rt.008/001 Kel. Pedurenan Kec. Karang Tengah Kota Tangerang Banten	multikreasindomegatama@gmail.com	081290953330	\N	\N
154	PT. MANDALA INDAH SAKTI	7	3671	JL. H MANSYUR NO.13 NEROKTOG. PINANG. KOTA TANGERANG. Kota Tangerang Banten	mandalaindahsakti@yahoo.com	02155702228	\N	\N
155	PT. ERINU PUTRA BUANA	7	3671	JL. KH HASYIM ASHARI BUANA PERMAI BLOK E.20 CIPONDOH Kota Tangerang Banten	pt_erinuputrabuana@yahoo.co.id	02155702228	\N	\N
156	PT. NUSA ABADI ELEKTRIKA	7	3671	Jl. Jamblang III No.61 Rt.003/015 Kel. Cibodasari Kec. Cibodas Kota Tangerang Banten	nusa.abadiel80@yahoo.com		\N	\N
157	PT. MAHKOTA RADIAND THERATOHN	7	3671	Jl. KH. Agus Salim Gg. Sawo II Rt.001/005 Poris Plawad Kec. Cipondoh Kota Tangerang Banten	mahkota.radiand.theratohn@gmail.com		\N	\N
158	PT. DEWI KENCANA SAKTI	7	3671	Jl. Arya Wangsakara No.103 Rt.004/003 Kelurahan Uwung Jaya Kecamatan Cibodas Kota Tangerang Kota Tangerang Banten	dewikencanasakti@yahoo.com		\N	\N
159	PT. INDO JAYA PERSADA	7	3674	JL RAYA PONDOK KACANG NO 15 B RT 002 / RW 003 KEL PONDOK KACANG TIMUR KEC. PONDOK AREN, TANGERANG SELATAN, BANTEN Kota Tangerang Selatan Banten	indojayapersada@yahoo.co.id	02129041400 / 081380809922	\N	\N
160	PT. TRI STARS NUSANTARA	7	3674	Ruko Versailles Blok FB/16 Lt. 2 Sektor 1-6 BSD City, Tangerang Selatan Kota Tangerang Selatan Banten	tsntri@yahoo.com	021 - 22756780	\N	\N
161	PT. PRAKASA PUTRA WIRATAMA SAKTI	7	3674	Graha Ayuni Lt.2A, Jl. Garuda Blok L, No. 1, Pondok Aren, Kec. Pondok Aren Kota Tangerang Selatan Banten	proc@prakasaws.com	021-7457791	\N	\N
162	PT. AMSAK BANGUN PERSADA	7	3674	Jl. Pahlawan Seribu, Ruko Golden Boulevard E-09, BSD City, Kel Lengkong Karya, Kec. Serpong Utara Kota Tangerang Selatan Banten	amsak_bp@yahoo.com	02153160814, 08159877500	\N	\N
163	PT. INTI SOLUSI PERMITELINDO	7	3674	JL.SALEM 1 RT/RW 001/07 NO 38, KEL.SERPONG, KOTA TANGERANG SELATAN Kota Tangerang Selatan Banten	isp.solusi@yahoo.com	081906174107	\N	\N
164	PT. Tripatra Engineers And Constructors	7	3674	Indy Bintaro Office Park Jl. Boulevard Bintaro Jaya Blok B7/A6 Sektor VII CBD Bintaro Jaya, Tangerang Selatan 15224 Kota Tangerang Selatan Banten	ppd@tripatra.com	021-29770700	\N	\N
165	PT. surya kencana multi perkasa	7	3674	jl. vinca kav. 522 bukit nusa indah, serua, ciputat, tangerang, banten Kota Tangerang Selatan Banten	skmperkasa@gmail.com		\N	\N
166	PT. YASA HARJA DHIPA	7	3674	Jl. Sawi No. 89 Rt. 003/005 Pondok Cabe Kec. Pamulang Kota Tangerang Selatan Banten	pusat@ptaskomelin.co.id		\N	\N
167	PT. ARTHO MORO LUMINTU	7	3674	Jl. Mekarsari No.64 Pondok Betung Kec. Pondok Aren Kota Tangerang Selatan Banten	pusat@ptaskomelin.co.id		\N	\N
510	PT. RAHMAYUTI SARANA MANDIRI	8	3101	JL. SULTAN ISKANDAR MUDA NO. 30 Lt. 1B RT. 001/007 Kab. Adm. Kep. Seribu DKI Jakarta	vivivivi884@gmail.com		\N	\N
511	PT. RULLYTAMA SABILLA BUANA	8	3173	Rukan Puri Botanical Blok I - 10 No. 20 Jl. Joglo Raya RT. 007 RW. 001, Kelurahan Joglo, Kecamatan Kembangan Kota Kota Adm. Jakarta Barat DKI Jakarta	rullytamasabillabuana@gmail.com	02158900075	\N	\N
512	PT. Sinar Energi Internusa	8	3173	Jl. Kapuk Cengkareng Rukan Hawaii City Resort Residence Blok A No. 50 RT/RW 007/014, Cengkareng Timur, Cengkareng, Jakarta Barat, 11730 Kota Adm. Jakarta Barat DKI Jakarta	finance@pt-sinergi.co.id	021-29319926/27/28	\N	\N
513	PT. Panca Rodearni Solusi	8	3173	Ruko Peta Selatan Indah Blok A6-A7 Kelurahan Kalideres Kecamatan Kalideres, Kota Adm. Jakarta Barat Kota Adm. Jakarta Barat DKI Jakarta	pr.solusi@yahoo.com	021-29020335 ; 021-54399375 / 08161638901	\N	\N
514	PT. BANGUN MEGA PERTIWI	8	3173	Kedoya Elok Plaza Blok DD No. 75 Rt.019 Rw.004 Kel. Kedoya Selatan Kec. Kebon Jeruk Kota Adm. Jakarta Barat DKI Jakarta	indralika2009@yahoo.com	021-79190530	\N	\N
515	PT. CITRA WAHANA SEKAR BUANA	8	3173	KOMP. RUKAN GRAHA ARTERI MAS KAV. 26, JL. PANJANG NO. 68, RT. 019 RW. 004, KEL. KEDOYA SELATAN, KEC. KEBON JERUK, JAKARTA BARAT 11520 Kota Adm. Jakarta Barat DKI Jakarta	cwsb@dnet.net.id	0816-1929850	\N	\N
516	PT. MANDIRI TEKNIK UTAMA SEJAHTERA	8	3173	JALAN KAPUK PULO NO. 100 RT. 015/RW.010 KAPUK CENGKARENG JAKARTA BARAT Kota Adm. Jakarta Barat DKI Jakarta	pt.mtus@gmail.com	021-5458060, 22559025	\N	\N
517	PT. PAMOR NUGRAHA	8	3173	Rukan Puri Botanical Residence Blok I 10 No. 20 RT. 007/001, Jl. Raya Joglo Kel. Joglo Kec. Kembangan Jakarta Barat 11640 Kota Adm. Jakarta Barat DKI Jakarta	Pamornugrahapt@gmail.com	021-58900075	\N	\N
518	PT. Mitra Adikarsa	8	3173	Ruko Citra Business Park Blok F No.33 Jl. Peta Barat Citra Business Park Rt.001 Rw.007 Kalideres Kec. Kalideres Kota Administrasi Jakarta Barat Kota Adm. Jakarta Barat DKI Jakarta	mitradikarsa@gmail.com		\N	\N
519	PT. Ondo Usahatama Bersama	8	3173	Graha Usahatama Lt.2, Jl. Budi Raya No. 41, RT/RW. 008/009. Kel. Kemanggisan Kec. Palmerah Kota Adm. Jakarta Barat DKI Jakarta	ptondousahatama@gmail.com		\N	\N
520	PT. ARENA CIPTA CAHYA CERIA	8	3173	JL. LINGKUNGAN III RT.09/09 NO.20 TEGAL ALUR-KALIDERES-JAKARTA BARAT Kota Adm. Jakarta Barat DKI Jakarta	pt.arenacipta_cahyaceria@yahoo.co.id		\N	\N
521	PT. SURYA PRIMA ELTRINDO	8	3173	Taman Palem Lestari, Cengkareng, Jakarta Barat Kota Adm. Jakarta Barat DKI Jakarta	sales@ptspe.com		\N	\N
522	PT. Gerbang Multindo Nusantara	8	3173	Jl.Raya Meruya Ilir, Ruko Taman Kebon Jeruk Blok A XI/21 Kel.Srengseng Kec.Kembangan Kota Adm. Jakarta Barat DKI Jakarta	gmn@indo.net.id		\N	\N
523	PT. BONA DUPANG SOALON	8	3173	Komp Green Mansion Jl. Daan Mogot 45 A/5 Blok C No. 8 Rt. 007/006 Kel. Jelambar Kec. Grogol Petamburan Kota Adm. Jakarta Barat DKI Jakarta	adm@bonadupang.com		\N	\N
524	PT. WISANGGENI MITRA SEJAHTERA	8	3173	Jl. Jembatan Batu No.82-83 Rt. 005/004 Pinangsia Kec. Tamansari Kota Adm. Jakarta Barat DKI Jakarta	info@wms-indonesia.com		\N	\N
525	PT. TRIMATEN GEMILANG	8	3173	Wisma Gemilang Lt. II Budi Raya No. 41 Komplek Pajak RT 008/09 Kel. Kemanggisan Kec. Palmerah Kota Adm. Jakarta Barat DKI Jakarta	trimaten_gemilang@yahoo.co.id		\N	\N
526	PT. ENERGI POWERINDO JAYA	8	3173	Jl. Kapuk Pulo No. 2 Rt 004 Rw 010 kel. Kapuk Kec Cengkareng jakarta Barat DKI Jakarta Kota Adm. Jakarta Barat DKI Jakarta	epj@epj.co.id		\N	\N
527	PT. INFORMATION DATASYSTEM	8	3173	Jl. Raya Duri Kosambi No. 26B Jakarta Barat 11750 Kota Adm. Jakarta Barat DKI Jakarta	tigor_adrian@indo.net.id		\N	\N
528	PT. Centra Multi Elektrindo	8	3173	Jl. Kemukus No.32 Blok B No. 27 RT?RW 004/006, kel. pinangsia kec.tamansari . Jakarta Barat . DKI JAKARTA . 11110 Kota Adm. Jakarta Barat DKI Jakarta	Centra.multi@gmail.com		\N	\N
529	PT. RIMBANUSA PIKANTIMAS	8	3173	Wisma Pikantimas Lt.II, Jl. Sakti 2 No. 35 RT. 008/09 Komplek Pajak Kel. Kemanggisan Kec. Palmerah Kota Adm. Jakarta Barat DKI Jakarta	rimbanusa_pikantimas@yahoo.co.id		\N	\N
530	PT. TOTAL TOLERANSI JAYA	8	3173	Rukan Puri Botanical Residence Blok I - 10 Jl. Joglo Raya Rt. 007/001 No. 20 Kota Adm. Jakarta Barat DKI Jakarta	totaltoleransi@gmail.com		\N	\N
531	PT. AGUNG PRIMA PERKASA	8	3173	Jl. H. Soleh II No. 15, RT. 003 RW. 02 Kel. Sukabumi Selatan, Kebon Jeruk Jakarta Barat Kota Adm. Jakarta Barat DKI Jakarta	soewarto_ds@yahoo.com		\N	\N
532	PT. PERFECT CIRCLE ENGINEERING	8	3173	Jl. Bungur Besar No. 46 F. Kel. Gunung Sahari Selatan, Kec. Kemayoran Jakarta Pusat, DKI Jakarta Kota Adm. Jakarta Pusat DKI Jakarta	perciejkt@percie.co.id	021 7947010	\N	\N
533	PT. Sinergi Terang Bangsa	8	3173	Jl. Tanah Abang 1 No. 11F, Petojo Selatan, Kecamatan Gambir Kota Adm. Jakarta Pusat DKI Jakarta	sinergiterangbangsa@gmail.com	081298062480 / 081327272828	\N	\N
534	PT. UNIAMP GRIDATAMA LISTRINDO	8	3173	JL. Kali Baru Barat no. 2 Kota Adm. Jakarta Pusat DKI Jakarta	uniamp.office@gmail.com	021-4251681	\N	\N
535	PT. Mega Eltra	8	3173	Jalan Menteng Raya no 27 Jakarta Pusat 10340 Kota Adm. Jakarta Pusat DKI Jakarta	marketingdjk.me@gmail.com	081262680064	\N	\N
536	PT. ZHEJIANG TENAGA PEMBANGUNAN INDONESIA	8	3173	Wisma BSG Lt. 3A Jl. Abdul Muis No. 40 Kel. Petojo Selatan Kec. Gambir Kota Adm. Jakarta Pusat DKI Jakarta 10160 Kota Adm. Jakarta Pusat DKI Jakarta	bayusantika8@gmail.com	021-3859000	\N	\N
537	PT. PRIMA LANGGENG UTAMA	8	3173	JL. KEBON SIRIH NO. 63, LT. 9 KEBON SIRIH, KEC. MENTENG, JAKARTA PUSAT Kota Adm. Jakarta Pusat DKI Jakarta	general@plu.co.id	0215686734	\N	\N
538	PT. DELTA SARANA ENGINEERING	8	3173	RUKO CEMPAKA MAS BLOK O NO. 48 JL. LETJEN SUPRAPTO JAKARTA PUSAT 10640 - INDONESIA Kota Adm. Jakarta Pusat DKI Jakarta	delta@sarana-eng.com	021-4269080	\N	\N
539	PT. MAXPOWER INDONESIA	8	3173	BRI II Building, 8 th Floor, Suite 810, Jl. Jend. Sudirman Kav. 44-46 Kota Adm. Jakarta Pusat DKI Jakarta	info@navigat.com	021 5724944	\N	\N
540	PT. ANGGADA PERKASA TEHNIK	8	3173	JL. KEMBANG RAYA NO.18B RT.03 RW.01 KEL.KWITANG KEC.SENEN JAKARTA PUSAT Kota Adm. Jakarta Pusat DKI Jakarta	anggada.pt@gmail.com	081310455978	\N	\N
541	PT. Cellindo Inti Perkasa	8	3173	jl. proklamasi No.75-75A, jakarta pusat Kota Adm. Jakarta Pusat DKI Jakarta	sales@cellindo.co.id	021-3146477	\N	\N
542	PT. Andika Binangun	8	3173	Jl.Gunung Sahari I No.42 RT018 RW004, Kel. Senen, Kec. Senen, Jakarta Pusat Kota Adm. Jakarta Pusat DKI Jakarta	andikabinangun.pt@gmail.com		\N	\N
543	PT. ROLLY ELECTRA KARYA	8	3173	JL. BENDUNGAN JAGO RAYA K 44 RT 013/02 SERDANG KEC. KEMAYORAN Kota Adm. Jakarta Pusat DKI Jakarta	rolly.electra@gmail.com		\N	\N
544	PT. Energi Indonesia Persada	8	3173	Perkantoran Menara Era lantai 12A, Unit 05 Jl.Senen Raya No. 135 Kel. Senen Kec.Senen Kota Adm. Jakarta Pusat DKI Jakarta	eip@energi-ip.com		\N	\N
545	PT. MATLAMAT CAKERA CANGGIH	8	3173	SINARMAS LAND PLAZA, MENARA 2, LANTAI 12, JL. MH. THAMRIN KAV. 51, JAKARTA 10350 Kota Adm. Jakarta Pusat DKI Jakarta	elly-hendriani@mcc.co.id		\N	\N
546	PT. ISPA MANDIRI	8	3173	Jl. Paseban Raya No. 20 RT 005 / RW 003 Kota Adm. Jakarta Pusat DKI Jakarta	imandirimz@gmail.com		\N	\N
547	PT. PESAT JAYA UTAMA	8	3173	JL. KRAN RAYA NO. 6 RT 005 RW 003 KEL. GUNUNG SAHARI SELATAN KEC. KEMAYORAN Kota Adm. Jakarta Pusat DKI Jakarta	pesat@cbn.net.id		\N	\N
548	PT. GRAHA INSTALINDO SEMESTA	8	3173	JL. LETJEND SUPRAPTO NO. 29M RT 07 RW 02 KELURAHAN HARAPAN MULIA KECAMATAN KEMAYORAN Kota Adm. Jakarta Pusat DKI Jakarta	g.i.s@msn.com		\N	\N
549	PT. TRUNOJOYO SUMBER LISTRINDO	8	3173	JL. ALAYDRUS NO.20 PETOJO UTARA JAKARTA PUSAT 10130 Kota Adm. Jakarta Pusat DKI Jakarta	info@tsl.co.id		\N	\N
550	PT. MACCINI RAYA PERKASA	8	3173	JL. CEMPAKA PUTIH II/1 CEMPAKA PUTIH PERMAI BLOK C-10 KEL. CEMPAKA PUTIH TIMUR KEC. CEMPAKA PUTIH Kota Adm. Jakarta Pusat DKI Jakarta	maccini_raya@yahoo.com		\N	\N
551	PT. Rekadaya Elektrika	8	3173	Gedung Bank Mandiri Lt.6 Jl. Tanjung Karang No.3-4A Kelurahan Kebon Melati Kecamatan Tanah Abang Jakarta 10230 Kota Adm. Jakarta Pusat DKI Jakarta	yudha.joko@rekadaya.co.id		\N	\N
552	PT. Duta Listrik Graha Prima	8	3173	Jl. Hayam Wuruk 4 F H, Kelurahan Kebon Kelapa, Kecamatan Gambir Kota Adm. Jakarta Pusat DKI Jakarta	sales@ptduta.com		\N	\N
553	PT. D&C Engineering Company	8	3173	Jl. Cideng Barat No. 33 RT 003 RW 011, Kelurahan Cideng, Kecamatan Gambir, Jakarta Pusat Kota Adm. Jakarta Pusat DKI Jakarta	legal.sumbergas@gmail.com		\N	\N
554	PT. THE SECOND CONSTRUCTION ENGINEERING THIRD ENGINEERING BUREAU	8	3173	Jl. Industri Kav. B. 16 Royalton Blok D No. 18 Kelurahan Gunung Sahari Utara Kecamatan Sawah Besar Jakarta Pusat, DKI Jakarta Kota Adm. Jakarta Pusat DKI Jakarta	jiangchao@china-hei.com		\N	\N
555	PT. ANDRITZ HYDRO	8	3173	Jl. Talang No. 3, Pegangsaan Jakarta Pusat 10320 Kota Adm. Jakarta Pusat DKI Jakarta	Contact-hydro.id@andritz.com		\N	\N
556	PT. GUNA SWASTIKA DINAMIKA	8	3173	Jl. Kwini I / B-4, Lantai 3, Rt.009/ Rw. 001, Kel. Senen, Kec. Senen Kota. Jakarta Pusat Kota Adm. Jakarta Pusat DKI Jakarta	gunaswastika@yahoo.com		\N	\N
557	PT. Berca Buana Sakti	8	3171	Bintaro Business Centre Ruangan 208 Lt. 2, Jln. RC. Vetrean No. 1-i RT. 001 RW. 003 Kel. Bintaro Kec. Pesanggrahan Kota Adm. Jakarta Selatan DKI Jakarta	business@bbsi.co.id	+21 7361979	\N	\N
558	PT. QDC TECHNOLOGIES	8	3171	GEDUNG GRAHA QDC, JL MAMPANG PRAPATAN RAYA NO 28 BLOK C, KEL PELA MAMPANG, KEC MAMPANG PRAPATAN. Kota Adm. Jakarta Selatan DKI Jakarta	sales@qdc.co.id	622179191234	\N	\N
559	PT. ALTRAK 1978	8	3171	BINTARO BUSSINESS CENTER Lt. 2 JL. RC. VETERAN No. 1-i, RT.006, RW. 003, Kota Adm. Jakarta Selatan DKI Jakarta	legal2@altrak1978.co.id	08159986279	\N	\N
560	PT. SYNTEK OTOMASI INDONESIA	8	3171	Jl. Mampang Prapatan Raya No.17 Blok G-H Lt.2 Kel. Mampang Prapatan Kec. Mampang Prapatan Kota Adm. Jakarta Selatan DKI Jakarta	info@syntek.co.id	0217982316	\N	\N
561	PT. REKAYASA INDUSTRI	8	3171	Jalan Kalibata Timur I No. 36 RT. 002 RW. 08 Kelurahan Kalibata, Kecamatan Pancoran Jakarta Selatan 12740 Kota Adm. Jakarta Selatan DKI Jakarta	perizinan_ifs@rekayasa.co.id	0217988700	\N	\N
562	PT. TRUBA JAYA ENGINEERING	8	3171	JL. SWADAYA II NO. 7 RT 006 RW 005 KEL. TANJUNG BARAT KEC. JAGAKARSA Kota Adm. Jakarta Selatan DKI Jakarta	marketing@trubagroup.com	021-7803300, 7801054	\N	\N
563	PT. TAIKISHA INDONESIA ENGINEERING	8	3171	Perkantoran Menara Bidakara 1 Lantai 13, Jl. Jend. Gatot Subroto Kav. 71-73, Kel. Menteng Dalam, Kec. Tebet - Jakarta Selatan Kota Adm. Jakarta Selatan DKI Jakarta	siti@taikisha.co.id	021-83793326	\N	\N
564	PT. BANGUN PRIMA SEMESTA	8	3171	Gd. Menara Karya Lt.3 Unit D, Jl. H.R. Rasuna Said Blok X5 Kav.12 Kelurahan. Kuningan Timur Kecamatan Setiabudi Kota Adm. Jakarta Selatan DKI Jakarta	info@bangunprimasemesta.com	021-82491705 / 08562047838	\N	\N
565	PT. SEPCO III ELECTRIC POWER CONSTRUCTION INDONESIA	8	3171	Gedung Menara Prima Lt.16 Unit E Jl. DR. Ide Anak Gde Agung Blok 6.2, Kelurahan Kuningan Timur, Kecamatan Setia Budi Kota Adm. Jakarta Selatan DKI Jakarta	shouxing.zhang@sepco3.com	0216124817	\N	\N
566	PT. POSCO E&C INDONESIA	8	3171	PACIFIC CENTURY PLACE LEVEL 17 SCBD LOT 10 JL. JEND. SUDIRMAN KAV 52-53 KEL. SENAYAN KEC. KEBAYORAN BARU Kota Adm. Jakarta Selatan DKI Jakarta	diansupenipujiastuti@posco.net	021-80864045	\N	\N
567	PT. GRAMA BAZITA	8	3171	Alamanda Tower Lantai 30 Jl. TB. Simatupang Kav. 23-24 RT/RW/001/001 Kel. Cilandak Barat Kec. Cilandak Kota Adm. Jakarta Selatan Kota Adm. Jakarta Selatan DKI Jakarta	diny.irmawati@gramabazita-tenaga.com	62-2129660300	\N	\N
568	PT. UNITEKNINDO INTI SARANA	8	3171	Gedung I Avenue Office Tower Unit B, Lantai 26, Jl. Raya Pasar Minggu Kav. 16 RT.007 RW.002 Kota Adm. Jakarta Selatan DKI Jakarta	info@uniteknindo.com	02180667084/02157000000	\N	\N
569	PT. Wahanayasa Trans Energi	8	3171	Jl. Lenteng Agung Raya No. 20B, Srengseng Sawah, Jagakarsa Kota Adm. Jakarta Selatan DKI Jakarta	info@ptwte.com	021-7873241	\N	\N
570	PT. SARANA UTAMA ADIMANDIRI	8	3171	PLASA SUA JL PROF DR SOEPOMO NO 27 RT 002 RW 003 KEL TEBET BARAT KEC TEBET JAKARTA SELATAN DKI JAKARTA Kota Adm. Jakarta Selatan DKI Jakarta	nita.kusnadi86@gmail.com	0218291717/082110197075	\N	\N
571	PT. INFRASTRUKTUR TELEKOMUNIKASI INDONESIA	8	3171	Jl. MT. Haryono Kav. 10 RT. 010 RW. 005 Gd. Mugi Griya R.502 Lt. 5 Tebet Barat, Kecamatan Tebet Kota Adm. Jakarta Selatan DKI Jakarta	telkominfra@co.id	021-83708471	\N	\N
572	PT. KALTIMEX ENERGY	8	3171	Jl. HR. Rasuna Said Blok X5 Gedung Menara Karya Lt.19 Kel. Kuningan Timur Kec. Setiabudi Kota Adm. Jakarta Selatan DKI Jakarta	kaltimex@kaltimex.co.id	021-57944600	\N	\N
573	PT. Supraco Indonesia	8	3171	Radiant Utama Group Building Jl. Kapt Tendean No. 24 Mampang Prapatan Kota Adm. Jakarta Selatan DKI Jakarta	spc-formalities@supraco.com	021-7191020	\N	\N
574	PT. Buanareksa Binaperkasa	8	3171	Gedung Graha Arsa Lt. 2, Jl. Siaga Raya No.31 RT 005 RW 005 Pejaten Barat, Kec. Pasar Minggu Kota Adm. Jakarta Selatan DKI Jakarta	ptbuanareksa@gmail.com	021-79194129	\N	\N
575	PT. ABB SAKTI INDUSTRI	8	3171	Gedung WTC 1 Lt. 15, Jl. Jendral Sudirman Kav. 29-31, Kelurahan Karet, Kecamatan Setiabudi Jakarta Selatan 12920 Kota Adm. Jakarta Selatan DKI Jakarta	sugi.sugianto@id.abb.com	021 2551 5555	\N	\N
576	PT. TAWAKAL KARYA TEKNIK	8	3171	GEDUNG DPD AKLI, Jl. KH. Abdullah Syafie'i no 36 H TEBET JAKARTA SELATAN 12840 Kota Adm. Jakarta Selatan DKI Jakarta	tawakalkaryateknik@gmail.com	089653650310	\N	\N
577	PT. ABDHIKA KREASI MANDIRI	8	3171	GEDUNG DPD AKLI, Jl. KH. Abdullah Syafie'i no.37 Kota Adm. Jakarta Selatan DKI Jakarta	abdhikakreasimandiri@gmail.com	087884985463	\N	\N
578	PT. ADHI KARYA (PERSERO) TBK	8	3171	Jl. Pasar Minggu Km. 18 Pasar Minggu Kec. Pasar Minggu Kota Adm. Jakarta Selatan DKI Jakarta	adhinet@adhi.co.id	021-7975312, 7975311	\N	\N
579	PT. Trakindo Utama	8	3171	Gd. TMT 1 Lt.11-17 Suite 1101-1701 Jl. Cilandak KKO No.1 Kelurahan Cilandak Timut Kecamatan Pasar Minggu Jakarta Selatan 12560 Kota Adm. Jakarta Selatan DKI Jakarta	TUGovrel@trakindo.co.id	02129976620	\N	\N
580	PT. TWINK INDONESIA	8	3171	GEDUNG TWINK CENTER LT.7 JL KAPTEN P TENDEAN NO.82 MAMPANG PRAPATAN JAKARTA SELATAN Kota Adm. Jakarta Selatan DKI Jakarta	twinkjkt@twink.co.id	021-7900088	\N	\N
581	PT. CAHAYA PRINASMANDIRI	8	3171	GED. DPD AKLI JL. KH. ABDULLAH SYAFI`IE NO. 36F RT. 001/006 KEL. BUKIT DURI KEC. TEBET JAKARTA SELATAN 12840 Kota Adm. Jakarta Selatan DKI Jakarta	cahayaprinas@gmail.com	021-83795222/081380417250	\N	\N
582	PT. ANDIKA ENERGINDO	8	3171	Jl. LEBAK BULUS TENGAH NO. 5, Cilandak Barat - Cilandak Kota Adm. Jakarta Selatan DKI Jakarta	demangnoval@yahoo.com	021-75901924 / 081334080974	\N	\N
583	PT. LEKTRIKA KARYATAMA	8	3171	Komp. Duta Mas Fatmawati Blok C2/2, Jl. R.S. Fatmawati No.39 Cipete Utara, Kebayoran Baru, Jakarta Selatan 12150 Kota Adm. Jakarta Selatan DKI Jakarta	kontak@lektrika-karyatama.com	021-7244762, 7501915	\N	\N
584	PT. EMSINDO POLA CITRA	8	3171	Gedung Graha Sucofindo Lt.9 Jl. Raya Pasar Minggu Kav.34 Pancoran - Jakarta Selatan Kota Adm. Jakarta Selatan DKI Jakarta	epc@emsindo.co.id	021 7986773/ 0811130719	\N	\N
585	PT SOLUSI DAYA ABADI	8	3171	JL. Raya Kebayoran Lama No 8 A RT 004 RW 10 Kel. Grogol Utara Kec. Kebayoran Lama Kota Adm. Jakarta Selatan DKI Jakarta	prake.kristianto@sdapower.co.id	021 5303747, 021 5303748	\N	\N
586	PT. Bangun Panca Sarana Abadi	8	3171	Wijaya Graha Puri Blok E No. 4 Jalan Darmawangsa Raya Kel. Pulo Kebayoran Baru Jakarta Selatan Kota Adm. Jakarta Selatan DKI Jakarta	sopianbpsa@yahoo.com	021 7251139 / 081296686600	\N	\N
587	PT. Tidak Ada Pembangunan	8	3171	- Kota Adm. Jakarta Selatan DKI Jakarta			\N	\N
588	PT. multi fabrindo gemilang	8	3171	Gedung Medco II, Lt. 3, Jl. Ampera Raya N0. 18-20, Cilandak Timur, Jakarta Selatan 12560 Kota Adm. Jakarta Selatan DKI Jakarta	headoffice@multifab.co.id	0217800824/081282674742	\N	\N
589	PT. SABDA MAJU PERKASA TEKNIK	8	3171	JL. K.H. ABDULLAH SYAFE'I NO. 36 RT. 001 RW. 006 BUKIT DURI KEC. TEBET Kota Adm. Jakarta Selatan DKI Jakarta	pt.sabdamaju.pt@gmail.com	02128541266	\N	\N
590	PT. HEIN GLOBAL UTAMA	8	3171	Menara Jamsostek Lt. 7 Menara Selatan Jl. Jend. Gatot Subroto Kav. 39 Kel. Kuningan Barat Kec. Mampang Prapatan Kota Adm. Jakarta Selatan DKI Jakarta	heinglobalutamapt@gmail.com	021-52964208	\N	\N
591	PT. JS ANUGERAH TEKNIK	8	3171	Gedung DPD AKLI Jl.KH.Abdullah Syafe'i No.37 F RT 001 RW 006 Kel. Bukit Duri Kec. Tebet Kota Adm Jakarta Selatan, DKI Jakarta Kota Adm. Jakarta Selatan DKI Jakarta	js_anugerah@yahoo.co.id	021 83786076, 021 83786076	\N	\N
592	PT. Prima Power Nusantara	8	3171	Jl. Wijaya I No.61, RT 009 RW 005, Petogogan, Kebayoran Baru Kota Adm. Jakarta Selatan DKI Jakarta	info@primponus.co.id	02127515424	\N	\N
593	PT. Grid Solutions Indonesia	8	3171	South Quarter Tower B Lantai 18 & 19 Jl. R.A Kartini Kav. 8 RT. 010 RW. 04 Kelurahan Cilandak Barat, Kecamatan Cilandak Kota Adm. Jakarta Selatan DKI Jakarta	dwi.handayani@alstom.com	021-5730401	\N	\N
594	PT. Metrindo Majupersada	8	3171	Komp. Perkantoran Bintaro 8 Jl. Bintaro Permai Raya No.7 Rt.006/001 Kel. Bintaro Kec. Pesangrahan Kota Adm. Jakarta Selatan DKI Jakarta	metrindomp@yahoo.co.id	02170283777	\N	\N
595	PT. DWIPA KONEKTRA	8	3171	Pusat Niaga Dutamas Blok D1 No. 32, Jl. RS. Fatmawati No. 39, Jakarta Selatan 12150 Kota Adm. Jakarta Selatan DKI Jakarta	dkonektra@dwipakonektra.co.id	021-72797480, 7226638, 7247961	\N	\N
596	PT. Cempaka Jayanindo	8	3171	Gedung DPD AKLI DKI Jakarta, Jl. K.H. Abdullah Syafe'i No. 37 Rt. 001 Rw. 006 Bukit Duri, Tebet - Jakarta Selatan Kota Adm. Jakarta Selatan DKI Jakarta	cempaka-jayanindo@hotmail.com	081210410903	\N	\N
597	PT. DATA ENERGY INFOMEDIA	8	3171	Jl. Lebak Bulus Tengah No. 5 RT/RW : 011/004 Cilandak Barat, Cilandak, Jakarta Selatan, DKI Jakarta Kota Adm. Jakarta Selatan DKI Jakarta	csptdei@yahoo.com	021-75901962, 021-75908327	\N	\N
598	PT. DARMA ADI YASA UTAMA	8	3171	Jl. Ciledug Raya No. 12 C, RT. 002/003, Kel. Ulujami, Kec. Pesanggrahan - Jakarta Selatan Kota Adm. Jakarta Selatan DKI Jakarta	darmaadiyasa@yahoo.com	021-70727482	\N	\N
599	PT. BIRA JAYA MANDIRI PERKASA	8	3171	Gedung Leksika, Jl. Raya Lenteng Agung No.101 Kel. Lenteng Agung Kec. Jagakarsa Kota Adm. Jakarta Selatan DKI Jakarta	bjmperkasa@gmail.com	021-29315537	\N	\N
600	PT. MULTI FABRINDO GEMILANG	8	3171	GD. MEDCO II, JL. AMPERA RAYA NO. 18-20, CILANDAK TIMUR, JAKARTA SELATAN 12560 Kota Adm. Jakarta Selatan DKI Jakarta	headoffice@multifab.co.id	021-7800824	\N	\N
601	PT. CANAS UNGGUL	8	3171	Ged. DPD AKLI Jl. KH. Abdullah Syafi`ie No. 36B Rt. 001/006 Kel Bukit Duri Kec. Tebet Jakarta Selatan 12840 Kota Adm. Jakarta Selatan DKI Jakarta	canasunggul.jakarta@gmail.com	021-83795222/081214343333	\N	\N
602	PT. ELPIRA PUTRA SEJAHTERA	8	3171	Gedung DPD AKLI Jl KH Abdullah Syafe'i No 37 Rt 001 Rw 006 Bukit Duri Kecamatan Tebet Kota Adm. Jakarta Selatan DKI Jakarta	elpiraputra@yahoo.co.id	02183709993	\N	\N
603	PT. EMIK KONSTRINDO	8	3171	Graha Sucofindo Lt. 9, Jl. Raya Pasar Minggu No. 34, Pancoran - Jakarta Selatan Kota Adm. Jakarta Selatan DKI Jakarta	emk@emikonstr.com	021 7986773	\N	\N
604	PT. GRAHA ASA PRADANA	8	3171	GD. PESONA JL. CIPUTAT RAYA NO. 20, KEBAYORAN LAMA UTARA - JAKARTA SELATAN Kota Adm. Jakarta Selatan DKI Jakarta	grahaasa@yahoo.co.id	02122704605	\N	\N
605	PT. ABDI REDJO TEKNIK LESTARI	8	3171	Jl. Bungur Raya No. 2A Lt. 3 Rt. 001/002 Kel. Kebayoran Lama Selatan Kec. Kebayoran Lama Kota Adm. Jakarta Selatan DKI Jakarta	abdiredjo@gmail.com	0217269873	\N	\N
606	PT. Catur Bersaudara Utama	8	3171	Jl. Raya Lenteng Agung No. 22 Kel. Lenteng Agung Kec. Jagakarsa Kota Adm. Jakarta Selatan DKI Jakarta	CaturBU@gmail.com	021-7820439/08115032227	\N	\N
607	CV. SAPTA BUANA JAYA	8	3171	Gedung ILP Centre Lt.3-07 Jl. Raya Pasar Minggu No.39A Kel. Pancoran Kota Adm. Jakarta Selatan DKI Jakarta	sapta_buanajaya@yahoo.com	021-27531488	\N	\N
608	PT. AGGREKO ENERGY SERVICES INDONESIA	8	3171	Kawasan Komersial Cilandak Gedung #208 N3 Jl. Raya Cilandak KKO RT.001 RW.005 Cilandak Timur Kec. Pasar Minggu Kota Adm. Jakarta Selatan DKI Jakarta	Ade.Yusuf@aggreko.biz		\N	\N
609	PT. PRIMORDIA SIPARUP PERKASA	8	3171	Gedung Leksika Lt. 3 Suite 02 Jl. Raya Lenteng Agung No. 101 RT.005 RW.001 Kelurahan Lenteng Agung, Kecamatan Jagakarsa Kota Adm. Jakarta Selatan DKI Jakarta	info@primordia.co.id		\N	\N
610	PT. TIARA RIFKI FIRANTI	8	3171	Ruko Rajawali Center Blok B-II Lt.2 Jl. Rajawali Rt.007/001 Kel. Pasar Minggu Kec. Pasar Minggu Kota Adm. Jakarta Selatan DKI Jakarta	pusat@ptaskomelin.co.id		\N	\N
611	PT. ARMADIAN PERKASA	8	3171	Ruko Rajawali Center Blok B No.11 Lt. 1&2 Jl. Rajawali Rt.007/001 Pasar Minggu Kec. Pasar Minggu Kota Adm. Jakarta Selatan DKI Jakarta	admin@armadianperkasa.com		\N	\N
612	PT. YUDITHA NUGRAHA KARYA	8	3171	JL. PANGLIMA POLIM RAYA NO. 127/D6 KEBAYORAN BARU, JAKARTA SELATAN 12160 Kota Adm. Jakarta Selatan DKI Jakarta	yuditha_n_k@yahoo.com		\N	\N
613	PT. DWI ELSA PRADANA	8	3171	Rukan Pondok Pinang Jl. Ciputat Raya Sektor X Blok BB No.7 Rt. 001/007 Pondok Pinang Kec. Kebayoran Lama Kota Adm. Jakarta Selatan DKI Jakarta	dwi.elsa.pradana@gmail.com		\N	\N
614	PT. INDOKOMAS BUANA PERKASA	8	3171	PLAZA OLEOS LANTAI 7 JL. TB SIMATUPANG NO.53A KEBAGUSAN - PASAR MINGGU Kota Adm. Jakarta Selatan DKI Jakarta	COMMUNICATION.INDOKOMAS@VINCI-ENERGIES.COM		\N	\N
615	PT. MULTI GRAHA INDUSTRI	8	3171	Menara Global Lt.26 Jl. Jend. Gatot Subroto Kav. 27 Kel. Kuningan Timur Kec.Setiabudi Kota Adm. Jakarta Selatan DKI Jakarta	pusat@ptaskomelin.co.id		\N	\N
616	PT. TRIGONOMITRA ANUGRAH	8	3171	Gd.18 Office Park, Tower A Lantai 17, Unit 17 G, Jl. TB.Simatupang No.18 Kota Adm. Jakarta Selatan DKI Jakarta	sales@trigonomitra.com		\N	\N
617	PT. DALLE ENGINEERING CONSTRUCTION	8	3171	Alamanda Tower Lt.26 JL.TB.Simatupang Kav.23-24 Kel.CIlandak Barat Kec.Cilandak Kota Adm. Jakarta Selatan DKI Jakarta	info@dalleenergy.com		\N	\N
618	PT. AGUNG CHRISTAL ABADI	8	3171	Jl. CIputat Raya Sektor X Blok BB No.7 Pondok Pinang, Kebayoran Lama Kota Adm. Jakarta Selatan DKI Jakarta	agung.christal.abadi@gmail.com		\N	\N
619	PT. Inti Karya Persada Tehnik	8	3171	Jl. MT. Haryono Kav.4-5, RT.006 RW.014 Wisma IKPT, Kelurahan Kebon Baru, Kecamatan Tebet Kota Adm. Jakarta Selatan DKI Jakarta	mail@ikpt.com		\N	\N
620	PT. Meiden Engineering Indonesia	8	3171	Gedung Summitmas I Lantai 20, Jl. Jend. Sudirman Kav. 61-62, Jakarta Selatan, 12190 Kota Adm. Jakarta Selatan DKI Jakarta	jennifer@meiden-indonesia.co.id		\N	\N
621	PT. Taraka Dua Indo	8	3171	Jl. Dr. Saharjo No.192 A RT.04 RW.05, Kelurahan Menteng Dalam, Kecamatan Tebet Kota Adm. Jakarta Selatan DKI Jakarta	taraka@gmail.com		\N	\N
622	PT. Ariesto Tunggal Engineering	8	3171	Jl. H. Soleh II/15 Rt.004/Rw.002, Kel. Sukabumi Selatan Kota Adm. Jakarta Selatan DKI Jakarta	aklidpp@gmail.com		\N	\N
623	PT. HEYMAN PRIMA	8	3171	Ruko Grand Bintaro Blok B 5 Jalan Bintaro Permai Raya No. 1, Bintaro Pesanggrahan - Jakarta Selatan Kota Adm. Jakarta Selatan DKI Jakarta	heymanprima@gmail.com		\N	\N
624	PT. HILMAN PRIMA PERKASA	8	3171	Ruko Grand Bintaro Blok B5 Jl. Bintaro Permai Raya No.1, Bintaro, Pesanggrahan, Jakarta Selatan Kota Adm. Jakarta Selatan DKI Jakarta	hilmanprimaperkasa@gmail.com		\N	\N
625	PT. CHRISTAL CIPTA INDAH	8	3171	JL CIPUTAT RAYA SEKTOR X BLOK BB NO.7 RT 01 RW 07 KEL PONDOK PINANG KEC KEBAYORAN LAMA Kota Adm. Jakarta Selatan DKI Jakarta	christal.cipta.indah@gmail.com		\N	\N
626	PT. CIPTA MITRA ANDALAS	8	3172	Jl. Raya Hankam No. 53 RT. 002 RW.003, Kelurahan Bambu Apus, Kec. Cipayung, Jakarta Timur 13890 Kota Adm. Jakarta Timur DKI Jakarta	marketing.cadas@gmail.com	021-84597760	\N	\N
627	PT. ALRISTA TERAMULYA	8	3172	Graha Komando Lantai 3 Jalan Raya Cipinang Indah No.1 RT 001 RW 013 Kel. Cipinang Muara Kota Adm. Jakarta Timur DKI Jakarta	alrista@ymail.com	021-8094481/ 081919817441	\N	\N
628	PT. DIAN GEDANG SEJAHTERA	8	3172	Graha Komando Lantai 3 Jalan Raya Cipinang Indah No.1 RT 001 RW 013 Kel. Cipinang Muara Kec. Jatinegara - Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	pt_diangedangsejahtera@ymail.com	021-808881237 / 081919817441	\N	\N
629	PT. Pembangunan Perumahan (Persero) Tbk	8	3172	Jl TB Simatupang No 57 Pasar Rebo Kota Adm. Jakarta Timur DKI Jakarta	epc@pt-pp.com	0218403902	\N	\N
630	PT. PACITAN SERIBU SATU GOA	8	3172	Pulo Gebang Permai Ruko Blok F No.12A RT.006 RW.012, Kel. Pulo Gebang, Kec. Cakung, Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	pacitan1001goa@yahoo.co.id	0218862176	\N	\N
631	PT. RAMANANDA ABADI	8	3172	Jl.Raya Bekasi KM 18 No.26 RT.008 RW.011Kel.Jatinegara, Kec. Cakung Jakarta Timur, DKI Jakarta Kota Adm. Jakarta Timur DKI Jakarta	ramananda_abadi@yahoo.com	021-4615456, 08129587713	\N	\N
632	PT. PUTRA SITONONG MANDIRI	8	3172	GEDUNG IS PLAZA LANTAI 5 R.504 JL. PRAMUKA RAYA KAV.150, KEL. UTAN KAYU UTARA, KEC. MATRAMAN, JAKARTA TIMUR Kota Adm. Jakarta Timur DKI Jakarta	sitonongboy@yahoo.com	081375247896	\N	\N
633	PT. MITRA TARULI PERKASA	8	3172	Gedung Pembina Graha Lt. 1 Suite 113 Jl. D.I Panjaitan No. 45 RT. 017/006 Kel. Rawa Bunga Kec. Jatinegara Kota Adm. Jakarta Timur, DKI Jakarta Kota Adm. Jakarta Timur DKI Jakarta	mitrataruliperkasa@yahoo.co.id	021-22850389	\N	\N
634	PT. WASKITA KARYA (PERSERO) Tbk	8	3172	Jl. MT. Haryono Kav.10 RT.003 RW.011, Kel. Cipinang Cempedak, Kec. Jatinegara Kota Adm. Jakarta Timur DKI Jakarta	waskita@waskita.co.id	0218508510	\N	\N
635	PT. MARINO BONA COMPANY	8	3172	Jl. Bekasi Timur Raya No. 8 Rt/Rw. 005/001 Kel. jatinegara Kaum Kec. Pulo Gadung Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	ptmarinobona@gmail.com	02122473858	\N	\N
636	PT. WIJAYA KARYA (Persero) Tbk	8	3172	Jl. D.I. Panjaitan Kav.9 Cipinang Cempedak Kec. Jatinegara Kota Adm. Jakarta Timur DKI Jakarta	epc@wika.co.id	021-8192808	\N	\N
637	PT. SIEMENS INDONESIA	8	3172	Jl. Jend A. Yani Kav. 67-68 Kel. Kayu Putih Kec. Pulogadung Kota Adm. Jakarta Timur DKI Jakarta	w.teguh_prihandono@gmail.com	0212-4555100	\N	\N
638	PT. NINDYA KARYA (PERSERO)	8	3172	Jl. Letjend Haryono MT KAV.22 Jakarta Cawang Kec. Kramat Jati Kota Adm. Jakarta Timur DKI Jakarta Kota Adm. Jakarta Timur DKI Jakarta	nindyakarya@nindyakarya.co.id	021-8093276 / 021-8093105	\N	\N
639	PT. META EPSI	8	3172	Jl. DI. Panjaitan Kav.2 Rt.009 Rw.009 Rawa Bunga Kecamatan Jatinegara Kota Adm. Jakarta Timur DKI Jakarta	aris.wiryawan@metaepsi.com	021-8564955	\N	\N
640	PT. TRISI ENERGI INDONESIA	8	3172	Jl. Kayu Putih VII Blok A No.8 Rt/Rw 003/008 Kel. Pulo Gadung Kec. Pulo Gadung Kota Adm. Jakarta Timur, DKI Jakarta Kota Adm. Jakarta Timur DKI Jakarta	teindonesia@yahoo.co.id	021-4892014	\N	\N
641	PT. PT WIRIA INTAN TEKNIK	8	3172	Jl.Utan Kayu Raya No.63, Kel.Utan Kayu Utara, Kec.Matraman, Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	pt.wiriaintanteknik@gmail.com	021 8853894/08129917799	\N	\N
642	PT. EYSSA JANTRA MANDIRI	8	3172	JL. BEKASI TIMUR RAYA NO. 8C RT 05 RW 01 KEL. JATINEGARA KAUM KEC. PULO GADUNG JAKARTA TIMUR Kota Adm. Jakarta Timur DKI Jakarta	eyssajantramandiri@yahoo.com	021-4894287/0817123618	\N	\N
643	PT. FELICIA TARIDA ASI	8	3172	Jl. Raya Bogor KM. 20 No. 10A LT. 1 Rt. 011/010 Kel. Kramat Jati, Kec. Kramat Jati - Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	feliciataridapt@yahoo.co.id	021-8005102	\N	\N
644	PT. BALEKAMBANG PRIMA	8	3172	Jl. Raya Bogor KM. 20 No. 10A Lt. 1 RT.011 / RW. 010 Kramat Jati Kec. Kramat Jati, Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	balekam_pt@yahoo.co.id	021-8090218	\N	\N
645	PT. WASA MITRA ENGINEERING	8	3172	JL RAYA CAKUNG CILINCING KM.1 NO.11 CAKUNG BARAT JAKARTA TIMUR Kota Adm. Jakarta Timur DKI Jakarta	wme@wasamitra.co.id	021-4604958	\N	\N
646	PT. ALDINA KRIDA TAMA	8	3172	Kantor Bersama Lantai I, Jalan Raya Mabes Hankam RT 06 RW 03 No. 67 A Kel. Bambu Apus Kec. Cipayung Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	aldinakridatama@ymail.com	021-87790373	\N	\N
647	PT. TANGGUK JAYA	8	3172	JL. BEKASI TIMUR RAYA NO. 08 RT 05 RW 01 KELURAHAN JATINEGARA KAUM KEC. PULO GADUNG Kota Adm. Jakarta Timur DKI Jakarta	tanggukjaya@yahoo.co.id	021-4756762, 021- 47862003	\N	\N
648	PT. ULUJAMI TEKNIK	8	3172	Gedung Pembina Graha Lt.3 R.310,Jl.DI.Panjaitan No.45 RT.017 RW.006,Kel. Rawa Bunga, Kec. Jatinegara, Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	ulujamiteknikpt@gmail.com	021-8096988	\N	\N
649	PT. AGUNG MAKARSA INDAH	8	3172	JL. MASJID ABIDIN NO. 40A KEL. PONDOK BAMBU KEC. DUREN SAWIT Kota Adm. Jakarta Timur DKI Jakarta	agung_m_i@yahoo.com	8608079	\N	\N
650	PT. Kertabumi Teknindo	8	3172	Perkantoran Mitra Matraman Blok A 1 No. 7 Jl. Matraman Raya No. 148 Kel. Kebon Manggis Kec. Matraman Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	kbt@kertabumi.com	02185918024	\N	\N
651	PT. HUTAMA KARYA (PERSERO)	8	3172	Jl. Letjend. MT. Haryono Kav.8 RT012 RW011 Cawang Cipinang Cempedak Kec. Jatinegara Kota Adm. Jakarta Timur, DKI Jakarta Kota Adm. Jakarta Timur DKI Jakarta	hkepc@hutamakarya.com	021 8563568	\N	\N
652	PT. LINARD POWER KONTRAKTOR	8	3172	Jl. Madu Indah No. 38 RT 002, RW 003, Kel. Bambu Apus, Kec. Cipayung Kota Adm. Jakarta Timur DKI Jakarta	linard.power@yahoo.com	0811837890	\N	\N
653	PT. AYAMA CAHAYA MANDIRI	8	3172	Patria Park Office & Apartment RK. 10 Jl. D.I. Panjaitan Kav. 5-7 RT. 003/011 Kel. Cipinang Cempedak Kec. Jatinegara Kota Adm. Jakarta Timur, DKI Jakarta Kota Adm. Jakarta Timur DKI Jakarta	ayama@ayama-group.com	021-8508529	\N	\N
654	PT. JASA PRATAMA KONSTRUKSI	8	3172	Jl. Jatinegara Timur IV No.9B Rt.010/002 Kel. Bali Mester Kec. Jatinegara Kota Adm. Jakarta Timur DKI Jakarta	sekretariat.akklindo@gmail.com	021-8514141	\N	\N
655	PT. LASKAR CIPTAUTAMA	8	3172	Jl. Perintis Kemerdekaan No. 37 Rt. 009 / 002 Kel. Pulo Gadung Kec. Pulo Gadung Jakarta Timur 13260 Kota Adm. Jakarta Timur DKI Jakarta	marketing@laskar-corp.com	08129096511	\N	\N
656	PT. BRANTAS ABIPRAYA	8	3172	Jl. D.I. Panjaitan Kav. 14 Cawang Kel. Cipinang Cempedak, Kec. Jatinegara, Jakarta Timur, DKI Jakarta. Kota Adm. Jakarta Timur DKI Jakarta	bap@brantas-abipraya.com	021-8516290	\N	\N
657	PT. BERKAH PUTRO LAKSONO	8	3172	JL. CIPINANG INDAH RAYA NO. 1 RT. 001 RW. 013 KEL. CIPINANG MUARA KEC. JATINEGARA Kota Adm. Jakarta Timur DKI Jakarta	berkahputralaksonobpl@ymail.com	0818888481/ 021-21381925	\N	\N
658	PT. DAYA USAHA MANDIRI	8	3172	Jl. Masjid Al Wustho No. 10 B RT. 009 RW. 007, Kel. Pondok Bambu, Kec. Duren Sawit Kota Adm. Jakarta Timur DKI Jakarta	admin@dayausaha.com		\N	\N
659	PT. AXENA PUTRA HARAPAN	8	3172	Jl. Terusan I Gusti Ngurah Rai No. 28 RT 004 RW 011 Kel. Pondok Kopi Kec. Duren Sawit Kota Adm. Jakarta Timur DKI Jakarta	ptaxenaputraharapan16@gmail.com		\N	\N
660	PT. GRAHA BARA LESTAR	8	3172	Jl. Basuki Rachmat No.28 Rt.005/009 Rawa Bunga Kec. Jatinegara Kota Adm. Jakarta Timur DKI Jakarta	gbl2000@rad.net.id		\N	\N
661	PT. BENGKALIS ERA JAYA	8	3172	Jl. Raya Pondk Gede No.5 Blok A Rt.005/002 Pinang Ranti Kec. Makasar Kota Adm. Jakarta Timur DKI Jakarta	bengkalisej@gmail.com		\N	\N
662	PT. RIZKI MEGAH ABADI	8	3172	JL. MADU INDAH NO. 72 RT. 002 RW. 03 KEL. BAMBU APUS CIPAYUNG Kota Adm. Jakarta Timur DKI Jakarta	dessyliana86@yahoo.com		\N	\N
663	PT. Sumber Terang Cemerlang	8	3172	Jl. Cipinang Indah Raya No. 1 RT. 001 RW. 013, Kel. Cipinang Muara, Kec. Jatinegara, Jakarta Timur Kota Adm. Jakarta Timur DKI Jakarta	sumberterangc@yahoo.com		\N	\N
664	PT. KEDUNGJAYA REKADAYATAMA	8	3172	RUKO URBANA, JL.RAYA DUREN SAWIT K.IV NO.7E Lt.3, Kel.KLENDER, Kec.DURENSAWIT JAKARTA TIMUR Kota Adm. Jakarta Timur DKI Jakarta	kedungjaya@yahoo.com		\N	\N
665	PT. BINA PERTIWI	8	3172	JL. RAYA BEKASI KM22 RT 001 RW 001 KEL. CAKUNG BARAT KEC. CAKUNG KOTA JAKARTA TIMUR, DKI JAKARTA Kota Adm. Jakarta Timur DKI Jakarta	zikri.nibras@binapertiwi.co.id		\N	\N
666	PT. SANTOSA ASIH JAYA	8	3172	Jl. Condet Raya No. 17 Kel. Balekambang Kec. Kramatjati Kota Adm. Jakarta Timur DKI Jakarta	pusat@ptaskomelin.co.id		\N	\N
667	PT. HENRUKY SEJAHTERA	8	3172	Jl. Asem Gede No. 23 RT. 008 RW. 004 Kel. Utan Kayu Selatan Kec. Matraman Kota Adm. Jakarta Timur DKI Jakarta	pusat@ptaskomelin.co.id		\N	\N
668	PT. CITACONTRAC	8	3172	Jl. Pinang Ranti No. 5 RT 015/01, Pinang Ranti, Makassar Kota Adm. Jakarta Timur DKI Jakarta	citacontrac@citacontrac.co.id		\N	\N
669	PT. ARDITA PUTRA BAKTI	8	3172	JL. BUDI VI NO. 42-E RT OO7/011 CIPINANG MELAYU - JAKTIM 13620 Kota Adm. Jakarta Timur DKI Jakarta	arditabakti@gmail.com		\N	\N
670	PT. MEGA POWER TEKNINDO	8	3172	JL. PRAMUKA RAYA NO. 19A Kota Adm. Jakarta Timur DKI Jakarta	MPOWERT@YAHOO.COM		\N	\N
671	PT. HAFIZ ENERGI TAMA	8	3172	Jl. Cipinang Indah Raya No. 01 Rt. 01/13 Cipinang Muara, Jatinegara Kota Adm. Jakarta Timur DKI Jakarta	hafizenergitama@gmail.com		\N	\N
672	PT. KM MANDIRI	8	3175	Jl. Raya Tugu No. 11A RT. 003 RW. 014 Kel. Tugu Utara Kec. Koja Kota Adm. Jakarta Utara, DKI Jakarta Kota Adm. Jakarta Utara DKI Jakarta	samsu_cuit@yahoo.com	021-4410020	\N	\N
673	PT. Mirlindo Padu Kencana	8	3175	Jl. Agung Timur 9 Blok N3 No. 1B Sunter Jaya, Jakarta Utara Kota Adm. Jakarta Utara DKI Jakarta	adiepkencana@ymail.com	0216512018, 6512072	\N	\N
674	PT. PRABA INDOPERSADA	8	3175	Jl. Danau Sunter Utara Blok G7 No. 15 Rt 014/013, Sunter Agung, Kec. Tanjung Priok Kota Adm. Jakarta Utara DKI Jakarta	general@praba.co.id	0216403818	\N	\N
675	PT. FOKUS INDO LIGHTING	8	3175	JL. PEGANGSAAN DUA KM 2 NO. 64 KELAPA GADING JAKARTA UTARA Kota Adm. Jakarta Utara DKI Jakarta	fokus.indo.lighting@gmail.com	021 - 4612110	\N	\N
676	PT. MAXI UTAMA ENERGY	8	3175	JL. PEGANGSAAN DUA BLOK RG 10 NO. 18B RT.005, RW. 024, KEL. PEGANGSAAN DUA, KEC. KELAPA GADING, JAKARTA UTARA Kota Adm. Jakarta Utara DKI Jakarta	corporate@maxideutz.com	021 45872239	\N	\N
677	PT. GUNA SARANA TEKNIK	8	3175	RUKAN ARTHA GADING NIAGA BLOK C NO. 29 JL. BOULEVARD ARTHA GADING JAKARTA UTARA Kota Adm. Jakarta Utara DKI Jakarta	kantor.gst@gmail.com	021 - 45876199	\N	\N
678	PT. Deka Buana Swastamas	8	3175	Jl. Toar II Blok B IV No. 42 RT 012 RW 002 Kel. Tugu Utara Kec. Koja Kota Adm. Jakarta Utara DKI Jakarta	dekabuanaswastamas@ymail.com		\N	\N
679	PT. Wisma Sarana Teknik	8	3175	Artha Gading Niaga Blok H No. 24 - 25 Jl. Boulevard Artha Gading ,Kelapa Gading Kota Adm. Jakarta Utara DKI Jakarta	tnd@wisma.com		\N	\N
680	PT. BINA PUTRA KUSYADI	9	3201	KP. LEBAK NANGKA RT.005/RW.002 NO. 45 BABAKAN MADANG KABUPATEN BOGOR Kab. Bogor Jawa Barat	pt.binaputrakusyadi@gmail.com	08111109997	\N	\N
681	PT. HANIF POWER PRATAMA	9	3201	Nirwana Estate Blok HH No. 12 RT. 008 RW. 013 Kel. Pakansari Kec. Cibinong Kab. Bogor Jawa Barat	pt.hanif.pratama@gmail.com	021-87906122	\N	\N
682	PT. FAJAR BUANA TEKNIK	9	3201	KH. ABDUL HAMID KM 1 NO 9 SUKAMAJU CIBUNGBULANG KAB. BOGOR 16630 Kab. Bogor Jawa Barat	fajarbuana_teknik@yahoo.co.id	082113915999	\N	\N
683	PT. WIDYA TATA PRATAMA	9	3201	Plaza Niaga I Blok D No. 07 RT. 03 RW. 05 Desa Citaringgul , Kecamatan Babakan Madang , Kabupaten Bogor, Jawa Barat Kab. Bogor Jawa Barat	ivon.widiarti@yahoo.com	02187961553	\N	\N
684	PT. ABADI MUKTIGUNA LESTARI	9	3201	PARUNG BANTENG N0 19 10/11 KATULAMPA , BOGOR TIMUR Kab. Bogor Jawa Barat	abadi.80@yahoo.com	0251 8379922	\N	\N
685	PT. ADIYANA TEKNIK MANDIRI	9	3201	PARUNG BANTENG 135 01/11 KATULAMPAK , BOGOR TIMUR Kab. Bogor Jawa Barat	adiyanateknik@yahoo.com	0251 329623	\N	\N
686	PT. Trijaya Teknika Mandiri	9	3201	Jl. Raya Jakarta Bogor KM 47,3 RT 05 RW 02 Nanggewer Mekar Kec. Cibinong Kab. Bogor JawaBarat Kab. Bogor Jawa Barat	trijayateknika@gmail.com	021-8762589 / 021-87903051	\N	\N
687	PT. WIJAYA KARYA TEKNIKA	9	3201	Jalan Suryakencana 236 RT. 01 RW. 04 Kel. Gudang Kec. Bogor Tengah Kab. Bogor Jawa Barat	btmaswi@wijayakarya.co.id	0251-8322697	\N	\N
688	PT. INDRA PUTRA BERLIAN	9	3201	Bulak Rata Rt.004/007 Pondok Rajeg Kec. Cibinong Kab. Bogor Jawa Barat	indraputraberlian@yahoo.com	021-87913549	\N	\N
689	PT. KLAPANUNGGAL PERMATA ENERGI	9	3201	Perum Coco Garden Blok K19/9 Rt.003 Rw. 014. DesaKlapanunggal, Kec. Klapanunggal. Bogor Jawa Barat Kab. Bogor Jawa Barat	ptpeklapanunggal@gmail.com	02122103871	\N	\N
690	PT. KARYA JONGGOL ENERGI	9	3201	Kp. Nyangegeng Rt.001/001 Singajaya Kec. Jonggol Kab. Bogor Jawa Barat	pt.karya.jonggol.energi@gmail.com	021-89930324	\N	\N
691	PT. HUMA JAYA TEKNIK	9	3201	Perum Villa Permata Mas Blok EB N0. 7 Rt.006 Rw.002 Kel. Karanggan Kec. Gunung Putri Kab. Bogor - Jawa Barat Kab. Bogor Jawa Barat	humajayateknik@gmail.com	021-86864482 / 089660075356	\N	\N
692	PT. DESBASS MANDIRI UTAMA	9	3201	Jl. Raya Cileungsi - Jonggol KM 1 No. 56, Cileungsi Kab. Bogor Jawa Barat	dessbassmandiriutama@gmail.com	021-8234025	\N	\N
693	PT. ANUGRAH PUTRA MANDIRI	9	3201	Kp. Cicadas RT.003/005 Cicadas Kab. Bogor Jawa Barat	apmandiri_2@yahoo.com	021-86860966	\N	\N
694	PT. PUTRA KUSUMA ELEKTRIK	9	3201	KP. SAWAH RT. 003 RW. 003, DESA CILEUNGSI KIDUL, KECAMATAN CILEUNGSI Kab. Bogor Jawa Barat	pt.putrakusumaelektrik@yahoo.com		\N	\N
695	PT. BERKAH TEKNIK ENERGI	9	3201	LALADON INDAH JL SIGURA GURA NO 1 CIOMAS - BOGOR KEL LALADON KEC CIOMAS KAB BOGOR JAWA BARAT Kab. Bogor Jawa Barat	pt.berkahteknikenergi@gmail.com		\N	\N
696	PT. GRAND KHAYA LESTARI	9	3201	Griya Kenari Mas E.2 No.32 Rt.005/010 Cileungsi Kidul Kec. Cileungsi Kab. Bogor Jawa Barat	ptgkllistrik@yahoo.com		\N	\N
697	PT. DUTA TERANG RUBBERINDO	9	3201	Jl. Raya Bogor Km.50 Rt.01/04 Kel. Cimandala Kec. Sukaraja Kab. Bogor Jawa Barat	dtrindonesia@yahoo.co.id		\N	\N
698	PT. CITRA LESTARI ELEKTRINDO	9	3201	KP. SAWAH NO. 22 RT. 003 / RW. 003, DESA CILEUNGSI KIDUL, KEC. CILEUNGSI, KAB BOGOR Kab. Bogor Jawa Barat	pt.citralestarielektrindo@yahoo.com		\N	\N
699	PT. MOLKISS SAKTI	9	3201	PERUM.METLAND CLUSTER EBONI NO.3 RT006/003 KEL.CILEUNGSI KIDUL KEC.CILEUNGSI Kab. Bogor Jawa Barat	molkiss.sakti@gmail.com		\N	\N
700	PT. AGUNG ELEKTRO BUANA	9	3201	Jl. H. Nawawi No. 17 Rt. 01/ 02 Cirimekar, Kec. Cibinong, Kab. Bogor. Kab. Bogor Jawa Barat	agungelectrobuana@yahoo.com		\N	\N
701	PT. PANCAR TEKNIK UTAMA MANDIRI	9	3201	Jl. Johar 2 blok C3 no.31 Kel.Padasuka Kec.Ciomas Kab. Bogor Jawa Barat	pt_ptum@yahoo.com		\N	\N
702	PT. MAJU JAYA SEJAHTERA BERSAMA	9	3201	Kp. Cukanggaleuh 1 Rt.003/003 Jambuluwuk Kec. Ciawi Kab. Bogor Jawa Barat	ptmjsb@yahoo.com		\N	\N
703	PT. JASA TEKHNIK MULYA	9	3201	Tmn Pagelaran Blok CC.5 No.9 Rt.003/010 Padasuka Kec. Ciomas Kab. Bogor Jawa Barat	sraflindo@yahoo.com		\N	\N
704	PT. SINAR RAFLINDO JAYA	9	3201	Taman Pagelaran Blok CC.5 No. 9 RT. 003/010 Padasuka Kec. Ciomas Kab. Bogor Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
705	PT. HADERA UTAMA TEKNIK	9	3201	Jl. Pulo Empang Gg. Bahagia No. 135 Paledang Kec. Bogor Tengah Kota Bogor, Jawa Barat Kab. Bogor Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
706	PT. STRATEGI INOVASI TEKNOLOGI INDONESIA	9	3201	Ruko Glory Victoria Blok A9/19 Griya Bukit Jaya Desa Tlajung Udik Kec. Gunung Putri Kab. Bogor Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
707	PT. WIRA SUDARMA PERSADA	9	3202	Perum Gentong Mas Rt.001/009 Limbangan Kec. Sukaraja Kab. Sukabumi Jawa Barat	wirasudarmapersada@gmail.com	08154633385	\N	\N
708	PT. TUNAS SAMUDRA ENERGI	9	3202	PERUM CIMAHPAR II JL. KETILANG NO. 4C PASIRHALANG SUKARAJA KABUPATEN SUKABUMI Kab. Sukabumi Jawa Barat	tse.indonesia@gmail.com	-	\N	\N
709	PT. ANNISA TEKNIK	9	3202	Kp.Asrama Cibeureum Rt.01 Rw.12 Desa Pasirhalang Kecamatan Sukaraja kab.Sukabumi Kab. Sukabumi Jawa Barat	info@annisa-group.com	-	\N	\N
710	PT. BUMI BANGKIT BERSAMA	9	3202	Kp. Mangkalaya Rt.005/005 Ds. Cibolang Kec. Gunungguruh Kab. Sukabumi Jawa Barat	bumibangkitbersama@gmail.com	-	\N	\N
711	PT. MENSIELLS JAYA LESTARI	9	3202	Kp. Kebon Manggu Pasirhalang Kec. Sukaraja Kab. Sukabumi Jawa Barat	mensiellsjayalestari@yahoo.com	-	\N	\N
712	PT. RISAN ANUGRAH PRATAMA	9	3203	Jl. RH Apandi RT. 01/05 Kel. Sayang Kec. Cianjur Kab. Cianjur Jawa Barat	risananugrah.rd@gmail.com	085720007751	\N	\N
713	PT. KHANSA LINGGA KARYA	9	3203	Jl. Nyi Raden Sitibodedar Mesjid Agung 131 RT.02/02 Kab. Cianjur Jawa Barat	khansalinggakarya@yahoo.com	082120220432	\N	\N
714	PT. SANJUR TRIDA RAYA	9	3203	JL. SITI JENAB NO. 47 KEL. PAMOYANAN KEC. CIANJUR Kab. Cianjur Jawa Barat	sanjurtridaraya@yahoo.co.id	(0263)271071 / (0263)263733 / 08122493780	\N	\N
715	PT. AMIKA JAYA SENTOSA	9	3203	JL. DIDI PRAWIRAKUSUMAH NO. 125. DES. MALEBER, KEC. KARANGTENGAH, KAB. CIANJUR JAWA BARAT Kab. Cianjur Jawa Barat	kayas.pt@gmail.com	(0263)290284 / 0818121390	\N	\N
716	PT. KEYRA AFKAR FIRRAS	9	3203	JLN. CILAKU NO. 19 KELURAHAN CIKAROYA, KEC. WARUNG KONDANG - CIANJUR Kab. Cianjur Jawa Barat	kaf_pt@yahoo.com	0263283613 / 08170123356	\N	\N
717	PT. ARDIS SURYA GEMILANG	9	3203	Gunteng Regency Blok F No. 6 Cianjur. Ds Bojong Kec Karang Tengah Kab. Cianjur Jawa Barat	ardis_suryagemilang.pt@yahoo.com	0263267893/081316343008	\N	\N
718	PT. ADRIAN ELEKTRIK CIANJUR	9	3203	Jl. Didi Prawirakusumah Kp. Joglo No. 54A Kel.Sabandar Kec. Karang Tengah Kab. Cianjur , Jawa Barat Kab. Cianjur Jawa Barat	adrian_elektrikcianjur@yahoo.com	081322809000	\N	\N
719	PT. DAHANA JAWARA KARYA	9	3203	JL. SITI JENAB NO. 77 KELURAHAN PAMOYANAN KECAMATAN CIANJUR Kab. Cianjur Jawa Barat	dahanajawarakarya@yahoo.com		\N	\N
720	PT. NURINDAH WANGI LESTARI	9	3203	Jl. Cilaku Warungkondang Km.1.9 Rt.01/04 Desa Cikaroya Kec. Warungkondang Kab. Cianjur Jawa Barat	pt.nurindahwl@gmail.com		\N	\N
721	PT. SYALDI PERKASA DJAYA	9	3203	Jl. Raya Cibeber Sadamaya Km.10 Kec. Cibeber Kab. Cianjur Jawa Barat	pt.syaldiperkasadjaya@yahoo.co.id		\N	\N
722	PT. SUGIH FAMILI PUTRA	9	3203	BTN Pasir Sembung Blok D.98 Rt. 006/011 Ds. Sirnagalih Kec. Cilaku Kab. Cianjur Jawa Barat	sugihfamili@yahoo.com		\N	\N
723	PT. MANDIRI CITRA BERSATU	9	3203	Kp.Cipeuyeum Rt.03/07 Ds.Kertamukti Kec.Haurwangi Kab. Cianjur Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
724	PT. METAMORFOSA SEJAHTERA ABADI	9	3203	Jl. Siti Jenap No. 20 Rt. 002/003 Kel. Pamoyanan Kec. Cianjur Kab. Cianjur Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
725	PT. RISAN KENCANA GEMILANG	9	3203	Jl. RH. Apandi Rt.01/05 Kel.Sayang Kec. Cianjur Kab. Cianjur Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
726	PT. JARIKA NATA PRATAMA UTAMA	9	3204	MARGAHAYU KENCANA G5 NO. 6 RT. 07 RW. 13 DESA MARGAHAYU SELATAN KEC. MARGAHAYU Kab. Bandung Jawa Barat	jarika.npu@gmail.com	081223376125	\N	\N
727	PT. NAJWAN JAYA MANDIRI	9	3204	Kp. Sukamanah Rt 003 Rw 004 Sukamanah Kec. Paseh Kab. Bandung Jawa Barat Kab. Bandung Jawa Barat	najwanjayamandiri@gmail.com	022-5955725	\N	\N
728	PT. DWI PUTRA ANDALAN	9	3204	JL. MELUR III NO 27 RT 10 RW 15, KELURAHAN RANCAEKEK KENCANA, KABUPATEN BANDUNG Kab. Bandung Jawa Barat	dwi.putra327@yahoo.co.id	0227794377/08122379295	\N	\N
729	PT. JIANT ADIKA ENGINEERING	9	3204	JL. TERATAI RAYA NO 219, KELURAHAN RANCAEKEK KENCANA, KECAMATAN RANCAEKEK Kab. Bandung Jawa Barat	jiantadika.engineering@yahoo.co.id	0227794377/081312110693	\N	\N
730	PT. RHACINDO ADI PERSADA	9	3204	JL. ADIPATI UKUR NO. 45 RT. 04/11 KEL/KEC. BALEENDAH Kab. Bandung Jawa Barat	rhacindo@yahoo.co.id	022 5947463	\N	\N
731	PT. PINUS ELEKTRIKA ENGINEERING	9	3204	JALAN CEMPAKA VI No. 2 RT.008 RW 006 Kel.Rancaekek Kec. Rancaekek Kencana Kab. Bandung Jawa Barat	pinusengineering@yahoo.co.id	022-7796613/081220015577	\N	\N
732	PT. Satria manggala	9	3204	Jl.Ciganitri no.13 Rt.07 RW.05 kel.Cipagalo Kec.Bojongsoang Kab. Bandung Jawa Barat	ptsatriamanggala@yahoo.co.id	081320415345.- 08226262334	\N	\N
733	PT. LATIIF PERDANA JAYA	9	3204	KOMP.GRAHA SARI ENDAH JL.ANGGUR II NO.1 RT.01 RW.26 KEL.BALEENDAH KEC.BALEENDAH KAB.BANDUNG JAWA BARAT Kab. Bandung Jawa Barat	latiifperdanajaya@gmail.com	022 5211509	\N	\N
734	PT. Karya Jaya Enterprindo Utama	9	3204	Jl. Raa Wiranata Kusumah No.04 Baleendah Kab. Bandung Kab. Bandung Jawa Barat	sonara_amd@yahoo.com	022-87799459	\N	\N
735	PT. MANDENGGAL JAYA SENTOSA	9	3204	KK. SANGGAR MAS LESTARI BLOK H NO. 69 RT. 04 RW. 12 DESA TARAJUSARI KEC. BANJARAN KAB. BANDUNG Kab. Bandung Jawa Barat	mandenggaljayasentosa@gmail.com	08236596317/081394266171	\N	\N
736	PT. RESTHU KENCANA MANDIRI	9	3204	KP. KEBON JERUK RT 2 RW 9 DESA SUKAMANTR KECAMATAN PASEH Kab. Bandung Jawa Barat	resthukencana@yahoo.com	081221322924	\N	\N
737	PT. CALIFPENA SINAMBUNG DAYA	9	3204	Komp. GBA 1 Blok H-1 RT.005 RW.015 Desa Bojongosang, Kecamatan Bojongsoang, Kabupaten Bandung, Jawa Barat Kab. Bandung Jawa Barat	project@califpena.com	081320708773	\N	\N
738	PT. CAHAYA PERDANA UTAMA	9	3204	Jl. KH. A Syadili Cluster Panorama RT. 002 RW. 021 Jelegong Rancaekek Kab. Bandung 40396 Kab. Bandung Jawa Barat	cp.utama15@gmail.com		\N	\N
739	PT. MEGARINDO POWER	9	3204	Komp. GBA II Blok M1 No.11 Rt.005/008 Desa Cipagalo Kec. Bojongsoang Kab. Bandung Jawa Barat	gba2m111@gmail.com		\N	\N
740	PT. ASRAPINDO MEGA UTAMA	9	3204	KOMP. BOJONG MALAKA INDAH BLOCK I.2 BALEENDAH KAB.BANDUNG Kab. Bandung Jawa Barat	sonara_amd@yahoo.com		\N	\N
741	PT. CAHAYA BUANA ELECTRICAL	9	3204	KK. BARUJATI BLOK III NO. 6 RT.03/16 DESA PAKUTANDANG KEC. CIPARAY Kab. Bandung Jawa Barat	cahayabuanaelectrical@gmail.com		\N	\N
742	PT. MEGA WATT PUTRA PERDANA	9	3204	JL. Paseh Bojongsari Rt. 002 Rw. 009 Kel. Tugujaya Kec. Cihideung Kota Tasikmalaya Kab. Bandung Jawa Barat	megawattputra@gmail.com		\N	\N
743	PT. PUTRA NAJWAN MANDIRI	9	3204	Kp. Gunung Leutik Rt. 02 Rw. 05 Ds. Gn Leutik Ciparay, Desa Sukamanah Kecamatan Paseh Kabupaten Bandung Kab. Bandung Jawa Barat	azhimmuntazharn@yahoo.com		\N	\N
744	PT. ANDHIKA KARYA PERKASA	9	3204	Jl. Raya Laswi No. 606 Kel. Pakutandang Kec. Ciparay, Kab. Bandung Kab. Bandung Jawa Barat	andhikarya@gmail.com		\N	\N
745	CV. GAMBA UTAMA	9	3204	Perum Tamansari Indah C9 Rt 003 Rw 011 Kelurahan Karsamenak Kec Kawalu Kota Tasikmalaya Jawa Barat Kab. Bandung Jawa Barat	gamba_utama@yahoo.com		\N	\N
746	PT. RIZKY ANUGRAH PERKASA	9	3204	Perumahan Gading Tutuka II Jl. Mutiara Blok P5 No. 39 Kel. Ciluncat Kec. Cangkuang Kab. Bandung Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
747	CV. JEMBAR LAKSANA	9	3204	Jl. Sekeloa No. 12 RT. 05 RW. 09 Margahayu Selatan Kab. Bandung Jawa Barat	cvjembarlaksana@gmail.com		\N	\N
748	PT. ELEMENTARIKA NUSANTARA	9	3204	Jl. Budi Asih Gg. Erin No. 3 Bnadung Cegerkalong Kec. Sukasari Kab. Bandung Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
749	PT. NOVITA ELECTRIK MULTI ENERGI	9	3205	Kp. Nagrog Rt.003/002 Ds. Rancabango Kec. Tarogong Kaler Kab. Garut Jawa Barat	cv.novitaelectrik@yahoo.co.id		\N	\N
750	PT. AHMAD NASHIR REKONSTRUKSI	9	3205	Jln. Jend. Sudirman No. 02 Ds. Suci Karangpawitan Garut 44182 Kab. Garut Jawa Barat	nashir.anr@gmail.com	02622247708	\N	\N
751	PT. DSTEKNIK PUTRA UTAMA	9	3205	Kp.Panggilingan Rt.03/007 Ds.Margalaksana Kec.Cilawu Kab. Garut Jawa Barat	dsteknik.service@gmail.com	085220776013	\N	\N
752	PT. SABION MULTI KARYA	9	3205	Jl. Pembangunan No. 94 RT..002 RW. 05 Kelurahan Sukajaya Kec. Tarogong Kidul Kab. Garut Jawa Barat	sabionmultikarya@gmail.com		\N	\N
753	PT. KARYA DAM RAYA	9	3205	Komp. Pepabri Blok E No.30 Rt.03/03 Desa Langensari Kec. Tarogong Kaler Kab. Garut Jawa Barat	karya.damraya@yahoo.co.id		\N	\N
754	PT. JASA ASKOGAR INDO	9	3205	Komp. Intan Regency Blok M.14 Rt.03/09 Desa Tarogong Kec. Tarogong Kidul Kab. Garut Jawa Barat	jasa.askogarindo@yahoo.co.id		\N	\N
755	PT. DELIMA TEKNIK INDO	9	3205	Kp. Umbul Tengah Rt.04/10 Kel. Jayawaras Kec. Tarogong Kidu; Kab. Garut Jawa Barat	delima.teknikindo@yahoo.com		\N	\N
756	PT. BATU KRAMAT SAPUTRA	9	3205	Kp. Kaum Kaler Rt.03/02 Desa Pameungpeuk Kec. Pameungpeuk Kab. Garut Jawa Barat	batukramatsaputra@gmail.com		\N	\N
757	PT. DEWI KAMILA KENCANA	9	3205	JL. CIMANUK 326B, RT 002/005, PAMINGGIR, GARUT KOTA, GARUT 44151, JAWA BARAT Kab. Garut Jawa Barat	pt.dewikamilakencana@yahoo.co.id		\N	\N
758	PT. DANICO BANGUN PERDANA	9	3205	JL CIMANUK NO.332 PAMINGGIR KEC GARUT KOTA Kab. Garut Jawa Barat	danicobangunperdana@yahoo.co.id		\N	\N
759	PT. BAHIJ TECHNICAL ENERGY	9	3205	Komp. Margamulya B-607 Rt.02/05 Kel. Pataruman Kec. Tarogong Kidul Kab. Garut Jawa Barat	bahijtechnicalenergy@gmail.com		\N	\N
760	PT. REZA PRIMA SENTOSA	9	3205	Kp. Cibunar Girang Rt.22/05 Ds. Cibunar Kec. Tarogong Kidul Kab. Garut Jawa Barat	rezaprimasentosa@gmail.com		\N	\N
761	PT. BANGKIT PIJAR GRAHA	9	3207	Jl. Wanayasa RT. 09 RW. 02 Cibadak Banjarsari kab. Ciamis Kab. Ciamis Jawa Bara	bangkitpijargraha@yahoo.com	081320479419	\N	\N
762	PT. LENTERA BUANA LISTRIK	9	3207	PERUM GRAHA PERSADA BLOK G NO 44, SINDANGKASIH, CIMIS Kab. Ciamis Jawa Barat	lenterabuana10@gmail.com	085322288681	\N	\N
763	PT. KARYA TRI BERSAUDARA	9	3207	Perum Green Garden Estate B 35 RT. 001 RW. 020 Kelurahan Imbanagara Kecamatan Ciamis Kab. Ciamis Jawa Barat	p_bersaudara@yahoo.com		\N	\N
764	PT. Damas Putra Maulana	9	3207	Jl. KH. Ahmad Dahlan No.91 RT.005 RW.024 Kelurahan Ciamis, Kecamatan Ciamis, Kabupaten Ciamis Jawa Barat Kab. Ciamis Jawa Barat	damas_putra@yahoo.com		\N	\N
765	PT. SURYA KUMALA ELEKTRINDO	9	3207	Dusun Tengah RT.001/003, Kelurahan CIhaurbeuti , Kecamatan CIhaurbeuti Kabupaten Ciamis, Jawa Barat Kab. Ciamis Jawa Barat	pt.suryakumalaelektrindo@yahoo.com		\N	\N
766	PT. CULA MEGA ALBAROKAH	9	3207	JL. Jendral Sudirman Lingkungan Yudanegara RT. 003/001 Ciamis Kel. Sindangrasa Kec. Ciamis Kab. Ciamis Jawa Barat	culamegaalbarokah@gmail.com		\N	\N
767	PT. CAHAYA TASIK MANDIRI	9	3207	Perum Graha Persada Blok C No. 33 RT. 053 RW. 016 Sindangkasih Ciamis Kel. Sindangkasih Kec. Sindangkadih Kab. Ciamis Jawa Barat	cahayatasikmandiri@gmail.com		\N	\N
768	PT. RAHARJA USAHA ABADI	9	3207	Perum Taman Jati Indah Blok D 25 RT.03 RW. 09 Panyingkiran Ciamis Kel. Panyingkiran Kec. Ciamis Jawa Barat Kab. Ciamis Jawa Barat	raharjausahaabadi@gmail.com		\N	\N
769	PT. YUNIAR SATYA WIGUNA	9	3208	Jl. Jendral Sudirman Lingk Ciweri No, 171 Rt. 004 Rw. 006 Awirangan Kel. Awirangan Kec. Kuningan Kab. Kuningan Kab. Kuningan Jawa Barat	dadangmahyudi@gmail.com	081214143472	\N	\N
770	PT. MAKMUR PINUNJUL	9	3208	Dusun Kliwon Rt.10/05 Desa Pajambon Kec. Kramatmulya Kab. Kuningan Jawa Barat	pt.makmurpinunjul@yahoo.co.id	-	\N	\N
771	PT. ABIMANYU ANDIJAYA PRATAMA	9	3208	Jl. KH. Idris No.93 Rt.019/005 Ds. Cilimus Kec. Cilimus Kab. Kuningan Jawa Barat	andijaya1972@gmail.com	-	\N	\N
772	PT. ADITYA SENTOSA	9	3209	Jl Jendral Sudirman lemah abang Indramayu Kab. Cirebon Jawa Barat	pt_adityasentosa@yahoo.co.id	081909602056	\N	\N
773	PT. FANNY PUTRI ELEKTRIK	9	3209	DUSUN I RT. 004/002 DS. KEPONGPONPONGAN KEC. TALUN KAB. CIREBON JAWA BARAT Kab. Cirebon Jawa Barat	fannyputrielektrik.pt@gmail.com	081224357333	\N	\N
774	PT. DENIS PUTRA JAYA ELEKTRIK	9	3209	Dusun I Rt. 003/002 Ds. Kepongpongan Kec. Talun Kab. Cirebon Kab. Cirebon Jawa Barat	denisputrajayaelektrik.pt@gmail.com	081224357333	\N	\N
775	PT. MEKAR JAYA ELECTRIC	9	3209	Jl. Mawar No. 20 RT. 02/03 Tedengdaya Ds. Kedungjaya Kec. Kedawung Kab. Cirebon Jawa Barat	pt.mekar.jaya.electric@gmail.com		\N	\N
776	PT. KARYA JAYA TEHNIK	9	3209	Jl. Mawar No.20 Rt.02/03 Ds. Kedungjaya Kec. Kedawung Kab. Cirebon Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
777	PT. SINAR BERLIAN JAYA SAKTI	9	3209	Jl. KH. Agung Salim No. 36/18 Rt. 005/007 Desa Pegagan Kec. Palimanan Kab. Cirebon Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
778	PT. Surya Dharma	9	3210	Jl. Gunung Sari No.17 RT.01 RW.01 Desa Gunung Sari Kec. Kasokandel Kab. Majalengka, Jawa Barat Kab. Majalengka Jawa Barat	suryadharmapt@ymail.com	08122498477	\N	\N
779	PT. CAHAYA DHARMA	9	3210	JL. RAYA SINDANGWASA NO. 42 RT. 03 RW. 01 DESA SINDANGWASA KEC. PALASAH KABUPATEN MAJALENGKA PROVINSI JAWA BARAT Kab. Majalengka Jawa Barat	pt.cahayadharma@gmail.com	081312334444	\N	\N
780	PT. PRADIPA PRIMA BUANA	9	3210	Perum BCA Blok VI Jl. Jambu II No.155 Desa Cikalong Kec. Sukahaji Kab. Majalengka Jawa Barat	arif_m73@yahoo.com	-	\N	\N
781	PT. TRIYASA KARYA	9	3210	Jl. Raya Waringin No. 10 Desa Waringin Kec. Palasah Kab. Majalengka Kab. Majalengka Jawa Barat	triyasakarya.pt@gmail.com	-	\N	\N
782	PT. RADJASYA GALUH PRATAMA	9	3210	Blok Babakansari Rt. 028/004 Kel. Babakansari Kec. Bantarujeg Kab. Majalengka Jawa Barat	pusat@ptaskomelin.co.id	-	\N	\N
783	PT. HADIS ELEKTRIK SEJAHTERA	9	3211	JL.PANGERAN KORNEL LINGKUNGAN SAWAHBERA NO.05 SUMEDANG Kab. Sumedang Jawa Barat	hadiselektriksejahtera@gmail.com	-	\N	\N
784	PT. DITAR ELEKTRIK MANDIRI	9	3211	Lingkungan Sawah Bera No.01 RT.01/05 Kel.Pasanggrahan Baru Kec.Sumedang Selatan Kabupaten Sumedang Kab. Sumedang Jawa Barat	mandiriditarelektrik@yahoo.com	-	\N	\N
785	PT. ASTRIA MANDIRI	9	3211	Dusun Gudang Rt.05/03 Kel. Gudang Kec. Tanjungsari Kab. Sumedang Jawa Barat	pt.astria.mandiri@gmail.com	-	\N	\N
786	PT. ADHI RAHAYU MANDIRI	9	3211	Jl.Seroja IV No..3 Perum Griya Jatinangor I Kab. Sumedang Jawa Barat	adhirahayumandiri.pt@gmail.com	-	\N	\N
787	PT. MEDAL KARYA MANDIRI	9	3211	DUSUN PANYIRAPAN RT 01/07 KEL URAHAN MEKARJAYA KECAMATAN SUMEDANG UTARA KABUPATEN SUMEDANG Kab. Sumedang Jawa Barat	medalkaryamandiri@gmail.com	-	\N	\N
788	PT. TANDANG JAYA UTAMA SEJAHTERA	9	3211	Jl. Pangeran Santri No.115 Kel. Kotakulon Kec. Sumedang Selatan Kab. Sumedang Jawa Barat	pt.tjus@yahoo.com	-	\N	\N
789	PT. SINAR KENCANA TECH	9	3211	Dusun Margamukti Rt.04/05 Licin Kec. Cimalaka Kab. Sumedang Jawa Barat	skencana@yahoo.co.id	-	\N	\N
790	PT. ZIYAAD BERKAH ELEKTRIK	9	3211	Jl. Simpang Parakan Muncang No.7 Rt.04 Rw.01 Haurngombong Kec. Pamulihan Kab. Sumedang Jawa Barat	pt.zibel@gmail.com	-	\N	\N
791	PT. CEPI PUTRI ELEKTRIK	9	3211	Dusun Rancamekar Rt. 003/001 Ds. Haurgombong Kec. Pamulihan Kab. Sumedang Jawa Barat	cepi_putri@gmail.com	-	\N	\N
792	PT. ZIYAAD BERKAH ELEKTRIK	9	3211	JL. SIMPANG PARAKAN MUNCANG NO.7 RT.04/RW.01 HAURNGOMBONG PAMULIHAN Kab. Sumedang Jawa Barat	pt.zibel@yahoo.co.id	-	\N	\N
793	PT. OPIK ONLINE MANDIRI	9	3211	Dusun Cipacung Rt.01/01 Ds. Darmajaya Kec. Darmajaya Kab. Sumedang Jawa Barat	cvopikonline@yahoo.com	-	\N	\N
794	PT. RISMA PRIMA KARYA	9	3211	Griya jatinangor II D2 No. 05 Kel. cinanjung Kec. Tanjungsari Kab. Sumedang Jawa Barat	pusat@ptaskomelin.co.id	-	\N	\N
795	PT. FUZI CITRA MANDIRI	9	3211	Jl.Padasuka Rt. 01/02 Kel. Padasuka Kec. Sumedang Utara Kab. Sumedang Jawa Barat	pusat@ptaskomelin.co.id	-	\N	\N
796	PT. NABIL ZAIDAN ELECTRICKomp. Griya Taman Lestari D4 No. 23 Gudang Kec. Tanjungsari Kab. Sumedang Jawa Baratpusat.askomelin@gmail.com-16 September 2021Instalasi Pemanfaatan Tenaga Listrik	9	3211				\N	\N
797	PT. BIRO TEKNIK CAHAYA	9	3212	Jl.Letnan Purbadi No.200/D Rt.009 Rw.002 Kel.Lemahabang Kec.Indramayu Kab. Indramayu Jawa Barat	cahayapt_crb@yahoo.com	0234-272769	\N	\N
798	PT. JASOKA INDRA PRATAMA	9	3212	Blok Weluntas RT.03 RW.01 Desa Lelea Kec.Lelea Kab. Indramayu Jawa Barat	jasokaindrapratama@gmail.com	085314508566 / 087727533933	\N	\N
799	PT. TRIYAN MULYA INSTALATIKA	9	3212	Dsn. Kesambi Rt.017/008 Kedungwungu Kec. Anjatan Kab. Indramayu Jawa Barat	pt.triyanmulyainstalatika@yahoo.com		\N	\N
800	PT. VARIA DUTA ENGENEERING	9	3212	Jl. Jendral Sudirman No. 76 Blok Cipancuh Rt.025/012 Desa Cipancuh Kec. Haurgeulis Kab. Indramayu Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
801	PT. TRI MULYA ENG	9	3212	Dusun sukadamai Rt. 03/01 Bogor Kec. Sukra Kab. Indramayu Jawa Barat	pusat@ptaskomelin.co.id		\N	\N
802	PT. RIKI ELEKTRIK INDONESIA	9	3213	Dsn. Dukuh Hilir Timur RT. 004/001 Ds. Dukuh Kec. Kec. Ciasem Kab. Subang Jawa Barat	rikielektrik.rd@gmail.com	0852-2433-8400	\N	\N
803	PT. SATRIA TEHNIK INDONESIA	9	3213	Dusun Patimban Rt.04/02 Ds Patimban Kec. Pusakanagara Kab. Subang Jawa Barat	pt.satriateknikindonesia@yahoo.com	082299070808	\N	\N
804	PT. ANUGRAH JAYA ENJENERING	9	3213	Kp. Kamarung Selatan Rt.035/009 Desa Kamarung Kec. Pagaden Kab. Subang Jawa Barat	pt.anugrah_jaya90@yahoo.com	-	\N	\N
805	PT. ZAZKYA DWI PRATAMA	9	3213	Jl. Plamboyan No.21 Rt.90/25 Kel. Karanganyar Kec. Subang Kab. Subang Jawa Barat	zazkyadp14pt@gmail.com	-	\N	\N
806	PT. MEDIA PUTRA ENG	9	3213	Jl, Darmodiharjo No. 11 Lio Rt. 049/006 Kel. Sukamelang Kec. Subang Kab. Subang Jawa Barat	pt.mediaputra_eng@yahoo.com	-	\N	\N
807	PT. SARANA KARYA PUTRA	9	3213	Jl. Cigugur No. 02 Cigugur Kaler Kec. Pusakajaya Kab. Subang Jawa Barat	pusat@ptaskomelin.co.id	-	\N	\N
808	PT. MITRA PUTRA ENG	9	3213	Dusun Karang Tengah Rt.008/003 21 Desa Cigugur Kaler Kec. Pusakajaya Kab. Subang Jawa Barat	pusat@ptaskomelin.co.id	-	\N	\N
809	PT. PANCA ANUGRAH SEJAHTERA	9	3214	Jl. Terusan Kapten Halim No, 31L RT.12 RW. 05 Desa Lebak Anyar Kec. Pasawahan Kab. Purwakarta Jawa Barat	ade.pancaanugrah@yahoo.co.id	0264-210152	\N	\N
810	PT. INDAH JAYA PRIMA	9	3214	Jl. Terusan Kapten Halim No. 66I RT. 13 RW. 05 Desa Lebak Anyar Kec. Pesawahan Kab. Purwakarta Jawa Barat	indahjaya95@yahoo.com	0264-210152	\N	\N
811	PT. DESINDO ANUGRAH ENERGI	9	3214	KOMP BUMI JAYA INDAH KAV. X-1 RT/RW : 53/04 MUNJUL Kab. Purwakarta Jawa Barat	pt.desinergi@yahoo.com	08172388893	\N	\N
812	PT. SAFARI SINAR PURWAKARTA	9	3214	Gg beringin RT 76 RW 08, Kelurahan Nagri Kaler Kecamatan Purwakarta Kab. Purwakarta Jawa Barat	safarisinar_pwk@yahoo.com	(0264)201086, HP 0811242117	\N	\N
813	PT. ADHIKARYA SENTOSA PRIMA	9	3214	Jl. Gunung Putri No. 1 RT.01 RW.05 Kel. Ciseureuh Kec. Purwakarta Kab. Purwakarta, Jawa Barat Kab. Purwakarta Jawa Barat	karya_sentosa@ymail.com	081310491117	\N	\N
814	PT. ARYA MULTIDAYA PERKASA	9	3214	Kp. Sukamanah Rt.017/005 Desa Cigelam Kec. Babakancikao Kab. Purwakarta Jawa Barat	pt.aryamultidayaperkasa@yahoo.co.id	-	\N	\N
815	PT. MITRA GHASSANI TEKHNIK	9	3214	Kp. Sukasari No.72A Rt.03 Rw.01 Desa.Ciwareng Kec.Babakan Cikao Kab.Purwakarta Kab. Purwakarta Jawa Barat	mitraghassani_tekhnik@yahoo.com	-	\N	\N
816	PT. MITRA TANGGUH UTAMA	9	3214	Kp. Sukasari No.72A Rt.03 Rw.01 Desa.Ciwareng Kec.Babakan Cikao Kab.Purwakarta Kab. Purwakarta Jawa Barat	mitratangguhutama@ymail.com	-	\N	\N
817	PT. PRIMA JAYA ELECTRICKp. Cibodas Rt.006/002 Desa Cibodas Kec. Sukatani Kab. Purwakarta Jawa Baratprimajayaelectric@gmail.com-18 April 2022menengah	9	3214				\N	\N
818	PT. CIPTA JAYA SEJAHTRA	9	3215	Jl Benteng Jaya kaceot I Rt 001 Rw 0.14 Kel Tunggak Jati Kec. karawang Barat Kab. Karawang Jawa Barat	ciptajaya.general@gmail.com	081288166445	\N	\N
819	PT. CAHAYA BUMI AGUNG TEKNIK	9	3215	jln. Tamelang Barat No. 72 Kel. Margasari Kec. Karawang Timur Kab. Karawang Kab. Karawang Jawa Barat	bumi_agungteknik@yahoo.com	08129542673	\N	\N
820	PT. PULO DAMAR ABADI	9	3215	Perumnas Bumi Telukjambe Ruko Blok 1 Desa Sukaluyu, Kec. Telukjambe Timur, Kabupaten Karawang, Jawa Barat Kab. Karawang Jawa Barat	pt.pulodamarabadi@yahoo.com	0267-8458132	\N	\N
821	PT. HANDY PURNAMA BERDIKARI	9	3215	KP. WARUNG KALIJURANG RT 03/02 DS. PURWASARI KEC. PURWASARI KAB. KARAWANG Kab. Karawang Jawa Barat	chaos.cambel@gmail.com	085713911833	\N	\N
822	PT. HYAT SEJAHTERA TEKNIK	9	3215	Jl. Singaperbangsa No. 7 Dusun II Rt. 003 Rw. 002 Desa Sumurgede Kec. Cilamaya Kulon Kab. Karawang Jawa Barat Kab. Karawang Jawa Barat	hst.krw@gmail.com	081510293485	\N	\N
823	PT. BINA USAHA TEKNIK	9	3215	jl. Tamelang Barat No. 72 Rt 22/Rw 05 Desa Margasari Kec Karawang Timur Kab Karawang Kab. Karawang Jawa Barat	bina_usaha_karawang@yahoo.co.id	08129542673	\N	\N
824	PT. GADINO SEJAHTERA ABADI	9	3215	JJL MASHUDI NO 26 KP CARIU RT 003 RW 005 DS WANCIMEKAR KEC KOTABARU Kab. Karawang Jawa Barat	gadino.pt@gmail.com	0264-318984 / 085216429955	\N	\N
825	PT. CITRA JAYA TEKNIKA	9	3215	Jl. Malabar No. 87C RT.07/13 Kel. Karangpawitan Kec. Karawang Barat Kab. Karawang Jawa Barat	pt.citrajayateknika@yahoo.co.id	0267-414802	\N	\N
826	PT. GUNTUR PERKASA ENGINERING	9	3215	Jl. Malabar No. 87C RT.07/13 Kel. Karangpawitan Kec. Karawang Barat Kab. Karawang, Jawa Barat Kab. Karawang Jawa Barat	pt.gunturperkasa@yahoo.co.id	0267-414802	\N	\N
827	PT. WIBAWA NURSEWA SELATAN	9	3215	RUKO KARABA BLOK III NO.14 Kab. Karawang Jawa Barat	RABAN27WNS@YAHOO.COM	08111897040	\N	\N
828	PT. ELANG SURYA SAKTI	9	3215	Karawang Green Village Jl. Violet IV Blok V8 No.29 Rt.014/004 Ds. Sukaluyu Kec. Telukjambe Timur Kab. Karawang Jawa Barat	ptelangsuryasakti@gmail.com		\N	\N
829	PT. SINAR MUSTIKA SAKTI	9	3215	Jl. Syeh Quro No.06 Krajan Rt.003/009 Kel Palumbonsari Kec. Karawang Timur Kab. Karawang Jawa Barat	pt.sinarmustikasakti@gmail.com		\N	\N
830	PT. FIRMAN JAYA UTAMA TEKNIK	9	3215	Perum Buana Asri Blok A10/02 Rt.004/017 Kel. Palumbonsari Kec. Karawang Timur Kab. Karawang Jawa Barat	firmanjayautamateknik_pt@yahoo.com		\N	\N
831	PT. BINTANG LISTRINDO UTAMA	9	3215	Jl. Suntara Amijaya Lubangsari Rt.003/013 Karawang Wetan Kec. Karawang Timur Kab. Karawang Jawa Barat	bintanglistrindoutama_pt@yahoo.com		\N	\N
832	PT. BISMA JAYA UTAMA	9	3215	Perum pesona blok C.10 No.06-07 Desa Kalangsurya Kec. Rengasdengklok Kab. Karawang Jawa Barat	pt.bismajayautama@gmail.com	-	\N	\N
833	PT. ADI KARYA GUNA UTAMA	9	3215	Jl. Damai I Perum Pesona Kalangsuria Blok C 10/6-7 Rt.21/06 Desa Kalangsuria Kec. Rengasdengklok Kab. Karawang Jawa Barat	pt.adikaryagunautama@gmail.com	-	\N	\N
834	PT. ADI KARYA PERSADA	9	3215	Bumi Telukjambe Blok Y/11 Rt. 001/013 Ds. Sukaharja Kec. Telukjambe Timur Kab. Karawang Jawa Barat	adikaryapersada.pt@gmail.com	-	\N	\N
835	PT. VARIA KARYA NUSANTARA	9	3215	Perum. Resinda Blok A5/10, RT.01 RW.11, Kel. Purwadana, Kec. Telukjambe Timur Kab. Karawang Jawa Barat	pt.vk_nusantara@yahoo.com	-	\N	\N
836	PT. FIKAWAN UTAMA	9	3215	Jl. Perumnas Bumi Telukjambe Blok H No.63 Desa Sukaluyu Kec. Telukjambe Timur Kab. Karawang Jawa Barat	rella.fikawanutama@gmail.com	-	\N	\N
837	PT. NIAGAMAS SETIAUSAHA	9	3216	PERMATA METROPOLITAN, BLOK A.3 NO. 25 RT/RW. 003/002 KELURAHAN TAMBUN KECAMATAN TAMBUN SELATAN Kab. Bekasi Jawa Barat	INFO@NIAGAMAS-SETIA.COM	021-88332544	\N	\N
838	PT. PILAR JAYA	9	3216	JL. RAYA CIBARUSAH RT 17 RW 06 KELURAHAN PASIRSARI KECAMATAN CIKARANG SELATAN Kab. Bekasi Jawa Barat	pt.pilar_jaya@yahoo.co.id	021-8900033	\N	\N
839	PT. PUTRA KAWASEN	9	3216	JL. MEKARSARI TENGAH NO. 50 MEKARSARI TAMBUN SELATAN KAB. BEKASI JAWA BARAT Kab. Bekasi Jawa Barat	kennibutar27@gmail.com	021-88391563/08129522084	\N	\N
840	PT. TRIMULYA DAYA UTAMA	9	3216	Kp. Kaum Utara RT.04/01 Jl. KH Fudholi NO. 21 Kel. Karangasih Kec. Cikarang Utara Kab. Bekasi Jawa Barat	pt_trimulyadayautama@yahoo.co.id	021-8900033	\N	\N
841	PT. ATE KINDO	9	3216	Kp. Kaum Utara RT. 04/04, Jl. KH Fudholi No. 60 Kel. Karang Aasih Kec. Cikarang Utara Kab. Bekasi Jawa Barat	atekindopt@yahoo.com	021-89106930	\N	\N
842	PT. Citramasjaya Teknikmandiri	9	3216	Jl. Pangeran Diponegoro No. 108, Kelurahan Jatimulya, Kecamatan Tambun Selatan, Kabupaten Bekasi, Jawa Barat Kab. Bekasi Jawa Barat	citramas@citramas.co.id	021-8822149	\N	\N
843	PT. MAJU JAYA PRIMA PERKASA	9	3216	Mega Regency Blok B2 No. 12, RT 04 RW 021, SUKARAGAM, SERANG BARU, BEKASI, JAWA BARAT Kab. Bekasi Jawa Barat	majujayaprimaperkasa@gmail.com	02129370240/082122448000	\N	\N
844	PT. BUKILLA MANDIRI	9	3216	Tmn Alamanda Blok F5 No. 23 Tambun Utara Bekasi Kab. Bekasi Jawa Barat	bukillamandiri@yahoo.co.id	081218520126	\N	\N
845	PT. INTI PERSADA PERKASA	9	3216	KP. BURANGKENG RT. 001 RW. 006, DESA BURANGKENG, KEC. SETU KABUPATEN BEKASI, JAWA BARAT Kab. Bekasi Jawa Barat	ptintipersadaperkasa@gmail.com	08111093499	\N	\N
846	PT. DWIKEI MULIA PRATAMA	9	3216	Bumi Yapemas Indah Jl.Pendakian Blok B2 No.7-8 Desa Sumber jaya Tambun Selatan kab.Bekasi - Jawa Barat Kab. Bekasi Jawa Barat	dwikeimuliapratama@yahoo.com	02188391246/081311256901	\N	\N
847	PT. SINAR ANISA MANDIRI	9	3216	Kp. kalijambe RT 002 RW 004. Desa Lambangsari, Kecamatan Tambun, Kabupaten Bekasi, Jawa Barat Kab. Bekasi Jawa Barat	pt.sinaranisamandiri@gmail.com		\N	\N
848	PT. Rajawali Solusi Energindo	9	3216	Ruko Grand Kalimalang Unit 8 Y, Jl. KH. Noer Ali No. 8, Jati Mulya - Tambun Selatan, Bekasi Kab. Bekasi Jawa Barat	rajawalisolusienergindo@yahoo.co.id		\N	\N
849	PT. DHAKO MITRA MANDIRI	9	3216	Perum Bumi Cikarang Makmur Blok D4 No.6 Desa. Sukadami Kec. Cikarang Selatan Kab. Bekasi Kab. Bekasi Jawa Barat	dakhomitramandiri@gamil.com		\N	\N
850	PT. ALEXANDRIA TRI KARYA	9	3216	Perumahan Mustika Gandaria Blok B5 No.5 Rt.001/011 Ciledug Kec. Setu Kab. Bekasi Jawa Barat	surotodimas@gmail.com		\N	\N
851	PT. KARYA GUNA TEKNIK	9	3216	Ruko Delta Niaga 2 JL. Waru Blok C No.21Lippo Cikarang Cibatu Kab. Bekasi Jawa Barat	kgt.cikarang@gmail.com		\N	\N
852	PT. YI SU ELECTRONICS	9	3216	JL. Samsung I Blok B-9A Ds. Mekarmukti Kec. Cikarang Utara Kab. Bekasi Jawa Barat	yisuelectronics@gmail.com		\N	\N
853	PT. PRASTIWAHYU TRIMITRA ENGINEERING	9	3216	Kawasan Industri Jababeka II Kav. KK-38 Jl. Industri Selatan 1B Kab. Bekasi Jawa Barat	info@prastiwahyu.com		\N	\N
854	PT. HANIF TUNGGAL MANDIRI	9	3217	PURI MELIA GARDEN BLOK C9 RT 01 RW 16, DESA JAMBUDIPA, KECAMATAN CISARUA, KABUPATEN BANDUNG BARAT Kab. Bandung Barat Jawa Barat	pthaniftmbdg@gmail.com	022 - 82783810	\N	\N
855	PT. KANDIS CIPTA PERSADA	9	3217	JL. RAYA CIMAREME NO. 230 RT.001 RW.003 DESA CIMAREME KECAMATAN NGAMPRAH 40552 Kab. Bandung Barat Jawa Barat	kandisciptapersada@gmail.com	0226621047 / 085103580200	\N	\N
856	PT. PUTRI DAM RAYA ENGENEERING	9	3217	Bukit Cipageran Indah A-98 Rt.04/03 Ds. Tanimulya Kec. Ngamprah Kab. Bandung Barat Jawa Barat	putri.damraya@yahoo.com	022-6626163	\N	\N
857	PT. ASTER JAYA PERKASA	9	3217	Jl. Cihideung Gudang Rt.003 Rw.003 Kel. Gudang Kahuripan Kec. Lembang Kab. Bandung Barat Jawa Barat	pt.asterj@yahoo.com		\N	\N
858	PT. INTAN MUFAKAT RAYA	9	3273	Jl. Manunggal No. 25 Kel. Gegerkalong, Kec. Sukasari, Kota Bandung 40153 Kota Bandung Jawa Barat	ptimura@gmail.com	0222011229	\N	\N
859	PT. DUTA GLOBAL ELEKTRINDO	9	3273	Jl. Mekar Puspita No. 52 RT.004 RW.002, Kelurahan Mekarwangi, Kecamatan Bojongloa Kidul Kota Bandung Jawa Barat	adm@dutaglobalindo.com	022-21300400	\N	\N
860	PT. SANGKAN JAYA	9	3273	Jl. RAA Maratanegara No. 80 RT01 RW 02 kelurahan Turangga kecamatan Lengkong Kota Bandung Jawa Barat	ptsangkanbdg@yahoo.co.id	022 7319335/085722664242	\N	\N
861	PT. SINAR PERSICO KENCANA PUTRA	9	3273	Jl. Cibadak No.195 Bandung Kota Bandung Jawa Barat	sinar.persico.kencana.putra@gmail.com	085659274191	\N	\N
862	PT. Tritama Mitra Lestari	9	3273	Jl. Soekarno Hatta No.541 C, Kelurahan Gumuruh, Kecamatan Batununggal, Bandung Kota Bandung Jawa Barat	krisponda@gmail.com	081321482760	\N	\N
863	PT. LUCY ELEKTRIK DJAYA	9	3273	Jl. H. Kurdi I No. 8 Moh. Toha Bandung Kota Bandung Jawa Barat	lucy.elektrikdjaya@gmail.com	022-5206600	\N	\N
864	PT. MUGHNI SALIM	9	3273	Jalan Soekarno Hatta No.441 A Bandung Kota Bandung Jawa Barat	mughnisalim@gmail.com	022-5221690	\N	\N
865	PT. BANDA KARYA ABADI	9	3273	Jalan Banda No. 22 Lt. 3 Bandung 40115 Kota Bandung Jawa Barat	bandakaryaabadi@yahoo.co.id	022 - 4209607	\N	\N
866	PT. PERLINAS ENERGI UTAMA	9	3273	Jl. Setra Dago Utama No. 31 Antapani Wetan, Kec. Antapani, Bandung Kota Bandung Jawa Barat	perlinasenergi@gmail.com	022-87771822	\N	\N
867	PT. HEN JAYA	9	3273	Jl.m Kurdi Raya No. 14 Moh. Toha Bandung Kota Bandung Jawa Barat	pt_henjaya@yahoo.com	022-5206600	\N	\N
868	PT. Industri Telekomunikasi Indonesia	9	3273	JL Moch Toha 77 Bandung Kota Bandung Jawa Barat	info@inti.co.id	(022)5201501	\N	\N
869	PT. SURYA ENERGI INDOTAMA	9	3273	Jl. Soekarno Hatta No. 442 RT. 001 RW. 007 Kel. Pasirlayu Kec. Regol Kota Bandung Kota Bandung Jawa Barat	info@suryaenergi.com	022-5202682	\N	\N
870	PT. REYRAKA DINASTY	9	3273	JL. PALEDANG NO. 154 CAMPAKA ANDIR BANDUNG Kota Bandung Jawa Barat	pt.reyraka_dinasty@yahoo.com	022 6027753/6027889	\N	\N
871	PT. BANGUN PUJI LUMBUNG ABADI	9	3273	Jl. Cijawura Girang V No. 50 RT 03 / RW 13 Kel. Sekejati Kec. Buah Batu Bandung Kota Bandung Jawa Barat	ptbangunpuji50@gmail.com	022-7563901	\N	\N
872	PT. KEMBAR ABADI PRIMA	9	3273	Jl. Banda No. 22 Bandung 40115 Jawa Barat Kota Bandung Jawa Barat	kembarabadiprima@yahoo.co.id	022-4204355	\N	\N
873	PT. ARUS KUAT ENERGI PRIMA	9	3273	Jl. Haremis II No. 15A Kel. Turangga Kec. Lengkong Kota Bandung Kota Bandung Jawa Barat	pt.aruskuat_energi_prima@yahoo.com	0811225404	\N	\N
874	PT. BIMA CAHAYA TEKNIK	9	3273	JL. TUBAGUS ISMAIL VII NO. 22A KEL. SEKELOA KEC. COBLONG Kota Bandung Jawa Barat	pt.bimacahayateknik@yahoo.co.id	022 2511129	\N	\N
875	PT. TOTAL DAYA	9	3273	Grand Antapani Town House No. A2 RT. 001 RW. 025 Kel. Antapani Tengah Kec. Antapani Kota Bandung, Jawa Barat Kota Bandung Jawa Barat	totaldaya@yahoo.com	022-88883397, 022-88883398	\N	\N
876	PT. PANDAN WANGI SAE	9	3273	JL. CIJAWURA GIRANG V NO. 45, BUAH BATU, BANDUNG Kota Bandung Jawa Barat	pandanwangi_sae@yahoo.com	0227510396	\N	\N
877	PT. SINGGASANA ARTHA PRATAMA	9	3273	Jl. Mekar Utama No.27 Mekarwangi Kec. Bojongloa Kidul Kota Bandung Jawa Barat	singgasana.arthapratama@gmail.com	081221082108	\N	\N
878	PT. MITRA SEJATI CITRAPERWIRA	9	3273	Griya Cempaka Arum Cluster Andalus Blok C No.29 Cimincrang Kota Bandung Jawa Barat	citraperwira@gmail.com	022-87794926, 022-2014875	\N	\N
879	PT. JAVFLO CIPTA MANDIRI	9	3273	Sky Building Blok A Jl. Rancabolang No.167 Kel. Sekejati Kec. Buah Batu Kota Bandung Jawa Barat	marketing@javflo.com	022-87303680	\N	\N
880	PT. SURYAMULTI UTAMA BAHANACITRA	9	3273	Jl. Karasak Tengah III No.12 Kel. Karasak Kec. Astanaanyar Kota Bandung Jawa Barat	suryamultibc@gmail.com	022-5208659	\N	\N
881	PT. CIPTA TRIDAYA DINAMIKA	9	3273	Jl. Moh. Toha No.218 RT.05 RW.06 Karasak Kota Bandung Jawa Barat	ciptatridayadinamika@gmail.com	022-5228496	\N	\N
882	PT. BANDA KARYA ABADI	9	3273	Jl. Banda No.22 Lt.3 Rt.003/006 Kel. Citarum Kec. Bandung Wetan Kota Bandung Jawa Barat	bandakaryaabadi@yahoo.co.id	022-4209607	\N	\N
883	PT. PUTRA AGRAMANDALA SAKTI	9	3273	JL.Peta Kopo Plaza Blok F5 Suka Asih Kec. Bojong Loa Kaler Kota Bandung Jawa Barat Kota Bandung Jawa Barat	mandalasakti.hs96@yahoo.co.id	022 6010389	\N	\N
884	PT. PUJI TRI DAYA	9	3273	JL. CIJAWURA GIRANG V NO.50 RT.003 RT.013 Kota Bandung Jawa Barat	pt_pujitridaya@yahoo.com	0227563901 / 082121432808	\N	\N
885	PT. REKADAYA SEMESTA	9	3273	JL. GOTONG ROYONG NO. 138 Kota Bandung Jawa Barat	pt_rds@yahoo.com	022 7597949 / 085105074464	\N	\N
886	PT. INDISI	9	3273	Jl. Bengawan No. 28 Kelurahan Cihapit Kecamatan Bandung Wetan Kota Bandung Kota Bandung Jawa Barat	pt_indisi@yahoo.com	022 7234572	\N	\N
887	PT. ARIMULTI ENGINEERING	9	3273	JL. Raya Palem Permai No.6-8 Soekarno Hatta, Jatisari, Kec. Buah Batu Kota Bandung Jawa Barat	arimultiengineering@gmail.com	0227309262	\N	\N
888	PT. HARIFF POWER SERVICES	9	3273	Jl. A. H. Nasution No. 8 Cipadung - Bandung Kota Bandung Jawa Barat	info@hariffpower.co.id	0227815555 / 089655334422	\N	\N
889	PT. SANGKURIANG LINK ELEKTRIK	9	3273	JL. BUAH BATU NO.88 BURANGRANG KEC. LENGKONG Kota Bandung Jawa Barat	sangkuriang.elektrik@gmail.com	081322357332	\N	\N
890	PT. DINASTY ADI RAKA	9	3273	JL.PALEDANG NO.177 RT.03 RW.02 KEL.CAMPAKA KEC.ANDIR KOTA BANDUNG JAWA BARAT Kota Bandung Jawa Barat	pt.dinasty.adiraka@gmail.com	022-6027753	\N	\N
891	PT. MAHIR LASTANA GANTARI	9	3273	Jl.Rasamala No.1 Kota Bandung Jawa Barat	mahirlastana@gmail.com	022-7234080	\N	\N
892	PT. ARCA ENERGI	9	3273	Jl. Taman Gantole No.08 Arcamanik Kota Bandung Jawa Barat	ptarcaenergi@gmail.com	022-7208643	\N	\N
893	PT. CATUDAYA DATA PRAKASA	9	3273	Jl. Sekajati No. 08 RT. 01 RW. 11 Kel. Kebon Kangkung Kec. Kiaracondong Kab. Bandung, Jawa barat Kota Bandung Jawa Barat	surjadi@cdpbdg.co.id	-	\N	\N
894	PT. HASINDO TEKNO MANDIRI	9	3273	JL. RAHAYU NO. 17 RT 02/06 KEL. PASIR ENDAH KEC. UJUNG BERUNG KOTA BANDUNG JAWA BARAT Kota Bandung Jawa Barat	hasindo.tekman@yahoo.com	-	\N	\N
895	PT. BANDUNG ISCO	9	3273	Jl. Andromeda Blok L II A No.42 Margahayu Raya Bandung Kota Bandung Jawa Barat	bandung.isco@yahoo.co.id	-	\N	\N
896	CV. ZOOM	9	3273	Jl. Bumi Parakan Asri A03 Kel Cisaranteun Endah Kec Arcamanik Kota Bandung Jawa Barat	radjagantira@gmail.com	-	\N	\N
897	CV. PRIMA JAYA TEKNIK	9	3273	Kp. Sukawangi Rt 001 Rw 09 Kel Situwangi Kec Cihampelas Kota Bandung Jawa Barat	teguhmulyadi_46@yahoo.com	-	\N	\N
950	PT. REJEKI CAHAYA ELEKTRO	11	3402	POTRONANGGAN TAMANAN BANGUNTAPAN BANTUL Kab. Bantul Daista Yogyakarta	rejekicahaya@yahoo.com	0274-412315	\N	\N
951	PT. DIAN PERTIWI PERKASA	11	3402	Jl. Bantul KM. 6,5 Niten, Tirtonirmolo Kec. Kasihan Kab. Bantul, Yogyakarta Kab. Bantul Daista Yogyakarta	sinar_i@rocketmail.com		\N	\N
952	PT. DECO CIPTA PRATAMA	11	3402	JL IMOGIRI BARAT KM 5 RT 08 DESA BANGUNHARJO KECAMATAN SEWON BANTUL Kab. Bantul Daista Yogyakarta	deco_cp@yahoo.co.id		\N	\N
953	PT. GIRI CIPTA ELEKTRIKAL	11	3403	JL WONOSARI-SEMANU KM 03 WUKIRSARI RT 07 RW 03 BALEHARJO WONOSARI GUNUNG KIDUL Kab. Gunung Kidul Daista Yogyakarta	pt.gtc_gk@yahoo.co.id	0274394375	\N	\N
954	PT. ADHI CITRA MULIA	11	3404	Nusupan RT/RW 001/028 Kel. Trihanggo, Kec. Gamping, Kab. Sleman Kab. Sleman Daista Yogyakarta	pt.adhicitramulia@gmail.com	0274-6415419	\N	\N
955	PT. DAMAR ALAM SEMESTA	11	3404	PATEN RT 05/05 TRIDADI SLEMAN Kab. Sleman Daista Yogyakarta	damar.semesta@yahoo.co.id	0274866134	\N	\N
956	PT. LIMA SAUDARA SLEMAN	11	3404	JONGKE LOR NO.03 RT.027 RW.001 SENDANGADI MLATI Kab. Sleman Daista Yogyakarta	pt.lima_saudara_sleman@yahoo.com		\N	\N
957	PT. Novita Persada	11	3404	Tambak Bayan TB.IV 9A Catur Tunggal Depok Sleman Kab. Sleman Daista Yogyakarta	pt.novita.persada@gmail.com		\N	\N
958	PT. Sinar Persada	11	3404	Jl. Kledokan III No 145 Caturtunggal Depok Sleman Kab. Sleman Daista Yogyakarta	pt.sinarpersada@yahoo.co.id		\N	\N
959	PT. CAHAYA ABADI SEJATI	11	3404	JETAKAN RT.02RW.17 PANDOWOHARJO Kab. Sleman Daista Yogyakarta	cahayaabadisejati@yahoo.co.id		\N	\N
960	PT. BOSRI INDONESIA	11	3404	Perum Nandan Mandiri Blok 1G Kel. Sariharjo Kec. Ngaglik Kab. Sleman Daista Yogyakarta	bosri_indonesia@yahoo.com		\N	\N
961	PT. SCIENTRONIKA INDONESIA	11	3471	Jl. Glagahsari No 51 RT 13 RW 03 Kelurahan Warungboto, Kecamatan Umbulharjo Kota Yogyakarta Daista Yogyakarta	scientronika-indonesia@yahoo.co.id		\N	\N
962	PT. CAHAYA BINTANG ELECTRICAL	13	5101	Jl. Nakula No 24, Br. Tengah Negara Kab. Jembrana Bali	cahayabintang09@yahoo.com	081916196286	\N	\N
963	PT. AMI PERDANA TEKNIK	13	5101	JL. ANYELIR NO 1 BALER BALE AGUNG, KECAMATAN NEGARA Kab. Jembrana Bali	amiperdanateknik@yahoo.com	081558013474	\N	\N
964	PT. ADI KARTIKA GEMILANG	13	5102	JL. RATNA GANG 1 NO. 1 B DS. DAUH PEKEN Kab. Tabanan Bali	pt.adikartikagemilang@gmail.com	0361 819498	\N	\N
965	PT. SASTRA MAS ESTETIKA	13	5102	JALAN RAYA TUAKILANG WANASARI 99X TABANAN Kab. Tabanan Bali	sastramas@yahoo.com		\N	\N
966	PT. EKA SABDA KARYA	13	5102	JALAN GUNUNG LUMUT II NO.6A DENPASAR Kab. Tabanan Bali	ekasabdakarya@gmail.com		\N	\N
967	PT. WASKITA JAYA NUGRAHA	13	5104	JL. TUKAD BALIAN NO1, SAMPLANGAN Kab. Gianyar Bali	waskitajayanugraha12@gmail.com		\N	\N
968	PT. ARANT SEJAHTERA MANDIRI	13	5105	BR. DINAS PANGI KAWAN, DESA PIKAT, KEC. DAWAN, KLUNGKUNG Kab. Klungkung Bali	pt.arantsejahteramandiri@gmail.com		\N	\N
969	PT. SINAR MAS UTAMA	13	5105	BR./DUSUN SANGGING DESA KAMASAN KECAMATAN KLUNGKUNG Kab. Klungkung Bali	sinar.jayaabadi@yahoo.co.id		\N	\N
970	PT. DANA ABADI SENTOSA	13	5107	Jl. Pesagi, Lingkungan Pebukit, Kelurahan Karangasem, Kec. Karangasem Kab. Karangasem Bali	pt.danaabadisentosa@gmail.com	0366-4301898	\N	\N
971	PT. Dhana Elektrik	13	5107	Jl. Raya Buitan no. 99 Ds. Buitan, Manggis, Karangasem Kab. Karangasem Bali	De_darpa@yahoo.com	(0363) 41227	\N	\N
972	PT. ARIESTA JAYA TEKNIK	13	5107	JL. GUNUNG AGUNG NO. 88 AMLAPURA Kab. Karangasem Bali	ariestateknik@gmail.com	(0363) 22297	\N	\N
973	PT. MERCU SARANA ELEKTRIK	13	5107	Jl. Raya Buitan no. 88 Dsn. Buitan Desa. Manggis, Kec. Manggis, Kab. Karangasem Kab. Karangasem Bali	stif_ok@yahoo.com		\N	\N
974	PT. KRESNA ADITYA DARMA	13	5108	JALAN UDAYANA NO 15 A KEL. KALIUNTU KEC. BULELENG Kab. Buleleng Bali	ptkresnabali@gmail.com	0362 24656	\N	\N
975	PT. CAHAYA LISTRIK PINTAR	13	5108	Banjar Dinas dharmayadnya Gang angsoka desa tukad mungga kecamatan buleleng Kab. Buleleng Bali	clistrik@yahoo.co.id	081337570182	\N	\N
976	PT. Darma Cipta Mulia	13	5108	Jl. Srikandi Gg. Seroja No. 37, Kec. Sukasada Kab. Buleleng Bali	pt.darmacm@gmail.com		\N	\N
977	PT. INTI JAYA TEKNIK	13	5108	JL. IMAM BONJOL No.110 Kab. Buleleng Bali	pt.intijt@gmail.com		\N	\N
978	PT. MAHATMA JAYA DAMAI	13	5108	JL. GATOT KACA NO 10 SINGARAJA Kab. Buleleng Bali	mahatmajaya_damai@yahoo.com		\N	\N
979	PT. DARMA LESTARI TEKNIK	13	5108	JL PULAU MENJANGAN BTN BANYUNING BLOK I NO.64A Kab. Buleleng Bali	pt.darmalt@gmail.com		\N	\N
980	PT. DARMA KARYA LESTARI	13	5108	JL. PULAU MENJANGAN BTN BANYUNING BLOK I NO. 64 Kab. Buleleng Bali	pt.darmakl@gmal.com		\N	\N
981	PT. TIRTA AMERTA TEHNIK	13	5171	JL. COKROAMINOTO NO. 57 A DENPASAR Kota Denpasar Bali	tirtaamertatehnik@gmail.com	0361 - 426189 / 0811397726	\N	\N
982	PT. BALI BINA ELECTRO	13	5171	JL.LETDA WINDA NO.31 KEL.DANGIN PURI KEC.DENPASAR TIMUR Kota Denpasar Bali	balibinaelektro@gmail.com	0361 228 244	\N	\N
983	PT. ANDIKA MITRA JAYA	13	5171	Jl. Bedahulu IV/4, Denpasar Kota Denpasar Bali	andika.mitrajaya@gmail.com	0361-421836	\N	\N
984	PT. CAHAYA BARU TEKNIK	13	5171	JL. COKROAMINOTO NO. 57 A DENPASAR Kota Denpasar Bali	newcahayateknik@gmail.com	0361 - 426189	\N	\N
985	PT. PANCAR ELECTRIC METRICAL	13	5171	Jl. Tjok Agung Tresna No. 88 Desa Sumerta Kelod Kec. Denpasar Timur Kota Denpasar Bali	pancarelectricmetrical99@gmail.com	0361- 225599, 0361- 222899	\N	\N
986	PT. SURYA TERANG SEJATI	13	5171	JL. GUNUNG LEBAH NO.6 BR. SARI BUANA, TEGAL HARUM Kota Denpasar Bali	surya_terang_sejati01@yahoo.co.id	0361 485540	\N	\N
987	PT. PRATAMA SINAR UTAMA	13	5171	JALAN TUKAD BATANGHARI VI NO. 3 DENPASAR Kota Denpasar Bali	cv.pratama@ymail.com	0361 223206	\N	\N
988	PT. CAHAYA LESTARI NIRMALA	13	5171	JALAN TUKAD BATANGHARI VI NO. 3 DENPASAR Kota Denpasar Bali	ptcahayalestarinirmala@yahoo.com	081 337 026 823	\N	\N
989	PT. EKA PURNAMA MANDIRI	13	5171	JL. Astasura No. 98 - Denpasar Kota Denpasar Bali	ekapurnamamandiri@gmail.com	(0361) - 9008050	\N	\N
990	PT. Tri Githa Sarana	13	5171	Jl. Tukad Asahan No. 1 Panjer, Denpasar Selatan Kota Denpasar Bali	trigithasarana@gmail.com	0361 8955995	\N	\N
991	PT. Surya Dewata Abadi	13	5171	Jl. Tukad Asahan No. 1 Panjer, Denpasar Selatan Kota Denpasar Bali	suryadewataabadi15@gmail.com	0361 8955995	\N	\N
992	PT. WANATARA JAYA ABADI	13	5171	JL ARJUNA NO 17C LANTAI II DUSUN LELANGON KELURAHAN DAUH PURI KAJA KECAMATAN DENPASAR UTARA Kota Denpasar Bali	wanatarajaya99@yahoo.co.id	(0361)225122	\N	\N
993	PT. JATI KARYA MAS	13	5171	JL. SEKAR JEPUN I NO.1 LANTAI I KESIMAN KERTALANGU DENPASAR TIMUR Kota Denpasar Bali	sertifikasi.bali@gmail.com	(0361) 467736	\N	\N
994	PT. RAJA ISOLATOR ELEKTRIKAL	13	5171	JL. Danau Tondano IV No. 11 B Kota Denpasar, Bali Kota Denpasar Bali	rajaisolatorelektrikal@gmail.com	0361271056 / 081338022826	\N	\N
995	PT. MARGA UTAMA MANDIRI	13	5171	JL. NANGKA - GRAHA PERMAI NO. 1 TONJA Kota Denpasar Bali	sertifikasi.bali@gmail.com	0361421777,0361432980	\N	\N
996	PT. ADI PUTRA	13	5171	Jl. Gunung Catur II B (Lantai Basement) No. 6 Kel. Padangsambian Kaja Kec. Denpasar Barat Kota Denpasar, Bali Kota Denpasar Bali	adiputra_cv@yahoo.com	(0361) 423029	\N	\N
997	PT. DWI EKA BAKTI	13	5171	JALAN KENYERI NO. 70 XX DENPASAR Kota Denpasar Bali	Dwiekab@yahoo.com	081 338 304 333	\N	\N
998	PT. SAKSAMA KARYA ABADI	13	5171	JL. JAYAGIRI III NO. 6, DS. DANGIN PURI KELOD DENPASAR TIMUR Kota Denpasar Bali	dominikusngabut@gmail.com	085100892952/0361262172	\N	\N
999	PT. SRITI SAKTI ABADI	13	5171	JALAN TEGAL DUKUH V NO. 9 DENPASAR 80117 Kota Denpasar Bali	sritisakti21@gmail.com	0361 9005170	\N	\N
1000	PT. NURJAYA KARYA LISTRIK	13	5171	JL. TUNJUNG SARI NO. 48 DESA PADANGSAMBIAN KEC. DENPASAR BARAT Kota Denpasar Bali	nurjaya_kl@yahoo.com		\N	\N
1001	CV. LINTAS DAYA	13	5171	JALAN TUKAD CITARUM BLOK i NO.7B RENON, DENPASAR Kota Denpasar Bali	mail@lintasdaya.com		\N	\N
1002	PT. Bumi Sentosa	13	5171	Jln. Pendidikan I Perum Graha Kerti Blok H No. 4 Denpasar Kota Denpasar Bali	info@bumisentosa.co.id		\N	\N
1003	CV. Seno Sakti	13	5171	Jl. Sarigading No 81 Denpasar Kota Denpasar Bali	senosakti@ymail.com		\N	\N
1004	PT. GALANG WIGUNA ABADI	13	5171	Jl gunung tangkuban perahu no 285 Kota Denpasar Bali	wayan.sintur@gmail.com		\N	\N
1005	PT. SUARTITO TEKNIK MANDIRI	13	5171	JALAN GUNUNG LEBAH NO 6 DENPASAR Kota Denpasar Bali	pt.stmandiri@yahoo.com		\N	\N
1006	PT. ADI CANDRA	13	5171	JL GUNUNG CATUR II B NO 6 DENPASAR Kota Denpasar Bali	adicandra_cv@yahoo.com		\N	\N
1007	PT. ADI DWIJAYA MANDIRI	13	5171	JL.SANDAT V/I NO 7 DENPASAR Kota Denpasar Bali	dwijaya2014@gmail.com		\N	\N
1008	PT. Karyamulya Mukti Utama	13	5171	Jl. Pura Duwe No. 3 (Teuku Umar Barat) Desa Padangsambian Kelod Kec. Denpasar Barat Kota Denpasar, Bali Kota Denpasar Bali	karyamulya_07@yahoo.com		\N	\N
1009	PT. ELEKTRA KARYA PERSADA	13	5171	JL. TUKAD BADUNG NO. 234 RENON Kota Denpasar Bali	elektra.persada@gmail.com		\N	\N
1010	PT. Suluh Gangga Wisesa	13	5171	Jl. Nangka Utara Gg. Kesumasari No. 8 Tonja Denpasar Kota Denpasar Bali	pt_sgw@yahoo.co.id		\N	\N
1011	PT. Witala Jaya Abadi	13	5171	Jl. Jepun Pipil No. 1 Gatot Subroto Timur Kota Denpasar Bali	ptwitala@yahoo.co.id		\N	\N
1012	PT. PUTERA PETIR PERKASA	13	5171	JALAN AHMAD YANI GG.V NO.5 DAUH PURI KAJA KECAMATAN DENPASAR UTARA Kota Denpasar Bali	petirbali@yahoo.com		\N	\N
1013	PT. KARYA INDAH TEKNIKAL	13	5171	JL NANGKA GG PAKSIMAS II NO 4 DENPASAR Kota Denpasar Bali	karya.indahteknikal@yahoo.co.id		\N	\N
1014	PT. CATUR PUTRA TEKNIK	13	5171	JL.WIBISANA NO.2A DUSUN PANTI SARI DESA PEMECUTAN KAJA KECAMATAN DENPASAR UTARA Kota Denpasar Bali	caturputrateknik@gmail.com		\N	\N
1015	PT. GUMANDIRI UTAMA ELEKTRIK	13	5171	JL. ARJUNA NO.17C LT.II Kota Denpasar Bali	gumandiri@yahoo.co.id		\N	\N
1016	PT. LESTARI KARYA ELEKTRIK	13	5171	JL. TEGAL DUKUH V NO. 9 KEL. PADANG SAMBIAN KEC. DENPASAR BARAT Kota Denpasar Bali	ptlestarikarya@gmail.com		\N	\N
1017	PT. KUSUMA JAYA TEKNIK	13	5171	JL. NANGKA GG KENARI IV NO. 6 Kota Denpasar Bali	teknikkusuma@gmail.com		\N	\N
1018	PT. TEA KIRANA	13	5171	Jl. Sarigading No. 81 Denpasar Kota Denpasar Bali	teakirana@yahoo.com		\N	\N
1019	PT. KERTHA JAYA TEKNIK	13	5171	JL. TUKAD BANYU POH NO 1 KELURAHAN PANJER KECAMATAN DENPASAR SELATAN Kota Denpasar Bali	kerthajayateknik@gmail.com		\N	\N
1020	PT. ristu dharma	13	5171	jln kampus ngurah rai gang jaya raya no 1 denpasar Kota Denpasar Bali	ristu_dharma@yahoo.com		\N	\N
1021	PT. FAJAR UTAMA ENGINEERING	13	5171	Jl. G. Andaksa Tegal Dukuh V No. 16 Kota Denpasar Bali	fuejaya@yahoo.co.id		\N	\N
1022	PT. LOKA SEJAHTERA ABADI	13	5171	Jl. Tjok Agung Tresna No. 87 Denpasar Kota Denpasar Bali	lokasejahteraabadi@gmail.com		\N	\N
1023	PT. PURI SURYA TERANG	13	5171	JL. PALAPA XIII NO. 27 LINGKUNGAN BR. PEGOK KELURAHAN SESETAN Kota Denpasar Bali	ptpurisurya@yahoo.com		\N	\N
1025	PT. ARDIENTHA RAYA	10	3301	Jl. Bharata RT. 004 RW. 006 No. 15 Desa Tritih Wetan Jeruklegi Kab. Cilacap Jawa Tengah	kuswanhassansest@gmail.com	0282-541785	\N	\N
1026	PT. ALFA MEGA ELEKTRO	10	3301	Jl. Urip Sumoharjo RT. 03/10 Kel. Gumilir Kec. Cilacap Utara Kab. Cilacap Kab. Cilacap Jawa Tengah	pt.alfamegaelektro@yahoo.com	085743611700	\N	\N
1027	PT. ERLINDO JAYA UTAMA	10	3301	KOMPLEK RUKO JL. GATOT SUBROTO 173 A GUNUNG SIMPING - CILACAP KEC. CILACAP TENGAH Kab. Cilacap Jawa Tengah	pt.erlindojayautama01@gmail.com	0282-520979	\N	\N
1028	PT. BIMA PUTRA YASA UTAMA	10	3301	JL. GATOT SUBROTO NO. 173 - A GUNUNG SIMPING CILACAP, KEC. CILACAP TENGAH, KAB. CILACAP, JAWA TENGAH Kab. Cilacap Jawa Tengah	pt.bimaputrayasautama@yahoo.co.id	0282-520979	\N	\N
1029	PT. KHARISMA DUA PUTRI	10	3301	Jl. Mataram RT: 06 RW: 01 Pekuncen-Kroya Kab. Cilacap Jawa Tengah	ptkharisma01@gmail.com	-	\N	\N
1030	PT. KARUNIA JAYA ELECTRIKA	10	3301	JL. DR. SUTOMO NO. 55 RT. 02/03 KEL. GUNUNGSIMPING KEC. CILACAP TENGAH KAB. CILACAP Kab. Cilacap Jawa Tengah	kje_clp@gmail.com	-	\N	\N
1031	PT. SAHABAT PRATAMA	10	3301	Jl. Abimanyu No. 188 Rt. 01/06 Kebonmanis Kec. Cilacap Utara Kab. Cilacap Jawa Tengah	amanatun@sp-mps.com	-	\N	\N
1032	PT. SURYA KENCANA RAHAYU	10	3301	JL. PRAMBANAN NO. 246 RT. 11/4 PESANGGRAHAN KEC. KROYA KAB. CILACAP Kab. Cilacap Jawa Tengah	surya_kencana_rahayu@yahoo.com	-	\N	\N
1033	PT. SEMESTA PELITA HARAPAN	10	3301	Jl. Gebe AB No. 24/11 RT. 04 RW. 11 Kel. Gunungsimping Kec. Cilacap Tengah Kab. Cilacap Jawa Tengah	sinarpelitaharapan@gmail.com	-	\N	\N
1034	PT. ENDIRA BUMI PRAKARSA	10	3301	Jl. Protokol Rt.02/03 Desa Kalisabuk Kec. Kesugihan Kab. Cilacap Jawa Tengah	pt.endirabumiprakarsa@yahoo.co.id	-	\N	\N
1035	PT. WINDI KEMBAR ABADI	10	3301	Jl. Lembu Seto RT. 02 RW. 03 Desa Ayamalas Kec. KroyaKab. Cilacap Kab. Cilacap Jawa Tengah	windi_kembar2@yahoo.com	-	\N	\N
1036	PT. ANUGRAH JAYA ELEKTRIKA	10	3301	Perum Gunungsimping Permai Jl. Gebe AC 32 No. 9 RT. 03 RW. 13 Kelurahan Gunungsimping, Kecamatan Cilacap Tengah Kab. Cilacap Jawa Tengah	aje_purwadi@yahoo.co.id	-	\N	\N
1037	PT. GUNA JAYA ELEKTRIK	10	3301	Jl. Madraji No.45 Rt.07/03 Kel. Tambaksari Kec. Kedungreja Kab. Cilacap Jawa Tengah	gunajaya_elektrik@yahoo.co.id	-	\N	\N
1038	PT. KUDI MAS PRIMATAMA	10	3301	Jl. Hansip No.107 Desa Kalikudi Rt.02/08 Kel. Kalikudi Kec. Adipala Kab. Cilacap Jawa Tengah	kudi_mas21@yahoo.com	-	\N	\N
1039	PT. GANDA ANUGRAH PERKASA	10	3301	Jl. Kendeng No.240 Rt.04 Rw.07 Ds. Kroya Kec. Kroya Kab. Cilacap Kab. Cilacap Jawa Tengah	gandaanugrah672@yahoo.co.id	-	\N	\N
1040	PT. YONEK INDOTEKNIK JAYA	10	3301	Jl. Kemiri Rt. 01/02 Ds. Karangkemiri Kec. Maos Kab. Cilacap Jawa Tengah	yonekindoteknikjaya@gmail.com	-	\N	\N
1041	PT. RAHAYU MURNI ELECTRICAL	10	3301	Jl. Kenanga No.24 Rt.01/03 Kel. Kalikudi Kec. Adipala Kab. Cilacap Jawa Tengah	rahayumurni_cv@yahoo.com	-	\N	\N
1042	PT. BRAVO ELEKTRIKA JAYA ABADI	10	3301	Jl. Nakula No.25 Rt. 02/16 Ds.Gumilir Kec. Cilacap Utara Kab. Cilacap Jawa Tengah	pusat@ptaskomelin.co.id	-	\N	\N
1043	PT. EMILIA JAYA ABADI	10	3301	Jl. Lunjar No. 92A Rt.03 Rw.12 Desa Menganti Kec. Kesugihan Kab. Cilacap Kab. Cilacap Jawa Tengah	pt.emiliajayaabadi@yahoo.co.id	-	\N	\N
1044	PT. MAJENANG ELEKTRIK PUTRA	10	3301	Jl. Pramuka No. 33 Rt. 01/12 Ds. Jenang Kec. Majenang Kab. Cilacap Jawa Tengah	pusat.askomelin@gmail.com	-	\N	\N
1045	PT. SINAR TRIJAYA ABADI	10	3301	Jl. Protokol No. 117 Kalisabuk Kel. Kalisabuk Kec. Kesugihan Kab. Cilacap Jawa Tengah	pusat.askomelin@ygmail.com	-	\N	\N
1046	PT. RAFINDRA PUTRA JAYA	10	3301	Jl. Kutilang Tegalreja Cilacap Kel. Tegalreja Kec. Cilacap Selatan Kab. Cilacap Jawa Tengah	pusat.askomelin@gmail.com	-	\N	\N
1047	PT. SURYA INDAH ELECTRICAL	10	3301	Jl. Anggrek Kuripan Kidul, Kesugihan - Cilacap Kel. Kuripan Kidul Kec. Kesugihan Kab. Cilacap Jawa Tengah	pusat.askomelin@gmail.com	-	\N	\N
1048	PT. ARWANI INSAN MANDIRI	10	3301	Jl. RAMA No 23 RT 01 RW 04 Kel. Gumilir Kec. Cilacap Kab. Cilacap Jawa Tengah	pusat.askomelin@gmail.com	-	\N	\N
1049	PT. CATUR PERDANA	10	3304	Jl. Let. Jend. Suprapto No. 5KC Cilacap Sidanegara Kecamatan Cilacap Tengah Kab. Banjarnegara Jawa Tengah	caturperdana_clp@yahoo.com	0282-534536	\N	\N
1050	PT. GELANG SAKTI ABADI	10	3304	DESA GELANG RT 03 RW 05 KEC.RAKIT,KAB.BANJARNEGARA Kab. Banjarnegara Jawa Tengah	gelangsaktiabadi@gmail.com	-	\N	\N
1131	PT. MARSUDI TEKNIK ABADI	10	3311	Dk.Selo Rt.02 Rw.04 Tangkisan Tawangsari, Kab. Sukoharjo Kab. Sukoharjo Jawa Tengah	mteknikabadi@yahoo.com	08122606697	\N	\N
1051	PT. NUR MEISINDO JAYA ABADI	10	3304	JL. KI JAGAPATI 1 RT. 007 RW.005 KEL KUTABANJARNEGARA BANJARNEGARA Kab. Banjarnegara Jawa Tengah	nurmeisindo.ja@gmail.com	-	\N	\N
1052	PT. BUDI SUJUD ABADI	10	3304	Krajan Rt.06/01 Kel. Badamita Kec. Rakit Kab. Banjarnegara Jawa Tengah	budisujudabadi.pt@gmail.com	-	\N	\N
1053	PT. FLEXINDO JAYA MAKMUR	10	3304	Desa Candiwulan Rt.04/01 Kel. Candiwulan Kec. Mandiraja Kab. Banjarnegara Jawa Tengah	pusat@ptaskomelin.co.id	-	\N	\N
1054	PT. PUTRA BANJAR MANDIRI SEJAHTERA	10	3304	JL. KH. AGUS SALIM No.31 RT.02 RW.07 KUTABANJARNEGARA KEL. KUTABANJARNEGARA KEC. BANJARNEGARA Kab. Banjarnegara Jawa Tengah	pbmandirisejahtera@gmail.com	-	\N	\N
1055	PT. SETAR DELTA PUTRA	10	3302	JL. KH. Dimyati No. 43 RT. 01/02 Desa Sudimara Kec. Cilongok Kab. Banyumas Kab. Banyumas Jawa Tengah	setardeltaputra@gmail.com	08122794536	\N	\N
1056	PT. KAROHMAH SINAR ARTHA	10	3302	JL. RAYA BAYAWULUNG KM.2 GEBANGSARI RT.02 RW.02 Kel. GEBANGSARI Kec. TAMBAK Kab. BANYUMAS Kab. Banyumas Jawa Tengah	ptksa2016@gmail.com	081226710220	\N	\N
1057	PT. MUNTINDO JAYA TEKNIKA	10	3302	JL. MARTOSAYOGO NO. 11 KEL. TELUK KEC.PURWOKERTO SELATAN Kab. Banyumas Jawa Tengah	muntindo.pt@gmail.com	0281-634019	\N	\N
1058	PT. INTAN BIRU GEMILANG	10	3302	Jl. KH. Wahid Hasyim No. 33 Purwokerto Karanglasem Kec. Purwokerto Selatan Kab. Banyumas Jawa Tengah	pt.intanbiru@gmail.com	0281 621669	\N	\N
1059	PT. KARISMA JAYA TEKNOLOGI	10	3302	Jl. Kemojing RT. 02 RW. 05 Sidabowa Sidabowa Kec. Sidabowa Kab. Banyumas Jawa Tengah	lskn_pusat@yahoo.co.id	-	\N	\N
1060	PT. LISNA JAYA UTAMA	10	3302	Jl. Raya Tambak No.15 Rt.04/02 Ds. Purwodadi Kel. Banyumas Kec. Tambak Kab. Banyumas Jawa Tengah	cv.lisna_jaya@yahoo.com	-	\N	\N
1061	PT. SURYO CONDRO KARTIKO	10	3302	Perum Griya Satria Mandala Tama Blok 22 No. 27 Rt.04 Rw.06 Ds. Karanglewas Kidul Kec. Karanglewas Kab. Banyumas Jawa Tengah	pusat.askomelin@gmail.com	-	\N	\N
1062	PT. ARDHYA RIZKY LAJU ABADI	10	3325	PERUM PAMARDI GRAHA BLOK B NO.2 JL. PASEKARAN - LEBO RT 02 RW 01PASEKARAN KEC. BATANG Kab. Batang Jawa Tengah	ninuksyaufudin@gmail.com	(0285) 3970918 / 085229059686	\N	\N
1063	PT. SYAMSUL HUDA MANDIRI	10	3325	DESA BABADAN RT. 01 RW. 01 KEC. LIMPUNG Kab. Batang Jawa Tengah	huda.mandiri72@gmail.com	081575228273	\N	\N
1064	PT. PUTRA BATANG MANDIRI	10	3325	DK.Sikebo RT. 02 RW. 04 Kel. Limpung Kec. Limpung Kab. Batang Kab. Batang Jawa Tengah	lskn_pusat@yahoo.co.id	-	\N	\N
1065	PT. SURYA GEMILANG INDOPERKASA	10	3325	Jl. Gajahmada Gg. Branjangan No. 22 Batang Proyonanggan Tengah Kab. Batang Jawa Tengah	suryagemilangindoperkasa@yahoo.co.id	-	\N	\N
1066	PT. THREE DIAN PUTRA	10	3316	Jl.Agil Kusumodiyo Gg. 1 No. 44 Kelurahan Kauman Rt 003 rw 002 Kecamatan Blora Kabupaten Blora Kab. Blora Jawa Tengah	pt.threedianputra@gmail.com	-	\N	\N
1067	PT. SURYA MUDA	10	3316	KOMPLEK BRI No.2 RT.005 RW.015 KELURAHAN BALUN KECAMATAN CEPU Kab. Blora Jawa Tengah	suryamuda_cepu@yahoo.co.id	-	\N	\N
1068	PT. SURYA DIMAS AJENG SAHPUTRA	10	3316	JL. DIPONEGORO Gg. 7 No. 11A Kel. CEPU Kec. CEPU Blora JAWA TENGAH Kab. Blora Jawa Tengah	pt.suryadimasajengsahputra@gmail.com	-	\N	\N
1069	PT. SANTOSA BUDI JAYA	10	3309	JL RAYA BOYOLALI SELO SUROWEDANAN RT 2 RW 1 PULISEN BOYOLALI Kab. Boyolali Jawa Tengah	santosabudijaya_pt@yahoo.com	0276-3293014	\N	\N
1070	PT. NUGRAHA TATA ANUGRAH JAYA	10	3309	Jl. Raya Solo - Semarang Km. 22 Barakrejo RT.02 RW.01 Randusari Teras Boyolali Kab. Boyolali Jawa Tengah	ptnatajaya16@gmail.com	0276-327777	\N	\N
1071	PT. GESIT PUTRA SANJAYA	10	3309	Dk. Krajan RT. 016 RW. 04 Kel. Karang,ojo, Kec. Klego Kab. Boyolali Kab. Boyolali Jawa Tengah	gesitputrasanjaya@gmail.com	08103250440271	\N	\N
1072	PT. MEGA ELEKTRIKAL ENERGI ABADI	10	3309	DK. SURO RT. 02/08 DS. TAMBAK. KEC. MOJOSONGO Kab. Boyolali Jawa Tengah	pt.mega68@yahoo.co.id	-	\N	\N
1073	PT. MEGA ELEKTRIKAL MEGA ABADI	10	3309	DK. SURO RT. 02 RW. 08 DS. TAMBAK KEC. MOJOSONGO Kab. Boyolali Jawa Tengah	pt.mega68@yahoo.co.id	-	\N	\N
1074	PT. INDOJAYA SAMPURNA ABADI	10	3329	Wanatirta RT. 13 RW. 01 Kel. Wanatirta KEc. Paguyangan Kab. Brebes Jawa Tengah	lskn_pusat@yahoo.co.id	-	\N	\N
1075	CV. INDOJAYA	10	3329	Wanatirta RT. 13 RW. 01 Kel. Wanatirata Kec. Paguyangan Kab.Brebes - Jawa Tengah Kab. Brebes Jawa Tengah	lskn_pusat@yahoo.co.id	-	\N	\N
1076	CV. TUNAS JATI MAS	10	3329	DUSUN SIGEMPOL RT.04, RW.03 DS. RANDUSANGAN KULON KEC. BREBES Kab. Brebes Jawa Tengah	tunasjatimas@gmail.com	-	\N	\N
1077	PT. Manunggal GLobal Nusantara	10	3321	Jalan Teuku Umar No 75 Kel. Mangunjiwan Kec. Demak Kab. Demak Jawa Tengah	mgn_manunggal.globalnusantara@yahoo.com	0291 686199	\N	\N
1078	PT. BANGUN SAEINDO UTAMA	10	3321	Jalan Rajawali Desa Gajah Rt. 02 Rw. 03 Kec. Gajah Kab. Demak 59581 Kab. Demak Jawa Tengah	bangunsaeindoutamapt@gmail.com	-	\N	\N
1079	PT. IZZA ELEKTRIKA JAYA	10	3321	JL. JOGOLOYO, NO. 25A, KEC. WONOSALAM, KAB. DEMAK 59571 Kab. Demak Jawa Tengah	pt.izzaelektrikajaya@yahoo.com	-	\N	\N
1080	PT. PUTRA WAHYU ELEKTRIK	10	3315	JL TENTARA PELAJAR NO 45A KALONGAN PURWODADI Kab. Grobogan Jawa Tengah	anggoropwd@gmail.com	08122932040	\N	\N
1081	PT. PUTRA MANDIRI JEPARA	10	3320	Desa Purwogondo RT. 007 RW. 001 Kec. Kalinyamatan Kab. Jepara Jawa Tengah	irkh4m@gmail.com	0291-755435	\N	\N
1082	PT. SALUYU JATIDIRI	10	3320	JL. RAYA BANGSRI NO 32 BANGSRI - JEPARA Kab. Jepara Jawa Tengah	saluyu_djatidiri@yahoo.co.id	-	\N	\N
1083	PT. SAPTO ARGO PERKASA	10	3320	DS. SEKURO RT 19/4 KEC. MLONGGO KAB. JEPARA JAWA TENGAH Kab. Jepara Jawa Tengah	saptoargo.pt@yahoo.com	-	\N	\N
1084	PT. CAHAYA WAHANA MANDIRI	10	3313	JL. RINGROAD PALUR RT.01 RW 02 SROYO JATEN KARANGANYAR Kab. Karanganyar Jawa Tengah	cahayawahana_mandiri@yahoo.co.id	0271 8203078	\N	\N
1085	PT. FAJARINDO WAHYU UTAMA	10	3313	Punukan RT. 04 RW. 15 Ngadiluwih Matesih Karanganyar Kab. Karanganyar Jawa Tengah	fajarindo_wahyutama@yahoo.co.id	-	\N	\N
1086	PT. NUSA MENTARI INDONESIA	10	3313	JL.KLEBEN RT.01/RW.08, KEL.GEDONGAN KEC.COLOMADU KAB.KARANGANYAR Kab. Karanganyar Jawa Tengah	nusamentari@gmail.com	-	\N	\N
1087	PT. CAHAYA WAHANA MANDIRI	10	3305	JL. RINGROAD PALUR RT.01 RW 02 SROYO JATEN KARANGANYAR Kab. Karanganyar Jawa Tengah	cahayawahana_mandiri@yahoo.co.id	0271 8203078	\N	\N
1088	PT. FAJARINDO WAHYU UTAMA	10	3305	Punukan RT. 04 RW. 15 Ngadiluwih Matesih Karanganyar Kab. Karanganyar Jawa Tengah	fajarindo_wahyutama@yahoo.co.id	-	\N	\N
1089	PT. NUSA MENTARI INDONESIA	10	3305	JL.KLEBEN RT.01/RW.08, KEL.GEDONGAN KEC.COLOMADU KAB.KARANGANYAR Kab. Karanganyar Jawa Tengah	nusamentari@gmail.com	-	\N	\N
1090	PT. ENERGI SEJATI TEKNIK UTAMA	10	3310	Dukuh Karangsari RT.19 RW.09 Kel. Pandeyan Kec. Jatinom, Kab. Klaten Kab. Klaten Jawa Tengah	ighandoyo@gmail.com	081226066691	\N	\N
1091	PT. Kusuma Petir Biru	10	3310	Dk. Mranggen RT. 01 RW. 04, Krecek, Kec. Delanggu, Kab. Klaten Provinsi Jawa Tengah Kab. Klaten Jawa Tengah	kpetirbiru@yahoo.co.id	0271 723077 - 652211	\N	\N
1092	PT. PULUNG ABADI DUTA LISTRIKINDO MITRA LANGGENG	10	3310	KARANG RT.17 RW.04 KARANG DELANGGU KLATEN JAWA TENGAH Kab. Klaten Jawa Tengah	ptpulung17@gmail.com	0272-554611	\N	\N
1093	PT. MITRA ABADI ANDALAN JAYA UTAMA	10	3310	GEMBLEGAN RT.01 RW. 08 GEMBLEGAN KALIKOTES KLATEN Kab. Klaten Jawa Tengah	pt.maju.klaten@gmail.com	-	\N	\N
1094	PT. MUKJIZAT JAYA MANDIRI CABANG KUDUS	10	3319	DESA WONOSOCO RT 02 RW 01 Kab. Kudus Jawa Tengah	darsono_mcr@yahoo.co.id	085258597211	\N	\N
1095	PT. SINAR JAYA KUDUS	10	3319	Desa Cendono Rt. 03 Rw. 09 Kec. Dawe Kab. Kudus Jawa Tengah	sinarjaya.kudus@gmail.com	0291-442759	\N	\N
1096	PT. LANGGENG REJEKI	10	3319	Jl. Ganesha IV No. 118 Kudus Kel. Purwosari Kec. Kudus Kab. Kudus Jawa Tengah	Langgeng_rejeki@ymail.com	0291-430004	\N	\N
1097	PT. RIZKI ADHI PERKASA	10	3319	Desa Pasuruhan Lor RT.01 RW. XI Kec. Jati Kudus Kab. Kudus Jawa Tengah	rizkiadhiperkasapt@yahoo.co.id	08122932066	\N	\N
1098	PT. HARTA PUTRA KARYA	10	3319	JL. KALIYITNO KULON RT. 04/05 KELURAHAN SAMIREJO KEC. DAWE KAB. KUDUS Kab. Kudus Jawa Tengah	pt.hartaputrakarya@yahoo.co.id	0291 4101584	\N	\N
1099	PT. TIRTA NUGROHO JATI	10	3319	JL. AKBP R. AGIL KUSUMADYA GG. II RT 01 RW 01 NO. 34 JATIKULON KEC. JATI KAB. KUDUS Kab. Kudus Jawa Tengah	tinuja@yahoo.com	0291 - 443731	\N	\N
1100	PT. HARTA JAYA	10	3319	JL. KALIYITNO KULON NO. 81 RT.04 RW. 02, DESA SAMIREJO, KECAMATAN DAWE, KABUPATEN KUDUS, JAWA TENGAH Kab. Kudus Jawa Tengah	pt.harta_jaya@yahoo.co.id	(0291)-4101582	\N	\N
1101	PT. ARIKON JAYA KARYA	10	3319	KARANG WETAN RT. 04/07 KEL. BULUNG KULON KEC. JEKULO Kab. Kudus Jawa Tengah	pt.ajkudus@gmail.com	-	\N	\N
1102	PT. PUTRA PERKASA MURIA	10	3319	Ds Puyoh RT 4 RW 1 Kec Dawe Kab Kudus Kab. Kudus Jawa Tengah	pt.putraperkasamuria@gmail.com	-	\N	\N
1103	PT. SEKAWAN ADI LUHUR	10	3319	Jl. Jatikulon Gang III No.141 rt. 05 rw. 01 desa jatikulon kec. jati Kab. Kudus Jawa Tengah	sekawanadiluhur@gmail.com	-	\N	\N
1104	PT. ANDROMEDA JAYA GEMILANG	10	3308	Dsn. Gembung RT. 02/01 Soroyudan Kec. Tegalrejo Kab. Magelang Jawa Tengah	andromedajayabtl@yahoo.com	08122746623	\N	\N
1105	PT. KENCANA MULYA SARI	10	3308	Jl. Magelang Purworejo KM. 09 Banjarnegoro Mertoyudan Magelang Kab. Magelang Jawa Tengah	kencanamulyasarimgl@yahoo.co.id	0293-5560999	\N	\N
1106	PT. SURYA INDAH ABADI ELEKTRIK	10	3308	Perum Pondok Rejo Asri Jl. Indrakila II No 210 ds. Danurejo Mertoyudan Kab. Magelang Jawa Tengah	suryaindah.ae@gmail.com	-	\N	\N
1107	PT. ADHINATA BRAJAMUKTI PERKASA	10	3308	Jl. Sunan Ampel III RT. 03 RW. 02 Kel. Jurangombo Selatan Kec. Magelang Selatan Kab. Magelang Jawa Tengah	lskn_pusat@yahoo.co.id	-	\N	\N
1108	PT. ADHIMUKTI ANTASENA	10	3308	Sampangan RT. 02 RW. 03 Kel. Jurangomboh Utara Kec. Magelang Kab. Magelang Jawa Tengah	lskn_pusat@yahoo.co.id	-	\N	\N
1109	PT. KARYA PUTRA KEMBAR	10	3308	JL. Soekarno Hatta No.46 Sawitan Kel Sawitan Kec. Mungkid Kota Magelang Kab. Magelang Jawa Tengah	ptkaryaputra49@yahoo.com	-	\N	\N
1110	PT. MURIA CAHAYA SEJATI	10	3318	DS SUKOAGUNG RT 007/002 KEL SUKOAGUNG KEC BATANGAN KAB PATI Kab. Pati Jawa Tengah	caesarnet00@gmail.com	085328220999	\N	\N
1111	PT. WANA TIRTA JAYA	10	3318	DS. LANGGENHARJO RT 10 RW 02 KEC. JUWANA KAB. PATI Kab. Pati Jawa Tengah	ariirfan21@yahoo.co.id	081390566000	\N	\N
1112	PT. SUMBER SAE SATU	10	3318	PERUM PURI TAMAN GADING RT. 004 RW. 001 KELURAHAN PURI KECAMATAN PATI Kab. Pati Jawa Tengah	joss6006@gmail.com	081229260607	\N	\N
1113	PT. BRAWIJAYA PESANTENAN PATI	10	3318	DESA NGEMPLAK LOR RT. 02/01 KEC. MARGOYOSO KAB. PATI Kab. Pati Jawa Tengah	bpppt08@gmail.com	-	\N	\N
1114	PT. MEDYA TEKNIK MANUNGGALDesa Gandarum RT.02 RW.04 Kajen Kab. Pekalongan Kab. Pekalongan Jawa Tengahmedisubagyo@yahoo.com08157526645909 Maret 2022kecil	10	3326				\N	\N
1115	PT. HASSCO JAYA UTAMA	10	3327	Jl, Panjunan Selatan No. 12 Panjunan Kec. Petarukan Kab. Pemalang Jawa Tengah	hassco_ju@yahoo.com	0285-4473960	\N	\N
1116	PT. KUSUMA ELEKTRIK BOJONGBATA	10	3327	Jl. Samosir Barat II N0. 134 Rt. 01/Rw. 014 Kab. Pemalang Kab. Pemalang Jawa Tengah	pt.kusumaelektrik@gmail.com	-	\N	\N
1117	PT. DWI PERKASA PUTRI	10	3303	Ds, Mangunegara Rt 03/01 Mrebet Purbalingga Kab. Purbalingga Jawa Tengah	pt.dwiperkasaputri@gmail.com	085869427376	\N	\N
1118	PT. BANGKIT JAYA PERWIRA	10	3303	JL/LETKOL ISDIMAN NO.46 KELURAHAN PURBALINGGA WETAN Kab. Purbalingga Jawa Tengah	bangkitjayaperwira_46@yahoo.com	-	\N	\N
1119	PT. SINAR KAMAL NIAGA	10	3303	DESA BANDINGAN RT. 13/06 KEL. BANDINGAN KEC. KEJOBONG Kab. Purbalingga Jawa Tengah	sinarkamalniaga@gmail.com	-	\N	\N
1120	PT. DAFFA JAYA UTAMA	10	3317	Jl. Muria No E. 38 RT 02 RW 05 Kabongan Kidul Kec. Rembang Kab. Rembang Jawa Tengah Kab. Rembang Jawa Tengah	daffa.jayautama@gmail.com	02956997030/081326669918	\N	\N
1121	PT. SISTEMDO KHARISMA	10	3322	Jl. Raya Salatiga - Solo KM. 04 Bener Kec. Tengaran Kab. Semarang Jawa Tengah	sistemdo@sistemdo.co.id	08562716024	\N	\N
1122	PT. POWERINDO CAHAYA BUANA	10	3322	JL. BOUGENVILE 1 KUPANGPETE RT. 06 RW. 02 KELURAHAN KUPANG KECAMATAN AMBARAWA Kab. Semarang Jawa Tengah	powerindocahayabuana@gmail.com	081326082523	\N	\N
1123	PT. CITRA PESONA TEHNIK	10	3322	JL. Fatmawati No.22A RT.1 RW. 3 Desa Tuntang Kec. Tuntang, Kab, Semarang, Jawa Tengah Kab. Semarang Jawa Tengah	citra_pt@yahoo.com	0298(3405579)/085713527119	\N	\N
1124	PT. BERKAH JAYA TEKNIK	10	3322	JL. TENTARA PELAJAR PERUM SANG RATU RESIDENCE B-1 07/02 GEDANGANAK UNGARAN TIMUR SEMARANG Kab. Semarang Jawa Tengah	pt.berkahjayateknik@gmail.com	085741582556	\N	\N
1125	PT. PRABOWO PERKASA LISTRIKINDO	10	3322	DALEMAN TUNTANG RT.03 RW. 04 DESA TUNTANG KEC.TUNTANG Kab. Semarang Jawa Tengah	prabowoperkasa@yahoo.com	085225570001	\N	\N
1126	PT. BAYU TEHNIK WICAKSONO	10	3322	JL. MERPATI I/6 BANDARJO UNGARAN BARAT Kab. Semarang Jawa Tengah	bayutehnik65@gmail.com	08122855540	\N	\N
1127	PT. SINAR TERANG GEMILANG	10	3322	JL. Banyubiru Raya Dsn.Demakan Rt.02/11 Banyubiru Kab.Semarang Jawa Tengah 50664 Kab. Semarang Jawa Tengah	pt.sinarteranggemilang@gmail.com	-	\N	\N
1128	PT. ANTERA JAYA ABADI	10	3322	JL. MGR SOEGIYOPRANOTO No. 69 GARUNG, KEL NGAMPIN, KEC AMBARAWA Kab. Semarang Jawa Tengah	anterajadi@gmail.com	-	\N	\N
1129	PT. MAJUKARYA ALAM SEMESTA	10	3322	RT 01 RW 01 KELURAHAN PATEMON KECAMATAN GUNUNG PATI Kab. Semarang Jawa Tengah	massmg@yahoo.com	-	\N	\N
1130	PT. NGUDI MULYO UTOMO	10	3322	Jl.Prambanan No. 27 RT 002 RW 002 Kelurahan Candirejo Kecamatan Ungaran Barat Kab. Semarang Jawa Tengah	mulyongudi@yahoo.co.id	-	\N	\N
1132	PT. Sinar Abadi Sukoharjo	10	3311	Dk. Jatimalang Rt. 01 Rw. 05 Kateguhan Tawangsari Kabupaten Sukoharjo Kab. Sukoharjo Jawa Tengah	pt.sinarabadisukoharjo@gmail.com	(0272) 881092	\N	\N
1133	PT. CAHAYA ALAM KUSUMA	10	3311	TEGALAN RT.02 RW.04 KELURAHAN KATEGUHAN KECAMATAN TAWANGSARI Kab. Sukoharjo Jawa Tengah	pt.cahayaalamkusuma@gmail.com	-	\N	\N
1134	PT. REXA CIPTA MANDIRI	10	3328	Jl. Raya Kalikangkung No. 7 Ds. Kalikangkung Rt. 04/Rw. 02 Kec. Pangkah Kab. Tegal Jawa Tengah	rexac1707@gmail.com	0283 4542026 - 085101301707	\N	\N
1135	PT. RIZQI CAHYO NUGROHO	10	3328	Jl. PEMARON 111 RT.016 RW.004 HARJOSARI LOR Kec. ADIWERNA Kab. TEGAL Kab. Tegal Jawa Tengah	cahyonugroho87@yahoo.com	0283-443820	\N	\N
1136	PT. REXA TEKNIKA MANDIRI	10	3328	JL. RAYA KALIKANGKUNG NO. 07 RT.04 RW. 02 KEC. PANGKAH KAB. TEGAL Kab. Tegal Jawa Tengah	rexat7716@gmail.com	0283 - 4542026	\N	\N
1137	PT. AGRO TEKNIK MULTIGUNA	10	3328	JL. PALA BARAT I NO. 10 MEJASEM BARAT KEC. KRAMAT KAB. TEGAL JAWA TENGAH Kab. Tegal Jawa Tengah	agroperintis@ymail.com	0283 6144131	\N	\N
1138	PT. MUMTAZ JAYA ABADI	10	3328	Jl. Kesambi No. 7 Ds. Kramat Rt. 01/Rw. 01 Kec. Kramat - Kab. Tegal Kab. Tegal Jawa Tengah	mumtazjayaabadi@gmail.com	-	\N	\N
1139	PT. PUTRA JAYA ELEKTRIK	10	3323	WARUNGSARI RT 001 RW 002 MANDISARI PARAKAN TEMANGGUNG Kab. Temanggung Jawa Tengah	putrajaya.elektrik@yahoo.com	(0293) 5921657	\N	\N
1140	PT. RAJAWALI MAS SINDORO	10	3307	Mendolo RT.01 RW.03 Kelurahan Bumireso Kab. Wonosobo Jawa Tengah	rajawalimas907@gmail.com	-	\N	\N
1141	PT. WONGS SOBO PUTRA	10	3307	KRAKAL DAWUNG RT 06 RW 09 KERTEK Kab. Wonosobo Jawa Tengah	ptwongssoboputra@gmail.com	-	\N	\N
1142	PT. DUTAKARYA PERSADA INDONESIA	10	3371	Jl. Pahlawan 54 RT.001 RW.005 Kelurahan Magelang , Magelang Jawa Tengah Kota Magelang Jawa Tengah	pt_dkpi@yahoo.com	(0293) 365110 / 365109	\N	\N
1143	PT. SENTOSA MAKMUR SEJAHTERA INDONESIA	10	3375	JL. HOS COKROAMINOTO NO.110 RT. 03 RW. 14 KEL. NOYONTAANSARI KEC. PEKALONGAN TIMUR KAB. PEKALONGAN JAWA TENGAH Kota Pekalongan Jawa Tengah	pt_smsi@ymail.com	0285427227	\N	\N
1144	PT. ABATA BERKAH JAYA	10	3375	LLANDUNGSARI JL. KIHAJAR DEWANTORO GG. 22 NO. 95 KEL. NOYONTAAN SARI PEKALONGAN Kota Pekalongan Jawa Tengah	abataberkahjaya@gmail.com	-	\N	\N
1145	PT. MANDIRI ABADI JAYA INDONESIA	10	3373	JL. Raya Salatiga � Solo Km.06 Prokimad Wedilelo Rt.39 / Rw.10, Karangduren, Tengaran, Kab.Semarang Kota Salatiga Jawa Tengah	project.ptmaji@gmail.com	( 0298 )3432042	\N	\N
1146	PT. GALUH BETUAH UTAMA	10	3373	JL. TEGALREJO RAYA NO. 26 SALATIGA Kota Salatiga Jawa Tengah	galuhbetuah99@gmail.com	0298 - 312145	\N	\N
1147	PT. SAKHA FAJR UTAMA	10	3373	JL. SUROPATI NO.15A RT.04 RW.05 TOGATE MANGUNSARI SALATIGA 50721 Kota Salatiga Jawa Tengah	ptsakha.fu2015@gmail.com	085878839000/08122813626	\N	\N
1148	PT. JATI MAJU BERSAMA	10	3374	JL. JATINGALEH II No.2 NGESREP BANYUMANIK KOTA SEMARANG Kota Semarang Jawa Tengah	jatimajubersama@gmail.com	08990369835	\N	\N
1149	PT. Mei Karya	10	3374	Jl. Abdulrahman Saleh No. 67 Kota Semarang Jawa Tengah	info@meikarya.com	024 - 7615874/08112991231	\N	\N
1150	PT. DUTA SINAR ABADI	10	3374	Jl. MT. Haryono No. 730 Karangturi Kec. Semarang Timur Kota Semarang Kota Semarang Jawa Tengah	sinarabadi730@yahoo.co.id	024-3549730	\N	\N
1151	PT. ARMARIN JAYA MANDIRI	10	3374	JL. MERBABU 3 No.78 RT.04 RW.07 Kel. PDANGSARI Kec. BANYUMANIK Kota SEMARANG, JAWA TENGAH Kota Semarang Jawa Tengah	armarin_jm@yahoo.com	081325625988	\N	\N
1152	PT. MITRA KARYA ELEKTRIK	10	3374	JL. KANFER UTARA III NO.125 RT.005 RW.005 KEL.PEDALANGAN KEC.BANYUMANIK KOTA SEMARANG JAWA TENGAH Kota Semarang Jawa Tengah	mkelektrik2015@gmail.com	085640459234	\N	\N
1153	PT. ANUGERAH LESTARI SUKSES	10	3374	JL. CINDE RAYA NO. 34 Kota Semarang Jawa Tengah	anugerahlestarisukss@gmail.com	081325288384	\N	\N
1154	PT. PURWITA SARI	10	3374	JL. KARANGGAWANG LAMA NO. 7 SEMARANG Kota Semarang Jawa Tengah	pt_purwitasari@yahoo.com	024-6719692/0817293936	\N	\N
1155	PT. CHRISTI MANUNGGAL	10	3374	JL. PLAMONGAN ABADI NO 123 B, PEDURUNGAN KIDUL SEMARANG . KODE POS 50192 Kota Semarang Jawa Tengah	christi_manunggal@yahoo.com	024 - 6710831 / -24 - 6724293	\N	\N
1156	PT. WIJATI AJI	10	3374	JL. LET, JEND. S PARMAN NO.52 RT.008 RW.004, KELURAHAN BENDUNGAN, KECAMATAN GAJAH MUNGKUR Kota Semarang Jawa Tengah	wijatiaji@wijatiaji.co.id	+62248441546	\N	\N
1157	PT. THANIA INDO JAYA	10	3374	JL. UNTUNG SUROPATI No.18 RT.01 RW.XI KALIPANCUR - NGALIYAN Kota Semarang Jawa Tengah	pt_thania@yahoo.co.id	024 76636399	\N	\N
1158	PT. CAHAYA PUTRI UTAMA	10	3374	JL Gebanganom Raya Rt 01 / Rw 08 Kel Genuksari Kec Genuk Kota Semarang Jawa Tengah	cahayaputriutama2015@gmail.com	024-67580131	\N	\N
1159	PT. PERINTIS UTAMA	10	3374	Jl. Kledung 353 Kel. Karangturi Kec. Semarang Timur Kota Semarang Kota Semarang Jawa Tengah	perintisutama@yahoo.com	(024)8315550	\N	\N
1160	PT. ANUGRAH MULIA ABADI	10	3374	Ruko Tugu Mas II Kav. No. 12 LT 2 Jl. Walaisongo Rt.01 Rw. 01 Kelurahan Randugarut Kecamatan Tugu Kota Semarang Jawa Tengah	anugrahmakmur_abadi@yahoo.co.id	-	\N	\N
1161	PT. JAYA SAKTI ELEKTRIK	10	3374	JL. SENDANG UTARA III NO. 9 RT. 2 RW. 7 KEL. GEMAH KEC. PEDURUNGAN Kota Semarang Jawa Tengah	pt.jayasaktielektrik@gmail.com	-	\N	\N
1162	PT. MANUNGGAL ABADI SUKSES	10	3374	Jl. Borobudur Utara Raya No. 34 Kel. Manyaran Semarang Barat Kota Semarang Jawa Tengah	mitraabadisejahtera@mas.com	-	\N	\N
1163	PT. SIMETRIK MAJU JAYA	10	3374	JL.SIKLUWUNG ASRI NO.9 RT.01/RW.01 KELURAHAN TANDANG KECAMATAN TEMBALANG Kota Semarang Jawa Tengah	simetrikmajujaya@gmail.com	-	\N	\N
1164	PT. ENERGI PUTRA NUSANTARA JAYA	10	3374	JL. LEBDOSARI II NO. 112 KALIBANTENG KULON SEMARANG Kota Semarang Jawa Tengah	pt_epanusa@yahoo.co.id	-	\N	\N
1165	PT. SARANA BANGUNINDO SEJAHTERA	10	3374	JL. WR SUPRATMAN NO. 26 SEMARANG Kota Semarang Jawa Tengah	ptsbs_smg@yahoo.com	-	\N	\N
1166	PT. TIRTHA BHUWANA ASRI	10	3374	JL. WR SUPRATMAN NO. 69 SEMARANG Kota Semarang Jawa Tengah	tbasmg@yahoo.co.id	-	\N	\N
1167	PT. Nunas Cipta Agung	10	3374	Jl.Tambak Mas VII/CM 1 Semarang Kota Semarang Jawa Tengah	nunascipta@yahoo.co.id	-	\N	\N
1168	PT. WIJAYA PUTRA KITA	10	3374	Jl. Ngalian Indah III Blok O No. 3 RT 02 RW 07 Kelurahan Ngaliyan, Kecamatan Ngaliyan Kota Semarang Jawa Tengah	wijayaputrakita@gmail.com	-	\N	\N
1169	PT. KARYA MITRA NUGRAHA	10	3374	Jl. Veteran No 39 Lempongsari Kecamatan Gajah Mungkur Kota Semarang Jawa Tengah	ptkmnsem@indosat.net.id	-	\N	\N
1170	PT. Manira Arta Rama	10	3372	Jl. Mataram No. 11 Gayamsari RT. 01 RW. XI, Banyuanyar Kec. Banjarsari Kota Surakarta Kota Surakarta Jawa Tengah	ptmanira@yahoo.com	0271 652211 - 723077	\N	\N
1171	PT. SEKAWAN KURNIA UTAMA	10	3372	DUKUHAN NAYU, JL KELUD SELATAN IV, RT 06/RW 15, KADIPIRO, BANJARSARI, SURAKARTA Kota Surakarta Jawa Tengah	pt.sekawankurniautama@yahoo.com	0271-8501390	\N	\N
1172	PT. SEKAWAN TRIYASA SOLUSINDO	10	3372	NGADISONO RT 006 RW 014 KADIPIRO BANJARSARI SURAKARTA Kota Surakarta Jawa Tengah	solusindotriyasa@gmail.com	0271 858233, 08122233182	\N	\N
1173	PT. BANYU BENING	10	3372	JL.TANJUNG NO. 40 KARANGASEM LAWEYAN SURAKARTA Kota Surakarta Jawa Tengah	banyubeningsolo@gmail.com	0271 739815	\N	\N
1174	PT. MULYO AGUNG SOLO	10	3372	Jl. Gambirsari, Rt 06/XIII, Kadipiro, Surakarta ,Provinsi Jawa Tengah. Kota Surakarta Jawa Tengah	mulyoagungsolo@yahoo.co.id	0271854637	\N	\N
1175	PT. CIPTA TEHNIKA TAMA	10	3372	JL. PAJAJARAN BARAT I NO. 10 SUMBER BANJARSARI SURAKARTA Kota Surakarta Jawa Tengah	cipta_tehnik@yahoo.com	-	\N	\N
1176	PT. CIPTA KARSA SEMESTA	10	3372	JL. PAJAJARAN BARAT I NO. 8 RT 02 RW XVII KEL. SUMBER KEC. BANJARSARI KOTA SURAKARTA Kota Surakarta Jawa Tengah	btl.cikata@yahoo.com	-	\N	\N
1177	PT. HENDRA JAYA SOLO	10	3372	BIBIS BARU RT. 05/23 KEL. NUSUKAN KEC. BANJARSARI KOTA SURAKARTA Kota Surakarta Jawa Tengah	pthendrajaya@yahoo.com	-	\N	\N
1178	PT. ALDO JAYA PRAKASA	10	3376	Jl. Perintis Kemerdekaan No 85 Tegal Kota Tegal Jawa Tengah	aldo.jayaprakasa@yahoo.co.id	0283 358759 / 08156527956	\N	\N
1179	PT. ANDA JAYA PRADANA	10	3376	Jl. Jendral Sudirman No 44 Tegal Kota Tegal Jawa Tengah	aldo.jayaprakasa@yahoo.co.id	0283 356920 / 08156527956	\N	\N
1180	PT. MITRA USAHA TEHNIK	10	3376	Jl. RAMBUTAN 4 NO. 13 KEL. KRATON - KEC. TEGAL BARAT - KOTA TEGAL Kota Tegal Jawa Tengah	ptmitrausahatehnik2018@gmail.com	08122669330	\N	\N
1181	PT. CHITRAKARSA INTI PERSADA	10	3376	Jl. Slamet No. 57 Kel. Panggung Kec. Tegal Timur - Kota Tegal Kota Tegal Jawa Tengah	chitrakarsa@yahoo.co.id	-	\N	\N
1806	CV. Bupolo Mandiri	16	8109	Desa Labuang, Kec. Namrole, Kab. Buru Selatan, Provinsi Maluku Kab. Buru Selatan Maluku	bupolomandiri@yahoo.co.id	-	\N	\N
1807	PT. SIMPATIK CAHAYA ABADI	16	8171	KOMPLEK BTN KEBUN CENGKEH RT 004 RW 15 KEL BATU MERAH KEC SIRIMAU KOTA AMBON Kota Ambon Maluku	sca_amq@yahoo.co.id	0911-3832626/081343207880	\N	\N
1808	PT. SINAR GLORI DATIER	16	8171	Jl. Rijali No. 20 Rt 001 Rw 004 Kel. Karang Panjang Kec. Sirimau Kota Ambon - Maluku Kota Ambon Maluku	pt.sinargloridatier@gmail.com	-	\N	\N
1861	PT. ANTAR KITA GROUP	15	5307	JLN.RW MONGINSIDI RT.002/RW.001Kel.RUKUN LIMA Kec.ENDE SELATAN Kab. Alor Nusa Tenggara Timur	pt.antarkita01@gmail.com	-	\N	\N
1862	CV. ATHESIA	15	5311	JL. UDAYANA RT. 005 / RW. 001 KELURAHAN ONEKORE KECAMATAN ENDE TENGAH Kab. Ende Nusa Tenggara Timur	athesiaprima16@gmail.com	081380623153	\N	\N
1863	CV. PERSADA TEKNIK	15	5311	JL.MELATI NO.88 RT 004/RW 005 KELURAHAN PAUPIRE,KECAMATAN ENDE TENGAH KABUPATEN ENDE,NUSA TENGGARA TIMUR Kab. Ende Nusa Tenggara Timur	cvpersada_teknik17@gmail.com	0381-2500687	\N	\N
1864	PT. ARNOLDUS NUSA INDAH	15	5311	JALAN KATEDRAL NO. O5; RT/RW:03/06; KEL. POTULANDO; KEC. ENDE TENGAH; KABUPATEN ENDE; PROVINSI NUSA TENGGARA TIMUR Kab. Ende Nusa Tenggara Timur	arnoldusnusaindah@gmail.com	(0381) 21892	\N	\N
1865	PT. CRISNA PRATAMA TEKNIK	15	5311	JL.UDAYANA NO.107 RT.021 RW.006 KELURAHAN ENDE TENGAH Kab. Ende Nusa Tenggara Timur	cristamapt@gmail.com	082247849137	\N	\N
1866	PT. MADANI TAMA ENERGI	15	5311	JLN. Masjid Agung RT.024/ RW.008 kelurahan Kelimutu Kec. ende Tengah Kab. Ende Nusa Tenggara Timur	madanitama@yahoo.com	082144353637	\N	\N
1867	PT. BUNGA TONDA ENDE	15	5311	JLN.PERWIRA RT.005/RW..003 No.10 Kel.Kota Raja Kec.Ende Utara Kab. Ende Nusa Tenggara Timur	pt.bungatondaende01@gmail.com	0381-21834	\N	\N
1868	PT. TELAGA ENDE	15	5311	Jl. Pahlawan No. 15 RT.001 RW.004 Kelurahan Kota Raja Kecamatan Ende Utara Kab. Ende Nusa Tenggara Timur	telagaende@yahoo.co.id	-	\N	\N
1869	CV. KARYA DAMAI	15	5309	Waiwerang RT 014 RW 005 Kel. Waiwerang Kota Kec. Adonara Timur, Flores Timur, Nusa Tenggara Timur Kab. Flores Timur Nusa Tenggara Timur	cvkaryadamai@gmail.com	0813 37216237	\N	\N
1870	PT. AKELOHE MULYA INDAH	15	5308	Lamahora Timur RT/RW. 038/008 Kel. Lewoleba Kec. Nubatukan Kab. Lembata Nusa Tenggara Timur	avrilialangoday@gmail.com	081238115871	\N	\N
1871	CV. Lades Teknik	15	5308	Jl.Woloklaus,Kelurahan Lewoleba Utara, Kecamatan Nubatukan, Kabupaten Lembata, Nusa Tenggara Timur Kab. Lembata Nusa Tenggara Timur	Ladesteknik@gmail.com	081337100038	\N	\N
1872	CV. CV. MAHARANI	15	5308	Kota Baru, Jalan Tujuh Maret, RT.012 / RW.005, Kelurahan Lewoleba Selatan, Kecamatan Nubatukan, Kabupaten Lembata Kab. Lembata Nusa Tenggara Timur	romanuspayongwadan92@gmail.com	082118224916	\N	\N
1873	PT. ERICSON RANGGA TEKNIK	15	5313	JL NASUTION TENDA KEL TENDA KEC LANGKE REMBONG KAB MANGGARAI FLORES NTT Kab. Manggarai Nusa Tenggara Timur	ptericsonteknik@gmail.com	081236878380	\N	\N
1874	PT. DANI FATO ELEKTRIK	15	5315	JL.FRANS LEGA WAE MATA Kel. GORONTALO Kec.KOMODO Kab. Manggarai Barat Nusa Tenggara Timur	danifato08@gmail.com	082147373618	\N	\N
1875	PT. JASTEL MANDIRI PRATAMA	15	5307	Jl. Wolongadha, Kelurahan Bajawa, Kecamatan Bajawa, Kabupaten Ngada Kab. Ngada Nusa Tenggara Timur	pt.jmp73@gmail.com	085239404748	\N	\N
1657	PT. Martarindo Berkah Gemilang	19	6303	Jl.Sukaramai Rt.003 Rw.001 Kel.Jawa Kec.Martapura Kab.Banjar Kab. Banjar Kalimantan Selatan	martarindo.bg@yahoo.com	08125014242	\N	\N
1658	PT. CITRA SUBUR BARU	19	6304	Komp. Handil Bakti Indah Jalur.5 No.67 Rt.18 Berangas Timur Kec. Alalak Kab. Barito Kuala Kalimantan Selatan	pusat@ptaskomelin.co.id	-	\N	\N
1659	PT. MURAKATA MULTI POWER	19	6307	JL. GERILYA RT.07 RW.02 DESA MANDINGIN KEC. BARABAI Kab. Hulu Sungai Tengah Kalimantan Selatan	pt.murakata_multi_power@yahoo.co.id	085248380121	\N	\N
1660	PT. GAUNG UMAR ENERGI	19	6307	JL. SURAPATI BANUA JINGAH, RT.006 RW.002, KELURAHAN BANUA JINGAH, KECAMATAN BARABAI, KABUPATEN HULU SUNGAI TENGAH, KALIMANTAN SELATAN Kab. Hulu Sungai Tengah Kalimantan Selatan	gaungbarabai@gmail.com	+62 813 5168 2013	\N	\N
1661	PT. KARYA ABADI PERSADA	19	6307	Jl. Putera Harapan Simpang 10 No. 6 RT. 006 RW. 003 Kel. Matang Ginakin Kec. Pandawa Kab. Hulu Sungai Tengah Kalimantan Selatan	karyaabadipersada_pt@ymail.com	081251266902	\N	\N
1662	PT. GLOBAL COMMUNITY BORNEO	19	6307	Jl. H. Damanhuri No. 78 Rt.012 Rw.004 Barabai Timur Kec. Barabai Kab. Hulu Sungai Tengah Kalimantan Selatan	ptglobalcommunityborneo@gmail.com	-	\N	\N
1663	PT. TIGA SAUDARA TEKNIK	19	6308	Jl. Amuntai - Tanjung, Pangkalaan No. 08 RT. 02 Ds. Pangkalaan Kec. Amuntai Utara Kab. HSU Kab. Hulu Sungai Utara Kalimantan Selatan	pt.tigasaudarateknik@gmail.com	0527- 61819	\N	\N
1664	PT. HARAPAN PUTRA UTAMA JAYA	19	6302	JL. PUTRI JALEHA NO. 30 RT. 02 BAHARU SELATAN KEC. PULAU LAUT UTARA KAB. KOTABARU, KALIMANTAN SELATAN Kab. Kotabaru Kalimantan Selatan	ptharapanputrautamajaya@gmail.com	051824692, 081348881382	\N	\N
1665	PT. PANCA USAHA JAYA ABADI	19	6302	Jl. Padat Karya No.12 Rt.20 Rw.01, Ds Semayap Kec Pulau Laut Utara Kab. Kotabaru. Kab. Kotabaru Kalimantan Selatan	pt.pujab@gmail.com	051825307/082153580029	\N	\N
1666	PT. PANCA USAHA BERSATU	19	6302	JL.PADAT KARYA NO.12 RT.20 RW.01 Ds.SEMAYAP,KEC.PULAU LAUT UTARA ,KAB.KOTABARU. Kab. Kotabaru Kalimantan Selatan	pt.pujab@gmail.com	051825307/082153580029	\N	\N
1667	PT. TRIKARYA UTAMA BANGUN PERSADA	19	6302	Jl. Raya Stagen RT. 005 Komp. Perum Purnama Indah No. 1A Ds. Sei Taib Kel. Semayap Kec. Pulau Laut Utara Kab. Kotabaru Kalimantan Selatan	pt.trikarya.ktb@gmail.com	0518-23988	\N	\N
1668	CV. IQNA MANDIRI	19	6302	JL. PANGERAN HIDAYAT NO. RT. 003/001 KEC. PULAU LAUT Kab. Kotabaru Kalimantan Selatan	alingsambolelo01@gmail.com	-	\N	\N
1669	CV. SURYA TECHNIK	19	6302	JALAN PERUMNAS HILIR NO. 30 DESA HILIR MUARA Kab. Kotabaru Kalimantan Selatan	alingsambolelo01@gmail.com	-	\N	\N
1670	PT. SULTHAN SKYPOWER ENERGY	19	6309	JL. ASSYUHADA NO.36 RT.010 KEL. BELIMBING KEC. MURUNG PUDAK Kab. Tabalong Kalimantan Selatan	pt.sulthanskypowerenergy@gmail.com	085246616185	\N	\N
1671	PT. MAHAKARYA PUTRA PERKASA	19	6310	Jl. Darma Praja RT. 07 Perumahan Qiramah Alam No. 02, Kersik Putih Kec. Batulicin Kab. Tanah Bumbu Kalimantan Selatan	pt.mahakaryapp@gmail.com	0518-24085	\N	\N
1672	PT. BERKAT IKHTIAR ELEKTRINDO	19	6310	Jl. Transmigrasi Km. 3 RT. 10 No. 083 Kel. Barokah Kec. Simpang Empat Kab. Tanah Bumbu Kab. Tanah Bumbu Kalimantan Selatan	berkatikhtiar62@gmail.com	081351040042	\N	\N
1673	PT. THOMAS ADI JAYA	19	6310	JL TRANSMIGRASI NO 083 RT 010 DESA BAROKAH KECAMATAN SIMPANG EMPAT Kab. Tanah Bumbu Kalimantan Selatan	thomasadijaya1@gmail.com	08125005362	\N	\N
1674	CV. BERINGIN JAYA MANDIRI	19	6301	KOMP. PARIT BARU BLOK A/II RT.26 RW.06 DESA. ANGSAU - KEC. PELAIHARI Kab. Tanah Laut Kalimantan Selatan	rizky.mmbc@gmail.com	-	\N	\N
1675	PT. RAHMAN NUR RAHIM	19	6305	JL. GERILYA (FUTSAL ARRAHMAN) RT.20 KELURAHAN RANTAU KANAN KECAMATAN TAPIN UTARA KABUPATEN TAPIN Kab. Tapin Kalimantan Selatan	pt.rahmannurrahim@gmail.com	-	\N	\N
1676	PT. SURYA ZALI UTAMA	19	6372	JL. MERDEKA BLOK. E RT.11 RW. 4 KEL. PALAM KEC. CEMPAKA Kota Banjarbaru Kalimantan Selatan	rizky.mmbc@gmail.com	081349030598	\N	\N
1677	PT. HAFIZHI BERSAMA JAYA	19	6372	Jl. A. Yani KM. 36,5 Gg. Bersama Mo. 64 RT. 003 RW. 006 Kel. Komet Kec. Banjarbaru Utara Kota Banjarbaru Kalimantan Selatan	hafizi_bersamajaya@yahoo.com	081251060699	\N	\N
1678	PT. INTI MITRA ELECTRINDO	19	6372	KP. KERUING INDAH F-14 RT. 08 Kota Banjarbaru Kalimantan Selatan	geniuslistrik@gmail.com	05113301906	\N	\N
1679	PT. INDO SETIA TEKNIK	19	6372	JL. SAPTA MARGA BLOK B NO. 100 RT. 013 RW. 003 GUNTUNG PAYUNG KEC. LANDASAN ULIN Kota Banjarbaru Kalimantan Selatan	pt.indosetiateknik@yahoo.co.id	-	\N	\N
1680	PT. BERKAT USAHA JAYA	19	6372	Jl. Galaxi Komp. Assifa Al Zahra RT. 027 RW. 004 Kel. Guntung Manggis Kec. Landasan Ulin Kota Banjarbaru Kalimantan Selatan	berkatusahajaya@gmail.com	-	\N	\N
1681	PT. SENTRAL UTAMA TEKNIKAL MANDIRI	19	6372	JL. INTAN SARI NO. 106 RT. 20 BANJARBARU Kota Banjarbaru Kalimantan Selatan	sutm_pt@yahoo.com	-	\N	\N
1682	PT. BERKAT SUMBER ENERGI	19	6371	JL. LINGKAR DALAM SELATAN NO.14 RT.022 KELURAHAN PEMURUS BARU KEC. BANJARMASIN SELATAN 70249 Kota Banjarmasin Kalimantan Selatan	irfancitra22@yahoo.com	0511-6774157	\N	\N
1683	PT. BORNEO MAJUJAYA	19	6371	JL. Kuripan No. 2A RT.5 Banjarmasin Kota Banjarmasin Kalimantan Selatan	fransiska@borneomajujaya.com	0511-3257427 ; 081388324773	\N	\N
1684	PT. LANGIT	19	6371	JL. AHMAD YANI KM 4,5 DHARMA BAKTI IV NOMOR 54 BANJARMASIN. Kota Banjarmasin Kalimantan Selatan	information@langit.co.id	0511-6743761 / 081255551130	\N	\N
1685	PT. BINTANG PERKASA TEKNIK	19	6371	JL SUNGAI ANDAI NOMOR 33 RT 04 KEL SUNGAI ANDAI KEC BANJARMASIN UTARA Kota Banjarmasin Kalimantan Selatan	armiani76@yahoo.co.id	08125018991	\N	\N
1686	PT. NADYA MAJU MANDIRI	19	6371	JL. SUNGAI ANDAI KOMP. HERLINA PERKASA NO.16 RT.037 SUNGAI ANDAI KEC. BANJARMASIN UTARA Kota Banjarmasin Kalimantan Selatan	ptnadyamajumandiri2015@gmail.com	081346838383	\N	\N
1687	PT. ADIQUATRO ELEKTRIKINDO PERKASA	19	6371	JL.Brigjend H. Hasan Basri No.23 RT.016, Kelurahan Pangeran, Kecamatan Banjaramsin Utara Kota Banjaramsin, Kalimantan Selatan. Kota Banjarmasin Kalimantan Selatan	edi.suhardi@adiquatro.co.id	08125115751	\N	\N
1688	PT. CAKRATAMA KRESNA BUANA	19	6371	Jl.Brigjend H.Hasan Basri No. 82 RT.015, Kelurahan Pangeran, Kecamatan Banjarmasin Utara Kota Banjarmasin,Kalimantan Selatan Kota Banjarmasin Kalimantan Selatan	ptcakratamakresnabuana@yahoo.co.id	0811502120	\N	\N
1689	PT. BORNEO PUTRA TABALONG	19	6371	JL. PADAT KARYA RT.03 PONDOK SUNGAI ANDAI PERMAI No.16 Kota Banjarmasin Kalimantan Selatan	borneoputratabalong@gmail.com	0511-6774518, 082158926003	\N	\N
1690	PT. TIGA JAVA ELEKTRA INDONESIA	19	6371	Jl. Sutoya S No. 16 RT. 017 RW. 002 Kel. Antasan Besar Kec. Banjarmasin Kota Banjarmasin Kalimantan Selatan	tigajavaelektraindonesia@gmail.com	0511 335031	\N	\N
1691	PT. RIDHO ILHAM UTAMA	19	6371	Jl. Sintuk II No. 165 Rt. 013/002 Kel. Pemurus Dalam Kec. Banjarmasin Selatan Kota Banjarmasin Kalimantan Selatan	pt.riubjm@gmail.com	05113265349/082254087767	\N	\N
1692	PT. BANUA MITRA ELECTRIK	19	6371	JL. SIMP. GUSTI RAYA NO. 36B RT. 033 RW. 003 ALALAK UTARA KEC. BANJARMASIN UTARA Kota Banjarmasin Kalimantan Selatan	pt.buanamitraelectrik@gmail.com	+62 813-4969-6130	\N	\N
1693	PT. POROS BANUA INDONESIA	19	6371	Jl. Sultan Adam Komp.Perkasa Indah No.20 RT.21 RW. 02 Kel. Surgi Mufti Kec. Banjarmasin Utara Kota banjarmasin Kota Banjarmasin Kalimantan Selatan	porosbanua1@gmail.com	0511-3306141/081253970344	\N	\N
1694	PT. DWI KARYA TEKNIKA	19	6371	JL. SAKA PERMAI GG.HJ. MARDIAH NO.38 RT.011 RW.001 KEL. ANTASAN BESAR KEC. BANJARMASIN TENGAH BANJARMASIN 70114 KALIMANTAN SELATAN Kota Banjarmasin Kalimantan Selatan	dkt@dwikaryateknika.co.id	-	\N	\N
1695	PT. PAHALA MITRA ELEKTRIK	19	6371	Jl.Melati Indah Gg Melati 1 RT 10 RW.02 Kel. Sei Lulut Kec. Banjarmasin Timur Kota Banjarmasin Kalimantan Selatan	pahala.pme@gmail.com	-	\N	\N
1696	PT. KARYA TUNGGAL SUKSES	19	6371	Jl. Saka Permai Gg. H. Mardiah No. 2 RT. 011 RW. 007 Kel. Antasan Besar Kec. Banjarmasin Tengah Kota Banjarmasin Kalimantan Selatan	pt.karyatungalsukses@gmail.com	-	\N	\N
1697	PT. MITRA TEKNIK SEJATI ELECTRINDO	19	6371	JL. SUNGAI MIAI DALAM RT.10 NO.50 BANJARMASIN Kota Banjarmasin Kalimantan Selatan	cv.mts_bj@ymail.com	-	\N	\N
1698	PT. BANUA JAYA MANDIRI	19	6371	Jl. Pramuka Gg. Teratai III No.19 Pamurus Luar Kec. Banjarmasin Timur Kota Banjarmasin Kalimantan Selatan	banuajayamandiri@gmail.com	-	\N	\N
1699	PT. AURACO INSAN BORNEO	19	6371	JL. SEI JINGAH RT. 04 NO. 16 BANJARMASIN Kota Banjarmasin Kalimantan Selatan	auraco@yahoo.com	-	\N	\N
1809	PT. STEBERS CLAN	17	8205	Desa Wosia RT.003 RW.01 Kec. Tobelo Tengah Desa Wosia Kab. Halmahera Utara, Maluku Utara Kab. Halmahera Utara Maluku Utara	stebersclan@gmail.com	081220248984	\N	\N
1810	PT. AINUL HAYAT INDOJAYA	17	8271	KEL. TOBOKO KOTA TERNATE SELATAN Kota Ternate Maluku Utara	pt.ahi2017@gmail.com	085397778899	\N	\N
1811	PT. DLIMA KARYA PRIBUMI	17	8271	KEL. KALUMATA KOTA TERNATE SELATAN Kota Ternate Maluku Utara	pt.dlima789@gmail.com	082218778899	\N	\N
1876	CV. GLOBAL TEKNIK	15	5310	RT.007 RW.003 KELURAHAN UNENG KABOR KECAMATAN ALOK KABUPATEN SIKKA NUSA TENGGARA TIMUR Kab. Sikka Nusa Tenggara Timur	bangleoganteng123@gmail.com	085239200900	\N	\N
1877	PT. VIRKO TEKNIK MAUMERE	15	5310	JL. DIPONEGORO NO. 36 RT.007 RW.001 WOLOMARANG KEC. ALOK BARAT KAB. SIKKA Kab. Sikka Nusa Tenggara Timur	virkoteknik@yahoo.com	085239217054	\N	\N
1878	PT. MANDIRI PUTRA ELECTRIC	15	5310	DUSUN NITAKLOANG,RT.003/RW.002 DESA KOPONG KECAMATAN KEWAPANTE Kab. Sikka Nusa Tenggara Timur	mandiri.teknik7@gmail.com	081347823612	\N	\N
1879	PT. ROSI PUTRA ELEKTRIK	15	5310	Dusun Guru RT.006 RW.003 Desa Takaplager Kec. Nita Kab. Sikka, Nusa Tenggara Timur Kab. Sikka Nusa Tenggara Timur	rosielectrik.mof@gmail.com	081353944712	\N	\N
1880	PT. PRIMA ANGKASA ELEKTRINDO	15	5371	JLN. W. CH OEMATAN NO. 20 KEL. KELAPA LIMA Kota Kupang Nusa Tenggara Timur	primaangkasaelektrindo@yahoo.co.id	081353294635	\N	\N
1881	PT. EVCHADORI SUKSES ABADI	15	5371	JL.SASANDO RT.004 RW.001 KELURAHAN FATUFETO KECAMATAN ALAK Kota Kupang Nusa Tenggara Timur	ptevchadorisuksesabadi@gmail.com	08123924835 / 081246541766	\N	\N
1882	CV. KARYA MANDIRI TEKNIK	15	5371	JL. Frans Seda Gg.Amtaran RT/RW. 032/010 Kelurahan Fatululi Kecamatan Oebobo Kota Kupang, Nusa Tenggara Timur Kota Kupang Nusa Tenggara Timur	cv.Kmandiri_teknik@yahoo.co.id	081 337 407 641	\N	\N
1883	PT. SINAR DUTA TEKNIK	15	5371	RT.021 RW.007 KELURAHAN OESAPA BARAT KECAMATAN KELAPA LIMA Kota Kupang Nusa Tenggara Timur	sinardutateknik@gmail.com	082147717869 / 0811381455	\N	\N
1884	CV. BERKAT MANDIRI TEKNIK	15	5371	RT.047 RW.014 KELURAHAN LILIBA KECAMATAN OEBOBO KOTA KUPANG NUSA TENGGARA TIMUR Kota Kupang Nusa Tenggara Timur	berkatmandiri.teknik@yahoo.com	081353800882	\N	\N
1885	CV. SERVIAM TEKNIK	15	5371	Jl. Kejora RT.036/RW.001, Kel. Oebufu Kec Oebobo Kota Kupang Nusa Tenggara Timur	serviamteknik@yahoo.com	081353795888	\N	\N
1886	PT. ALLBERS TEKNIK SEJAHTERA	15	5371	Jl. RSS Liliba Blok A No.42 RT.023/RW.015 Kel. Liliba Kec. Oebobo Kota Kupang Nusa Tenggara Timur	ptallbersteknik@gmail.com	081339259021	\N	\N
1887	PT. CHRESTY TEKNIK ABADI	15	5371	Jalan Nusantara RT.041/ RW.001 Kel. Liliba Kec. Oebobo, Kota Kupang, Nusa Tenggara Timur Kota Kupang Nusa Tenggara Timur	ptchrestyteknikabadi@gmail.com	082146624111	\N	\N
1888	PT. ROMANSA ELEKTRIKAL INDONESIA	15	5371	JL.NISNONI RT 002 RW. 04 KELURAHAN NUNLEU KECAMATAN KOTA RAJA Kota Kupang Nusa Tenggara Timur	ignasius.ndelu@gmail.com	081339424137	\N	\N
1889	CV. KARYA TEKNIK	15	5371	JL.BAKTI MARGA NO.16 RT.025 RW.010 KEL.FATULULI KEC.OEBOBO KOTA KUPANG NUSA TENGGARA TIMUR Kota Kupang Nusa Tenggara Timur	cemewilhemus@yahoo.com	(0380)821975	\N	\N
1890	CV. BONE TEKNIK	15	5371	RT.007 RW.002 Kelurahan Alak Kecamatan Alak Kota Kupang, Nusa Tenggara Timur Kota Kupang Nusa Tenggara Timur	boneteknik@yahoo.com	081337808282	\N	\N
1891	CV. NDONA TEKNIK	15	5371	RT.001 RW.001 KELURAHAN TUAK DAUN MERAH Kota Kupang Nusa Tenggara Timur	cv.ndona_teknik@yahoo.co.id	081339426059	\N	\N
1892	CV. BATARA JAYA	15	5371	RT 022 RW 006 KELURAHAN KAYU PUTIH KEC. OEBOBO Kota Kupang Nusa Tenggara Timur	cvbatarajaya.rd@gmail.com	0821-46899687	\N	\N
1893	CV. SONIA TEKNIK	15	5371	JL. FRANS DA ROMES MAULAFA KEL. MAULAFA KEC. MAULAFA KOTA KUPANG Kota Kupang Nusa Tenggara Timur	cvsoniateknik.rd@gmail.com	081339033317	\N	\N
1894	PT. ALINKA UTAMA KARYA	15	5371	JL. NUSA BUNGA 1 RT.026 RW.007 KEL. KAYU PUTIH KEC. OEBOBO Kota Kupang Nusa Tenggara Timur	pt.alinka.uk@gmail.com	081237305500	\N	\N
1895	CV. SEINA TENGGARA	15	5371	BTN Kolhua Blok R2 No. 20 RT. 020 RW. 006 Kel. Kolhua Kec. Maulafa Kota Kupang Nusa Tenggara Timur	cvseinatenggara.rd@gmail.com	0380-821732	\N	\N
1896	PT. Nusantara Engineering	15	5371	Jalan Kejora I no.1 Oebufu Kupang - NTT Kota Kupang Nusa Tenggara Timur	nusantara.kupang@gmail.com	081339056000	\N	\N
1897	PT. TERANG TIMOR UTAMA KITA	15	5371	JL. HATI MULIA V/3 OEBOBO, KUPANG - NTT Kota Kupang Nusa Tenggara Timur	terangtimorutamakita@gmail.com	081338749794	\N	\N
1898	CV. TRIO PUTRA TEKNIK	15	5371	RT.036 RW.016 KELURAHAN SIKUMANA KECAMATAN MAULAFA KOTA KUPANG - NUSA TENGGARA TIMUR Kota Kupang Nusa Tenggara Timur	trioputrateknik@yahoo.com	085238422899	\N	\N
1899	CV. SANTIKA KARYA	15	5371	Jl. SMPN II RT.12/04 Kel.Naimata Kec. Maulafa Kota Kupang Nusa Tenggara Timur Kota Kupang Nusa Tenggara Timur	cv.santikakarya@yahoo.co.id	082145174778	\N	\N
1900	PT. DWI TEHNIK KUPANG	15	5371	Jalan Nangka No. 33 Kelurahan Oebobo Kecamatan Oebobo Kota Kupang Nusa Tenggara Timur	pt.dwitehnikkupang@gmail.com	0380-8555017	\N	\N
1901	PT. PALAPA RING ENGINEERING	15	5371	Jl. Gerbang Media RT. 004 RW. 002 Kelurahan Kelapa Lima Kecamatan Kelapa Lima Kota Kupang Kota Kupang Nusa Tenggara Timur	palaparing@gmail.com	0380-823628	\N	\N
1700	PT. PUTRA WAHYU AGUNG	18	6405	Jalan. Durian III Gang Haur Gading No. 27 Kel. Gunung Panjang Kec. Tanjung Redeb Kab. Berau - Kaltim Kab. Berau Kalimantan Timur	pt_putrawahyuagung@yahoo.com	082331660808	\N	\N
1701	PT. ARKA NUSA PRATAMA	18	6405	JL. PEMUDA GANG PINANG MERAH NO. 135 RT. 09 KEL. TANJUNG REDEB KEC. TANJUNG REDEB Kab. Berau Kalimantan Timur	arkanusa16@gmail.com	085100725677	\N	\N
1702	PT. NUSA MANDIRI	18	6405	JL. AKB. SANIPAH NO. 69 TANJUNG REDEBrnBERAU - KALIMANTAN TIMUR Kab. Berau Kalimantan Timur	nusamandiri69@gmail.com	08125864558	\N	\N
1703	PT. CAHAYA TRIJAYA SENTOSA	18	6405	JL. GUNUNG MARITAM GG PEMANCAR RT. 30 NO.02 TANJUNG REDEB Kab. Berau Kalimantan Timur	cahayatrijayasentosa@gmail.com	-	\N	\N
1704	PT. SUMBER CAHAYA BERAU	18	6405	Jl. Gunung Maritam Gg. Pemancar Rt. 30 Kel. Tanjung Redeb Kec. Tanjung Redeb Kab. Berau Kalimantan Timur	pt.scb_76@yahoo.com	-	\N	\N
1705	PT. PENOON JAYA	18	6402	JL. KH. DEWANTARA, GG. KELUARGA RT. 27, KELURAHAN MELAK ULU, KECAMATAN MELAK, Kab. Kutai Barat Kalimantan Timur	pt.penoonjaya@gmail.com	-	\N	\N
1706	PT. RIDHO PRIMA TEKNIK	18	6404	JL. YOS SUDARSO II GANG CEMPAKA NO. 39 RT. 54 KEL. SANGATTA UTARA KEC. SANGATTA Kab. Kutai Timur Kalimantan Timur	hairil.ashar01@gmail.com	08115812101	\N	\N
1707	PT. SAHABAT KITA UTAMA	18	6402	JL. MUNTHE RAYA NO.4 RT.07 KEL. TELUK LINGGA KEC. SANGATTA UTARA Kab. Kutai Timur Kalimantan Timur	sahabatkitautama.pt@gmail.com	-	\N	\N
1708	PT. PUTPA INDAH ELEKTRIKAL	18	6401	JALAN KESEHATAN PASER BELENGKONG RT.7 KECAMATAN PASER BELENGKONG Kab. Paser Kalimantan Timur	putpaindah_02@yahoo.com	085247575755	\N	\N
1709	PT. SUNANGIRI KARYA ENERGI	18	6401	SENAKEN GG. SEROJA RT.06 DESA JONE KEL. JONE KEC. TANAH GROGOT KAB. PASER Kab. Paser Kalimantan Timur	sunangiri22@gmail.com	-	\N	\N
1339	PT. ANUGRAH TUNAS MANDIRI	12	3515	DELTA SARI INDAH BLOK AE NO. 09 KELURAHAN KUREKSARI, KECAMATAN WARU, KAB SIDOARJO Kab. Sidoarjo Jawa Timur	electric4betterlife@gmail.com	03175621181	\N	\N
1710	PT. KARYA UTHAMA ELEKTRIK	18	6401	JL. PADAT KARYA GG. PADA ELOK RT. 11 RW.05, TANA PASER KECAMATAN TANAH GROGOT KABUPATEN PASER PROVINSI KALIMANTAN TIMUR Kab. Paser Kalimantan Timur	karyautama.pt@gmail.com	-	\N	\N
1711	PT. CAHAYA MULIA ELEKTRINDO	18	6401	Jl. Sultan Iskandar Muda No.06 Rt.11/02 Tana Paser Kab. Paser Kalimantan Timur	cahayamulia98@gmail.com	-	\N	\N
1712	PT. MANDIRI MARGA MULYA	18	6409	Jl. Trans Sebakung RT. 022 Kel. Babulu darat Kec. Babulu Kab. Penajam Paser Utara Kalimantan Timur	pt.mandirimargamulya@gmail.com	-	\N	\N
1713	PT. TRI YOGA GEMILANG	18	6471	Jl. MT. Haryono RT. 51 No. 48, Gunung Samarinda, Balikpapan Utara, Balikpapan, Kalimantan Timur Kota Balikpapan Kalimantan Timur	triyoga_gemilang@yahoo.com	0542874492 / 081520380440	\N	\N
1714	PT. DISTRACO	18	6471	Jl. Komp Balikpapan Baru Ruko Sentral Eropa I Blok AA-4 No.12 RT.09, Kelurahan Damai Baru, Kecamatan Balikpapan Selatan Kab. Kota Balikpapan, Kalimantan Timur Kota Balikpapan Kalimantan Timur	dtcbpp@yahoo.co.id	0542877383 / 0811591829	\N	\N
1715	PT. SETYA BHAKTI PERSADA	18	6471	Jl. Pattimura Perum. Batu Ampar Lestari Blok D-7A No.06 RT.47 Kelurahan Batu Ampar, Kecamatan Balikpapan Utara, Kota Balikpapan, Kalimantan Timur Kota Balikpapan Kalimantan Timur	setyabhaktipersada@yahoo.com	0542-8530343	\N	\N
1716	PT. KAMAL KARYA JAYA	18	6471	Jl. Soekarno Hatta No. 90 RT.35 Kel. Graha Indah Kec. Balikpapan Utara Kota Balikpapan Kota Balikpapan Kalimantan Timur	ptkamalkaryajaya@gmail.com	08115908990	\N	\N
1717	PT. YUTINDA PUTRA MANDIRI	18	6471	JL.ABDI PRAJA III PERUMAHAN KORPRI BLOK B-1 NO. 14 KELURAHAN SEPINGGAN BARU KECAMATAN BALIKPAPAN SELATAN, KALIMANTAN TIMUR Kota Balikpapan Kalimantan Timur	ptyutindaputramandiri@gmail.com	0542 7202269 / 0542 8705788 / 081254015906	\N	\N
1718	PT. MAJAR PRATAMA	18	6471	JL. KOMP. BALIKPAPAN BARU BLOK B3 NO. 19 DAMAI KEC. BALIKPAPAN SELATAN KOTA BALIKPAPAN, KALIMANTAN TIMUR Kota Balikpapan Kalimantan Timur	majarpratama@gmail.com	0542-8862014	\N	\N
1310	PT. TERATAI MANDIRI PACITAN	12	3501	JL. KH. HASYIM ASY'ARI GG. JAMBU NO.2 LINGK. KRAJAN LOR RT.04 RW.04 KEL. PLOSO KEC. PACITAN Kab. Pacitan Jawa Timur	pt.terataimandiri@gmail.com	081335706851	\N	\N
1311	PT. ASLI 72 PACITAN	12	3501	Dusun Duduhan RT 02 RW O2 DESA Mentoro Kecamatan Pacitan Kabupaten Pacitan Kab. Pacitan Jawa Timur	asli72pacitan@yahoo.co.id	0357886695	\N	\N
1312	PT. TERANG TIGA SAUDARA	12	3528	DUSUN DASOK RT.011 RW.003 KELURAHAN DASOK KECAMATAN PADEMAWU PAMEKASAN Kab. Pamekasan Jawa Timur	terang3saudara@gmail.com	0818307500	\N	\N
1313	PT. IFAMA NUANSA ASRI	12	3528	JL. STADION V / 117, KELURAHAN LAWANGAN DAYA, KECAMATAN PADEMAWU KABUPATEN PAMEKASAN Kab. Pamekasan Jawa Timur	ifama.nuansa.asri@gmail.com	081231699922	\N	\N
1314	PT. SURYA CAHAYA SEJAHTERA	12	3528	JL. LAWANGAN DAYA NO. 27A KEC. PADEMAWU KAB. PAMEKASAN Kab. Pamekasan Jawa Timur	suryadi262@yahoo.co.id	082245293702 / 087850421542	\N	\N
1315	PT. BLAMBANGAN JAYA	12	3528	JL.BONOROGO LEBAR 28. PAMEKASAN Kab. Pamekasan Jawa Timur	blambangan_jaya@ymail.com	0324 - 336161 / 0818318973	\N	\N
1316	PT. MANDIRI MADURA	12	3528	JL.LAWANGAN DAYA. PAMEKASAN Kab. Pamekasan Jawa Timur	mandirielectrindo@yahoo.co.id	0324 - 328086 / 0818598387	\N	\N
1317	PT. SUMBER ANUGERAH AGUNG	12	3528	JL.VETERAN MUDA 5A. PAMEKASAN Kab. Pamekasan Jawa Timur	cv.sumberagung_pks@yahoo.co.id	0324-324315/081939289115	\N	\N
1318	PT. ARTHA NUSANTARA ELEKTRON	12	3528	JL.R.ABD.AZIS 4.PAMEKASAN Kab. Pamekasan Jawa Timur	osa_hari@yahoo.co.id	0324-334133/081357366698	\N	\N
1319	PT. GALANG CIPTA UTAMA	12	3528	Jl. Jingga No. 48 Barurambat Kota Kecamatan Pamekasan Kab. Pamekasan Jawa Timur	galang.cipta@yahoo.co.id	0324 321206/ 08175005025	\N	\N
1320	PT. CAHAYA MARTA PERKASA	12	3528	Jl.Lawangan Daya 27. Kelurahan Lawangan Daya, Kecamatan Pademawu.Kabupaten Pamekasan Kab. Pamekasan Jawa Timur	pt.cahayamartaperkasa@yahoo.co.id	0324-333107/081703294333	\N	\N
1321	PT. MADU WANGI SEJAHTERA	12	3528	JL. RAYA PADEMAWU NO. 94 RT. 01 RW 05 DESA PADEMAWU BARAT Kab. Pamekasan Jawa Timur	madu.wangi5ejahtera@gmail.com	081703704766	\N	\N
1322	PT. PUTERA NUSANTARA UTAMA	12	3514	SUKUN RT.004 RW.002 DS. WIDORO KEC. KREJENGAN KAB. PROBOLINGGO JAWA TIMUR Kab. Pasuruan Jawa Timur	pt.putera.nusantara.u@gmail.com	085353460412	\N	\N
1323	PT. AFRIZAL JAYA ELEKTRIK	12	3514	Jl. Raya Pandaan Km. 3 RT.03 RW.11 Desa Nogosari Kecamatan Pandaan Kab. Pasuruan Jawa Timur	pt.aje_14@gmail.com	03434-639204	\N	\N
1324	CV. Berkah Elektrik	12	3502	Dukuh Sidorejo RT.01 RW.01 Ds. Balong Kec.Balong Kab. Ponorogo Jawa Timur Kab. Ponorogo Jawa Timur	regptamin@gmail.com	081556677366	\N	\N
1325	CV. SINAR SEMBILAN	12	3502	Dukuh Jurug RT.01 RW.01 Desa Jurug Kec. Sooko Kab. Ponorogo Jawa Timur Kab. Ponorogo Jawa Timur	regptamin@gmail.com	082299178046	\N	\N
1326	PT. SURYA INTI ABADI	12	3502	JL. NIKEN GANDINI 76C RT. 2/4 SINGOSAREN KEC. JENANGAN Kab. Ponorogo Jawa Timur	suryaintiabadiponorogo@yahoo.com	-	\N	\N
1327	PT. ADIGUNA MANDIRI PERKASA	12	3502	Jl Pemuda No 5 Balong Ponorogo Kab. Ponorogo Jawa Timur	adiguna.mp2010@gmail.com	-	\N	\N
1328	PT. GLOBAL KHATULISTIWA JAYA	12	3502	Jl Godang RT.02/RW.01 Patihan Kidul Siman Ponorogo Kab. Ponorogo Jawa Timur	gkj.pt@gmail.com	-	\N	\N
1329	PT. SRIKANDI BERKAH ABADI	12	3502	Jl Godang 71A Patihan Kidul Siman Ponorogo Kab. Ponorogo Jawa Timur	pt.srikandi@yahoo.co.id	-	\N	\N
1330	PT. INDRA SAPUTRA	12	3513	Jalan Yos Sudarso Gg.2 No.93 RT04 RW01 Kecamatan Dringu Kab. Probolinggo Jawa Timur	pt.indrasaputra@gmail.com	081233300579	\N	\N
1331	PT. NUR CAHAYA TEKNIK	12	3513	DNS. BENGKINGAN NO.86 KALIREJO KEC.DRINGU KAB.PROBOLINGGO Kab. Probolinggo Jawa Timur	suryautama01112011@gmail.com	-	\N	\N
1332	PT. NORRA PANUNGGAL PERSADA	12	3513	Jl. Supriyadi RT.005 RW. 004 Curah Grinting, Kanigaran Kota Probolinggo Kab. Probolinggo Jawa Timur	ilham.berkahutama@gmail.com	-	\N	\N
1333	PT. SURYA UTAMA ELECTRIC	12	3513	JALAN RAYA DRINGU NOMOR 12 DESA KALIREJO KECAMATAN DRINGU Kab. Probolinggo Jawa Timur	suryautama_prob86@yahoo.com	-	\N	\N
1334	PT. HADI MULYA SANJAYA	12	3513	JL. ARGOPURO III NO.44 KANDANGJATI KULON KEC. KRAKSAAN PROBOLINGGO, JAWA TIMUR Kab. Probolinggo Jawa Timur	pthadimulya.sanjaya@yahoo.co.id	-	\N	\N
1335	PT. TANG GHEJE BERKAH	12	3527	DESA PANGARENGAN KECAMATAN PANGARENGAN KABUPATEN SAMPANG Kab. Sampang Jawa Timur	pttanggheje2014@gmail.com	087806806536	\N	\N
1336	PT. SAMPOERO AGUNG MANDIRI	12	3527	Jl. Jago No. 06 Desa Dharma Camplong Kec. Camplong Kab. Sampang Kab. Sampang Jawa Timur	pt.sampoeroagungmandiri@gmail.com	081332235090	\N	\N
1337	CV. PUTRI BAHARI	12	3527	Jl. Karangloh No. 19 , Ds, Dharma Camplong Kec. Camplong Kab. Sampang Kab. Sampang Jawa Timur	cv.putribahari@gmail.com	081332235090	\N	\N
1338	PT. ZAFIRA CITRA PERSADA	12	3515	JL KH SAHLAN DSN TUNDUNGAN RT01 RW02 SIDOMOJO KRIAN SIDOARJO Kab. Sidoarjo Jawa Timur	zafiracp@yahoo.com	08123144893	\N	\N
1340	PT. BANGUN PERSADA JAYA ABADI	12	3515	DSN TUNDUNGAN RT 01 RW 02 SIDOMOJO KRIAN SIDOARJO Kab. Sidoarjo Jawa Timur	bangunpersada_jb@yahoo.com	081217000046	\N	\N
1341	PT. Hasta Prajatama	12	3515	Jl. Lingkar Timur No. 01 Desa Kemiri Kecamatan Sidoarjo 61234 Kab. Sidoarjo Jawa Timur	hastaprajatama@yahoo.com	(031) 8073893, 8956551	\N	\N
1342	PT. Hasta Prajatama	12	3515	Jl. Lingkar Timur No. 01 Desa Kemiri Kecamatan Sidoarjo Kabupaten Sidoarjo, Jawa Timur 61234 Kab. Sidoarjo Jawa Timur	hastaprajatama@yahoo.com	(031) 8073893, 8956551	\N	\N
1343	PT. AYO BERJAYA BAROKAH	12	3515	PONDOK MUTIARA MEA-28 RT032 RW14 DESA BANJARBENDO KEC SIDOARJO KAB SIDOARJO Kab. Sidoarjo Jawa Timur	AYOBERJAYA@GMAIL.COM	081234561707/031 8052030	\N	\N
1344	PT. YUSSY MULYA ABADI	12	3515	PURI INDAH LESTARI FA 12 RT.61 RW.10, KELURAHAN SUKO KECAMATAN SIDOARJO, KABUPATEN SIDOARJO JAWA TIMUR Kab. Sidoarjo Jawa Timur	yussy_mulyaabadi@yahoo.com	-	\N	\N
1345	PT. MULYA JATRA	12	3515	JL. KYAI NAWAWI NO. 51-53 WADUNGASRI WARU SIDOARJO Kab. Sidoarjo Jawa Timur	marketing@mulyajatra.com	-	\N	\N
1346	PT. GUNUNG GAJAH INDONESIA	12	3515	Puri Indah Lestari FG-01 Sidoarjo Kab. Sidoarjo Jawa Timur	guganesia.pt@gmail.com	-	\N	\N
1347	PT. HASTA PRAJATAMA	12	3515	JL. LINGKAR TIMUR NO. 01 DESA KEMIRI Kab. Sidoarjo Jawa Timur	ADMIN@HASTAPRAJATAMA.CO.ID	-	\N	\N
1348	PT. SIXMA MEGA UTAMA	12	3515	JL. MASANGAN WETAN NO. 18 Kab. Sidoarjo Jawa Timur	sixma_mu@yahoo.com	-	\N	\N
1349	PT. SARANA DWI MAKMUR	12	3515	Puri Surya Jaya - Vancouver JI-20 Gedangan Kab. Sidoarjo Jawa Timur	saranadm@saranadwimakmur.com	-	\N	\N
1350	PT. SDM BERKARYA SEJAHTERA	12	3515	Perumahan Jenggolo Asri N 19-20 Sidokerto-Buduran Kab. Sidoarjo Jawa Timur	sdm_bs@yahoo.com	-	\N	\N
1351	PT. RIZKY KURNIA MULYA ABADI	12	3515	Jl. Nanas IV/Blok N No. 438 Pondok Candra Indah - Waru Kab. Sidoarjo Jawa Timur	pt.rkma@gmail.com	-	\N	\N
1352	PT. BERKAH MULIA JAYA	12	3515	Pondok Sedati Asri H-33 Kel. Pepe Kec. Sedati Kab. Sidoarjo Jawa Timur	bmj_cv@yahoo.co.id	-	\N	\N
1353	PT. MUKJIZAT JAYA MANDIRI CABANG SITUBONDO	12	3512	Jl. Sucipto XII No 14 Situbondo Kab. Situbondo Jawa Timur	mukjizatjayamandiri.situbondo@gmail.com	085258597211 - 0852140672672	\N	\N
1354	PT. KARINDA PERMATA NUSANTARA	12	3512	Perum Panji Permai Blok C No. 10 Kel. Mimbaan Kec. Panji Kab. Situbondo Jawa Timur	pt.karindapermata_nusantara@yahoo.co.id	0338-677679	\N	\N
1355	PT. RIFA ADI PUTRA	12	3512	Jl. Semeru GG.II RT1 RW11 KEL. MIMBAAN KEC. PANJI KAB. SITUBONDO, JAWA TIMUR Kab. Situbondo Jawa Timur	ptrifaadiputra@yahoo.co.id	082330883450	\N	\N
1356	PT. CAHAYA WANDA ELEKTRIKAL	12	3529	perum Graha arya wiraraja Blok A III B NO.02. RT.005 RW.002 Desa Gunggung Kecamatan Batuan Kabupaten Sumenep,Jawa timur Kab. Sumenep Jawa Timur	cahaya.wanda_elektrikal@yahoo.co.id	081803189977	\N	\N
1357	PT. PURNAMA AURA ELEKTRIKAL	12	3529	PERUM GRAHA ARYA WIRARAJA Blok A IIIB RT.005 RW.002, kelurahan Gunggung,Kecamatan Batuan Kabupaten Sumenep,Jawa timur Kab. Sumenep Jawa Timur	purnama_electrical@yahoo.co.id	081803189977	\N	\N
1358	PT. Cahaya Agung Alif	12	3503	JL.Agus Salim No.03 Sumbergedong Kec.Trenggalek Kab.trenggalek,Jawa Timur Kab. Trenggalek Jawa Timur	agung_alifpt@yahoo.com	0355-794985	\N	\N
1359	PT. MARDI UTOMO PUTRA	12	3503	JL. I. GUSTI NGURAH RAI NO. 41 KEL. SURODAKAN RT. 20/06 TRENGGALEK Kab. Trenggalek Jawa Timur	puriadhi@gmail.com	-	\N	\N
1360	CV. WARNOWO TEHNIK	12	3503	JL. I. GUSTI NGURAHRAI NO. 61 - A RT. 20 RW. 06 KEL. SURODAKAN Kab. Trenggalek Jawa Timur	nonotsukriswarnowo@yahoo.co.id	-	\N	\N
1361	PT. LANGGENG JAYA BERKAH TUBAN	12	3523	Jl. Mayangsari Ds. Merkawang Kec. Tambakboyo Kab. Tuban Jawa Timur	pt.ljbtuban@gmail.com	081330336083	\N	\N
1362	PT. BUKIT ABADI PRATAMA	12	3523	JL. SUNAN KALIJOGO NO. 53 - A TUBAN Kab. Tuban Jawa Timur	Bukitpermai48@gmail.com	081359100994	\N	\N
1363	PT. YUGA ELEKTRO TUBAN	12	3523	JL. RAYA BULU - JATIROGO 1/3 Ds. BADER KEC. JATIROGO KAB. TUBAN Kab. Tuban Jawa Timur	pt.yugaelektrotuban@gmail.com	082133341799	\N	\N
1364	PT. EKA KARTIKA PERSADA	12	3523	Ds. Socorejo RT.01/RW.03 Kec. Njenu Kab. Tuban Kab. Tuban Jawa Timur	ilham.berkahutama@gmail.com	085200785758	\N	\N
1365	PT. DM MANDIRI	12	3523	Jl Semarang Km. 31 Ds gadon Kec. Tambakboyo Kab. Tuban Jawa Timur	dmmunib@gmail.com	-	\N	\N
1366	PT. CAHAYA CEMORO KEMBAR	12	3523	Dsn Krajan RT. 02 RW. 02 Ds. Prunggahan Kulon Kec.Semanding Kab. Tuban Kab. Tuban Jawa Timur	ilham.berkahutama@gmail.com	-	\N	\N
1367	PT. FERIBO KARYA UTAMA	12	3523	Jl. Hayam Wuruk, Dusun Krajan RT. 05 RW. 01 Desa Bejagung Kec. Semanding Kab. Tuban Kab. Tuban Jawa Timur	ilham.berkahutama@gmail.com	-	\N	\N
1368	PT. AURA KARYA SEJAHTERA	12	3504	JL PAHLAWAN 56 KETANON KEDUNGWARU TULUNGAGUNG Kab. Tulungagung Jawa Timur	pt.aura5758@gmail.com	081335146846	\N	\N
1369	PT. BANUSA	12	3504	Jl. Sultan Agung no. 9A Ketanon Kedungwaru Tulungagung Kab. Tulungagung Jawa Timur	banusa09@gmail.com	081335656346	\N	\N
1370	PT.Anugerah Abdi Daya Tulungagung	12	3504	jl.Pahlawan No.002 Rw.021 Desa.Gendingan Kec.Kedungwaru Kab.Tulungagung Kab. Tulungagung Jawa Timur	aanbejan1@gmail.com	082330416755	\N	\N
1371	PT. PRATAMA GANDHI SEJAHTERA	12	3504	DSN. MAYANGAN RT. 14 RW. 06 DS. SRIKATON KEC. NGANTRU KAB. TULUNGAGUNG Kab. Tulungagung Jawa Timur	ptpratamagandhisejahtera@gmail.com	081359972395	\N	\N
1372	PT. DIMARSEWU UNGGUN WISESO	12	3504	jl.pahlawan gang II no.16D rt.02 rw.02 kelurahan ketanon kec.kedungwaru kab.tulungagung Kab. Tulungagung Jawa Timur	ilham.berkahutama@gmail.com	-	\N	\N
1373	PT. CAHAYA TEKNIK ELEKTRINDO	12	3571	Jl. Nusantara No. 02 Perum Putih Permai Ds. Putih Kec. Gampengrejo Kediri Kota Kediri Jawa Timur	pt_cte@yahoo.com	0354 672371	\N	\N
1374	PT. ASA MULIA NUSANTARA	12	3571	Jl. Raya Gampeng RT 02 RW 02 Ds. Gampeng Kec, Gampengrejo Kediri Kota Kediri Jawa Timur	asamulia@yahoo.com	0354 696982	\N	\N
1375	PT. RAJAWALI SAKTI ABADI	12	3577	Jl. Lombok No. 5 Madiun Kota Madiun Jawa Timur	ptrajawalisaktiabadi@yahoo.com	0351464358	\N	\N
1376	PT. TAPAK BIMO	12	3577	Jl. Sekolahan No. 27 RT. 008 RW. 003 Kelurahan Banjarejo Kecamatan Taman, Madiun Kota Madiun Jawa Timur	tapakbimo_mdn@yahoo.co.id	0351-451896 / 469753	\N	\N
1377	PT. KALISNA PUTRA CAHAYA	12	3577	JL CAHYO INDAH BLOK A7 KEL.KELUN KEC.KARTOHARJO KOTA MADIUN Kota Madiun Jawa Timur	ptkalisna@yahoo.co.id	081556575282	\N	\N
1378	PT. MARTAPURA CAHAYA ABADI	12	3577	JL RADEN WIJAYA NO.35 MANGUHARJO MADIUN Kota Madiun Jawa Timur	martapura.electric@yahoo.com	081359014236	\N	\N
1379	PT. ANUGERAH PUTRA PERMANA	12	3577	JL. DUNGUS MANIS 22 MADIUN Kota Madiun Jawa Timur	pt_app2005@yahoo.com	08123413043	\N	\N
1380	PT. NUSANTARA SURYA GEMILANG	12	3577	JL. SETIA BUDI NO 65 MADIUN Kota Madiun Jawa Timur	pt_nsg2006@yahoo.co.id	0811302023	\N	\N
1381	PT. KENARI INDAH PERKASA	12	3577	JL. KENARI NO. 19 KEL. KEJURON KEC. TAMAN - KOTA MADIUN Kota Madiun Jawa Timur	kip19_pt@yahoo.com	-	\N	\N
1382	PT. BINA KARYA TAMA	12	3577	PERUM. MARGATAMA BLOK 6 NO. 8 Kota Madiun Jawa Timur	tria8ro50@yahoo.com	-	\N	\N
1383	PT. NATA USAHA BERSAMA	12	3573	JL METRO NO 7 RT5 RW4 BUNULREJO BLIMBING MALANG Kota Malang Jawa Timur	nata.ubersama@gmail.com	0341482325	\N	\N
1384	PT. KALYA MEDIA TEKINDO	12	3573	JL SAXOFONE PERUM PERMATA BRANTAS INDAH KAV 42 KEL. JATIMULYO KEC. LOWOKWARU KOTA MADYA MALANG JAWA TIMUR Kota Malang Jawa Timur	kalya_mediatek@yahoo.co.id	08123366734	\N	\N
1385	PT. INDO CITRA PRATAMA	12	3573	Jl. Ranugrati No. 23 RT 002 RW 001 Kel. Sawojajar Kec. Kedungkandang Kotamadya Malang Jawa Timur Kota Malang Jawa Timur	ptindocitra007@yahoo.com	0341-715340 / 085749679329	\N	\N
1386	PT. BOROBUDUR MEDECON	12	3573	JL SOEKARNO HATTA NO 46 RT 03 RW 10 JATIMULYO LOWOKWARU MALANG JAWA TIMUR Kota Malang Jawa Timur	borobudara@yahoo.com	0341400222	\N	\N
1387	PT. ANINDO BERTAHANNUTS PERKASA	12	3573	JL. DANAU TOBA B-19 KEL. SAWOJAJAR KEC. KEDUNGKANDANG Kota Malang Jawa Timur	anindo_bp@yahoo.com	0341 717818	\N	\N
1388	PT. AYODYA JAYA	12	3573	JL. LAHOR NO. 5 KEL. BUNULREJO KEC. BLIMBING Kota Malang Jawa Timur	ayodya.jaya@gmail.com	0341494424	\N	\N
1389	CV. WIRA MULYA	12	3573	Jl. Raya Candi 2/27 Rt 05 Rw 02 Kelurahan Karang Besuki Kecamatan Sukun Kota Malang Kota Malang Jawa Timur	ariwahyunarni@ymail.com	-	\N	\N
1390	PT. hartanto reka daya	12	3576	Jl. Muria Raya No.5, Wates, Mojokerto Kota Mojokerto Jawa Timur	hartantoengineering@gmail.com	-	\N	\N
1391	PT. DIAN JAYA TEKNIK	12	3575	Jl. Kolonel Sugiono No.22, Kel. Mayangan, Kec. Panggungrejo, Kota Pasuruan Kota Pasuruan Jawa Timur	pt.dianjayateknik@yahoo.co.id	0343-422617	\N	\N
1392	PT. ANANDA SARI TEKNIK	12	3575	Jl. Kol. Sugiono No. 26 RT 03 RW 01 Kel. Mayangan Kec. Panggungrejo Kota Pasuruan Jawa Timur	pt.anandasari@yahoo.co.id	-	\N	\N
1393	PT. Handoyo Elektrik Utama	12	3575	Jl. Karya Bhakti Blok C - 3 RT 01 RW 07 Kel. Gentong Kec. Gadingrejo Pasuruan 67139 Kota Pasuruan Jawa Timur	cv.handoyo.pasuruan@gmail.com	-	\N	\N
1394	PT. AGP PUTRA PRATAMA	12	3575	PERUM. GRIYA PERMATA TEMBOK BLOK F NO.03 TEMBOKREJO KOTA PASURUAN Kota Pasuruan Jawa Timur	pt.agpputrapratama@yahoo.com	-	\N	\N
1395	CV. RYAN ANDHIKA	12	3574	JL SUNAN GIRI GANG HASAN BISRI NO.29 SUMBERTAMAN WONOASIH Kota Probolinggo Jawa Timur	ra.andhika@yahoo.com	-	\N	\N
1396	PT. TIGA PUTRA JAYA SEJAHTERA	12	3578	JL. Kedurus III Masjid Pilang Asri No. 02 Kelurahan Kedurus Kecamatan Karangpilang Kota Surabaya Jawa Timur	tpjs_surabaya@yahoo.com	031-7671108	\N	\N
1397	PT. JAYA ABADI ENERGY	12	3578	Jl. Bendul Merisi Indah No. 5 Kel. Bendulmerisi Kec. Wonocolo Kota Surabaya, Jawa Timur Kota Surabaya Jawa Timur	jayaabadienergy@gmail.com	031-7875764	\N	\N
1398	PT. SAPTA PUSAKA GRAHANUSANTARA	12	3578	JL. GUBENG POJOK NO. 5 KEL. KETABANG KEC. GENTENG KOTA SURABAYA, JAWA TIMUR Kota Surabaya Jawa Timur	sapta@saptapusaka.com	(031) 5354200, 5354201, 5354202, 5354204, 5354205	\N	\N
1399	PT. Atiga Surya	12	3578	Jl. Klampis Aji I/41 Kota Surabaya Jawa Timur	atigasurya@gmail.com	0811310845	\N	\N
1400	PT. And Technical Services	12	3578	Jl. Raya Kendangsari Blok C/14 RT.006 RW.002 Desa Kendangsari Tenggilis Mejoyo Surabaya Jawa Timur Kota Surabaya Jawa Timur	vodka.ciu61@yahoo.com	081335030066	\N	\N
1401	PT. USAHA BAKTI PERKASA	12	3578	JL. JEMURSARI 3/37 JEMUR WONOSARI Kota Surabaya Jawa Timur	usbak_sby@yahoo.com	031-8430016	\N	\N
1402	PT. General Electric Power Solution Indonesia	12	3578	Jl. Panti Mulia, Kel. Ujung, Kec. Ujung Semampir, Kota Surabaya, Jawa Timur Kota Surabaya Jawa Timur	wahyu.iryanto@ge.com	+62 21 80649878 / + 62 811 3545 402	\N	\N
1403	PT. PRIMA PERSADA NUSANTARA	12	3578	Jl. Gubeng Kertajaya V Raya No. 9 Kel. Gubeng Kec. Gubeng, Surabaya, Jawa Timur Kota Surabaya Jawa Timur	primapersadanusantara@yahoo.co.id	031-5031383	\N	\N
1404	PT. SEDYA UTAMA	12	3578	Jl. Kaliwaru I/28 Kel. Kalirungkut Kec. Rungkut Surabaya Kota Surabaya Jawa Timur	sedyautama@yahoo.com	031 8432089	\N	\N
1405	PT. WIRA JAYA TEKNIK	12	3578	JL. BANYU URIP WETAN 6/20 RT 005 RW 004 KEL. PUTAT JAYA KEC. SAWAHAN Kota Surabaya Jawa Timur	pt.wirajayateknik@gmail.com	031-5686338/081330353376	\N	\N
1406	PT. GLOBAL ENERGI INDONESIA	12	3578	JL DUPAK BANGUNSARI III / 30 KEL DUPAK KEC KREMBANGAN SURABAYA Kota Surabaya Jawa Timur	electric4betterlife@gmail.com	08123139956	\N	\N
1407	PT. Tribina Daya Utama	12	3578	Jl. Darmo Permai Timur I No. 30 Sonokwijenan, Kec. Sukomanunggal, Kota Surabaya Jawa Timur Kota Surabaya Jawa Timur	tribina_du@yahoo.com	031-7326558	\N	\N
1408	PT. ELPO INDONESIA	12	3578	JL. JOJORAN 3/135 MOJO KEC.GUBENG Kota Surabaya Jawa Timur	elpoindonesia01@gmail.com	031-5952292	\N	\N
1409	PT. GUNTUR JAYAMAKMUR	12	3578	Jl. Raden Saleh No. 24, Kel. Bubutan, Kec. Bubutan, Surabaya Kota Surabaya Jawa Timur	pt.gunturjayamakmur@yahoo.com	0315034715	\N	\N
1410	PT. ARTO JAYA TEHNICAL	12	3578	JL. SEMEMI REJO ASRI II NO.1 KEL.SEMEMI Kota Surabaya Jawa Timur	artojayatehnical99@gmail.com	031-99160693	\N	\N
1411	PT. METRIKA MULYA MANDIRI	12	3578	Jl. Gading II / 51 Kelurahan Gading Kecamatan Tambaksari Kota Surabaya Jawa Timur	metrika_cv@yahoo.com	031-3816605	\N	\N
1412	PT. PEMBINA JAYA	12	3578	JL. Tuban Raya No 89-91 Kel. Jepara Kec. Bubutan Kota Surabaya Jawa Timur	pembinajaya@yahoo.com	031-3533049, 031-3539325	\N	\N
1413	PT. ADI TEHNIK	12	3578	JL. TAMBAK DALAM BARU I NO.2 KEL.ASEMROWO KEC.ASEMROWO Kota Surabaya Jawa Timur	tehnik_adi@yahoo.co.id	031-72159009 031-7499135	\N	\N
1414	PT. PELITA BUMIWANGI	12	3578	Jl. Ngagel Jaya Tengah No. 107 Surabaya Kota Surabaya Jawa Timur	pelita_holding@yahoo.com	031-8549233	\N	\N
1415	PT. SABAR REJO	12	3578	Jalan Simorejo Timur I / 4 Kelurahan Simomulyo Kecamatan Sukomanunggal Kota Surabaya Jawa Timur	koban_pdkb@yahoo.co.id	0315324839	\N	\N
1416	PT. INDAMARDI JAYA ABADI	12	3578	JL DUPAK BANGUNSARI 3/22 KEL. DUPAK KEC. KREMBANGAN Kota Surabaya Jawa Timur	electric4betterlife@gmail.com	0313551233	\N	\N
1417	PT. MULYAABADIASRO	12	3578	JL KENDANGSARI NO. 75 SURABAYA Kota Surabaya Jawa Timur	mlyaabadi_1st@yahoo.co.id	-	\N	\N
1418	PT. ANDRE TEKNIK SOLUTION	12	3578	JL Kandangan I No 31 RT 01 RW 01 Kel, Kandangan Kec, Benowo Kota Surabaya Kota Surabaya Jawa Timur	akeiprovjatim@gmail.com	-	\N	\N
1419	PT. CIPTA KARSA BUMI LESTARI	12	3578	JL. GAYUNGSARI BARAT NO. 8-10 SURABAYA Kota Surabaya Jawa Timur	ptcipta2006_sing@yahoo.com	-	\N	\N
1420	PT. BINABUANA SEWAKA	12	3578	Jl. Arief Rachman Hakim 177 Lt-2B, Surabaya Kota Surabaya Jawa Timur	binabuanasewaka@yahoo.com	-	\N	\N
1421	PT. KENCANA ALAM PUTRA	12	3578	JL. KLAMPIS ANOM 17/D-4, SURABAYA Kota Surabaya Jawa Timur	admin@kencana-alam.com	-	\N	\N
1422	PT. MEGA JAYA SUKSES UTAMA	12	3578	JL. MENUR 38 B KEL. AIRLANGGA, KEC. GUBENG, SURABAYA Kota Surabaya Jawa Timur	PT.MEGAJAYASUKSESUTAMA@YAHOO.COM	-	\N	\N
1423	PT. KARYA MULIA SEMESTA	12	3578	JL JAMBANGAN TAMA II NO. 21 SURABAYA Kota Surabaya Jawa Timur	karyamulia23@yahoo.com	-	\N	\N
1424	PT. REKAYASYAA UTAMA INDONESIA	12	3578	JL JAMBANGAN TAMA II No. 19 SURABAYA Kota Surabaya Jawa Timur	rekayasateknik@yahoo.co.id	-	\N	\N
1425	PT. BANGUN PERKASA JAYA ENGINEERINGING	12	3578	JL. KERTOMENANGGAL VIII/5 DUKUH MENANGGAL KEC.GAYUNGAN Kota Surabaya Jawa Timur	esagroup@sby.dnet.net.id	-	\N	\N
1426	PT. KUSUMA JAYA ANUGRAH	12	3578	Karang Menur 2/21, Kel. Airlangga, Kec. Gubeng, Kab. Surabaya Kota Surabaya Jawa Timur	ilham.berkahutama@gmail.com	-	\N	\N
1427	PT. BELLA PERDANA RAHARJA	12	3578	Jl. Bronggalan Sawah 5A No. 22 Surabaya Kota Surabaya Jawa Timur	pt.berharja@yahoo.com	-	\N	\N
1428	PT. INDO KARYA ELEKTRIK MANDIRI	12	3578	Jl. Panglima Sudirman No. 101-103 Kel. Embong Kaliasin Kec. Genteng Kota Surabaya Jawa Timur	pusat@ptaskomelin.co.id	-	\N	\N
1429	PT. KOPERASI JASA KONSTRUKSI NASIONAL JAWA TIMUR	12	3578	GRHA PAKLINA JALAN TALES 2 NOMR 33 RT 001 RW 010 KELURAHAN JAGIR KECAMATAN WONOKROMO KOTA SURABAYA Kota Surabaya Jawa Timur	kopjkn@gmail.com	-	\N	\N
1430	PT. HASTA KARYA PERDANA	12	3578	JL. KETINTANG SELATAN 115 SURABAYA Kota Surabaya Jawa Timur	corporate@hastaindonesia.com	-	\N	\N
1431	PT. BINTANG TIMUR UTAMA SEJAHTERA	12	3578	Jl. Ngagel Dadi IIC/1 Kota Surabaya Jawa Timur	btu_sejahtera@yahoo.com	-	\N	\N
1432	PT. BINTANG TIMUR UTAMA BERKARYA	12	3578	Jl. Ngagel dadi III No. 16 RT 006 RW 010 Kel. Ngagel Rejo Kec. Wonokromo Kota Surabaya Jawa Timur	btu_berkarya@yahoo.com	-	\N	\N
1433	PT. NUSANTARA JAYA DIMAR	12	3578	Jl. Bronggalan Sawah 5A No. 22-24 Kota Surabaya Jawa Timur	nusantara_jaya_sby@yahoo.com	-	\N	\N
1434	PT. sigma tirta surya	12	3578	Jl. Kebraon II/39 Karang Pilang Kota Surabaya Jawa Timur	djopam.dp@gmail.com	-	\N	\N
1435	PT. mardika sarana engineering	12	3578	Jl. Raya Tenggilis 121 /AA-1 Kota Surabaya Jawa Timur	office@mse.co.id	-	\N	\N
1436	PT. PURNAMA INDONESIA	12	3578	Jl. Gresik Gadukan No.254 Surabaya Kota Surabaya Jawa Timur	purindo_07@yahoo.co.id	-	\N	\N
1719	PT. RANGGAH RAJASA UTAMA	18	6471	Jl. PJHI Dalam Gg. Aster No 74 RT 15 Kel. Manggar, Kec. Balikpapan Timur Kota Balikpapan, Kalimantan Timur Kota Balikpapan Kalimantan Timur	ranggahrajasa.u@gmail.com	081253526252	\N	\N
1720	PT. MAHAMERU ENERGI SEMESTA	18	6471	Jl. Sidomulyo No 052 RT 001 Margo Mulyo, Kec. Balikpapan Barat Kota Balikpapan, Kalimantan Timur Kota Balikpapan Kalimantan Timur	mahameru.energi@gmail.com	082157890013	\N	\N
1721	PT. TRI BHAKTI UTAMA	18	6471	Jl. Komp. Sepinggan Pratama Blok SQ - 2 No. 10 Rt. 47 Kel. Sepinggan Kec. Balikpapan Selatan Kota Balikpapan Kalimantan Timur	tri_bhakti_utama@yahoo.co.id	08125436476	\N	\N
1722	PT. BALIKPAPAN BHAKTI COKALINDO	18	6471	JL.SYARIFUDDIN YOES KOMP.SEPINGGAN PRATAMA BLOK SQ-2 No.10 RT.47 BALIKPAPAN Kota Balikpapan Kalimantan Timur	bbc_balikpapan@yahoo.co.id	0542-423680	\N	\N
1723	PT. NUSA JAYA MANDIRI	18	6471	Komp.PGRI Blok III No.5 Gunung Bahagia Kec.Balikpapan Selatan Kota Balikpapan Kalimantan Timur	pt.nusajayamandiribpp@gmail.com	-	\N	\N
1724	PT. BUMI INDAH ELECTRIC	18	6471	Komplek Balikpapan Baru Blok B3 No. 19 Rt. 05 Kel. Damai Baru Kec. Balikpapan Selatan Kota Balikpapan Kalimantan Timur	bimiindahelectric@gmail.com	-	\N	\N
1725	PT. ANUGRAH MIRAN MANDIRI	18	6471	JL. TUMARITIS RT.42 GANG LUMBA-LUMBA, BALIKPAPAN KELURAHAN GRAHA INDAH KECAMATAN BALIKPAPAN UTARA Kota Balikpapan Kalimantan Timur	anugrahmandiri2004@gmail.com	-	\N	\N
1726	PT. MAMMIRI TEKNIK UTAMA	18	6471	Jl. Jendral A.Yani Rt.14 No. 43 Kel. Gunung Sari Ilir Kec. Balikpapan Tengah Kota Balikpapan Kalimantan Timur	cvmammiriteknik@yahoo.com	-	\N	\N
1727	PT. AQFA PERSADA TEKNIK	18	6471	Jl. Flamboyant Rt. 14 No.43 Gunung Sari Ilir Kec. Balikpapan Utara Kota Balikpapan Kalimantan Timur	cv.aqfapersadateknik@yahoo.com	-	\N	\N
1728	PT. SEMERU AGUNG TRIMUKTI	18	6471	Jl. MT. Haryono (Ring road) 303 Kota Balikpapan Kalimantan Timur	sa.trimukti@yahoo.com	-	\N	\N
1729	PT. KASAI JAYA AGUNG	18	6474	Jl. AP. Mankunegoro No. 3A Rt. 5 Kel. Berebes Tengah Kec. Bontang Selatan Kota Bontang Kalimantan Timur Kota Bontang Kalimantan Timur	kjaakin@yahoo.co.id	0548-3033079	\N	\N
1730	PT. ALFAN NIA JAYA	18	6474	JL. KRIKIL RT.13, KEL. BONTANG KUALA, KEC. BONTANG UTARA, KALTIM Kota Bontang Kalimantan Timur	alfan.niajaya@gmail.com	082155816089	\N	\N
1731	PT. MOETIARA WANGI TIMOER	18	6474	Jl. KS Tubun Gg. PLN No. 34 Rt 32 Kel Api-api, Kec Bontang Utara Kota Bontang Kalimantan Timur	moetiarawangitimoerpt@yahoo.co.id	-	\N	\N
1732	PT. ADHY YASA ELEKTRIK	18	6474	Jl. Kalimantan Block F-02 RT.19 Kel. Api-Api, Kec. Bontang Utara, Kota Bontang, Kalimantan Timur Kota Bontang Kalimantan Timur	pt.adhyyasa@gmail.com	-	\N	\N
1733	PT. TEKNIK YASA EKATAMA	18	6474	JL. Sumatera A-10A RT. 19 Kel. Api-api Kec. Bontang Utara Kota Bontang Kota Bontang Kalimantan Timur	teyatama@gmail.com	-	\N	\N
1734	PT. ATIGA CAHAYA SEJAHTERA UTAMA	18	6474	Jl. Denpasar Rt 12 Kel. Gunung Telihan Kec Bontang Barat Kota Bontang Kalimantan Timur Kota Bontang Kalimantan Timur	bsr_bontang@yahoo.com	-	\N	\N
1735	PT. SINAR JAYA ELECTRIK	18	6474	JL. MT. HARYONO GG. TELKOM NO. 48 Kota Bontang Kalimantan Timur	sinarjaya.cv01@gmail.com	-	\N	\N
1736	PT. MAHKOTA RAJA KALTIM	18	6474	JL. ZAMRUD GG. ZAMRUD 10 BERBAS TENGAH Kota Bontang Kalimantan Timur	mrk.faozan@yahoo.com	-	\N	\N
1737	PT. ARY PRATAMA SEJAHTERA	18	6472	JL. Telkom Gg. Elot No. 35 RT. 019 Kel. Sambutan Kec. Sambutan Kota Samarinda Kalimantan Timur	ary.pratama.sejahtera@gmail.com	08125525151	\N	\N
1738	PT. PELITA BUANA KOMBANG	18	6472	JL. IR. H. JUANDA NO. 45 RT. 059 KEL. SIDODADI KEC. SAMARINDA ULU KOTA SAMARINDA PROVINSI KALIMANTAN TIMUR Kota Samarinda Kalimantan Timur	pelitabuanakombang@gmail.com	085288880188	\N	\N
1739	PT. MANDIRI SURYA ALAM	18	6472	Perum. Sampaja Lestari Jl. Rubi Blok TA No. 03 RT. 01 Samarinda Kel. Sempaja Timur Kec. Samarinda Utara Kota Samarinda Kalimantan Timur	mandirisurya@gmail.com	0541-6221926	\N	\N
1740	PT. SINAR TERANG PURNAMA	18	6472	Jl. Jend. A Yani II RT.09 No. 06 Samarinda Kalimantan Timur Kota Samarinda Kalimantan Timur	pt.sinarterangp@gmail.com	(0541) 733844, 7079169	\N	\N
1741	PT. CITRA SURYA PERDANA	18	6472	JL. BUKIT BARISAN RT. 20 NO. 117 SAMARINDA Kota Samarinda Kalimantan Timur	pt_citrasuryaperdana@yahoo.co.id	(0541) 731643	\N	\N
1742	PT. KARYA LANGGENG ABADI	18	6472	Jl. Sentosa Gg. Kenangan IX No.97 RT. 75 Kel. Sungai Pinang Dalam, Kec. Sungai Pinang, Kab. Kota Samarinda Kota Samarinda Kalimantan Timur	pt.karyalanggengabadi@gmail.com	0541-771071 / 081297669372	\N	\N
1743	PT. PESONA PRIMA GEMILANG	18	6472	JL.M. SAID KOMP. ELEKTRIK MAS BLOK H NO.55 RT.25 KEL. LOK BAHU KEC. SUNGAI KUNJANG KOTA SAMARINDA Kota Samarinda Kalimantan Timur	pesona.primagemilang@gmail.com	0541271428	\N	\N
1744	PT. MAHAKAM LEMBU MULAWARMAN	18	6472	Jl. Pakis Merah 17 Blok D No 649 RT 044 Kel. Sempaja Utara, Kec. Samarinda Utara Kota Samarinda Kalimantan Timur	lskn_pusat@yahoo.co.id	081585076531	\N	\N
1745	PT. MATARIM	18	6472	JL. MUSO SALIM No. 90 RT. 15 KEL. KARANG MUMUS SAMARINDA Kota Samarinda Kalimantan Timur	pt.matarimsmr@yahoo.co.id	0541-206984	\N	\N
1746	PT. FAJAR KHATULISTIWA BERSAUDARA	18	6472	JL. MAS PENGHULU GANG KARYA BARU KEL. TENUN KEC. SAMARINDA SEBERANG Kota Samarinda Kalimantan Timur	fajar.khatulistiwasmd@yahoo.co.id	081346247683	\N	\N
1747	PT. GARUDA KENCANA INDONESIA	18	6472	JL. LABU SIAM 2 PERUM BENGKURING BLOK. B NO. 46 RT. 032, SEMPAJA TIMUR KEC. SAMARINDA UTARA Kota Samarinda Kalimantan Timur	garudakencanaindonesia@gmail.com	081347698969	\N	\N
1748	PT. NIEFA MIEMI UTAMA	18	6472	JL. MERDEKA BLOK B NO. 92 KEL. SUNGAI PINANG DALAM Kota Samarinda Kalimantan Timur	niefapt@yahoo.co.id	0541734701	\N	\N
1749	PT. ARIS BERSAUDARA ABADI	18	6472	JL. WIRATAMA NO. 74 RT. 04 SIDODADI KEC. SAMARINDA ULU Kota Samarinda Kalimantan Timur	arisbersaudaraabadi@gmail.com	0541-4118916	\N	\N
1750	PT. WIJAYA KARYA ANUGRAH	18	6472	JL. PM. NOOR, PERUM PONDOK SURYA INDAHBLOK CA 15 Kota Samarinda Kalimantan Timur	sales.wka@gmail.com	-	\N	\N
1751	PT. TRI KARYA LENALATU	18	6472	Jl. Arjuna Gang. 2 Rt. 13 No. 049 Kel. Jawa Kec. Samarinda Ulu Kota Samarinda Kalimantan Timur	trikaryalenalatu@gmail.com	-	\N	\N
1752	PT. TATA NURUL BESTARI	18	6472	Jl. MT. Haryono Rawasari 3 Rt. 011 Kel. Air Putih Kec. Samarinda Ulu Kota Samarinda Kalimantan Timur	pt.tatanurulbestari@yahoo.com	-	\N	\N
1753	PT. PERKASA PRATAMA MANDIRI	18	6472	Jl. Jakarta Blok BE No.20 Rt.58 Kel. Loa Bakung Kec. Sungai Kunjang Kota Samarinda Kalimantan Timur	perkasa.pratama.mandiri@gmail.com	-	\N	\N
1754	PT. PUTRI TRIYOGA GEMILANG	18	6472	JL. Komp. Solong Durian A3 No.026 RT.027 Kelurahan Sempaja Utara Kecamatan Samarinda Utara Kota Samarinda, Kalimantan Timur Kota Samarinda Kalimantan Timur	pt.putritriyogagemilang@gmail.com	-	\N	\N
1812	PT. GARUDA MUDA ELEKTRIK	14	5205	Dusun Rasabou RT.001 RW.002 Desa Ranggo/Desa Temba Lae Kecamatan Pajo Kabupaten Dompu Propinsi Nusa Tenggara Barat Kab. Dompu Nusa Tenggara Barat	elektrik_garuda@yahoo.com	087866905305	\N	\N
1813	PT. FAZA MUBAROK ABADI JAYA	14	5201	Jl. Rajawali Blok K5 No.18 Perumahan Bumi Harapan Permai, Dusun Karang Bongkot, Desa Karang Bongkot, Kecamatan Labuapi, Kabupaten Lombok Barat, Nusa Tenggara Barat Kab. Lombok Barat Nusa Tenggara Barat	pt.fazamubarok@gmail.com	087864117252	\N	\N
1814	PT. LOMBOK MANUNGGAL ELEKTRIK	14	5201	DUSUN PERESAK, DESA MENINTING KECAMATAN BATULAYAR Kab. Lombok Barat Nusa Tenggara Barat	Manunggal.Electric@gmail.com	081917959222	\N	\N
1815	PT. DETRA ADIYASA PUTRA MANDIRI	14	5201	JALAN RAYA JATIMAS BLOK C.NO.06 BTN PEMDA,DUSUN PERUMDA DESA DASAN TAPEN KEC.GERUNG KABUPATEN LOMBOK BARAT NUSA TENGGARA BARAT Kab. Lombok Barat Nusa Tenggara Barat	dapmandiri.pt2017@gmail.co	087864003292	\N	\N
1816	PT. LOMBOK TEKNIK PRATAMA	14	5205	Jalan Raya Senggigi Dusun Melase Desa Batu Layar Barat Kab. Lombok Barat Nusa Tenggara Barat	lombokteknik9@gmail.com	085337165353	\N	\N
1817	PT. ANTAR NUSA TEKNIK	14	5201	DUSUN PERESAK, DESA MENINTING RT. 02 KECAMATAN BATU LAYAR, KABUPATEN LOMBOK BARAT - NTB Kab. Lombok Barat Nusa Tenggara Barat	antarnusateknik@ymail.com	08122928129	\N	\N
1818	PT. KARYA INSANI JAYA	14	5201	DN. MEDAIN BARAT DS.BADRAIN, KEC. NARMADA KABUPATEN LOMBOK BARAT Kab. Lombok Barat Nusa Tenggara Barat	abdul.manap25@gmail.com	087865890002	\N	\N
1819	CV. PUTRA MANDAR	14	5201	Kampung Mandar, Desa Lab. Lombok Kec. Pringgabaya Kab. Lombok Barat Nusa Tenggara Barat	putramandar462@yahoo.com	-	\N	\N
1820	PT. LOMBOK BALI SUMBAWA	14	5201	Jl. Perempung No. 7x Dusun Perempung Desa Sandik, Kec. Batu Layar Kab. Lombok Barat Nusa Tenggara Barat	ganda058@gmail.com	-	\N	\N
1821	PT. TRANS MEKANIKAL ELEKTRIK	14	5202	JAGO DESA JAGO Kab. Lombok Tengah Nusa Tenggara Barat	transmekanikalelektrik@gmail.com	085253000421	\N	\N
1822	PT. ANUGRAH PELANGI TEHNIK	14	5202	Jl. Raya Puyung - Jago Desa Gemel Kecamatan Jonggat Kabupaten Lombok Tengah NTB Kab. Lombok Tengah Nusa Tenggara Barat	anugrahpelangitehnik@gmail.com	08175729405	\N	\N
1823	PT. HAMUN ABADI TEHNIK	14	5202	LENDANG DODE,DESA BAREBALI KEC. BATUKLIANG KAB. LOMBOK TENGAH NTB Kab. Lombok Tengah Nusa Tenggara Barat	hamunabadi@gmail.com	08175796653	\N	\N
1824	CV. MITRA SURYA	14	5202	DUSUN NGOLAK DESA LAJUT PRAYA TENGAH Kab. Lombok Tengah Nusa Tenggara Barat	mitrasurya2018@gmail.com	081805205758	\N	\N
1825	CV. KARYA SEMARANG TEKNIK	14	5202	Polak Penyayang, Kelurahan gerunung, Kecamatan Praya. Kabupaten Lombok Tengah. NTB Kab. Lombok Tengah Nusa Tenggara Barat	suarlehanusa1234@gmail.com	087865575603	\N	\N
1826	PT. SILVIA LOMBOK ELEKTRINDO	14	5202	DESA JAGO KECAMATAN PRAYA KAB. LOMBOK TENGAH, NUSA TENGGARA BARAT Kab. Lombok Tengah Nusa Tenggara Barat	silviateknik@gmail.com	081805729528	\N	\N
1827	CV. SINAR TIMUR	14	5202	Dusun Lilin II Desa Bebuak Kec. Kopang Kab. Lombok Tengah Nusa Tenggara Barat	sinartimur44@gmail.com	-	\N	\N
1828	PT. WINATA MANDIRI SEJAHTERA ABADI	14	5203	JL.CUTNYAKDIEN SANDUBAYA SELONG Kab. Lombok Timur Nusa Tenggara Barat	pt.winatamandirisejahteraabadi@gmail.com	(0376)2991663	\N	\N
1829	CV. FATHIR ELEKTRIKAL	14	5203	Gelumpang desa Rumbuk Timur Kec. Sakra Kab. Lombok Timur Nusa Tenggara Barat	atharpln01@gmail.com	-	\N	\N
1830	PT. AKSYA MANDIRI	14	5204	Jln. Pendidikan no.2 desa Goa, Kecamatan Jereweh Kabupaten Sumbawa Barat, Nusa Tenggara Barat Kab. Sumbawa Nusa Tenggara Barat	aksya_mandiri@yahoo.co.id	085333706201	\N	\N
1831	PT. TIRTANING PUTRA MANDIRI	14	5204	JL SETIA BUDI NO 1 SUMBAWA BESAR Kab. Sumbawa Nusa Tenggara Barat	tirtaningrum27@gmail.com	082146796303	\N	\N
1832	PT. IDA KURNIA ABADI	14	5204	JL SETIA BUDI NO 1 SEKETENG KECAMATAN SUMBAWA Kab. Sumbawa Nusa Tenggara Barat	kiaabadi@gmail.com	08123789022	\N	\N
1833	PT. INDAH KARYA NUSRAINDO	14	5207	Jl. Ahmad Yani RT. 02 RW. 03 Kuang Taliwang Kuang Kec. Taliwang Kab. Sumbawa Barat, Nusa Tenggara Barat Kab. Sumbawa Barat Nusa Tenggara Barat	pt.indahkaryanusraindo@gmail.com	0372-81676	\N	\N
1834	PT. RANDIKA TELAGA RABA	14	5272	Jl. Ir. Soetami Rt. 018 Rw. 006 Lingkungan Nggaro Kumbe Kel. Rabadompu Timur Kec. Raba Kota Bima Kota Bima Nusa Tenggara Barat	telagaraba85@yahoo.com	0374-42831/ 082359071749	\N	\N
1835	PT. NUSA INA PUTRA	14	5272	Jl. Belimbing No. 99 RT.008 RW.003 Raba Dompu Raba Dompu Barat Kec. Raba Kota Bima NTB Kota Bima Nusa Tenggara Barat	nusa_inaputra@yahoo.com	085339112374	\N	\N
2210	PT. DUTA LISTRIK PRATAMA	22	7472	Jl. Bulawambona Kota Bau Bau Sulawesi Tenggara	anugerahpratama053@gmail.com	-	\N	\N
1836	PT. OLYMPIC DELTA ELEKTRIKAL	14	5272	JL. DAM RONTU NO.08 RT 02 RW 01 RABANGODU SELATAN KEC. RABA Kota Bima Nusa Tenggara Barat	OLYMPIC.RABA@GMAIL.COM	0374-45065	\N	\N
1837	PT. UJUNG LIMA PERKASA	14	5271	JL DUKUH SALEH NO. 10 PEJERUK PEJERUK KEC. AMPENAN KOTA MATARAM, PROV. NUSA TENGGARA BARAT Kota Mataram Nusa Tenggara Barat	pt.ujung5p@gmail.com	0370-640999	\N	\N
1779	PT. GIMETRIDA INDO KARYA	4	2102	Perum Hawaii Garden Blok H No.3A,Kel Belian , Kec Batam Kota. Kota Batam - Kepri Kab. Bintan Kepulauan Riau	tonny.simaremare@gimetridaindokarya.com	0778-7495972/ 08117004881	\N	\N
1780	PT. CAHAYA PELANGI ENERGY	4	2101	JL.SUNGAI AYAM RT.01 ,RW.03 KELURAHAN KAPLING ,KECAMATAN TEBING, KABUPATEN KARIMUN Kab. Karimun Kepulauan Riau	cahayapelangi261175@gmail.com	085271279933	\N	\N
1781	PT. PUTRA MANDIRI ELEKTRIK	4	2101	JL.JABAR NUR RT.06/RW.01 NO.60 BUKIT SENANG TANJUNG BALAI KARIMUN Kab. Karimun Kepulauan Riau	marliaspnw@gmail.com	07777360430/081270383464	\N	\N
1782	PT. MERAPI ELEKTRIKAL JAYA	4	2101	JL.SOEKARNO HATTA POROS RT.01, RW03 KELURAHAN HARJOSARI KECAMATAN TEBING KABUPATEN KARIMUN Kab. Karimun Kepulauan Riau	merapielektrikal75@gmail.com	08127758018	\N	\N
1783	PT. SATRIA AULIA KENCANA ABADI	4	2101	JL.MT HARYONO TELUK USA RT. 04/RW.02 KELURAHAN TELUK USA KECAMATAN TEBING KABUPATEN KARIMUN Kab. Karimun Kepulauan Riau	satriaauliakencanaabadi@gmail.com	085376776722	\N	\N
1784	PT. INDAH CAHAYA ELEKTRIKAL	4	2101	JL. MT HARYONO RT. 03, RW02 KELURAHAN TEBING KECAMATAN TEBING KABUPATEN KARIMUN. Kab. Karimun Kepulauan Riau	nandamuharif@gmail.com	081270046620	\N	\N
1785	PT. CAHAYA NASYWA ELEKTRIK	4	2101	JL. PENDIDIKAN BUKIT SENANG TANJUNG BALAI KARIMUN Kab. Karimun Kepulauan Riau	edichandra8888@gmail.com	-	\N	\N
1786	PT. LANGIT BIRU ELEKTRIKAL, PT	4	2101	IVORA JAYA NO. 85 SUNGAI RAYA MERAL KEC. MERAL Kab. Karimun Kepulauan Riau	pt.lbe@yahoo.com	-	\N	\N
1787	PT. MARIO PRATAMA MANDIRI	4	2103	JL. PATIMURA-PERING RT.001 RW.006 KELURAHAN BANDARSYAH KECAMATAN BUNGURAN TIMUR Kab. Natuna Kepulauan Riau	cv.mariopratama@gmail.com	081364096912	\N	\N
1788	PT. ANODA ELEKTRO ABADI	4	2172	JL. Pramuka, Lr Bali No.02. Bukit Bestari. Kec Tanjung Ayun Sakti. kota Tanjungpinang Kota Tanjung Pinang Kepulauan Riau	anoda_elektro@yahoo.com	081364545464 / 077123217	\N	\N
1789	PT. Sentra Cahaya Trapindo	4	2172	Jl.DI Panjaitan No.13 Tanjungpinang Air Raja Kec. Tanjungpinang Timur Kota Tanjung Pinang Kepulauan Riau	bennysct1@gmail.com	08117048855	\N	\N
1790	PT. ONENOV WIMA PUTRI	4	2172	JL. BUNGURAN NO.11 PERUMNAS PEMUDA TANJUNG PINANG SEI JANG KEC. BUKIT BESTARI KOTA TANJUNGPINANG, KEPULAUAN RIAU Kota Tanjung Pinang Kepulauan Riau	pt.onenov2016@yahoo.com	0811699574 , 0778-460897	\N	\N
1791	PT. SURYA PINANG TEKNIK	4	2172	JL. SOEKARNO HATTA NO. 11 - TANJUNGPINANG Kota Tanjung Pinang Kepulauan Riau	shntpi@hotmail.com	077126009	\N	\N
1792	PT. PUTRA INHU MEGA PRATAMA	4	2172	Jl. Cendrawasih No.55 002/003 Batu IX Kel. Batu IX Kec. Tanjungpinang Timur Kota Tanjungpinang Kepulauan Riau Kota Tanjung Pinang Kepulauan Riau	putra_inhu1@yahoo.co.id	+6271144302,+6277144302	\N	\N
1793	PT. Surya Murgana Sinergi	4	2172	Jl. Adi Sucipto Km.10 Perum. Kijang Kencana II Blok A No.8 Pinang Kencana Kec. Tanjung Pinang Timur Kota Tanjung Pinang Kepulauan Riau	surya.murgana@yahoo.com	(0771) 41183	\N	\N
1794	PT. JURTI AGUNG MULIA	4	2172	JL,SULTAN SULAIMAN NO.44 KP,BULANG TANJUNGPINANG Kota Tanjung Pinang Kepulauan Riau	irwansyahktu99@yahoo.com	085315070125	\N	\N
1795	PT. KEKAR JAYA MANDIRI	4	2172	JL. SULTAN MACHMUD NO.88 Kota Tanjung Pinang Kepulauan Riau	kekarjaya@yahoo.co.id	(0771)28300/081275596299	\N	\N
1796	PT. MEGUNG ANUGERAH REJEKI	4	2172	Jl Brigjen Katamso GG Kenanga III No 09 Kel Tanjung Unggat Kec Bukit Bestari Kota Tanjungpinang Kota Tanjung Pinang Kepulauan Riau	megungrejeki@yahoo.com	081372207552	\N	\N
1797	PT. MADANI SARANA SUKSES	4	2172	Jl. R.H Fisabilillah Gg. Pulau Pandan No. 93 Tanjung Pinang Kec. Bukit Bestari Kota Tanjung Pinang Kepulauan Riau	madanisaranasukses@gmail.com	07717330242	\N	\N
1798	PT. SINAR ELECTRIKAL	4	2172	jln. soekarno hatta no.10 Tanjungpinang Kota Tanjung Pinang Kepulauan Riau	sinarelectrikal@yahoo.com	-	\N	\N
1799	PT. WANNUSUKI GLOBAL ENGINEERING	4	2172	RUKO PURI INDAH NO 2 JALAN SUKA RAMAI KEL. PINANG KENCANA KEC. TANJUNGPINANG TIMUR Kota Tanjung Pinang Kepulauan Riau	pt.wannusuki@yahoo.com	-	\N	\N
1800	PT. ARDE JAYA MANDIRI	4	2172	Jalan Brigjen Katamso Lt.2 No 79 Kel Kamboja Kec. Tanjung Pinang Barat Kota Tanjung Pinang Kepri Kota Tanjung Pinang Kepulauan Riau	ajmandiri_2016@yahoo.com	-	\N	\N
1801	PT. ZAHRA BINTAN MANDIRI	4	2172	Jalan Soekarno Hatta no 06 RT 004 RW 009, Kelurahan Tanjung pinang Barat, Kecamatan Tanjung pinang Barat Kota tanjung pinang Kota Tanjung Pinang Kepulauan Riau	darvi.izaf@yahoo.co.id	-	\N	\N
1838	PT. RINA MITRA LESTARI	14	5271	BTN. LIngkar Permai Blok D.13 Tanjung Karang Kec. Sekarbela Kota Mataram Nusa Tenggara Barat	rinateknik.d13@gmail.com	08175750026	\N	\N
1839	PT. PUTRA ANJANI MANDIRI	14	5271	Jl. Baru No. 30 Pintu Air Kel.Ampenan Tengah, Ampenan Tengah, Kec. Ampenan Kota Mataram Nusa Tenggara Barat	pt.putraanjani@gmail.com	0370-629434	\N	\N
1840	PT. PANJI ELEKTRIK	14	5271	Jl. Barito Raya No. 16 Tanjung Karang permai, Sekarbela Kel. Tanjung Karang Permai Kel. Tanjung Karang Permai Kec. Sekarbela Kota Mataram Nusa Tenggara Barat	panji_electric@yahoo.com	0370-626373	\N	\N
1841	PT. BATU SELA TEHNIK	14	5271	Jl. Tanggul Gang Anggrek Sukaraja Timur Ampenan Tengah Kota Mataram Kota Mataram Nusa Tenggara Barat	batu.sela@yahoo.com	085337090231	\N	\N
1842	PT. ELITE CENTAUR INDONESIA	14	5271	Jalan Bangil V No. 5 Taman Baru Lombok Pagesangan Kec. Mataram Kota Mataram Nusa Tenggara Barat Kota Mataram Nusa Tenggara Barat	elitecentaur@yahoo.com	0370620886	\N	\N
1843	PT. BIRO TEKNIK DELTA	14	5271	JL. ALAS I NO. 19 AMPENAN KEC. TAMAN SARI Kota Mataram Nusa Tenggara Barat	biro.teknik@yahoo.com	0370-644190	\N	\N
1844	PT. TIDAR GALA YUDHA	14	5271	JL. ALAS I NO. 12 AMPENAN KEC. TAMAN SARI Kota Mataram Nusa Tenggara Barat	tidargalayudha@gmail.com	0370-7505349	\N	\N
1845	PT. SAKA ANGKASA MULIA	14	5271	JL. RAJAWALI, DASAN CERMEN BARAT, DASAN CERMEN KEC. SANDUBAYA Kota Mataram Nusa Tenggara Barat	pt.samulia@gmail.com	081339888759	\N	\N
1904	PT. PUTRA BANGKIT MANDIRI SEJATI	15	5371	JL.R.A KARTINI KELAPA LIMA Kota Kupang Nusa Tenggara Timur	samba.puaharun@gmail.com	082144958354	\N	\N
1555	PT. TRI RAKHMAT UTAMA	20	6106	JL.KARYA TANI NO.26 RT.009 RW.001 KEL.SUKAHARJA KEC.DELTA PAWAN KABUPATEN KETAPANG Kab. Ketapang Kalimantan Barat	pt.trirakhmatutama@gmail.com	(0534)33907/ 085 387 084 411	\N	\N
1556	PT. RAJA INTAN ELECTRICAL	20	6106	Jalan Merdeka, Kelurahan Kalibambang, Kecamatan Kali Upas, Kapupaten Ketapang, Kalimantan Barat Kab. Ketapang Kalimantan Barat	rajaintan_electrical@gmail.com	+62 852 46144444	\N	\N
1557	PT. MITRA MEKANIKAL ELEKTRIKAL	20	6106	Jl. Beringin Gg. Padi No. 30 RT. 015 RW. 005, Kelurahan Tengah, Kecamatan Delta Pawan, Kabupaten Ketapang, Kalimantan Barat Kab. Ketapang Kalimantan Barat	multimitra_m@yahoo.com	+62 811 5708073	\N	\N
1558	CV. RAJA INTAN	20	6106	Jalan Brigjend Katamso RT. 030 RW. 004 Kel. Sukaharja Kec. Delta Pawan Kab. Ketapang Kalimantan Barat	rajaintan.erictrical01@gmail.com	-	\N	\N
1559	PT. NUSA DELTA PANDU	20	6171	Jl.Harapan RT.006 RW.002 Kelurahan Anjungan Dalam,Kec.Anjongan Kabupaten Mempawah,Kalimantan Barat Kab. Pontianak Kalimantan Barat	nusadeltapandu@gmail.com	0561691020/081352533777	\N	\N
1560	PT. RUMPUN UTAMA SAMBAS	20	6101	DUSUN KUBU RT 01 RT 01 DESA DURIAN KEC. SAMBAS Kab. Sambas Kalimantan Barat	gelisutama.sambas@gmail.com	085246621387	\N	\N
1561	PT. MARABUNTA ANUGRAH SAMBAS	20	6101	DUSUN LUMBUNG SARI RT. 12 RW.06 DESA PENDAWAN KEC. SAMBAS Kab. Sambas Kalimantan Barat	marabunta197@gmail.com	089689478016	\N	\N
1562	PT. MEGA JAYA ELECTRINDO	20	6105	Jl. Pangeran Paduka Rt. 03 No. 145 Balai Karangan Kec. Sekayam Kab. Sanggau Kalimantan Barat	pt.megajayaelectrindo@gmail.com	0564-2022022	\N	\N
1563	PT. KOPERASI KARYAWAN LISTRIK SANGGAU	20	6105	Jl. Dr. Setia Budi No. 1 Rt. 006 Rw. 002 Kel. Beringin Kab. Sanggau Kalimantan Barat	koperasi_karlisa@gmail.com	(0564) 23569	\N	\N
1564	PT. BINTANG NUR SYAHBAS	20	6105	JL.Cermai No.33 RT.01 RW.001 Kel. Tanjung Kapuas Kec. Kapuas Kab. Sanggau, Kalimantan Barat Kab. Sanggau Kalimantan Barat	bintangnursyahbas_sgu@yahoo.com	0564 - 22810	\N	\N
1565	PT. RODA TEKNIK ENERGI	20	6105	jalan mungguk badang no. 2 sanggau RT.021/RW.007, Kec. Kapuas, Kab. Sanggau Kab. Sanggau Kalimantan Barat	rodateknik11@gmail.com	0564-2020026/ 081352137558	\N	\N
1566	PT. CAHAYA FAJAR ELEKTRINDO	20	6107	Jl. Jeroja II Rt. 002 / Rw. 001 Kel. Akcaya Kec. Sintang Kab. Sintang Kalimantan Barat	pt.cfe81@yahoo.com	0821- 48554290	\N	\N
1567	PT. PUTRA SITI HAJAR	20	6171	Jl. Sepakat II Blok Q No. 184 RT. 04 / RW. 007 Kel. Bansir Laut Kec. Pontianak Tenggara Kota Pontianak Kalimantan Barat	cv.putrasitihadjar@gmail.com	0561- 771742,	\N	\N
1568	PT. SATRIA ANUGRAH USAHA	20	6171	Jl. Perdana Gg. Sejahtera No. 2 Parit Tokaya Kec. Pontianak Selatan Kota Pontianak Kalimantan Barat	satria.sau18@gmail.com	0813-52435599	\N	\N
1569	PT. PUTRA CANDIMULYO MANUNGGAL	20	6171	Jl. Sungai Landak Timur Tanjung Hulu Kec. Pontianak Timur Kota Pontianak Kalimantan Barat	pt.putracandimulyomanunggal@gmail.com	0561- 575997, 0561 - 746404	\N	\N
1570	PT. Mekar Karya Sejahtera Kalbar	20	6171	Jl. H. Rasuna Said Gg. Mabang 4 No. 56 Tanjung Hulu Kec. Pontianak Timur Kota Pontianak Kalimantan Barat	mekarkarya66@yahoo.com	081345146809	\N	\N
1571	PT. DWI PRATAMA SEJAHTERA	20	6171	Jl. Johar Gg. Batu Bara 2 No. 6 Darat Sekip Kec. Pontianak Kota Kota Pontianak Kalimantan Barat	pt.dwipratamasejahtera75@gmail.com	0823 54383717	\N	\N
1572	PT. MERAUKE ARTHA PERDANA	20	6171	JL. PUTRI DARANANTE-PAWANJAYA NO. 53 PONTIANAK Kota Pontianak Kalimantan Barat	pt.meraukearthaperdana@gmail.com	0561-730549/089693253185	\N	\N
1573	PT. ARNISA AZKA TEKNINDO	20	6171	JL. DR. WAHIDIN SUDIROHUSODO KOMP. BATARA INDAH I BLOK AA NO. 28 PONTIANAK Kota Pontianak Kalimantan Barat	pt.arnisaazkateknindo@gmail.com	0561-7900291/085245922004	\N	\N
1574	PT. ERA TRI WIRATAMA	20	6171	JL. PARIT H. HUSIN 2 KOMP. ALEX GRIYA PERMAI 3 BLOK F NO. 15 RT.005 RW.002 KEL. BANSIR DARAT KEC.PONTIANAK TENGGARA KOTA PONTIANAK KALIMANTAN BARAT Kota Pontianak Kalimantan Barat	eratriwiratama@gmail.com	08125635572	\N	\N
1575	PT. JAYA MANDIRI ELEKTRINDO	20	6171	JL. SAMAN HUDI, GG. RAMIN VI No.86 TANJUNG HULU, KEC. PONTIANAK TIMUR Kota Pontianak Kalimantan Barat	jayamandirielektrindo@gmail.com	0561-7931007	\N	\N
1576	PT. TRIO MANUNGGAL SAKTI	20	6171	JL. PANGLIMA AIM NO. 14 KEL. TANJUNG HULU KEC. PONTIANAK TIMUR Kota Pontianak Kalimantan Barat	triomanunggalsakti.pt@gmail.com	0561 - 6592904	\N	\N
1577	PT. SINAR CANDIMULYO ABADI	20	6171	Jl. Tekam Komp. saigon Permai No. 32 Kel.Saigon Kec. Pontianak timur Kota Pontianak Kalimantan Barat	sinarcandimulyo@gmail.com	0561575997	\N	\N
1578	PT. MITRA SOLUSINDO GEMILANG	20	6171	JL UJUNG PANDANG GG SELAT PANJANG Kota Pontianak Kalimantan Barat	ms.gemilang17@gmail.com	05618127770	\N	\N
1579	PT. FAJARINDO AGUNG KALBAR	20	6171	Jl. Dr. Wahidin Sudirohusodo Komp. Sepakat Damai Blok 1 No. 2 Sungai Jawi Kec. Pontianak Kota Kota Pontianak Kalimantan Barat	fajarindo_agunf@yahoo.co.id	0561 585962	\N	\N
1580	PT. SUMBER PARAHIYANGAN	20	6171	JL. PROF M YAMIN GG. PGA NO. 32 RT.007 RW. 018 KEL SUNGAI BANGKONG KEC. PONTIANAK BARAT Kota Pontianak Kalimantan Barat	heldi_sp@yahoo.com	081255716185	\N	\N
1581	PT. BIRO INSTALATIR DIANA	20	6171	Jl. Setia Budi No. 30 Benua Melay Darat Kec. Pontianak Selatan Kota Pontianak Kalimantan Barat	soemedin@asterindo.asia	0561 - 735163, 0561 - 762726	\N	\N
1582	PT. ATTAMIM INDONESIA	20	6171	JL. DR WAHIDIN S KOMPLEK SEPAKAT DAMAI BLOK H NO.8 RT.002 RW.028 KEL. SUNGAI JAWI KEC. PONTIANAK KOTA Kota Pontianak Kalimantan Barat	indattamimpt@gmail.com	081345953383	\N	\N
1583	PT. JAGE MUARE ULAKAN	20	6171	JL. DR WAHIDIN S. KOMP BATAR INDAH I BLOK LL NO.15 PONTIANAK Kota Pontianak Kalimantan Barat	jmu_ptk2012@yahoo.co.id	081345116047	\N	\N
1584	PT. PURNA ELEKTRIKAL KHATULISTIWA	20	6171	Gg. Pala 2 Rt. 001 Rw. 015 Kel. Sungaijawi Luar Kec. Pontianak Barat Kota Pontianak Kota Pontianak Kalimantan Barat	lskn_pusat@yahoo.co.id	081352558887	\N	\N
1585	PT. Endah Tunggal Perkasa	20	6171	Jl.Prof M.Yamin Gg.PGA No.32 Kelurahan Sei Bangkong,Kecamatan Pontianak Kota Pontianak Provinsi Kalimantan Barat Kota Pontianak Kalimantan Barat	etp_ptk@yahoo.com	0561-749485 / 0811573606	\N	\N
1586	PT. KALIMANTAN INTERCONECSI	20	6171	Jl. Prof. Dr. Hamka Gg Padi 08/62 Rt.004/005 Kel. Sungai Jawi Kec. Pontianak Kota Kota Pontianak - Kalaimantan Barat Kota Pontianak Kalimantan Barat	lskn_pusat@yahoo.co.id	082154168899	\N	\N
1587	PT. IRREN JAYA ABADI	20	6171	JL. Ujung Pandang Komp. Kurnia 5 B No. B 10 Kelurahan Sungai Jawi Kec. Pontianak Kota Kota Pontianak Kalimantan Barat	pt.irren.jaya.abadi@gmail.com	0561 - 574718	\N	\N
1588	PT. ARIF BORNEO AZZAHRA	20	6171	Jl. Tanjung Raya II Gg. Kurnia Jaya No. 4 A Saigon Kec. Pontianak Timur Kota Pontianak Kalimantan Barat	pt.arifborneoazzahra@yahoo.com	0561- 576685	\N	\N
1589	PT. BATARA PILAR TEKNIK	20	6171	Jl. H.M Swignyo No. 88 Sungai Jawi Kec. Pontianak Kota Kota Pontianak Kalimantan Barat	pt.batara.pt@gmail.com	0561-774798, 0561-732328	\N	\N
1590	PT. KHARISMA KARYA SEJATI	20	6171	JL. PURNAMA KOMP. PURNAMA ANGGREK IV LT.2 NO. 32 PONTIANAK Kota Pontianak Kalimantan Barat	kharisma.karya@workmail.com	0811573624	\N	\N
1591	PT. ALIFA RIZKI UTAMA	20	6171	Jl. Sukamulya Gg. Sukma 3 No. 2 Sei Jawi Kec. Pontianak Kota Kota Pontianak Kalimantan Barat	pt.alifarizkiutama@gmail.com	0821-52021199	\N	\N
1592	PT. CITRA JALISKA UTAMA	20	6171	Jl. Sultan Abdurrahman No. 09 Akcaya Kec. Pontianak Selatan Kota Pontianak Kalimantan Barat	cijaliska@gmail.com	0561-730226	\N	\N
1593	PT. BANDAR SAKTI RAYA	20	6171	JL. DR. WAHIDIN SUDIROHUSODO NO. 29 A LT. 3 PONTIANAK Kota Pontianak Kalimantan Barat	bandarsaktiptk@yahoo.co.id	0561-585258	\N	\N
1594	PT. ARINSA ENERGY PRATAMA	20	6171	Jl. H. Rais A. Rahman Gg. Hasia No. 2 RT. 002 RW. 016 Kel. Sungai Jawi Dalam Kec. Pontianak Barat Kota Pontianak, Kalimantan Barat Kota Pontianak Kalimantan Barat	pt.arinsa@gmail.com	0852-45217058	\N	\N
1595	PT. PT. BANDAR SAKTI RAYA	20	6171	Jl. Dr. Wahidin Sudirohusodo No. 29 A Lt. 3 Pontianak Kota Pontianak Kalimantan Barat	bandarsaktiptk@yahoo.co.id	0561-585258	\N	\N
1596	PT. RIZKI WIDYATAMA ANUGRAH	20	6171	Jl. M. sohor No. 45 A Pontianak Kota Pontianak Kalimantan Barat	rizki_widyatama.anugrah@contractor.net	0561 570356 / 0811573624	\N	\N
1597	PT. ARISTA KENCANA ABADI	20	6171	JL. DR. WAHIDIN SUDIROHUSODO NO. 29B (LT. 2 ) PONTIANAK Kota Pontianak Kalimantan Barat	arkendi_0708@yahoo.co.id	0561-769760	\N	\N
1598	PT. BORNEO BUJANG BEJI	20	6171	JL.RE.MARTADINATA GG.SEDERHANA 1 NO.30 Kota Pontianak Kalimantan Barat	borneobujangbeji@gamil.com	082254827378	\N	\N
1599	PT. SHERLINDO LESTARI UTAMA	20	6171	JL.H.R.A. Rachman Gg.Tiong Kandang 2 No.60 Pontianak Kota Pontianak Kalimantan Barat	sherlindolestariutama@gmail.com	+6281348444322	\N	\N
1600	PT. karunia mekar teknindo	20	6171	JL. RE MARTADINATA GANG PALA 2 NO. 2 Kota Pontianak Kalimantan Barat	Pt.karuniamekarteknindo@gmail.com	081352558887	\N	\N
1601	PT. MULTI BANGUN ELEKTRIK	20	6171	Jl. Tekam Komp. Saigon Permai No. 32 Saigon Kec. Pontianak Timur Kota Pontianak Kalimantan Barat	ptmultibangunelektrik@yahoo.com	0856 - 5074399	\N	\N
1602	PT. INDRA JAYA ELEKTRIC	20	6171	Jl. Karet G. Intan Permata B 16 Sungai Beliung Kec. Pontianak Barat Kota Pontianak Kalimantan Barat	pt.indrajayaelektric@gmail.com	0561- 6783605	\N	\N
1603	PT. HARAPAN PRIMA NUSANTARA	20	6171	Jl. Swadaya No. 20 Tambelansampit Kec. Kec. Pontianak Timur Kota Pontianak Kalimantan Barat	ptharapanprimanusantara@yahoo.com	0561 - 762844	\N	\N
1604	PT. NAYRA NUSA RAYA	20	6171	Jl. Tabrani Ahmad Gg. Zurriat No. 3 Sungai Jawi Dalam Kec. Pontinak Barat Kota Pontianak Kalimantan Barat	pt.nayranusaraya76@yahoo.com	0561- 774169	\N	\N
1605	PT. TASMANIA BAKTI KARYA	20	6171	Jl. Prof. DR. Hamka No. 62A RT. 004 RW. 005 Kel. Sungaijawi Kec. Pontianak Kota Kota Pontianak Kalimantan Barat	lskn_pusat@yahoo.co.id	-	\N	\N
1606	PT. BORNEO ADELIA PRATAMA	20	6171	Jl. Gajah Mada No. 90 Benua Melayu Darat Kec. Pontianak Selatan Kota Pontianak Kalimantan Barat	devaa_damanik@yahoo.com	-	\N	\N
1607	PT. ERVIJA AMANDA PUTRI	20	6172	Jl. Tengah No. 19 Tengah Kec. Singkawang Tengah Kota Singkawang Kalimantan Barat	ervija.ap@gmail.com	0812 57044477	\N	\N
1608	PT. EKA JAYA ELECTRIK	20	6172	Jl. Bambang Ismoyo No. 28 Kelurahan Jawa Kec. Singkawang Tengah Kota Singkawang Kalimantan Barat	jaya.electrik007@gmail.com	0853-87357666	\N	\N
1609	PT. KARYA ANAK NUSANTARA SAKTI	20	6172	Jalan Suhada Kelurahan Condong, Kecamatan, Singkawang Tengah Kota Singkawang Kalimantan Barat	ptkaryaanaknusantarasakti2017@gmail.com	08125695670	\N	\N
1610	PT. JASA BANGUN ENERGI	20	6172	Jl. Yohana Godang Gg. Sukses No. 63 A RT. 048 RW. 017 Kel. Pasiran Kec. Singkawang Barat Kota Singkawang Kalimantan Barat Kota Singkawang Kalimantan Barat	optimakalbar@gmail.com	0823-51218033	\N	\N
1611	PT. KARYA MURRI SEJAHTERA	20	6172	JL.DEMANG AKUB SUNGAI BULAN RT.001/RW.001 Kota Singkawang Kalimantan Barat	pt.kms16@gmail.com	(056204200225 / 085252232356	\N	\N
1612	PT. WAHANA IMPLEMENTASI ENERGI	20	6172	Jl. Kacang G. Kencana RT. 014 RW. 005 Kel. Sekip Lama Kec. Singkawang Tengah Kota Singkawang Kalimantan Barat Kota Singkawang Kalimantan Barat	win.optek@gmail.com	0823 - 53577771	\N	\N
1846	PT. BIN YAFIL AL ALAM	14	5271	Jalan TGH Lopan Komplek Pertokoan Griya Rumak Asri Blok A No. 16 Dusun Rumak Timur Utara Desa Rumak Kecamatan Kediri Kabupaten Lombok barat Provinsi Nusa Tenggara Barat Kota Mataram Nusa Tenggara Barat	pt.binyafilalalam@gmail.com	0818362244	\N	\N
1847	PT. DAYA SURYA TEKNIK	14	5271	JL.ASAKOTA NO.15 KEL.TAMAN SARI KEC. AMPENAN KOTA MATARAM NTB Kota Mataram Nusa Tenggara Barat	dayasuryatehnik560@gmail.com	082340895701	\N	\N
1848	PT. PUTRA NANDA ABYAN	14	5271	Jln. Adi Sucipto Gg. Al - Rahman Lingk. Penan Kelurahan Pejarakan Karya. Kec. Ampenan Kota Mataram Nusa Tenggara Barat	putra_nanda_abyan@yahoo.co.id	0370-626373	\N	\N
1849	PT. CIPTA TEKNIK ABADI	14	5271	JL. KERTANEGARA V NO. 5 KARANG PULE KEC.SEKARBELA KOTA MATARAM Kota Mataram Nusa Tenggara Barat	cv.ciptateknik33@yahoo.com	081916001888	\N	\N
1850	PT. BINTANG LOMBOK UTAMA	14	5271	Jln. Barito Raya No. 18 Tanjung Karang Kec. Sekarbela Kota Mataram Kota Mataram Nusa Tenggara Barat	bintanglombokutama@yahoo.com	0370 - 626373	\N	\N
1851	PT. DIJA JAYA PERKASA	14	5271	JL. SURABAYA NO. 48 TAMAN BARU MATARAM Kota Mataram Nusa Tenggara Barat	dija_jaya@yahoo.co.id	0370 623384	\N	\N
1852	PT. WARIH KUSUMA PRATAMA	14	5271	JL. BONDOWOSO II NO. 4 PAGESANGAN TIMUR KEC. MATARAM Kota Mataram Nusa Tenggara Barat	wrhkusumapratama@gmail.com	0370-636212	\N	\N
1853	PT. AGUNG LANGGENG SENTOSA	14	5271	JL. BONDOWOSO II NO. 5A TAMAN BARU MATARAM MATARAM TIMUR KEC. MATARAM Kota Mataram Nusa Tenggara Barat	pt.als.lombok@gmail.com	0370-626299	\N	\N
1854	PT. SIGMA TIRTA UTAMA MANDIRI	14	5271	JL. TUNJUNG SARI NO.17 MONJOK TIMUR KECAMATAN SELAPARANG KOTA MATARAM NUSA TENGGARA BARAT Kota Mataram Nusa Tenggara Barat	dewamederupadi@yahoo.com	'0818360489	\N	\N
1855	PT. CIMBUANA PUTRA ADITYA	14	5271	JL. LEO NO. 08 SELAPARANG AMPENAN Kota Mataram Nusa Tenggara Barat	pt.cimbuana@gmail.com	0370-7503008	\N	\N
1856	PT. KURNIA JAYA SAKTI	14	5271	JL. DR. WAHIDIN NO. 6 REMBIGA KEC. SELAPARANG Kota Mataram Nusa Tenggara Barat	kujasa.cv@gmail.com	-	\N	\N
1857	PT. CIPTA BKTI PERSADA ABADI	14	5271	JL. ALAS NO. 2 TAMAN SARI - AMPENAN Kota Mataram Nusa Tenggara Barat	ptcibada@gmail.com	-	\N	\N
1858	PT. KAMILIA UTAMA SENTOSA	14	5271	Jalan Segara Anak No. 45 Ampenan Selatan, Kec. Ampenan, KotaMataram. Tlp. 0370-633700 Kota Mataram Nusa Tenggara Barat	kamilia_utama@yahoo.com	-	\N	\N
1859	PT. hari mukti abadi	14	5271	jl alas no 2 ampenan ntb Kota Mataram Nusa Tenggara Barat	pthamua@gmail.com	-	\N	\N
1860	CV. PANCA KARTIKA	14	5271	Blongas No. 45 Kelurhan Dayen Peken Ampenan., Jl Kota Mataram Nusa Tenggara Barat	pancakartika.cv@yahoo.com	-	\N	\N
1902	PT. SUKSES PERDANA KUPANG	15	5371	JL.ARTHA GRAHA Gg.I No.9 RT.034 RW.009 KELURAHAN TUAK DAUN MERAH KECAMATAN OEBOBO Kota Kupang Nusa Tenggara Timur	thandono@yahoo.co.id	08123761582	\N	\N
1903	PT. Ariston Kupang Optima	15	5371	Jln Gereja Moria Rt25 RW 06 Kelurahan Liliba Kupang NTT Kota Kupang Nusa Tenggara Timur	rudi_foo1592@yahoo.com	081338129410	\N	\N
1905	PT. CAHAYA KENCANA KUPANG	15	5371	JLN. MOHAMMAD HATTA NO.21, KEL. OETETE, KEC. KOTA RAJA, KOTA KUPANG, NUSA TENGGARA TIMUR Kota Kupang Nusa Tenggara Timur	jefry543210@yahoo.com	085333853333 03808430521	\N	\N
1906	PT. TELAGA MULYA JAYA	15	5371	TUAK DAUN MERAH RT.019 RW.005 KELURAHAN TUAK DAUN MERAH KECAMATAN OEBOBO Kota Kupang Nusa Tenggara Timur	dpdaklindontt@yahoo.co.id	081339228945	\N	\N
1907	PT. NAPTUN PUTRA TEKNIK	15	5371	NAIMATA RT.009 RW.003 KELURAHAN NAIMATA KECAMATAN MAULAFA Kota Kupang Nusa Tenggara Timur	dpdaklindontt@yahoo.co.id	082197593444	\N	\N
1908	PT. NENO MAYANA TEKNIK	15	5371	NAIMATA RT.009 RW.003 KELURAHAN NAIMATA KECAMATAN MAULAFA Kota Kupang Nusa Tenggara Timur	dpdaklindontt@yahoo.co.id	081339439314	\N	\N
1909	PT. SANTRINITAS TEKNIK UTAMA	15	5371	JL.FRANS SEDA RT 23 RW 07 KEL.FATULULI KEC OEBOBO Kota Kupang Nusa Tenggara Timur	santrinitas.17utama@gmail.com	081237069947	\N	\N
1910	PT. MAROVAN PUTERA TEKNIK	15	5371	JL TAEBENU -NAIMATA KEL.NAIMATA KEC.MAULAFA KOTA KUPANG Kota Kupang Nusa Tenggara Timur	pt.marovanputerateknik@yahoo.com	082144191555	\N	\N
1911	PT. ANUGERAH KARYA ENGINEERING	15	5371	JL.ELANG NO.15 RT 12/RW 06 KEL. BONIPOI KEC. KOTA LAMA,KOTA KUPANG Kota Kupang Nusa Tenggara Timur	kasdi.anugerah@yahoo.com	08123901557	\N	\N
1912	PT. SONATA CAHAYA TEKNIK	15	5371	Perumahan Bukopin Manulai II Blok A No.04 Rt.14/005 Kel. Manulai II Kec. Alak Kota Kupang Nusa Tenggara Timur	sonatacahayateknik@gmail.com	081351883313	\N	\N
1913	PT. Dutha Terang Tehnik	15	5371	Jl. Damai No 01 RT 003/RW 001 Kel. Oebobo Kec. Oebobo Kota Kupang Nusa Tenggara Timur	ptduthaterangtehnik@gmail.com	081237101285	\N	\N
1914	PT. GELEKAT LEWO TANA	15	5371	RT.35 RW.09 KELURAHAN OEBUFU KECAMATAN OEBOBO KOTA KUPANG NUSA TENGGARA TIMUR Kota Kupang Nusa Tenggara Timur	gelekat14@yahoo.com	081337191273	\N	\N
1915	CV. ADONARA PUTRA TEKNIK	15	5371	RT.35 RW.09 KELURAHAN OEBUFU KECAMATAN OEBOBO KOTA KUPANG - NTT Kota Kupang Nusa Tenggara Timur	cv.adonaraputrateknik@yahoo.co.id	081 337 191 273	\N	\N
1916	PT. TEON JAYA	15	5371	JL. AMTARAN NO. 10 KELURAHAN FATULULI KECAMATAN OEBOBO KOTA KUPANG, NUSA TENGGARA TIMUR Kota Kupang Nusa Tenggara Timur	pt.teonjaya@ymail.com	081339095888	\N	\N
1917	CV. MEGA TAMA TEKNIK	15	5371	Jl. Nasution III Rt.022/006 Kayu Putih Kec. Oebobo Kota Kupang Nusa Tenggara Timur	megatamateknik@gmail.com	0380	\N	\N
1918	CV. TIMOR BERSINAR	15	5371	Jl. Perintis Kemerdekaan Rt.037/010 Oebufu Kel. Oebufu Kec.Kecamatan Oebobo Kota Kupang Nusa Tenggara Timur	cv.timorbersinar@yahoo.com	0380	\N	\N
1919	PT. DELCON TERANG INDO	15	5371	JL. HATI MULIA V/3 OEBOBO KUPANG Kota Kupang Nusa Tenggara Timur	pt.delconterangindo@gmail.com	081339336744	\N	\N
1920	CV. Dwi Putri	15	5371	Manulai II Blok A No.4 Perumahan Ajobaki Kel. Manulai II Kec. Alak Kota Kupang Nusa Tenggara Timur	cvdwiputri@gmail.com	081315329810	\N	\N
1921	PT. Putra Solor teknik	15	5371	Komp. X Perum Bukopin I No.3 Rt.014/005 Kel. Manulai II Kec. Alak Kota Kupang Nusa Tenggara Timur	putrasolorteknik@gmail.com	0853-33526287	\N	\N
1922	PT. RIDE MANDIRI KUPANG	15	5371	JL.ADVOKAD NO.12A KEL.NAIKOTEN 1 KEC.KOTA RAJA KOTA KUPANG Kota Kupang Nusa Tenggara Timur	pt.ridemandirikupang@gmail.com	-	\N	\N
1923	PT. Canayu Telaga Pratama	15	5371	Jl.W.Ch.Oematan No.21 RT.021 RW.007 Kelurahan Kelapa Lima Kota Kupang Nusa Tenggara Timur	CTPratama@yahoo.com	-	\N	\N
1984	PT. ROY JAYA MANDIRI	25	9409	Jl. WANDAMEN KEC. SAMOFA Kab. Biak Numfor P A P U A	ptroyjayamandiri@gmail.com	0981 23188	\N	\N
1985	PT. DUTA TEHNIK MANGKEY	25	9403	Jl. Lorong Torabika Kelurahan Kamoro Jaya Kecamatan Wania - Jayapura Kab. Jayapura P A P U A	dutatehnikmangkey@yahoo.com	081343395490	\N	\N
1986	PT. Tiga Putra Irja	25	9403	Jalan Flamboyan Perum Istana Flamboyan Blok C4, Kelurahan Vim, Kecamatan Distrik Abepura, Kabupaten Jayapura, Papua Kab. Jayapura P A P U A	putrairja333@yahoo.com	0967551810	\N	\N
1987	PT. MUTIARA CINTANAMI SEJAHTRA	25	9403	Jln. Jeruk Nipis No. 224 Furia Kotaraja - Jayapura Kab. Jayapura P A P U A	jhon.sitanggang60@gmail.com	0811480032	\N	\N
1988	PT. MUTIARA CINTANAMI MUTIARA	25	9403	Jl. Jeruk Nipis No. 224 Furia Kotaraja-Jayapura Kab. Jayapura P A P U A	jhon.sitanggang60@gmail.com	0811480032	\N	\N
1989	PT. PARDAME BINTANG UTARA	25	9403	Jl. Tanjung Ria Base G No. 78 Kel. Tanjung Ria Kec. Jayapura Utara Kab. Jayapura P A P U A	pardamebintangutara@gmail.com	0967-541686	\N	\N
1990	PT. IRIANA KARYA UTAMA	25	9403	Jl. Gereja Moria No. 65 A, Kotaraja Dalam Kel. Vim Kec Distrik Abepura Kab. Jayapura P A P U A	pt.irianakaryautama@gmail.com	0967-583277	\N	\N
1991	PT. CENTRO PUTRA PAPUA MANDIRI	25	9403	KOMP PAPUA TRADE CENTER BLOK A NO 05-06 KELAPA DUA ENTROP Kab. Jayapura P A P U A	centroputrapapuamandiri@yahoo.com	-	\N	\N
1992	PT. MANSINAM MESER ISNA	25	9402	JL.YOS SUDARSO WAMENA KOTA Kab. Jayawijaya P A P U A	deckykawai@gmail.com	08114811773	\N	\N
1993	PT. TRI PUTRA SENGGOGA	25	9408	Jl. Frans Kaisepo Serui Warari Kec. Yapen Selatan Kab. Kepulauan Yapen - PAPUA Kab. Kepulauan Yapen P A P U A	senggoga@gmai.com	081342327986	\N	\N
1994	CV. PRATAMA KARYA	25	9401	Jl. Angkasa Merauke RW.004 / RT.002 Kel. Kelapa Lima Kec. Merauke Kab. Merauke, Papua Kab. Merauke P A P U A	cvpratama.karya@yahoo.co.id	0971 - 325623 / 08124820194	\N	\N
1995	PT. ACHMADANY CAHAYA ABADI	25	9401	Jl. Menara Lampu Satu RT. 001 RW. 001 Kel. Samkai Kec. Merauke Kab. Merauke P A P U A	ptaca.merauke@gmail.com	0823-00184435	\N	\N
1996	PT. SMART BRILLIANT ENERGI	25	9401	JL, PARAKOMANDO Kab. Merauke P A P U A	Smartbrilliant01@gmail.com	082198958996 / 081354057321	\N	\N
1997	PT. TUNAS MUDA PAPUA	25	9401	JL. MENARA LAMPU SATU KEL.SAMKAI KEC. MERAUKE Kab. Merauke P A P U A	tunasmudapapua@gmail.com	085244510665	\N	\N
1998	PT. BAHARAJA PAJAMA PRIMA	25	9401	JALAN CEMARA Kab. Merauke P A P U A	pt.baharaja@gmail.com	0813445 06025	\N	\N
1999	CV. KASIH SEJATI PAPUA	25	9401	JL. SPADEM RT.018 RW.002 KEL. MULI KEC. DISTRIK MERAUKE Kab. Merauke P A P U A	kasihsetiapapua@gmail.com	08124119889	\N	\N
2000	PT. RAJAWALI PERKASA RAYA	25	9401	JALAN ARU RT.014-RW. 004 KELURAHAN MANDALA, DISTRIK MERAUKE PAPUA Kab. Merauke P A P U A	rajawaliperkasaraya@gmail.com	0971 324508	\N	\N
2001	CV. SINAR ABADI PAPUA	25	9401	JL. ANGKASA RT.017 RW.004 KEL. KELAPA LIMA KEC. MERAUKE KAB. MERAUKE Kab. Merauke P A P U A	papuasinarabadi@gmail.com	0971788788	\N	\N
2002	PT. MITRAKARYA TEKNIK UTAMA	25	9401	JALAN RAYA MANDALA RT.001 RW.002 KEL. BAMBU PEMALI KEC. MERAUKE KAB. MERAUKE, PAPUA Kab. Merauke P A P U A	mitratek.utama@gmail.com	0971 325623	\N	\N
2003	CV. INDO KARYA PRATAMA	25	9401	JL. ERMASU - MERAUKE Kab. Merauke P A P U A	indokaryapratama@gmail.com	-	\N	\N
2004	PT. DAYA UTAMA PAPUA SELATAN	25	9401	JL. ERMASU - MERAUKE Kab. Merauke P A P U A	indokaryapratama@gmail.com	-	\N	\N
2005	PT. QUANTUM PERSADA NUSANTARA	25	9401	Jl. Raya Mandala Muli Gg, Falon RT.001 RW.001 Kel. Rimba Jaya Kec. Merauke Kab. Merauke P A P U A	quantum.pn@gmail.com	-	\N	\N
2006	CV. KANAAN JAYA	25	9412	JL. CENDRAWASIH - TIMIKA JAYA KWAMKI KEC.MIMIKA BARU KAB. MIMIKA PAPUA Kab. Mimika P A P U A	cv_kanaanjaya@yahoo.com	08124004578 / 0811490034	\N	\N
2007	PT. PESONA WANIA MIMIKA	25	9412	JALAN YOS SUDARSO NO 3 Kab. Mimika P A P U A	ptpesonawania@yahoo.co.id	-	\N	\N
2008	PT. MAJU SEJAHTERA UTAMA	25	9412	JL. LEO MAMIRI KOPERAPOKA, KELURAHAN WONOSARI JAYA, KECAMATAN WANIA TIMIKA Kab. Mimika P A P U A	pt.majusejahterautama@yahoo.co.id	-	\N	\N
2009	PT. JAYA JOFAA SEMESTA	25	9471	Jl. Kamboja Padang Bulan, Kelurahan Hedam, Distrik Heram Kota Jayapura P A P U A	jofaasemestajaya@gmail.com	082131143322	\N	\N
2010	PT. MUTIARA PAPUA JAYA	25	9471	Perum BTI Blok III No.71 Dok VIIII Atas Jayapura Kota Jayapura P A P U A	pt.mpjaya@gmail.com	08114820868	\N	\N
2011	PT. JERIL INDAH POWER	25	9471	BUCEND II ENTROP ARDIPURA ARDIPURA KEC. JAYAPURA SELATAN Kota Jayapura P A P U A	pt_jerilindahpower@yahoo.com	0967551341	\N	\N
2012	PT. URSULA ABADI	25	9471	Jl. Tanjung Ria No. 10, Kelurahan Tanjung Ria, Kecamatan Jayapura Utara Kota Jayapura P A P U A	rainanugerah14@gmail.com	08525418054	\N	\N
2013	PT. GOVARI JAYA MANDIRI	25	9471	KOMP. PERUMAHAN JAYA ASRI BLOK AE NO.10 KEL. ENTROP KEC.JAYAPURA SELATAN Kota Jayapura P A P U A	govarijayamandiri.pt@gmail.com	08124801438	\N	\N
2014	PT. TIGA BINTANG PAPUA PERKASA	25	9471	Jl. Kelapa Dua Entrop No.88 Kel. Polimak Kec. Jayapura Selatan Kota Jayapura P A P U A	sindupranata@gmail.com	0967-524088	\N	\N
2015	PT. BUBUN BUSU	25	9471	PERUMAHAN PERMATA INDAH BLOK B NO. 5 KEL. ASANO DIST. ABEPURA Kota Jayapura P A P U A	pt.bubunbusu@gmail.com	0967-585552	\N	\N
2016	PT. LAKAWAN	25	9471	JL. YOUTEFA NO. 42 KEL. AWIYO DISTRIK ABEPURA Kota Jayapura P A P U A	pt.lakawan@gmail.com	081344338484	\N	\N
2017	PT. HIMALAYA PAPUA MANDIRI	25	9471	JL. HAMADI RAWA II NO. 08 KEL HAMADI Kota Jayapura P A P U A	himalayapapua@yahoo.com	08114800155	\N	\N
2018	CV. YEDIJA MAKIBERS	25	9471	JL. SETIAPURA II RT.003 / RW.004 KELURAHAN NUMBAY DISTRIK JAYAPURA SELATAN Kota Jayapura P A P U A	cv.yedijamakibers@gmail.com	-	\N	\N
2019	PT. KABANTARAS	25	9471	GG. TANJUNG 1 NO.12 PERUMNAS 1 KEL. WAENA DISTRIK HERAM Kota Jayapura P A P U A	pt.kabantaras@yahoo.com	-	\N	\N
2020	PT. AMBA OPSIONAL GEMILANG	25	9471	JL. PASIFIK INDAH II NO. 160 JAYAPURA-PAPUA Kota Jayapura P A P U A	ptambaopsional@yahoo.com	-	\N	\N
2021	PT. HARAPAN PAPUA SEJAHTERA	25	9471	Perum Jaya Asri Blok AC No. 6 Kota Jayapura P A P U A	pt.hrps@gmail.com	-	\N	\N
2022	PT. SINAR TIMUR PASIFIK	25	9471	JLN. PASIFIK INDAH III KELURAHAN TANJUNG RIA Kota Jayapura P A P U A	sinartimurpasifik@gmail.com	-	\N	\N
2023	PT. YEDIJA MAKIBERS PAPUA	25	9471	JL. SETIAPURA II RT.003 RW.004 KELURAHAN NUMBAY DISTRIK JAYAPURA SELATAN Kota Jayapura P A P U A	pt.yedijamakibers@gmail.com	-	\N	\N
2024	PT. YEDIJA YAWADATUM PAPUA	25	9471	JL. SETIAPURA II NO.72 RT.003 RW.004 KELURAHAN NUMBAY Kota Jayapura P A P U A	ptyedijayawadatum@gmail.com	-	\N	\N
2025	CV. GUNUNG SESEAN	25	9101	JL. MERAPI II VANINDI St, KELURAHAN MANOKWARI BARAT. KEC.MANOKWARI BARAT Kab. Fak Fak Papua Barat	gunungsesean@gmail.com	-	\N	\N
2026	PT. ADAS MANIS ARAS	25	9101	JL. GUNUNG SALJU FANINDI KEL. MANOKWARI BARAT.KEC. MANOKWARI, PAPUA BARAT Kab. Fak Fak Papua Barat	adasmanisaras@gmail.com	-	\N	\N
2027	CV. BONTO MANAI	25	9101	JL. EKONOMI REMIMI NO.31 Kab. Fak Fak Papua Barat	yhuniwahyunie@yahoo.com	-	\N	\N
2028	CV. NUR AKBAR	25	9101	JL. DRS. ESAU SESA WOSI Kab. Fak Fak Papua Barat	yhuniwahyunie@yahoo.com	-	\N	\N
2029	PT. RAFFIAN TEKNIK MANDIRI	25	9105	JL. KPR Reremi Permai Manokwari Barat Kec. Manokwari Barat Kab. Manokwari Papua Barat	raffianteknik99@gmail.com	081344027517	\N	\N
2030	PT. MOORIANA JAYA PAPUA	25	9105	JL.SUMBER JAYA ANGKASA MULYONO MANOKWARI Kab. Manokwari Papua Barat	moorianajayapapuapt@gmail.com	085254082210	\N	\N
2031	PT. OVIM JAYA MULIA	25	9105	Jl. Terong RT.01 RW.02 Kel. Malawele, Distrik Aimas Kab. Sorong, Papua Barat Kab. Manokwari Papua Barat	ptovimjayamulia@gmail.com	081252196027	\N	\N
2032	PT. WIDYA KENCANA PRATAMA	25	9105	Jl. Jend Sudirman, No. 10, Manokwari Barat, Kec. Manokwari Barat Kab. Manokwari Papua Barat	pt.wkp@gmail.com	0986-213100	\N	\N
2033	PT. CEMERLANG YASA PERSADA	25	9105	Jl. Kapten Yugoharto Kel. Sanggeng Kec. Manokwari Barat Kab. Manokwari, Papua Barat Kab. Manokwari Papua Barat	Cypersada58@Gmail.com	0986-211810	\N	\N
2034	PT. ROMANSTA JAYA MANDIRI	25	9105	Jl. Brawijaya Kodim 1703 RT.003 RW.007 Kel. Padarni Kec. Manokwari Barat Kab. Manokwari, Papua Barat Kab. Manokwari Papua Barat	pt.romanstajayamandiri@gmail.com	082198708042/082239075614	\N	\N
2035	PT. DOA KARYA ATAN	25	9105	Jl. Trikora Arfai I Rt.001/001 Andai Kec. Manokwari Selatan Kab. Manokwari Papua Barat	pusat@ptaskomelin.co.id	-	\N	\N
2036	CV. NUR INDAH	25	9105	JL. DRS ESAU SESA MANOKWARI, KELURAHAN WOSI,DISTRIK MANOKWARI BARAT Kab. Manokwari Papua Barat	riaz.ahmad.zd@gmail.com	-	\N	\N
2037	PT. NEJEN PRATAMA ELECTRIK	25	9107	Jl. Sapta Taruna KM. 10 Klawuyuk Kec. Sorong Utara Kab. Sorong Papua Barat	nejen_pjaya@yahoo.co.id	081344249897	\N	\N
2038	CV. GRIFF PERKASA	25	9104	KAMPUNG LAMA BINTUNI TIMUR, Kab. Teluk Bintuni Papua Barat	griffperkasa@yahoo.co.id	-	\N	\N
2039	PT. BOGDAN JAYA PAPUA	25	9104	KAMPUNG LAMA BINTUNI TIMUR, BINTUNI TIMUR, KEC BINTUNI. Kab. Teluk Bintuni Papua Barat	riaz.ahmad.zd@gmail.com	-	\N	\N
2040	PT. TRI TUNGGAL AIMAS SORONG	25	9409	Jl. Nangka Rt.21 Rw.07 Malawili Kec. Aimas Kota Sorong Papua Barat	pttritunggalaimassorong@yahoo.com	0951-322796	\N	\N
2041	PT. TODO CIPTA KONSTRUKSI	25	9409	JALAN SAPTA TARUNA KM. 10 MASUK MATALAMAGI KEC.SORONG Kota Sorong Papua Barat	todociptakonstruksi15@gmail.com	081344249897	\N	\N
2042	PT. MAKMUR GOLDEN ELECTRIK	25	9409	JALAN SAPTA TARUNA IV KM 10, MATALUMAGI, SORONG UTARA Kota Sorong Papua Barat	aklidpp@gmail.com	095131197	\N	\N
2043	PT. MALISTA KONTRUKSI	24	7303	Jl. Pelita Jaya II Perumahan Regensi No. 9 makasar Kab. Bantaeng Sulawesi Selatan	pusat@ptaskomelin.co.id	-	\N	\N
2044	PT. MARANNU MEGAH ELEKTRIK (MME)	24	7311	LEMPANGAN DESA PATANGKAI KEC. LAPPARIAJA. KAB. BONE Kab. Bone Sulawesi Selatan	pt.marannu@gmail.com	082811463121	\N	\N
2045	CV. GHINA ZHALFA GHANIYA	24	7311	BTN BIRU INDAH PERMAI BLOK D/22 SODDANGGE KELURAHAN BIRU KECAMATAN TANETE RIATTANG Kab. Bone Sulawesi Selatan	cv.ghinazhalfaghaniya@yahoo.com	08124111753	\N	\N
2046	CV. BULU KASA MANDIRI	24	7311	DUSUN WAE KECCEE, DESA WAE KECCEE, KEC. LAPPARIAJA, KAB.BONE, SULAWEI SELATAN Kab. Bone Sulawesi Selatan	andiirman02@gmail.com	085241659111	\N	\N
2047	PT. TIWANA ELECTRIKAL TEKNIK	24	7311	JL. A. MALLA PERUMNAS TIBOJONG BLOK C NO. 51 KEL. TIBOJONG KEC. TANETE RIATTANG TIMUR KAB. BONE SULAWESI SELATAN Kab. Bone Sulawesi Selatan	pt.tiwanaelectrikalteknik@gmail.com	-	\N	\N
2048	PT. MUTIARA TEKNIK MUSTIKA	24	7311	JL. BHAYANGKARA RT. 000 / RW. 000 KEL. WATAMPONE KEC. TANETE RIATTANG KAB. BONE SULAWESI SELATAN Kab. Bone Sulawesi Selatan	pt_mtmustika@gmail.com	-	\N	\N
2049	PT. BONE SINRIJALA TURATEA	24	7311	JL. YOS SUDARSO BTN CITRA D / 4 KEL. CELLU KEC. TANETE RIATTANG TIMUR KABUPATEN BONE SULAWESI SELATAN Kab. Bone Sulawesi Selatan	istiqqamah@gmail.com	-	\N	\N
2050	PT. ALIRAN LISTRIK UTAMA	24	7311	JL. PAROTO DESA SAMAELO KEL. SAMAELO, KEC. BAREBBO KABUPATEN BONE Kab. Bone Sulawesi Selatan	pt.aliranlistrikutama@gmail.com	-	\N	\N
2051	PT. MEGA JAYA ELEKTRIK	24	7311	Jl.Irian No.29 Watampone Kel.Mannurungnge Kec.Tanete Riattang Kab. Bone Sulawesi Selatan	megajaya394@yahoo.co.id	-	\N	\N
2052	PT. ROSMAN JAYA TEKNIK	24	7311	Jl. Yos Sudarso No. 10 RT. 002 RW. 002 Kel. Cellu Kec. Tanete Riattang Kab. Bone Sulawesi Selatan	cv.rosmanjaya77@yahoo.co.id	-	\N	\N
2053	PT. PANDU PRAKARSA PROGRESIF	24	7303	JL. CAKALANG NO 3 KEL. TERANGTERANG KEC. UJUNG BULU KEC. BULUKUMBA Kab. Bulukumba Sulawesi Selatan	akhyanarief@yahoo.com	08124204462	\N	\N
2054	PT. MUTU KENDALI KONSTRUKSI	24	7303	Jl. Sunu No.4B Kel. Terang-Terang Kec. Ujung Bulu Kab. Bulukumba Sulawesi Selatan	mutukendali.konstruksi@gmail.com	-	\N	\N
2055	PT. AINI REUNI TEKNIK	24	7303	BTN Zanur Park Estate Blok AZ No.168 Ds. Taccorong Kec. Gantarang Kab. Bulukumba Sulawesi Selatan	chuaascho@yahoo.co.id	-	\N	\N
2056	PT. ZAFFA ELECTRICAL BULUKUMBA	24	7303	JL. KOMP. BTN SOMBA 2 BLOK D 5 - 6 KEL. CAILE KEC. UJUNG BULU KAB. BULUKUMBA SULAWESI SELATAN Kab. Bulukumba Sulawesi Selatan	pt.zaffa.electrical.bulukumba@gmail.com	-	\N	\N
2057	PT. PERKASA ZAMMA MASILA	24	7303	KOMP PERUM. TIARA PERMAI 7 BLOK F NO.5,KELURAHAN TACCORONG KECAMATAN GANTARANG KABUPATEN BULUKUMBA,SULAWESI SELATAN Kab. Bulukumba Sulawesi Selatan	ptperkasazamma.masila@yahoo.com	-	\N	\N
2058	PT. Harapan Mulya Sejati	24	7303	Jl. Cikalang No.3 Ujung Bulu Bulukumba Kab. Bulukumba Sulawesi Selatan	pancarancahaya@yahoo.com	-	\N	\N
2059	PT. MEGARILNA SEJATI KONSTRUKSI	24	7303	JL. BONTO TANGNGA PAO-PAO KOMP. HERTASNING MADANI BLOK I NO. 1 GOWA PECCINONGAN KEC. SOMBA OPU Kab. Gowa Sulawesi Selatan	hasbullah7775@gmail.com	0411-8231390 / 081318132087	\N	\N
2060	PT. INDRIA JAYA GOWA	24	7303	Jl. Mangka Dg. Bombong No. 3 Kab. Gowa Sulawesi Selatan	ptindriajayagowa@gmail.com	-	\N	\N
2061	PT. KENDALI UTAMA JAYA	24	7303	JL. KARAENG MAKKAWARI SAMATA RT. 001 RW. 001 KEL. SAMATA KEC. SOMBA OPU KAB. GOWA SULAWESI SELATAN Kab. Gowa Sulawesi Selatan	ptkendalijaya@gmail.com	-	\N	\N
2062	CV. TRI PELANGI	24	7303	Jl. Poros Balaburu No. 5 Kab. Gowa Sulawesi Selatan	tripelangi@yahoo.co.id	-	\N	\N
2063	PT. ANUGRAH DUA LIMA TUJUH	24	7303	JL. MERDEKA RAYA NO. 2 RT. 001 RW. 001 KEL. EMPOANG KEC. BINAMU KAB. JENEPONTO SULAWESI SELATAN Kab. Jeneponto Sulawesi Selatan	pt_anugrah257@gmail.com	-	\N	\N
2064	PT. QODRI PUTRA UTAMA	24	7303	JL. POROS PADANG SUBUR KEL. PADANG SUBUR KEC. PONRANG KAB. LUWU SULAWESI SELATAN Kab. Luwu Sulawesi Selatan	pt.qodriputrautama@gmail.com	-	\N	\N
2065	PT. ASYIFA ALAM TEKNIK	24	7303	JL. SALLA BELOPA KEL. BELOPA KEC. BELOPA Kab. Luwu Sulawesi Selatan	pt.asyifaalamteknik@gmail.com	-	\N	\N
2066	PT. ANUGERAH PRATAMA PUTRA DEWATA	24	7303	Dusun Darma Bakti Desa Sumber Agung Kec. Kalaena Kab. Luwu Timur, Sulawesi Selatan Kab. Luwu Timur Sulawesi Selatan	dedisatriaprata@gmail.com	082396742228	\N	\N
2067	PT. CAKRA VARANI KONSTRUKSI	24	7303	Desa Tarengge Timur Kel. Tarengge Kec. Wotu Kab. Luwu Timur Sulawesi Selatan	lskn_pusat@yahoo.co.id	-	\N	\N
2068	PT. FANHY MULYA MANDIRI	24	7303	Jl. Poros Langnga, Awang-awang Kelurahan Sipatokong Kab. Pinrang Kab. Pinrang Sulawesi Selatan	pt.fanhymulyamandiri@gmail.com	085388399091	\N	\N
2069	PT. CAHAYA TEHNIK ELEKTRIKAL	24	7303	JL. JENDRAL SUDIRMAN NO. 120 KEL. MACCORAWALIE KEC. WATANG SAWITTO KAB. PINRANG SULAWESI SELATAN Kab. Pinrang Sulawesi Selatan	cahayatehnikelektrikal@gmail.com	-	\N	\N
2070	PT. RADAR ELEKTRIKAL UTAMA	24	7303	JL. SULTAN HASANUDDIN NO. 8 KEL. SAWITTO KEC. WATAN SAWITTO KAB. PINRANG SULAWESI SELATAN Kab. Pinrang Sulawesi Selatan	radar_pg@yahoo.co.id	-	\N	\N
2071	PT. HAMSAL IHKWAN PRATAMA	24	7303	JL.POROS PINRANG PASSENO I KEL.DUAMPANUA KEC.BARANTI KAB.SIDENRENG RAPPANG Kab. Sidenreng Rappang Sulawesi Selatan	hamsalihkwanpratama@gmail.com	-	\N	\N
2072	PT. KARYA PUTRA SIDRAP	24	7303	JL. A.TAKKO NO. 29 Kab. Sidenreng Rappang Sulawesi Selatan	adridjoha@yahoo.co.id	-	\N	\N
2073	PT. ARTHA LESTARI ENGINEERING	24	7303	JL.DR.SAM RATULANGI,KELURAHAN BALANGNIPA KECAMATAN SINJAI UTARA KABUPATEN SINJAI KOTA MAKASSAR, SULAWESI SELATAN Kab. Sinjai Sulawesi Selatan	arthalestari@yahoo.co.id	0411-43211	\N	\N
2074	PT. MITRA JAYA UNGGUL	24	7303	JL. POROS MAKALE RANTEPAO NO. 67 KABUPATEN TANA TORAJA Kab. Tana Toraja Sulawesi Selatan	ptmitrajayaunggul@gmail.com	-	\N	\N
2075	PT. KBU Elektrikal	24	7303	Kelurahan Tallunglipu, Kecamatan Tallunglipu Kab.Toraja Utara, Sulawesi Selatan Kab. Toraja Utara Sulawesi Selatan	kbuelektrikal@gmail.com	-	\N	\N
2076	PT. RESKY BERSAMA WAJO	24	7303	BTN PERUMNAS ASSORAJANG BLOK A NO.41 ASSORAJANG KECAMATAN TANASITOLO Kab. Wajo Sulawesi Selatan	cvreskybersama@yahoo.com	0811428073	\N	\N
2077	PT. TIARA LISTRIK SULAWESI	24	7303	JL. BERINGIN NO. 7 SENGKANG KEL. BULUPABBULU KEC. TEMPE KAB. WAJO SULAWESI SELATAN Kab. Wajo Sulawesi Selatan	tiaralistriksulawesi@gmail.com	-	\N	\N
2078	PT. NEFKENS KONSTRUKSI SENGKANG	24	7303	JL. SAWERIGADING PATTIRO SOMPE KEC. TEMPE Kab. Wajo Sulawesi Selatan	sudirmana84@gmail.com	-	\N	\N
2079	PT. SUMBER SINAR SIWA	24	7303	JL. A. Jaja Poros Siwa Kec. Pitumpanua Kab. Wajo Sulawesi Selatan	sumbersiwa@gmail.com	-	\N	\N
2080	PT. Karya Resky Maharani	24	7303	Jl. Bougenville Raya, 55. Komp. Maizonette Kota Makassar Sulawesi Selatan	krmaharani@gmail.com	0811406690	\N	\N
2081	CV. TAUFIKAH KARYA PERKASA	24	7303	Jl.Hertasning Perum Angin Mammiri Blok E4 No.9 Kota Makassar Sulawesi Selatan	taufikahkarya_perkasa@yahoo.co.id	081244919244	\N	\N
2082	PT. Bintang Sulawesi Energi Prima	24	7303	Jl. Anggrek No. 14, Kelurahan BontoRannu, Kecamatan Mariso Kota Makassar Sulawesi Selatan	bseprima@yahoo.com	02411-853538	\N	\N
2083	PT. TUJUH SEMBILAN SAHAS	24	7303	Jl. Balana I No. 49 Kel. Barana Kec. Makassar Kota Makassar Prov. Su;lawesi Selatan Kota Makassar Sulawesi Selatan	tujuhsembilansahas79@yahoo.com	0411-447653	\N	\N
2084	PT. SAFAAT MUSMA INDAH	24	7303	Jl. Goa Ria BTN Pepabri Blok A 7 No. 3 Sudiang Kecamatan Biringkanaya Kota Makassar Sulawesi Selatan Kota Makassar Sulawesi Selatan	safaatmusmaindah@yahoo.com	0411-5775535	\N	\N
2085	PT. PT. TEKNIK INTEGRASI MANDIRI	24	7303	Jl. Ks Tubun No. 4 Makassar Mattoangin, Kec. Mariso Kota Makassar Sulawesi Selatan	teknikintegrasi@yahoo.com	0411-3612387	\N	\N
2086	PT. HAJI ANDI WITTIRI	24	7303	Jl. G. Bawakaraeng No. 218 FG Kel. Maccini Gusung Kec. Makassar Kota Makassar Sulawesi Selatan	pt.hajiandiwittiri@gmail.com	0411452229	\N	\N
2087	PT. DARMA KARYA ELEKTRIK	24	7303	JL.RUKO INSIGNIA BTP BLOK A NO.7 KELURAHAN TAMALANREA, KECAMATAN TAMALANREA Kota Makassar Sulawesi Selatan	darmawati.ptdke@gmail.com	08124243198	\N	\N
2088	PT. ARYA SABILA ELECTRIK	24	7303	Jl. Kemauan I No. 4 Kel. Maccini Parang Kec. Makassar Kota Makassar Sulawesi Selatan Kota Makassar Sulawesi Selatan	pt.arya.sabila.elektrik@gmail.com	0811410684	\N	\N
2089	PT. LUNUR BERKAH PERSADA	24	7303	BTP RUKO INSIGNIA BOULEVARD BLOK A. NO.7 KEL.TAMALANREA KEC.TAMALANREA Kota Makassar Sulawesi Selatan	matrix30@rocketmail.com	08124243198	\N	\N
2090	PT. SEN ENJINIRING KELISTRIKAN	24	7303	Jl. Serui No. 15 Lantai 2 Kel. Pattunuang Kec. Wajo Kota Makassar Sulawesi Selatan Kota Makassar Sulawesi Selatan	senji.kelistrikan@gmail.com	0411-3623716, 0411-3611887	\N	\N
2091	PT. DUTA MARGA SEJATI	24	7303	JL.RAPPOCINI RAYA LR 9.D NO.1MAKASSAR Kota Makassar Sulawesi Selatan	dutamargasejati@gmail.com	085256786652/081342733310	\N	\N
2092	PT. Almira Lintang Pratama	24	7303	BTP JL. TAMALANREA SELATAN IIA BLOK M NO. 1 KEL. TAMALANREA KEC. TAMALANREA Kota Makassar Sulawesi Selatan	pt.alp.mks@gmail.com	08114110802 / 082188934173	\N	\N
2093	PT. RIDHO TEKNIK	24	7303	JL. MESJID MUHAJIRIN III/25, MALLENGKERI, KEL. PARRANG TAMBUNG KEC. TAMALATE Kota Makassar Sulawesi Selatan	ridhoteknik8@gmail.com	08114454549	\N	\N
2094	PT. RAJA TEKNIK SEJATI	24	7303	JL. VETERAN SELATAN NO. 62 Kota Makassar Sulawesi Selatan	pt.rajatekniksejati@gmail.com	081242525200	\N	\N
2095	PT. JATIMULIA INDONESIA	24	7303	JL. SUNGAI SADDANG BARU NO. 10, KOTA MAKASSAR Kota Makassar Sulawesi Selatan	JATIMULIAINDONESIA@YMAIL.COM	0816806701	\N	\N
2096	PT. ISPERIAL TUNGGAL CONTRACTOR	24	7303	JL. HERTASNING BARAT II GPI BLOK E NO. 7 Kota Makassar Sulawesi Selatan	isperial@yahoo.co.id	0411864262/082189420867	\N	\N
2097	PT. Dipa Jaya Sejahtera	24	7303	Jl. Lanto DG Pasewang No. 15, Kelurahan Maricaya, Kecamatan Makassar Kota Makassar Sulawesi Selatan	dipa_jaya@yahoo.com	0411854705	\N	\N
2098	PT. DISTRIBUSI ENERGI MANDIRI	24	7303	Jl. Cendrawasih Komplek Ruko Cendrawasih Square Blok C No.18, Kel. Sambung Jawa, Kec. Mamajang Kota Makassar Sulawesi Selatan	distribusi.energi@gmail.com	0852 5946 3645	\N	\N
2099	PT. Ainul Jaya Makassar	24	7303	Jl.Sibula Dalam No.66 Sulawesi Selartan Kota Makassar Kota Makassar Sulawesi Selatan	ainul.lpse@gmail.com	08124137298	\N	\N
2100	PT. FASRAT UTAMA	24	7303	JL.MONUMEN EMMY SAELAN IIID/3C TIDUNG KEC. RAPPOCINI Kota Makassar Sulawesi Selatan	fasratutama_pt@yahoo.com	(0411) 880888	\N	\N
2101	PT. SATRIA TEKNIK UTAMA	24	7303	Jl. Bitoa Lama No.34 Kel.Antang kec.Manggala Kota Makassar Sulawesi Selatan Kota Makassar Sulawesi Selatan	pt.satriateknikutama@yahoo.com	0411-494964	\N	\N
2102	PT. KELINCI MAS UNGGUL	24	7303	JL. VETERAN UTARA NO. 180 Kota Makassar Sulawesi Selatan	ptkelincimasunggul@yahoo.co.id	0411-3613001	\N	\N
2103	PT. KARYATAMA MULTI PRIMA	24	7303	JL. PARINRING I No. 21 Kota Makassar Manggala Kec. Manggala Kota Makassar Sulawesi Selatan	iful010291@gmail.com	0411-494833	\N	\N
2104	PT. TRIPUTRA GAHARA INDONESIA	24	7303	Jl. Gunung Merapi No.74 Kel Pisang Utara Kec. Ujung Pandang Kota Makassar Sulawesi Selatan	triputragaharaindonesia@gmail.com	0411-3620368	\N	\N
2105	PT. MUT ENGINEERING	24	7303	JL. PELITA RAYA TENGAH BLOK A1 NO.2 KEL. BALLAPARANG KEC. RAPPOCINI KOTA MAKASSAR SULAWESI SELATAN Kota Makassar Sulawesi Selatan	mut@mut.co.id	0411-432432, 081242706328 / 0411-448446	\N	\N
2106	PT. BAGUS KARYA	24	7303	Jl. Metro Tanjung Bunga Kav.AA No.29, RT 01/RW 08, Kel Tanjung Merdeka, Kec Tamalate, Kota Makassar, Sulawesi Selatan Kota Makassar Sulawesi Selatan	baguskarya@baguskarya.com	0411-8113713	\N	\N
2107	PT. Citra Riantara Sejahtera	24	7303	Jl. Pelanduk Lt. III No. 77 Kelurahan Maricaya, Kecamatan Makassar Kota Makassar Sulawesi Selatan	asma@dipanjaya-sejahtera.com	+624118910709	\N	\N
2108	PT. CAHAYA PUTRA BERSAMA	24	7303	Jl. Skarda N Komp. Mangasa Permai Blok V1. No.1 Kota Makassar Sulawesi Selatan	pt.cpb_mks@yahoo.com	0411-518995/0811010063	\N	\N
2109	PT. MENARA INDRA UTAMA	24	7303	Jl. P. Kemerdekaan KM.17 (Blkg Kobelco) Pai Kec. Biringkanaya Kota Makassar Sulawesi Selatan	menaraindrautama1@gmail.com	-	\N	\N
2110	PT. MHI ISTIQOMAH PUTRA	24	7303	JALAN SKARDA N 2 NO. 12, RT 004, RW 016, KEL. GUNUNG SARI, Kec. RAPPOCINI Kota Makassar Sulawesi Selatan	ptmhi.ip@gmail.com	-	\N	\N
2111	PT. KRISTAL PURI KENCANA	24	7303	Jl. Puri Kencana No. 09, Makassar Kota Makassar Sulawesi Selatan	pt.kristal_purikencana@yahoo.com	-	\N	\N
2112	PT. MEGATIKA DAYA LESTARI	24	7303	JL. KEBANGKITAN NO. 7 RT. 006 / RW. 010 KEL. MACCINI PARANG KEC. MAKASSAR KOTA MAKASSAR SULAWESI SELATAN Kota Makassar Sulawesi Selatan	pt.megatikadayalestari@gmail.com	-	\N	\N
2113	PT. CITRA BAKTI PERSADA	24	7303	Jl. Perintis Kemerdekaan Komp. Almarhamah Blok A1/1 Kel. Tamalanrea Kec. Tamalanrea Kota Makassar Sulawesi Selatan	pcitrabakti@yahoo.com	-	\N	\N
2114	PT. BAYU DIRAWA ALTIN	24	7303	JL. SULTAN ALAUDDIN III A NO. 7 KEL. MANGASA KEC. TAMALATE KOTA MAKASSAR SULAWESI SELATAN Kota Makassar Sulawesi Selatan	ptbayudirawa@yahoo.com	-	\N	\N
2115	PT. TEPPO ELECTRIC PERSADA	24	7303	Jl. Manggala Dalam I No. 03, Kota Makassar Kota Makassar Sulawesi Selatan	teppoelectricpersada@yahoo.com	-	\N	\N
2116	PT. ATIQA RAMADHAN SEJAHTERA	24	7303	BTN MINASA UPA BLOK L13 NO 16 Kota Makassar Sulawesi Selatan	atiqa56@gmail.com	-	\N	\N
2117	PT. TRIO DARMA NUSANTARA	24	7303	BTP BLOK AE NO 229 KOTA MAKASSAR Kota Makassar Sulawesi Selatan	jumriatiati@gmail.com	-	\N	\N
2118	PT. DUA PUTRA MAJU	24	7303	JL. TODDOPULI VI BERINGIN P BLOK G NO. 3 KEL. BORONG KEC. MANGGALA KOTA MAKASSAR SULAWESI SELATAN Kota Makassar Sulawesi Selatan	pt.duaputramaju@yahoo.com	-	\N	\N
2119	PT. NIRHA JAYA TEHNIK	24	7303	JL. MUH. PALEO II NO. 5 Kota Makassar Sulawesi Selatan	ptnirhajayatehnik@gmail.com	-	\N	\N
2120	PT. ALTIN ELEKTRONIK ABADI	24	7303	JL. SULTAN ALAUDDIN III NO. 11 KOTA MAKASSAR SULAWESI SELATAN Kota Makassar Sulawesi Selatan	ptaltinabadi@yahoo.com	-	\N	\N
2121	CV. HIKARI DENKI TEKNIK	24	7303	BTN SAO SARANA INDAH BLOK A 13 NO 17, SUDIANG RAYA Kota Makassar Sulawesi Selatan	riaz.ahmad.zd@gmail.com	-	\N	\N
2122	PT. DILI JAYA INDONESIA	24	7303	Villa Surya Mas Blok J/9, Kelurahan Borong, Kecamatan Manggala Kota Makassar Sulawesi Selatan	ptdilijayaindonesia@gmail.com	-	\N	\N
2123	PT. SINAR PANCARAN CAHAYA	24	7303	Komp. Hartaco Indah Blok III S No. 15 RT 003 RW 009 Kel. Parang Tambung, Kec. Tamalate, Kota Makassar Kota Makassar Sulawesi Selatan	sinarpancarancahaya@gmail.com	-	\N	\N
2124	PT. KARYA ANAK SULAWESI	24	7303	Jl. Costa Brava No. 39 Espana Residence Tanjung bungan Makassar Kota Makassar Sulawesi Selatan	kasulawesi2016@gmail.com	-	\N	\N
2125	PT. Makarya Anugerah Sejati	24	7303	Jl. Orchid view No. 19 Kota Makassar Sulawesi Selatan	makarya_anugerah_sejati@yahoo.co.id	-	\N	\N
2126	PT. Baruga Asrinusa Development	24	7303	Jl. Raya Baruga No. 1 Sektor Mahameru Makassar Manggala Kec. Antang Kota Makassar Sulawesi Selatan	tradbad@gmail.com	-	\N	\N
2127	PT. SAMARI BONE RAYA	24	7303	JL. MANNURUKI II PERUM GRIYA SYARTINI BLOK C NO. 12 KOTA MAKASSAR Kota Makassar Sulawesi Selatan	pt.samariboneraya@gmail.com	-	\N	\N
2128	PT. Bili Bili Makassar Utama	24	7303	JL. TODDOPULI RAYA PERUM VILLA SURYA MAS BLOK G4 NO 1 Kota Makassar Sulawesi Selatan	bilibilimakassar@yahoo.com	-	\N	\N
2129	PT. PANCARAN CAHAYA ABADI	24	7303	Jl. Hartaco Indah III S No. 15 Rt.003 Rw.009 Kel. Parang Tambung Kec. Tamalate Kota Makassar Sulawesi Selatan	pusat.askomelin@gmail.com	-	\N	\N
2130	PT. PARAIKATTE KARYA MANDIRI	24	7303	Jl.Gunung Merapi No.74 Kel.Lajangiru Kec.Ujung Pandang Kota Makassar Sulawesi Selatan	fadlisugandi87@gmail.com	-	\N	\N
2131	PT. JAYA CAHAYA UTAMA	24	7303	jl. Perumtel 2 Blok C1 No.15 Kota Makassar Sulawesi Selatan	jayacahayautama@gmail.com	-	\N	\N
2132	PT. FAMILY ENERGY INDONESIA	24	7303	JL.GOA RIA KOMP.GRIYA PRATAMA SUDIANG BLOK C2 NO 12 MAKASASAR Kota Makassar Sulawesi Selatan	pt.fenindo@yahoo.co.id	-	\N	\N
2133	PT. Cahaya Alam Teknik	24	7303	btn sao sarana indah blok A13/17 Kota Makassar Sulawesi Selatan	riaz.ahmad.zd@gmail.com	-	\N	\N
2134	PT. TRI LINTAS NUSANTARA	24	7303	JL. BOULEVARD RUKO JASCINTH II NO 23 Kota Makassar Sulawesi Selatan	trilintasnusantara@yahoo.co.id	-	\N	\N
2135	PT. MITRA ELEKTRIKAL PERKASA	24	7303	JL. SAWERIGADING NO. 32 KEL. BATUPASI KEC. WARA UTARA KOTA PALOPO SULAWESI SELATAN Kota Palopo Sulawesi Selatan	pt.mitra.elektrikal.perkasa@gmail.com	-	\N	\N
2136	PT. DIAN TEKNIK ELEKRINDO	24	7303	JL. PERUMAHAN BUMI TAKKALALA PERMAI BLOK D 02 KEL. TAKKALALA KEC. WARA SELATAN KOTA PALOPO SULAWESI SELATAN Kota Palopo Sulawesi Selatan	pt.dianteknikelekrindo@gmail.com	-	\N	\N
2137	PT. DIAH TEHNIK PALOPO	24	7303	BTN NYIUR JL. LIBUKANG BLOK A.03,NO. 03 Kota Palopo Sulawesi Selatan	sudirmana84@gmail.com	-	\N	\N
2138	PT. BHAKTI PRIBUMI RESKYTA	24	7303	PERUMAHAN PNS SONGKA BLOK B 4 NO. 2 PALOPO Kota Palopo Sulawesi Selatan	pt.bhaktipribumireskyta@yahoo.com	-	\N	\N
2139	PT. Malatunrung Rezkindo	24	7303	Jl. Andi Mappanyompa Non Blok Malatunrung Kec. Wara Timur Kota Palopo Sulawesi Selatan	malatunrungrezkindo@gmail.com	-	\N	\N
2140	PT. ENERGI CIPTA MANDIRI CORPORINDO	24	7303	BTN MERDEKA NON BLOK NO.16 PALOPO Kota Palopo Sulawesi Selatan	pt.energiciptamandiricorporindo@gmail.com	-	\N	\N
2141	PT. KARYA ELEKTRIK KONSTRUKSI	24	7303	JL. PULAU SERAM KEL. MALATUNRUNG KEC. WARA TIMUR Kota Palopo Sulawesi Selatan	pt.karyaelektrikkonstruksi@yahoo.com	-	\N	\N
2142	PT. SUMBER CAHAYA PARE	24	7303	Jl. Andi Makkasau No. 21 Kampung Pisang Kel. Kampung Pisang Kec. Soreang Kota ParePare Sulawesi Selatan Kota Pare Pare Sulawesi Selatan	sumber_cahaya84@yahoo.com	0421-21018, 0421-22808 / 081341581100	\N	\N
2143	PT. DUTA KONSTRUKSI MEKANIKAL ELEKTRIKAL	24	7303	JALAN BAU MASSEPE NO. 185, KOTA PAREPARE Kota Pare Pare Sulawesi Selatan	jerryjong87@gmail.com	082137423334	\N	\N
2144	PT. AMAL JAYA INDONESIA	24	7303	JALAN BAU MASSEPE NO. 185, KOTA PAREPARE Kota Pare Pare Sulawesi Selatan	rachman.amaljayaindonesia@gmail.com	0811417581	\N	\N
2145	PT. NUR KARYA ELEKTRIK PARE	24	7303	JL. KIJANG NO. 1 LABUKKANG KEC. UJUNG KOTA PARE PARE SULAWESI SELATAN Kota Pare Pare Sulawesi Selatan	nurkaryaelektrik@yahoo.com	0421-23078 / 081242861254	\N	\N
2146	PT. BINTANG INTI GELORA	24	7303	Jl. Bumi Galung Maloang Blok A No. 27 KEL. GALUNG KEC. BACUKIKI Kota Pare Pare Sulawesi Selatan	big.makassar@yahoo.com	-	\N	\N
2147	PT. MURNI PERKASA ELECTRIK	24	7303	Jl. Beringin BTN Blok A/4 Kota Pare-Pare Bumi Harapan Kec. Bacukiki Barat Kota Pare Pare Sulawesi Selatan	lskn_pusat@yahoo.co.id	-	\N	\N
2148	PT. MURNI PERKASA ELECTRIC	24	7303	JALAN BERINGIN KOMP.BTN A/4 KOTA PARE-PARE Kota Pare Pare Sulawesi Selatan	jumriatiati@gmail.com	-	\N	\N
2152	PT. PERDANA SATRIA LUWUK	24	7303	Jl. D.I Panjaitan No. 11 Luwuk Kab. Banggai Sulawesi Tengah	perdana_satria77@yahoo.com	0461-22092	\N	\N
2153	PT. ANEKA PRIMA MONDOK	24	7303	Jl. Jend A. Yani No.102 Luwuk Kec. Luwuk Kab. Banggai Sulawesi Tengah	apmondok@yahoo.com	-	\N	\N
2154	PT. Jafpat Jaya Abadi	23	7201	JL.KRI TELUK WAJO RT 05 DUSUN 03 DESA BAKA KECAMATAN TINANGKUNG Kab. Banggai Kepulauan Sulawesi Tengah	jafpatbangkep@gmail.com	(0462)2222295/082193045012	\N	\N
2155	PT. LIPU BULAGI BERJAYA	23	7201	Jl. Pasar Tua Bulagi Satu Kec. Bulagi Kab. Banggai Kepulauan Sulawesi Tengah	mlipubulagi@yahoo.com	-	\N	\N
2156	PT. MOKOPONU PEEMA	23	7207	Jalan Tugu Leok I Kel. Leok I Kec. Biau Kab. Buol Sulawesi Tengah	mokoponupeema@yahoo.com	0453-21797	\N	\N
2157	PT. BUMI DAMPELAS RAYA	23	7205	JL. SAMIUN NO 28 DESA SABANG KEC. DAMPELAS Kab. Donggala Sulawesi Tengah	bumidampelas@gmail.com	085299667101	\N	\N
2158	PT. INSTAL ANUGERAH JAYA	23	7206	JL. JENDRAL SUDIRMAN NO.101 Kab. Toli Toli Sulawesi Tengah	pt.instalanugerahjaya@yahoo.com	081341367775	\N	\N
2159	PT. OTAKARIA	23	7206	Jl. Jend. Ahmad Yani No. 89 Kel. Baru Kec. Baolan Kab. Toli Toli Sulawesi Tengah	pt.otakaria@yahoo.com	0453 - 21206	\N	\N
2160	PT. MACINI INDAH PERKASA	23	7206	Jl. Moh. Hatta No. 207 Baru Kec. Baolan Kab. Toli Toli Sulawesi Tengah	macini_indah@yahoo.com	0453-21205	\N	\N
2161	PT. BOANERGES ANUGERAH SEJAHTERA	23	7271	Jl. Towua Lrg.Malaya Palu Kota Palu Sulawesi Tengah	pt.boanergesanugerahsejahtera@yahoo.com	082243960731	\N	\N
2162	PT. JERICO PRATAMA PALU	23	7271	Jl. Tg. Tururuka. Lrg. Mercury. Palu Kota Palu Sulawesi Tengah	ptjericopratamapalu@yahoo.com	085145256059	\N	\N
2163	PT. CAHAYA ZALVI PRATAMA	23	7271	Jl. Dayo Dara BTN. Lagarutu Blok F No. 18 A CPI 3 Kota Palu Sulawesi Tengah	rizal2779@yahoo.com	085256888318	\N	\N
2164	PT. MARTOBORO MITRA JAYA	23	7271	Jl. Sungai Balantak No. 21 Nunu Kec. Tatanga Kota Palu Sulawesi Tengah	martoborome@yahoo.co.id	0451 - 422490	\N	\N
2165	PT. INTI INDO MAKMUR	23	7271	Jl. Miangas I No. 9 Lolu Selatan Kec. Palu Selatan Kota Palu Sulawesi Tengah	pt_inti_indo_makmur@yahoo.co.id	0451-458856	\N	\N
2166	PT. INSTALASI	23	7271	JL. SULAWESI NO. 44 B Kota Palu Sulawesi Tengah	pt.instalmakmurjayasentosapt@gmai.com	082292662453	\N	\N
2167	PT. BELONA JAYA MANDIRI	23	7271	JLN. TANJUNG DAKO NO.53 Kota Palu Sulawesi Tengah	pt.belonajayamandiri@yahoo.com	082292662453	\N	\N
2168	PT. Mitratama Energi Selebes	23	7271	Jl. karajalemba Perum Kaluku Indah Blok D6 no 34 Kota Palu Sulawesi Tengah	mitratama.palu@gmail.com	-	\N	\N
2169	PT. JAYA TRIKON MANDIRI	23	7271	Jl. Seruni Raya No. 18 Balaroa Kec. Palu Barat Kota Palu Sulawesi Tengah	pt.jayatrikonmandiri@gmail.com	-	\N	\N
2170	PT. POPULA JAYA MANDIRI	23	7271	Jl. Bali No. 22 Lolu Selatan Kec. Palu Selatan Kota Palu Sulawesi Tengah	popula_palu@yahoo.co.id	-	\N	\N
2171	PT. CAHAYA DAMAI ELECTRICAL	23	7271	Jl. Purnawirawan II No. 35 Kel. Tatura Utara Kec. Palu Selatan Kota Palu Sulawesi Tengah	lskn_pusat@yahoo.co.id	-	\N	\N
2172	PT. SETIA MULIA ABADI	23	7271	Jl. Kerinci No. 2 Kel. Talise Kec. Mantikulore Kota Palu Sulawesi Tengah	lskn_pusat@yahoo.co.id	-	\N	\N
2173	PT. VOLTA ELECTRICAL DAN MECANICAL	23	7271	JL. KARANA BTN II BLOK E NO.37. Kota Palu Sulawesi Tengah	yc8pjt@gmail.com	-	\N	\N
2174	PT. INSTALCON	23	7271	Jl. Tanjung Santigi No.24 Lolu Selatan Kec. Palu Selatan Kota Palu Sulawesi Tengah	ptinstalcon@gmail.com	-	\N	\N
2175	PT. GANDARIA ARTA MANDIRI	23	7271	Jl.Sis Aljufri No. 91 Boyaoge Kec. Palu Barat Kota Palu Sulawesi Tengah	pt.gandaria@gmail.com	-	\N	\N
2176	PT. JAYA KENCANA ABADI	23	7271	Jl. Sis Aljufri No. 62 Bayauge Kec. Palu Barat Kota Palu Sulawesi Tengah	pt.jayakencanaabadi@gmail.com	-	\N	\N
2177	PT. SARANA ENERGI HANDAL	23	7271	Jl. Zebra I No. 82 Birobuli Utara Kec. Palu Selatan Kota Palu Sulawesi Tengah	pt.saranaenergi_handal@yahoo.com	-	\N	\N
2178	PT. GUNUNG SARI MITRAKO	23	7271	Jl. Danau Lindu Blok I No. 1A Siranindi Kec. Palu Barat Kota Palu Sulawesi Tengah	pt.gunungsarimitrako@gmail.com	-	\N	\N
2179	PT. NIAGA BARU MADUMA RAYA	23	7271	Jl. Pulau Halmahera No. 2 Lolu Selatan Kec. Palu Timur Kota Palu Sulawesi Tengah	pt.nbmr.palu@gmail.com	-	\N	\N
2180	PT. DIAN PRIMA JAYA	23	7271	Jl. P. Halmahera No. 2 Lolu Selatan Kec. Palu Timur Kota Palu Sulawesi Tengah	dian_primajaya@yahoo.com	-	\N	\N
2181	PT. HIKARI PUTRAKO MANDIRI	23	7271	Jl. Sungai Moutong No. 9A Ujuna Kec. Palu Barat Kota Palu Sulawesi Tengah	pt.hikariputramandiri@gmail.com	-	\N	\N
2182	PT. MEGATRONIC ANUGRAH ABADI	23	7271	Jl. Tombolotutu Lr. Ayu No. 143 C Talise Kec. Palu Timur Kota Palu Sulawesi Tengah	amegatronic@yahoo.com	-	\N	\N
2183	PT. INDO RAYA NAGAYA	23	7271	JL. TOWUA NO. 72, TATURA SELATAN, KEC. PALU SELATAN Kota Palu Sulawesi Tengah	aklidpp@gmail.com	-	\N	\N
2184	PT. INDO NUSA MANDIRI	23	7271	JL. TOWUA NO. 72 TATURA SELATAN, KEC. PALU SELATAN Kota Palu Sulawesi Tengah	aklidpp@gmail.com	-	\N	\N
2185	PT. WENANG PERKASA ABADI	23	7271	JL. WOLTER MONGINSIDI NO 82 Kota Palu Sulawesi Tengah	perkasa.wenang@yahoo.co.id	-	\N	\N
2186	PT. ALPHA MULIA MANDIRI	23	7271	Jl. TG Manimbaya Lrg mandjerina Tatura Utara Kec. Palu Selatan Kota Palu Sulawesi Tengah	aklidpp@gmail.com	-	\N	\N
2187	PT. BUDI JAYA CITRA UTAMA	23	7271	Jl. Pulo Halmahera No. 2 lolu selatan, Kec. Palu Timur Kota Palu Sulawesi Tengah	aklidpp@gmail.com	-	\N	\N
2188	PT. AURELLIA RAFIFA BUTON	22	7406	Jl. Jendral Sudirman Kel. Lauru Kec. Rumbia Tengah Kab. Bombana Sulawesi Tenggara	pt.aurelliarafifabuton@gmail.com	085230375566	\N	\N
2189	PT. BUMDES TAMBEA SULTRA	22	7404	Dusun I Sanrebulu Kel. Tambea Kec. Pomalaa Kab. Kolaka Sulawesi Tenggara	asdar.astra@yahoo.com	085241674274	\N	\N
2190	CV. WOLIO JAYA	22	7404	JL. POROS TONDOWOLIO DESA TONDOWOLIO KECAMATAN TANGGETADA Kab. Kolaka Sulawesi Tenggara	dumboyakuza@gmail.com	085394443360	\N	\N
2191	PT. Rahmat Jaya Mandiri	22	7404	Jl. Indumo Kel. Watuliandu Kec. Kolaka Kab. Kolaka Sulawesi Tenggara	pt.rahmatjayamadiri@gmai.com	-	\N	\N
2192	PT. AYUNDA TEKHNIK SULTRA	22	7404	JL. KELINCI NO. 31 KEL. LALOMBAA KEC. KOLAKA KAB. KOLAKA SUL-TRA Kab. Kolaka Sulawesi Tenggara	ary.aryadi77@gmail.com	-	\N	\N
2193	PT. PRIMA CENTRAL ENGINEERING	22	7408	JL. TOMAKKEDA, KELURAHAN LASUSUA, KECAMATAN LASUSUA, KABUPATEN KOLAKA UTARA Kab. Kolaka Utara Sulawesi Tenggara	primacentralengineering@gmail.com	-	\N	\N
2194	PT. GUSMOTAHA TEHNIK PERKASA	22	7408	JL. GUNUNG TOJABI KEL. TOJABI KEC. LASUSUA Kab. Kolaka Utara Sulawesi Tenggara	ptgusmotaha@gmail.com	-	\N	\N
2195	PT. GUSMOTAHA TEKNIK PERKASA	22	7408	Jl. Gunung Tojabi Kel. Tojabi Kec. Lasusua Kab. Kolaka Utara Sulawesi Tenggara	pusat@ptaskomelin.co.id	-	\N	\N
2196	PT. BENTENG LIPUTAMA	22	7403	JL. MT HARYONO KOMPLEKS RUKO BCA PETAK 7-8 KEL. WAWONGGOLE KEC. UNAAHA Kab. Konawe Sulawesi Tenggara	anovaproperty@gmail.com	-	\N	\N
2197	PT. WONUA SANGIA ELEKTRIK	22	7405	DS. ANGGONDARA KEC. PALANGGA Kab. Konawe Selatan Sulawesi Tenggara	ptwonuasangiaelektrik@gmail.com	-	\N	\N
2198	PT. SEJAHTERA MANDIRI KONDA	22	7405	Jalan Mayjend Katamso No. 51 Puso Jaya Kec. Konda Kab. Konawe Selatan Sulawesi Tenggara	sejahteramandirikonda@gmail.com	-	\N	\N
2199	PT. WANUA SANGIA ELEKTRIK	22	7405	Desa Anggondara Anggondara Kec. Palangga Kab. Konawe Selatan Sulawesi Tenggara	pusat@ptaskomelin.co.id	-	\N	\N
2200	CV. INTAN BERLIAN SEJATI	22	7402	Jalan Landak No 14 Kel Raha III Kec. Katobu Kab. Muna Sulawesi Tenggara	cv.intanberlianraha@gmail.com	082193095187	\N	\N
2201	PT. GINAR ARRAZAQ ELEKTRIK	22	7402	Jalan Sultan Syahrir No. 6 Blok D Kel. Raha I Kec. Katobu Kab. Muna Sulawesi Tenggara	ginarelectrik68@gmail.com	-	\N	\N
2202	PT. SOULTAN FINA TEKNIK	22	7472	Jalan Jend. Sudirman No. 70 B Kel. Wangkanapi Kec. Wolio Kota Bau Bau Sulawesi Tenggara	soultanfinateknik@gmail.com	082394262727	\N	\N
2203	PT. AZZABUUR REZKY BAROKAH	22	7472	Jl. Jend. Sudirman No.1, Kel. Wale, Kec Wolio Kota Baubau Prov. Sulawesi Tenggara Kota Bau Bau Sulawesi Tenggara	pt.azzabuurrezkybarokah@gmail.com	081241611379	\N	\N
2204	PT. RAMA CAKRA NAYLA	22	7472	Jalan Jend. Sudirman No. 70B Kel. Wangkanapi Kec. Wolio Kota Bau Bau Sulawesi Tenggara	ramacakrateknik@gmail.com	082187438192	\N	\N
2205	PT. SINAR HARAPAN ELEKTRIK	22	7472	Jl. Teuku Umar No.26 Rt.01/03 Kel. Batulo Kec. Wolio Kota Bau Bau Sulawesi Tenggara	rabiulashar@yahoo.com	0811401145	\N	\N
2206	PT. PUTRA GOWA SAMA JAYA	22	7472	Jalan Erlangga Lrg. Panti Asuhan RT 020 RW 005 Kel. Bone Bone Kec. Batupoaro Kota Bau Bau Sulawesi Tenggara	putragowasamajaya@gmail.com	085255200419	\N	\N
2207	PT. INTAN BERLIAN ABADI	22	7472	Jalan Bakti ABRI No. 11E Bukit Wolio INdah Kec. Wolio Kota Bau Bau Sulawesi Tenggara	aklidpp@gmail.com	-	\N	\N
2208	PT. Melwin Electric Rekadaya	22	7472	JL. BAKTI ABRI BTN PALA TIGA B1A NO.5 KOTA BAUBAU Kota Bau Bau Sulawesi Tenggara	melwinelectric@gmail.com	-	\N	\N
2209	PT. FAFA MOLAGINA PRATAMA	22	7472	Jl. Artum No.75A Rt.02 Rw.02 Katobengke Kec. Betoambari Kota Bau Bau Sulawesi Tenggara	pt.fafamolagina@gmail.com	-	\N	\N
2211	PT. Mentari Mandiri Sejahtera	22	7472	Jl. Pahlawan Kel. Bukit Wolio Indah Kec. Wolio Kota Bau Bau Sulawesi Tenggara	pt.mentarimandirisejahtera@yahoo.com	-	\N	\N
2212	PT. TIJARI MUBARAK	22	7406	JL. JATI RAYA NO. 41 B KEL. WOWAWANGGU KEC. KADIA KOTA KENDARI PROV. SULAWESI TENGGARA Kota Kendari Sulawesi Tenggara	pt.tijarmubarak@yahoo.co.id	0401-3196709	\N	\N
2213	PT. METRO TEKNIK JAYA UTAMA	22	7406	Jl. Simbo No 10 Kel. Watubangga Bonggoeya Kec. Baruga Kota Kendari Sulawesi Tenggara	metroteknikjayautama01@gmail.com	081241353799	\N	\N
2214	PT. CITRA UTAMA INTERNUSA	22	7406	JL. BUDI UTOMO LORONG SEPAKAT NO. 13B, KENDARI Kota Kendari Sulawesi Tenggara	pt.citra.utama.internusa@gmail.com	081341706701	\N	\N
2215	PT. TUNGGALA PRIMA TEKNIK	22	7406	Jalan KH. Ahmad Dahlan No 14 Bonggoeya Kecamatan Wua Wua Kota Kendari Sulawesi Tenggara	tunggalaprimateknik@gmail.com	0811409815	\N	\N
2216	PT. DIAN TEKNIK UTAMA KENDARI	22	7406	Jalan Cakalang No 17 RT 011 RW 004 Kelurahan Sanua Kecamatan Kendari Barat Kota Kendari Sulawesi Tenggara	dian.lpse@gmail.com	0401-3126348	\N	\N
2217	PT. IKHTIAR BERSAMA INDONESIA	22	7406	Jl. Mayjen Katamso Perumahan Baruga Griya Asri Blok B No. 4 Kendari Kota Kendari Sulawesi Tenggara	pt.ibi30@gmail.com	0852 4173 0401	\N	\N
2218	PT. ANDIRA JAYA RAYA	22	7406	JL. TOARIMA II NO.30 PUNGGOLAKA KECAMATAN PUUWATU Kota Kendari Sulawesi Tenggara	cv.andira.jaya@gmail.com	-	\N	\N
2219	PT. ANGKASA UTAMA KENDARI	22	7406	BTN. Tunggala Jl. TK No.1 Kel. Anawai Kec. Wua-Wua Kota Kendari Sulawesi Tenggara	ptangkasautama@gmail.com	-	\N	\N
2220	PT. AKILIKIWO TEKNIK NUSANTARA	22	7406	JL. Kelengkeng Kel. Andonohu Kec. Poasia Kota Kendari Kota Kendari Sulawesi Tenggara	akilikiwotekniknusantara@yahoo.com	-	\N	\N
2221	PT. HADSUL SAMUDRA JAYA	22	7406	Jl. Abunawas No.1 Rt.001 Rw.001 Bende Kec. Kadia Kota Kendari Sulawesi Tenggara	pt.hjs@gmail.com	-	\N	\N
2222	PT. ANOA MULTY ENERGY	22	7406	KOMPLEKS PERUMAHAN BTN KENDARI PERMAI BLOK G1 NO. 8 RT.06 RW.02 KEL. PADALEU KEC. KAMBU Kota Kendari Sulawesi Tenggara	jhurazenith@yahoo.co.id	-	\N	\N
2223	PT. ANUGRAH PUTRA FAJAR GLOBAL	22	7406	Jl. Sewangi Kel. Abeli Kec. Abeli Kota Kendari Sulawesi Tenggara	mpfg.kendari@gmail.com	-	\N	\N
2224	PT. MULTI ELECTRICAL INDONESIA	22	7406	Jl. Maranai No. 88 Perumnas Kendari Kel. Bende Kec. Kadia Kota Kendari Sulawesi Tenggara	multielectricalindonesia01@gmail.com	-	\N	\N
2225	PT. KENDARI LINTAS ENERGI	22	7406	Jl. Haeba V Kota Kendari Sulawesi Tenggara	kendarilintasenergi03@gmail.com	-	\N	\N
2226	PT. BUMI RAYA MEGATRINDO	22	7406	Jalan Nanga Nanga RT 10 RW 03 Padaleu Kec. Kambu Kota Kendari Sulawesi Tenggara	pt.bumirayamegatrindo@gmail.com	-	\N	\N
2227	PT. PRAJAYA UTAMA KENDARI	22	7406	Jalan Bunga Kamboja No. 21 A Kel. Lahundape Kec. Kendari Barat Kota Kendari Sulawesi Tenggara	prajaya.utama@gmail.com	-	\N	\N
2228	PT. KENCANA TIMUR PERKASA	22	7406	JALAN H.E.A.MOKODOMPIT KOTA KENDART Kota Kendari Sulawesi Tenggara	jumriatiati@gmail.com	-	\N	\N
2229	PT. Global Electrical Indonesia	22	7406	Jl. Jend. A.H. Nasution No.32 Kel. Kambu Kec. Kambu Kota Kendari Sulawesi Tenggara	ge_indonesia@yahoo.com	-	\N	\N
2230	PT. Nur Pandawa Teknik	22	7406	Jl.H.E.A. Mokodompit Lrg. Mata Air I, Kelurahan Lalolara, Kecamatan Kambu Kota Kendari Sulawesi Tenggara	nurpandawa1@gmail.com	-	\N	\N
2231	PT. DIAN PURNAMA KENDARI	22	7406	Jalan Tunggala No. 49 Kel. Anawai Kec. Wua Wua Kota Kendari Sulawesi Tenggara	ptdian_purnama@yahoo.co.id	-	\N	\N
2240	PT. SUNAN GIRI JAYA	21	7174	RT. 08 RW. 04 Lingkungan 04, Kelurahan Sinindian, Kecamatan Kotamobagu Timur, Kota Kotamobagu. Sulawesi Utara Kota Kotamobagu Sulawesi Utara	Wahidmkalalag@gmail.com	08124428341	\N	\N
2241	PT. POLANDOUW	21	7171	Jl.Raya Winangun Blok 1-B Komplek Ruko Winangun 95261 Kota Manado Kota Manado Sulawesi Utara	pt.polandouw@gmail.com	0431 826052	\N	\N
2242	PT. CELEBES PROSPERINDO PRATAMA	21	7171	Jl. Bailang no. 55 lingk 1 Bailang Kec. Bunaken Kota Manado Sulawesi Utara	aklidpp@gmail.com	081910555785	\N	\N
2243	PT. GAYA TEKNIK MALEOSAN	21	7171	JALAN NUSANTARA II NO 82 CALACA KEC. WENANG, MANADO Kota Manado Sulawesi Utara	aklidpp@gmail.com	082232771731	\N	\N
2244	PT. SAHABAT MAKNA SEJATI	21	7171	JL. SAM RATULANGI NO. 229 Kota Manado Sulawesi Utara	vitowrp72@gmail.com	081244044444	\N	\N
2245	PT. JAGO ELFAH ANUGERAH	21	7171	Pall Dua Lingk. II, No. 3, Manado Kota Manado Sulawesi Utara	jago.elfah.anugerah@gmail.com	-	\N	\N
2246	PT. WAILAN JAYA SEJATI	21	7173	JL. opo worang ling VII Kelurahan Wailan, kecamatan tomohon utara Kota Tomohon Sulawesi Utara	bilibilimakassar@yahoo.com	-	\N	\N
2247	PT. NADYA JAYA TEHNIK	3	1208	Jl. Protokol Dusun I Desa Gedangan Kec. Pulo Bandring Kab. Asahan Sumatera Utara	ptnjt@yahoo.com	085371400300	\N	\N
2248	CV. ZULITA	3	1219	Jl. T. Rahmadsyah No. 38 Sipare-pare Kc. Air Putih Kab. Batu Bara Sumatera Utara	cv.zulita@gmail.com	-	\N	\N
2249	CV. BIRO TEHNIK HINSA	3	1215	Lumban Parsait Desa Tapian Nauli Kec. Lintongnihuta Kab. Humbang Hasundutan Sumatera Utara	cv.biroteknik123@gmail.com	0813-70519811	\N	\N
2250	CV. ARGAMA KARYA	3	1215	Jl. Sisingamangaraja Gg. GBI Simapang Bakara Pasaribu Kec. Dolok Sanggul Kab. Humbang Hasundutan Sumatera Utara	argamakarya@yahoo.com	-	\N	\N
2251	PT. JAYA SINAR CAHAYA ABADI	3	1218	Jl. Besar Firdaus Km.58 Desa Firdaus Kab. Serdang Bedagai Sumatera Utara	ptjsca01@yahoo.com	0621-442064	\N	\N
2252	CV. KARYA MAJU	3	1205	Jl. Sisingamangaraja No.23 Hutatoruan X Kec. Tarutung Kab. Tapanuli Utara Sumatera Utara	cvkaryamaju55@gmail.com	-	\N	\N
2253	CV. GANDA JAYA PERKASA	3	1206	Jl. Sisingamangaraja No. 6 Silaen Kec.Silaen Kab. Toba Samosir Sumatera Utara	cvgandajayaperkasa@yahoo.com	-	\N	\N
2254	PT. SION JAYA	3	1276	JL. DR WAHIDIN No. 10 LINGK .II JATI MAKMUR BINJAI UTARA KOTA BINJAI Kota Binjai Sumatera Utara	pt.sionjaya@yahoo.com	-	\N	\N
2255	PT. Putri Kharisma Pratama	3	1275	Jl. Pancing III No. 37 Medan Kota Medan Sumatera Utara	yalkonst@yahoo.co.id	085297849404	\N	\N
2256	PT. ADINDA PRATIWI ENGINEERING	3	1275	JL. Alfaka VII No. 32 A Tanjung Mulia Hilir Kota Medan Sumatera Utara	pratiwiengineering@gmail.com	061.80016226	\N	\N
2257	PT. PUTERA PERSADA JAYA	3	1275	Jl. Jend Gatot Subroto No.8 Kel.Sei Sikambing CII, Kec Medan Helvetia Kota Medan Sumatera Utara	cvputerapersada8@gmail.com	061-8464858	\N	\N
2258	PT. GODRAYNS	3	1275	Jl. Bunga Mawar XVIII No. 22 C LK-XV Kel. P.B. Selayang II Kec. Medan Selayang Kota Medan Sumatera Utara	martuasinurat@gmail.com	061-8224252	\N	\N
2259	PT. MANGUN COY	3	1275	Jl. Pelajar Timur Gg. Asahan No. 11 Kel. Binjai Kec. Medan Denai Kota Medan Sumatera Utara	ptmanguncoy@yahoo.com	061-7353917, 061 7351474	\N	\N
2260	PT. INDO RAJA TOWER	3	1275	JL. SEMBADA GG. SEDERHANA NO. 1C KEL. BERINGIN KEC. MEDAN SELAYANG KOTA MEDAN, SUMATERA UTARA Kota Medan Sumatera Utara	indorajatowerpt@yahoo.com	08116150324	\N	\N
2261	PT. Sejahtera Mulia Kencana	3	1275	Jl. Setia Budi Pasar II, Komp. Graha Tanjung Sari Blok G No. 17 Tanjung Sari Kecamatan Medan Selayang Kota Medan Sumatera Utara	sejahteramuliakencana@gmail.com	061-8213775	\N	\N
2262	PT. ENAM ENAM GROUP	3	1275	Jalan Kapten Muslim Komp. Tata Plaza Blok B No. 22 Lt. I Kota Medan Sumatera Utara	admin@enamenamgroups.com	085261162643	\N	\N
2263	PT. PUTRA SELATAN JAYA ABADI	3	1275	JL. GARU I NO 73 C LK I KEL HARJOSARI I KEC MEDAN AMPLAS Kota Medan Sumatera Utara	pt.psja@gmail.com	061-42779980	\N	\N
2264	PT. GLOBAL ALFINDO ADIGRAHA	3	1275	Bilal No. 16-C Pulo Brayan Darat I Kec. Medan Timur Kota Medan Sumatera Utara	globalfindo@gmail.com	061-6645861	\N	\N
2265	CV. PUTRA JAYA ABADI	3	1275	JL. GARU I NO 73 C Kota Medan Sumatera Utara	cv.pja123@gmail.com	081260493242	\N	\N
2266	PT. SIBRAMA SAKTI	3	1275	Jl. Karya Lingk. XII No. 53, Karang Berombak, Kec. Medan Barat Kota Medan Sumatera Utara	pt.sibramasakti@gmail.com	061-6622830	\N	\N
2267	PT. RAZZA PRIMA TRAFO	3	1275	JL. WILLIEM ISKANDAR NO. 54/54 A MEDAN Kota Medan Sumatera Utara	razza_trafo@ymail.com	0811655390	\N	\N
2268	PT. Aulia Rizky Engineering	3	1275	Jl. Bank Komp. Deli Raya Lk. I No. 84 Kel. Titi Papan Kec. Medan Deli Kota Medan Sumatera Utara	pt.auliarizky@gmail.com	061-6859316	\N	\N
2269	PT. Sinar Mutiara Engindo	3	1275	Jl. Bank Komp. Deli Raya No. 88 Lk. I Kel. Titi Papan, Kec. Medan Deli Kota Medan Sumatera Utara	smeng_88@yahoo.com	061-6840622	\N	\N
2270	PT. AGUNG CAKRA NUSANTARA	3	1275	JL. ASRAMA NO. 36 PULO BRAYAN BENGKEL BARU MEDAN TIMUR Kota Medan Sumatera Utara	acn_pt@yahoo.com	08126387046	\N	\N
2271	PT. Eka Jaya Sakti	3	1275	Komp. Multatuli Indah Blok G 17, Kel. Alur, Kec. Medan Maimun Kota Medan Sumatera Utara	pt_ekajayasakti@yahoo.com	08116029422	\N	\N
2272	PT. ARMO NACA KARYA	3	1275	Jl. Karya Jasa Gg. Horas No. 20 Lk. XI Kel. Pangkalan Masyhur Kec. Medan Johor Kota Medan Sumatera Utara	armonacakarya@yahoo.com	-	\N	\N
2273	PT. GLOBAL MENARA BERDIKARI	3	1275	Jl. AR. Hakim No. 110 - A Kel. Sukaramai 1 Kec. Medan Area Kota Medan Sumatera Utara	pt.globalmenaraberdikari@gmail.com	-	\N	\N
2274	PT. Sapta Guna Utama	3	1275	Jl. Kapt, Muslim Komp. Tata Plaza Blok B. No. 22 Lt. II Medan Dwi Kora Kecamatan Medan Helvetia Kota Medan Sumatera Utara	ptsaptagunautama@gmail.com	-	\N	\N
2275	PT. TIGA DIMENSI KARYA KONSTRUKSI	3	1275	JL. JEND. AH. NASUTION, HARJOSARI, MEDAN APLAS Kota Medan Sumatera Utara	PT.TIGADIMENSIKARYAKONSTRUKSI@YAHOO.COM	-	\N	\N
2276	PT. ZIDAN MULYA ELEKTRIK	3	1275	Jl. Cempaka Gg. Sahabat No. 3 Kel. Sari Rejo Kec. Medan Polonia Kota Medan Sumatera Utara	zidan_mulyaelektrik@yahoo.co.id	-	\N	\N
2277	CV. PATUDU RIZKY	3	1275	Jl. Brigjen Katamso Gg. Jarak No. 9A Kelurahan Kampung Baru Kota Medan Sumatera Utara	risky_engineering_medan@yahoo.co.id	-	\N	\N
2278	PT. BIMA GOLDEN POWERINDO	3	1275	Jl. DR.TD. Pardede No.8 Kel. Petisah Hulu Kec. Medan Baru Kota Medan Sumatera Utara	pusat@ptaskomelin.co.id	-	\N	\N
2279	PT. GALTON ENGINEERING	3	1275	Jalan Martimbang No.8 Masjid Kecamatan Medan Kota Kota Medan Sumatera Utara	galtonengineering@yahoo.com	-	\N	\N
2280	PT. MEDAN SMART JAYA	3	1275	Jl. Bilal No. 10 A Kelurahan Pulo Brayan Darat I Kecamatan Medan Timur Kota Medan Sumatera Utara	mdnsmartjaya@yahoo.co.id	-	\N	\N
2281	PT. KEMUNING JAYA MANDIRI	3	1277	Jl. Sultan Soripada Mulia Gg. Mulia Lk.II Kel. Tano Bato Kec. Padangsidempuan Utara Kota Padangsidimpuan Sumatera Utara	kemuningjayacv@yahoo.co.id	0634-27566	\N	\N
2282	CV. LUBUK BARA	3	1277	JL MERDEKA GANG MAWAR N0.05 PADANGSIDIMPUAN Kota Padangsidimpuan Sumatera Utara	lubukbarapsp71@gmail.com	081263194488	\N	\N
2283	PT. Cemara Karya Tangkas	3	1277	Jl. Abdul Jalil Lubis Lk. IV Kel. Batunadua Jae Kec. Padangsidimpuan Batunadua Kota Padangsidimpuan Sumatera Utara	cekatan19@gmail.com	-	\N	\N
2284	CV. PARIS	3	1208	Jl. Pangaribuan No. 1 Pematangsiantar Kel. Martimbang Kec. Siantar Selatan Kota Pematang Siantar Sumatera Utara	cvparis1990@gmail.com	-	\N	\N
2285	PT. PARIS JAYA SENTOSA	3	1208	Jl. Pangaribuan No. 20 Blk Pematangsiantar Kel. Martimbang Kec. Siantar Selatan Kota Pematang Siantar Sumatera Utara	ptparisjayasentosa@gmail.com	-	\N	\N
2286	PT. NUR AMSHARI SENTOSA TEKNIK	3	1272	Jl. Pandan Lk.III Kel. Selat Tanjung Medan, Kec. Datuk, Bandar Timur, Kota Tanjung Balai, Sumatera Utara Kota Tanjung Balai Sumatera Utara	ptamsharitehnik@gmail.com	0623-595456	\N	\N
2287	CV. INDO TEKHNIK	3	1274	Jl. Badak No. 55 J Kota Tebing Tinggi Sumatera Utara	indoteknik@live.com	-	\N	\N
\.


--
-- Name: btl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('btl_id_seq', 1, true);


--
-- Data for Name: calon_pelanggan_web; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY calon_pelanggan_web (id, created_at, updated_at, nama, email, no_telepon, alamat, tipe, status, kode_prov, kode_kota, id_kwilayah, status_daya) FROM stdin;
346	2019-03-20 09:35:50	2019-03-20 09:35:50	Ade Nugroho	adenugroho404@gmail.com	085336161364	Jl.Genuk	Baru	Belum Bayar	33	3374	59	Tambah Daya
347	2019-03-30 17:14:52	2019-03-30 17:14:52	Ade Nugroho	adenugroho404@gmail.com	0895388585433	Jl. Taman Kelud Selatan\r\nJl. Taman Kelud Selatan	Baru	Belum Bayar	10	59	\N	Pasang Baru
348	2019-03-30 17:14:52	2019-03-30 17:14:52	Ade Nugroho	adenugroho404@gmail.com	0895388585433	Jl. Taman Kelud Selatan\r\nJl. Taman Kelud Selatan	Baru	Belum Bayar	10	59	\N	Pasang Baru
349	2020-01-30 17:48:39	2020-01-30 17:48:39	Azza Mujibuz Zamzami	issam88884@gmail.com	082323276209	Alamat dua	Baru	Belum Bayar	9	45	\N	Pasang Baru
\.


--
-- Data for Name: daya; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY daya (id, daya, jenis_fasa, harga, biaya, ppn, total, terbilang, created_at, updated_at) FROM stdin;
2	900	1 Fasa	0	60000	0	60000	Enam Puluh Ribu Rupiah	\N	\N
3	1300	1 Fasa	0	95000	0	95000	Sembilan Puluh Lima Ribu Rupiah	\N	\N
4	2200	1 Fasa	0	110000	0	110000	Seratus Sepuluh Ribu Rupiah	\N	\N
5	3500	1 Fasa	30	105000	0	105000	Seratus Lima Ribu Rupiah	\N	\N
6	3900	3 Fasa	30	117000	0	117000	Seratus Tujuh Belas Ribu Rupiah	\N	\N
7	4400	1 Fasa	30	132000	0	132000	Seratus Tiga Puluh Dua Ribu Rupiah	\N	\N
8	5500	1 Fasa	30	165000	0	165000	Seratus Enam Puluh Lima Ribu Rupiah	\N	\N
9	6600	3 Fasa	30	198000	0	198000	Seratus Sembilan Puluh Delapan Ribu Rupiah	\N	\N
10	7700	1 Fasa	30	231000	0	231000	Dua Ratus Tiga Puluh Satu Rbu Rupiah	\N	\N
11	10600	3 Fasa	25	265000	0	265000	Dua Ratus Enam Puluh Lima Ribu Rupiah	\N	\N
12	11000	1 Fasa	25	275000	0	275000	Dua Ratus Tujuh Puluh Lima Rupiah	\N	\N
13	13200	3 Fasa	25	330000	0	330000	Tiga Ratus Tiga Puluh Tiga Ribu Rupiah	\N	\N
14	13900	3 Fasa	25	347500	0	347500	Tiga Ratus Empat Puluh Tujuh Ribu Lima Ratus Rupiah	\N	\N
15	16500	3 Fasa	25	412500	0	412500	Empat Ratus Dua Belas Ribu Lima Ratus Rupiah	\N	\N
16	17600	3 Fasa	25	440000	0	440000	Empat Ratus Empat Puluh Ribu Rupiah	\N	\N
17	22000	1 Fasa	25	550000	0	550000	Lima Ratus Lima Puluh Ribu Rupiah	\N	\N
18	23000	3 Fasa	25	575000	0	575000	Lima Ratus Tujuh Puluh Lima Ribu Rupiah	\N	\N
19	33000	3 Fasa	20	660000	0	660000	Enam Ratus Enam Puluh Ribu Rupiah	\N	\N
20	41500	3 Fasa	20	830000	0	830000	Delapan Ratus Tiga Puluh Ribu Rupiah	\N	\N
21	53000	3 Fasa	20	1060000	0	1060000	Satu Juta Enam Puluh Ribu Rupiah	\N	\N
22	55000	3 Fasa	20	1100000	0	1100000	Satu Juta Seratus Ribu Rupiah	\N	\N
23	66000	3 Fasa	20	1320000	0	1320000	Satu Juta Tiga Ratus Dua Puluh Ribu Rupiah	\N	\N
24	82500	3 Fasa	15	1237500	0	1237500	Satu Juta Dua Ratus Tiga Puluh Tujuh Ribu Lima Ratus Rupiah	\N	\N
25	105000	3 Fasa	15	1575000	0	1575000	Satu Juta Lima Ratus Tujuh Puluh Lima Ribu Rupiah	\N	\N
26	131000	3 Fasa	15	1965000	0	1965000	Satu Juta Sembilan Ratus Enam Puluh Lima Ribu Rupiah	\N	\N
27	147000	3 Fasa	15	2205000	0	2205000	Dua Juta Dua Ratus Lima Ribu Rupiah	\N	\N
28	164000	3 Fasa	15	2460000	0	2460000	Dua Juta Empat Ratus Enam Puluh Ribu	\N	\N
29	197000	3 Fasa	15	2955000	0	2955000	Dua Juta Sembilan Ratus Lima Puluh Lima Ribu	\N	\N
30	210000	3 Fasa	13	2730000	0	2730000	Dua Juta Tujuh Ratus Tiga Puluh Ribu Rupiah	\N	\N
31	233000	3 Fasa	13	3029000	0	3029000	Tiga Juta Dua Puluh Sembilan Ribu Rupiah	\N	\N
32	240000	3 Fasa	13	3120000	0	3120000	Tiga Juta Seratus Dua Puluh Ribu Rupiah	\N	\N
33	279000	3 Fasa	13	3627000	0	3627000	Tiga Juta Enam Ratus Dua Puluh Tujuh Ribu Rupiah	\N	\N
34	345000	3 Fasa	13	4485000	0	4485000	Empat Juta Empat Ratus Delapan Puluh Lima Ribu Rupiah	\N	\N
35	380000	3 Fasa	13	4940000	0	4940000	Empat Juta Sembilan Ratus Empat Puluh Ribu Rupiah	\N	\N
36	415000	3 Fasa	13	5395000	0	5395000	Lima Juta Tiga Ratus Sembilan Puluh Lima Ribu Rupiah	\N	\N
37	485000	3 Fasa	13	6305000	0	6305000	Enam Juta Tiga Ratus Lima Ribu Rupiah	\N	\N
38	555000	3 Fasa	13	7215000	0	7215000	Tujuh Juta Dua Ratus Lima Belas Ribu Rupiah	\N	\N
39	630000	3 Fasa	13	8190000	0	8190000	Delapan Juta Seratus Sembilan Puluh Ribu Rupiah	\N	\N
40	690000	3 Fasa	13	8970000	0	8970000	Delapan Juta Sembilan Ratus Tujuh Puluh Ribu Rupiah	\N	\N
41	830000	3 Fasa	13	10790000	0	10790000	Sepuluh Juta Tujuh Ratus Sembilan Puluh Ribu Rupiah	\N	\N
42	865000	3 Fasa	13	11245000	0	11245000	Sebelas Juta Dua Ratus Empat Puluh Lima Ribu Rupiah	\N	\N
43	970000	3 Fasa	13	12610000	0	12610000	Dua Belas Juta Enam Ratus Sepuluh Ribu Rupiah	\N	\N
44	1110000	3 Fasa	11	12210000	0	12210000	Dua Belas Juta Dua Ratus Sepuluh Ribu Rupiah	\N	\N
45	1210000	3 Fasa	7	8470000	0	8470000	Delapan Juta Empat Ratus Tujuh Puluh Ribu Rupiah	\N	\N
46	1250000	3 Fasa	11	13750000	0	13750000	Tiga Belas Juta Tujuh Ratus Lima Puluh Ribu Rupiah	\N	\N
47	1385000	3 Fasa	11	15235000	0	15235000	Lima Belas Juta Dua Ratus Tiga Puluh Lima Ribu Rupiah	\N	\N
48	1600000	3 Fasa	11	17600000	0	17600000	Tujuh Belas Juta Enam Ratus Ribu Rupiah	\N	\N
49	1730000	3 Fasa	11	19030000	0	19030000	Sembilan Belas Juta Tiga Puluh Ribu Rupiah	\N	\N
50	2180000	3 Fasa	9	19620000	0	19620000	Sembilan Belas Juta Enam Ratus Dua Puluh Ribu Rupiah	\N	\N
51	2770000	3 Fasa	9	24930000	0	24930000	Dua Puluh Empat Juta Sembilan Ratus Tiga Puluh Ribu Rupiah	\N	\N
52	3200000	3 Fasa	7	22400000	0	22400000	dua puluh dua juta empat ratus rupiah	\N	\N
53	3465000	3 Fasa	7	24255000	0	24255000	Dua Puluh Empat Juta Dua Ratus Lima Puluh Lima Ribu Rupiah	\N	\N
54	4000000	3 Fasa	7	28000000	0	28000000	dua puluh delapan juta rupiah	\N	\N
55	4330000	3 Fasa	7	30310000	0	30310000	Tiga Puluh Juta Tiga Ratus Sepuluh Ribu Rupiah	\N	\N
56	5190000	3 Fasa	5	25950000	0	25950000	Dua Puluh Lima Juta Sembilan Ratus Lima Puluh Ribu Rupiah	\N	\N
57	8305000	3 Fasa	5	41525000	0	41525000	empat puluh satu juta lima ratus dua puluh lima ribu rupiah	\N	\N
58	10000000	3 Fasa	5	50000000	0	50000000	Lima Puluh Juta Rupiah	\N	\N
59	13800000	3 Fasa	4	55200000	0	55200000	Lima Puluh Lima Juta Dua Ratus Ribu Rupiah	\N	\N
1	450	1 Fasa	0	40000	0	40000	Empat Puluh Ribu Rupiah	\N	2019-02-12 04:41:42
\.


--
-- Name: daya_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('daya_id_seq', 60, true);


--
-- Data for Name: galeri; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY galeri (id, created_at, updated_at, judul, gambar) FROM stdin;
6	2019-03-08 14:01:30	2019-03-08 14:01:30	SIP	147074065.jpg
7	2019-03-19 09:47:35	2019-03-19 09:47:35	SIP	661212846.jpg
8	2019-03-19 09:47:50	2019-03-19 09:47:50	SIP	103995811.jpg
\.


--
-- Name: galeri_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('galeri_id_seq', 13, true);


--
-- Data for Name: informasi; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY informasi (id, berita, created_at, updated_at) FROM stdin;
6	<p>VISI :&nbsp;</p><p>“MENJADI LIT-TR TERBAIK YANG BERORIENTASI KEPADA KEPUASAN PELANGGAN, DENGAN BERTUMPU PADA INTEGRITAS, INOVASI DAN KREATIFITAS”&nbsp;<br><br>MISI : &nbsp;<br>1. MEMBANGUN KOMUNIKASI YANG BAIK DENGAN PELANGGAN &nbsp;GUNA MEMAHAMI DAN DAPAT MEMBERI LAYANAN TERBAIK BAGI PELANGGAN.&nbsp;<br>2. MENJALIN KEMITRAAN YANG BERORIENTASI KEPADA KEPUASAN PELANGGAN.&nbsp;<br>3. MENGHARGAI DAN MENDORONG HADIRNYA INTEGRITAS, INOVASI DAN KREATIFITAS DI LINGKUNGAN ORGANISASI.&nbsp;<br><br>MOTTO :&nbsp;</p><p>“HADIR UNTUK MEMBERI MANFAAT”</p><p>&nbsp;</p><p>-Terima Kasih-</p>	2019-03-25 13:55:40	2020-01-14 13:15:28
\.


--
-- Data for Name: jenis_bangunan; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY jenis_bangunan (id, jenis_bangunan, created_at, updated_at) FROM stdin;
1	Perkantoran	2019-01-24 04:07:43	2019-01-24 04:07:43
2	Kesehatan / Keagamaan	2019-01-24 04:07:48	2019-01-24 04:07:48
3	Pendidikan / Kebudayaan	2019-01-24 04:07:53	2019-01-24 04:07:53
4	Perdagangan	2019-01-24 04:07:57	2019-01-24 04:07:57
5	Perhotelan	2019-01-24 04:08:02	2019-01-24 04:08:02
6	Industri	2019-01-24 04:08:06	2019-01-24 04:08:06
7	Transportasi	2019-01-24 04:08:10	2019-01-24 04:08:10
8	Lainnya	2019-01-24 04:08:15	2019-01-24 04:08:15
\.


--
-- Name: jenis_bangunan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('jenis_bangunan_id_seq', 1, false);


--
-- Data for Name: kantor_area; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY kantor_area (id, id_kwilayah, kode_kota, nama, manager, pjt, status, created_at, updated_at) FROM stdin;
1	2	1171	Banda Aceh	Anonim	Anonim	Aktif	\N	\N
2	2	1173	Langsa	Anonim	Anonim	Aktif	\N	\N
3	2	1174	Lhokseumawe	Anonim	Anonim	Aktif	\N	\N
4	2	1112	Meulaboh	Anonim	Anonim	Aktif	\N	\N
5	2	1109	Sigli	Anonim	Anonim	Aktif	\N	\N
6	2	1103	Sabulussalam	Anonim	Anonim	Aktif	\N	\N
7	3	1275	Medan	Anonim	Anonim	Aktif	\N	\N
8	3	1276	Binjai	Anonim	Anonim	Aktif	\N	\N
9	3	1218	Lubuk Pakam	Anonim	Anonim	Aktif	\N	\N
10	3	1204	Padang Sidempuan	Anonim	Anonim	Aktif	\N	\N
11	3	1273	Pematang Siantar	Anonim	Anonim	Aktif	\N	\N
12	3	1207	Rantau Prapat	Anonim	Anonim	Aktif	\N	\N
13	3	1271	Sibolga	Anonim	Anonim	Aktif	\N	\N
14	3	1201	Nias	Anonim	Anonim	Aktif	\N	\N
15	4	1471	Pekanbaru	Anonim	Anonim	Aktif	\N	\N
16	4	1473	Dumai	Anonim	Anonim	Aktif	\N	\N
17	4	2172	Tanjung Pinang	Anonim	Anonim	Aktif	\N	\N
18	4	1402	Rengat	Anonim	Anonim	Aktif	\N	\N
19	6	1671	Palembang	Anonim	Anonim	Aktif	\N	\N
20	6	1604	Lahat	Anonim	Anonim	Aktif	\N	\N
21	5	1571	Jambi	Anonim	Anonim	Aktif	\N	\N
22	5	1509	Muara Bungo	Anonim	Anonim	Aktif	\N	\N
23	8	3173	Menteng	Anonim	Anonim	Aktif	\N	\N
24	8	3174	Kebon Jeruk	Anonim	Anonim	Aktif	\N	\N
25	8	3171	Bintaro	Anonim	Anonim	Aktif	\N	\N
27	8	3173	Cempaka Putih	Anonim	Anonim	Aktif	\N	\N
28	8	3175	Bandengan	Anonim	Anonim	Aktif	\N	\N
29	8	3172	Jatinegara	Anonim	Anonim	Aktif	\N	\N
30	8	3175	Tanjung Priok	Anonim	Anonim	Aktif	\N	\N
31	8	3174	Cengkareng	Anonim	Anonim	Aktif	\N	\N
32	8	3175	Marunda	Anonim	Anonim	Aktif	\N	\N
33	8	3172	Pondok Kopi	Anonim	Anonim	Aktif	\N	\N
34	8	3172	Kramat Jati	Anonim	Anonim	Aktif	\N	\N
35	8	3171	Lenteng Agung	Anonim	Anonim	Aktif	\N	\N
36	8	3172	Ciracas	Anonim	Anonim	Aktif	\N	\N
37	8	3216	Pondok Gede	Anonim	Anonim	Aktif	\N	\N
38	7	3603	Teluk Naga	Anonim	Anonim	Aktif	\N	\N
39	7	3674	Serpong	Anonim	Anonim	Aktif	\N	\N
40	7	3674	Cikupa	Anonim	Anonim	Aktif	\N	\N
41	7	3603	Cikokol	Anonim	Anonim	Aktif	\N	\N
42	7	3673	Banten Utara	Anonim	Anonim	Aktif	\N	\N
43	7	3673	Banten Selatan	Anonim	Anonim	Aktif	\N	\N
44	9	3204	Bandung	Anonim	Anonim	Aktif	\N	\N
45	9	3271	Bogor	Anonim	Anonim	Aktif	\N	\N
46	9	3275	Bekasi	Anonim	Anonim	Aktif	\N	\N
47	9	3203	Cianjur	Anonim	Anonim	Aktif	\N	\N
48	9	3277	Cimahi	Anonim	Anonim	Aktif	\N	\N
49	9	3274	Cirebon	Anonim	Anonim	Aktif	\N	\N
50	9	3276	Depok	Anonim	Anonim	Aktif	\N	\N
51	9	3217	Gunung Putri	Anonim	Anonim	Aktif	\N	\N
52	9	3205	Garut	Anonim	Anonim	Aktif	\N	\N
53	9	3215	Karawang	Anonim	Anonim	Aktif	\N	\N
54	9	3204	Majalaya	Anonim	Anonim	Aktif	\N	\N
55	9	3214	Purwakarta	Anonim	Anonim	Aktif	\N	\N
56	9	3272	Sukabumi	Anonim	Anonim	Aktif	\N	\N
57	9	3211	Sumedang	Anonim	Anonim	Aktif	\N	\N
58	9	3278	Tasikmalaya	Anonim	Anonim	Aktif	\N	\N
59	10	3374	Semarang	Anonim	Anonim	Aktif	\N	\N
60	10	3372	Surakarta	Anonim	Anonim	Aktif	\N	\N
61	10	3319	Kudus	Anonim	Anonim	Aktif	\N	\N
62	10	3301	Cilacap	Anonim	Anonim	Aktif	\N	\N
63	10	3321	Demak	Anonim	Anonim	Aktif	\N	\N
64	10	3310	Klaten	Anonim	Anonim	Aktif	\N	\N
65	10	3371	Magelang	Anonim	Anonim	Aktif	\N	\N
66	10	3302	Purwokerto	Anonim	Anonim	Aktif	\N	\N
67	10	3375	Pekalongan	Anonim	Anonim	Aktif	\N	\N
68	10	3373	Salatiga	Anonim	Anonim	Aktif	\N	\N
69	10	3376	Tegal	Anonim	Anonim	Aktif	\N	\N
70	11	3471	Yogyakarta	Anonim	Anonim	Aktif	\N	\N
71	12	3578	Surabaya Utara	Anonim	Anonim	Aktif	\N	\N
72	12	3578	Surabaya Selatan	Anonim	Anonim	Aktif	\N	\N
73	12	3578	Surabaya Barat	Anonim	Anonim	Aktif	\N	\N
74	12	3515	Sidoarjo	Anonim	Anonim	Aktif	\N	\N
75	12	3516	Mojokerto	Anonim	Anonim	Aktif	\N	\N
76	12	3575	Pasuruan	Anonim	Anonim	Aktif	\N	\N
77	12	3522	Bojonegoro	Anonim	Anonim	Aktif	\N	\N
78	12	3525	Gresik	Anonim	Anonim	Aktif	\N	\N
79	12	3577	Madiun	Anonim	Anonim	Aktif	\N	\N
80	12	3510	Banyuwangi	Anonim	Anonim	Aktif	\N	\N
81	12	3509	Jember	Anonim	Anonim	Aktif	\N	\N
82	12	3506	Kediri	Anonim	Anonim	Aktif	\N	\N
83	12	3507	Malang	Anonim	Anonim	Aktif	\N	\N
84	12	3528	Pamekasan	Anonim	Anonim	Aktif	\N	\N
85	12	3502	Ponorogo	Anonim	Anonim	Aktif	\N	\N
86	12	3512	Situbondo	Anonim	Anonim	Aktif	\N	\N
87	13	5171	Bali Selatan	Anonim	Anonim	Aktif	\N	\N
88	13	5171	Bali Timur	Anonim	Anonim	Aktif	\N	\N
89	13	5171	Bali Utara	Anonim	Anonim	Aktif	\N	\N
90	14	5271	Mataram	Anonim	Anonim	Aktif	\N	\N
91	14	5204	Sumbawa	Anonim	Anonim	Aktif	\N	\N
92	14	5272	Bima	Anonim	Anonim	Aktif	\N	\N
93	14	5371	Kupang	Anonim	Anonim	Aktif	\N	\N
94	15	5371	Kupang	Anonim	Anonim	Aktif	\N	\N
95	15	5309	Flores Barat	Anonim	Anonim	Aktif	\N	\N
96	15	5309	Flores Timur	Anonim	Anonim	Aktif	\N	\N
97	15	5302	Sumba	Anonim	Anonim	Aktif	\N	\N
98	16	8171	Ambon	Anonim	Anonim	Aktif	\N	\N
99	16	8103	Masohi	Anonim	Anonim	Aktif	\N	\N
100	17	8271	Ternate	Anonim	Anonim	Aktif	\N	\N
101	17	7605	Sofifi	Anonim	Anonim	Aktif	\N	\N
102	17	8172	Tual	Anonim	Anonim	Aktif	\N	\N
103	18	6472	Samarinda	Anonim	Anonim	Aktif	\N	\N
104	18	6471	Balikpapan	Anonim	Anonim	Aktif	\N	\N
105	18	6405	Berau	Anonim	Anonim	Aktif	\N	\N
106	18	6474	Bontang	Anonim	Anonim	Aktif	\N	\N
107	19	6371	Banjarmasin	Anonim	Anonim	Aktif	\N	\N
108	19	6307	Barabai	Anonim	Anonim	Aktif	\N	\N
109	19	6302	Kota Baru	Anonim	Anonim	Aktif	\N	\N
110	20	6171	Pontianak	Anonim	Anonim	Aktif	\N	\N
111	20	6106	Ketapang	Anonim	Anonim	Aktif	\N	\N
112	20	6105	Sanggau	Anonim	Anonim	Aktif	\N	\N
113	20	6172	Singkawang	Anonim	Anonim	Aktif	\N	\N
114	21	7171	Manado	Anonim	Anonim	Aktif	\N	\N
115	21	7571	Gorontalo	Anonim	Anonim	Aktif	\N	\N
116	21	7174	Kotamobagu	Anonim	Anonim	Aktif	\N	\N
117	22	7471	Kendari	Anonim	Anonim	Aktif	\N	\N
118	22	7472	Bau-Bau	Anonim	Anonim	Aktif	\N	\N
119	23	7271	Palu	Anonim	Anonim	Aktif	\N	\N
120	23	7202	Luwuk	Anonim	Anonim	Aktif	\N	\N
121	23	7206	Toli-Toli	Anonim	Anonim	Aktif	\N	\N
122	24	7373	Palopo	Anonim	Anonim	Aktif	\N	\N
123	24	7371	Makassar Selatan	Anonim	Anonim	Aktif	\N	\N
124	24	7371	Makassar Utara	Anonim	Anonim	Aktif	\N	\N
125	24	7302	Bulu Kumba	Anonim	Anonim	Aktif	\N	\N
126	24	7372	Pare-Pare	Anonim	Anonim	Aktif	\N	\N
127	24	7315	Pinrang	Anonim	Anonim	Aktif	\N	\N
128	24	7311	Watampone	Anonim	Anonim	Aktif	\N	\N
129	25	9471	Jayapura	Anonim	Anonim	Aktif	\N	\N
130	25	9171	Sorong	Anonim	Anonim	Aktif	\N	\N
131	25	9409	Biak	Anonim	Anonim	Aktif	\N	\N
132	25	9105	Manokwari	Anonim	Anonim	Aktif	\N	\N
133	25	9401	Merauke	Anonim	Anonim	Aktif	\N	\N
134	25	9412	Timika	Anonim	Anonim	Aktif	\N	\N
26	8	3674	Ciputat	Anonim	Anonim	Aktif	\N	\N
135	26	1174	WARINGIN BARAT	ISTIKOMAH	EDY KURNIA SITUMEANG	Aktif	2019-08-08 16:27:33	2019-08-08 16:27:33
136	32	1175	PADANG	BP PONO	EDY KURNIA SITUMEANG	Aktif	2019-08-09 11:01:45	2019-08-09 11:01:45
137	2	1103	tes area satu	manager satu	pjt satu	Aktif	2020-01-08 23:36:49	2020-01-08 23:36:49
\.


--
-- Name: kantor_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('kantor_area_id_seq', 137, true);


--
-- Data for Name: kantor_wilayah; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY kantor_wilayah (id, nama_wilayah, general_manager, kode_prov, created_at, updated_at, alamat, telp, email, nomor) FROM stdin;
2	Nangroe Aceh Darussalam	Ir. Twk Muntazar	11	2019-02-18 02:26:06	2019-02-18 02:26:06	Lt. 1 Jl. Tgk. Cik Di Tiro no. 10 Kampong Peuniti Kec. Baiturahman Kota Banda Aceh	08126918000	sipnad@gmail.com	1
3	Sumatera Utara	Ir. Reinhart L. Tobing	12	2019-02-18 02:27:05	2019-02-18 02:27:05	Jl. Dahlia Raya Perum. Taman Dahlia no.8 Helvetia Medan	0811470885	sipsumut@gmail.com	2
4	Kepulauan Riau	Novianto	14	2019-02-18 02:27:58	2019-02-18 02:27:58	Perum Kopkar PLN Blok D8 Batam Center Kota Batam	081276850777	sipkepri@gmail.com	3
5	Jambi	Chris Kartika Catarina	15	2019-02-18 02:28:39	2019-02-18 02:28:39	Perum Tanjung Permata Blok MD no. 04, Eka Jaya Jambi Selatan	085379380600	sipjambi4@gmail.com	4
6	Sumatera Selatan	Fajarnata, SH	16	2019-02-18 02:29:23	2019-02-18 02:29:23	Jl. RE Martadinata no.01 RT/RW : 01/01 Sei Buah 2 Ilir Palembang	081373377188	sipsumsel@gmail.com	5
7	Banten	Ir. Karmen Tarigan	36	2019-02-18 02:30:12	2019-02-18 02:30:12	Ruko Puri Naga Blok A No. 8 Kp. Melayu Timur, Teluk Naga(15510) Kab. Tangerang, Banten	081219197343	sipbanten@gmail.com	6
8	DKI Jakarta	Ir. Achmad Bachrulalam	31	2019-02-18 02:31:00	2019-02-18 02:31:00	Jl. Giro II Blok R 12 RT 002 RW 010 Pegangsaan Dua, Kelapa Gading, Jakarta Utara, 14250	08121001105	sipdki@gmail.com	7
9	Jawa Barat	Ir. Mangatas Tambunan, MT	32	2019-02-18 02:31:52	2019-02-18 02:31:52	Jl. Krajan II no. 24 RT/RW : 02/02 Desa Gintung Kerta Kec. Klari Kab. Karawang	081353644207	sipjabar@gmail.com	8
10	Jawa Tengah	Sari Isyana P, ST, MM	33	2019-02-18 02:33:38	2019-02-18 02:33:38	Jl. Jatiluhur No. 71 RT 04 RW 05 Kel. Ngesrep Kec. Banyumanik, Semarang	(024)7470119	sipnasional@gmail.com	9
11	DI Yogyakarta	Ir. JM Ginting, MM	34	2019-02-18 02:34:30	2019-02-18 02:34:30	Perum SDAD No. 24 Jongke RT/RW :09/024, Desa Sendangadi Kec. Melati, Kabupaten Sleman	081227559988	sipdiy@gmail.com	10
12	Jawa Timur	Ir. Edward Simorangkir	35	2019-02-18 02:35:27	2019-02-18 02:35:27	Ketintang Tengah 03.05 RT/RW : 002/010 Kel ketintang, Kec. Gayungan Surabaya	082142839218	sipjatim@gmail.com	11
13	Bali	Ida Bagus Anom	51	2019-02-18 02:40:33	2019-02-18 02:40:33	Komp. Dalung Permai Blok K II-I/127-128, Kuta Utara, Badung, Bali	081239327444	sipbali@gmail.com	12
14	Nusa Tenggara Barat	Yustinus Wuri	52	2019-02-18 02:41:16	2019-02-18 02:41:16	Jl. Industri No.14E, Ampenan-Mataram, Lombok-NTB	Jl. Industri No.14E, Ampenan-Mataram, Lombok-NTB	sipntb@gmail.com	13
15	Nusa Tenggara Timur	Tige Bangngu Windoe Kale	53	2019-02-18 02:41:54	2019-02-18 02:41:54	Jl. Samratulangi II No. 37, RT/RW : 038/ 017, Kelapa Lima, Kupang	0811383647	sipntt@gmail.com	14
16	Maluku	Julius Sipahelut	81	2019-02-18 02:42:44	2019-02-18 02:42:44	Jl.DR. Kayadoe RT/RW : 010/005 Kudamati, Nusaniwe Ambon	08124733940	sipmaluku@gmail.com	15
17	Maluku Utara	Nurjan Soleman, SH	82	2019-02-18 02:43:24	2019-02-18 02:43:24	Jl. Hasan Esa no. 40 RT/RW : 02/01 Kel. Toboko Ternate	082290644706	sipmalut@gmail.com	16
18	Kalimantan Timur	Ir. Harya Bagaswara Cahaya Widiyanto	64	2019-02-18 02:44:12	2019-02-18 02:44:12	Kantor Central Eropa 1 Blok AA 4 no. 12 BPN Baru,  Balikpapan	08542877383	sipkaltim@gmail.com	17
19	Kalimantan Selatan	Hidayat, SE	63	2019-02-18 02:45:29	2019-02-18 02:45:29	Jl. Sungai Andai Komp. Andai Jaya Persada Blok A1 no.05 Banjarmasin	085390885139	sipkalsel@gmail.com	18
20	Kalimantan Barat	M. Nugraha Aprianto	61	2019-02-18 02:46:13	2019-02-18 02:46:13	Jl. Tanjung Raya 2 Gg. Kurnia Jaya no. 4 A Pontianak Kalbar	081215855607	sipkalbar@gmail.com	19
21	Sulawesi Utara	Rochmadi	71	2019-02-18 02:46:56	2019-02-18 02:46:56	Paal 2 Lingkungan 1 no. 1 Kec. Paal 2 Kota Menado	081356152929	sipsulut@gmail.com	20
22	Sulawesi Tenggara	Achmad Fauzi Dalimunte	74	2019-02-18 02:47:59	2019-02-18 02:47:59	Jl. Haeba No.5, Kec.Wua-wua kota Kendari	081220891957	sipsultra@gmail.com	21
23	Sulawesi Tengah	Andhika Pandelaki Amate	72	2019-02-18 02:50:08	2019-02-18 02:50:08	Jl. Kimaja no 48 RT/RW : 001/009 Besusu Barat, Palu	08114507082	sipsulteng@gmail.com	22
24	Sulawesi Selatan	Ir. Chairil Anwar	73	2019-02-18 02:50:51	2019-02-18 02:50:51	Jl. Toddopuli X Perum. Green Villa Garden Blok A4 No. 2 Makassar	085342036503	sipsulsel@gmail.com	23
25	Papua & Papua Barat	Susmaryani	94	2019-02-18 02:51:48	2019-02-18 02:51:48	Jl. Baru Tembus Melati No. 61, Kotaraja Abepura, Jayapura	081344454485	sipppb@gmail.com	24
26	Kalimantan Tengah	ISTIKOMAH	62	2019-08-08 16:13:23	2019-08-09 10:25:48	DESA UMPANG RT.02 KEC. ARUT SELATAN KAB. KOTAWARINGIN BARAT	081528553432	sipkalteng@gmail.com	\N
30	Lampung	WIWIK WIRANTI	18	2019-08-09 10:27:19	2019-08-09 10:27:19	DUSUN OO5 RT.027 KAMPUNG ENDANG REJO, KEC. SEPUTIH KAB. LAMPUNG TENGAH	082377734737	siplampung@gmail.com	\N
31	Riau	MUHAMMAD FARHANSYAH MONDARI	14	2019-08-09 10:29:45	2019-08-09 10:29:45	JL. UTAMA NO.1A RT.04 RW.04 KEL. REJOSARI KEC. TENAYAN KOTA PEKANBARU	081270560283	sipriau@gmail.com	\N
32	Sumatera Barat	BP PONO	13	2019-08-09 10:31:42	2019-08-09 10:31:42	JL. KAMPUNG BARU RT.05 RW.04 KEL. SAWAHAN TIMUR KEC. PADANG TIMUR KOTA PADANG	081268242714	sipsumbar@gmail.com	\N
\.


--
-- Name: kantor_wilayah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('kantor_wilayah_id_seq', 34, true);


--
-- Data for Name: kanwil; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY kanwil (kantor_wilayah, general_manager, alamat, id) FROM stdin;
Nangro Aceh Darussalam	Ir. TwkMuntazar	Perum Tanjung Permata Blok MD no. 04, Eka Jaya Jambi Selatan/ 0853793806ipjambi4@gmail.com00/ s	\N
Sumatera Utara	Ir. Reinhart L. Tobing	Perum Tanjung Permata Blok MD no. 04, Eka Jaya Jambi Selatan/ 0853793806ipjambi4@gmail.com00/ s	\N
Kepulauan Riau	Novianto	Perum Tanjung Permata Blok MD no. 04, Eka Jaya Jambi Selatan/ 0853793806ipjambi4@gmail.com00/ s	\N
Jambi	Chris Kartika Catarina	Perum Tanjung Permata Blok MD no. 04, Eka Jaya Jambi Selatan/ 0853793806ipjambi4@gmail.com00/ s	\N
\.


--
-- Data for Name: kota; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY kota (id, kode_kota, kode_prov, kota, created_at, updated_at) FROM stdin;
3	1103	11	KABUPATEN ACEH SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
4	1104	11	KABUPATEN ACEH TENGGARA	2019-01-21 03:06:08	2019-01-21 03:06:08
5	1105	11	KABUPATEN ACEH TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
6	1106	11	KABUPATEN ACEH TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
8	1108	11	KABUPATEN ACEH BESAR	2019-01-21 03:06:08	2019-01-21 03:06:08
9	1109	11	KABUPATEN PIDIE	2019-01-21 03:06:08	2019-01-21 03:06:08
10	1110	11	KABUPATEN BIREUEN	2019-01-21 03:06:08	2019-01-21 03:06:08
12	1112	11	KABUPATEN ACEH BARAT DAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
13	1113	11	KABUPATEN GAYO LUES	2019-01-21 03:06:08	2019-01-21 03:06:08
14	1114	11	KABUPATEN ACEH TAMIANG	2019-01-21 03:06:08	2019-01-21 03:06:08
16	1116	11	KABUPATEN ACEH JAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
17	1117	11	KABUPATEN BENER MERIAH	2019-01-21 03:06:08	2019-01-21 03:06:08
18	1118	11	KABUPATEN PIDIE JAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
19	1171	11	KOTA BANDA ACEH	2019-01-21 03:06:08	2019-01-21 03:06:08
21	1173	11	KOTA LANGSA	2019-01-21 03:06:08	2019-01-21 03:06:08
22	1174	11	KOTA LHOKSEUMAWE	2019-01-21 03:06:08	2019-01-21 03:06:08
24	1201	12	KABUPATEN NIAS	2019-01-21 03:06:08	2019-01-21 03:06:08
25	1202	12	KABUPATEN MANDAILING NATAL	2019-01-21 03:06:08	2019-01-21 03:06:08
27	1204	12	KABUPATEN TAPANULI TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
28	1205	12	KABUPATEN TAPANULI UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
29	1206	12	KABUPATEN TOBA SAMOSIR	2019-01-21 03:06:08	2019-01-21 03:06:08
30	1207	12	KABUPATEN LABUHAN BATU	2019-01-21 03:06:08	2019-01-21 03:06:08
32	1209	12	KABUPATEN SIMALUNGUN	2019-01-21 03:06:08	2019-01-21 03:06:08
33	1210	12	KABUPATEN DAIRI	2019-01-21 03:06:08	2019-01-21 03:06:08
34	1211	12	KABUPATEN KARO	2019-01-21 03:06:08	2019-01-21 03:06:08
36	1213	12	KABUPATEN LANGKAT	2019-01-21 03:06:08	2019-01-21 03:06:08
37	1214	12	KABUPATEN NIAS SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
39	1216	12	KABUPATEN PAKPAK BHARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
40	1217	12	KABUPATEN SAMOSIR	2019-01-21 03:06:08	2019-01-21 03:06:08
41	1218	12	KABUPATEN SERDANG BEDAGAI	2019-01-21 03:06:08	2019-01-21 03:06:08
42	1219	12	KABUPATEN BATU BARA	2019-01-21 03:06:08	2019-01-21 03:06:08
44	1221	12	KABUPATEN PADANG LAWAS	2019-01-21 03:06:08	2019-01-21 03:06:08
45	1222	12	KABUPATEN LABUHAN BATU SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
46	1223	12	KABUPATEN LABUHAN BATU UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
47	1224	12	KABUPATEN NIAS UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
49	1271	12	KOTA SIBOLGA	2019-01-21 03:06:08	2019-01-21 03:06:08
50	1272	12	KOTA TANJUNG BALAI	2019-01-21 03:06:08	2019-01-21 03:06:08
51	1273	12	KOTA PEMATANG SIANTAR	2019-01-21 03:06:08	2019-01-21 03:06:08
53	1275	12	KOTA MEDAN	2019-01-21 03:06:08	2019-01-21 03:06:08
54	1276	12	KOTA BINJAI	2019-01-21 03:06:08	2019-01-21 03:06:08
55	1277	12	KOTA PADANGSIDIMPUAN	2019-01-21 03:06:08	2019-01-21 03:06:08
57	1301	13	KABUPATEN KEPULAUAN MENTAWAI	2019-01-21 03:06:08	2019-01-21 03:06:08
58	1302	13	KABUPATEN PESISIR SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
59	1303	13	KABUPATEN SOLOK	2019-01-21 03:06:08	2019-01-21 03:06:08
61	1305	13	KABUPATEN TANAH DATAR	2019-01-21 03:06:08	2019-01-21 03:06:08
62	1306	13	KABUPATEN PADANG PARIAMAN	2019-01-21 03:06:08	2019-01-21 03:06:08
63	1307	13	KABUPATEN AGAM	2019-01-21 03:06:08	2019-01-21 03:06:08
65	1309	13	KABUPATEN PASAMAN	2019-01-21 03:06:08	2019-01-21 03:06:08
66	1310	13	KABUPATEN SOLOK SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
67	1311	13	KABUPATEN DHARMASRAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
69	1371	13	KOTA PADANG	2019-01-21 03:06:08	2019-01-21 03:06:08
70	1372	13	KOTA SOLOK	2019-01-21 03:06:08	2019-01-21 03:06:08
71	1373	13	KOTA SAWAH LUNTO	2019-01-21 03:06:08	2019-01-21 03:06:08
73	1375	13	KOTA BUKITTINGGI	2019-01-21 03:06:08	2019-01-21 03:06:08
74	1376	13	KOTA PAYAKUMBUH	2019-01-21 03:06:08	2019-01-21 03:06:08
75	1377	13	KOTA PARIAMAN	2019-01-21 03:06:08	2019-01-21 03:06:08
77	1402	14	KABUPATEN INDRAGIRI HULU	2019-01-21 03:06:08	2019-01-21 03:06:08
78	1403	14	KABUPATEN INDRAGIRI HILIR	2019-01-21 03:06:08	2019-01-21 03:06:08
79	1404	14	KABUPATEN PELALAWAN	2019-01-21 03:06:08	2019-01-21 03:06:08
81	1406	14	KABUPATEN KAMPAR	2019-01-21 03:06:08	2019-01-21 03:06:08
82	1407	14	KABUPATEN ROKAN HULU	2019-01-21 03:06:08	2019-01-21 03:06:08
83	1408	14	KABUPATEN BENGKALIS	2019-01-21 03:06:08	2019-01-21 03:06:08
85	1410	14	KABUPATEN KEPULAUAN MERANTI	2019-01-21 03:06:08	2019-01-21 03:06:08
86	1471	14	KOTA PEKANBARU	2019-01-21 03:06:08	2019-01-21 03:06:08
87	1473	14	KOTA D U M A I	2019-01-21 03:06:08	2019-01-21 03:06:08
89	1502	15	KABUPATEN MERANGIN	2019-01-21 03:06:08	2019-01-21 03:06:08
90	1503	15	KABUPATEN SAROLANGUN	2019-01-21 03:06:08	2019-01-21 03:06:08
91	1504	15	KABUPATEN BATANG HARI	2019-01-21 03:06:08	2019-01-21 03:06:08
93	1506	15	KABUPATEN TANJUNG JABUNG TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
94	1507	15	KABUPATEN TANJUNG JABUNG BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
95	1508	15	KABUPATEN TEBO	2019-01-21 03:06:08	2019-01-21 03:06:08
97	1571	15	KOTA JAMBI	2019-01-21 03:06:08	2019-01-21 03:06:08
98	1572	15	KOTA SUNGAI PENUH	2019-01-21 03:06:08	2019-01-21 03:06:08
99	1601	16	KABUPATEN OGAN KOMERING ULU	2019-01-21 03:06:08	2019-01-21 03:06:08
101	1603	16	KABUPATEN MUARA ENIM	2019-01-21 03:06:08	2019-01-21 03:06:08
102	1604	16	KABUPATEN LAHAT	2019-01-21 03:06:08	2019-01-21 03:06:08
103	1605	16	KABUPATEN MUSI RAWAS	2019-01-21 03:06:08	2019-01-21 03:06:08
105	1607	16	KABUPATEN BANYU ASIN	2019-01-21 03:06:08	2019-01-21 03:06:08
106	1608	16	KABUPATEN OGAN KOMERING ULU SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
107	1609	16	KABUPATEN OGAN KOMERING ULU TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
108	1610	16	KABUPATEN OGAN ILIR	2019-01-21 03:06:08	2019-01-21 03:06:08
110	1612	16	KABUPATEN PENUKAL ABAB LEMATANG ILIR	2019-01-21 03:06:08	2019-01-21 03:06:08
111	1613	16	KABUPATEN MUSI RAWAS UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
112	1671	16	KOTA PALEMBANG	2019-01-21 03:06:08	2019-01-21 03:06:08
114	1673	16	KOTA PAGAR ALAM	2019-01-21 03:06:08	2019-01-21 03:06:08
115	1674	16	KOTA LUBUKLINGGAU	2019-01-21 03:06:08	2019-01-21 03:06:08
117	1702	17	KABUPATEN REJANG LEBONG	2019-01-21 03:06:08	2019-01-21 03:06:08
118	1703	17	KABUPATEN BENGKULU UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
119	1704	17	KABUPATEN KAUR	2019-01-21 03:06:08	2019-01-21 03:06:08
120	1705	17	KABUPATEN SELUMA	2019-01-21 03:06:08	2019-01-21 03:06:08
122	1707	17	KABUPATEN LEBONG	2019-01-21 03:06:08	2019-01-21 03:06:08
123	1708	17	KABUPATEN KEPAHIANG	2019-01-21 03:06:08	2019-01-21 03:06:08
125	1771	17	KOTA BENGKULU	2019-01-21 03:06:08	2019-01-21 03:06:08
126	1801	18	KABUPATEN LAMPUNG BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
129	1804	18	KABUPATEN LAMPUNG TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
130	1805	18	KABUPATEN LAMPUNG TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
131	1806	18	KABUPATEN LAMPUNG UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
133	1808	18	KABUPATEN TULANGBAWANG	2019-01-21 03:06:08	2019-01-21 03:06:08
134	1809	18	KABUPATEN PESAWARAN	2019-01-21 03:06:08	2019-01-21 03:06:08
135	1810	18	KABUPATEN PRINGSEWU	2019-01-21 03:06:08	2019-01-21 03:06:08
137	1812	18	KABUPATEN TULANG BAWANG BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
138	1813	18	KABUPATEN PESISIR BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
139	1871	18	KOTA BANDAR LAMPUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
141	1901	19	KABUPATEN BANGKA	2019-01-21 03:06:08	2019-01-21 03:06:08
142	1902	19	KABUPATEN BELITUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
144	1904	19	KABUPATEN BANGKA TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
145	1905	19	KABUPATEN BANGKA SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
146	1906	19	KABUPATEN BELITUNG TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
147	1971	19	KOTA PANGKAL PINANG	2019-01-21 03:06:08	2019-01-21 03:06:08
149	2102	21	KABUPATEN BINTAN	2019-01-21 03:06:08	2019-01-21 03:06:08
150	2103	21	KABUPATEN NATUNA	2019-01-21 03:06:08	2019-01-21 03:06:08
152	2105	21	KABUPATEN KEPULAUAN ANAMBAS	2019-01-21 03:06:08	2019-01-21 03:06:08
153	2171	21	KOTA B A T A M	2019-01-21 03:06:08	2019-01-21 03:06:08
154	2172	21	KOTA TANJUNG PINANG	2019-01-21 03:06:08	2019-01-21 03:06:08
156	3171	31	KOTA JAKARTA SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
157	3172	31	KOTA JAKARTA TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
158	3173	31	KOTA JAKARTA PUSAT	2019-01-21 03:06:08	2019-01-21 03:06:08
160	3175	31	KOTA JAKARTA UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
161	3201	32	KABUPATEN BOGOR	2019-01-21 03:06:08	2019-01-21 03:06:08
162	3202	32	KABUPATEN SUKABUMI	2019-01-21 03:06:08	2019-01-21 03:06:08
164	3204	32	KABUPATEN BANDUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
165	3205	32	KABUPATEN GARUT	2019-01-21 03:06:08	2019-01-21 03:06:08
166	3206	32	KABUPATEN TASIKMALAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
168	3208	32	KABUPATEN KUNINGAN	2019-01-21 03:06:08	2019-01-21 03:06:08
169	3209	32	KABUPATEN CIREBON	2019-01-21 03:06:08	2019-01-21 03:06:08
171	3211	32	KABUPATEN SUMEDANG	2019-01-21 03:06:08	2019-01-21 03:06:08
172	3212	32	KABUPATEN INDRAMAYU	2019-01-21 03:06:08	2019-01-21 03:06:08
173	3213	32	KABUPATEN SUBANG	2019-01-21 03:06:08	2019-01-21 03:06:08
174	3214	32	KABUPATEN PURWAKARTA	2019-01-21 03:06:08	2019-01-21 03:06:08
176	3216	32	KABUPATEN BEKASI	2019-01-21 03:06:08	2019-01-21 03:06:08
177	3217	32	KABUPATEN BANDUNG BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
179	3271	32	KOTA BOGOR	2019-01-21 03:06:08	2019-01-21 03:06:08
180	3272	32	KOTA SUKABUMI	2019-01-21 03:06:08	2019-01-21 03:06:08
181	3273	32	KOTA BANDUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
182	3274	32	KOTA CIREBON	2019-01-21 03:06:08	2019-01-21 03:06:08
184	3276	32	KOTA DEPOK	2019-01-21 03:06:08	2019-01-21 03:06:08
185	3277	32	KOTA CIMAHI	2019-01-21 03:06:08	2019-01-21 03:06:08
187	3279	32	KOTA BANJAR	2019-01-21 03:06:08	2019-01-21 03:06:08
188	3301	33	KABUPATEN CILACAP	2019-01-21 03:06:08	2019-01-21 03:06:08
189	3302	33	KABUPATEN BANYUMAS	2019-01-21 03:06:08	2019-01-21 03:06:08
191	3304	33	KABUPATEN BANJARNEGARA	2019-01-21 03:06:08	2019-01-21 03:06:08
192	3305	33	KABUPATEN KEBUMEN	2019-01-21 03:06:08	2019-01-21 03:06:08
193	3306	33	KABUPATEN PURWOREJO	2019-01-21 03:06:08	2019-01-21 03:06:08
195	3308	33	KABUPATEN MAGELANG	2019-01-21 03:06:08	2019-01-21 03:06:08
196	3309	33	KABUPATEN BOYOLALI	2019-01-21 03:06:08	2019-01-21 03:06:08
197	3310	33	KABUPATEN KLATEN	2019-01-21 03:06:08	2019-01-21 03:06:08
199	3312	33	KABUPATEN WONOGIRI	2019-01-21 03:06:08	2019-01-21 03:06:08
200	3313	33	KABUPATEN KARANGANYAR	2019-01-21 03:06:08	2019-01-21 03:06:08
201	3314	33	KABUPATEN SRAGEN	2019-01-21 03:06:08	2019-01-21 03:06:08
203	3316	33	KABUPATEN BLORA	2019-01-21 03:06:08	2019-01-21 03:06:08
204	3317	33	KABUPATEN REMBANG	2019-01-21 03:06:08	2019-01-21 03:06:08
205	3318	33	KABUPATEN PATI	2019-01-21 03:06:08	2019-01-21 03:06:08
207	3320	33	KABUPATEN JEPARA	2019-01-21 03:06:08	2019-01-21 03:06:08
208	3321	33	KABUPATEN DEMAK	2019-01-21 03:06:08	2019-01-21 03:06:08
209	3322	33	KABUPATEN SEMARANG	2019-01-21 03:06:08	2019-01-21 03:06:08
211	3324	33	KABUPATEN KENDAL	2019-01-21 03:06:08	2019-01-21 03:06:08
212	3325	33	KABUPATEN BATANG	2019-01-21 03:06:08	2019-01-21 03:06:08
214	3327	33	KABUPATEN PEMALANG	2019-01-21 03:06:08	2019-01-21 03:06:08
215	3328	33	KABUPATEN TEGAL	2019-01-21 03:06:08	2019-01-21 03:06:08
216	3329	33	KABUPATEN BREBES	2019-01-21 03:06:08	2019-01-21 03:06:08
217	3371	33	KOTA MAGELANG	2019-01-21 03:06:08	2019-01-21 03:06:08
219	3373	33	KOTA SALATIGA	2019-01-21 03:06:08	2019-01-21 03:06:08
220	3374	33	KOTA SEMARANG	2019-01-21 03:06:08	2019-01-21 03:06:08
221	3375	33	KOTA PEKALONGAN	2019-01-21 03:06:08	2019-01-21 03:06:08
223	3401	34	KABUPATEN KULON PROGO	2019-01-21 03:06:08	2019-01-21 03:06:08
224	3402	34	KABUPATEN BANTUL	2019-01-21 03:06:08	2019-01-21 03:06:08
226	3404	34	KABUPATEN SLEMAN	2019-01-21 03:06:08	2019-01-21 03:06:08
227	3471	34	KOTA YOGYAKARTA	2019-01-21 03:06:08	2019-01-21 03:06:08
228	3501	35	KABUPATEN PACITAN	2019-01-21 03:06:08	2019-01-21 03:06:08
230	3503	35	KABUPATEN TRENGGALEK	2019-01-21 03:06:08	2019-01-21 03:06:08
231	3504	35	KABUPATEN TULUNGAGUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
232	3505	35	KABUPATEN BLITAR	2019-01-21 03:06:08	2019-01-21 03:06:08
234	3507	35	KABUPATEN MALANG	2019-01-21 03:06:08	2019-01-21 03:06:08
235	3508	35	KABUPATEN LUMAJANG	2019-01-21 03:06:08	2019-01-21 03:06:08
236	3509	35	KABUPATEN JEMBER	2019-01-21 03:06:08	2019-01-21 03:06:08
238	3511	35	KABUPATEN BONDOWOSO	2019-01-21 03:06:08	2019-01-21 03:06:08
239	3512	35	KABUPATEN SITUBONDO	2019-01-21 03:06:08	2019-01-21 03:06:08
240	3513	35	KABUPATEN PROBOLINGGO	2019-01-21 03:06:08	2019-01-21 03:06:08
242	3515	35	KABUPATEN SIDOARJO	2019-01-21 03:06:08	2019-01-21 03:06:08
243	3516	35	KABUPATEN MOJOKERTO	2019-01-21 03:06:08	2019-01-21 03:06:08
244	3517	35	KABUPATEN JOMBANG	2019-01-21 03:06:08	2019-01-21 03:06:08
246	3519	35	KABUPATEN MADIUN	2019-01-21 03:06:08	2019-01-21 03:06:08
247	3520	35	KABUPATEN MAGETAN	2019-01-21 03:06:08	2019-01-21 03:06:08
248	3521	35	KABUPATEN NGAWI	2019-01-21 03:06:08	2019-01-21 03:06:08
250	3523	35	KABUPATEN TUBAN	2019-01-21 03:06:08	2019-01-21 03:06:08
251	3524	35	KABUPATEN LAMONGAN	2019-01-21 03:06:08	2019-01-21 03:06:08
252	3525	35	KABUPATEN GRESIK	2019-01-21 03:06:08	2019-01-21 03:06:08
254	3527	35	KABUPATEN SAMPANG	2019-01-21 03:06:08	2019-01-21 03:06:08
255	3528	35	KABUPATEN PAMEKASAN	2019-01-21 03:06:08	2019-01-21 03:06:08
256	3529	35	KABUPATEN SUMENEP	2019-01-21 03:06:08	2019-01-21 03:06:08
258	3572	35	KOTA BLITAR	2019-01-21 03:06:08	2019-01-21 03:06:08
259	3573	35	KOTA MALANG	2019-01-21 03:06:08	2019-01-21 03:06:08
261	3575	35	KOTA PASURUAN	2019-01-21 03:06:08	2019-01-21 03:06:08
262	3576	35	KOTA MOJOKERTO	2019-01-21 03:06:08	2019-01-21 03:06:08
264	3578	35	KOTA SURABAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
265	3579	35	KOTA BATU	2019-01-21 03:06:08	2019-01-21 03:06:08
267	3602	36	KABUPATEN LEBAK	2019-01-21 03:06:08	2019-01-21 03:06:08
268	3603	36	KABUPATEN TANGERANG	2019-01-21 03:06:08	2019-01-21 03:06:08
269	3604	36	KABUPATEN SERANG	2019-01-21 03:06:08	2019-01-21 03:06:08
271	3672	36	KOTA CILEGON	2019-01-21 03:06:08	2019-01-21 03:06:08
272	3673	36	KOTA SERANG	2019-01-21 03:06:08	2019-01-21 03:06:08
273	3674	36	KOTA TANGERANG SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
275	5102	51	KABUPATEN TABANAN	2019-01-21 03:06:08	2019-01-21 03:06:08
276	5103	51	KABUPATEN BADUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
277	5104	51	KABUPATEN GIANYAR	2019-01-21 03:06:08	2019-01-21 03:06:08
279	5106	51	KABUPATEN BANGLI	2019-01-21 03:06:08	2019-01-21 03:06:08
280	5107	51	KABUPATEN KARANG ASEM	2019-01-21 03:06:08	2019-01-21 03:06:08
281	5108	51	KABUPATEN BULELENG	2019-01-21 03:06:08	2019-01-21 03:06:08
283	5201	52	KABUPATEN LOMBOK BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
284	5202	52	KABUPATEN LOMBOK TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
286	5204	52	KABUPATEN SUMBAWA	2019-01-21 03:06:08	2019-01-21 03:06:08
287	5205	52	KABUPATEN DOMPU	2019-01-21 03:06:08	2019-01-21 03:06:08
288	5206	52	KABUPATEN BIMA	2019-01-21 03:06:08	2019-01-21 03:06:08
290	5208	52	KABUPATEN LOMBOK UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
291	5271	52	KOTA MATARAM	2019-01-21 03:06:08	2019-01-21 03:06:08
292	5272	52	KOTA BIMA	2019-01-21 03:06:08	2019-01-21 03:06:08
294	5302	53	KABUPATEN SUMBA TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
295	5303	53	KABUPATEN KUPANG	2019-01-21 03:06:08	2019-01-21 03:06:08
296	5304	53	KABUPATEN TIMOR TENGAH SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
298	5306	53	KABUPATEN BELU	2019-01-21 03:06:08	2019-01-21 03:06:08
299	5307	53	KABUPATEN ALOR	2019-01-21 03:06:08	2019-01-21 03:06:08
300	5308	53	KABUPATEN LEMBATA	2019-01-21 03:06:08	2019-01-21 03:06:08
301	5309	53	KABUPATEN FLORES TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
302	5310	53	KABUPATEN SIKKA	2019-01-21 03:06:08	2019-01-21 03:06:08
304	5312	53	KABUPATEN NGADA	2019-01-21 03:06:08	2019-01-21 03:06:08
305	5313	53	KABUPATEN MANGGARAI	2019-01-21 03:06:08	2019-01-21 03:06:08
307	5315	53	KABUPATEN MANGGARAI BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
308	5316	53	KABUPATEN SUMBA TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
309	5317	53	KABUPATEN SUMBA BARAT DAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
311	5319	53	KABUPATEN MANGGARAI TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
312	5320	53	KABUPATEN SABU RAIJUA	2019-01-21 03:06:08	2019-01-21 03:06:08
313	5321	53	KABUPATEN MALAKA	2019-01-21 03:06:08	2019-01-21 03:06:08
315	6101	61	KABUPATEN SAMBAS	2019-01-21 03:06:08	2019-01-21 03:06:08
316	6102	61	KABUPATEN BENGKAYANG	2019-01-21 03:06:08	2019-01-21 03:06:08
318	6104	61	KABUPATEN MEMPAWAH	2019-01-21 03:06:08	2019-01-21 03:06:08
319	6105	61	KABUPATEN SANGGAU	2019-01-21 03:06:08	2019-01-21 03:06:08
320	6106	61	KABUPATEN KETAPANG	2019-01-21 03:06:08	2019-01-21 03:06:08
322	6108	61	KABUPATEN KAPUAS HULU	2019-01-21 03:06:08	2019-01-21 03:06:08
323	6109	61	KABUPATEN SEKADAU	2019-01-21 03:06:08	2019-01-21 03:06:08
324	6110	61	KABUPATEN MELAWI	2019-01-21 03:06:08	2019-01-21 03:06:08
326	6112	61	KABUPATEN KUBU RAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
327	6171	61	KOTA PONTIANAK	2019-01-21 03:06:08	2019-01-21 03:06:08
328	6172	61	KOTA SINGKAWANG	2019-01-21 03:06:08	2019-01-21 03:06:08
330	6202	62	KABUPATEN KOTAWARINGIN TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
331	6203	62	KABUPATEN KAPUAS	2019-01-21 03:06:08	2019-01-21 03:06:08
332	6204	62	KABUPATEN BARITO SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
334	6206	62	KABUPATEN SUKAMARA	2019-01-21 03:06:08	2019-01-21 03:06:08
335	6207	62	KABUPATEN LAMANDAU	2019-01-21 03:06:08	2019-01-21 03:06:08
336	6208	62	KABUPATEN SERUYAN	2019-01-21 03:06:08	2019-01-21 03:06:08
337	6209	62	KABUPATEN KATINGAN	2019-01-21 03:06:08	2019-01-21 03:06:08
339	6211	62	KABUPATEN GUNUNG MAS	2019-01-21 03:06:08	2019-01-21 03:06:08
340	6212	62	KABUPATEN BARITO TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
341	6213	62	KABUPATEN MURUNG RAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
343	6301	63	KABUPATEN TANAH LAUT	2019-01-21 03:06:08	2019-01-21 03:06:08
344	6302	63	KABUPATEN KOTA BARU	2019-01-21 03:06:08	2019-01-21 03:06:08
345	6303	63	KABUPATEN BANJAR	2019-01-21 03:06:08	2019-01-21 03:06:08
347	6305	63	KABUPATEN TAPIN	2019-01-21 03:06:08	2019-01-21 03:06:08
348	6306	63	KABUPATEN HULU SUNGAI SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
350	6308	63	KABUPATEN HULU SUNGAI UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
351	6309	63	KABUPATEN TABALONG	2019-01-21 03:06:08	2019-01-21 03:06:08
352	6310	63	KABUPATEN TANAH BUMBU	2019-01-21 03:06:08	2019-01-21 03:06:08
353	6311	63	KABUPATEN BALANGAN	2019-01-21 03:06:08	2019-01-21 03:06:08
354	6371	63	KOTA BANJARMASIN	2019-01-21 03:06:08	2019-01-21 03:06:08
356	6401	64	KABUPATEN PASER	2019-01-21 03:06:08	2019-01-21 03:06:08
357	6402	64	KABUPATEN KUTAI BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
359	6404	64	KABUPATEN KUTAI TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
360	6405	64	KABUPATEN BERAU	2019-01-21 03:06:08	2019-01-21 03:06:08
361	6409	64	KABUPATEN PENAJAM PASER UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
363	6471	64	KOTA BALIKPAPAN	2019-01-21 03:06:08	2019-01-21 03:06:08
364	6472	64	KOTA SAMARINDA	2019-01-21 03:06:08	2019-01-21 03:06:08
365	6474	64	KOTA BONTANG	2019-01-21 03:06:08	2019-01-21 03:06:08
366	6501	65	KABUPATEN MALINAU	2019-01-21 03:06:08	2019-01-21 03:06:08
368	6503	65	KABUPATEN TANA TIDUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
369	6504	65	KABUPATEN NUNUKAN	2019-01-21 03:06:08	2019-01-21 03:06:08
370	6571	65	KOTA TARAKAN	2019-01-21 03:06:08	2019-01-21 03:06:08
372	7102	71	KABUPATEN MINAHASA	2019-01-21 03:06:08	2019-01-21 03:06:08
373	7103	71	KABUPATEN KEPULAUAN SANGIHE	2019-01-21 03:06:08	2019-01-21 03:06:08
374	7104	71	KABUPATEN KEPULAUAN TALAUD	2019-01-21 03:06:08	2019-01-21 03:06:08
376	7106	71	KABUPATEN MINAHASA UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
377	7107	71	KABUPATEN BOLAANG MONGONDOW UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
379	7109	71	KABUPATEN MINAHASA TENGGARA	2019-01-21 03:06:08	2019-01-21 03:06:08
380	7110	71	KABUPATEN BOLAANG MONGONDOW SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
381	7111	71	KABUPATEN BOLAANG MONGONDOW TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
382	7171	71	KOTA MANADO	2019-01-21 03:06:08	2019-01-21 03:06:08
383	7172	71	KOTA BITUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
385	7174	71	KOTA KOTAMOBAGU	2019-01-21 03:06:08	2019-01-21 03:06:08
386	7201	72	KABUPATEN BANGGAI KEPULAUAN	2019-01-21 03:06:08	2019-01-21 03:06:08
387	7202	72	KABUPATEN BANGGAI	2019-01-21 03:06:08	2019-01-21 03:06:08
1	1101	11	KABUPATEN SIMEULUE	2019-01-21 03:06:08	2019-01-21 03:06:08
390	7205	72	KABUPATEN DONGGALA	2019-01-21 03:06:08	2019-01-21 03:06:08
391	7206	72	KABUPATEN TOLI-TOLI	2019-01-21 03:06:08	2019-01-21 03:06:08
392	7207	72	KABUPATEN BUOL	2019-01-21 03:06:08	2019-01-21 03:06:08
394	7209	72	KABUPATEN TOJO UNA-UNA	2019-01-21 03:06:08	2019-01-21 03:06:08
395	7210	72	KABUPATEN SIGI	2019-01-21 03:06:08	2019-01-21 03:06:08
396	7211	72	KABUPATEN BANGGAI LAUT	2019-01-21 03:06:08	2019-01-21 03:06:08
398	7271	72	KOTA PALU	2019-01-21 03:06:08	2019-01-21 03:06:08
399	7301	73	KABUPATEN KEPULAUAN SELAYAR	2019-01-21 03:06:08	2019-01-21 03:06:08
400	7302	73	KABUPATEN BULUKUMBA	2019-01-21 03:06:08	2019-01-21 03:06:08
401	7303	73	KABUPATEN BANTAENG	2019-01-21 03:06:08	2019-01-21 03:06:08
403	7305	73	KABUPATEN TAKALAR	2019-01-21 03:06:08	2019-01-21 03:06:08
404	7306	73	KABUPATEN GOWA	2019-01-21 03:06:08	2019-01-21 03:06:08
406	7308	73	KABUPATEN MAROS	2019-01-21 03:06:08	2019-01-21 03:06:08
407	7309	73	KABUPATEN PANGKAJENE DAN KEPULAUAN	2019-01-21 03:06:08	2019-01-21 03:06:08
408	7310	73	KABUPATEN BARRU	2019-01-21 03:06:08	2019-01-21 03:06:08
410	7312	73	KABUPATEN SOPPENG	2019-01-21 03:06:08	2019-01-21 03:06:08
411	7313	73	KABUPATEN WAJO	2019-01-21 03:06:08	2019-01-21 03:06:08
412	7314	73	KABUPATEN SIDENRENG RAPPANG	2019-01-21 03:06:08	2019-01-21 03:06:08
414	7316	73	KABUPATEN ENREKANG	2019-01-21 03:06:08	2019-01-21 03:06:08
415	7317	73	KABUPATEN LUWU	2019-01-21 03:06:08	2019-01-21 03:06:08
417	7322	73	KABUPATEN LUWU UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
418	7325	73	KABUPATEN LUWU TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
419	7326	73	KABUPATEN TORAJA UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
420	7371	73	KOTA MAKASSAR	2019-01-21 03:06:08	2019-01-21 03:06:08
422	7373	73	KOTA PALOPO	2019-01-21 03:06:08	2019-01-21 03:06:08
423	7401	74	KABUPATEN BUTON	2019-01-21 03:06:08	2019-01-21 03:06:08
424	7402	74	KABUPATEN MUNA	2019-01-21 03:06:08	2019-01-21 03:06:08
426	7404	74	KABUPATEN KOLAKA	2019-01-21 03:06:08	2019-01-21 03:06:08
427	7405	74	KABUPATEN KONAWE SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
428	7406	74	KABUPATEN BOMBANA	2019-01-21 03:06:08	2019-01-21 03:06:08
430	7408	74	KABUPATEN KOLAKA UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
431	7409	74	KABUPATEN BUTON UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
433	7411	74	KABUPATEN KOLAKA TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
434	7412	74	KABUPATEN KONAWE KEPULAUAN	2019-01-21 03:06:08	2019-01-21 03:06:08
435	7413	74	KABUPATEN MUNA BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
437	7415	74	KABUPATEN BUTON SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
438	7471	74	KOTA KENDARI	2019-01-21 03:06:08	2019-01-21 03:06:08
439	7472	74	KOTA BAUBAU	2019-01-21 03:06:08	2019-01-21 03:06:08
440	7501	75	KABUPATEN BOALEMO	2019-01-21 03:06:08	2019-01-21 03:06:08
442	7503	75	KABUPATEN POHUWATO	2019-01-21 03:06:08	2019-01-21 03:06:08
443	7504	75	KABUPATEN BONE BOLANGO	2019-01-21 03:06:08	2019-01-21 03:06:08
445	7571	75	KOTA GORONTALO	2019-01-21 03:06:08	2019-01-21 03:06:08
446	7601	76	KABUPATEN MAJENE	2019-01-21 03:06:08	2019-01-21 03:06:08
447	7602	76	KABUPATEN POLEWALI MANDAR	2019-01-21 03:06:08	2019-01-21 03:06:08
448	7603	76	KABUPATEN MAMASA	2019-01-21 03:06:08	2019-01-21 03:06:08
450	7605	76	KABUPATEN MAMUJU UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
451	7606	76	KABUPATEN MAMUJU TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
453	8102	81	KABUPATEN MALUKU TENGGARA	2019-01-21 03:06:08	2019-01-21 03:06:08
454	8103	81	KABUPATEN MALUKU TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
455	8104	81	KABUPATEN BURU	2019-01-21 03:06:08	2019-01-21 03:06:08
456	8105	81	KABUPATEN KEPULAUAN ARU	2019-01-21 03:06:08	2019-01-21 03:06:08
458	8107	81	KABUPATEN SERAM BAGIAN TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
459	8108	81	KABUPATEN MALUKU BARAT DAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
460	8109	81	KABUPATEN BURU SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
461	8171	81	KOTA AMBON	2019-01-21 03:06:08	2019-01-21 03:06:08
463	8201	82	KABUPATEN HALMAHERA BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
464	8202	82	KABUPATEN HALMAHERA TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
466	8204	82	KABUPATEN HALMAHERA SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
467	8205	82	KABUPATEN HALMAHERA UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
468	8206	82	KABUPATEN HALMAHERA TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
470	8208	82	KABUPATEN PULAU TALIABU	2019-01-21 03:06:08	2019-01-21 03:06:08
471	8271	82	KOTA TERNATE	2019-01-21 03:06:08	2019-01-21 03:06:08
472	8272	82	KOTA TIDORE KEPULAUAN	2019-01-21 03:06:08	2019-01-21 03:06:08
473	9101	91	KABUPATEN FAKFAK	2019-01-21 03:06:08	2019-01-21 03:06:08
475	9103	91	KABUPATEN TELUK WONDAMA	2019-01-21 03:06:08	2019-01-21 03:06:08
476	9104	91	KABUPATEN TELUK BINTUNI	2019-01-21 03:06:08	2019-01-21 03:06:08
478	9106	91	KABUPATEN SORONG SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
479	9107	91	KABUPATEN SORONG	2019-01-21 03:06:08	2019-01-21 03:06:08
480	9108	91	KABUPATEN RAJA AMPAT	2019-01-21 03:06:08	2019-01-21 03:06:08
482	9110	91	KABUPATEN MAYBRAT	2019-01-21 03:06:08	2019-01-21 03:06:08
483	9111	91	KABUPATEN MANOKWARI SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
484	9112	91	KABUPATEN PEGUNUNGAN ARFAK	2019-01-21 03:06:08	2019-01-21 03:06:08
486	9401	94	KABUPATEN MERAUKE	2019-01-21 03:06:08	2019-01-21 03:06:08
487	9402	94	KABUPATEN JAYAWIJAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
489	9404	94	KABUPATEN NABIRE	2019-01-21 03:06:08	2019-01-21 03:06:08
490	9408	94	KABUPATEN KEPULAUAN YAPEN	2019-01-21 03:06:08	2019-01-21 03:06:08
491	9409	94	KABUPATEN BIAK NUMFOR	2019-01-21 03:06:08	2019-01-21 03:06:08
493	9411	94	KABUPATEN PUNCAK JAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
494	9412	94	KABUPATEN MIMIKA	2019-01-21 03:06:08	2019-01-21 03:06:08
495	9413	94	KABUPATEN BOVEN DIGOEL	2019-01-21 03:06:08	2019-01-21 03:06:08
497	9415	94	KABUPATEN ASMAT	2019-01-21 03:06:08	2019-01-21 03:06:08
498	9416	94	KABUPATEN YAHUKIMO	2019-01-21 03:06:08	2019-01-21 03:06:08
500	9418	94	KABUPATEN TOLIKARA	2019-01-21 03:06:08	2019-01-21 03:06:08
501	9419	94	KABUPATEN SARMI	2019-01-21 03:06:08	2019-01-21 03:06:08
502	9420	94	KABUPATEN KEEROM	2019-01-21 03:06:08	2019-01-21 03:06:08
503	9426	94	KABUPATEN WAROPEN	2019-01-21 03:06:08	2019-01-21 03:06:08
505	9428	94	KABUPATEN MAMBERAMO RAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
506	9429	94	KABUPATEN NDUGA	2019-01-21 03:06:08	2019-01-21 03:06:08
507	9430	94	KABUPATEN LANNY JAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
509	9432	94	KABUPATEN YALIMO	2019-01-21 03:06:08	2019-01-21 03:06:08
510	9433	94	KABUPATEN PUNCAK	2019-01-21 03:06:08	2019-01-21 03:06:08
511	9434	94	KABUPATEN DOGIYAI	2019-01-21 03:06:08	2019-01-21 03:06:08
513	9436	94	KABUPATEN DEIYAI	2019-01-21 03:06:08	2019-01-21 03:06:08
514	9471	94	KOTA JAYAPURA	2019-01-21 03:06:08	2019-01-21 03:06:08
2	1102	11	KABUPATEN ACEH SINGKIL	2019-01-21 03:06:08	2019-01-21 03:06:08
7	1107	11	KABUPATEN ACEH BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
11	1111	11	KABUPATEN ACEH UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
15	1115	11	KABUPATEN NAGAN RAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
20	1172	11	KOTA SABANG	2019-01-21 03:06:08	2019-01-21 03:06:08
23	1175	11	KOTA SUBULUSSALAM	2019-01-21 03:06:08	2019-01-21 03:06:08
26	1203	12	KABUPATEN TAPANULI SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
31	1208	12	KABUPATEN ASAHAN	2019-01-21 03:06:08	2019-01-21 03:06:08
35	1212	12	KABUPATEN DELI SERDANG	2019-01-21 03:06:08	2019-01-21 03:06:08
38	1215	12	KABUPATEN HUMBANG HASUNDUTAN	2019-01-21 03:06:08	2019-01-21 03:06:08
43	1220	12	KABUPATEN PADANG LAWAS UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
48	1225	12	KABUPATEN NIAS BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
52	1274	12	KOTA TEBING TINGGI	2019-01-21 03:06:08	2019-01-21 03:06:08
56	1278	12	KOTA GUNUNGSITOLI	2019-01-21 03:06:08	2019-01-21 03:06:08
60	1304	13	KABUPATEN SIJUNJUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
64	1308	13	KABUPATEN LIMA PULUH KOTA	2019-01-21 03:06:08	2019-01-21 03:06:08
68	1312	13	KABUPATEN PASAMAN BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
72	1374	13	KOTA PADANG PANJANG	2019-01-21 03:06:08	2019-01-21 03:06:08
76	1401	14	KABUPATEN KUANTAN SINGINGI	2019-01-21 03:06:08	2019-01-21 03:06:08
80	1405	14	KABUPATEN S I A K	2019-01-21 03:06:08	2019-01-21 03:06:08
84	1409	14	KABUPATEN ROKAN HILIR	2019-01-21 03:06:08	2019-01-21 03:06:08
88	1501	15	KABUPATEN KERINCI	2019-01-21 03:06:08	2019-01-21 03:06:08
92	1505	15	KABUPATEN MUARO JAMBI	2019-01-21 03:06:08	2019-01-21 03:06:08
96	1509	15	KABUPATEN BUNGO	2019-01-21 03:06:08	2019-01-21 03:06:08
100	1602	16	KABUPATEN OGAN KOMERING ILIR	2019-01-21 03:06:08	2019-01-21 03:06:08
104	1606	16	KABUPATEN MUSI BANYUASIN	2019-01-21 03:06:08	2019-01-21 03:06:08
109	1611	16	KABUPATEN EMPAT LAWANG	2019-01-21 03:06:08	2019-01-21 03:06:08
113	1672	16	KOTA PRABUMULIH	2019-01-21 03:06:08	2019-01-21 03:06:08
116	1701	17	KABUPATEN BENGKULU SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
121	1706	17	KABUPATEN MUKOMUKO	2019-01-21 03:06:08	2019-01-21 03:06:08
124	1709	17	KABUPATEN BENGKULU TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
127	1802	18	KABUPATEN TANGGAMUS	2019-01-21 03:06:08	2019-01-21 03:06:08
128	1803	18	KABUPATEN LAMPUNG SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
132	1807	18	KABUPATEN WAY KANAN	2019-01-21 03:06:08	2019-01-21 03:06:08
136	1811	18	KABUPATEN MESUJI	2019-01-21 03:06:08	2019-01-21 03:06:08
140	1872	18	KOTA METRO	2019-01-21 03:06:08	2019-01-21 03:06:08
143	1903	19	KABUPATEN BANGKA BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
148	2101	21	KABUPATEN KARIMUN	2019-01-21 03:06:08	2019-01-21 03:06:08
151	2104	21	KABUPATEN LINGGA	2019-01-21 03:06:08	2019-01-21 03:06:08
155	3101	31	KABUPATEN KEPULAUAN SERIBU	2019-01-21 03:06:08	2019-01-21 03:06:08
159	3174	31	KOTA JAKARTA BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
163	3203	32	KABUPATEN CIANJUR	2019-01-21 03:06:08	2019-01-21 03:06:08
167	3207	32	KABUPATEN CIAMIS	2019-01-21 03:06:08	2019-01-21 03:06:08
170	3210	32	KABUPATEN MAJALENGKA	2019-01-21 03:06:08	2019-01-21 03:06:08
175	3215	32	KABUPATEN KARAWANG	2019-01-21 03:06:08	2019-01-21 03:06:08
178	3218	32	KABUPATEN PANGANDARAN	2019-01-21 03:06:08	2019-01-21 03:06:08
183	3275	32	KOTA BEKASI	2019-01-21 03:06:08	2019-01-21 03:06:08
186	3278	32	KOTA TASIKMALAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
190	3303	33	KABUPATEN PURBALINGGA	2019-01-21 03:06:08	2019-01-21 03:06:08
194	3307	33	KABUPATEN WONOSOBO	2019-01-21 03:06:08	2019-01-21 03:06:08
198	3311	33	KABUPATEN SUKOHARJO	2019-01-21 03:06:08	2019-01-21 03:06:08
202	3315	33	KABUPATEN GROBOGAN	2019-01-21 03:06:08	2019-01-21 03:06:08
206	3319	33	KABUPATEN KUDUS	2019-01-21 03:06:08	2019-01-21 03:06:08
210	3323	33	KABUPATEN TEMANGGUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
213	3326	33	KABUPATEN PEKALONGAN	2019-01-21 03:06:08	2019-01-21 03:06:08
218	3372	33	KOTA SURAKARTA	2019-01-21 03:06:08	2019-01-21 03:06:08
222	3376	33	KOTA TEGAL	2019-01-21 03:06:08	2019-01-21 03:06:08
225	3403	34	KABUPATEN GUNUNG KIDUL	2019-01-21 03:06:08	2019-01-21 03:06:08
229	3502	35	KABUPATEN PONOROGO	2019-01-21 03:06:08	2019-01-21 03:06:08
233	3506	35	KABUPATEN KEDIRI	2019-01-21 03:06:08	2019-01-21 03:06:08
237	3510	35	KABUPATEN BANYUWANGI	2019-01-21 03:06:08	2019-01-21 03:06:08
241	3514	35	KABUPATEN PASURUAN	2019-01-21 03:06:08	2019-01-21 03:06:08
245	3518	35	KABUPATEN NGANJUK	2019-01-21 03:06:08	2019-01-21 03:06:08
249	3522	35	KABUPATEN BOJONEGORO	2019-01-21 03:06:08	2019-01-21 03:06:08
253	3526	35	KABUPATEN BANGKALAN	2019-01-21 03:06:08	2019-01-21 03:06:08
257	3571	35	KOTA KEDIRI	2019-01-21 03:06:08	2019-01-21 03:06:08
260	3574	35	KOTA PROBOLINGGO	2019-01-21 03:06:08	2019-01-21 03:06:08
263	3577	35	KOTA MADIUN	2019-01-21 03:06:08	2019-01-21 03:06:08
266	3601	36	KABUPATEN PANDEGLANG	2019-01-21 03:06:08	2019-01-21 03:06:08
270	3671	36	KOTA TANGERANG	2019-01-21 03:06:08	2019-01-21 03:06:08
274	5101	51	KABUPATEN JEMBRANA	2019-01-21 03:06:08	2019-01-21 03:06:08
278	5105	51	KABUPATEN KLUNGKUNG	2019-01-21 03:06:08	2019-01-21 03:06:08
282	5171	51	KOTA DENPASAR	2019-01-21 03:06:08	2019-01-21 03:06:08
285	5203	52	KABUPATEN LOMBOK TIMUR	2019-01-21 03:06:08	2019-01-21 03:06:08
289	5207	52	KABUPATEN SUMBAWA BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
293	5301	53	KABUPATEN SUMBA BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
297	5305	53	KABUPATEN TIMOR TENGAH UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
303	5311	53	KABUPATEN ENDE	2019-01-21 03:06:08	2019-01-21 03:06:08
306	5314	53	KABUPATEN ROTE NDAO	2019-01-21 03:06:08	2019-01-21 03:06:08
310	5318	53	KABUPATEN NAGEKEO	2019-01-21 03:06:08	2019-01-21 03:06:08
314	5371	53	KOTA KUPANG	2019-01-21 03:06:08	2019-01-21 03:06:08
317	6103	61	KABUPATEN LANDAK	2019-01-21 03:06:08	2019-01-21 03:06:08
321	6107	61	KABUPATEN SINTANG	2019-01-21 03:06:08	2019-01-21 03:06:08
325	6111	61	KABUPATEN KAYONG UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
329	6201	62	KABUPATEN KOTAWARINGIN BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
333	6205	62	KABUPATEN BARITO UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
338	6210	62	KABUPATEN PULANG PISAU	2019-01-21 03:06:08	2019-01-21 03:06:08
342	6271	62	KOTA PALANGKA RAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
346	6304	63	KABUPATEN BARITO KUALA	2019-01-21 03:06:08	2019-01-21 03:06:08
349	6307	63	KABUPATEN HULU SUNGAI TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
355	6372	63	KOTA BANJAR BARU	2019-01-21 03:06:08	2019-01-21 03:06:08
358	6403	64	KABUPATEN KUTAI KARTANEGARA	2019-01-21 03:06:08	2019-01-21 03:06:08
362	6411	64	KABUPATEN MAHAKAM HULU	2019-01-21 03:06:08	2019-01-21 03:06:08
367	6502	65	KABUPATEN BULUNGAN	2019-01-21 03:06:08	2019-01-21 03:06:08
371	7101	71	KABUPATEN BOLAANG MONGONDOW	2019-01-21 03:06:08	2019-01-21 03:06:08
375	7105	71	KABUPATEN MINAHASA SELATAN	2019-01-21 03:06:08	2019-01-21 03:06:08
378	7108	71	KABUPATEN SIAU TAGULANDANG BIARO	2019-01-21 03:06:08	2019-01-21 03:06:08
384	7173	71	KOTA TOMOHON	2019-01-21 03:06:08	2019-01-21 03:06:08
388	7203	72	KABUPATEN MOROWALI	2019-01-21 03:06:08	2019-01-21 03:06:08
389	7204	72	KABUPATEN POSO	2019-01-21 03:06:08	2019-01-21 03:06:08
393	7208	72	KABUPATEN PARIGI MOUTONG	2019-01-21 03:06:08	2019-01-21 03:06:08
397	7212	72	KABUPATEN MOROWALI UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
402	7304	73	KABUPATEN JENEPONTO	2019-01-21 03:06:08	2019-01-21 03:06:08
405	7307	73	KABUPATEN SINJAI	2019-01-21 03:06:08	2019-01-21 03:06:08
409	7311	73	KABUPATEN BONE	2019-01-21 03:06:08	2019-01-21 03:06:08
413	7315	73	KABUPATEN PINRANG	2019-01-21 03:06:08	2019-01-21 03:06:08
416	7318	73	KABUPATEN TANA TORAJA	2019-01-21 03:06:08	2019-01-21 03:06:08
421	7372	73	KOTA PAREPARE	2019-01-21 03:06:08	2019-01-21 03:06:08
425	7403	74	KABUPATEN KONAWE	2019-01-21 03:06:08	2019-01-21 03:06:08
429	7407	74	KABUPATEN WAKATOBI	2019-01-21 03:06:08	2019-01-21 03:06:08
432	7410	74	KABUPATEN KONAWE UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
436	7414	74	KABUPATEN BUTON TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
441	7502	75	KABUPATEN GORONTALO	2019-01-21 03:06:08	2019-01-21 03:06:08
444	7505	75	KABUPATEN GORONTALO UTARA	2019-01-21 03:06:08	2019-01-21 03:06:08
449	7604	76	KABUPATEN MAMUJU	2019-01-21 03:06:08	2019-01-21 03:06:08
452	8101	81	KABUPATEN MALUKU TENGGARA BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
457	8106	81	KABUPATEN SERAM BAGIAN BARAT	2019-01-21 03:06:08	2019-01-21 03:06:08
462	8172	81	KOTA TUAL	2019-01-21 03:06:08	2019-01-21 03:06:08
465	8203	82	KABUPATEN KEPULAUAN SULA	2019-01-21 03:06:08	2019-01-21 03:06:08
469	8207	82	KABUPATEN PULAU MOROTAI	2019-01-21 03:06:08	2019-01-21 03:06:08
474	9102	91	KABUPATEN KAIMANA	2019-01-21 03:06:08	2019-01-21 03:06:08
477	9105	91	KABUPATEN MANOKWARI	2019-01-21 03:06:08	2019-01-21 03:06:08
481	9109	91	KABUPATEN TAMBRAUW	2019-01-21 03:06:08	2019-01-21 03:06:08
485	9171	91	KOTA SORONG	2019-01-21 03:06:08	2019-01-21 03:06:08
488	9403	94	KABUPATEN JAYAPURA	2019-01-21 03:06:08	2019-01-21 03:06:08
492	9410	94	KABUPATEN PANIAI	2019-01-21 03:06:08	2019-01-21 03:06:08
496	9414	94	KABUPATEN MAPPI	2019-01-21 03:06:08	2019-01-21 03:06:08
499	9417	94	KABUPATEN PEGUNUNGAN BINTANG	2019-01-21 03:06:08	2019-01-21 03:06:08
504	9427	94	KABUPATEN SUPIORI	2019-01-21 03:06:08	2019-01-21 03:06:08
508	9431	94	KABUPATEN MAMBERAMO TENGAH	2019-01-21 03:06:08	2019-01-21 03:06:08
512	9435	94	KABUPATEN INTAN JAYA	2019-01-21 03:06:08	2019-01-21 03:06:08
\.


--
-- Name: kota_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('kota_id_seq', 1, true);


--
-- Data for Name: log_status; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY log_status (id, no_pendaftaran, status, created_at, updated_at, kode_status) FROM stdin;
3	SIP-0319-011-070-0003	Pembayaran dikonfirmasi. Proses pemeriksaan	2019-03-25 14:53:33	2019-03-25 14:54:00	1
2	SIP-0319-011-070-0002	Telah diperiksa. Proses verifikasi	2019-03-21 09:36:18	2019-03-25 15:41:22	2
1	SIP-0319-010-065-0001	SLO telah terbit. Proses verifikasi PLN	2019-03-21 09:34:13	2019-03-30 09:48:30	4
7	SIP-0419-011-070-0003	Pembayaran dikonfirmasi. Proses pemeriksaan	2019-04-18 11:27:26	2019-04-18 11:32:07	1
4	SIP-0419-011-070-0004	Pembayaran dikonfirmasi. Proses pemeriksaan	2019-04-01 11:26:00	2019-04-18 11:32:09	1
9	SIP-0419-010-065-0005	Pembayaran dikonfirmasi. Proses pemeriksaan	2019-04-18 11:34:38	2019-04-18 11:34:47	1
10	SIP-0419-011-070-0005	Pembayaran dikonfirmasi. Proses pemeriksaan	2019-04-18 11:40:16	2019-04-18 11:40:20	1
6	SIP-0419-010-065-0002	SLO telah terbit. Proses verifikasi PLN	2019-04-18 11:18:29	2019-04-23 11:52:24	4
5	SIP-0419-010-065-0001	SLO telah terbit. Proses verifikasi PLN	2019-04-18 11:17:35	2019-04-23 11:52:43	4
12	SIP-0819-010-059-0007	SLO telah terbit. Proses verifikasi PLN	2019-08-20 20:35:11	2019-08-20 21:17:39	4
13	SIP-0819-010-059-0008	Pembayaran dikonfirmasi. Proses pemeriksaan	2019-08-20 21:31:29	2019-08-20 21:32:35	1
14	SIP-0819-010-059-0009	SLO telah terbit. Proses verifikasi PLN	2019-08-21 13:38:36	2019-08-21 14:24:06	4
15	SIP-1019-010-059-0010	SLO telah terbit. Proses verifikasi PLN	2019-10-23 08:26:08	2019-10-23 09:09:47	4
16	SIP-1119-010-059-0011	Menunggu Pembayaran	2019-11-29 14:49:45	2019-11-29 14:49:45	1
11	SIP-0819-010-059-0006	SLO telah terbit. Proses verifikasi PLN	2019-08-12 19:49:31	2020-01-10 02:27:43	4
17	SIP-0120-010-059-0012	Pembayaran dikonfirmasi. Proses pemeriksaan	2020-01-10 00:37:09	2020-01-14 14:23:23	1
\.


--
-- Name: log_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('log_status_id_seq', 17, true);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
3	2019_01_20_150240_create_provinsis_table	1
4	2019_01_20_150430_create_kotas_table	1
5	2019_01_20_150519_create_kantor_wilayahs_table	1
6	2019_01_20_150556_create_kantor_areas_table	1
7	2019_01_22_021051_create_asosiasis_table	1
9	2019_01_22_050433_create_btls_table	1
10	2019_01_23_080813_create_tarifs_table	1
11	2019_01_23_090836_create_jenis_bangunans_table	1
13	2019_01_24_023105_create_penyedia_listriks_table	1
14	2019_01_24_092551_tabel_kantor_wilayah	2
15	2016_06_01_000001_create_oauth_auth_codes_table	3
16	2016_06_01_000002_create_oauth_access_tokens_table	3
17	2016_06_01_000003_create_oauth_refresh_tokens_table	3
18	2016_06_01_000004_create_oauth_clients_table	3
19	2016_06_01_000005_create_oauth_personal_access_clients_table	3
24	2019_01_22_042904_create_dayas_table	5
27	2019_01_26_031412_create_pelanggans_table	6
28	2019_01_29_044634_create_pembayarans_table	6
29	2019_02_09_054322_create_surat_tugas_table	6
30	2019_02_10_042842_create_table_surat_tugas_pivot	6
31	2019_02_15_024041_create_table_calon_pelanggan_web	7
33	2019_02_18_073706_create_devices_table	8
34	2019_02_15_061112_create_pemeriksas_table	9
39	2019_03_04_093231_create_table_log_status	12
40	2019_02_15_033943_create_notifications_table	13
43	2019_03_04_044915_create_table_berita	14
44	2019_03_04_054221_create_table_galeri	15
45	2019_03_05_142251_create_people_slideshow	16
46	2019_03_05_142313_create_slideshow	16
47	2019_02_08_090728_create_sertifikasis_table	17
48	2014_10_12_100000_create_password_resets_table	18
49	2019_02_02_025337_create_verifikasis_table	19
50	2019_03_20_071724_create_runingtext_frontend	20
51	2019_01_23_093309_create_sub_areas_table	21
52	2019_03_27_093638_create_staff_btl	22
55	2019_04_08_160351_create_rekap_sub_areas_table	23
56	2019_04_09_135828_create_rekap_areas_table	23
57	2019_04_11_094105_create_tagihan_sub_areas_table	23
58	2019_04_12_092044_create_setor_sub_areas_table	23
59	2019_04_13_175051_create_tagihan_areas_table	23
60	2019_04_14_200441_create_setor_areas_table	23
61	2019_04_15_094000_create_rekap_wilayahs_table	23
62	2019_04_15_105637_create_share_wilayahs_table	23
63	2019_04_15_145601_create_tagihan_wilayahs_table	23
64	2019_04_16_145541_create_setor_wilayahs_table	23
65	2019_04_16_160226_create_rekap_pusats_table	23
66	2019_04_18_094723_create_tagihan_pusats_table	23
67	2019_04_22_143757_create_alokasi_shares_table	24
68	2019_04_23_100740_create_share_internals_table	24
\.


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('migrations_id_seq', 68, true);


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY notifications (id, type, notifiable_id, notifiable_type, data, read_at, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: oauth_access_tokens; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY oauth_access_tokens (id, user_id, client_id, name, scopes, revoked, created_at, updated_at, expires_at) FROM stdin;
\.


--
-- Data for Name: oauth_auth_codes; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY oauth_auth_codes (id, user_id, client_id, scopes, revoked, expires_at) FROM stdin;
\.


--
-- Data for Name: oauth_clients; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY oauth_clients (id, user_id, name, secret, redirect, personal_access_client, password_client, revoked, created_at, updated_at) FROM stdin;
1	\N	Laravel Personal Access Client	jxlX4qZs4GiVthbgZ7fQsZuXEa4ncIO1D7GZQscs	http://localhost	t	f	f	2019-01-28 02:37:26	2019-01-28 02:37:26
2	\N	Laravel Password Grant Client	isFtriifcVa4VZ7WCvwXaZBOcfe5EKhskc2ssKAn	http://localhost	f	t	f	2019-01-28 02:37:26	2019-01-28 02:37:26
3	\N	Laravel Personal Access Client	koZ09FPiJH9MgxVxDTJFbsjGth2kDXvtQiAQ0sW3	http://localhost	t	f	f	2019-02-06 12:44:24	2019-02-06 12:44:24
4	\N	Laravel Password Grant Client	1efEuJK3dWnRgvhScitrAskOaUt5q2NkZPsTXn3J	http://localhost	f	t	f	2019-02-06 12:44:24	2019-02-06 12:44:24
\.


--
-- Name: oauth_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('oauth_clients_id_seq', 4, true);


--
-- Data for Name: oauth_personal_access_clients; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY oauth_personal_access_clients (id, client_id, created_at, updated_at) FROM stdin;
1	1	2019-01-28 02:37:26	2019-01-28 02:37:26
2	3	2019-02-06 12:44:24	2019-02-06 12:44:24
\.


--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('oauth_personal_access_clients_id_seq', 2, true);


--
-- Data for Name: oauth_refresh_tokens; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY oauth_refresh_tokens (id, access_token_id, revoked, expires_at) FROM stdin;
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY password_resets (id, email, token, created_at, updated_at) FROM stdin;
5	okicandra241@gmail.com	3474	2019-03-11 12:09:10	2019-03-11 12:09:10
6	okicandra21@gmail.com	2212	2019-03-26 10:14:06	2019-03-26 10:48:34
\.


--
-- Name: password_resets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('password_resets_id_seq', 6, true);


--
-- Data for Name: pelanggan; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY pelanggan (id, no_pendaftaran, no_agenda, nama, no_identitas, no_telepon, alamat, id_kwilayah, kode_kota, id_karea, id_subarea, telp, id_tarif, id_daya, id_btl, id_bangunan, id_penyedia, tipe, status, no_gambar, tgl_gambar, id_asosiasi, no_agendapln, tgl_regpln, diinput_oleh, status_temp, created_at, updated_at) FROM stdin;
4	SIP-0419-011-070-0004	BR004	Mahmudin	813698754012001	081365478010	Jln. Kasih	11	3471	70	353	\N	10	13	958	6	396	Baru	Sudah Bayar	\N	2019-04-18	8	\N	2019-04-18	Ninik Muni	0	2019-04-18 11:28:38	2019-04-18 11:32:09
8	SIP-0419-011-070-0005	BR006	Kurniawan	87500120120023	08123657420	Jln. Kurniawan	11	3471	70	353	\N	8	9	955	3	397	Baru	Sudah Bayar	\N	2019-04-18	6	\N	2019-04-18	Nonik Caca	0	2019-04-18 11:40:16	2019-04-18 11:40:20
7	SIP-0419-010-065-0005	BR005	Putra Andika	8136478012012001	081365478440	Jln. Mondar Mandir	10	3371	65	324	\N	8	14	1032	3	361	Baru	Sudah Bayar	\N	2019-04-18	4	\N	2019-04-18	Mahfud MD	1	2019-04-18 11:34:38	2019-04-23 11:46:11
1	SIP-0419-010-065-0001	BR001	I Putu Gede	813657801200101	081368714201	Jln. Ahmad Yani	10	3371	65	324	\N	12	50	1032	3	363	Baru	Sudah Bayar	\N	2019-04-18	15	\N	2019-04-18	Sri Lestari	1	2019-04-18 11:17:35	2019-04-23 11:49:10
2	SIP-0419-010-065-0002	BR002	Andika Kurniawan	87920150012001	082145785010	Jln. Tlogopancing 4	10	3371	65	324	\N	14	41	1034	3	362	Baru	Sudah Bayar	\N	2019-04-18	15	\N	2019-04-18	Sri Lestari	1	2019-04-18 11:18:29	2019-04-23 11:49:12
3	SIP-0419-011-070-0003	BR003	Murniati	813456780120001	081369852121	Jln. Kebenaran	11	3471	70	353	\N	12	46	953	3	395	Baru	Sudah Bayar	\N	2019-04-18	2	\N	2019-04-18	Ninik Muni	0	2019-04-18 11:27:26	2019-08-18 23:46:32
15	SIP-0120-010-059-0012	BR013	Raja Batubara	898989	08122334455	Jl. Wolter monginsidi 5A Semarang	10	3374	59	285	\N	6	5	1025	8	323	Baru	Sudah Bayar	\N	2020-01-10	1	\N	2020-01-10	SiSIP	0	2020-01-10 00:37:09	2020-01-14 14:23:23
10	SIP-0819-010-059-0007	BR008	anies baswedan	987987	0876454	semarang tengah qweqweq	10	3374	59	283	\N	1	3	1025	8	313	Baru	Sudah Bayar	\N	2019-08-20	1	\N	2019-08-20	bbb	1	2019-08-20 20:35:11	2019-08-20 20:36:04
12	SIP-0819-010-059-0009	BR010	Wahyu pratama	343434	081123123456	semarang tengah 123	10	3374	59	283	\N	2	2	1026	8	314	Baru	Sudah Bayar	\N	2019-08-21	1	\N	2019-08-21	bbb	1	2019-08-21 13:38:36	2019-08-21 13:47:01
13	SIP-1019-010-059-0010	BR011	joko sampurna	909090	090909	semarang semarang	10	3374	59	285	\N	3	4	1026	8	315	Baru	Sudah Bayar	\N	2019-10-23	6	\N	2019-10-23	bbb	1	2019-10-23 08:26:08	2019-10-23 08:32:53
14	SIP-1119-010-059-0011	BR012	Siroj	878787	878	plampitan semarang tengah	10	3374	59	283	\N	3	3	1025	8	313	Baru	Belum Bayar	\N	2019-11-29	1	\N	2019-11-29	aaa	\N	2019-11-29 14:49:45	2019-11-29 14:49:45
9	SIP-0819-010-059-0006	BR007	rano karno	123123123	081081081	jl. semarang kota semarang	10	3374	59	283	\N	1	3	1025	8	312	Baru	Sudah Bayar	123	2019-08-01	1	\N	2019-08-12	Jateng	1	2019-08-12 19:49:31	2020-01-10 01:51:04
\.


--
-- Name: pelanggan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('pelanggan_id_seq', 23, true);


--
-- Data for Name: pelanggan_temp; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY pelanggan_temp (id, id_pelanggan, id_tarif, id_daya, id_penyedia, id_staff, no_surat, status_terbit, created_at, updated_at, id_subarea, status_periksa) FROM stdin;
5	7	8	14	361	7	\N	0	2019-04-23 11:46:11	2019-04-23 11:46:11	324	\N
6	1	12	50	363	1	00003/IV/2019	1	2019-04-23 11:49:10	2019-04-23 11:50:14	324	1
7	2	14	41	362	1	00003/IV/2019	1	2019-04-23 11:49:12	2019-04-23 11:50:56	324	1
10	10	1	3	313	49	00004/VIII/2019	1	2019-08-20 20:36:04	2019-08-20 21:11:20	283	1
11	11	2	3	314	49	00005/VIII/2019	1	2019-08-20 21:32:46	2019-08-20 21:33:12	283	0
12	12	2	2	314	49	00006/VIII/2019	1	2019-08-21 13:47:01	2019-08-21 14:06:58	283	1
14	13	3	4	315	49	00007/X/2019	1	2019-10-23 08:32:53	2019-10-23 09:03:18	285	1
15	9	1	3	312	49	00008/I/2020	1	2020-01-10 01:51:04	2020-01-10 02:02:45	283	1
\.


--
-- Name: pelanggan_temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('pelanggan_temp_id_seq', 15, true);


--
-- Data for Name: pembayaran; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY pembayaran (id, no_pendaftaran, no_kwitansi, diterima, cetak_kwitansi, cetak, dicetak_oleh, created_at, updated_at, jumlah, tipe, tanggal) FROM stdin;
3	SIP-0419-011-070-0003	00003/BP-SIP/IV/2019	Ninik Muni	\N	Belum Dicetak	\N	2019-04-18 11:32:07	2019-04-18 11:32:07	13750000	Sub Area	2019-04-18
4	SIP-0419-011-070-0004	00004/BP-SIP/IV/2019	Ninik Muni	\N	Belum Dicetak	\N	2019-04-18 11:32:09	2019-04-18 11:32:09	330000	Sub Area	2019-04-18
5	SIP-0419-010-065-0005	00005/BP-SIP/IV/2019	Mahfud MD	\N	Belum Dicetak	\N	2019-04-18 11:34:47	2019-04-18 11:34:47	347500	Area	2019-04-18
6	SIP-0419-011-070-0005	00006/BP-SIP/IV/2019	Nonik Caca	\N	Belum Dicetak	\N	2019-04-18 11:40:20	2019-04-18 11:40:20	198000	Area	2019-04-18
2	SIP-0419-010-065-0002	00002/BP-SIP/IV/2019	Sri Lestari	1	Sudah Dicetak 1X	SiSipOk	2019-04-18 11:18:42	2019-07-04 14:24:25	10790000	Sub Area	2019-04-18
7	SIP-0819-010-059-0007	00007/BP-SIP/VIII/2019	bbb	\N	Belum Dicetak	\N	2019-08-20 20:35:31	2019-08-20 20:35:31	95000	Area	2019-08-20
8	SIP-0819-010-059-0008	00008/BP-SIP/VIII/2019	bbb	\N	Belum Dicetak	\N	2019-08-20 21:32:35	2019-08-20 21:32:35	95000	Area	2019-08-20
10	SIP-1019-010-059-0010	00010/BP-SIP/X/2019	bbb	\N	Belum Dicetak	\N	2019-10-23 08:27:41	2019-10-23 08:27:41	110000	Area	2019-10-23
11	SIP-0819-010-059-0006	00011/BP-SIP/I/2020	bbb	\N	Belum Dicetak	\N	2020-01-10 01:47:50	2020-01-10 01:47:50	95000	Area	2020-01-10
9	SIP-0819-010-059-0009	00009/BP-SIP/VIII/2019	bbb	1	Sudah Dicetak 1X	bbb	2019-08-21 13:39:21	2020-01-10 01:48:25	60000	Area	2019-08-21
1	SIP-0419-010-065-0001	00001/BP-SIP/IV/2019	Sri Lestari	6	Sudah Dicetak 6X	SiSIP	2019-04-18 11:18:36	2020-01-14 14:20:49	19620000	Sub Area	2019-04-18
12	SIP-0120-010-059-0012	00012/BP-SIP/I/2020	bbb	\N	Belum Dicetak	\N	2020-01-14 14:23:23	2020-01-14 14:23:23	105000	Area	2020-01-14
\.


--
-- Name: pembayaran_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('pembayaran_id_seq', 12, true);


--
-- Data for Name: pemeriksa; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY pemeriksa (id, no_pendaftaran, no_surat_tugas, no_pemeriksaan, no_lhpp, tgl_lhpp, gambar_instalasi, diagram_garis_tunggal, pe_utama, pe_cabang, pe_akhir, pe_kotak_kontak, jenis_peng_utama, jenis_peng_cabang, jenis_peng_akhir, penghantar_bumi_jenis, penghantar_bumi_penampang, penghantar_bumi_sistem, saklar_utama, saklar_cabang1, saklar_cabang2, phbk_utama, phbk_cabang1, phbk_cabang2, penghantar_utama, penghantar_cabang, penghantar_akhir, penghantar_3fasa, fitting_lampu, kotak_kontak, sakelar, tinggi_kotak_kontak, tinggi_phbk, jenis_kotak_kontak, tanda_komponen, pengujian_pembebanan, jml_phb_utama, jml_phb_1fasa, jml_phb_3fasa, jml_phb_cabang, jml_saluran_cabang, jml_saluran_akhir, jml_titik_lampu, jml_sakelar, kkb, kkk, tahanan_isolasi_penghantar, resisten_pembumian, jml_motor_listrik_unit, jml_motor_listrik_kwh, catatan, foto1, foto2, foto3, foto4, location, lat, lng, status_verifikasi, created_at, updated_at) FROM stdin;
2	SIP-0419-010-065-0002	00003/IV/2019	1836	LHI-0419-010-004-0002	2019-04-23	Gambar Sesuai	Diagram Sesuai	Ada	Ada	Ada	Ada	NYM	NYM	NYM	NYM	4 mm2	TTN-C-S	Sakelar 25 A	Sakelar 25 A	Sakelar 25 A	MCB 2 A	MCB 2 A	MCB 2 A	3x4 mm2	3x4 mm2	3x2.5 mm2	0	Tidak Sesuai	Sesuai	Sesuai	160	150	Putar	SNI	Baik	1	1	0	1	1	1	2	1	1	1	500	3	0	0	Laik Operasi	/images/pemeriksa/1555995056.2150.jpg	/images/pemeriksa/1555995056.2801.jpg	/images/pemeriksa/1555995056.4506.jpg	/images/pemeriksa/1555995056.2385.jpg	Jalan Gemah Kencana I, Gemah, Semarang City, Central Java, Indonesia	-7.01183809999999941	110.467951500000026	1	2019-04-23 11:50:56	2019-04-23 11:51:31
1	SIP-0419-010-065-0001	00003/IV/2019	2702	LHI-0419-010-004-0001	2019-04-23	Gambar Sesuai	Diagram Sesuai	Ada	Ada	Ada	Ada	NYM	NYM	NYM	NYM	4 mm2	TTN-C-S	Sakelar 25 A	Sakelar 25 A	Sakelar 25 A	MCB 2 A	MCB 2 A	MCB 2 A	3x4 mm2	3x4 mm2	3x2.5 mm2	0	Sesuai	Sesuai	Sesuai	160	150	Biasa	SNI	Baik	1	1	0	1	1	1	2	1	1	1	500	3	0	0	Laik Operasi	/images/pemeriksa/1555995014.3716.jpg	/images/pemeriksa/1555995014.7769.jpg	/images/pemeriksa/1555995014.7421.jpg	/images/pemeriksa/1555995014.7610.png	Tlogo Pancing, Jalan Tlogosari Raya 1, Muktiharjo Kidul, Semarang City, Central Java, Indonesia	-6.97909759999999935	110.460808300000053	1	2019-04-23 11:50:14	2019-04-23 11:51:44
3	SIP-0819-010-059-0007	00004/VIII/2019	4828	LHI-0819-010-047-0003	2019-08-20	Gambar Sesuai	Diagram Sesuai	Ada	Ada	Ada	Ada	4	4	5	6	2	TT	2	4	3	2	2	2	2	2	2	2	Sesuai	Sesuai	Sesuai	3	45	Biasa	SNI	Baik	2	2	2	2	2	2	2	2	2	2	2	2	2	3	JHJH	/images/pemeriksa/1566310275.8552.jpg	/images/pemeriksa/1566310276.5680.jpg	/images/pemeriksa/1566310280.3740.jpg	/images/pemeriksa/1566310280.5153.jpg	Semarang Tengah, Kota Semarang, Jawa Tengah, Indonesia	-6.98054950000000041	110.420250500000066	1	2019-08-20 21:11:20	2019-08-20 21:15:36
5	SIP-0819-010-059-0009	00006/VIII/2019	2943	LHI-0819-010-047-0004	2019-08-21	Gambar Sesuai	Diagram Sesuai	Ada	Ada	Ada	Ada	NYM	NYM3X2.5mm	NYM3X2.5mm	BC	2.5mm	TT	2	2	2	2	3	3	2m2	2	2	2	Sesuai	Sesuai	Sesuai	2	3	Biasa	SNI	Baik	1	1	4	2	1	2	10	3	3	3	3	3	2	2	catatan tes	/images/pemeriksa/1566371215.5603.jpg	/images/pemeriksa/1566371215.1098.jpg	/images/pemeriksa/1566371215.1526.jpg	/images/pemeriksa/1566371215.3181.jpg	semarang tengah	-7.00332020000000011	110.443606599999995	1	2019-08-21 14:06:58	2019-08-21 14:18:39
7	SIP-1019-010-059-0010	00007/X/2019	8242	LHI-1019-010-047-0005	2019-10-23	Gambar Sesuai	Diagram Sesuai	Ada	Ada	Ada	Ada	1	1	1	1	1	TT	1	1	1	1	1	1	1	1	1	1	Sesuai	Sesuai	Sesuai	2	2	Biasa	SNI	Baik	2	2	2	2	2	2	2	2	2	2	2	2	2	2	2	/images/pemeriksa/1571796195.5328.jpg	/images/pemeriksa/1571796198.8091.jpg	/images/pemeriksa/1571796198.2814.jpg	/images/pemeriksa/1571796198.6137.jpg	Semarang Timur KEC, Jalan Tirtoyoso IX, Rejosari, Kota Semarang, Jawa Tengah, Indonesia	-6.98094909999999924	110.439615000000003	1	2019-10-23 09:03:18	2019-10-23 09:06:41
9	SIP-0819-010-059-0006	00008/I/2020	8492	LHI-0120-010-047-0006	2020-01-10	Gambar Sesuai	Diagram Sesuai	Ada	Ada	Ada	Ada	aa	aa	cc	dd	ee	TT	1	2	1	2	1	2	1	2	1	2	Sesuai	Sesuai	Sesuai	10	11	Biasa	SNI	Baik	1	1	2	2	1	2	1	2	2	1	1	2	1	2	baik semua	/images/pemeriksa/1578596564.3563.jpg	/images/pemeriksa/1578596565.5497.jpg	/images/pemeriksa/1578596565.6450.jpg	/images/pemeriksa/1578596565.4398.jpg	\N	-6.22972799999999971	106.689428199999995	1	2020-01-10 02:02:45	2020-01-10 02:15:40
\.


--
-- Name: pemeriksa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('pemeriksa_id_seq', 9, true);


--
-- Data for Name: penyedia_listrik; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY penyedia_listrik (id, nama_penyedia, kode_pln, id_kwilayah, id_karea, alamat, status, created_at, updated_at) FROM stdin;
1	PT. PLN (Persero) ULP JANTHO	11113	2	1	-	Aktif	\N	\N
2	PT. PLN (Persero) ULP KEUDEBIENG	11111	2	1	-	Aktif	\N	\N
3	PT. PLN (Persero) ULP KOTA MERDUATI	11110	2	1	-	Aktif	\N	\N
4	PT. PLN (Persero) ULP LAMBARO	11112	2	1	Jln. Blang Bintang	Aktif	\N	\N
5	PT. PLN (Persero) ULP MERDUATI	11110	2	1	Jln. Tentara Pelajar No. 11 1	Aktif	\N	\N
6	PT. PLN (Persero) ULP SABANG	11114	2	1	-	Aktif	\N	\N
7	PT. PLN (Persero) ULP SYIAH KUALA	11115	2	1	-	Aktif	\N	\N
8	PT. PLN (Persero) ULP BLANG KEJEREN	11335	2	2	Jln. Kuta Panjang Blower Blank Kejern Kec. Gayo Luwes	Aktif	\N	\N
9	PT. PLN (Persero) ULP IDI	11332	2	2	Jln. Medan Banda Aceh Kp. Tanoh ano Kec.Idi Rayeuk	Aktif	\N	\N
10	PT. PLN (Persero) ULP KUALA SIMPANG	11331	2	2	Jln. Ir. H Juanda Kec. Karang Baru Kab. Aceh Tamiang	Aktif	\N	\N
11	PT. PLN (Persero) ULP KUTACANE	11334	2	2	Jln. A. Yani No.187 Pulo Latong Kutacane	Aktif	\N	\N
12	PT. PLN (Persero) ULP 2	11330	2	2	Jln. Ayani No.6 2	Aktif	\N	\N
13	PT. PLN (Persero) ULP PEUREULAK	11333	2	2	-	Aktif	\N	\N
14	PT. PLN (Persero) ULP BIREUEN	11225	2	3	-	Aktif	\N	\N
15	PT. PLN (Persero) ULP GANDAPURA	11228	2	3	Jln B. Aceh- Medan, Desa Keude Lapang, Kec. Gandapura, Bireun	Aktif	\N	\N
16	PT. PLN (Persero) ULP GEUDONG	11222	2	3	-	Aktif	\N	\N
17	PT. PLN (Persero) ULP JANARATA	11230	2	3	-	Aktif	\N	\N
18	PT. PLN (Persero) ULP KRUENGGEUKUEH	11221	2	3	-	Aktif	\N	\N
21	PT. PLN (Persero) ULP LHOKSUKON	11223	2	3	-	Aktif	\N	\N
22	PT. PLN (Persero) ULP MATANG GLUMPANG DUA	11226	2	3	-	Aktif	\N	\N
23	PT. PLN (Persero) ULP PANTONLABU	11224	2	3	-	Aktif	\N	\N
24	PT. PLN (Persero) ULP SAMALANGA	11227	2	3	Jln. T Bendaharawan, Gampong Matang Jareueng. Kec. Samalanga Kab Bireun	Aktif	\N	\N
25	PT. PLN (Persero) ULP SAMALANGA	11227	2	3	-	Aktif	\N	\N
26	PT. PLN (Persero) ULP TAKENGON	11229	2	3	-	Aktif	\N	\N
27	PT. PLN (Persero) ULP CALANG	11441	2	4	-	Aktif	\N	\N
28	PT. PLN (Persero) ULP JEURAM	11442	2	4	-	Aktif	\N	\N
30	PT. PLN (Persero) ULP SINABANG	11443	2	4	Jln. Pahlawan Desa Suka Karya Kec. Simeulue	Aktif	\N	\N
31	PT. PLN (Persero) ULP TEUNOM	11444	2	4	Jln. Banda Aceh- 4 Desa Panton Teunom Aceh Jaya	Aktif	\N	\N
32	PT. PLN (Persero) ULP BEUREUNEUN	11661	2	5	Jln.RSU Mutiara Timur Kota Mini	Aktif	\N	\N
33	PT. PLN (Persero) ULP MEUREUDU	11662	2	5	Jln. Listrik No.1 Kota Meureudu	Aktif	\N	\N
36	PT. PLN (Persero) ULP BLANG PIDIE	11553	2	6	Jln. Iskandar Muda No.45	Aktif	\N	\N
37	PT. PLN (Persero) ULP KOTA FAJAR	11555	2	6	Simpang 4	Aktif	\N	\N
38	PT. PLN (Persero) ULP LABUHAN HAJI	11556	2	6	Desa Kuta Iboh	Aktif	\N	\N
39	PT. PLN (Persero) ULP RIMO	11551	2	6	Jln. Sidorejo DSN III Jln. Perjuangan	Aktif	\N	\N
40	PT. PLN (Persero) ULP SINGKIL	11552	2	6	Jln. Siti Ambia	Aktif	\N	\N
41	PT. PLN (Persero) ULP 6	11550	2	6	Jln. TGK Umar Penanggalan Subusallam	Aktif	\N	\N
42	PT. PLN (Persero) ULP TAPAK TUAN	11554	2	6	Jln. Rahmat No.74	Aktif	\N	\N
43	KAWASAN INDUSTRI 7	75000	3	7	-	Aktif	\N	\N
44	PT. PLN (Persero) ULP BELAWAN	12010	3	7	-	Aktif	\N	\N
45	PT. PLN (Persero) ULP HELVETIA	12011	3	7	-	Aktif	\N	\N
46	PT. PLN (Persero) ULP LABUHAN	12013	3	7	-	Aktif	\N	\N
52	PT. PLN (Persero) ULP SUNGGAL	12012	3	7	-	Aktif	\N	\N
53	PT. PLN (Persero) ULP 8 BARAT	12203	3	8	-	Aktif	\N	\N
54	PT. PLN (Persero) ULP 8 KOTA	12201	3	8	-	Aktif	\N	\N
55	PT. PLN (Persero) ULP 8 TIMUR	12202	3	8	-	Aktif	\N	\N
56	PT. PLN (Persero) ULP BRASTAGI	12212	3	8	-	Aktif	\N	\N
57	PT. PLN (Persero) ULP GEBANG	12216	3	8	-	Aktif	\N	\N
58	PT. PLN (Persero) ULP KABANJAHE	12219	3	8	-	Aktif	\N	\N
59	PT. PLN (Persero) ULP KUALA	12213	3	8	-	Aktif	\N	\N
60	PT. PLN (Persero) ULP PKL BRANDAN	12217	3	8	-	Aktif	\N	\N
61	PT. PLN (Persero) ULP PKL SUSU	12218	3	8	-	Aktif	\N	\N
62	PT. PLN (Persero) ULP SIDIKALANG	12210	3	8	-	Aktif	\N	\N
63	PT. PLN (Persero) ULP STABAT	12214	3	8	-	Aktif	\N	\N
64	PT. PLN (Persero) ULP TANJUNGPURA	12215	3	8	-	Aktif	\N	\N
65	PT. PLN (Persero) ULP TIGA BINANGA	12211	3	8	-	Aktif	\N	\N
66	KAWASAN INDUSTRI MEDAN STAR	77000	3	9	-	Aktif	\N	\N
67	PT. PLN (Persero) ULP DELITUA	12615	3	9	-	Aktif	\N	\N
68	PT. PLN (Persero) ULP DENAI	12610	3	9	-	Aktif	\N	\N
69	PT. PLN (Persero) ULP GALANG	12617	3	9	-	Aktif	\N	\N
70	PT. PLN (Persero) ULP LUBUKPAKAM	12612	3	9	-	Aktif	\N	\N
71	PT. PLN (Persero) ULP PANCUR BATU	12611	3	9	-	Aktif	\N	\N
72	PT. PLN (Persero) ULP PERBAUNGAN	12614	3	9	-	Aktif	\N	\N
73	PT. PLN (Persero) ULP RAMPAH	12616	3	9	-	Aktif	\N	\N
74	PT. PLN (Persero) ULP TANJUNG MORAWA	12613	3	9	-	Aktif	\N	\N
75	PT. PLN (Persero) ULP GUNUNGTUA	12414	3	10	-	Aktif	\N	\N
76	PT. PLN (Persero) ULP KOTANOPAN	12411	3	10	-	Aktif	\N	\N
29	PT. PLN (Persero) ULP MEULABOH	11440	2	4	Jln. Suadaya No.30	Aktif	\N	2019-03-26 15:04:33
34	PT. PLN (Persero) ULP SIGLI	11660	2	5	Jln. Tgk Chik Ditiro No.3 5	Aktif	\N	2019-03-26 15:04:53
47	PT. PLN (Persero) ULP MEDAN BARU	12002	3	7	-	Aktif	\N	2019-03-26 15:05:08
48	PT. PLN (Persero) ULP MEDANJOHOR	12014	3	7	-	Aktif	\N	2019-03-26 15:05:17
49	PT. PLN (Persero) ULP MEDAN KOTA	12001	3	7	-	Aktif	\N	2019-03-26 15:05:28
50	PT. PLN (Persero) ULP MEDAN SELATAN	12003	3	7	-	Aktif	\N	2019-03-26 15:05:33
51	PT. PLN (Persero) ULP MEDAN TIMUR	12004	3	7	-	Aktif	\N	2019-03-26 15:05:40
77	PT. PLN (Persero) ULP NATAL	12415	3	10	-	Aktif	\N	\N
78	PT. PLN (Persero) ULP PANYABUNGAN	12410	3	10	-	Aktif	\N	\N
79	PT. PLN (Persero) ULP SIBUHUAN	12412	3	10	-	Aktif	\N	\N
80	PT. PLN (Persero) ULP SIDEMPUAN KOTA	12401	3	10	-	Aktif	\N	\N
81	PT. PLN (Persero) ULP SIPIROK	12413	3	10	-	Aktif	\N	\N
82	KAWASAN INDUSTRI SEI MANGKEI	7600	3	11	-	Aktif	\N	\N
83	PT. PLN (Persero) ULP DOLOK MASIHOL	12119	3	11	-	Aktif	\N	\N
84	PT. PLN (Persero) ULP INDRAPURA	12116	3	11	-	Aktif	\N	\N
85	PT. PLN (Persero) ULP KISARAN	12111	3	11	-	Aktif	\N	\N
86	PT. PLN (Persero) ULP LIMA PULUH	12102	3	11	-	Aktif	\N	\N
87	PT. PLN (Persero) ULP PANGURURAN	12118	3	11	-	Aktif	\N	\N
88	PT. PLN (Persero) ULP PARAPAT	12113	3	11	-	Aktif	\N	\N
89	PT. PLN (Persero) ULP PERDAGANGAN	12112	3	11	-	Aktif	\N	\N
90	PT. PLN (Persero) ULP SIANTAR KOTA	12101	3	11	-	Aktif	\N	\N
91	PT. PLN (Persero) ULP SIDAMANIK	12115	3	11	-	Aktif	\N	\N
92	PT. PLN (Persero) ULP TANAH JAWA	12117	3	11	-	Aktif	\N	\N
93	PT. PLN (Persero) ULP TANJUNG TIRAM	12114	3	11	-	Aktif	\N	\N
94	PT. PLN (Persero) ULP TEBING TINGGI	14850	3	11	-	Aktif	\N	\N
95	PT. PLN (Persero) ULP AEK KANOPAN	12512	3	12	-	Aktif	\N	\N
96	PT. PLN (Persero) ULP AEK KOTABATU	12514	3	12	-	Aktif	\N	\N
97	PT. PLN (Persero) ULP AEK NABARA	12515	3	12	-	Aktif	\N	\N
98	PT. PLN (Persero) ULP KOTA PINANG	12510	3	12	-	Aktif	\N	\N
99	PT. PLN (Persero) ULP LABUHAN BILIK	12513	3	12	-	Aktif	\N	\N
101	PT. PLN (Persero) ULP SIMPANG KAWAT	12516	3	12	-	Aktif	\N	\N
102	PT. PLN (Persero) ULP TANJUNG BALAI	12511	3	12	-	Aktif	\N	\N
103	PT. PLN (Persero) ULP BALIGE	12311	3	13	-	Aktif	\N	\N
104	PT. PLN (Persero) ULP BARUS	12315	3	13	-	Aktif	\N	\N
105	PT. PLN (Persero) ULP DOLOK SANGGUL	12313	3	13	-	Aktif	\N	\N
106	PT. PLN (Persero) ULP PORSEA	12310	3	13	-	Aktif	\N	\N
108	PT. PLN (Persero) ULP SIBORONGBORONG	12312	3	13	-	Aktif	\N	\N
109	PT. PLN (Persero) ULP TARUTUNG	12314	3	13	-	Aktif	\N	\N
110	PT. PLN (Persero) ULP GUNUNG SITOLI	12710	3	14	-	Aktif	\N	\N
112	PT. PLN (Persero) ULP TELUK DALAM	12711	3	14	-	Aktif	\N	\N
113	PELALAWAN SIAK	79000	4	15	-	Aktif	\N	\N
114	PT. PLN (Persero) ULP AIR MOLEK	52	4	15	-	Aktif	\N	\N
115	PT. PLN (Persero) ULP BANGKINANG	18190	4	15	-	Aktif	\N	\N
116	PT. PLN (Persero) ULP KAMPAR	18112	4	15	-	Aktif	\N	\N
117	PT. PLN (Persero) ULP LIPAT KAIN	18113	4	15	-	Aktif	\N	\N
118	PT. PLN (Persero) ULP PANAM	18140	4	15	-	Aktif	\N	\N
119	PT. PLN (Persero) ULP PANGKALAN KERINCI	18180	4	15	-	Aktif	\N	\N
120	PT. PLN (Persero) ULP PASIR PANGARAIAN	18160	4	15	-	Aktif	\N	\N
123	PT. PLN (Persero) ULP PERAWANG	18150	4	15	-	Aktif	\N	\N
124	PT. PLN (Persero) ULP RUMBAI	18130	4	15	-	Aktif	\N	\N
125	PT. PLN (Persero) ULP SIAK SRI INDRAPURA	18170	4	15	-	Aktif	\N	\N
126	PT. PLN (Persero) ULP SIMPANG TIGA	18120	4	15	-	Aktif	\N	\N
127	PT. PLN (Persero) ULP UJUNG BATU	18114	4	15	-	Aktif	\N	\N
128	PT. PLN (Persero) ULP BAGAN BATU	18260	4	16	-	Aktif	\N	\N
129	PT. PLN (Persero) ULP BAGAN SIAPI-API	18220	4	16	-	Aktif	\N	\N
130	PT. PLN (Persero) ULP BENGKALIS	18230	4	16	-	Aktif	\N	\N
133	PT. PLN (Persero) ULP DURI	18210	4	16	-	Aktif	\N	\N
134	PT. PLN (Persero) ULP SELATPANJANG	18240	4	16	-	Aktif	\N	\N
135	PT. PLN (Persero) ULP AIR MOLEK	18450	4	18	-	Aktif	\N	\N
136	PT. PLN (Persero) ULP Kuala Enok	18430	4	18	-	Aktif	\N	\N
138	PT. PLN (Persero) ULP TALUK KUANTAN	18420	4	18	-	Aktif	\N	\N
139	PT. PLN (Persero) ULP Tembilahan	18440	4	18	-	Aktif	\N	\N
140	SEMELINANG TEBING PERANAP	78000	4	18	-	Aktif	\N	\N
141	PT. PLN (Persero) ULP BULIAN	14350	5	21	-	Aktif	\N	\N
142	PT. PLN (Persero) ULP KOTA BARU	14310	5	21	-	Aktif	\N	\N
143	PT. PLN (Persero) ULP KUALA TUNGKAL	14360	5	21	-	Aktif	\N	\N
144	PT. PLN (Persero) ULP SABAK	14390	5	21	-	Aktif	\N	\N
145	PT. PLN (Persero) ULP SEBERANG KOTA	14320	5	21	-	Aktif	\N	\N
146	PT. PLN (Persero) ULP TELANAI PURA	14300	5	21	-	Aktif	\N	\N
147	PT. PLN (Persero) ULP BANGKO	14410	5	22	-	Aktif	\N	\N
149	PT. PLN (Persero) ULP MUARA TEBO	14430	5	22	-	Aktif	\N	\N
150	PT. PLN (Persero) ULP RIMBO BUJANG	14440	5	22	-	Aktif	\N	\N
151	PT. PLN (Persero) ULP SAROLANGUN	14420	5	22	-	Aktif	\N	\N
152	PT. PLN (Persero) ULP AMPERA	14130	6	19	-	Aktif	\N	\N
153	PT. PLN (Persero) ULP INDERALAYA	14900	6	19	-	Aktif	\N	\N
154	PT. PLN (Persero) ULP KAYU AGUNG	14910	6	19	-	Aktif	\N	\N
155	PT. PLN (Persero) ULP KENTEN	14110	6	19	-	Aktif	\N	\N
156	PT. PLN (Persero) ULP MARIANA	14240	6	19	-	Aktif	\N	\N
157	PT. PLN (Persero) ULP PANGKALAN BALAI	14230	6	19	-	Aktif	\N	\N
158	PT. PLN (Persero) ULP RIVAI	14100	6	19	-	Aktif	\N	\N
159	PT. PLN (Persero) ULP Seduduk Putih	1	6	19	-	Aktif	\N	\N
107	PT. PLN (Persero) ULP SIBOLGA KOTA	12301	3	13	-	Aktif	\N	2019-03-26 15:06:15
111	PT. PLN (Persero) ULP NIAS BARAT	12712	3	14	-	Aktif	\N	2019-03-26 15:06:24
121	PT. PLN (Persero) ULP PEKANBARU KOTA BARAT	18111	4	15	-	Aktif	\N	2019-03-26 15:07:01
131	PT. PLN (Persero) ULP DUMAI	18270	4	16	-	Aktif	\N	2019-03-26 15:07:27
132	PT. PLN (Persero) ULP DUMAI KOTA	18250	4	16	-	Aktif	\N	2019-03-26 15:07:42
137	PT. PLN (Persero) ULP RENGAT KOTA	18410	4	18	-	Aktif	\N	2019-03-26 15:07:55
148	PT. PLN (Persero) ULP MUARA BUNGO	14400	5	22	-	Aktif	\N	2019-03-26 15:08:06
160	PT. PLN (Persero) ULP SEKAYU	14220	6	19	-	Aktif	\N	\N
161	PT. PLN (Persero) ULP SUKARAME	14120	6	19	-	Aktif	\N	\N
162	PT. PLN (Persero) ULP TUGU MULYO	14930	6	19	-	Aktif	\N	\N
163	PT. PLN (Persero) ULP BATURAJA	14750	6	20	-	Aktif	\N	\N
164	PT. PLN (Persero) ULP LEMBAYUNG	14700	6	20	-	Aktif	\N	\N
165	PT. PLN (Persero) ULP LUBUK LINGGAU	14730	6	20	-	Aktif	\N	\N
166	PT. PLN (Persero) ULP Marta Pura	14760	6	20	-	Aktif	\N	\N
167	PT. PLN (Persero) ULP MUARA BELITI	14780	6	20	-	Aktif	\N	\N
168	PT. PLN (Persero) ULP MUARA DUA	14770	6	20	-	Aktif	\N	\N
169	PT. PLN (Persero) ULP MUARA ENIM	14710	6	20	-	Aktif	\N	\N
170	PT. PLN (Persero) ULP PAGAR ALAM	14720	6	20	-	Aktif	\N	\N
171	PT. PLN (Persero) ULP PENDOPO	14790	6	20	-	Aktif	\N	\N
172	PT. PLN (Persero) ULP PRABUMULIH	14920	6	20	-	Aktif	\N	\N
173	PT. PLN (Persero) ULP TEBING TINGGI	14850	6	20	-	Aktif	\N	\N
178	KAWASAN INDUSTRI KRAKATAU	65000	7	42	-	Aktif	\N	\N
179	KAWASAN KRAKATAU POSCO	68000	7	42	-	Aktif	\N	\N
180	PABRIK SULFINDO ADI USAHA	70000	7	42	-	Aktif	\N	\N
181	PT INDAH KIAT PULP & PAPER 1	66000	7	42	-	Aktif	\N	\N
182	PT INDAH KIAT PULP & PAPER 2	67000	7	42	-	Aktif	\N	\N
183	PT. PLN (Persero) ULP ANYER	56130	7	42	-	Aktif	\N	\N
184	PT. PLN (Persero) ULP CIKANDE	56140	7	42	-	Aktif	\N	\N
185	PT. PLN (Persero) ULP CILEGON	56120	7	42	-	Aktif	\N	\N
186	PT. PLN (Persero) ULP PRIMA KRAKATAU (TT/TM)	56150	7	42	-	Aktif	\N	\N
187	PT. PLN (Persero) ULP SERANG KOTA	56110	7	42	JL.DIPONEGORO NO. 2 SERANG	Aktif	\N	\N
188	PT. PLN (Persero) ULP LABUAN	56240	7	43	-	Aktif	\N	\N
189	PT. PLN (Persero) ULP MALINGPING	56230	7	43	-	Aktif	\N	\N
190	PT. PLN (Persero) ULP PANDEGLANG	56220	7	43	-	Aktif	\N	\N
191	PT. PLN (Persero) ULP RANGKASBITUNG	56210	7	43	-	Aktif	\N	\N
192	PT. PLN (Persero) UP3 BANDENGAN	54210	8	23	Jln. Bandengan Utara No.79	Aktif	\N	\N
193	PT. PLN (Persero) UP3 BINTARO	54380	8	23	Jln. MH Thamrin Kav A-2 No. 17 Sektor 8 Central Bussiness Bintaro	Aktif	\N	\N
194	PT. PLN (Persero) UP3 BULUNGAN	54310	8	23	Jln. Sisingamangaraja No.1 Kebayoran Baru	Aktif	\N	\N
195	PT. PLN (Persero) UP3 Cengkareng	54630	8	23	Jln. Lingkar Duri Kosambi, Cengkareng	Aktif	\N	\N
196	PT. PLN (Persero) UP3 CIPUTAT	54360	8	23	Jln. RE Martadinata KM.27, Ciputat Tangerang	Aktif	\N	\N
197	PT. PLN (Persero) UP3 KEBON JERUK	54330	8	23	Pert. Intercon Blok A IX No.7-8 Jl Meruya Ilir raya Jakarta Telp. 5852534 5852562 5873905	Aktif	\N	\N
199	APARTEMEN CITY LIGHT	69000	8	26	-	Aktif	\N	\N
201	PT. PLN (Persero) UP3 CIRACAS	54720	8	27	-	Aktif	\N	\N
202	PT. PLN (Persero) UP3 Tanjung Priok	54510	8	27	Jln. Yos Sudarso Kav.85, Sunter	Aktif	\N	\N
204	KAWASAN PELABUHAN TJ PRIOK	57000	8	30	-	Aktif	\N	\N
214	PT. PLN (Persero) ULP CIBADAK	53621	9	44	-	Aktif	\N	\N
215	PT. PLN (Persero) ULP CIBATU	53286	9	44	-	Aktif	\N	\N
216	PT. PLN (Persero) ULP CICURUG	53659	9	44	-	Aktif	\N	\N
217	PT. PLN (Persero) ULP CIJAWURA	53567	9	44	-	Aktif	\N	\N
218	PT. PLN (Persero) ULP CIKAJANG	53276	9	44	-	Aktif	\N	\N
219	PT. PLN (Persero) ULP CIKEMBAR	53641	9	44	-	Aktif	\N	\N
220	PT. PLN (Persero) ULP CIMAHI SELATAN	53599	9	44	-	Aktif	\N	\N
221	PT. PLN (Persero) ULP GARUT KOTA	53271	9	44	-	Aktif	\N	\N
222	PT. PLN (Persero) ULP JATIWANGI	53527	9	44	-	Aktif	\N	\N
223	PT. PLN (Persero) ULP KOPO	53575	9	44	-	Aktif	\N	\N
224	PT. PLN (Persero) ULP MAJALENGKA	53521	9	44	-	Aktif	\N	\N
225	PT. PLN (Persero) ULP Ngangel	1	9	44	Jln. Ngangel	Aktif	\N	\N
226	PT. PLN (Persero) ULP PAGADEN	53421	9	44	-	Aktif	\N	\N
227	PT. PLN (Persero) ULP PAMANUKAN	53411	9	44	-	Aktif	\N	\N
228	PT. PLN (Persero) ULP PAMEUNGPEUK	53277	9	44	-	Aktif	\N	\N
229	PT. PLN (Persero) ULP PELABUHAN RATU	53631	9	44	-	Aktif	\N	\N
230	PT. PLN (Persero) ULP PLERED	53431	9	44	-	Aktif	\N	\N
231	PT. PLN (Persero) ULP PRIANGAN (TT/TM)	53577	9	44	-	Aktif	\N	\N
232	PT. PLN (Persero) ULP PRIMA CIBABAT	53598	9	44	-	Aktif	\N	\N
233	PT. PLN (Persero) ULP PRIMA MAJALAYA (TT/TM)	53536	9	44	-	Aktif	\N	\N
234	PT. PLN (Persero) ULP PURWAKARTA KOTA	53444	9	44	-	Aktif	\N	\N
235	PT. PLN (Persero) ULP SUBANG	53425	9	44	-	Aktif	\N	\N
175	PT. PLN (Persero) UP3 SERPONG	56620	7	39	Jln. Raya 39 BSD Sektor 8, Tangerang	Aktif	\N	2019-03-26 15:08:25
176	PT. PLN (Persero) ULP CIKUPA	56640	7	40	Jl. Raya Serang Km.17, 40 Tangerang	Aktif	\N	2019-03-26 15:08:50
198	PT. PLN (Persero) UP3 MENTENG	54110	8	23	Jln. M.I.R. Rais No.1	Aktif	\N	2019-03-26 15:09:15
200	PT. PLN (Persero) UP3 CEMPAKA PUTIH	54130	8	27	Jln. Jend A. Yani No. 60	Aktif	\N	2019-03-26 15:09:23
203	PT. PLN (Persero) UP3 JATINEGARA	54410	8	29	Jln. Tajinegara Timur No.75 Jakarta 11310	Aktif	\N	2019-03-26 15:09:33
205	PT. PLN (Persero) UP3 MARUNDA	54530	8	32	Ruko Unjung Menteng No. 25-28 Jln. Bekasi Raya Km. 25 Jakarta Timur	Aktif	\N	2019-03-26 15:09:40
206	PT. PLN (Persero) UP3 PONDOK KOPI	54420	8	33	Jln. Sentra Primer Baru Timur, Pulogebang	Aktif	\N	2019-03-26 15:09:47
207	PT. PLN (Persero) UP3 KRAMAT JATI	54710	8	34	Jln. Raya Bogor Km. 20 Jakarta 13520	Aktif	\N	2019-03-26 15:09:54
208	PT. PLN (Persero) UP3 LENTENG AGUNG	54740	8	35	Jln. Raya Tanjung Barat No. 55	Aktif	\N	2019-03-26 15:10:02
209	PT. PLN (Persero) UP3 PONDOK GEDE	54730	8	37	Jln. Raya Jatimakmur No.150	Aktif	\N	2019-03-26 15:10:11
210	PT. PLN (Persero) ULP BANDUNG BARAT	53555	9	44	-	Aktif	\N	2019-03-26 15:10:22
211	PT. PLN (Persero) ULP BANDUNG SELATAN	53551	9	44	-	Aktif	\N	2019-03-26 15:10:27
213	PT. PLN (Persero) ULP BANDUNG UTARA	53559	9	44	-	Aktif	\N	2019-03-26 15:10:42
236	PT. PLN (Persero) ULP SUKABUMI KOTA	53611	9	44	-	Aktif	\N	\N
237	PT. PLN (Persero) ULP SUKARAJA	53651	9	44	-	Aktif	\N	\N
238	PT. PLN (Persero) ULP SUMEDANG KOTA	53511	9	44	-	Aktif	\N	\N
239	PT. PLN (Persero) ULP TANJUNG SARI	53517	9	44	-	Aktif	\N	\N
240	PT. PLN (Persero) ULP UJUNGBERUNG	53571	9	44	-	Aktif	\N	\N
241	PASIR SINGKIL KELAPA NUNGGAL	64000	9	45	-	Aktif	\N	\N
245	PT. PLN (Persero) ULP CIPAYUNG	53811	9	45	-	Aktif	\N	\N
246	PT. PLN (Persero) ULP JASINGA	53853	9	45	-	Aktif	\N	\N
247	PT. PLN (Persero) ULP LEUWILIANG	53851	9	45	-	Aktif	\N	\N
248	PT. PLN (Persero) ULP PRIMA PAKUAN (TT/TM)	53825	9	45	-	Aktif	\N	\N
249	EAST JAKARTA INDUSTRIAL PARK (EJIP)	59003	9	46	-	Aktif	\N	\N
250	GUNUNG CIREMAI INTI	59005	9	46	-	Aktif	\N	\N
251	HYUNDAI INTI DEVELOPMENT	59004	9	46	-	Aktif	\N	\N
252	KAWASAN INDUSTRI JABABEKA	59001	9	46	-	Aktif	\N	\N
253	KAWASAN MEGALOPOLIS MANUNGGAL 2100	59002	9	46	-	Aktif	\N	\N
254	PT GERBANG TEKNOLOGI CIKARANG	60000	9	46	-	Aktif	\N	\N
255	PT. PLN (Persero) ULP BABELAN	53732	9	46	-	Aktif	\N	\N
256	PT. PLN (Persero) ULP BANTARGEBANG	53741	9	46	-	Aktif	\N	\N
258	PT. PLN (Persero) ULP CIBITUNG	53762	9	46	-	Aktif	\N	\N
259	PT. PLN (Persero) ULP CIKARANG	53721	9	46	-	Aktif	\N	\N
260	PT. PLN (Persero) ULP LEMAHABANG	53761	9	46	-	Aktif	\N	\N
261	PT. PLN (Persero) ULP MEDAN SATRIA	53731	9	46	-	Aktif	\N	\N
262	PT. PLN (Persero) ULP MUSTIKA JAYA	53742	9	46	-	Aktif	\N	\N
264	PT. PLN (Persero) ULP TAMBUN	53751	9	46	-	Aktif	\N	\N
265	TURI JAYA SEGARA MAKMUR	63000	9	46	-	Aktif	\N	\N
267	PT. PLN (Persero) ULP CIPANAS	53671	9	47	-	Aktif	\N	\N
268	PT. PLN (Persero) ULP MANDE	53681	9	47	-	Aktif	\N	\N
269	PT. PLN (Persero) ULP SUKANAGARA	53695	9	47	-	Aktif	\N	\N
270	PT. PLN (Persero) ULP TANGGEUNG	53691	9	47	-	Aktif	\N	\N
271	PT. PLN (Persero) ULP CILILIN	53592	9	48	-	Aktif	\N	\N
273	PT. PLN (Persero) ULP LEMBANG	53589	9	48	-	Aktif	\N	\N
274	PT. PLN (Persero) ULP PADALARANG	53585	9	48	-	Aktif	\N	\N
275	PT. PLN (Persero) ULP RAJAMANDALA	53595	9	48	-	Aktif	\N	\N
276	PT. PLN (Persero) ULP CILEDUG	53351	9	49	-	Aktif	\N	\N
277	PT. PLN (Persero) ULP CILIMUS	53371	9	49	-	Aktif	\N	\N
279	PT. PLN (Persero) ULP HAURGEULIS	53381	9	49	-	Aktif	\N	\N
280	PT. PLN (Persero) ULP INDRAMAYU	53341	9	49	-	Aktif	\N	\N
281	PT. PLN (Persero) ULP KUNINGAN	53321	9	49	-	Aktif	\N	\N
282	PT. PLN (Persero) ULP SUMBER	53331	9	49	-	Aktif	\N	\N
283	PT. PLN (Persero) ULP BOJONGGEDE	53875	9	50	-	Aktif	\N	\N
284	PT. PLN (Persero) ULP CIBINONG	53872	9	50	-	Aktif	\N	\N
285	PT. PLN (Persero) ULP CIMANGGIS	53874	9	50	-	Aktif	\N	\N
286	PT. PLN (Persero) ULP 50 KOTA	53871	9	50	-	Aktif	\N	\N
287	PT. PLN (Persero) ULP SAWANGAN	53873	9	50	-	Aktif	\N	\N
288	PT. PLN (Persero) ULP CILEUNGSI	53865	9	51	-	Aktif	\N	\N
289	PT. PLN (Persero) ULP CITEUREUP	53861	9	51	-	Aktif	\N	\N
290	PT. PLN (Persero) ULP JONGGOL	53867	9	51	-	Aktif	\N	\N
291	KAWASAN INDUSTRI KOTA BUKIT INDAH	58000	9	53	-	Aktif	\N	\N
292	PT PINDO DELI & PAPER MILL ADIARSA	61000	9	53	-	Aktif	\N	\N
293	PT PINDO DELI & PAPER MILL KUTA MEKAR	62000	9	53	-	Aktif	\N	\N
294	PT. PLN (Persero) ULP CIKAMPEK	53461	9	53	-	Aktif	\N	\N
296	PT. PLN (Persero) ULP KOSAMBI	53468	9	53	-	Aktif	\N	\N
297	PT. PLN (Persero) ULP LELES	53293	9	53	-	Aktif	\N	\N
299	PT. PLN (Persero) ULP RENGASDENGKLOK	53482	9	53	-	Aktif	\N	\N
300	PT. PLN (Persero) ULP BALEENDAH	53535	9	54	-	Aktif	\N	\N
301	PT. PLN (Persero) ULP BANJARAN	53534	9	54	-	Aktif	\N	\N
303	PT. PLN (Persero) ULP RANCAEKEK	53533	9	54	-	Aktif	\N	\N
304	PT. PLN (Persero) ULP SOREANG	53532	9	54	-	Aktif	\N	\N
305	PT. PLN (Persero) ULP BANJAR	53221	9	58	-	Aktif	\N	\N
306	PT. PLN (Persero) ULP CIAMIS	53231	9	58	-	Aktif	\N	\N
307	PT. PLN (Persero) ULP KARANGNUNGGAL	53265	9	58	-	Aktif	\N	\N
308	PT. PLN (Persero) ULP PANGANDARAN	53211	9	58	-	Aktif	\N	\N
309	PT. PLN (Persero) ULP RAJAPOLAH	53251	9	58	-	Aktif	\N	\N
310	PT. PLN (Persero) ULP SINGAPARNA	53261	9	58	-	Aktif	\N	\N
312	KAWASAN INDUSTRI KALIWUNGU	71000	10	59	-	Aktif	\N	\N
313	PT. PLN (Persero) ULP BEO	31820	10	59	-	Aktif	\N	\N
314	PT. PLN (Persero) ULP BOJA	52310	10	59	-	Aktif	\N	\N
315	PT. PLN (Persero) ULP KENDAL	52305	10	59	-	Aktif	\N	\N
316	PT. PLN (Persero) ULP LIRUNG	31830	10	59	-	Aktif	\N	\N
317	PT. PLN (Persero) ULP MELONGUANE	31870	10	59	-	Aktif	\N	\N
318	PT. PLN (Persero) ULP PETTA	31840	10	59	-	Aktif	\N	\N
319	PT. PLN (Persero) ULP Porwadadi	1	10	59	Jln. Soponyono V/21 RT.14/ RW.21 Purwodadi	Aktif	\N	\N
244	PT. PLN (Persero) ULP BOGOR TIMUR	53821	9	45	-	Aktif	\N	2019-03-26 15:11:05
257	PT. PLN (Persero) ULP BEKASI KOTA	53711	9	46	-	Aktif	\N	2019-03-26 15:11:16
263	PT. PLN (Persero) ULP PRIMA BEKASI (TT/TM)	53771	9	46	-	Aktif	\N	2019-03-26 15:11:26
266	PT. PLN (Persero) ULP CIANJUR KOTA	53661	9	47	-	Aktif	\N	2019-03-26 15:11:35
272	PT. PLN (Persero) ULP CIMAHI KOTA	53581	9	48	-	Aktif	\N	2019-03-26 15:11:43
278	PT. PLN (Persero) ULP CIREBON KOTA	53311	9	49	-	Aktif	\N	2019-03-26 15:11:52
295	PT. PLN (Persero) ULP KARAWANG KOTA	53475	9	53	-	Aktif	\N	2019-03-26 15:12:15
302	PT. PLN (Persero) ULP MAJALAYA	53531	9	54	-	Aktif	\N	2019-03-26 15:12:24
311	PT. PLN (Persero) ULP TASIKMALAYA KOTA	53241	9	58	-	Aktif	\N	2019-03-26 15:12:34
320	PT. PLN (Persero) ULP SEMARANG BARAT	52302	10	59	-	Aktif	\N	\N
321	PT. PLN (Persero) ULP SEMARANG SELATAN	52304	10	59	-	Aktif	\N	\N
322	PT. PLN (Persero) ULP SEMARANG TENGAH	52301	10	59	-	Aktif	\N	\N
323	PT. PLN (Persero) ULP SEMARANG TIMUR	52303	10	59	-	Aktif	\N	\N
324	PT. PLN (Persero) ULP TAMAKO	31850	10	59	-	Aktif	\N	\N
325	PT. PLN (Persero) ULP WELERI	52309	10	59	-	Aktif	\N	\N
326	PT. PLN (Persero) ULP GROGOL	52652	10	60	-	Aktif	\N	\N
327	PT. PLN (Persero) ULP JATISRONO	52654	10	60	-	Aktif	\N	\N
328	PT. PLN (Persero) ULP KARANGANYAR	52655	10	60	-	Aktif	\N	\N
329	PT. PLN (Persero) ULP KARTOSURA	52055	10	60	-	Aktif	\N	\N
330	PT. PLN (Persero) ULP MANAHAN	52052	10	60	-	Aktif	\N	\N
331	PT. PLN (Persero) ULP PALUR	52057	10	60	-	Aktif	\N	\N
332	PT. PLN (Persero) ULP SRAGEN	52058	10	60	-	Aktif	\N	\N
333	PT. PLN (Persero) ULP SUKOHARJO	52651	10	60	Ruko Solo Permai HC-6, JL. IR Soekarno Grogol	Aktif	\N	\N
334	PT. PLN (Persero) ULP SUMBERLAWANG	52059	10	60	-	Aktif	\N	\N
335	PT. PLN (Persero) ULP SURAKARTA KOTA	52051	10	60	-	Aktif	\N	\N
336	PT. PLN (Persero) ULP WONOGIRI	52653	10	60	-	Aktif	\N	\N
337	PT. PLN (Persero) ULP BANGSRI	52003	10	61	-	Aktif	\N	\N
338	PT. PLN (Persero) ULP BLORA	52007	10	61	-	Aktif	\N	\N
339	PT. PLN (Persero) ULP CEPU	52008	10	61	-	Aktif	\N	\N
340	PT. PLN (Persero) ULP JEPARA	52002	10	61	-	Aktif	\N	\N
341	PT. PLN (Persero) ULP JUWANA	52005	10	61	-	Aktif	\N	\N
342	PT. PLN (Persero) ULP KUDUS KOTA	52001	10	61	-	Aktif	\N	\N
343	PT. PLN (Persero) ULP PATI	52004	10	61	-	Aktif	\N	\N
344	PT. PLN (Persero) ULP REMBANG	52006	10	61	-	Aktif	\N	\N
346	PT. PLN (Persero) ULP GOMBONG	52503	10	62	-	Aktif	\N	\N
347	PT. PLN (Persero) ULP KEBUMEN	52504	10	62	-	Aktif	\N	\N
348	PT. PLN (Persero) ULP KROYA	52505	10	62	-	Aktif	\N	\N
349	PT. PLN (Persero) ULP MAJENANG	52502	10	62	-	Aktif	\N	\N
350	PT. PLN (Persero) ULP SIDAREJA	52506	10	62	-	Aktif	\N	\N
351	PT. PLN (Persero) ULP DEMAK	52551	10	63	Jln. Jogoloya No.25 63	Aktif	\N	\N
352	PT. PLN (Persero) ULP PURWODADI	52553	10	63	-	Aktif	\N	\N
353	PT. PLN (Persero) ULP TEGOWANU	52552	10	63	Desa Tlogosir RT. 03/ RW.02 Kebon Agung	Aktif	\N	\N
354	PT. PLN (Persero) ULP WIROSARI	52554	10	63	Jln. Soponyono V/21 RT.14/ RW.21 Purwodadi	Aktif	\N	\N
355	PT. PLN (Persero) ULP BOYOLALI	52403	10	64	-	Aktif	\N	\N
356	PT. PLN (Persero) ULP DELANGGU	52402	10	64	-	Aktif	\N	\N
358	PT. PLN (Persero) ULP PEDAN	52401	10	64	-	Aktif	\N	\N
359	PT. PLN (Persero) ULP TULUNG	52405	10	64	-	Aktif	\N	\N
360	PT. PLN (Persero) ULP BOROBUDUR	52154	10	65	-	Aktif	\N	\N
361	PT. PLN (Persero) ULP KUTOARJO	52153	10	65	-	Aktif	\N	\N
362	PT. PLN (Persero) ULP 65 KOTA	52151	10	65	-	Aktif	\N	\N
363	PT. PLN (Persero) ULP PARAKAN	52156	10	65	-	Aktif	\N	\N
364	PT. PLN (Persero) ULP PURWOREJO	52152	10	65	-	Aktif	\N	\N
365	PT. PLN (Persero) ULP TEGALREJO	52157	10	65	-	Aktif	\N	\N
366	PT. PLN (Persero) ULP TEMANGGUNG	52155	10	65	-	Aktif	\N	\N
367	PT. PLN (Persero) ULP AJIBARANG	52206	10	66	-	Aktif	\N	\N
368	PT. PLN (Persero) ULP BANJARNEGARA	52204	10	66	-	Aktif	\N	\N
369	PT. PLN (Persero) ULP BANYUMAS	52202	10	66	-	Aktif	\N	\N
370	PT. PLN (Persero) ULP PURBALINGGA	52203	10	66	-	Aktif	\N	\N
371	PT. PLN (Persero) ULP 66 KOTA	52201	10	66	-	Aktif	\N	\N
372	PT. PLN (Persero) ULP 66 Kota	52201	10	66	-	Aktif	\N	\N
373	PT. PLN (Persero) ULP WANGON	52207	10	66	-	Aktif	\N	\N
374	PT. PLN (Persero) ULP WONOSOBO	52205	10	66	-	Aktif	\N	\N
375	PT. PLN (Persero) ULP BATANG	52452	10	67	-	Aktif	\N	\N
376	PT. PLN (Persero) ULP KEDUNGWUNI	52453	10	67	-	Aktif	\N	\N
378	PT. PLN (Persero) ULP WIRADESA	52454	10	67	-	Aktif	\N	\N
379	PT. PLN (Persero) ULP AMBARAWA	52352	10	68	-	Aktif	\N	\N
380	PT. PLN (Persero) ULP SALATIGA KOTA	52351	10	68	-	Aktif	\N	\N
381	PT. PLN (Persero) ULP UNGARAN	52353	10	68	-	Aktif	\N	\N
382	PT. PLN (Persero) ULP BALAPULANG	52259	10	69	-	Aktif	\N	\N
383	PT. PLN (Persero) ULP BREBES	52255	10	69	-	Aktif	\N	\N
384	PT. PLN (Persero) ULP BUMIAYU	52256	10	69	-	Aktif	\N	\N
385	PT. PLN (Persero) ULP COMAL	52257	10	69	-	Aktif	\N	\N
386	PT. PLN (Persero) ULP JATIBARANG	53361	10	69	-	Aktif	\N	\N
387	PT. PLN (Persero) ULP PEMALANG	52253	10	69	-	Aktif	\N	\N
388	PT. PLN (Persero) ULP RANDUDONGKAL	52260	10	69	-	Aktif	\N	\N
389	PT. PLN (Persero) ULP SLAWI	52254	10	69	-	Aktif	\N	\N
390	PT. PLN (Persero) ULP TEGAL KOTA	52251	10	69	-	Aktif	\N	\N
391	PT. PLN (Persero) ULP TEGAL TIMUR	52252	10	69	-	Aktif	\N	\N
392	PT. PLN (Persero) ULP BANTUL	52103	11	70	-	Aktif	\N	\N
393	PT. PLN (Persero) ULP KALASAN	52101	11	70	-	Aktif	\N	\N
394	PT. PLN (Persero) ULP SEDAYU	52104	11	70	-	Aktif	\N	\N
395	PT. PLN (Persero) ULP SLEMAN	52106	11	70	-	Aktif	\N	\N
396	PT. PLN (Persero) ULP WATES	52102	11	70	-	Aktif	\N	\N
397	PT. PLN (Persero) ULP WONOSARI	52105	11	70	-	Aktif	\N	\N
398	PT. PLN (Persero) ULP YOGYAKARTA SELATAN	52108	11	70	-	Aktif	\N	\N
399	PT. PLN (Persero) ULP YOGYAKARTA UTARA	52107	11	70	-	Aktif	\N	\N
400	PT. PLN (Persero) ULP EMBONG WUNGU	51106	12	71	-	Aktif	\N	\N
401	PT. PLN (Persero) ULP Embong Wungu	51106	12	71	-	Aktif	\N	\N
402	PT. PLN (Persero) ULP INDRAPURA	51101	12	71	-	Aktif	\N	\N
377	PT. PLN (Persero) ULP PEKALONGAN KOTA	52451	10	67	-	Aktif	\N	2019-03-26 15:13:09
403	PT. PLN (Persero) ULP Indrapura	51101	12	71	-	Aktif	\N	\N
404	PT. PLN (Persero) ULP KENJERAN	51105	12	71	-	Aktif	\N	\N
405	PT. PLN (Persero) ULP PERAK	51104	12	71	-	Aktif	\N	\N
406	PT. PLN (Persero) ULP PLOSO	51102	12	71	-	Aktif	\N	\N
407	PT. PLN (Persero) ULP TANDES	51103	12	71	-	Aktif	\N	\N
408	PT. PLN (Persero) ULP DARMO PERMAI	51140	12	72	-	Aktif	\N	\N
409	PT. PLN (Persero) ULP Darmo Permai	51140	12	72	-	Aktif	\N	\N
410	PT. PLN (Persero) ULP DUKUH KUPANG	51141	12	72	-	Aktif	\N	\N
411	PT. PLN (Persero) ULP Dukuh Kupang	51141	12	72	-	Aktif	\N	\N
412	PT. PLN (Persero) ULP Gedangan	51146	12	72	-	Aktif	\N	\N
413	PT. PLN (Persero) ULP NGAGEL	51142	12	72	-	Aktif	\N	\N
414	PT. PLN (Persero) ULP RUNGKUT	51143	12	72	-	Aktif	\N	\N
415	PT. PLN (Persero) ULP KARANG PILANG	51162	12	73	-	Aktif	\N	\N
416	PT. PLN (Persero) ULP MENGANTI	51163	12	73	-	Aktif	\N	\N
417	PT. PLN (Persero) ULP TAMAN	51161	12	73	-	Aktif	\N	\N
418	TERMINAL TELUK LAMONG	73000	12	73	-	Aktif	\N	\N
419	PT. PLN (Persero) ULP Gedangan	51146	12	74	-	Aktif	\N	\N
420	PT. PLN (Persero) ULP KRIAN	51181	12	74	-	Aktif	\N	\N
421	PT. PLN (Persero) ULP PORONG	51182	12	74	-	Aktif	\N	\N
422	PT. PLN (Persero) ULP SIDOARJO KOTA	51180	12	74	-	Aktif	\N	\N
423	PT. PLN (Persero) ULP Taman	51161	12	74	-	Aktif	\N	\N
424	PT. PLN (Persero) ULP JOMBANG	51453	12	75	-	Aktif	\N	\N
425	PT. PLN (Persero) ULP KERTOSONO	51458	12	75	-	Aktif	\N	\N
426	PT. PLN (Persero) ULP MOJOAGUNG	51450	12	75	-	Aktif	\N	\N
427	PT. PLN (Persero) ULP MOJOKERTO UTARA	51452	12	75	-	Aktif	\N	\N
428	PT. PLN (Persero) ULP MOJOSARI	51456	12	75	-	Aktif	\N	\N
429	PT. PLN (Persero) ULP NGANJUK	51460	12	75	-	Aktif	\N	\N
430	PT. PLN (Persero) ULP NGORO	51454	12	75	-	Aktif	\N	\N
431	PT. PLN (Persero) ULP PACET	51457	12	75	-	Aktif	\N	\N
432	PT. PLN (Persero) ULP Ploso	51455	12	75	-	Aktif	\N	\N
433	PT. PLN (Persero) ULP WARUJAYENG	51459	12	75	-	Aktif	\N	\N
434	PT. PLN (Persero) ULP BANGIL	51353	12	76	-	Aktif	\N	\N
435	PT. PLN (Persero) ULP GONDANG WETAN	51351	12	76	-	Aktif	\N	\N
436	PT. PLN (Persero) ULP GRATI	51352	12	76	-	Aktif	\N	\N
437	PT. PLN (Persero) ULP KRAKSAAN	51357	12	76	-	Aktif	\N	\N
438	PT. PLN (Persero) ULP PANDAAN	51354	12	76	-	Aktif	\N	\N
439	PT. PLN (Persero) ULP PASURUAN KOTA	51350	12	76	-	Aktif	\N	\N
440	PT. PLN (Persero) ULP PRIGEN	51355	12	76	-	Aktif	\N	\N
441	PT. PLN (Persero) ULP PROBOLINGGO	51356	12	76	-	Aktif	\N	\N
442	PT. PLN (Persero) ULP SUKOREJO	51358	12	76	-	Aktif	\N	\N
443	PT. PLN (Persero) ULP BABAT	51804	12	77	-	Aktif	\N	\N
444	PT. PLN (Persero) ULP BOJONEGORO	51801	12	77	-	Aktif	\N	\N
445	PT. PLN (Persero) ULP BRONDONG	51806	12	77	-	Aktif	\N	\N
446	PT. PLN (Persero) ULP JATIROGO	51807	12	77	-	Aktif	\N	\N
447	PT. PLN (Persero) ULP LAMONGAN	51803	12	77	-	Aktif	\N	\N
448	PT. PLN (Persero) ULP PADANGAN	51805	12	77	-	Aktif	\N	\N
449	PT. PLN (Persero) ULP SUMBERREJO	51808	12	77	-	Aktif	\N	\N
450	PT. PLN (Persero) ULP TUBAN	51802	12	77	-	Aktif	\N	\N
451	KAWASAN JAYA INTEGRATED INDUSTRIAL (JIIPE)	72000	12	78	-	Aktif	\N	\N
452	KOMPLEK INDUSTRI PT. PETROKIMIA GRESIK	74000	12	78	-	Aktif	\N	\N
453	PT. PLN (Persero) ULP BAWEAN	51123	12	78	-	Aktif	\N	\N
454	PT. PLN (Persero) ULP BENJENG	51122	12	78	-	Aktif	\N	\N
455	PT. PLN (Persero) ULP GRESIK	51120	12	78	-	Aktif	\N	\N
456	PT. PLN (Persero) ULP Menganti	51163	12	78	-	Aktif	\N	\N
457	PT. PLN (Persero) ULP SIDAYU	51121	12	78	-	Aktif	\N	\N
458	PT. PLN (Persero) ULP CARUBAN	51507	12	79	-	Aktif	\N	\N
459	PT. PLN (Persero) ULP DOLOPO	51508	12	79	-	Aktif	\N	\N
460	PT. PLN (Persero) ULP MADIUN KOTA	51501	12	79	-	Aktif	\N	\N
461	PT. PLN (Persero) ULP MAGETAN	51503	12	79	-	Aktif	\N	\N
462	PT. PLN (Persero) ULP MANTINGAN	51509	12	79	-	Aktif	\N	\N
463	PT. PLN (Persero) ULP MAOSPATI	51505	12	79	-	Aktif	\N	\N
464	PT. PLN (Persero) ULP NGAWI	51504	12	79	-	Aktif	\N	\N
465	PT. PLN (Persero) ULP BANYUWANGI	51670	12	80	-	Aktif	\N	\N
466	PT. PLN (Persero) ULP GENTENG	51673	12	80	-	Aktif	\N	\N
467	PT. PLN (Persero) ULP JAJAG	51675	12	80	-	Aktif	\N	\N
468	PT. PLN (Persero) ULP MUNCAR	51672	12	80	-	Aktif	\N	\N
469	PT. PLN (Persero) ULP ROGOJAMPI	51671	12	80	-	Aktif	\N	\N
470	PT. PLN (Persero) ULP AMBULU	51605	12	81	-	Aktif	\N	\N
471	PT. PLN (Persero) ULP JEMBER KOTA	51601	12	81	-	Aktif	\N	\N
472	PT. PLN (Persero) ULP KALISAT	51603	12	81	-	Aktif	\N	\N
473	PT. PLN (Persero) ULP KENCONG	51608	12	81	-	Aktif	\N	\N
474	PT. PLN (Persero) ULP KLAKAH	51606	12	81	-	Aktif	\N	\N
475	PT. PLN (Persero) ULP LUMAJANG	51602	12	81	-	Aktif	\N	\N
476	PT. PLN (Persero) ULP RAMBI PUJI	51604	12	81	-	Aktif	\N	\N
477	PT. PLN (Persero) ULP TANGGUL	51607	12	81	-	Aktif	\N	\N
478	PT. PLN (Persero) ULP TEMPEH	51609	12	81	-	Aktif	\N	\N
479	PT. PLN (Persero) ULP BLITAR	51402	12	82	-	Aktif	\N	\N
480	PT. PLN (Persero) ULP CAMPUR DARAT	51412	12	82	-	Aktif	\N	\N
481	PT. PLN (Persero) ULP KEDIRI BARAT	51411	12	82	-	Aktif	\N	\N
482	PT. PLN (Persero) ULP KEDIRI SELATAN	51410	12	82	-	Aktif	\N	\N
483	PT. PLN (Persero) ULP KEDIRI UTARA	51401	12	82	-	Aktif	\N	\N
484	PT. PLN (Persero) ULP NGUNUT	51404	12	82	-	Aktif	\N	\N
485	PT. PLN (Persero) ULP PARE	51406	12	82	-	Aktif	\N	\N
486	PT. PLN (Persero) ULP SRENGAT	51405	12	82	-	Aktif	\N	\N
487	PT. PLN (Persero) ULP SUTOJAYAN	51409	12	82	-	Aktif	\N	\N
488	PT. PLN (Persero) ULP TULUNGAGUNG	51403	12	82	-	Aktif	\N	\N
489	PT. PLN (Persero) ULP WLINGI	51408	12	82	-	Aktif	\N	\N
490	PT. PLN (Persero) ULP BATU	51303	12	83	-	Aktif	\N	\N
491	PT. PLN (Persero) ULP BLIMBING	51312	12	83	-	Aktif	\N	\N
492	PT. PLN (Persero) ULP BULULAWANG	51302	12	83	-	Aktif	\N	\N
493	PT. PLN (Persero) ULP DAMPIT	51308	12	83	-	Aktif	\N	\N
494	PT. PLN (Persero) ULP DINOYO	51311	12	83	-	Aktif	\N	\N
495	PT. PLN (Persero) ULP GONDANGLEGI	51307	12	83	-	Aktif	\N	\N
496	PT. PLN (Persero) ULP KEBONAGUNG	51314	12	83	-	Aktif	\N	\N
497	PT. PLN (Persero) ULP KEPANJEN	51305	12	83	-	Aktif	\N	\N
498	PT. PLN (Persero) ULP LAWANG	51301	12	83	-	Aktif	\N	\N
499	PT. PLN (Persero) ULP MALANG KOTA	51313	12	83	-	Aktif	\N	\N
500	PT. PLN (Persero) ULP NGANTANG	51309	12	83	-	Aktif	\N	\N
501	PT. PLN (Persero) ULP PANDAAN	51354	12	83	-	Aktif	\N	\N
502	PT. PLN (Persero) ULP SINGOSARI	51304	12	83	-	Aktif	\N	\N
503	PT. PLN (Persero) ULP SUMBER PUCUNG	51310	12	83	-	Aktif	\N	\N
504	PT. PLN (Persero) ULP TUMPANG	51306	12	83	-	Aktif	\N	\N
505	PT. PLN (Persero) ULP AMBUNTEN	51790	12	84	-	Aktif	\N	\N
506	PT. PLN (Persero) ULP BANGKALAN	51730	12	84	-	Aktif	\N	\N
507	PT. PLN (Persero) ULP BLEGA	51760	12	84	-	Aktif	\N	\N
508	PT. PLN (Persero) ULP KAMAL	51740	12	84	-	Aktif	\N	\N
509	PT. PLN (Persero) ULP KEPULAUAN KANGEAN	51715	12	84	-	Aktif	\N	\N
510	PT. PLN (Persero) ULP KETAPANG	51750	12	84	-	Aktif	\N	\N
511	PT. PLN (Persero) ULP PAMEKASAN	51700	12	84	-	Aktif	\N	\N
512	PT. PLN (Persero) ULP PRENDUAN	51770	12	84	-	Aktif	\N	\N
513	PT. PLN (Persero) ULP SAMPANG	51720	12	84	-	Aktif	\N	\N
514	PT. PLN (Persero) ULP SUMENEP	51710	12	84	-	Aktif	\N	\N
515	PT. PLN (Persero) ULP WARU	51780	12	84	-	Aktif	\N	\N
516	PT. PLN (Persero) ULP BALONG	51541	12	85	-	Aktif	\N	\N
517	PT. PLN (Persero) ULP PACITAN	51542	12	85	-	Aktif	\N	\N
518	PT. PLN (Persero) ULP PONOROGO	51540	12	85	-	Aktif	\N	\N
519	PT. PLN (Persero) ULP PONOROGO	51540	12	85	-	Aktif	\N	\N
520	PT. PLN (Persero) ULP TRENGGALEK	51543	12	85	-	Aktif	\N	\N
521	PT. PLN (Persero) ULP TRENGGALEK	51543	12	85	-	Aktif	\N	\N
522	PT. PLN (Persero) ULP ASEMBAGUS	51652	12	86	-	Aktif	\N	\N
523	PT. PLN (Persero) ULP BESUKI	51651	12	86	-	Aktif	\N	\N
524	PT. PLN (Persero) ULP BONDOWOSO	51653	12	86	-	Aktif	\N	\N
525	PT. PLN (Persero) ULP PANARUKAN	51650	12	86	-	Aktif	\N	\N
526	PT. PLN (Persero) ULP WONOSARI	51654	12	86	-	Aktif	\N	\N
527	PT. PLN (Persero) ULP DENPASAR	55100	13	87	-	Aktif	\N	\N
528	PT. PLN (Persero) ULP KUTA	55120	13	87	-	Aktif	\N	\N
529	PT. PLN (Persero) ULP Mengwi	55110	13	87	-	Aktif	\N	\N
530	PT. PLN (Persero) ULP TABANAN	55130	13	87	-	Aktif	\N	\N
531	PT. PLN (Persero) ULP BANGLI	55220	13	88	-	Aktif	\N	\N
532	PT. PLN (Persero) ULP GIANYAR	55200	13	88	-	Aktif	\N	\N
533	PT. PLN (Persero) ULP KARANGASEM	55230	13	88	-	Aktif	\N	\N
534	PT. PLN (Persero) ULP KLUNGKUNG	55210	13	88	-	Aktif	\N	\N
535	PT. PLN (Persero) ULP SANUR	55140	13	88	-	Aktif	\N	\N
536	PT. PLN (Persero) ULP GILIMANUK	55340	13	89	-	Aktif	\N	\N
537	PT. PLN (Persero) ULP NEGARA	55330	13	89	-	Aktif	\N	\N
538	PT. PLN (Persero) ULP SERIRIT	55320	13	89	-	Aktif	\N	\N
539	PT. PLN (Persero) ULP SINGARAJA	55300	13	89	-	Aktif	\N	\N
540	PT. PLN (Persero) ULP TEJAKULA	55310	13	89	-	Aktif	\N	\N
541	PT. PLN (Persero) ULP ALAS	44210	14	91	-	Aktif	\N	\N
542	PT. PLN (Persero) ULP AMPENAN	44150	14	90	-	Aktif	\N	\N
543	PT. PLN (Persero) ULP BIMA KOTA	44340	14	92	-	Aktif	\N	\N
544	PT. PLN (Persero) ULP CAKRA	44110	14	90	-	Aktif	\N	\N
545	PT. PLN (Persero) ULP DOMPU	44320	14	92	-	Aktif	\N	\N
546	PT. PLN (Persero) ULP EMPANG	44230	14	91	-	Aktif	\N	\N
547	PT. PLN (Persero) ULP PRAYA	44140	14	90	-	Aktif	\N	\N
548	PT. PLN (Persero) ULP PRINGGABAYA	44160	14	90	-	Aktif	\N	\N
549	PT. PLN (Persero) ULP SAMAWA REA	44240	14	91	-	Aktif	\N	\N
550	PT. PLN (Persero) ULP SAPE	44310	14	92	-	Aktif	\N	\N
551	PT. PLN (Persero) ULP SELONG	44120	14	90	-	Aktif	\N	\N
552	PT. PLN (Persero) ULP TALIWANG	44220	14	91	-	Aktif	\N	\N
553	PT. PLN (Persero) ULP TANJUNG	44130	14	90	-	Aktif	\N	\N
554	PT. PLN (Persero) ULP WOHA	44330	14	92	-	Aktif	\N	\N
555	PT. PLN (Persero) ULP ADONARA	43550	15	96	-	Aktif	\N	\N
556	PT. PLN (Persero) ULP ATAMBUA	43130	15	94	-	Aktif	\N	\N
557	PT. PLN (Persero) ULP BAJAWA	43730	15	95	-	Aktif	\N	\N
558	PT. PLN (Persero) ULP Ende	43700	15	95	-	Aktif	\N	\N
559	PT. PLN (Persero) ULP Kalabahi	43160	15	94	-	Aktif	\N	\N
560	PT. PLN (Persero) ULP Kefamenanu	43150	15	94	-	Aktif	\N	\N
561	PT. PLN (Persero) ULP Kupang	43100	15	94	-	Aktif	\N	\N
562	PT. PLN (Persero) ULP Labuan Bajo	43760	15	95	-	Aktif	\N	\N
563	PT. PLN (Persero) ULP Larantuka	43530	15	96	-	Aktif	\N	\N
564	PT. PLN (Persero) ULP Lembata	43570	15	96	-	Aktif	\N	\N
565	PT. PLN (Persero) ULP Maumere	43500	15	96	-	Aktif	\N	\N
566	PT. PLN (Persero) ULP Mbay	43770	15	95	-	Aktif	\N	\N
567	PT. PLN (Persero) ULP Oesao	43180	15	94	-	Aktif	\N	\N
568	PT. PLN (Persero) ULP Rotendao	43190	15	94	-	Aktif	\N	\N
569	PT. PLN (Persero) ULP Ruteng	43740	15	95	-	Aktif	\N	\N
570	PT. PLN (Persero) ULP Soe	43110	15	94	-	Aktif	\N	\N
571	PT. PLN (Persero) ULP Sumba Barat	43350	15	97	-	Aktif	\N	\N
572	PT. PLN (Persero) ULP Sumba Jaya	43380	15	97	-	Aktif	\N	\N
573	PT. PLN (Persero) ULP Sumba Timur	43300	15	97	-	Aktif	\N	\N
574	PT. PLN (Persero) ULP AMBON KOTA	41111	16	98	-	Aktif	\N	\N
575	PT. PLN (Persero) ULP BAGUALA	41130	16	98	-	Aktif	\N	\N
576	PT. PLN (Persero) ULP BANDA	41160	16	98	-	Aktif	\N	\N
577	PT. PLN (Persero) ULP HARUKU	41150	16	98	-	Aktif	\N	\N
578	PT. PLN (Persero) ULP HITU	41142	16	98	-	Aktif	\N	\N
579	PT. PLN (Persero) ULP KOTA TUAL	41310	16	98	-	Aktif	\N	\N
580	PT. PLN (Persero) ULP NAMLEA	41180	16	98	-	Aktif	\N	\N
581	PT. PLN (Persero) ULP NUSANIWE	41140	16	98	-	Aktif	\N	\N
582	PT. PLN (Persero) ULP Ranting Dobo	41330	16	98	-	Aktif	\N	\N
583	PT. PLN (Persero) ULP Ranting Saumlaki	41350	16	98	-	Aktif	\N	\N
584	PT. PLN (Persero) ULP SAPARUA	41151	16	98	-	Aktif	\N	\N
585	PT. PLN (Persero) ULP TULEHU	41134	16	98	-	Aktif	\N	\N
587	PT. PLN (Persero) ULP JAILOLO	41520	17	101	-	Aktif	\N	\N
588	PT. PLN (Persero) ULP SOFIFI	41530	17	101	-	Aktif	\N	\N
589	PT. PLN (Persero) ULP TOBELO	41510	17	101	-	Aktif	\N	\N
590	PT. PLN (Persero) ULP KOTA BANGUN	23120	18	103	-	Aktif	\N	\N
591	PT. PLN (Persero) ULP MELAK	23141	18	103	-	Aktif	\N	\N
592	PT. PLN (Persero) ULP SAMARINDA ILIR	23157	18	103	-	Aktif	\N	\N
593	PT. PLN (Persero) ULP SAMARINDA KOTA	23100	18	103	-	Aktif	\N	\N
594	PT. PLN (Persero) ULP SAMARINDA SEBERANG	23102	18	103	-	Aktif	\N	\N
595	PT. PLN (Persero) ULP SAMARINDA ULU	23156	18	103	-	Aktif	\N	\N
596	PT. PLN (Persero) ULP TARAKAN	23500	18	103	-	Aktif	\N	\N
597	PT. PLN (Persero) ULP TENGGARONG	23112	18	103	-	Aktif	\N	\N
598	KAWASAN INDUSTRI KARIANGAU	93000	18	104	-	Aktif	\N	\N
599	PT. PLN (Persero) ULP BALIKPAPAN SELATAN	23200	18	104	-	Aktif	\N	\N
600	PT. PLN (Persero) ULP BALIKPAPAN UTARA	23201	18	104	-	Aktif	\N	\N
601	PT. PLN (Persero) ULP LONGIKIS	23222	18	104	-	Aktif	\N	\N
602	PT. PLN (Persero) ULP NEGARA	55330	18	104	-	Aktif	\N	\N
603	PT. PLN (Persero) ULP PETUNG	23220	18	104	-	Aktif	\N	\N
604	PT. PLN (Persero) ULP SAMBOJA	23211	18	104	-	Aktif	\N	\N
605	PT. PLN (Persero) ULP TANAH GROGOT	23230	18	104	-	Aktif	\N	\N
606	KALTIM INDUSTRIAL ESTATE	94000	18	105	-	Aktif	\N	\N
607	PT. PLN (Persero) ULP BONTANG	23400	18	105	-	Aktif	\N	\N
608	PT. PLN (Persero) ULP MALINAU	23350	18	105	-	Aktif	\N	\N
609	PT. PLN (Persero) ULP NUNUKAN	23360	18	105	-	Aktif	\N	\N
610	PT. PLN (Persero) ULP SANGATTA	23420	18	105	-	Aktif	\N	\N
611	PT. PLN (Persero) ULP TANJUNG REDEB	23300	18	105	-	Aktif	\N	\N
612	PT. PLN (Persero) ULP TANJUNG SELOR	23320	18	105	-	Aktif	\N	\N
613	WILAYAH OPERASI PT ADARO INDONESIA	90000	19	107	-	Aktif	\N	\N
614	PT. PLN (Persero) ULP BANJARBARU	22120	19	107	-	Aktif	\N	\N
615	PT. PLN (Persero) ULP GAMBUT	22160	19	107	-	Aktif	\N	\N
616	PT. PLN (Persero) ULP KANTOR L. MANGKURAT	22100	19	107	-	Aktif	\N	\N
617	PT. PLN (Persero) ULP KANTOR UP A YANI	22110	19	107	-	Aktif	\N	\N
618	PT. PLN (Persero) ULP MARABAHAN	22150	19	107	-	Aktif	\N	\N
619	PT. PLN (Persero) ULP MARTAPURA	22130	19	107	-	Aktif	\N	\N
620	PT. PLN (Persero) ULP PELAIHARI	22140	19	107	-	Aktif	\N	\N
621	PT. PLN (Persero) ULP AMUNTAI	22230	19	108	-	Aktif	\N	\N
622	PT. PLN (Persero) ULP Barabai	22200	19	108	-	Aktif	\N	\N
623	PT. PLN (Persero) ULP Binuang	22260	19	108	-	Aktif	\N	\N
624	PT. PLN (Persero) ULP Binuang	22260	19	108	-	Aktif	\N	\N
625	PT. PLN (Persero) ULP Daha	22270	19	108	-	Aktif	\N	\N
626	PT. PLN (Persero) ULP Kandangan	22220	19	108	-	Aktif	\N	\N
627	PT. PLN (Persero) ULP Paringin	22250	19	108	-	Aktif	\N	\N
628	PT. PLN (Persero) ULP Rantau	22210	19	108	-	Aktif	\N	\N
629	PT. PLN (Persero) ULP Tanjung	22240	19	108	-	Aktif	\N	\N
630	PT. PLN (Persero) ULP BATULICIN	22320	19	109	-	Aktif	\N	\N
631	PT. PLN (Persero) ULP KOTABARU	22300	19	109	-	Aktif	\N	\N
632	PT. PLN (Persero) ULP SATUI	22330	19	109	-	Aktif	\N	\N
633	PT. PLN (Persero) ULP BALAI KARANGAN	21360	20	112	-	Aktif	\N	\N
634	PT. PLN (Persero) ULP BENGKAYANG	21230	20	113	-	Aktif	\N	\N
635	PT. PLN (Persero) ULP KAKAP	21121	20	110	-	Aktif	\N	\N
636	PT. PLN (Persero) ULP KETAPANG	21400	20	111	-	Aktif	\N	\N
637	PT. PLN (Persero) ULP KOTA	21100	20	110	-	Aktif	\N	\N
638	PT. PLN (Persero) ULP MEMPAWAH	21110	20	110	-	Aktif	\N	\N
639	PT. PLN (Persero) ULP NANGA PINOH	21380	20	112	-	Aktif	\N	\N
640	PT. PLN (Persero) ULP NGABANG	21140	20	110	-	Aktif	\N	\N
641	PT. PLN (Persero) ULP PEMANGKAT	21210	20	113	-	Aktif	\N	\N
642	PT. PLN (Persero) ULP PUTUSSIBAU	21330	20	112	-	Aktif	\N	\N
643	PT. PLN (Persero) ULP RASAU	21120	20	110	-	Aktif	\N	\N
644	PT. PLN (Persero) ULP SAMBAS	21220	20	113	-	Aktif	\N	\N
645	PT. PLN (Persero) ULP SANDAI	21420	20	111	-	Aktif	\N	\N
646	PT. PLN (Persero) ULP SANGGAU	21300	20	112	-	Aktif	\N	\N
647	PT. PLN (Persero) ULP SEI DURI	21250	20	113	-	Aktif	\N	\N
648	PT. PLN (Persero) ULP SEI JAWI	21102	20	110	-	Aktif	\N	\N
649	PT. PLN (Persero) ULP SEKADAU	21370	20	112	-	Aktif	\N	\N
650	PT. PLN (Persero) ULP SEKURA	21240	20	113	-	Aktif	\N	\N
651	PT. PLN (Persero) ULP SIANTAN	21101	20	110	-	Aktif	\N	\N
652	PT. PLN (Persero) ULP SINGKAWANG	21200	20	113	-	Aktif	\N	\N
653	PT. PLN (Persero) ULP SINTANG	21310	20	112	-	Aktif	\N	\N
654	PT. PLN (Persero) ULP SUKADANA	21410	20	111	-	Aktif	\N	\N
655	PT. PLN (Persero) ULP TUMBANG TITI	21430	20	111	-	Aktif	\N	\N
656	PT. PLN (Persero) ULP AIRMADIDI	31120	21	114	-	Aktif	\N	\N
657	PT. PLN (Persero) ULP AMURANG	31170	21	114	-	Aktif	\N	\N
658	PT. PLN (Persero) ULP BINTAUNA	31740	21	114	-	Aktif	\N	\N
659	PT. PLN (Persero) ULP BITUNG	31110	21	114	-	Aktif	\N	\N
660	PT. PLN (Persero) ULP IMANDI	31710	21	114	-	Aktif	\N	\N
661	PT. PLN (Persero) ULP INOBONTO	31720	21	114	-	Aktif	\N	\N
662	PT. PLN (Persero) ULP KANTOR CABANG KOTAMOBAGU	31760	21	114	-	Aktif	\N	\N
663	PT. PLN (Persero) ULP KAWANGKOAN	31150	21	114	-	Aktif	\N	\N
664	PT. PLN (Persero) ULP MANADO SELATAN	31102	21	114	-	Aktif	\N	\N
665	PT. PLN (Persero) ULP MANADO UTARA	31101	21	114	-	Aktif	\N	\N
666	PT. PLN (Persero) ULP MODAYAG	31730	21	114	-	Aktif	\N	\N
667	PT. PLN (Persero) ULP MOLIBAGU	31750	21	114	-	Aktif	\N	\N
668	PT. PLN (Persero) ULP MOTOLING	31180	21	114	-	Aktif	\N	\N
669	PT. PLN (Persero) ULP PANIKI	31190	21	114	-	Aktif	\N	\N
670	PT. PLN (Persero) ULP RATAHAN	31160	21	114	-	Aktif	\N	\N
671	PT. PLN (Persero) ULP SIAU	31810	21	114	-	Aktif	\N	\N
672	PT. PLN (Persero) ULP TAGULANDANG	31860	21	114	-	Aktif	\N	\N
673	PT. PLN (Persero) ULP TOMOHON	31130	21	114	-	Aktif	\N	\N
674	PT. PLN (Persero) ULP TONDANO	31140	21	114	-	Aktif	\N	\N
675	KAWASAN INDUSTRI PT. KENDARI INDUSTRIAL PRATAMA	98000	22	117	-	Aktif	\N	\N
676	PT. PLN (Persero) ULP BENU BENUA	32330	22	117	-	Aktif	\N	\N
677	PT. PLN (Persero) ULP BOMBANA	32340	22	117	-	Aktif	\N	\N
678	PT. PLN (Persero) ULP KOLAKA	32380	22	117	-	Aktif	\N	\N
679	PT. PLN (Persero) ULP KOLAKA UTARA	32350	22	117	-	Aktif	\N	\N
680	PT. PLN (Persero) ULP KONAWE SELATAN	32370	22	117	-	Aktif	\N	\N
681	PT. PLN (Persero) ULP UNAAHA	32360	22	117	-	Aktif	\N	\N
682	PT. PLN (Persero) ULP WUA WUA	32320	22	117	-	Aktif	\N	\N
683	KAWASAN INDUSTRI PT. KENDARI INDUSTRIAL PRATAMA	98000	22	117	-	Aktif	\N	\N
684	PT. PLN (Persero) ULP BENU BENUA	32330	22	117	-	Aktif	\N	\N
685	PT. PLN (Persero) ULP BOMBANA	32340	22	117	-	Aktif	\N	\N
686	PT. PLN (Persero) ULP BAUBAU KOTA	32810	22	118	Jln. Jenderal Sudiirman No.62 Baubau, Tomba	Aktif	\N	\N
687	PT. PLN (Persero) ULP BUNGKU	31211	23	119	-	Aktif	\N	\N
688	PT. PLN (Persero) ULP DONGGALA	31240	23	119	-	Aktif	\N	\N
689	PT. PLN (Persero) ULP KAMONJI	31220	23	119	-	Aktif	\N	\N
690	PT. PLN (Persero) ULP AMPANA	31930	23	120	-	Aktif	\N	\N
691	PT. PLN (Persero) ULP BANGGAI	31920	23	120	-	Aktif	\N	\N
692	PT. PLN (Persero) ULP KANTOR CABANG LUWUK	31950	23	120	-	Aktif	\N	\N
693	PT. PLN (Persero) ULP BANGKIR	31530	23	121	-	Aktif	\N	\N
694	PT. PLN (Persero) ULP BELOPA	32640	24	122	-	Aktif	\N	\N
695	PT. PLN (Persero) ULP KALEBAJENG	32141	24	123	-	Aktif	\N	\N
696	PT. PLN (Persero) ULP DAYA	32020	24	124	-	Aktif	\N	\N
697	PT. PLN (Persero) ULP BANTAENG	32740	24	125	-	Aktif	\N	\N
698	PT. PLN (Persero) ULP JENEPONTO	32730	24	125	-	Aktif	\N	\N
699	PT. PLN (Persero) ULP KALUMPANG	32720	24	125	-	Aktif	\N	\N
700	PT. PLN (Persero) ULP MATTIROTASI	32510	24	126	-	Aktif	\N	\N
701	PT. PLN (Persero) ULP PAJALESANG	32570	24	126	-	Aktif	\N	\N
702	PT. PLN (Persero) ULP BARRU	32520	24	126	-	Aktif	\N	\N
703	PT. PLN (Persero) ULP ENREKANG	32440	24	127	-	Aktif	\N	\N
704	PT. PLN (Persero) ULP HASANUDDIN	32210	24	128	-	Aktif	\N	\N
706	KAWASAN PT TUNAS SEWA ERMA	57100	25	129	-	Aktif	\N	\N
707	PT. PLN (Persero) ULP ABEPURA	42110	25	129	-	Aktif	\N	\N
708	PT. PLN (Persero) ULP ADONARA	52	25	129	-	Tidak Aktif	\N	\N
709	PT. PLN (Persero) ULP ARSO	42160	25	129	-	Aktif	\N	\N
710	PT. PLN (Persero) ULP GENYEM	42130	25	129	-	Aktif	\N	\N
711	PT. PLN (Persero) ULP JAYAPURA	42180	25	129	-	Aktif	\N	\N
712	PT. PLN (Persero) ULP AIMAS	42330	25	130	-	Aktif	\N	\N
713	PT. PLN (Persero) ULP DUM	42320	25	130	-	Aktif	\N	\N
714	PT. PLN (Persero) ULP FAKFAK	42350	25	130	-	Aktif	\N	\N
715	PT. PLN (Persero) ULP KAIMANA	42360	25	130	-	Aktif	\N	\N
716	PT. PLN (Persero) ULP BIAK	42210	25	131	-	Aktif	\N	\N
717	PT. PLN (Persero) ULP BINTUNI	42440	25	132	-	Aktif	\N	\N
19	PT. PLN (Persero) ULP LHOKSEMAUWE	11220	2	3	-	Aktif	\N	2019-03-26 15:04:07
20	PT. PLN (Persero) ULP LHOKSEMAUWE	11220	2	3	Jl. Iskandar Muda No.01, Lancang Garam, Banda Sakti, Kota 3, Aceh	Aktif	\N	2019-03-26 15:04:17
100	PT. PLN (Persero) ULP RANTAU PRAPAT KOTA	12501	3	12	-	Aktif	\N	2019-03-26 15:06:03
122	PT. PLN (Persero) ULP PEKANBARU KOTA TIMUR	18110	4	15	-	Aktif	\N	2019-03-26 15:07:19
174	PT. PLN (Persero) UP3 TELUK NAGA	56660	7	38	-	Aktif	\N	2019-03-26 15:08:19
177	PT. PLN (Persero) UP3 CIKOKOL	56610	7	41	Jln. Jenderal Sudirman (Akses By Pass) No. 1 Tangerang	Aktif	\N	2019-03-26 15:09:00
212	PT. PLN (Persero) ULP BANDUNG TIMUR	53563	9	44	-	Aktif	\N	2019-03-26 15:10:35
242	PT. PLN (Persero) ULP BOGOR BARAT	53841	9	45	-	Aktif	\N	2019-03-26 15:10:55
243	PT. PLN (Persero) ULP BOGOR KOTA	53831	9	45	-	Aktif	\N	2019-03-26 15:11:01
298	PT. PLN (Persero) ULP PRIMA KARAWANG (TT/TM)	53476	9	53	-	Aktif	\N	2019-03-26 15:12:05
345	PT. PLN (Persero) ULP CILACAP KOTA	52501	10	62	-	Aktif	\N	2019-03-26 15:12:47
357	PT. PLN (Persero) ULP KLATEN KOTA	52404	10	64	-	Aktif	\N	2019-03-26 15:12:58
\.


--
-- Name: penyedia_listrik_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('penyedia_listrik_id_seq', 1, false);


--
-- Data for Name: perangkat; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY perangkat (id, id_users, username, token, created_at, updated_at) FROM stdin;
58	47	issam	frl0wMUXWZw:APA91bGChCYAMM7Qw-ukts9XsZ9uSI0kKSTGgdQqsn23Uc3CVEt3UXoTI3zcbTIZFzWg8o8t4MNTbpKF_rGJ0lcxNNBtClS5qXVSN0EdDqexela6gUjoFJJCJFnqIPoNyHp0eboE1fMO	2019-08-20 23:45:25	2019-08-20 23:45:25
59	26	jabar	eYfq13CuiI4:APA91bEaxkLypcjyVHhnxQI-pfzRNhELm6AaJiKIEIWIL5E9r89D0nQYKK34y-woapD6_IVozmKRnmY6ENYIUVoa87DMrUWB7WY0UGrN0IeHCNp-tPUsV4FINOEzJBchyjeq0L9YTWMJ	2019-10-28 06:50:33	2019-10-28 06:50:33
60	6	putra	dPvSeglASmU:APA91bGdMo08ILphaP72EXnQ7mvT-mr5biclem46vJWqwBHfJWcgCVho6NhUP_5H1Di9bumCCe2Vy8fTxRxngcO2dwrPMb0sX68AqLftjBE-kB_barDZ8ubawzrl3cUR3VoW8Fns8imZ	2019-11-19 23:59:51	2019-11-19 23:59:51
61	51	feri1	eNWI3HtcOKU:APA91bH2gmaEXpo8k9VT3O2VGz5rLGk5xQzkRR5kUQEWyCC7msMVo-oL97ldJgRfhVmPZxKCilN9NEqoXZ5DxvqfdZYCwr7Dn0mfGVUwREKsrRT6ilfLkOsHONyzFMoSTnhdZhAN5zl6	2020-01-05 23:32:05	2020-01-05 23:32:05
62	13	dios	eNWI3HtcOKU:APA91bH2gmaEXpo8k9VT3O2VGz5rLGk5xQzkRR5kUQEWyCC7msMVo-oL97ldJgRfhVmPZxKCilN9NEqoXZ5DxvqfdZYCwr7Dn0mfGVUwREKsrRT6ilfLkOsHONyzFMoSTnhdZhAN5zl6	2020-01-05 23:35:26	2020-01-05 23:35:26
63	4	wahyu	cEatoWT1dWE:APA91bGPM3xXeLXY8twWFJcRUeELpkEsv0vRwiSUQK777WjpMkg-J_ZWvAzGIkdbEeKD4QxrCPwCb84m-m1_WfwfmhR4xu88ESrYj5WRB9SKqSQzer9uTXLfVNrexpqufFy-NMNxBBw6	2020-01-07 10:58:41	2020-01-07 10:58:41
64	53	ekapranata	cEatoWT1dWE:APA91bGPM3xXeLXY8twWFJcRUeELpkEsv0vRwiSUQK777WjpMkg-J_ZWvAzGIkdbEeKD4QxrCPwCb84m-m1_WfwfmhR4xu88ESrYj5WRB9SKqSQzer9uTXLfVNrexpqufFy-NMNxBBw6	2020-01-07 11:08:04	2020-01-07 11:08:04
65	1	sipp	djzWZLCIHYA:APA91bFQ-B557-YSS_u8r5LoNYbRcG8yWD15jE0wdDWfG9tUUbx11Oiv0XST-WEPggi38g3Kxuanihm9YAUPOnMqzcZAdxMsZNLMVPHo4syP3SZLuqSNlTnvdd8_OMwEHktRNL-6vpFE	2020-01-14 11:54:50	2020-01-14 11:54:50
66	27	jateng	djzWZLCIHYA:APA91bFQ-B557-YSS_u8r5LoNYbRcG8yWD15jE0wdDWfG9tUUbx11Oiv0XST-WEPggi38g3Kxuanihm9YAUPOnMqzcZAdxMsZNLMVPHo4syP3SZLuqSNlTnvdd8_OMwEHktRNL-6vpFE	2020-01-14 11:56:14	2020-01-14 11:56:14
\.


--
-- Name: perangkat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('perangkat_id_seq', 66, true);


--
-- Data for Name: provinsi; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY provinsi (id, kode_prov, provinsi, created_at, updated_at) FROM stdin;
1	11	ACEH	2019-01-22 04:20:14	2019-01-22 04:20:14
2	12	SUMATERA UTARA	2019-01-22 04:20:14	2019-01-22 04:20:14
3	13	SUMATERA BARAT	2019-01-22 04:20:14	2019-01-22 04:20:14
4	14	RIAU	2019-01-22 04:20:14	2019-01-22 04:20:14
5	15	JAMBI	2019-01-22 04:20:14	2019-01-22 04:20:14
6	16	SUMATERA SELATAN	2019-01-22 04:20:14	2019-01-22 04:20:14
7	17	BENGKULU	2019-01-22 04:20:14	2019-01-22 04:20:14
8	18	LAMPUNG	2019-01-22 04:20:14	2019-01-22 04:20:14
9	19	KEPULAUAN BANGKA BELITUNG	2019-01-22 04:20:14	2019-01-22 04:20:14
10	21	KEPULAUAN RIAU	2019-01-22 04:20:14	2019-01-22 04:20:14
11	31	DKI JAKARTA	2019-01-22 04:20:14	2019-01-22 04:20:14
12	32	JAWA BARAT	2019-01-22 04:20:14	2019-01-22 04:20:14
13	33	JAWA TENGAH	2019-01-22 04:20:14	2019-01-22 04:20:14
14	34	DI YOGYAKARTA	2019-01-22 04:20:14	2019-01-22 04:20:14
15	35	JAWA TIMUR	2019-01-22 04:20:14	2019-01-22 04:20:14
16	36	BANTEN	2019-01-22 04:20:14	2019-01-22 04:20:14
17	51	BALI	2019-01-22 04:20:14	2019-01-22 04:20:14
18	52	NUSA TENGGARA BARAT	2019-01-22 04:20:14	2019-01-22 04:20:14
19	53	NUSA TENGGARA TIMUR	2019-01-22 04:20:14	2019-01-22 04:20:14
20	61	KALIMANTAN BARAT	2019-01-22 04:20:14	2019-01-22 04:20:14
21	62	KALIMANTAN TENGAH	2019-01-22 04:20:14	2019-01-22 04:20:14
22	63	KALIMANTAN SELATAN	2019-01-22 04:20:14	2019-01-22 04:20:14
23	64	KALIMANTAN TIMUR	2019-01-22 04:20:14	2019-01-22 04:20:14
24	65	KALIMANTAN UTARA	2019-01-22 04:20:14	2019-01-22 04:20:14
25	71	SULAWESI UTARA	2019-01-22 04:20:14	2019-01-22 04:20:14
26	72	SULAWESI TENGAH	2019-01-22 04:20:14	2019-01-22 04:20:14
27	73	SULAWESI SELATAN	2019-01-22 04:20:14	2019-01-22 04:20:14
28	74	SULAWESI TENGGARA	2019-01-22 04:20:14	2019-01-22 04:20:14
29	75	GORONTALO	2019-01-22 04:20:14	2019-01-22 04:20:14
30	76	SULAWESI BARAT	2019-01-22 04:20:14	2019-01-22 04:20:14
31	81	MALUKU	2019-01-22 04:20:14	2019-01-22 04:20:14
32	82	MALUKU UTARA	2019-01-22 04:20:14	2019-01-22 04:20:14
33	91	PAPUA BARAT	2019-01-22 04:20:14	2019-01-22 04:20:14
34	94	PAPUA	2019-01-22 04:20:14	2019-01-22 04:20:14
\.


--
-- Name: provinsi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('provinsi_id_seq', 1, true);


--
-- Data for Name: rekap_area; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY rekap_area (id, id_kwilayah, id_karea, bulan, pendapatan_pelanggan, pendapatan_sub_area, total, created_at, updated_at) FROM stdin;
2	11	70	2019-04-18	198000	14080000	14278000	2019-04-18 11:40:29	2019-04-18 11:40:29
3	10	65	2019-04-18	347500	30410000	30757500	2019-04-18 11:47:40	2019-04-18 11:47:40
4	10	59	2019-08-20	190000	\N	190000	2019-08-20 21:50:53	2019-08-20 21:50:53
\.


--
-- Name: rekap_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('rekap_area_id_seq', 4, true);


--
-- Data for Name: rekap_pusat; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY rekap_pusat (id, periode, total_penerimaan, created_at, updated_at) FROM stdin;
1	2019-04-18	15000000	2019-04-18 12:36:16	2019-04-18 12:48:38
2	2019-08-20	10000000	2019-08-20 22:28:12	2019-08-20 22:28:12
\.


--
-- Name: rekap_pusat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('rekap_pusat_id_seq', 2, true);


--
-- Data for Name: rekap_sub_area; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY rekap_sub_area (id, id_karea, id_subarea, bulan, total, created_at, updated_at) FROM stdin;
1	65	324	2019-04-18	30410000	2019-04-18 11:24:30	2019-04-18 11:24:30
2	70	353	2019-04-18	14080000	2019-04-18 11:32:50	2019-04-18 11:32:50
\.


--
-- Name: rekap_sub_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('rekap_sub_area_id_seq', 2, true);


--
-- Data for Name: rekap_wilayah; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY rekap_wilayah (id, id_kwilayah, periode, total_penerimaan, created_at, updated_at) FROM stdin;
1	10	2019-04-18	20000000	2019-04-18 12:28:27	2019-04-18 12:28:27
2	11	2019-04-18	10000000	2019-04-18 12:30:45	2019-04-18 12:30:45
\.


--
-- Name: rekap_wilayah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('rekap_wilayah_id_seq', 2, true);


--
-- Data for Name: runingtext_frontend; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY runingtext_frontend (id, created_at, updated_at, judul, isi) FROM stdin;
\.


--
-- Name: runingtext_frontend_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('runingtext_frontend_id_seq', 10, true);


--
-- Data for Name: sertifikasi; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY sertifikasi (id, no_pendaftaran, no_agenda, no_lhpp, no_registrasi, no_sertifikat, no_seri, pjt_cetak, tt_cetak, tgl_cetak, masa_berlaku, penanda_tangan, dicetak_oleh, created_at, updated_at) FROM stdin;
1	SIP-0419-010-065-0002	BR002	LHI-0419-010-004-0002	B191589618975	8UA8.628.7.3322.JM19.19	1	5	6	2019-04-23	2034-04-23	Sari Isyana P, ST, MM	1	2019-04-23 11:52:24	2019-04-23 11:52:24
2	SIP-0419-010-065-0001	BR001	LHI-0419-010-004-0001	B191589618975	8UA8.628.7.3322.JM19.19	2	5	6	2019-04-23	2034-04-23	Sari Isyana P, ST, MM	1	2019-04-23 11:52:43	2019-04-23 11:52:43
3	SIP-0819-010-059-0007	BR008	LHI-0819-010-047-0003	B191589618975	8UA8.628.7.3322.JM19.19	454545	51	47	2019-08-20	2034-08-20	Sari Isyana P, ST, MM	48	2019-08-20 21:17:39	2019-08-20 21:17:39
4	SIP-0819-010-059-0009	BR010	LHI-0819-010-047-0004	B191589618975	8UA8.628.7.3322.JM19.19	23423	51	47	2019-08-21	2034-08-21	Sari Isyana P, ST, MM	1	2019-08-21 14:24:06	2019-08-21 14:24:06
5	SIP-1019-010-059-0010	BR011	LHI-1019-010-047-0005	B191589618975	8UA8.628.7.3322.JM19.19	8787	51	47	2019-10-23	2034-10-23	Sari Isyana P, ST, MM	49	2019-10-23 09:09:47	2019-10-23 09:09:47
6	SIP-0819-010-059-0006	BR007	LHI-0120-010-047-0006	B191589618975	8UA8.628.7.3322.JM19.19	898989	51	47	2020-01-10	2035-01-10	Sari Isyana P, ST, MM	48	2020-01-10 02:27:43	2020-01-10 02:27:43
\.


--
-- Name: sertifikasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('sertifikasi_id_seq', 6, true);


--
-- Data for Name: setor_area; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY setor_area (id, id_tagihan, no_ref, jml_setoran, id_users, status, created_at, updated_at) FROM stdin;
1	3	KA9069	20000000	7	1	2019-04-18 12:24:14	2019-04-18 12:24:21
2	2	KA2605	10000000	12	1	2019-04-18 12:30:21	2019-04-18 12:30:40
3	2	KA2605	2000000	12	1	2019-04-18 13:19:08	2019-04-18 13:19:56
5	4	KA8460	190000	49	1	2019-08-20 21:56:19	2019-08-20 22:24:56
4	4	KA8460	90000	49	1	2019-08-20 21:52:34	2019-08-21 04:52:53
\.


--
-- Name: setor_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('setor_area_id_seq', 5, true);


--
-- Data for Name: setor_sub_area; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY setor_sub_area (id, id_tagihan, no_ref, jml_setoran, id_users, status, created_at, updated_at) FROM stdin;
1	1	SA9557	20000000	16	1	2019-04-18 11:50:19	2019-04-18 11:50:26
2	2	SA3064	10000000	17	1	2019-04-18 12:21:44	2019-04-18 12:21:49
3	2	SA3064	2000000	17	1	2019-04-18 13:14:59	2019-04-18 13:17:18
\.


--
-- Name: setor_sub_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('setor_sub_area_id_seq', 3, true);


--
-- Data for Name: setor_wilayah; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY setor_wilayah (id, no_ref, id_tagihan, jml_setoran, id_users, status, created_at, updated_at) FROM stdin;
1	WL6923	2	10000000	8	1	2019-04-18 12:36:04	2019-04-18 12:36:09
2	WL9982	3	5000000	9	1	2019-04-18 12:47:08	2019-04-18 12:48:30
3	WL6923	2	10000000	48	1	2019-08-20 22:26:46	2019-08-20 22:27:20
\.


--
-- Name: setor_wilayah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('setor_wilayah_id_seq', 3, true);


--
-- Data for Name: share_internal; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY share_internal (id, kode_alokasi, periode, nama, persen, jumlah, created_at, updated_at) FROM stdin;
1	S001	2019-04-23	Azza	40.00	760.00	\N	\N
2	S001	2019-04-23	Pak Kastono	20.00	380.00	\N	\N
3	S001	2019-04-23	Doni	20.00	380.00	\N	\N
4	S001	2019-04-23	Wahyu	20.00	380.00	\N	\N
\.


--
-- Name: share_internal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('share_internal_id_seq', 1, false);


--
-- Data for Name: share_wilayah; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY share_wilayah (id, id_kwilayah, share, created_at, updated_at) FROM stdin;
1	2	10	\N	\N
2	3	10	\N	\N
3	4	10	\N	\N
4	5	10	\N	\N
5	6	10	\N	\N
6	7	10	\N	\N
7	8	10	\N	\N
8	9	10	\N	\N
11	12	10	\N	\N
12	13	10	\N	\N
13	14	10	\N	\N
14	15	10	\N	\N
15	16	10	\N	\N
16	17	10	\N	\N
17	18	10	\N	\N
18	19	10	\N	\N
19	20	10	\N	\N
20	21	10	\N	\N
21	22	10	\N	\N
22	23	10	\N	\N
23	24	10	\N	\N
24	25	10	\N	\N
9	10	40	\N	2019-04-16 14:36:50
10	11	10	\N	2019-04-19 17:41:11
\.


--
-- Name: share_wilayah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('share_wilayah_id_seq', 1, false);


--
-- Data for Name: slideshow; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY slideshow (id, created_at, updated_at, judul, gambar) FROM stdin;
2	2019-03-05 22:21:53	2019-03-05 22:21:53	SIP	620511412.png
3	2019-04-30 22:21:53	2019-04-30 22:21:53	Slide 1	slide1.jpg
4	2019-04-30 22:21:53	2019-04-30 22:21:53	Slide 2	slide2.jpg
5	2019-04-30 22:21:53	2019-04-30 22:21:53	Slide 3	slide3.jpg
\.


--
-- Name: slideshow_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('slideshow_id_seq', 12, true);


--
-- Data for Name: staff_btl; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY staff_btl (id, id_users, id_btl, created_at, updated_at) FROM stdin;
1	15	952	2019-03-27 20:55:29	2019-03-27 20:55:29
\.


--
-- Name: staff_btl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('staff_btl_id_seq', 1, true);


--
-- Data for Name: sub_area; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY sub_area (id, id_kwilayah, id_karea, nama_subarea, kode, alamat, nama_manager, status, created_at, updated_at) FROM stdin;
1	2	1	Kota Merduati	1	-	-	Aktif	\N	\N
2	2	1	Keude Bieng	2	-	-	Aktif	\N	\N
3	2	1	Lambaro	3	-	-	Aktif	\N	\N
4	2	1	Jantho	4	-	-	Aktif	\N	\N
5	2	1	Sabang	5	-	-	Aktif	\N	\N
6	2	2	Kota Langsa	6	-	-	Aktif	\N	\N
7	2	2	Peureulak	7	-	-	Aktif	\N	\N
8	2	2	Idi	8	-	-	Aktif	\N	\N
9	2	2	Kuala Simpang	9	-	-	Aktif	\N	\N
10	2	2	Kuta Cane	10	-	-	Aktif	\N	\N
11	2	2	Blang Kejeren	11	-	-	Aktif	\N	\N
12	2	3	Kota Lhokseumawe	12	-	-	Aktif	\N	\N
13	2	3	Geudong	13	-	-	Aktif	\N	\N
14	2	3	Lhoksukon	14	-	-	Aktif	\N	\N
15	2	3	Panton Labu	15	-	-	Aktif	\N	\N
16	2	3	Krueng Geukuh	16	-	-	Aktif	\N	\N
17	2	3	Ganda Pura	17	-	-	Aktif	\N	\N
18	2	3	Matang Geulumpang Dua	18	-	-	Aktif	\N	\N
19	2	3	Bireun	19	-	-	Aktif	\N	\N
20	2	3	Samalanga	20	-	-	Aktif	\N	\N
21	2	3	Janarata	21	-	-	Aktif	\N	\N
22	2	3	Takengon	22	-	-	Aktif	\N	\N
23	2	4	Kota Meulaboh	23	-	-	Aktif	\N	\N
24	2	4	Jeuram	24	-	-	Aktif	\N	\N
25	2	4	Teunom	25	-	-	Aktif	\N	\N
26	2	4	Calang	26	-	-	Aktif	\N	\N
27	2	4	Sinabang	27	-	-	Aktif	\N	\N
28	2	5	Kota Sigli	28	-	-	Aktif	\N	\N
29	2	5	Beureunuen	29	-	-	Aktif	\N	\N
30	2	5	Meureudu	30	-	-	Aktif	\N	\N
31	2	6	Kota Sabulussalam	31	-	-	Aktif	\N	\N
32	2	6	Singkil	32	-	-	Aktif	\N	\N
33	2	6	Rimo	33	-	-	Aktif	\N	\N
34	2	6	Kota Fajar	34	-	-	Aktif	\N	\N
35	2	6	Tapak Tuan	35	-	-	Aktif	\N	\N
36	2	6	Labuhan Haji	36	-	-	Aktif	\N	\N
37	2	6	Blangpidie	37	-	-	Aktif	\N	\N
38	3	7	Kota Medan	38	-	-	Aktif	\N	\N
39	3	7	Medan Baru	39	-	-	Aktif	\N	\N
40	3	7	Medan Selatan	40	-	-	Aktif	\N	\N
41	3	7	Sunggal	41	-	-	Aktif	\N	\N
42	3	7	Medan Timur	42	-	-	Aktif	\N	\N
43	3	7	Belawan	43	-	-	Aktif	\N	\N
44	3	7	Labuhan	44	-	-	Aktif	\N	\N
45	3	7	Helvetia	45	-	-	Aktif	\N	\N
46	3	7	Johor	46	-	-	Aktif	\N	\N
47	3	8	Kota Binjai	47	-	-	Aktif	\N	\N
48	3	8	Binjai Timur	48	-	-	Aktif	\N	\N
49	3	8	Binjai Barat	49	-	-	Aktif	\N	\N
50	3	8	Stabat	50	-	-	Aktif	\N	\N
51	3	8	Kuala	51	-	-	Aktif	\N	\N
52	3	8	Tanjung Pura	52	-	-	Aktif	\N	\N
53	3	8	Gebang	53	-	-	Aktif	\N	\N
54	3	8	Pangkalan Brandan	54	-	-	Aktif	\N	\N
55	3	8	Pangkalan Susu	55	-	-	Aktif	\N	\N
56	3	8	Brastagi	56	-	-	Aktif	\N	\N
57	3	8	Kabanjahe	57	-	-	Aktif	\N	\N
58	3	8	Tiga Binanga	58	-	-	Aktif	\N	\N
59	3	8	Sidikalang	59	-	-	Aktif	\N	\N
60	3	9	Deli Tua	60	-	-	Aktif	\N	\N
61	3	9	Medan Denai	61	-	-	Aktif	\N	\N
62	3	9	Pancur Batu	62	-	-	Aktif	\N	\N
63	3	9	Tanjung Morawa	63	-	-	Aktif	\N	\N
64	3	9	Lubuk Pakam	64	-	-	Aktif	\N	\N
65	3	9	Galang	65	-	-	Aktif	\N	\N
66	3	9	Perbaungan	66	-	-	Aktif	\N	\N
67	3	9	Sei Rampah	67	-	-	Aktif	\N	\N
68	3	10	Kota Padang Sidempuan	68	-	-	Aktif	\N	\N
69	3	10	Gunung Tua	69	-	-	Aktif	\N	\N
70	3	10	Sipirok	70	-	-	Aktif	\N	\N
71	3	10	Sibuhuan	71	-	-	Aktif	\N	\N
72	3	10	Panyabungan	72	-	-	Aktif	\N	\N
73	3	10	Natal	73	-	-	Aktif	\N	\N
74	3	10	Kotanopan	74	-	-	Aktif	\N	\N
75	3	11	Kota Pematang Siantar	75	-	-	Aktif	\N	\N
76	3	11	Tanah Jawa	76	-	-	Aktif	\N	\N
77	3	11	Sidamanik	77	-	-	Aktif	\N	\N
78	3	11	Parapat	78	-	-	Aktif	\N	\N
79	3	11	Tebing Tinggi	79	-	-	Aktif	\N	\N
80	3	11	Dolok Masihul	80	-	-	Aktif	\N	\N
81	3	11	Indrapura	81	-	-	Aktif	\N	\N
82	3	11	Perdagangan	82	-	-	Aktif	\N	\N
83	3	11	Tanjung Tiram	83	-	-	Aktif	\N	\N
84	3	11	Kisaran	84	-	-	Aktif	\N	\N
85	3	11	Pangururan	85	-	-	Aktif	\N	\N
86	3	12	Kota Rantau Prapat	86	-	-	Aktif	\N	\N
87	3	12	Aek Nabara	87	-	-	Aktif	\N	\N
88	3	12	Aek Kota Batu	88	-	-	Aktif	\N	\N
89	3	12	Aek Kanopan	89	-	-	Aktif	\N	\N
90	3	12	Tanjung Balai	90	-	-	Aktif	\N	\N
91	3	12	Kota Pinang	91	-	-	Aktif	\N	\N
92	3	12	Labuhan Bilik	92	-	-	Aktif	\N	\N
93	3	13	Kota Sibolga	93	-	-	Aktif	\N	\N
94	3	13	Porsea	94	-	-	Aktif	\N	\N
95	3	13	Balige	95	-	-	Aktif	\N	\N
96	3	13	Tarutung	96	-	-	Aktif	\N	\N
97	3	13	Dolok Sanggul	97	-	-	Aktif	\N	\N
98	3	13	Siborong-borong	98	-	-	Aktif	\N	\N
99	3	13	Barus	99	-	-	Aktif	\N	\N
100	3	14	Gunung Sitoli	100	-	-	Aktif	\N	\N
101	3	14	Teluk Dalam	101	-	-	Aktif	\N	\N
102	4	15	Kota Barat	102	-	-	Aktif	\N	\N
103	4	15	Kota Timur	103	-	-	Aktif	\N	\N
104	4	15	Simpang Tiga	104	-	-	Aktif	\N	\N
105	4	15	Panam	105	-	-	Aktif	\N	\N
106	4	15	Rumbai	106	-	-	Aktif	\N	\N
107	4	15	Perawang	107	-	-	Aktif	\N	\N
108	4	15	Siak Sri Indrapura	108	-	-	Aktif	\N	\N
109	4	15	Pangkalan Kerinci	109	-	-	Aktif	\N	\N
110	4	15	Bangkinang	110	-	-	Aktif	\N	\N
111	4	15	Pasir Pangaraian	111	-	-	Aktif	\N	\N
112	4	16	Kota Dumai	112	-	-	Aktif	\N	\N
113	4	16	Duri	113	-	-	Aktif	\N	\N
114	4	16	Bagan Batu	114	-	-	Aktif	\N	\N
115	4	16	Bagan Siapi-api	115	-	-	Aktif	\N	\N
116	4	16	Selat Panjang	116	-	-	Aktif	\N	\N
117	4	16	Bengkalis	117	-	-	Aktif	\N	\N
118	4	17	Kota Tanjung Pinang	118	-	-	Aktif	\N	\N
119	4	17	Tanjung Balai Karimun	119	-	-	Aktif	\N	\N
120	4	17	Bintan Center	120	-	-	Aktif	\N	\N
121	4	17	Kijang	121	-	-	Aktif	\N	\N
122	4	17	Tanjung Batu	122	-	-	Aktif	\N	\N
123	4	17	Belakang Padang	123	-	-	Aktif	\N	\N
124	4	17	Dabo Singkep	124	-	-	Aktif	\N	\N
125	4	17	Tanjung Uban	125	-	-	Aktif	\N	\N
126	4	17	Ranai	126	-	-	Aktif	\N	\N
127	4	17	Anambas	127	-	-	Aktif	\N	\N
128	4	18	Teluk Kuantan	128	-	-	Aktif	\N	\N
129	4	18	Tembilahan	129	-	-	Aktif	\N	\N
130	4	18	Rengat	130	-	-	Aktif	\N	\N
131	4	18	Air Molek	131	-	-	Aktif	\N	\N
132	4	18	Kuala Enok	132	-	-	Aktif	\N	\N
133	5	21	Telanaipura	133	-	-	Aktif	\N	\N
134	5	21	Kota Baru	134	-	-	Aktif	\N	\N
135	5	21	Seberang Kota	135	-	-	Aktif	\N	\N
136	5	21	Muara Bulian	136	-	-	Aktif	\N	\N
137	5	21	Muara Sabak	137	-	-	Aktif	\N	\N
138	5	21	Kuala Tungkal	138	-	-	Aktif	\N	\N
139	5	22	Bungo Kota	139	-	-	Aktif	\N	\N
140	5	22	Bangko	140	-	-	Aktif	\N	\N
141	5	22	Sarolangun	141	-	-	Aktif	\N	\N
142	5	22	Rimbo Bujang	142	-	-	Aktif	\N	\N
143	6	19	Rivai	143	-	-	Aktif	\N	\N
144	6	19	Sukarami	144	-	-	Aktif	\N	\N
145	6	19	Ampera	145	-	-	Aktif	\N	\N
146	6	19	Kenten	146	-	-	Aktif	\N	\N
147	6	19	Mariana	147	-	-	Aktif	\N	\N
148	6	19	Indralaya	148	-	-	Aktif	\N	\N
149	6	19	Kayu Agung	149	-	-	Aktif	\N	\N
150	6	19	Pangkalan Balai	150	-	-	Aktif	\N	\N
151	6	19	Sekayu	151	-	-	Aktif	\N	\N
152	6	19	Tugu Mulyo	152	-	-	Aktif	\N	\N
153	6	20	Lembayung	153	-	-	Aktif	\N	\N
154	6	20	Muara Enim	154	-	-	Aktif	\N	\N
155	6	20	Prabumulih	155	-	-	Aktif	\N	\N
156	6	20	Pendopo	156	-	-	Aktif	\N	\N
157	6	20	Pagar Alam	157	-	-	Aktif	\N	\N
158	6	20	Baturaja	158	-	-	Aktif	\N	\N
159	6	20	Muara Beliti	159	-	-	Aktif	\N	\N
160	6	20	Martapura	160	-	-	Aktif	\N	\N
161	6	20	Muara Dua	161	-	-	Aktif	\N	\N
162	6	20	Lubuk Linggau	162	-	-	Aktif	\N	\N
163	6	20	Tebing Tinggi	163	-	-	Aktif	\N	\N
164	7	38	Teluk Naga	164	-	-	Aktif	\N	\N
165	7	39	Serpong	165	-	-	Aktif	\N	\N
166	7	40	Cikupa	166	-	-	Aktif	\N	\N
167	7	41	Cikokol	167	-	-	Aktif	\N	\N
168	7	42	Prima	168	-	-	Aktif	\N	\N
169	7	42	Cikande	169	-	-	Aktif	\N	\N
170	7	42	Anyer	170	-	-	Aktif	\N	\N
171	7	42	Serang	171	-	-	Aktif	\N	\N
172	7	42	Cilegon	172	-	-	Aktif	\N	\N
173	7	43	Rangkasbitung	173	-	-	Aktif	\N	\N
174	7	43	Pandeglang	174	-	-	Aktif	\N	\N
175	7	43	Malingping	175	-	-	Aktif	\N	\N
176	7	43	Labuan	176	-	-	Aktif	\N	\N
177	8	23	Menteng	177	-	-	Aktif	\N	\N
178	8	24	Kebon Jeruk	178	-	-	Aktif	\N	\N
179	8	25	Bintaro	179	-	-	Aktif	\N	\N
180	8	26	Ciputat	180	-	-	Aktif	\N	\N
181	8	27	Cempaka Putih	181	-	-	Aktif	\N	\N
182	8	28	Bandengan	182	-	-	Aktif	\N	\N
183	8	29	Jatinegara	183	-	-	Aktif	\N	\N
184	8	30	Tanjung Priok	184	-	-	Aktif	\N	\N
185	8	31	Cengkareng	185	-	-	Aktif	\N	\N
186	8	32	Marunda	186	-	-	Aktif	\N	\N
187	8	33	Pondok Kopi	187	-	-	Aktif	\N	\N
188	8	34	Kramat Jati	188	-	-	Aktif	\N	\N
189	8	35	Lenteng Agung	189	-	-	Aktif	\N	\N
190	8	36	Ciracas	190	-	-	Aktif	\N	\N
191	8	37	Pondok Gede	191	-	-	Aktif	\N	\N
192	9	44	Prima Priangan	192	-	-	Aktif	\N	\N
193	9	44	Bandung Barat	193	-	-	Aktif	\N	\N
194	9	44	Bandung Selatan	194	-	-	Aktif	\N	\N
195	9	44	Bandung Utara	195	-	-	Aktif	\N	\N
196	9	44	Bandung Timur	196	-	-	Aktif	\N	\N
197	9	44	Cijawura	197	-	-	Aktif	\N	\N
198	9	44	Kopo	198	-	-	Aktif	\N	\N
199	9	44	Ujung Berung	199	-	-	Aktif	\N	\N
200	9	45	Prima Pakuan	200	-	-	Aktif	\N	\N
201	9	45	Bogor Timur	201	-	-	Aktif	\N	\N
202	9	45	Bogor Kota	202	-	-	Aktif	\N	\N
203	9	45	Bogor Barat	203	-	-	Aktif	\N	\N
204	9	45	Cipayung	204	-	-	Aktif	\N	\N
205	9	45	Leuwiliang	205	-	-	Aktif	\N	\N
206	9	45	Jasinga	206	-	-	Aktif	\N	\N
207	9	46	Prima	207	-	-	Aktif	\N	\N
208	9	46	Bekasi Kota	208	-	-	Aktif	\N	\N
209	9	46	Cikarang	209	-	-	Aktif	\N	\N
210	9	46	Medan Satria	210	-	-	Aktif	\N	\N
211	9	46	Bantar Gebang	211	-	-	Aktif	\N	\N
212	9	46	Cibitung	212	-	-	Aktif	\N	\N
213	9	46	Lemah Abang	213	-	-	Aktif	\N	\N
214	9	46	Mustika Jaya	214	-	-	Aktif	\N	\N
215	9	46	Tambun	215	-	-	Aktif	\N	\N
216	9	46	Babelan	216	-	-	Aktif	\N	\N
217	9	47	Kota Cianjur	217	-	-	Aktif	\N	\N
218	9	47	Cipanas	218	-	-	Aktif	\N	\N
219	9	47	Mande	219	-	-	Aktif	\N	\N
220	9	47	Sukanagara	220	-	-	Aktif	\N	\N
221	9	47	Tanggeung	221	-	-	Aktif	\N	\N
222	9	48	Prima	222	-	-	Aktif	\N	\N
223	9	48	Cimahi Kota	223	-	-	Aktif	\N	\N
224	9	48	Cimahi Selatan	224	-	-	Aktif	\N	\N
225	9	48	Padalarang	225	-	-	Aktif	\N	\N
226	9	48	Lembang	226	-	-	Aktif	\N	\N
227	9	48	Cililin	227	-	-	Aktif	\N	\N
228	9	48	Rajamandala	228	-	-	Aktif	\N	\N
229	9	49	Kota Cirebon	229	-	-	Aktif	\N	\N
230	9	49	Kuningan	230	-	-	Aktif	\N	\N
231	9	49	Indramayu	231	-	-	Aktif	\N	\N
232	9	49	Sumber	232	-	-	Aktif	\N	\N
233	9	49	Jatibarang	233	-	-	Aktif	\N	\N
234	9	49	Ciledug	234	-	-	Aktif	\N	\N
235	9	49	Haurgeulis	235	-	-	Aktif	\N	\N
236	9	49	Cilimus	236	-	-	Aktif	\N	\N
237	9	50	Kota Depok	237	-	-	Aktif	\N	\N
238	9	50	Cibinong	238	-	-	Aktif	\N	\N
239	9	50	Sawangan	239	-	-	Aktif	\N	\N
240	9	50	Cimanggis	240	-	-	Aktif	\N	\N
241	9	50	Bojonggede	241	-	-	Aktif	\N	\N
242	9	51	Citeureup	242	-	-	Aktif	\N	\N
243	9	51	Cileungsi	243	-	-	Aktif	\N	\N
244	9	51	Jonggol	244	-	-	Aktif	\N	\N
245	9	52	Kota Garut	245	-	-	Aktif	\N	\N
246	9	52	Cibatu	246	-	-	Aktif	\N	\N
247	9	52	Leles	247	-	-	Aktif	\N	\N
248	9	52	Cikajang	248	-	-	Aktif	\N	\N
249	9	52	Pameungkeuk	249	-	-	Aktif	\N	\N
250	9	53	Prima	250	-	-	Aktif	\N	\N
251	9	53	Cikampek	251	-	-	Aktif	\N	\N
252	9	53	Kosambi	252	-	-	Aktif	\N	\N
253	9	53	Kota Karawang	253	-	-	Aktif	\N	\N
254	9	53	Rengasdengklok	254	-	-	Aktif	\N	\N
255	9	54	Prima	255	-	-	Aktif	\N	\N
256	9	54	Bale Endah	256	-	-	Aktif	\N	\N
257	9	54	Rancaekek	257	-	-	Aktif	\N	\N
258	9	54	Majalaya	258	-	-	Aktif	\N	\N
259	9	54	Soreang	259	-	-	Aktif	\N	\N
260	9	54	Banjaran	260	-	-	Aktif	\N	\N
261	9	55	Kota Purwakarta	261	-	-	Aktif	\N	\N
262	9	55	Subang	262	-	-	Aktif	\N	\N
263	9	55	Pagaden	263	-	-	Aktif	\N	\N
264	9	55	Pamanukan	264	-	-	Aktif	\N	\N
265	9	55	Plered	265	-	-	Aktif	\N	\N
266	9	56	Kota Sukabumi	266	-	-	Aktif	\N	\N
267	9	56	Cibadak	267	-	-	Aktif	\N	\N
268	9	56	Cikembar	268	-	-	Aktif	\N	\N
269	9	56	Cicurug	269	-	-	Aktif	\N	\N
270	9	56	Pelabuhan Ratu	270	-	-	Aktif	\N	\N
271	9	56	Sukaraja	271	-	-	Aktif	\N	\N
272	9	57	Kota Sumedang	272	-	-	Aktif	\N	\N
273	9	57	Tanjungsari	273	-	-	Aktif	\N	\N
274	9	57	Majalengka	274	-	-	Aktif	\N	\N
275	9	57	Jatiwangi	275	-	-	Aktif	\N	\N
276	9	58	Kota Tasikmalaya	276	-	-	Aktif	\N	\N
277	9	58	Kota Banjar	277	-	-	Aktif	\N	\N
278	9	58	Ciamis	278	-	-	Aktif	\N	\N
279	9	58	Pangandaran	279	-	-	Aktif	\N	\N
280	9	58	Rajapolah	280	-	-	Aktif	\N	\N
281	9	58	Singaparna	281	-	-	Aktif	\N	\N
282	9	58	Karangnunggal	282	-	-	Aktif	\N	\N
283	10	59	Semarang Tengah	283	-	-	Aktif	\N	\N
284	10	59	Semarang Barat	284	-	-	Aktif	\N	\N
285	10	59	Semarang Timur	285	-	-	Aktif	\N	\N
286	10	59	Semarang Selatan	286	-	-	Aktif	\N	\N
287	10	59	Kendal	287	-	-	Aktif	\N	\N
288	10	59	Weleri	288	-	-	Aktif	\N	\N
289	10	59	Boja	289	-	-	Aktif	\N	\N
290	10	60	Kota Surakarta	290	-	-	Aktif	\N	\N
291	10	60	Manahan	291	-	-	Aktif	\N	\N
292	10	60	Palur	292	-	-	Aktif	\N	\N
293	10	60	Sukoharjo	293	-	-	Aktif	\N	\N
294	10	60	Sragen	294	-	-	Aktif	\N	\N
295	10	60	Grogol	295	-	-	Aktif	\N	\N
296	10	60	Wonogiri	296	-	-	Aktif	\N	\N
297	10	60	Jatisrono	297	-	-	Aktif	\N	\N
298	10	60	Kartosuro	298	-	-	Aktif	\N	\N
299	10	60	Sumberlawang	299	-	-	Aktif	\N	\N
300	10	60	Karanganyar	300	-	-	Aktif	\N	\N
301	10	61	Kudus	301	-	-	Aktif	\N	\N
302	10	61	Pati	302	-	-	Aktif	\N	\N
303	10	61	Jepara	303	-	-	Aktif	\N	\N
304	10	61	Juwana	304	-	-	Aktif	\N	\N
305	10	61	Rembang	305	-	-	Aktif	\N	\N
306	10	61	Blora	306	-	-	Aktif	\N	\N
307	10	61	Bangsri	307	-	-	Aktif	\N	\N
308	10	61	Cepu	308	-	-	Aktif	\N	\N
309	10	62	Cilacap	309	-	-	Aktif	\N	\N
310	10	62	Majenang	310	-	-	Aktif	\N	\N
311	10	62	Gombong	311	-	-	Aktif	\N	\N
312	10	62	Kebumen	312	-	-	Aktif	\N	\N
313	10	62	Kroya	313	-	-	Aktif	\N	\N
314	10	62	Sidareja	314	-	-	Aktif	\N	\N
315	10	63	Demak	315	-	-	Aktif	\N	\N
316	10	63	Purwodadi	316	-	-	Aktif	\N	\N
317	10	63	Tegowanu	317	-	-	Aktif	\N	\N
318	10	63	Wirosari	318	-	-	Aktif	\N	\N
319	10	64	Klaten Kota	319	-	-	Aktif	\N	\N
320	10	64	Boyolali	320	-	-	Aktif	\N	\N
321	10	64	Tulung	321	-	-	Aktif	\N	\N
322	10	64	Pedan	322	-	-	Aktif	\N	\N
323	10	64	Delanggu	323	-	-	Aktif	\N	\N
324	10	65	Magelang	324	-	-	Aktif	\N	\N
325	10	65	Borobudur	325	-	-	Aktif	\N	\N
326	10	65	Purworejo	326	-	-	Aktif	\N	\N
327	10	65	Kutoarjo	327	-	-	Aktif	\N	\N
328	10	65	Temanggung	328	-	-	Aktif	\N	\N
329	10	65	Parakan	329	-	-	Aktif	\N	\N
330	10	65	Tegalrejo	330	-	-	Aktif	\N	\N
331	10	66	Purwokerto	331	-	-	Aktif	\N	\N
332	10	66	Ajibarang	332	-	-	Aktif	\N	\N
333	10	66	Wangon	333	-	-	Aktif	\N	\N
334	10	66	Banyumas	334	-	-	Aktif	\N	\N
335	10	66	Purbalingga	335	-	-	Aktif	\N	\N
336	10	66	Banjarnegara	336	-	-	Aktif	\N	\N
337	10	66	Wonosobo	337	-	-	Aktif	\N	\N
338	10	67	Pekalongan	338	-	-	Aktif	\N	\N
339	10	67	Kedungwuni	339	-	-	Aktif	\N	\N
340	10	67	Wiradesa	340	-	-	Aktif	\N	\N
341	10	67	Batang	341	-	-	Aktif	\N	\N
342	10	68	Salatiga	342	-	-	Aktif	\N	\N
343	10	69	Tegal Kota	343	-	-	Aktif	\N	\N
344	10	69	Tegal Timur	344	-	-	Aktif	\N	\N
345	10	69	Pemalang	345	-	-	Aktif	\N	\N
346	10	69	Slawi	346	-	-	Aktif	\N	\N
347	10	69	Brebes	347	-	-	Aktif	\N	\N
348	10	69	Jatibarang	348	-	-	Aktif	\N	\N
349	10	69	Bumiayu	349	-	-	Aktif	\N	\N
350	10	69	Balapulang	350	-	-	Aktif	\N	\N
351	10	69	Comal	351	-	-	Aktif	\N	\N
352	10	69	Randudongkal	352	-	-	Aktif	\N	\N
353	11	70	Kota Yogyakarta	353	-	-	Aktif	\N	\N
354	11	70	Sleman	354	-	-	Aktif	\N	\N
355	11	70	Bantul	355	-	-	Aktif	\N	\N
356	11	70	Wonosari	356	-	-	Aktif	\N	\N
357	11	70	Wates	357	-	-	Aktif	\N	\N
358	11	70	Sedayu	358	-	-	Aktif	\N	\N
359	11	70	Kalasan	359	-	-	Aktif	\N	\N
360	12	71	Embong Wungu	360	-	-	Aktif	\N	\N
361	12	71	Tandes	361	-	-	Aktif	\N	\N
362	12	71	Indrapura	362	-	-	Aktif	\N	\N
363	12	71	Ploso	363	-	-	Aktif	\N	\N
364	12	71	Kenjeran	364	-	-	Aktif	\N	\N
365	12	71	Perak	365	-	-	Aktif	\N	\N
366	12	72	Darmo Permai	366	-	-	Aktif	\N	\N
367	12	72	Dukuh Kupang	367	-	-	Aktif	\N	\N
368	12	72	Ngagel	368	-	-	Aktif	\N	\N
369	12	72	Rungkut	369	-	-	Aktif	\N	\N
370	12	72	Gedangan	370	-	-	Aktif	\N	\N
371	12	73	Taman	371	-	-	Aktif	\N	\N
372	12	73	Karang Pilang	372	-	-	Aktif	\N	\N
373	12	73	Menganti	373	-	-	Aktif	\N	\N
374	12	74	Kota Sidoarjo	374	-	-	Aktif	\N	\N
375	12	74	Krian	375	-	-	Aktif	\N	\N
376	12	74	Porong	376	-	-	Aktif	\N	\N
377	12	75	Kota Mojokerto	377	-	-	Aktif	\N	\N
378	12	75	Jombang	378	-	-	Aktif	\N	\N
379	12	75	Mojosari	379	-	-	Aktif	\N	\N
380	12	75	Ploso	380	-	-	Aktif	\N	\N
381	12	75	Ngoro	381	-	-	Aktif	\N	\N
382	12	75	Kertosono	382	-	-	Aktif	\N	\N
383	12	75	Warujayeng	383	-	-	Aktif	\N	\N
384	12	75	Nganjuk	384	-	-	Aktif	\N	\N
385	12	75	Pacet	385	-	-	Aktif	\N	\N
386	12	75	Mojoagung	386	-	-	Aktif	\N	\N
387	12	76	Probolinggo	387	-	-	Aktif	\N	\N
388	12	76	Pandaan	388	-	-	Aktif	\N	\N
389	12	76	Grati	389	-	-	Aktif	\N	\N
390	12	76	Kota Pasuruan	390	-	-	Aktif	\N	\N
391	12	76	Kraksaan	391	-	-	Aktif	\N	\N
392	12	76	Sukorejo	392	-	-	Aktif	\N	\N
393	12	76	Gondang Wetan	393	-	-	Aktif	\N	\N
394	12	76	Bangil	394	-	-	Aktif	\N	\N
395	12	76	Prigen	395	-	-	Aktif	\N	\N
396	12	77	Tuban	396	-	-	Aktif	\N	\N
397	12	77	Kota Bojonegoro	397	-	-	Aktif	\N	\N
398	12	77	Lamongan	398	-	-	Aktif	\N	\N
399	12	77	Babat	399	-	-	Aktif	\N	\N
400	12	77	Padangan	400	-	-	Aktif	\N	\N
401	12	77	Brondong	401	-	-	Aktif	\N	\N
402	12	77	Jatirogo	402	-	-	Aktif	\N	\N
403	12	77	Sumber Rejo	403	-	-	Aktif	\N	\N
404	12	78	Giri	404	-	-	Aktif	\N	\N
405	12	78	Benjeng	405	-	-	Aktif	\N	\N
406	12	78	Sedayu	406	-	-	Aktif	\N	\N
407	12	78	Bawean	407	-	-	Aktif	\N	\N
408	12	79	Kota Madiun	408	-	-	Aktif	\N	\N
409	12	79	Magetan	409	-	-	Aktif	\N	\N
410	12	79	Ngawi	410	-	-	Aktif	\N	\N
411	12	79	Caruban	411	-	-	Aktif	\N	\N
412	12	79	Mantingan	412	-	-	Aktif	\N	\N
413	12	79	Maospati	413	-	-	Aktif	\N	\N
414	12	79	Dolopo	414	-	-	Aktif	\N	\N
415	12	80	Kota Banyuwangi	415	-	-	Aktif	\N	\N
416	12	80	Rogojampi	416	-	-	Aktif	\N	\N
417	12	80	Genteng	417	-	-	Aktif	\N	\N
418	12	80	Muncar	418	-	-	Aktif	\N	\N
419	12	80	Jajag	419	-	-	Aktif	\N	\N
420	12	81	Kota Jember	420	-	-	Aktif	\N	\N
421	12	81	Lumajang	421	-	-	Aktif	\N	\N
422	12	81	Kalisat	422	-	-	Aktif	\N	\N
423	12	81	Rambipuji	423	-	-	Aktif	\N	\N
424	12	81	Ambulu	424	-	-	Aktif	\N	\N
425	12	81	Kencong	425	-	-	Aktif	\N	\N
426	12	81	Tempeh	426	-	-	Aktif	\N	\N
427	12	81	Klakah	427	-	-	Aktif	\N	\N
428	12	81	Tanggul	428	-	-	Aktif	\N	\N
429	12	82	Kota Kediri	429	-	-	Aktif	\N	\N
430	12	82	Ngadiluwih	430	-	-	Aktif	\N	\N
431	12	82	Grogol	431	-	-	Aktif	\N	\N
432	12	82	Pare	432	-	-	Aktif	\N	\N
433	12	82	Sutojayan	433	-	-	Aktif	\N	\N
434	12	82	Blitar	434	-	-	Aktif	\N	\N
435	12	82	Wlingi	435	-	-	Aktif	\N	\N
436	12	82	Campur Darat	436	-	-	Aktif	\N	\N
437	12	82	Ngunut	437	-	-	Aktif	\N	\N
438	12	82	Tulungagung	438	-	-	Aktif	\N	\N
439	12	82	Srengat	439	-	-	Aktif	\N	\N
440	12	83	Kota Malang	440	-	-	Aktif	\N	\N
441	12	83	Dinoyo	441	-	-	Aktif	\N	\N
442	12	83	Blimbing	442	-	-	Aktif	\N	\N
443	12	83	Kebonagung	443	-	-	Aktif	\N	\N
444	12	83	Lawang	444	-	-	Aktif	\N	\N
445	12	83	Batu	445	-	-	Aktif	\N	\N
446	12	83	Singosari	446	-	-	Aktif	\N	\N
447	12	83	Gondang Legi	447	-	-	Aktif	\N	\N
448	12	83	Tumpang	448	-	-	Aktif	\N	\N
449	12	83	Kepanjen	449	-	-	Aktif	\N	\N
450	12	83	Sumber Pucung	450	-	-	Aktif	\N	\N
451	12	83	Dampit	451	-	-	Aktif	\N	\N
452	12	83	Ngantang	452	-	-	Aktif	\N	\N
453	12	83	Bululawang	453	-	-	Aktif	\N	\N
454	12	84	Kota Pamekasan	454	-	-	Aktif	\N	\N
455	12	84	Sumenep	455	-	-	Aktif	\N	\N
456	12	84	Sampang	456	-	-	Aktif	\N	\N
457	12	84	Bangkalan	457	-	-	Aktif	\N	\N
458	12	84	Prenduan	458	-	-	Aktif	\N	\N
459	12	84	Ambunten	459	-	-	Aktif	\N	\N
460	12	84	Waru	460	-	-	Aktif	\N	\N
461	12	84	Blega	461	-	-	Aktif	\N	\N
462	12	84	Ketapang	462	-	-	Aktif	\N	\N
463	12	84	Kamal	463	-	-	Aktif	\N	\N
464	12	85	Kota Ponorogo	464	-	-	Aktif	\N	\N
465	12	85	Balong	465	-	-	Aktif	\N	\N
466	12	85	Trenggalek	466	-	-	Aktif	\N	\N
467	12	85	Pacitan	467	-	-	Aktif	\N	\N
468	12	86	Panarukan	468	-	-	Aktif	\N	\N
469	12	86	Bondowoso	469	-	-	Aktif	\N	\N
470	12	86	Besuki	470	-	-	Aktif	\N	\N
471	12	86	Wonosari	471	-	-	Aktif	\N	\N
472	12	86	Asembagus	472	-	-	Aktif	\N	\N
473	13	87	Denpasar	473	-	-	Aktif	\N	\N
474	13	87	Mengwi	474	-	-	Aktif	\N	\N
475	13	87	Kuta	475	-	-	Aktif	\N	\N
476	13	87	Tabanan	476	-	-	Aktif	\N	\N
477	13	88	Gianyar	477	-	-	Aktif	\N	\N
478	13	88	Klengkung	478	-	-	Aktif	\N	\N
479	13	88	Bangli	479	-	-	Aktif	\N	\N
480	13	88	Karangasem	480	-	-	Aktif	\N	\N
481	13	89	Singaraja	481	-	-	Aktif	\N	\N
482	13	89	Tejakula	482	-	-	Aktif	\N	\N
483	13	89	Seririt	483	-	-	Aktif	\N	\N
484	13	89	Gilimanuk	484	-	-	Aktif	\N	\N
485	13	89	Negara	485	-	-	Aktif	\N	\N
486	14	90	Ampenan	486	-	-	Aktif	\N	\N
487	14	90	Cakranegara	487	-	-	Aktif	\N	\N
488	14	90	Praya	488	-	-	Aktif	\N	\N
489	14	90	Selong	489	-	-	Aktif	\N	\N
490	14	90	Pringgabaya	490	-	-	Aktif	\N	\N
491	14	90	Tanjung	491	-	-	Aktif	\N	\N
492	14	91	Samawa Rea	492	-	-	Aktif	\N	\N
493	14	91	Empang	493	-	-	Aktif	\N	\N
494	14	91	Alas	494	-	-	Aktif	\N	\N
495	14	91	Taliwang	495	-	-	Aktif	\N	\N
496	14	92	Kota Bima	496	-	-	Aktif	\N	\N
497	14	92	Dompu	497	-	-	Aktif	\N	\N
498	14	92	Sape	498	-	-	Aktif	\N	\N
499	14	93	Kupang	499	-	-	Aktif	\N	\N
500	15	94	Oesao	500	-	-	Aktif	\N	\N
501	15	94	Soe	501	-	-	Aktif	\N	\N
502	15	94	Kefamenanu	502	-	-	Aktif	\N	\N
503	15	94	Atambua	503	-	-	Aktif	\N	\N
504	15	94	Kalabahi	504	-	-	Aktif	\N	\N
505	15	94	Rote Ndao	505	-	-	Aktif	\N	\N
506	15	95	Flores Bagian Barat	506	-	-	Aktif	\N	\N
507	15	95	Bajawa	507	-	-	Aktif	\N	\N
508	15	95	Ruteng	508	-	-	Aktif	\N	\N
509	15	95	Labuan Bajo	509	-	-	Aktif	\N	\N
510	15	96	Flores Bagian Timur	510	-	-	Aktif	\N	\N
511	15	96	Larantuka	511	-	-	Aktif	\N	\N
512	15	96	Adonara	512	-	-	Aktif	\N	\N
513	15	96	Lembata	513	-	-	Aktif	\N	\N
514	15	97	Sumba Timur	514	-	-	Aktif	\N	\N
515	15	97	Sumba Barat	515	-	-	Aktif	\N	\N
516	15	97	Sumba Jaya	516	-	-	Aktif	\N	\N
517	16	98	Kota Ambon	517	-	-	Aktif	\N	\N
518	16	98	Baguala	518	-	-	Aktif	\N	\N
519	16	98	Nusaniwe	519	-	-	Aktif	\N	\N
520	16	98	Tulehu	520	-	-	Aktif	\N	\N
521	16	98	Hitu	521	-	-	Aktif	\N	\N
522	16	98	Haruku	522	-	-	Aktif	\N	\N
523	16	98	Saparua	523	-	-	Aktif	\N	\N
524	16	98	Namlea	524	-	-	Aktif	\N	\N
525	16	98	Banda	525	-	-	Aktif	\N	\N
526	16	99	Masohi	526	-	-	Aktif	\N	\N
527	16	99	Piru	527	-	-	Aktif	\N	\N
528	16	99	Bula	528	-	-	Aktif	\N	\N
529	17	100	Ternate Selatan	529	-	-	Aktif	\N	\N
530	17	100	Bacan	530	-	-	Aktif	\N	\N
531	17	100	Soa Siu	531	-	-	Aktif	\N	\N
532	17	100	Sanana	532	-	-	Aktif	\N	\N
533	17	101	Sofifi	533	-	-	Aktif	\N	\N
534	17	101	Jailolo	534	-	-	Aktif	\N	\N
535	17	101	Tobelo	535	-	-	Aktif	\N	\N
536	17	102	Kota Tual	536	-	-	Aktif	\N	\N
537	17	102	Saumlaki	537	-	-	Aktif	\N	\N
538	17	102	Dobo	538	-	-	Aktif	\N	\N
539	18	103	Kota Samarinda	539	-	-	Aktif	\N	\N
540	18	103	Samarinda Ilir	540	-	-	Aktif	\N	\N
541	18	103	Samarinda Seberang	541	-	-	Aktif	\N	\N
542	18	103	Samarinda Ulu	542	-	-	Aktif	\N	\N
543	18	103	Tenggarong	543	-	-	Aktif	\N	\N
544	18	103	Kota Bangun	544	-	-	Aktif	\N	\N
545	18	103	Melak	545	-	-	Aktif	\N	\N
546	18	104	Balikpapan Selatan	546	-	-	Aktif	\N	\N
547	18	104	Balikpapan Utara	547	-	-	Aktif	\N	\N
548	18	104	Samboja	548	-	-	Aktif	\N	\N
549	18	104	Petung	549	-	-	Aktif	\N	\N
550	18	104	Longikis	550	-	-	Aktif	\N	\N
551	18	104	Tanah Grogot	551	-	-	Aktif	\N	\N
552	18	105	Nunukan	552	-	-	Aktif	\N	\N
553	18	105	Malinau	553	-	-	Aktif	\N	\N
554	18	105	Tanjung Redeb	554	-	-	Aktif	\N	\N
555	18	105	Tanjung Selor	555	-	-	Aktif	\N	\N
556	18	106	Bontang	556	-	-	Aktif	\N	\N
557	18	106	Sangatta	557	-	-	Aktif	\N	\N
558	19	107	Lambung Mangkurat	558	-	-	Aktif	\N	\N
559	19	107	Ahmad Yani	559	-	-	Aktif	\N	\N
560	19	107	Banjar Baru	560	-	-	Aktif	\N	\N
561	19	107	Gambut	561	-	-	Aktif	\N	\N
562	19	107	Martapura	562	-	-	Aktif	\N	\N
563	19	107	Pelaihari	563	-	-	Aktif	\N	\N
564	19	107	Marabahan	564	-	-	Aktif	\N	\N
565	19	108	Barabai	565	-	-	Aktif	\N	\N
566	19	108	Daha	566	-	-	Aktif	\N	\N
567	19	108	Kandangan	567	-	-	Aktif	\N	\N
568	19	108	Rantau	568	-	-	Aktif	\N	\N
569	19	108	Binuang	569	-	-	Aktif	\N	\N
570	19	108	Amintai	570	-	-	Aktif	\N	\N
571	19	108	Paringin	571	-	-	Aktif	\N	\N
572	19	108	Tanjung	572	-	-	Aktif	\N	\N
573	19	109	Kotabaru	573	-	-	Aktif	\N	\N
574	19	109	Batu Licin	574	-	-	Aktif	\N	\N
575	19	109	Satui	575	-	-	Aktif	\N	\N
576	20	110	Kota	576	-	-	Aktif	\N	\N
577	20	110	Kakap	577	-	-	Aktif	\N	\N
578	20	110	Sei Jawi	578	-	-	Aktif	\N	\N
579	20	110	Siantan	579	-	-	Aktif	\N	\N
580	20	110	Mempawah	580	-	-	Aktif	\N	\N
581	20	110	Rasau Jaya	581	-	-	Aktif	\N	\N
582	20	110	Ngabang	582	-	-	Aktif	\N	\N
583	20	111	Kota Ketapang	583	-	-	Aktif	\N	\N
584	20	111	Sukadana	584	-	-	Aktif	\N	\N
585	20	111	Sandai	585	-	-	Aktif	\N	\N
586	20	111	Tumbang Titi	586	-	-	Aktif	\N	\N
587	20	112	Kota Sanggau	587	-	-	Aktif	\N	\N
588	20	112	Sekadau	588	-	-	Aktif	\N	\N
589	20	112	Sintang	589	-	-	Aktif	\N	\N
590	20	112	Nanga Pinoh	590	-	-	Aktif	\N	\N
591	20	112	Putus Sibau	591	-	-	Aktif	\N	\N
592	20	112	Balai Karangan	592	-	-	Aktif	\N	\N
593	20	113	Kota Singkawang	593	-	-	Aktif	\N	\N
594	20	113	Sambas	594	-	-	Aktif	\N	\N
595	20	113	Pemangkat	595	-	-	Aktif	\N	\N
596	20	113	Sekura	596	-	-	Aktif	\N	\N
597	20	113	Seiduri	597	-	-	Aktif	\N	\N
598	20	113	Bengkawang	598	-	-	Aktif	\N	\N
599	21	114	Manado Selatan	599	-	-	Aktif	\N	\N
600	21	114	Manado Utara	600	-	-	Aktif	\N	\N
601	21	114	Paniki	601	-	-	Aktif	\N	\N
602	21	114	Airmadidi	602	-	-	Aktif	\N	\N
603	21	114	Bitung	603	-	-	Aktif	\N	\N
604	21	114	Tondano	604	-	-	Aktif	\N	\N
605	21	114	Tomohon	605	-	-	Aktif	\N	\N
606	21	114	Kawangkoan	606	-	-	Aktif	\N	\N
607	21	114	Ratahan	607	-	-	Aktif	\N	\N
608	21	114	Amurang	608	-	-	Aktif	\N	\N
609	21	114	Motoling	609	-	-	Aktif	\N	\N
610	21	115	Telaga	610	-	-	Aktif	\N	\N
611	21	115	Limboto	611	-	-	Aktif	\N	\N
612	21	115	Kwandang	612	-	-	Aktif	\N	\N
613	21	115	Marisa	613	-	-	Aktif	\N	\N
614	21	116	Kotamobagu	614	-	-	Aktif	\N	\N
615	21	116	Modayag	615	-	-	Aktif	\N	\N
616	21	116	Inobonto	616	-	-	Aktif	\N	\N
617	21	116	Imandi	617	-	-	Aktif	\N	\N
618	21	116	Molibagu	618	-	-	Aktif	\N	\N
619	21	116	Bintauna	619	-	-	Aktif	\N	\N
620	22	117	Benu Benua	620	-	-	Aktif	\N	\N
621	22	117	Wua-Wua	621	-	-	Aktif	\N	\N
622	22	117	Kolaka Utara	622	-	-	Aktif	\N	\N
623	22	117	Konawe Selatan	623	-	-	Aktif	\N	\N
624	22	117	Unaaha	624	-	-	Aktif	\N	\N
625	22	117	Bombana	625	-	-	Aktif	\N	\N
626	22	117	Kolaka	626	-	-	Aktif	\N	\N
627	22	118	Kota Bau-Bau	627	-	-	Aktif	\N	\N
628	22	118	Raha	628	-	-	Aktif	\N	\N
629	22	118	Wangi-Wangi	629	-	-	Aktif	\N	\N
630	22	118	Mawasangka	630	-	-	Aktif	\N	\N
631	22	118	Pasar Wajo	631	-	-	Aktif	\N	\N
632	23	119	Kota Palu	632	-	-	Aktif	\N	\N
633	23	119	Kamonji	633	-	-	Aktif	\N	\N
634	23	119	Tawaeli	634	-	-	Aktif	\N	\N
635	23	119	Donggala	635	-	-	Aktif	\N	\N
636	23	119	Tambu	636	-	-	Aktif	\N	\N
637	23	119	Parigi	637	-	-	Aktif	\N	\N
638	23	119	Poso	638	-	-	Aktif	\N	\N
639	23	119	Tentena	639	-	-	Aktif	\N	\N
640	23	119	Kolonedale	640	-	-	Aktif	\N	\N
641	23	119	Bungku	641	-	-	Aktif	\N	\N
642	23	120	Luwuk	642	-	-	Aktif	\N	\N
643	23	120	Banggai	643	-	-	Aktif	\N	\N
644	23	120	Toili	644	-	-	Aktif	\N	\N
645	23	120	Ampana	645	-	-	Aktif	\N	\N
646	23	121	Toli-Toli	646	-	-	Aktif	\N	\N
647	23	121	Bangkir	647	-	-	Aktif	\N	\N
648	23	121	Moutong	648	-	-	Aktif	\N	\N
649	23	121	Kota Raya	649	-	-	Aktif	\N	\N
650	23	121	Leok	650	-	-	Aktif	\N	\N
651	24	122	Kota Palopo	651	-	-	Aktif	\N	\N
652	24	122	Rantepao	652	-	-	Aktif	\N	\N
653	24	122	Makale	653	-	-	Aktif	\N	\N
654	24	122	Tomoni	654	-	-	Aktif	\N	\N
655	24	122	Belopa	655	-	-	Aktif	\N	\N
656	24	122	Malili	656	-	-	Aktif	\N	\N
657	24	122	Masamba	657	-	-	Aktif	\N	\N
658	24	123	Panakkukang	658	-	-	Aktif	\N	\N
659	24	123	Mattoanging	659	-	-	Aktif	\N	\N
660	24	123	Sungguminasa	660	-	-	Aktif	\N	\N
661	24	123	Kalebajeng	661	-	-	Aktif	\N	\N
662	24	123	Takalar	662	-	-	Aktif	\N	\N
663	24	123	Malino	663	-	-	Aktif	\N	\N
664	24	124	Karebosi	664	-	-	Aktif	\N	\N
665	24	124	Daya	665	-	-	Aktif	\N	\N
666	24	124	Maros	666	-	-	Aktif	\N	\N
667	24	124	Pangkep	667	-	-	Aktif	\N	\N
668	24	125	Sinjai	668	-	-	Aktif	\N	\N
669	24	125	Bantaeng	669	-	-	Aktif	\N	\N
670	24	125	Jeneponto	670	-	-	Aktif	\N	\N
671	24	125	Panrita Lopi	671	-	-	Aktif	\N	\N
672	24	125	Tanete	672	-	-	Aktif	\N	\N
673	24	125	Kalumpang	673	-	-	Aktif	\N	\N
674	24	125	Selayar	674	-	-	Aktif	\N	\N
675	24	126	Tanru Tedong	675	-	-	Aktif	\N	\N
676	24	126	Pangsid	676	-	-	Aktif	\N	\N
677	24	126	Rappang	677	-	-	Aktif	\N	\N
678	24	126	Soppeng	678	-	-	Aktif	\N	\N
679	24	126	Pajalesang	679	-	-	Aktif	\N	\N
680	24	126	Mattirotasi	680	-	-	Aktif	\N	\N
681	24	126	Barru	681	-	-	Aktif	\N	\N
682	24	127	Lakawan	682	-	-	Aktif	\N	\N
683	24	127	Sawitto	683	-	-	Aktif	\N	\N
684	24	127	Kariango	684	-	-	Aktif	\N	\N
685	24	127	Pekkabata	685	-	-	Aktif	\N	\N
686	24	127	Enrekang	686	-	-	Aktif	\N	\N
687	24	128	Hasanuddin	687	-	-	Aktif	\N	\N
688	24	128	Sengkang	688	-	-	Aktif	\N	\N
689	24	128	Tellu Boccoe	689	-	-	Aktif	\N	\N
690	24	128	Paria	690	-	-	Aktif	\N	\N
691	24	128	Uloe	691	-	-	Aktif	\N	\N
692	24	128	Patangkai	692	-	-	Aktif	\N	\N
693	25	129	Jayapura	693	-	-	Aktif	\N	\N
694	25	129	Abepura	694	-	-	Aktif	\N	\N
695	25	129	Sentani	695	-	-	Aktif	\N	\N
696	25	129	Genyem	696	-	-	Aktif	\N	\N
697	25	129	Sarmi	697	-	-	Aktif	\N	\N
698	25	129	Arso	698	-	-	Aktif	\N	\N
699	25	129	Wamena	699	-	-	Aktif	\N	\N
700	25	130	Sorong	700	-	-	Aktif	\N	\N
701	25	130	Doom	701	-	-	Aktif	\N	\N
702	25	130	Teminabuan	702	-	-	Aktif	\N	\N
703	25	130	Fak-Fak	703	-	-	Aktif	\N	\N
704	25	130	Kaimana	704	-	-	Aktif	\N	\N
705	25	130	Aimas	705	-	-	Aktif	\N	\N
706	25	131	Biak	706	-	-	Aktif	\N	\N
707	25	131	Serui	707	-	-	Aktif	\N	\N
708	25	131	Yomdori	708	-	-	Aktif	\N	\N
709	25	132	Manokwari	709	-	-	Aktif	\N	\N
710	25	132	Nabire	710	-	-	Aktif	\N	\N
711	25	132	Prafi	711	-	-	Aktif	\N	\N
712	25	132	Bintuni	712	-	-	Aktif	\N	\N
713	25	133	Merauke	713	-	-	Aktif	\N	\N
714	25	133	Kuprik	714	-	-	Aktif	\N	\N
715	25	133	Kurik	715	-	-	Aktif	\N	\N
716	25	133	Tanah Merah	716	-	-	Aktif	\N	\N
717	25	134	Kota Timika	717	-	-	Aktif	\N	\N
718	25	134	Timika Jaya	718	-	-	Aktif	\N	\N
\.


--
-- Name: sub_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('sub_area_id_seq', 719, true);


--
-- Data for Name: surat_tugas; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY surat_tugas (id, no_surat, id_pemeriksa1, id_pemeriksa2, id_staff, created_at, updated_at, status, jangka_waktu) FROM stdin;
2	00002/III/2019	11	10	1	2019-03-30 12:13:01	2019-03-30 12:13:01	Belum	2019-04-01 12:13:01
3	00003/IV/2019	4	6	1	2019-04-23 11:49:20	2019-07-15 12:08:49	Sudah	2019-04-25 11:49:20
1	00001/III/2019	4	6	1	2019-03-26 14:07:57	2019-07-15 12:08:56	Sudah	2019-03-28 14:07:57
4	00004/VIII/2019	47	47	49	2019-08-20 20:36:29	2019-08-20 21:29:57	Sudah	2019-08-22 20:36:29
5	00005/VIII/2019	47	47	49	2019-08-20 21:33:12	2019-08-20 21:33:12	Belum	2019-08-22 21:33:12
6	00006/VIII/2019	47	47	49	2019-08-21 13:47:28	2019-08-21 13:47:28	Belum	2019-08-23 13:47:28
8	00007/X/2019	47	47	49	2019-10-23 08:47:08	2019-10-23 08:47:08	Belum	2019-10-25 08:47:08
9	00008/I/2020	47	47	49	2020-01-10 01:53:08	2020-01-10 01:53:08	Belum	2020-01-12 01:53:08
\.


--
-- Name: surat_tugas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('surat_tugas_id_seq', 9, true);


--
-- Name: table_calon_pelanggan_web_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('table_calon_pelanggan_web_id_seq', 349, true);


--
-- Data for Name: tagihan_area; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY tagihan_area (id, no_ref, id_kwilayah, id_karea, periode, jml_tagihan, jml_terbayar, jml_hutang, created_at, updated_at) FROM stdin;
3	KA9069	10	65	2019-04-18	30757500	20000000	10757500	2019-04-18 11:47:40	2019-04-18 12:24:21
2	KA2605	11	70	2019-04-18	14278000	12000000	12278000	2019-04-18 11:40:29	2019-04-18 13:19:56
4	KA8460	10	59	2019-08-20	190000	280000	-90000	2019-08-20 21:50:53	2019-08-21 04:52:53
\.


--
-- Name: tagihan_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('tagihan_area_id_seq', 4, true);


--
-- Data for Name: tagihan_pusat; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY tagihan_pusat (id, created_at, updated_at) FROM stdin;
\.


--
-- Name: tagihan_pusats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('tagihan_pusats_id_seq', 1, false);


--
-- Data for Name: tagihan_sub_area; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY tagihan_sub_area (id, no_ref, id_karea, id_subarea, periode, jml_tagihan, jml_terbayar, jml_hutang, created_at, updated_at) FROM stdin;
1	SA9557	65	324	2019-04-18	30410000	20000000	10410000	2019-04-18 11:24:30	2019-04-18 11:50:26
2	SA3064	70	353	2019-04-18	14080000	12000000	2080000	2019-04-18 11:32:50	2019-04-18 13:17:18
\.


--
-- Name: tagihan_sub_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('tagihan_sub_area_id_seq', 2, true);


--
-- Data for Name: tagihan_wilayah; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY tagihan_wilayah (id, no_ref, id_kwilayah, periode, id_share, rp_share, jml_tagihan, jml_terbayar, jml_hutang, created_at, updated_at) FROM stdin;
3	WL9982	11	2019-04-18	10	1427800	14278000	5000000	9278000	2019-04-18 12:30:45	2019-04-18 12:48:30
2	WL6923	10	2019-04-18	9	12303000	30757500	20000000	10757500	2019-04-18 12:28:27	2019-08-20 22:27:20
\.


--
-- Name: tagihan_wilayah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('tagihan_wilayah_id_seq', 3, true);


--
-- Data for Name: tarif; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY tarif (id, jenis_tarif, created_at, updated_at) FROM stdin;
1	S1	2019-01-24 09:33:59	2019-01-24 09:33:59
2	S2	2019-03-20 14:27:40	2019-03-20 14:27:40
3	S3	2019-03-20 14:27:46	2019-03-20 14:27:46
4	R1	2019-03-20 14:27:54	2019-03-20 14:27:54
5	R2	2019-03-20 14:28:00	2019-03-20 14:28:00
6	R3	2019-03-20 14:28:08	2019-03-20 14:28:08
7	B1	2019-03-20 14:28:18	2019-03-20 14:28:18
8	B2	2019-03-20 14:28:24	2019-03-20 14:28:24
9	B3	2019-03-20 14:28:30	2019-03-20 14:28:30
10	I1	2019-03-20 14:31:10	2019-03-20 14:31:10
11	I2	2019-03-20 14:31:19	2019-03-20 14:31:19
12	I3	2019-03-20 14:31:36	2019-03-20 14:31:36
13	P1	2019-03-20 14:32:13	2019-03-20 14:32:13
14	P2	2019-03-20 14:32:20	2019-03-20 14:32:20
15	P3	2019-03-20 14:32:29	2019-03-20 14:32:29
16	P3	2019-03-20 14:32:29	2019-03-20 14:32:29
17	L	2019-03-20 14:32:40	2019-03-20 14:32:40
18	L1	2019-03-20 14:32:45	2019-03-20 14:32:45
\.


--
-- Name: tarif_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('tarif_id_seq', 19, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY users (id, nip, nama, email, jabatan, departemen, wewenang, id_kwilayah, id_karea, id_subarea, status, email_verified_at, password, remember_token, created_at, updated_at, username, masa_berlaku, no_telp) FROM stdin;
15	812312	Agung Kurniawan	agungk@gmail.com	BTL	BTL	BTL	11	70	353	Aktif	\N	$2y$10$Sf3aMhHFIZuQbFrd3Eapo.D1MZ77M06P9NwDhb9KUhu1mN.MVuLAm	y5QvczDrROO7RnHL5mF74kePwxOEF4d7RVJHi7Rd1CeFFlDBJUZGTVT35HtD	2019-03-27 20:55:29	2019-03-27 20:55:29	agungk	\N	081277881122
13	3374012371922221	Dio S	dio@gmail.com	TT	Pemeriksa	Pemeriksa	10	59	285	Aktif	\N	$2y$10$rSJw1O1/y2I.oyCx2/6Tk.rUNpArAU8HT9WppSeL/zUp/gPxjJToC	\N	2019-03-26 09:33:59	2019-03-26 17:23:07	dios	2020-05-16	085112222322
16	8147820136001	Sri Lestari	sril70074@gmail.com	Admin	Admin Sub Area	Staff	10	65	324	Aktif	\N	$2y$10$yu8nN.j9MoHn2Vj2UluGeutXBVxAhytokSzegijF4Fvvq/LYkA/ti	nqXYDjmtMUHQPyisitULGKEwRo3gKuqiMG3tH3yFoIzHWsLSl7cPOWDlQOPA	2019-04-18 11:10:49	2019-04-18 11:10:49	srilestari	\N	08136877401
8	3301221703820007	Alexander	alexander@gmail.com	Admin	Admin Wilayah	Staff	10	65	324	Aktif	\N	$2y$10$DHy8MGn4iOgIZgORa/xmTO2HXWNTJ.8r8hyew0esyQ8ARLfRmHbh.	87osgGzkIZhVvuo5seh4G77iBrOvaMAKBVU1As92dJlYxRpm59jGmahHmnwP	2019-03-19 21:51:31	2019-03-19 21:51:31	alex	\N	081290901898
9	12780192181	Andika Putra	andika.putra@gmail.com	Admin	Admin Wilayah	Staff	11	70	353	Aktif	\N	$2y$10$YvAuY0wNn5grLAY9vgwLt.fYTzEdwbaRZC7oAgLhiTIAlqR0EfRUS	eIb29zdxRial0Ttd1tKwKLdgs0Sf334naN3yHyWVtqpMnnZhFMsrYQaX0Jug	2019-03-20 20:11:41	2019-03-20 20:11:41	andikaputra	\N	08123901910
10	812451121212	Moh Lakoni	mohlakoni@gmail.com	TT	Pemeriksa	Pemeriksa	11	70	353	Aktif	\N	$2y$10$pxTANU8r6vlzGb3f2EfurusJ58WG37t1xoioxcNHP6fXvQm5gklXq	Lf7XTHJNVy2F2GUkCf0sq5LjztAZyDxkNQZcna4MOdLe7RkM1L2NGLulplvu	2019-03-20 20:27:11	2019-03-25 15:08:30	mlakoni	2024-03-15	081290901892
11	78102512121	Suparno S	suparno@gmail.com	PJT	Pemeriksa	Pemeriksa	11	70	353	Aktif	\N	$2y$10$IcyQl3tj7Qaa0G8kRmeIm.WM6MmEhSCLPD4OWW1Tn7mD93buR.0tS	\N	2019-03-20 20:29:13	2019-03-25 15:08:41	suparnos	2024-03-02	081298121211
17	879654012001	Ninik Muni	ninik@gmail.com	Admin	Admin Sub Area	Staff	11	70	353	Aktif	\N	$2y$10$3sbwdWqeTGZDWy7pD37Tre3PDa1vO3ITF7tNG6BAPUG7Qml.D.cqy	BK2h3bi9RdleLsAQKsScmtiJY0FV32f1rqTSPN3neRUBaGyElRJ9octenCe6	2019-04-18 11:26:23	2019-04-18 11:26:23	ninik	\N	0875214478010
12	1999019019	Nonik Caca	nonik@gmail.com	Admin	Admin Area	Staff	11	70	353	Aktif	\N	$2y$10$fmhv8R1kQ9rrk4vNAcJin.iMSuLVFF1buA5580QIgALWwE.CNc8rO	PgWDNvkCkLhbCG4CENjMbzj4maco1bLeaMZQ6zhEq0XSeRUGMQZfF7tUafw8	2019-03-20 20:35:37	2019-03-25 15:08:49	nonik	\N	08192010101
5	912121	Muhammad Afthony	m.afthony@gmail.com	PJT	Verifikator	Verifikator	10	65	324	Aktif	\N	$2y$10$HZYQTu9QevnhM9jdngldzOT9xPPj9jxag3BLzkmxtfjsmqTC6.mum	0WVJKfYAlud7kPZHkDO53oq7GvDmwvpW2UQRU5aFFPtiK33Snis6unc5aJXO	2019-03-01 16:28:55	2019-03-18 16:56:25	afthony	2024-12-05	\N
3	890121921	Roger	roger12@gmail.com	PJT	Verifikator	Verifikator	11	70	353	Aktif	\N	$2y$10$HZYQTu9QevnhM9jdngldzOT9xPPj9jxag3BLzkmxtfjsmqTC6.mum	BBLm0KX2miN4qKmoNXmh7YNnH2Xnx4SJ7xMERQz8XOJwvsQFFARYxuL0KEoc	2019-01-28 05:10:43	2019-04-23 11:46:57	roger	2024-12-05	081280391919
19	8111111	Aceh	aceh@gmail.com	Admin	Admin Wilayah	Staff	2	1	1	Aktif	\N	$2y$10$l9QMuT2.ZLLThQPeSLqRquE5ouCas/Sj9rA9HMSl5dcMEEpVhDVsK	lXDDyyvQuBHlmWMAN8RyrqFGuQxe75GqBUKVEYmgqySeUSvuC943Oc9gYzY6	2019-05-10 17:19:50	2019-05-10 17:19:50	aceh	\N	0812342211
26	88888888	Jabar	jabar@gmail.com	Admin	Admin Wilayah	Staff	9	44	192	Aktif	\N	$2y$10$euoeVHyD1NQftHfuCGGqHuwy7OrQiGUFrwARIS1.PxU373MpVQpni	\N	2019-05-10 17:47:20	2019-05-10 17:47:20	jabar	\N	08124423112
29	9111111111	Jatim	jatim@gmail.com	Admin	Admin Wilayah	Staff	12	71	360	Aktif	\N	$2y$10$O9sbbeEFe4HjDMwcZlGGTOk/B/swuzynDtqqc6t4L7s1iqRcN/k.W	\N	2019-05-10 17:49:49	2019-05-10 17:49:49	jatim	\N	081245675433
31	93333333333	Nusa Tenggara Timur	ntb@gmail.com	Admin	Admin Wilayah	Staff	15	94	500	Aktif	\N	$2y$10$d9BaefSbbQjvuRTbuwtMmeoJSlkfe/usnhjTEIf8HM7BhC094HzJ2	\N	2019-05-10 17:51:28	2019-05-10 17:51:28	nusatt	\N	082341234411
32	9444444444	Nusa Tenggara Barat	ntt@gmail.com	Admin	Admin Wilayah	Staff	14	90	486	Aktif	\N	$2y$10$8kTF5QYgCOW4TvSDCcwZU.CKGr4tIw0nwf77nEsx8hi0Fmoi7g6tK	\N	2019-05-10 17:52:24	2019-05-10 17:52:24	nusatb	\N	081245322111
33	955555555	Maluku	maluku@gmail.com	Admin	Admin Wilayah	Staff	16	98	517	Aktif	\N	$2y$10$ASrc6LzuIId5pRV84viT4uvpswia4IZ5ovEUaYD9q7qvNHgU7cZk.	\N	2019-05-10 17:53:16	2019-05-10 17:53:16	maluku	\N	081245321122
20	822222	Sumut	sumut@gmail.com	Admin	Admin Wilayah	Staff	3	7	38	Aktif	\N	$2y$10$5HOAcEkE66SXqpGaeh4olu0k4X06OwM6UI4zZppanFFyI9sQmYDem	Ln5YtG9tEVHhQkyPdXMud9rNbLfNL5Lx1EwZZ82562CDXWPrHI1CfyilaWny	2019-05-10 17:42:40	2019-05-10 17:42:40	sumut	\N	08123457711
22	84444444	Jambi	jambi@gmail.com	Admin	Admin Wilayah	Staff	5	21	133	Aktif	\N	$2y$10$jvlweEuc2xgehHJjBqq5aeNzFDc9X3.qPGDzKeztKG/eeaGEjtSVO	ifehzDhmZTabkOdq9U6con4O6Bw5raUE7uXZCsGBiIQdE0yW9ggI5pPIaHjX	2019-05-10 17:43:56	2019-05-10 17:43:56	jambi	\N	08241122221
23	85555555	Sumsel	sumsel@gmail.com	Admin	Admin Wilayah	Staff	6	19	143	Aktif	\N	$2y$10$e1yj8CqiXre9HbnsRP0iGeUyEs2JiaZqwEo72t3H5aG71TvIQSLWe	HLy2ihMWyppMrjFlwPnJoUrp9eipgdbAuZFQJymy2x46YqleGJ2rhauMbslk	2019-05-10 17:44:47	2019-05-10 17:44:47	sumsel	\N	08122331111
24	8666666666	Banten	banten@gmail.com	Admin	Admin Wilayah	Staff	7	38	164	Aktif	\N	$2y$10$N32nPjjVJo3LoJRrbqQIOudCzSzNx7iAaUFtoZe1MMG0BhWC0xejS	an564Rhg83Bfw3XnsYusXM6RcrxGi3PktD8weLvsUjg33yoxN0LjGqWPsQze	2019-05-10 17:45:45	2019-05-10 17:45:45	banten	\N	081245664421
28	9000000	DI Yogyakarta	diyogya@gmail.com	Admin	Admin Wilayah	Staff	11	70	353	Aktif	\N	$2y$10$0m.92gQhuRCoNyOM4pa1iuL2wN/wWSxqt509uZnkOP9qoN0ZlWFni	Iokz4UMKKffX6KBuVqSlUWS9WyNjSi8Q4Rhl6ATY0M41gI5sepHqHW3U4zdd	2019-05-10 17:49:00	2019-05-10 17:49:00	diyogya	\N	081244332211
30	92222222222222	Bali	bali@gmail.com	Admin	Admin Wilayah	Staff	13	87	473	Aktif	\N	$2y$10$yed8a26FvMdFNnle4NrDdu9H1u7Mokcgk3.2QmlLm.1jUXhr2i5LC	2i0R7NqVIhKoYI5rPZ3cPXC8BuLtDg3ezSE4WSBRsrtqGZn706inB1toFMPj	2019-05-10 17:50:24	2019-05-10 17:50:24	bali	\N	082341223322
18	00000000000101	Internal	internal@gmail.com	Internal	Internal	Internal	10	59	283	Aktif	\N	$2y$10$YkZj0AQhdwrTXFsSVklrHOPQ799bBoe/szFrgCMXdGcuxWHZ6okCO	4aqFsSV44FDdywFTBSCXGOhTsrzmHuxWgosWL3RKv87oMuNy9W6vDjCqciBJ	2019-04-23 10:52:59	2019-04-23 10:52:59	internal	\N	081365478010
4	154	wahyu	azzamujib@gmail.com	TT	Pemeriksa	Pemeriksa	10	65	324	Aktif	\N	$2y$10$HZYQTu9QevnhM9jdngldzOT9xPPj9jxag3BLzkmxtfjsmqTC6.mum	nd7eu3qPmD4ptUmJXrTGcn3G8jauWRjJreYOCkqbFmGUyzxwEyNE48ggaWPP	2019-02-14 08:37:43	2019-07-15 12:20:56	wahyu	2024-12-05	081233220011
6	87810126485501	Putra Pratama	okicandra21@gmail.com	TT	Pemeriksa	Pemeriksa	10	65	324	Aktif	\N	$2y$10$UhaH5tXxY80e50l0I36RVupNjX.dFRljDCH559n4Oe6lVFicYheDu	Mgn7TY4UQl4W6WoBTa57Vxdwu4R2DhdjG7Qg6vKtof9p5Dj0wgnIPiA2g7L2	2019-03-12 15:57:32	2019-03-12 15:57:32	putra	2024-12-05	\N
7	781025	Mahfud	mahfudmd@gmail.com	Admin	Admin Area	Staff	10	65	324	Aktif	\N	$2y$10$oCRhPZaIBkmOjTkJIMntA.mlUvwF2HBjgudO1VFA/MUHEvfpD4BRO	UumxqkiC0fmJNbmpSVF4cH2D8B11AkkojQeSgPdEhLXJX53uEgJZV7eLEfNs	2019-03-19 21:25:28	2019-08-25 03:09:12	mahfud	\N	081270701919
27	89999999	Jateng	jateng@gmail.com	Admin	Admin Wilayah	Staff	10	59	283	Aktif	\N	$2y$10$oKB6kV/V4gBRdhyl9.qCRuT8w7YdUsiADt2OmL/kLBsx5AjbZS69y	EpKyubnLeADniamHZtoeY21i7MsuocNhaGA3IzY65aw2gE7Tz7eu1PcFWY1J	2019-05-10 17:48:01	2019-05-10 17:48:01	jateng	\N	0824533221
35	97777777	Kaltim	kaltim@gmail.com	Admin	Admin Wilayah	Staff	18	103	539	Aktif	\N	$2y$10$ABzKigQukZIeAwiFWcoTKeK9KresVmD/.WXNBugw2Cy.ZOETQ5itK	\N	2019-05-10 17:54:53	2019-05-10 17:54:53	kaltim	\N	081322112233
36	9888888888	Kalsel	kalsel@gmail.com	Admin	Admin Wilayah	Staff	19	107	558	Aktif	\N	$2y$10$7WWX24fuufai9.1Dh6nQbeJsH13nCsR8urK80WsurSGbzKNf6M0h.	\N	2019-05-10 17:56:34	2019-05-10 17:56:34	kalsel	\N	081234554422
38	22222222222	Sulawesi Utara	sulut@gmail.com	Admin	Admin Wilayah	Staff	21	114	602	Aktif	\N	$2y$10$yZqhzGPx7HHzZhjE3DP/yuGh2wMYNtCnBV7KuMvvGt9Ot89sC/azS	\N	2019-05-10 17:58:26	2019-05-10 17:58:26	sulut	\N	081254332211
40	24444444444	Sultengah	sultengah@gmail.com	Admin	Admin Wilayah	Staff	23	119	638	Aktif	\N	$2y$10$.8ELZ8t99uU84ZLpKqUuMem/INYCipv.zsv7TvHlVzVFQQKFJ2nsO	\N	2019-05-10 17:59:57	2019-05-10 17:59:57	sultengah	\N	08245124422
41	25555555555	Sulsel	sulsel@gmail.com	Admin	Admin Wilayah	Staff	24	122	651	Aktif	\N	$2y$10$PFqv.L/YPE8YjtBMNDeYNeqVRPXdJpTM9auRaPpRSLbVZVuAqsopm	\N	2019-05-10 18:00:41	2019-05-10 18:00:41	sulsesl	\N	082341221111
42	288888888888	Papua	papua@gmail.com	Admin	Admin Wilayah	Staff	25	129	693	Aktif	\N	$2y$10$aT2fUK7nf0K.HKvLWoLbpO2e2eQXeMVekxIOo2VFsEG8m28cIE4rq	\N	2019-05-10 18:01:37	2019-05-10 18:01:37	papua	\N	081245778899
21	83333333333	Kep Riau	kepriau@gmail.com	Admin	Admin Wilayah	Staff	4	15	102	Aktif	\N	$2y$10$1kP0a5NvHspMeYgYOweT5ekTdOBVABsYy13oMJb9ozlZ1Nwkf0McK	Cw6LNP6TsBkoXokIXaoZuhT9T9LXhT6qReEIln8U3zHHZ5U61a5iYgX7V29h	2019-05-10 17:43:21	2019-05-10 17:43:21	kepriau	\N	0823441122
25	877777777	Dki Jakarta	dkijakarta@gmail.com	Admin	Admin Wilayah	Staff	8	23	177	Aktif	\N	$2y$10$29mr4Ni4yp1AvsBVVy91i.TI2GIDrFiSA9GoBDLsU.V2NJXsWksz6	lsl7PMgQxIauxOjMnNrrt0mfvUB9n5dV8snWM4UJAibWNgCB8CkKojeaFMqG	2019-05-10 17:46:34	2019-05-10 17:46:34	dkijakarta	\N	0823112221
34	9666666	Maluku Utara	malukuutara@gmail.com	Admin	Admin Wilayah	Staff	17	100	529	Aktif	\N	$2y$10$uW3jBQ8vyanzB4Z4TkvKRe5Lb/OW60KJ9jR/tr3vBpXm2tpHCN08i	imheQ4PtgzbEfGqJpvsqkn5uiEyWKtKhCkqpPCA9LGA2DVQiDVuZepBSZAls	2019-05-10 17:54:06	2019-05-10 17:54:06	malukuutara	\N	081245688111
37	99999999	Kalbar	kalbar@gmail.com	Admin	Admin Wilayah	Staff	20	110	576	Aktif	\N	$2y$10$X6103j6eZCsCbcOSIAPXhuhKuRRvmmXpLH2NVOEVD8dpRfQwba.aa	9F4PqauGgpZYZzCAEtm1E9AOahso91dbtu9dxriGrPdtmpv4dIM2MPIniOaz	2019-05-10 17:57:14	2019-05-10 17:57:14	kalbar	\N	081247766112
39	233333333333	Sulteng	sulteng@gmail.com	Admin	Admin Wilayah	Staff	22	117	620	Aktif	\N	$2y$10$G3ErsUmyrkE/Aki/q.F2uu8d9Xmg0WlY55jq5dF.a5sEnmwpArR1.	d3L2dcovOxSO7ZPUUFLbb1zCvhmxZmivc452IWv1YyeXiaTt89zi8GhJpx94	2019-05-10 17:59:17	2019-05-10 17:59:17	sulteng	\N	082212457788
47	123123	Roma	issam88881@gmail.com	TT	Pemeriksa	Pemeriksa	10	59	283	Aktif	\N	$2y$10$tyhnDDg/6AZdBBgTvagDNO7vK.ibPU2R61Xcz7pEwRyT6SAqf7Vm2	cka4arucrM3QLYDtMalamClgJ5bsTqg0lZ5cTcFRRP91kYJCtWzU5EpzO2Iy	2019-08-12 19:58:43	2019-11-29 11:36:34	issam	2019-12-31	081234432123
43	123456789	ISTIQOMAH	sipkalteng@gmail.com	Admin	Admin Wilayah	Staff	26	135	\N	Aktif	\N	$2y$10$57/wP1IKZszbk/rpEjCq2.r8RsMozXahWjjJaqFuVZXpkw4Z5qjFS	eEeHUxAsIGoNHbEMZk9r78SiMboxtQu1hi3JtDTiTpEzK3w6bQeKH6eh8QC3	2019-08-08 16:28:49	2019-08-08 16:28:49	kalteng	\N	081268242714
44	123456	WIWIK WIRANTI	siplampung@gmail.com	Admin	Admin Wilayah	Staff	30	\N	0	Aktif	\N	$2y$10$fxndshkmLbNtLZamQnRc0.TR2wGnwClD/ixRnMjCufhEt5OeqySKq	\N	2019-08-09 11:04:25	2019-08-09 11:04:25	lampung	\N	082377734737
45	123456	MUHAMMAD FARHANSYAH MONDARI	sipriau@gmail.com	Admin	Admin Wilayah	Staff	31	\N	0	Aktif	\N	$2y$10$SU9hTM90AHfbEJwgljiJ1..Ci4PBi6bQ/oU.gqypGfRua8pUXgAJW	\N	2019-08-09 11:05:19	2019-08-09 11:10:06	riau	\N	081270560283
46	1234567	PONO	sipsumbar@gmail.com	Admin	Admin Wilayah	Staff	32	0	\N	Aktif	\N	$2y$10$k2TtozPBfxt4UFaExpLiC.ptYA62hH4mUSjYuvbjG4VNBR0yVkDB6	\N	2019-08-09 11:17:11	2019-08-09 11:17:11	sumbar	\N	0812682427145
51	8899	VERI SATU	veri1@gmail.com	PJT	Verifikator	Verifikator	10	59	283	Aktif	\N	$2y$10$KDDO2vP8q6rwZqD97/JkRu2R4lpU5eu/KIuC03CEwxLZ8al2G3SGa	WPOkRrAJIlbCvE5ytYAlpIxGkznRIck0JlEnqmYdlFbc3z8rRn35hIqLXdei	2019-08-20 21:14:30	2019-11-29 11:47:31	feri1	2020-01-02	3q423
54	1212	admin dua	issam88882@gmail.com	Admin Pusat	Admin Pusat	Admin Pusat	10	59	0	Aktif	\N	$2y$10$SLD57NEjM8NyXPpRs0ufU./WqJraIw05gLRzx.DeS8rHkhe4rLwHe	\N	2020-01-08 22:15:50	2020-01-08 22:15:50	admin2	\N	\N
48	1112227	aaa	aaa@gmail.com	Admin	Admin Wilayah	Staff	10	59	283	Aktif	\N	$2y$10$g/iMrWiDFBdEB6OOPH3DmOf5VH864ErtRRHDs4sLu4fzSrIFS2LFC	GPVhwqTAVYIntZAKn8Xb93WptbIJjqVAArsHLQVQqadxANJIbkjlkYpyK1Fa	2019-08-20 00:14:09	2019-08-20 22:11:21	aaaq	\N	081234433211
53	889988	Eka Pranata	eka@gmail.com	TT	Pemeriksa	Pemeriksa	10	59	284	Aktif	\N	$2y$10$dfo/XOtVip55jewiEBvoKO6noHEy3Mt1OoBrFwMl7drcLBvOhgP/S	\N	2020-01-07 11:07:40	2020-01-07 11:07:40	ekapranata	2020-11-19	180123123
49	222333	bbb	bbb@gmail.com	Admin	Admin Area	Staff	10	59	283	Aktif	\N	$2y$10$2TrwrNlU.QWT6/zRj0eK/.Q8iQwiwDCE8y..825TQZrgZ39IqgKmq	bXwCdqljrnvsymnFtwCwLMzp1PETQM6kiNubScke9LhJ0hpQT67GKFLq2exq	2019-08-20 06:30:06	2019-08-21 04:48:46	bbbq	\N	1231231231
50	333444	ccc	ccc@gmail.com	Admin	Admin Sub Area	Staff	10	59	283	Aktif	\N	$2y$10$6llbfyUVU4RccfIVUlt3dOPtPR8nZSqA4kJqOMbgBfOd51qkuxPtW	\N	2019-08-20 06:31:39	2019-08-21 04:50:00	cccq	\N	234234234
1	17	SiSIP	ptsip@gmail.com	Admin Pusat	Admin Pusat	Admin Pusat	10	65	324	Aktif	\N	$2y$10$tui/9JbjVnZkV0TlJXlS7.OglbX2EuKbFLT.fhe3uZHOZ34A3P.vG	glim32yLoIuoyAMlVpJRPadXnQ2PxJMXlGcKd7Tau4qqSi8OKdy5o8GnPvFw	2019-01-24 09:18:04	2020-01-08 19:46:20	sipp	\N	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('users_id_seq', 55, true);


--
-- Data for Name: verifikasi; Type: TABLE DATA; Schema: public; Owner: sertifik
--

COPY verifikasi (id, no_pendaftaran, no_lhpp, pemeriksaan_dokumen, proteksi_sentuh_langung, pemeriksaan_penghantar, pemeriksaan_phb, pemeriksaan_elektroda, pemeriksaan_polaritas, pemeriksaan_pemasangan, pemeriksaan_sni, pemeriksaan_kamar_mandi, catatan, hasil_pemeriksaan, ceklist_ppu1, ceklist_ppu2, hasil_verifikasi, alasan_reject, id_staff, status_sertifikasi, created_at, updated_at) FROM stdin;
1	SIP-0419-010-065-0002	LHI-0419-010-004-0002	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik Operasi	Laik Operasi (LO)			approve		5	1	2019-04-23 11:51:31	2019-04-23 11:52:24
2	SIP-0419-010-065-0001	LHI-0419-010-004-0001	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik Operasi	Laik Operasi (LO)			approve		5	1	2019-04-23 11:51:44	2019-04-23 11:52:43
3	SIP-0819-010-059-0007	LHI-0819-010-047-0003	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	wefwefsedfc	Laik Operasi (LO)			approve		51	1	2019-08-20 21:15:36	2019-08-20 21:17:39
4	SIP-0819-010-059-0009	LHI-0819-010-047-0004	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	catatan verifikator	Laik Operasi (LO)			approve		51	1	2019-08-21 14:18:38	2019-08-21 14:24:06
5	SIP-1019-010-059-0010	LHI-1019-010-047-0005	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	ewrtwert	Laik Operasi (LO)			approve		51	1	2019-10-23 09:06:41	2019-10-23 09:09:47
6	SIP-0819-010-059-0006	LHI-0120-010-047-0006	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	Laik	catatan laik	Laik Operasi (LO)			approve		51	1	2020-01-10 02:15:40	2020-01-10 02:27:43
\.


--
-- Name: verifikasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sertifik
--

SELECT pg_catalog.setval('verifikasi_id_seq', 6, true);


--
-- Name: Berita_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY "Berita"
    ADD CONSTRAINT "Berita_pkey" PRIMARY KEY (id);


--
-- Name: alokasi_share_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY alokasi_share
    ADD CONSTRAINT alokasi_share_pkey PRIMARY KEY (id);


--
-- Name: asosiasi_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY asosiasi
    ADD CONSTRAINT asosiasi_pkey PRIMARY KEY (id);


--
-- Name: berita_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY berita
    ADD CONSTRAINT berita_pkey PRIMARY KEY (id);


--
-- Name: btl_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY btl
    ADD CONSTRAINT btl_pkey PRIMARY KEY (id);


--
-- Name: daya_daya_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY daya
    ADD CONSTRAINT daya_daya_unique UNIQUE (daya);


--
-- Name: daya_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY daya
    ADD CONSTRAINT daya_pkey PRIMARY KEY (id);


--
-- Name: galeri_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY galeri
    ADD CONSTRAINT galeri_pkey PRIMARY KEY (id);


--
-- Name: jenis_bangunan_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY jenis_bangunan
    ADD CONSTRAINT jenis_bangunan_pkey PRIMARY KEY (id);


--
-- Name: kantor_area_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY kantor_area
    ADD CONSTRAINT kantor_area_pkey PRIMARY KEY (id);


--
-- Name: kantor_wilayah_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY kantor_wilayah
    ADD CONSTRAINT kantor_wilayah_pkey PRIMARY KEY (id);


--
-- Name: kota_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY kota
    ADD CONSTRAINT kota_pkey PRIMARY KEY (id);


--
-- Name: log_status_no_pendaftaran_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY log_status
    ADD CONSTRAINT log_status_no_pendaftaran_unique UNIQUE (no_pendaftaran);


--
-- Name: log_status_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY log_status
    ADD CONSTRAINT log_status_pkey PRIMARY KEY (id);


--
-- Name: migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: oauth_auth_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY oauth_auth_codes
    ADD CONSTRAINT oauth_auth_codes_pkey PRIMARY KEY (id);


--
-- Name: oauth_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY oauth_clients
    ADD CONSTRAINT oauth_clients_pkey PRIMARY KEY (id);


--
-- Name: oauth_personal_access_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY oauth_personal_access_clients
    ADD CONSTRAINT oauth_personal_access_clients_pkey PRIMARY KEY (id);


--
-- Name: oauth_refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY oauth_refresh_tokens
    ADD CONSTRAINT oauth_refresh_tokens_pkey PRIMARY KEY (id);


--
-- Name: password_resets_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY password_resets
    ADD CONSTRAINT password_resets_pkey PRIMARY KEY (id);


--
-- Name: pelanggan_nama_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pelanggan
    ADD CONSTRAINT pelanggan_nama_unique UNIQUE (nama);


--
-- Name: pelanggan_no_agenda_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pelanggan
    ADD CONSTRAINT pelanggan_no_agenda_unique UNIQUE (no_agenda);


--
-- Name: pelanggan_no_agendapln_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pelanggan
    ADD CONSTRAINT pelanggan_no_agendapln_unique UNIQUE (no_agendapln);


--
-- Name: pelanggan_no_identitas_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pelanggan
    ADD CONSTRAINT pelanggan_no_identitas_unique UNIQUE (no_identitas);


--
-- Name: pelanggan_no_pendaftaran_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pelanggan
    ADD CONSTRAINT pelanggan_no_pendaftaran_unique UNIQUE (no_pendaftaran);


--
-- Name: pelanggan_no_telepon_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pelanggan
    ADD CONSTRAINT pelanggan_no_telepon_unique UNIQUE (no_telepon);


--
-- Name: pelanggan_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pelanggan
    ADD CONSTRAINT pelanggan_pkey PRIMARY KEY (id);


--
-- Name: pelanggan_temp_id_pelanggan_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pelanggan_temp
    ADD CONSTRAINT pelanggan_temp_id_pelanggan_unique UNIQUE (id_pelanggan);


--
-- Name: pelanggan_temp_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pelanggan_temp
    ADD CONSTRAINT pelanggan_temp_pkey PRIMARY KEY (id);


--
-- Name: pembayaran_no_kwitansi_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pembayaran
    ADD CONSTRAINT pembayaran_no_kwitansi_unique UNIQUE (no_kwitansi);


--
-- Name: pembayaran_no_pendaftaran_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pembayaran
    ADD CONSTRAINT pembayaran_no_pendaftaran_unique UNIQUE (no_pendaftaran);


--
-- Name: pembayaran_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pembayaran
    ADD CONSTRAINT pembayaran_pkey PRIMARY KEY (id);


--
-- Name: pemeriksa_no_pemeriksaan_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pemeriksa
    ADD CONSTRAINT pemeriksa_no_pemeriksaan_unique UNIQUE (no_pemeriksaan);


--
-- Name: pemeriksa_no_pendaftaran_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pemeriksa
    ADD CONSTRAINT pemeriksa_no_pendaftaran_unique UNIQUE (no_pendaftaran);


--
-- Name: pemeriksa_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY pemeriksa
    ADD CONSTRAINT pemeriksa_pkey PRIMARY KEY (id);


--
-- Name: penyedia_listrik_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY penyedia_listrik
    ADD CONSTRAINT penyedia_listrik_pkey PRIMARY KEY (id);


--
-- Name: perangkat_id_users_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY perangkat
    ADD CONSTRAINT perangkat_id_users_unique UNIQUE (id_users);


--
-- Name: perangkat_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY perangkat
    ADD CONSTRAINT perangkat_pkey PRIMARY KEY (id);


--
-- Name: perangkat_username_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY perangkat
    ADD CONSTRAINT perangkat_username_unique UNIQUE (username);


--
-- Name: provinsi_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY provinsi
    ADD CONSTRAINT provinsi_pkey PRIMARY KEY (id);


--
-- Name: rekap_area_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY rekap_area
    ADD CONSTRAINT rekap_area_pkey PRIMARY KEY (id);


--
-- Name: rekap_pusat_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY rekap_pusat
    ADD CONSTRAINT rekap_pusat_pkey PRIMARY KEY (id);


--
-- Name: rekap_sub_area_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY rekap_sub_area
    ADD CONSTRAINT rekap_sub_area_pkey PRIMARY KEY (id);


--
-- Name: rekap_wilayah_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY rekap_wilayah
    ADD CONSTRAINT rekap_wilayah_pkey PRIMARY KEY (id);


--
-- Name: runingtext_frontend_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY runingtext_frontend
    ADD CONSTRAINT runingtext_frontend_pkey PRIMARY KEY (id);


--
-- Name: sertifikasi_no_pendaftaran_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY sertifikasi
    ADD CONSTRAINT sertifikasi_no_pendaftaran_unique UNIQUE (no_pendaftaran);


--
-- Name: sertifikasi_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY sertifikasi
    ADD CONSTRAINT sertifikasi_pkey PRIMARY KEY (id);


--
-- Name: setor_area_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY setor_area
    ADD CONSTRAINT setor_area_pkey PRIMARY KEY (id);


--
-- Name: setor_sub_area_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY setor_sub_area
    ADD CONSTRAINT setor_sub_area_pkey PRIMARY KEY (id);


--
-- Name: setor_wilayah_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY setor_wilayah
    ADD CONSTRAINT setor_wilayah_pkey PRIMARY KEY (id);


--
-- Name: share_internal_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY share_internal
    ADD CONSTRAINT share_internal_pkey PRIMARY KEY (id);


--
-- Name: share_wilayah_id_kwilayah_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY share_wilayah
    ADD CONSTRAINT share_wilayah_id_kwilayah_unique UNIQUE (id_kwilayah);


--
-- Name: share_wilayah_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY share_wilayah
    ADD CONSTRAINT share_wilayah_pkey PRIMARY KEY (id);


--
-- Name: slideshow_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY slideshow
    ADD CONSTRAINT slideshow_pkey PRIMARY KEY (id);


--
-- Name: staff_btl_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY staff_btl
    ADD CONSTRAINT staff_btl_pkey PRIMARY KEY (id);


--
-- Name: sub_area_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY sub_area
    ADD CONSTRAINT sub_area_pkey PRIMARY KEY (id);


--
-- Name: surat_tugas_no_surat_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY surat_tugas
    ADD CONSTRAINT surat_tugas_no_surat_unique UNIQUE (no_surat);


--
-- Name: surat_tugas_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY surat_tugas
    ADD CONSTRAINT surat_tugas_pkey PRIMARY KEY (id);


--
-- Name: table_calon_pelanggan_web_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY calon_pelanggan_web
    ADD CONSTRAINT table_calon_pelanggan_web_pkey PRIMARY KEY (id);


--
-- Name: tagihan_area_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY tagihan_area
    ADD CONSTRAINT tagihan_area_pkey PRIMARY KEY (id);


--
-- Name: tagihan_pusats_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY tagihan_pusat
    ADD CONSTRAINT tagihan_pusats_pkey PRIMARY KEY (id);


--
-- Name: tagihan_sub_area_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY tagihan_sub_area
    ADD CONSTRAINT tagihan_sub_area_pkey PRIMARY KEY (id);


--
-- Name: tagihan_wilayah_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY tagihan_wilayah
    ADD CONSTRAINT tagihan_wilayah_pkey PRIMARY KEY (id);


--
-- Name: tarif_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY tarif
    ADD CONSTRAINT tarif_pkey PRIMARY KEY (id);


--
-- Name: users_email_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_un; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_un UNIQUE (username);


--
-- Name: verifikasi_no_lhpp_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY verifikasi
    ADD CONSTRAINT verifikasi_no_lhpp_unique UNIQUE (no_lhpp);


--
-- Name: verifikasi_no_pendaftaran_unique; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY verifikasi
    ADD CONSTRAINT verifikasi_no_pendaftaran_unique UNIQUE (no_pendaftaran);


--
-- Name: verifikasi_pkey; Type: CONSTRAINT; Schema: public; Owner: sertifik; Tablespace: 
--

ALTER TABLE ONLY verifikasi
    ADD CONSTRAINT verifikasi_pkey PRIMARY KEY (id);


--
-- Name: notifications_notifiable_id_notifiable_type_index; Type: INDEX; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE INDEX notifications_notifiable_id_notifiable_type_index ON notifications USING btree (notifiable_id, notifiable_type);


--
-- Name: oauth_access_tokens_user_id_index; Type: INDEX; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE INDEX oauth_access_tokens_user_id_index ON oauth_access_tokens USING btree (user_id);


--
-- Name: oauth_clients_user_id_index; Type: INDEX; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE INDEX oauth_clients_user_id_index ON oauth_clients USING btree (user_id);


--
-- Name: oauth_personal_access_clients_client_id_index; Type: INDEX; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE INDEX oauth_personal_access_clients_client_id_index ON oauth_personal_access_clients USING btree (client_id);


--
-- Name: oauth_refresh_tokens_access_token_id_index; Type: INDEX; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE INDEX oauth_refresh_tokens_access_token_id_index ON oauth_refresh_tokens USING btree (access_token_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: sertifik; Tablespace: 
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: Berita; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE "Berita" FROM PUBLIC;
REVOKE ALL ON TABLE "Berita" FROM sertifik;
GRANT ALL ON TABLE "Berita" TO sertifik;
GRANT ALL ON TABLE "Berita" TO sertifik_slo;


--
-- Name: adminfrontend; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE adminfrontend FROM PUBLIC;
REVOKE ALL ON TABLE adminfrontend FROM sertifik;
GRANT ALL ON TABLE adminfrontend TO sertifik;
GRANT ALL ON TABLE adminfrontend TO sertifik_slo;


--
-- Name: alokasi_share; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE alokasi_share FROM PUBLIC;
REVOKE ALL ON TABLE alokasi_share FROM sertifik;
GRANT ALL ON TABLE alokasi_share TO sertifik;
GRANT ALL ON TABLE alokasi_share TO sertifik_slo;


--
-- Name: alokasi_share_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE alokasi_share_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE alokasi_share_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE alokasi_share_id_seq TO sertifik;
GRANT ALL ON SEQUENCE alokasi_share_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE alokasi_share_id_seq TO sertifik_slo;


--
-- Name: asosiasi; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE asosiasi FROM PUBLIC;
REVOKE ALL ON TABLE asosiasi FROM sertifik;
GRANT ALL ON TABLE asosiasi TO sertifik;
GRANT ALL ON TABLE asosiasi TO sertifik_slo;


--
-- Name: asosiasi_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE asosiasi_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE asosiasi_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE asosiasi_id_seq TO sertifik;
GRANT ALL ON SEQUENCE asosiasi_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE asosiasi_id_seq TO sertifik_slo;


--
-- Name: informasi; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE informasi FROM PUBLIC;
REVOKE ALL ON TABLE informasi FROM sertifik;
GRANT ALL ON TABLE informasi TO sertifik;
GRANT ALL ON TABLE informasi TO sertifik_slo;


--
-- Name: berita_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE berita_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE berita_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE berita_id_seq TO sertifik;
GRANT ALL ON SEQUENCE berita_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE berita_id_seq TO sertifik_slo;


--
-- Name: berita; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE berita FROM PUBLIC;
REVOKE ALL ON TABLE berita FROM sertifik;
GRANT ALL ON TABLE berita TO sertifik;
GRANT ALL ON TABLE berita TO sertifik_slo;


--
-- Name: btl; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE btl FROM PUBLIC;
REVOKE ALL ON TABLE btl FROM sertifik;
GRANT ALL ON TABLE btl TO sertifik;
GRANT ALL ON TABLE btl TO sertifik_slo;


--
-- Name: btl_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE btl_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE btl_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE btl_id_seq TO sertifik;
GRANT ALL ON SEQUENCE btl_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE btl_id_seq TO sertifik_slo;


--
-- Name: calon_pelanggan_web; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE calon_pelanggan_web FROM PUBLIC;
REVOKE ALL ON TABLE calon_pelanggan_web FROM sertifik;
GRANT ALL ON TABLE calon_pelanggan_web TO sertifik;
GRANT ALL ON TABLE calon_pelanggan_web TO sertifik_slo;


--
-- Name: daya; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE daya FROM PUBLIC;
REVOKE ALL ON TABLE daya FROM sertifik;
GRANT ALL ON TABLE daya TO sertifik;
GRANT ALL ON TABLE daya TO sertifik_slo;


--
-- Name: daya_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE daya_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE daya_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE daya_id_seq TO sertifik;
GRANT ALL ON SEQUENCE daya_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE daya_id_seq TO sertifik_slo;


--
-- Name: galeri; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE galeri FROM PUBLIC;
REVOKE ALL ON TABLE galeri FROM sertifik;
GRANT ALL ON TABLE galeri TO sertifik;
GRANT ALL ON TABLE galeri TO sertifik_slo;


--
-- Name: galeri_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE galeri_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE galeri_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE galeri_id_seq TO sertifik;
GRANT ALL ON SEQUENCE galeri_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE galeri_id_seq TO sertifik_slo;


--
-- Name: jenis_bangunan; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE jenis_bangunan FROM PUBLIC;
REVOKE ALL ON TABLE jenis_bangunan FROM sertifik;
GRANT ALL ON TABLE jenis_bangunan TO sertifik;
GRANT ALL ON TABLE jenis_bangunan TO sertifik_slo;


--
-- Name: jenis_bangunan_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE jenis_bangunan_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE jenis_bangunan_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE jenis_bangunan_id_seq TO sertifik;
GRANT ALL ON SEQUENCE jenis_bangunan_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE jenis_bangunan_id_seq TO sertifik_slo;


--
-- Name: kantor_area; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE kantor_area FROM PUBLIC;
REVOKE ALL ON TABLE kantor_area FROM sertifik;
GRANT ALL ON TABLE kantor_area TO sertifik;
GRANT ALL ON TABLE kantor_area TO sertifik_slo;


--
-- Name: kantor_area_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE kantor_area_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE kantor_area_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE kantor_area_id_seq TO sertifik;
GRANT ALL ON SEQUENCE kantor_area_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE kantor_area_id_seq TO sertifik_slo;


--
-- Name: kantor_wilayah; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE kantor_wilayah FROM PUBLIC;
REVOKE ALL ON TABLE kantor_wilayah FROM sertifik;
GRANT ALL ON TABLE kantor_wilayah TO sertifik;
GRANT ALL ON TABLE kantor_wilayah TO sertifik_slo;


--
-- Name: kantor_wilayah_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE kantor_wilayah_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE kantor_wilayah_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE kantor_wilayah_id_seq TO sertifik;
GRANT ALL ON SEQUENCE kantor_wilayah_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE kantor_wilayah_id_seq TO sertifik_slo;


--
-- Name: kanwil; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE kanwil FROM PUBLIC;
REVOKE ALL ON TABLE kanwil FROM sertifik;
GRANT ALL ON TABLE kanwil TO sertifik;
GRANT ALL ON TABLE kanwil TO sertifik_slo;


--
-- Name: kota; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE kota FROM PUBLIC;
REVOKE ALL ON TABLE kota FROM sertifik;
GRANT ALL ON TABLE kota TO sertifik;
GRANT ALL ON TABLE kota TO sertifik_slo;


--
-- Name: kota_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE kota_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE kota_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE kota_id_seq TO sertifik;
GRANT ALL ON SEQUENCE kota_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE kota_id_seq TO sertifik_slo;


--
-- Name: log_status; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE log_status FROM PUBLIC;
REVOKE ALL ON TABLE log_status FROM sertifik;
GRANT ALL ON TABLE log_status TO sertifik;
GRANT ALL ON TABLE log_status TO sertifik_slo;


--
-- Name: log_status_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE log_status_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE log_status_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE log_status_id_seq TO sertifik;
GRANT ALL ON SEQUENCE log_status_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE log_status_id_seq TO sertifik_slo;


--
-- Name: migrations; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE migrations FROM PUBLIC;
REVOKE ALL ON TABLE migrations FROM sertifik;
GRANT ALL ON TABLE migrations TO sertifik;
GRANT ALL ON TABLE migrations TO sertifik_slo;


--
-- Name: migrations_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE migrations_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE migrations_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE migrations_id_seq TO sertifik;
GRANT ALL ON SEQUENCE migrations_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE migrations_id_seq TO sertifik_slo;


--
-- Name: notifications; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE notifications FROM PUBLIC;
REVOKE ALL ON TABLE notifications FROM sertifik;
GRANT ALL ON TABLE notifications TO sertifik;
GRANT ALL ON TABLE notifications TO sertifik_slo;


--
-- Name: oauth_access_tokens; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE oauth_access_tokens FROM PUBLIC;
REVOKE ALL ON TABLE oauth_access_tokens FROM sertifik;
GRANT ALL ON TABLE oauth_access_tokens TO sertifik;
GRANT ALL ON TABLE oauth_access_tokens TO sertifik_slo;


--
-- Name: oauth_auth_codes; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE oauth_auth_codes FROM PUBLIC;
REVOKE ALL ON TABLE oauth_auth_codes FROM sertifik;
GRANT ALL ON TABLE oauth_auth_codes TO sertifik;
GRANT ALL ON TABLE oauth_auth_codes TO sertifik_slo;


--
-- Name: oauth_clients; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE oauth_clients FROM PUBLIC;
REVOKE ALL ON TABLE oauth_clients FROM sertifik;
GRANT ALL ON TABLE oauth_clients TO sertifik;
GRANT ALL ON TABLE oauth_clients TO sertifik_slo;


--
-- Name: oauth_clients_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE oauth_clients_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE oauth_clients_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE oauth_clients_id_seq TO sertifik;
GRANT ALL ON SEQUENCE oauth_clients_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE oauth_clients_id_seq TO sertifik_slo;


--
-- Name: oauth_personal_access_clients; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE oauth_personal_access_clients FROM PUBLIC;
REVOKE ALL ON TABLE oauth_personal_access_clients FROM sertifik;
GRANT ALL ON TABLE oauth_personal_access_clients TO sertifik;
GRANT ALL ON TABLE oauth_personal_access_clients TO sertifik_slo;


--
-- Name: oauth_personal_access_clients_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE oauth_personal_access_clients_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE oauth_personal_access_clients_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE oauth_personal_access_clients_id_seq TO sertifik;
GRANT ALL ON SEQUENCE oauth_personal_access_clients_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE oauth_personal_access_clients_id_seq TO sertifik_slo;


--
-- Name: oauth_refresh_tokens; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE oauth_refresh_tokens FROM PUBLIC;
REVOKE ALL ON TABLE oauth_refresh_tokens FROM sertifik;
GRANT ALL ON TABLE oauth_refresh_tokens TO sertifik;
GRANT ALL ON TABLE oauth_refresh_tokens TO sertifik_slo;


--
-- Name: password_resets; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE password_resets FROM PUBLIC;
REVOKE ALL ON TABLE password_resets FROM sertifik;
GRANT ALL ON TABLE password_resets TO sertifik;
GRANT ALL ON TABLE password_resets TO sertifik_slo;


--
-- Name: password_resets_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE password_resets_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE password_resets_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE password_resets_id_seq TO sertifik;
GRANT ALL ON SEQUENCE password_resets_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE password_resets_id_seq TO sertifik_slo;


--
-- Name: pelanggan; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE pelanggan FROM PUBLIC;
REVOKE ALL ON TABLE pelanggan FROM sertifik;
GRANT ALL ON TABLE pelanggan TO sertifik;
GRANT ALL ON TABLE pelanggan TO sertifik_slo;


--
-- Name: pelanggan_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE pelanggan_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE pelanggan_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE pelanggan_id_seq TO sertifik;
GRANT ALL ON SEQUENCE pelanggan_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE pelanggan_id_seq TO sertifik_slo;


--
-- Name: pelanggan_temp; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE pelanggan_temp FROM PUBLIC;
REVOKE ALL ON TABLE pelanggan_temp FROM sertifik;
GRANT ALL ON TABLE pelanggan_temp TO sertifik;
GRANT ALL ON TABLE pelanggan_temp TO sertifik_slo;


--
-- Name: pelanggan_temp_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE pelanggan_temp_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE pelanggan_temp_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE pelanggan_temp_id_seq TO sertifik;
GRANT ALL ON SEQUENCE pelanggan_temp_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE pelanggan_temp_id_seq TO sertifik_slo;


--
-- Name: pembayaran; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE pembayaran FROM PUBLIC;
REVOKE ALL ON TABLE pembayaran FROM sertifik;
GRANT ALL ON TABLE pembayaran TO sertifik;
GRANT ALL ON TABLE pembayaran TO sertifik_slo;


--
-- Name: pembayaran_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE pembayaran_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE pembayaran_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE pembayaran_id_seq TO sertifik;
GRANT ALL ON SEQUENCE pembayaran_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE pembayaran_id_seq TO sertifik_slo;


--
-- Name: pemeriksa; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE pemeriksa FROM PUBLIC;
REVOKE ALL ON TABLE pemeriksa FROM sertifik;
GRANT ALL ON TABLE pemeriksa TO sertifik;
GRANT ALL ON TABLE pemeriksa TO sertifik_slo;


--
-- Name: pemeriksa_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE pemeriksa_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE pemeriksa_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE pemeriksa_id_seq TO sertifik;
GRANT ALL ON SEQUENCE pemeriksa_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE pemeriksa_id_seq TO sertifik_slo;


--
-- Name: penyedia_listrik; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE penyedia_listrik FROM PUBLIC;
REVOKE ALL ON TABLE penyedia_listrik FROM sertifik;
GRANT ALL ON TABLE penyedia_listrik TO sertifik;
GRANT ALL ON TABLE penyedia_listrik TO sertifik_slo;


--
-- Name: penyedia_listrik_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE penyedia_listrik_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE penyedia_listrik_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE penyedia_listrik_id_seq TO sertifik;
GRANT ALL ON SEQUENCE penyedia_listrik_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE penyedia_listrik_id_seq TO sertifik_slo;


--
-- Name: perangkat; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE perangkat FROM PUBLIC;
REVOKE ALL ON TABLE perangkat FROM sertifik;
GRANT ALL ON TABLE perangkat TO sertifik;
GRANT ALL ON TABLE perangkat TO sertifik_slo;


--
-- Name: perangkat_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE perangkat_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE perangkat_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE perangkat_id_seq TO sertifik;
GRANT ALL ON SEQUENCE perangkat_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE perangkat_id_seq TO sertifik_slo;


--
-- Name: provinsi; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE provinsi FROM PUBLIC;
REVOKE ALL ON TABLE provinsi FROM sertifik;
GRANT ALL ON TABLE provinsi TO sertifik;
GRANT ALL ON TABLE provinsi TO sertifik_slo;


--
-- Name: provinsi_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE provinsi_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE provinsi_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE provinsi_id_seq TO sertifik;
GRANT ALL ON SEQUENCE provinsi_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE provinsi_id_seq TO sertifik_slo;


--
-- Name: rekap_area; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE rekap_area FROM PUBLIC;
REVOKE ALL ON TABLE rekap_area FROM sertifik;
GRANT ALL ON TABLE rekap_area TO sertifik;
GRANT ALL ON TABLE rekap_area TO sertifik_slo;


--
-- Name: rekap_area_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE rekap_area_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE rekap_area_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE rekap_area_id_seq TO sertifik;
GRANT ALL ON SEQUENCE rekap_area_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE rekap_area_id_seq TO sertifik_slo;


--
-- Name: rekap_pusat; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE rekap_pusat FROM PUBLIC;
REVOKE ALL ON TABLE rekap_pusat FROM sertifik;
GRANT ALL ON TABLE rekap_pusat TO sertifik;
GRANT ALL ON TABLE rekap_pusat TO sertifik_slo;


--
-- Name: rekap_pusat_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE rekap_pusat_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE rekap_pusat_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE rekap_pusat_id_seq TO sertifik;
GRANT ALL ON SEQUENCE rekap_pusat_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE rekap_pusat_id_seq TO sertifik_slo;


--
-- Name: rekap_sub_area; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE rekap_sub_area FROM PUBLIC;
REVOKE ALL ON TABLE rekap_sub_area FROM sertifik;
GRANT ALL ON TABLE rekap_sub_area TO sertifik;
GRANT ALL ON TABLE rekap_sub_area TO sertifik_slo;


--
-- Name: rekap_sub_area_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE rekap_sub_area_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE rekap_sub_area_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE rekap_sub_area_id_seq TO sertifik;
GRANT ALL ON SEQUENCE rekap_sub_area_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE rekap_sub_area_id_seq TO sertifik_slo;


--
-- Name: rekap_wilayah; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE rekap_wilayah FROM PUBLIC;
REVOKE ALL ON TABLE rekap_wilayah FROM sertifik;
GRANT ALL ON TABLE rekap_wilayah TO sertifik;
GRANT ALL ON TABLE rekap_wilayah TO sertifik_slo;


--
-- Name: rekap_wilayah_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE rekap_wilayah_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE rekap_wilayah_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE rekap_wilayah_id_seq TO sertifik;
GRANT ALL ON SEQUENCE rekap_wilayah_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE rekap_wilayah_id_seq TO sertifik_slo;


--
-- Name: runingtext_frontend; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE runingtext_frontend FROM PUBLIC;
REVOKE ALL ON TABLE runingtext_frontend FROM sertifik;
GRANT ALL ON TABLE runingtext_frontend TO sertifik;
GRANT ALL ON TABLE runingtext_frontend TO sertifik_slo;


--
-- Name: runingtext_frontend_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE runingtext_frontend_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE runingtext_frontend_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE runingtext_frontend_id_seq TO sertifik;
GRANT ALL ON SEQUENCE runingtext_frontend_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE runingtext_frontend_id_seq TO sertifik_slo;


--
-- Name: sertifikasi; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE sertifikasi FROM PUBLIC;
REVOKE ALL ON TABLE sertifikasi FROM sertifik;
GRANT ALL ON TABLE sertifikasi TO sertifik;
GRANT ALL ON TABLE sertifikasi TO sertifik_slo;


--
-- Name: sertifikasi_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE sertifikasi_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE sertifikasi_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE sertifikasi_id_seq TO sertifik;
GRANT ALL ON SEQUENCE sertifikasi_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE sertifikasi_id_seq TO sertifik_slo;


--
-- Name: setor_area; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE setor_area FROM PUBLIC;
REVOKE ALL ON TABLE setor_area FROM sertifik;
GRANT ALL ON TABLE setor_area TO sertifik;
GRANT ALL ON TABLE setor_area TO sertifik_slo;


--
-- Name: setor_area_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE setor_area_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE setor_area_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE setor_area_id_seq TO sertifik;
GRANT ALL ON SEQUENCE setor_area_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE setor_area_id_seq TO sertifik_slo;


--
-- Name: setor_sub_area; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE setor_sub_area FROM PUBLIC;
REVOKE ALL ON TABLE setor_sub_area FROM sertifik;
GRANT ALL ON TABLE setor_sub_area TO sertifik;
GRANT ALL ON TABLE setor_sub_area TO sertifik_slo;


--
-- Name: setor_sub_area_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE setor_sub_area_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE setor_sub_area_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE setor_sub_area_id_seq TO sertifik;
GRANT ALL ON SEQUENCE setor_sub_area_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE setor_sub_area_id_seq TO sertifik_slo;


--
-- Name: setor_wilayah; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE setor_wilayah FROM PUBLIC;
REVOKE ALL ON TABLE setor_wilayah FROM sertifik;
GRANT ALL ON TABLE setor_wilayah TO sertifik;
GRANT ALL ON TABLE setor_wilayah TO sertifik_slo;


--
-- Name: setor_wilayah_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE setor_wilayah_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE setor_wilayah_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE setor_wilayah_id_seq TO sertifik;
GRANT ALL ON SEQUENCE setor_wilayah_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE setor_wilayah_id_seq TO sertifik_slo;


--
-- Name: share_internal; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE share_internal FROM PUBLIC;
REVOKE ALL ON TABLE share_internal FROM sertifik;
GRANT ALL ON TABLE share_internal TO sertifik;
GRANT ALL ON TABLE share_internal TO sertifik_slo;


--
-- Name: share_internal_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE share_internal_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE share_internal_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE share_internal_id_seq TO sertifik;
GRANT ALL ON SEQUENCE share_internal_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE share_internal_id_seq TO sertifik_slo;


--
-- Name: share_wilayah; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE share_wilayah FROM PUBLIC;
REVOKE ALL ON TABLE share_wilayah FROM sertifik;
GRANT ALL ON TABLE share_wilayah TO sertifik;
GRANT ALL ON TABLE share_wilayah TO sertifik_slo;


--
-- Name: share_wilayah_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE share_wilayah_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE share_wilayah_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE share_wilayah_id_seq TO sertifik;
GRANT ALL ON SEQUENCE share_wilayah_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE share_wilayah_id_seq TO sertifik_slo;


--
-- Name: slideshow; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE slideshow FROM PUBLIC;
REVOKE ALL ON TABLE slideshow FROM sertifik;
GRANT ALL ON TABLE slideshow TO sertifik;
GRANT ALL ON TABLE slideshow TO sertifik_slo;


--
-- Name: slideshow_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE slideshow_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE slideshow_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE slideshow_id_seq TO sertifik;
GRANT ALL ON SEQUENCE slideshow_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE slideshow_id_seq TO sertifik_slo;


--
-- Name: staff_btl; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE staff_btl FROM PUBLIC;
REVOKE ALL ON TABLE staff_btl FROM sertifik;
GRANT ALL ON TABLE staff_btl TO sertifik;
GRANT ALL ON TABLE staff_btl TO sertifik_slo;


--
-- Name: staff_btl_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE staff_btl_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE staff_btl_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE staff_btl_id_seq TO sertifik;
GRANT ALL ON SEQUENCE staff_btl_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE staff_btl_id_seq TO sertifik_slo;


--
-- Name: sub_area; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE sub_area FROM PUBLIC;
REVOKE ALL ON TABLE sub_area FROM sertifik;
GRANT ALL ON TABLE sub_area TO sertifik;
GRANT ALL ON TABLE sub_area TO sertifik_slo;


--
-- Name: sub_area_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE sub_area_id_seq FROM PUBLIC;
GRANT ALL ON SEQUENCE sub_area_id_seq TO sertifik_user WITH GRANT OPTION;
GRANT ALL ON SEQUENCE sub_area_id_seq TO sertifik_slo WITH GRANT OPTION;


--
-- Name: surat_tugas; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE surat_tugas FROM PUBLIC;
REVOKE ALL ON TABLE surat_tugas FROM sertifik;
GRANT ALL ON TABLE surat_tugas TO sertifik;
GRANT ALL ON TABLE surat_tugas TO sertifik_slo;


--
-- Name: surat_tugas_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE surat_tugas_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE surat_tugas_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE surat_tugas_id_seq TO sertifik;
GRANT ALL ON SEQUENCE surat_tugas_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE surat_tugas_id_seq TO sertifik_slo;


--
-- Name: table_calon_pelanggan_web_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE table_calon_pelanggan_web_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE table_calon_pelanggan_web_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE table_calon_pelanggan_web_id_seq TO sertifik;
GRANT ALL ON SEQUENCE table_calon_pelanggan_web_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE table_calon_pelanggan_web_id_seq TO sertifik_slo;


--
-- Name: tagihan_area; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE tagihan_area FROM PUBLIC;
REVOKE ALL ON TABLE tagihan_area FROM sertifik;
GRANT ALL ON TABLE tagihan_area TO sertifik;
GRANT ALL ON TABLE tagihan_area TO sertifik_slo;


--
-- Name: tagihan_area_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE tagihan_area_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tagihan_area_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE tagihan_area_id_seq TO sertifik;
GRANT ALL ON SEQUENCE tagihan_area_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE tagihan_area_id_seq TO sertifik_slo;


--
-- Name: tagihan_pusat; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE tagihan_pusat FROM PUBLIC;
REVOKE ALL ON TABLE tagihan_pusat FROM sertifik;
GRANT ALL ON TABLE tagihan_pusat TO sertifik;
GRANT ALL ON TABLE tagihan_pusat TO sertifik_slo;


--
-- Name: tagihan_pusats_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE tagihan_pusats_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tagihan_pusats_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE tagihan_pusats_id_seq TO sertifik;
GRANT ALL ON SEQUENCE tagihan_pusats_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE tagihan_pusats_id_seq TO sertifik_slo;


--
-- Name: tagihan_sub_area; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE tagihan_sub_area FROM PUBLIC;
REVOKE ALL ON TABLE tagihan_sub_area FROM sertifik;
GRANT ALL ON TABLE tagihan_sub_area TO sertifik;
GRANT ALL ON TABLE tagihan_sub_area TO sertifik_slo;


--
-- Name: tagihan_sub_area_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE tagihan_sub_area_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tagihan_sub_area_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE tagihan_sub_area_id_seq TO sertifik;
GRANT ALL ON SEQUENCE tagihan_sub_area_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE tagihan_sub_area_id_seq TO sertifik_slo;


--
-- Name: tagihan_wilayah; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE tagihan_wilayah FROM PUBLIC;
REVOKE ALL ON TABLE tagihan_wilayah FROM sertifik;
GRANT ALL ON TABLE tagihan_wilayah TO sertifik;
GRANT ALL ON TABLE tagihan_wilayah TO sertifik_slo;


--
-- Name: tagihan_wilayah_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE tagihan_wilayah_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tagihan_wilayah_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE tagihan_wilayah_id_seq TO sertifik;
GRANT ALL ON SEQUENCE tagihan_wilayah_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE tagihan_wilayah_id_seq TO sertifik_slo;


--
-- Name: tarif; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE tarif FROM PUBLIC;
REVOKE ALL ON TABLE tarif FROM sertifik;
GRANT ALL ON TABLE tarif TO sertifik;
GRANT ALL ON TABLE tarif TO sertifik_slo;


--
-- Name: tarif_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE tarif_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tarif_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE tarif_id_seq TO sertifik;
GRANT ALL ON SEQUENCE tarif_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE tarif_id_seq TO sertifik_slo;


--
-- Name: users; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM sertifik;
GRANT ALL ON TABLE users TO sertifik;
GRANT ALL ON TABLE users TO sertifik_slo;


--
-- Name: users_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE users_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE users_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE users_id_seq TO sertifik;
GRANT ALL ON SEQUENCE users_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE users_id_seq TO sertifik_slo;


--
-- Name: verifikasi; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON TABLE verifikasi FROM PUBLIC;
REVOKE ALL ON TABLE verifikasi FROM sertifik;
GRANT ALL ON TABLE verifikasi TO sertifik;
GRANT ALL ON TABLE verifikasi TO sertifik_slo;


--
-- Name: verifikasi_id_seq; Type: ACL; Schema: public; Owner: sertifik
--

REVOKE ALL ON SEQUENCE verifikasi_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE verifikasi_id_seq FROM sertifik;
GRANT ALL ON SEQUENCE verifikasi_id_seq TO sertifik;
GRANT ALL ON SEQUENCE verifikasi_id_seq TO sertifik_user;
GRANT ALL ON SEQUENCE verifikasi_id_seq TO sertifik_slo;


--
-- PostgreSQL database dump complete
--

