<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::create([
    		"nip" => "1234",
    		"name" => "Hafizh Indra",
    		"email" => "hafizhindram@gmail.com",
    		"username" => "hafizhindram",
    		"jabatan" => "Jabatan",
    		"departemen" => "Departmen",
    		"wewenang" => "Wewenang",
    		"id_kwilayah" => "1",
    		"id_karea" => "1",
    		"id_subarea" => "1",
    		"status" => "Aktif",
    		"password" => \Hash::make('semarang'),
    		"remember_token" => ""
    	]);
    }
}
