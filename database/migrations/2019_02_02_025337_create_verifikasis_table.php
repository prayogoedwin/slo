<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifikasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verifikasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_pendaftaran')->unique();
            $table->string('no_lhpp')->unique();
            $table->string('pemeriksaan_dokumen');
            $table->string('proteksi_sentuh_langung');
            $table->string('pemeriksaan_penghantar');
            $table->string('pemeriksaan_phb');
            $table->string('pemeriksaan_elektroda');
            $table->string('pemeriksaan_polaritas');
            $table->string('pemeriksaan_pemasangan');
            $table->string('pemeriksaan_sni');
            $table->string('pemeriksaan_kamar_mandi');
            $table->string('catatan');
            $table->string('hasil_pemeriksaan');
            $table->string('ceklist_ppu1');
            $table->string('ceklist_ppu2');
            $table->string('hasil_verifikasi');
            $table->string('alasan_reject');
            $table->integer('id_staff');
            $table->integer('status_sertifikasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verifikasis');
    }
}
