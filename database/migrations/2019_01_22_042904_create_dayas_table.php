<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daya', function (Blueprint $table) {
            $table->increments('id');
            $table->string('daya')->unique();
            $table->string('jenis_fasa');
            $table->decimal('harga', 8, 0)->nullable()->default(0);
            $table->decimal('biaya', 8, 0)->nullable()->default(0);
            $table->decimal('ppn', 8, 0)->nullable()->default(0);
            $table->decimal('total', 8, 0)->default(0);
            $table->string('terbilang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dayas');
    }
}
