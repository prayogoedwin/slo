<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBtlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('btl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kodefikasi')->unique();
            $table->string('nama_btl');
            $table->integer('id_kwilayah');
            $table->integer('id_karea');
            $table->string('alamat');
            $table->string('nama_direktur');
            $table->string('nama_ahli_teknik');
            $table->integer('id_asosiasi');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('btls');
    }
}
