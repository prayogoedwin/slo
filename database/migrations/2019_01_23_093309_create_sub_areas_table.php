<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_area', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kwilayah');
            $table->integer('id_karea');
            $table->string('nama_subarea');
            $table->integer('kode');
            $table->string('alamat')->nullable();
            $table->string('nama_manager')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_areas');
    }
}
