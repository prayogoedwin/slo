<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemeriksasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_pendaftaran')->unique();
            $table->string('no_surat_tugas');
            $table->string('no_pemeriksaan')->unique()->nullable();
            $table->string('no_lhpp');
            $table->date('tgl_lhpp');
            $table->string('gambar_instalasi');
            $table->string('diagram_garis_tunggal');
            $table->string('pe_utama');
            $table->string('pe_cabang');
            $table->string('pe_akhir');
            $table->string('pe_kotak_kontak');
            $table->string('jenis_peng_utama');
            $table->string('jenis_peng_cabang');
            $table->string('jenis_peng_akhir');
            $table->string('penghantar_bumi_jenis');
            $table->string('penghantar_bumi_penampang');
            $table->string('penghantar_bumi_sistem');
            $table->string('saklar_utama');
            $table->string('saklar_cabang1');
            $table->string('saklar_cabang2');
            $table->string('phbk_utama');
            $table->string('phbk_cabang1');
            $table->string('phbk_cabang2');
            $table->string('penghantar_utama');
            $table->string('penghantar_cabang');
            $table->string('penghantar_akhir');
            $table->string('penghantar_3fasa');
            $table->string('fitting_lampu');
            $table->string('kotak_kontak');
            $table->string('sakelar');
            $table->string('tinggi_kotak_kontak');
            $table->string('tinggi_phbk');
            $table->string('jenis_kotak_kontak');
            $table->string('tanda_komponen');
            $table->string('pengujian_pembebanan');
            $table->string('jml_phb_utama');
            $table->string('jml_phb_1fasa');
            $table->string('jml_phb_3fasa');
            $table->string('jml_phb_cabang');
            $table->string('jml_saluran_cabang');
            $table->string('jml_saluran_akhir');
            $table->string('jml_titik_lampu');
            $table->string('jml_sakelar');
            $table->string('kkb');
            $table->string('kkk');
            $table->string('tahanan_isolasi_penghantar');
            $table->string('resisten_pembumian');
            $table->string('jml_motor_listrik_unit');
            $table->string('jml_motor_listrik_kwh');
            $table->string('catatan');
            $table->string('foto1');
            $table->string('foto2');
            $table->string('foto3');
            $table->string('foto4');
            $table->string('foto5');
            $table->string('location')->nullable();
            $table->float('lat')->nullable();
            $table->float('lng')->nullable();
            $table->integer('status_verifikasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksas');
    }
}
