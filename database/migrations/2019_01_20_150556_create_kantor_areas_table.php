<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKantorAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kantor_area', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kwilayah');
            $table->integer('kode_kota');
            $table->string('nama');
            $table->string('manager');
            $table->string('pjt');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kantor_areas');
    }
}
