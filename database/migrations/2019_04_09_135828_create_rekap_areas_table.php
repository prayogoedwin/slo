<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekapAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekap_area', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_kwilayah');
          $table->integer('id_karea');
          $table->date('bulan');
          $table->decimal('pendapatan_pelanggan');
          $table->decimal('pendapatan_sub_area');
          $table->decimal('total');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekap_areas');
    }
}
