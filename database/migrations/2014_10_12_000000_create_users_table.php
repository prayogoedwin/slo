<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nip')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('no_telp')->unique();
            $table->string('username')->unique();
            $table->string('jabatan')->nullable();
            $table->string('departemen')->nullable();
            $table->string('wewenang')->nullable();
            $table->integer('id_kwilayah')->nullable();
            $table->integer('id_karea')->nullable();
            $table->integer('id_subarea')->nullable();
            $table->string('status')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
