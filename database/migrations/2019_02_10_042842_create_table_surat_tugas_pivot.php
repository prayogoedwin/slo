<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSuratTugasPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelanggan_temp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pelanggan')->unique();
            $table->integer('id_tarif');
            $table->integer('id_daya');
            $table->integer('id_penyedia');
            $table->integer('id_subarea');
            $table->integer('id_staff');
            $table->string('no_surat')->nullable();
            $table->integer('status_terbit');
            $table->integer('status_periksa')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelanggan_temp');
    }
}
