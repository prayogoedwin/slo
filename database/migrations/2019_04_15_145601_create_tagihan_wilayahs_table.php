<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagihanWilayahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagihan_wilayah', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_ref');
            $table->integer('id_kwilayah');
            $table->date('periode');
            $table->integer('id_share');
            $table->decimal('rp_share');
            $table->decimal('jml_tagihan');
            $table->decimal('jml_terbayar');
            $table->decimal('jml_hutang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagihan_wilayah');
    }
}
