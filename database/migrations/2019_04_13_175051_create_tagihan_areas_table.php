<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagihanAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagihan_area', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_ref');
            $table->integer('id_kwilayah');
            $table->integer('id_karea');
            $table->date('periode');
            $table->decimal('jml_tagihan');
            $table->decimal('jml_terbayar');
            $table->decimal('jml_hutang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagihan_areas');
    }
}
