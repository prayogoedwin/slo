<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSertifikasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sertifikasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_pendaftaran')->unique();
            $table->string('no_agenda');
            $table->string('no_lhpp');
            $table->string('no_registrasi');
            $table->string('no_sertifikat');
            $table->string('no_seri');
            $table->integer('pjt_cetak');
            $table->integer('tt_cetak');
            $table->date('tgl_cetak');
            $table->date('masa_berlaku');
            $table->string('penanda_tangan');
            $table->integer('dicetak_oleh')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sertifikasi');
    }
}
