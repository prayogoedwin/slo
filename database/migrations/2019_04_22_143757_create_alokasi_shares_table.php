<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlokasiSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alokasi_share', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_alokasi');
            $table->date('periode');
            $table->decimal('pendapatan_kotor');
            $table->decimal('alokasi_pengembangan');
            $table->decimal('jumlah_share');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alokasi_share');
    }
}
