<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelanggansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelanggan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_pendaftaran')->unique();
            $table->string('no_agenda')->nullable()->unique();
            $table->string('nama')->unique();
            $table->string('no_identitas')->unique();
            $table->string('no_telepon')->unique();
            $table->string('alamat');
            $table->integer('id_kwilayah')->nullable();
            $table->integer('kode_kota')->nullable();
            $table->integer('id_karea')->nullable();
            $table->integer('id_subarea')->nullable();
            $table->integer('telp')->nullable();
            $table->integer('id_tarif')->nullable();
            $table->integer('id_daya')->nullable();
            $table->integer('id_btl')->nullable();
            $table->integer('id_bangunan')->nullable();
            $table->integer('id_penyedia')->nullable();
            $table->string('tipe')->nullable();
            $table->string('status')->nullable();
            $table->integer('no_gambar')->nullable();
            $table->date('tgl_gambar')->nullable();
            $table->integer('id_asosiasi')->nullable();
            $table->string('no_agendapln')->nullable()->unique();
            $table->date('tgl_regpln')->nullable();
            $table->string('diinput_oleh')->nullable();
            $table->string('status_temp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelanggans');
    }
}
