<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagihanSubAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagihan_sub_area', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_ref');
            $table->integer('id_karea');
            $table->integer('id_subarea');
            $table->date('periode');
            $table->decimal('jml_tagihan');
            $table->decimal('jml_terbayar');
            $table->decimal('jml_hutang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagihan_sub_area');
    }
}
